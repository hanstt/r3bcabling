/*
 * rackLAND2 
 * 
 * VME Crate with TAQ
 *
 *
 *
 */

FOUR_POST_RACK(rackLAND2)
{
  LABEL("NeuLAND readout <<TAMEX>> cards 1-7");
}
 
VME_CRATE(rackLAND2c3)
 {
 ;
 }


/*
 * TACQUILA BOARDS GFI. SIGNAL from JOINER_8 cables
 *
 * adapted from documentation for LAND in s412
 *
 */

LANDFEE2(rackLAND2c3s1u1)   // slot 1  //TAMEX card 7
{
 in1: rDPL04c213/out8;
 in2: rDPL04c213/out7;
 in3: rDPL04c213/out6;
 in4: rDPL04c213/out5;
 in5: rDPL04c213/out4;
 in6: rDPL04c213/out3;
 in7: rDPL04c213/out2;
 in8: rDPL04c213/out1;

// in9:  rNP1c22/out8;
// in10: rNP1c22/out7;
// in11: rNP1c22/out6;
// in12: rNP1c22/out5;
 in13: rDPL04c214/out4;
 in14: rDPL04c214/out3;
 in15: rDPL04c214/out2;
 in16: rDPL04c214/out1;


 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}

LANDFEE2(rackLAND2c3s2u1)  // slot 2
{
 in1: rDPL02c201/out8;
 in2: rDPL02c201/out7;
 in3: rDPL02c201/out6;
 in4: rDPL02c201/out5;
 in5: rDPL02c201/out4;
 in6: rDPL02c201/out3;
 in7: rDPL02c201/out2;
 in8: rDPL02c201/out1;

 in9:  rDPL02c202/out8;
 in10: rDPL02c202/out7;
 in11: rDPL02c202/out6;
 in12: rDPL02c202/out5;
 in13: rDPL02c202/out4;
 in14: rDPL02c202/out3;
 in15: rDPL02c202/out2;
 in16: rDPL02c202/out1;
 
 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}
LANDFEE2(rackLAND2c3s3u1)  //slot 3
{
 in1: rDPL02c203/out8;
 in2: rDPL02c203/out7;
 in3: rDPL02c203/out6;
 in4: rDPL02c203/out5;
 in5: rDPL02c203/out4;
 in6: rDPL02c203/out3;
 in7: rDPL02c203/out2;
 in8: rDPL02c203/out1;

 in9:  rDPL02c204/out8;
 in10: rDPL02c204/out7;
 in11: rDPL02c204/out6;
 in12: rDPL02c204/out5;

 in13: rDPL02c204/out4;
 in14: rDPL02c204/out3;
 in15: rDPL02c204/out2;
 in16: rDPL02c204/out1;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}

LANDFEE2(rackLAND2c3s4u1) //slot 4
{
 in1: rDPL02c205/out8;
 in2: rDPL02c205/out7;
 in3: rDPL02c205/out6;
 in4: rDPL02c205/out5;
 in5: rDPL02c205/out4;
 in6: rDPL02c205/out3;
 in7: rDPL02c205/out2;
 in8: rDPL02c205/out1;

 in9:  rDPL02c206/out8;
 in10: rDPL02c206/out7;
 in11: rDPL02c206/out6;
 in12: rDPL02c206/out5;

 in13: rDPL02c206/out4;
 in14: rDPL02c206/out3;
 in15: rDPL02c206/out2;
 in16: rDPL02c206/out1;
 
 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}
LANDFEE2(rackLAND2c3s5u1) //slot 5
{
 in1: rDPL02c207/out8;
 in2: rDPL02c207/out7;
 in3: rDPL02c207/out6;
 in4: rDPL02c207/out5;
 in5: rDPL02c207/out4;
 in6: rDPL02c207/out3;
 in7: rDPL02c207/out2;
 in8: rDPL02c207/out1;

 in9:  rDPL02c208/out8;
 in10: rDPL02c208/out7;
 in11: rDPL02c208/out6;
 in12: rDPL02c208/out5;
 in13: rDPL02c208/out4;
 in14: rDPL02c208/out3;
 in15: rDPL02c208/out2;
 in16: rDPL02c208/out1;
 
 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}



LANDFEE2(rackLAND2c3s6u1) //slot 6
{
 in1: rDPL02c209/out8;
 in2: rDPL02c209/out7;
 in3: rDPL02c209/out6;
 in4: rDPL02c209/out5;
 in5: rDPL02c209/out4;
 in6: rDPL02c209/out3;
 in7: rDPL02c209/out2;
 in8: rDPL02c209/out1;

 in9:  rDPL02c210/out8;
 in10: rDPL02c210/out7;
 in11: rDPL02c210/out6;
 in12: rDPL02c210/out5;
 in13: rDPL02c210/out4;
 in14: rDPL02c210/out3;
 in15: rDPL02c210/out2;
 in16: rDPL02c210/out1;
 
 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;

}

LANDFEE2(rackLAND2c3s7u1) //slot 7
{
 in1: rDPL02c211/out8;
 in2: rDPL02c211/out7;
 in3: rDPL02c211/out6;
 in4: rDPL02c211/out5;
 in5: rDPL02c211/out4;
 in6: rDPL02c211/out3;
 in7: rDPL02c211/out2;
 in8: rDPL02c211/out1;

 in9:  rDPL02c212/out8;
 in10: rDPL02c212/out7;
 in11: rDPL02c212/out6;
 in12: rDPL02c212/out5;
 in13: rDPL02c212/out4;
 in14: rDPL02c212/out3;
 in15: rDPL02c212/out2;
 in16: rDPL02c212/out1;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;

}

LANDFEE2(rackLAND2c3s8u1) //slot 8
{
 in1: rDPL02c213/out8;
 in2: rDPL02c213/out7;
 in3: rDPL02c213/out6;
 in4: rDPL02c213/out5;
 in5: rDPL02c213/out4;
 in6: rDPL02c213/out3;
 in7: rDPL02c213/out2;
 in8: rDPL02c213/out1;

// in9:  rNP1c22/out8;
// in10: rNP1c22/out7;
// in11: rNP1c22/out6;
// in12: rNP1c22/out5;
 in13: rDPL02c214/out4;
 in14: rDPL02c214/out3;
 in15: rDPL02c214/out2;
 in16: rDPL02c214/out1;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
  
}
/*
LANDFEE2(rackLAND2c3s9u1) //slot 9
{

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;

}


LANDFEE2(rackLAND2c3s10u1) //slot 10
{
 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;

  ;
}
*/
//                              who is its master, slave, triplex hex adress, who is connected to its upj, downj5 and downj7?
#define TRIPLEX2MASTER_TACQ_QDC2_TACQUILA3(rcs,gtbmaster,gtbslave,addr,upj,downj5,downj7,qserial,tserial,hostname) \
                                                \
  TRIPLEX2MASTER(rcs##u2)                       \
  {                                             \
    SETTING("ADDRESS" => addr);                 \
    SETTING("HOSTNAME" => hostname);            \
    in1_16:  .u1/e1_16;                         \
    out1_16: .u3/in1_16;                        \
                                                \
    /*j6: upj; */                               \
    j5: downj5;                                 \
    j7: downj7;                                 \
  }                                             \
                                                \
  TACQ_QDC2(rcs##u3)                            \
  {                                             \
    /* nonhack : SERIAL(qserial);*/             \
    /* hack */                                  \
    SERIAL(tserial);                            \
    /* endhack */                               \
    in1_16:  .u2/out1_16;                       \
  }                                             \
                                                \
  TACQUILA3(rcs)                                \
  {                                             \
    SERIAL(tserial);                            \
    in1_16: .u1/t1_16;                          \
    /* hack */                                  \
    out1: .s/hackin17;                          \
    hackin17: .s/out1;                          \
    /* endhack */                               \
    gtb_master: gtbmaster;                      \
    gtb_slave:  gtbslave;                       \
  }

#define TRIPLEX2_TACQ_QDC2_TACQUILA3(rcs,gtbmaster,gtbslave,addr,upj,downj5,downj7,qserial,tserial) \
                                                \
  TRIPLEX2(rcs##u2)                             \
  {                                             \
    SETTING("ADDRESS" => addr);                 \
    in1_16:  .u1/e1_16;                         \
    out1_16: .u3/in1_16;                        \
                                                \
    j6: upj;                                    \
    j5: downj5;                                 \
    j7: downj7;                                 \
  }                                             \
                                                \
  TACQ_QDC2(rcs##u3)                            \
  {                                             \
    /* nonhack : SERIAL(qserial);*/             \
    /* hack */                                  \
    SERIAL(tserial);                            \
    /* endhack */                               \
    in1_16:  .u2/out1_16;                       \
  }                                             \
                                                \
  TACQUILA3(rcs)                                \
  {                                             \
    SERIAL(tserial);                            \
    in1_16: .u1/t1_16;                          \
    /* hack */                                  \
    out1: .s/hackin17;                          \
    hackin17: .s/out1;                          \
    /* endhack */                               \
    gtb_master: gtbmaster;                      \
    gtb_slave:  gtbslave;                       \
  }

// not yet up to date, will be used for DPL02, Tacquila Cards only available in September 2014:
/*
TRIPLEX2_TACQ_QDC2_TACQUILA3(rackLAND2c3s10,
rackLAND1c1s13/gtb1,.s9/gtb_master,
"",
,,,
"SAM5_GTB0_TAC010","SAM5_GTB0_TAC10");
//
TRIPLEX2_TACQ_QDC2_TACQUILA3(rackLAND2c3s9,
.s10/gtb_slave,.s8/gtb_master,
"",
,,,
"SAM5_GTB0_TAC09","SAM5_GTB0_TAC09");
*/

// DPL02:
// TT_3 7:
TRIPLEX2_TACQ_QDC2_TACQUILA3(rackLAND2c3s8,
rackLAND1c1s11/gtb1,.s7/gtb_master,
"0x6",
.s6u2/j7,,,
"SAM5_GTB0_TAC08","SAM5_GTB0_TAC08");

// TT_3 6:
TRIPLEX2_TACQ_QDC2_TACQUILA3(rackLAND2c3s7,
.s8/gtb_slave,.s6/gtb_master,
"0x6",
.s6u2/j5,,,
"SAM5_GTB0_TAC07","SAM5_GTB0_TAC07");

//TT_3 5:
TRIPLEX2_TACQ_QDC2_TACQUILA3(rackLAND2c3s6,
.s7/gtb_slave,.s5/gtb_master,
"0x4",
.s3u2/j7,.s7u2/j6,.s8u2/j6,
"SAM5_GTB0_TAC06","SAM5_GTB0_TAC06");

//TT_3 4:
TRIPLEX2_TACQ_QDC2_TACQUILA3(rackLAND2c3s5,
.s6/gtb_slave,.s4/gtb_master,
"0xE",
.s4u2/j5,,,
"SAM5_GTB0_TAC05","SAM5_GTB0_TAC05");

//TT_3 3:
TRIPLEX2_TACQ_QDC2_TACQUILA3(rackLAND2c3s4,
.s5/gtb_slave,.s3/gtb_master,
"0x5",
.s3u2/j5,.s5u2/j6,,
"SAM5_GTB0_TAC04","SAM5_GTB0_TAC04");

//TT_3 2:
TRIPLEX2_TACQ_QDC2_TACQUILA3(rackLAND2c3s3,
.s4/gtb_slave,.s2/gtb_master,
"0x3",
.s2u2/j5,.s4u2/j6,.s6u2/j6,
"SAM5_GTB0_TAC03","SAM5_GTB0_TAC03");

//TT_3 1:
TRIPLEX2MASTER_TACQ_QDC2_TACQUILA3(rackLAND2c3s2,
.s3/gtb_slave,.s1/gtb_master,
"0x8",
,.s3u2/j6,rackNEULAND1c4s8u2/j6,
"SAM5_GTB0_TAC02","SAM5_GTB0_TAC02",
"laniic5");

//DPL04:
//TT_1 7:
TRIPLEX2_TACQ_QDC2_TACQUILA3(rackLAND2c3s1,
.s2/gtb_slave,,
//rackLAND1c1s11/gtb1,,
"0x6",
rackLAND2c2s8u2/j7,,,
"SAM5_GTB0_TAC01","SAM5_GTB0_TAC01");

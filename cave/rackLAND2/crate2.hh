LANDFEE2(rackLAND2c2s1u1) //slot 1
{
 in1: rDPL03c201/out8;
 in2: rDPL03c201/out7;
 in3: rDPL03c201/out6;
 in4: rDPL03c201/out5;
 in5: rDPL03c201/out4;
 in6: rDPL03c201/out3;
 in7: rDPL03c201/out2;
 in8: rDPL03c201/out1;

 in9:  rDPL03c202/out8;
 in10: rDPL03c202/out7;
 in11: rDPL03c202/out6;
 in12: rDPL03c202/out5;
 in13: rDPL03c202/out4;
 in14: rDPL03c202/out3;
 in15: rDPL03c202/out2;
 in16: rDPL03c202/out1;
 
 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;

  ;
}
LANDFEE2(rackLAND2c2s2u1) //slot 2
{
in1: rDPL03c203/out8;
 in2: rDPL03c203/out7;
 in3: rDPL03c203/out6;
 in4: rDPL03c203/out5;
 in5: rDPL03c203/out4;
 in6: rDPL03c203/out3;
 in7: rDPL03c203/out2;
 in8: rDPL03c203/out1;

 in9:  rDPL03c204/out8;
 in10: rDPL03c204/out7;
 in11: rDPL03c204/out6;
 in12: rDPL03c204/out5;

 in13: rDPL03c204/out4;
 in14: rDPL03c204/out3;
 in15: rDPL03c204/out2;
 in16: rDPL03c204/out1;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;

  ;
}
LANDFEE2(rackLAND2c2s3u1) //slot 3
{
 in1: rDPL03c205/out8;
 in2: rDPL03c205/out7;
 in3: rDPL03c205/out6;
 in4: rDPL03c205/out5;
 in5: rDPL03c205/out4;
 in6: rDPL03c205/out3;
 in7: rDPL03c205/out2;
 in8: rDPL03c205/out1;

 in9:  rDPL03c206/out8;
 in10: rDPL03c206/out7;
 in11: rDPL03c206/out6;
 in12: rDPL03c206/out5;

 in13: rDPL03c206/out4;
 in14: rDPL03c206/out3;
 in15: rDPL03c206/out2;
 in16: rDPL03c206/out1;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;

  ;
}
LANDFEE2(rackLAND2c2s4u1) //slot 4
{
 in1: rDPL03c207/out8;
 in2: rDPL03c207/out7;
 in3: rDPL03c207/out6;
 in4: rDPL03c207/out5;
 in5: rDPL03c207/out4;
 in6: rDPL03c207/out3;
 in7: rDPL03c207/out2;
 in8: rDPL03c207/out1;

 in9:  rDPL03c208/out8;
 in10: rDPL03c208/out7;
 in11: rDPL03c208/out6;
 in12: rDPL03c208/out5;
 in13: rDPL03c208/out4;
 in14: rDPL03c208/out3;
 in15: rDPL03c208/out2;
 in16: rDPL03c208/out1;


 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;

  ;
}
LANDFEE2(rackLAND2c2s5u1) //slot 5
{
 in1: rDPL03c209/out8;
 in2: rDPL03c209/out7;
 in3: rDPL03c209/out6;
 in4: rDPL03c209/out5;
 in5: rDPL03c209/out4;
 in6: rDPL03c209/out3;
 in7: rDPL03c209/out2;
 in8: rDPL03c209/out1;

 in9:  rDPL03c210/out8;
 in10: rDPL03c210/out7;
 in11: rDPL03c210/out6;
 in12: rDPL03c210/out5;
 in13: rDPL03c210/out4;
 in14: rDPL03c210/out3;
 in15: rDPL03c210/out2;
 in16: rDPL03c210/out1;

 
 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;

  ;
}

LANDFEE2(rackLAND2c2s6u1) //slot 6
{
 in1: rDPL03c211/out8;
 in2: rDPL03c211/out7;
 in3: rDPL03c211/out6;
 in4: rDPL03c211/out5;
 in5: rDPL03c211/out4;
 in6: rDPL03c211/out3;
 in7: rDPL03c211/out2;
 in8: rDPL03c211/out1;

 in9:  rDPL03c212/out8;
 in10: rDPL03c212/out7;
 in11: rDPL03c212/out6;
 in12: rDPL03c212/out5;
 in13: rDPL03c212/out4;
 in14: rDPL03c212/out3;
 in15: rDPL03c212/out2;
 in16: rDPL03c212/out1;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;

  ;
}
LANDFEE2(rackLAND2c2s7u1) //slot 7
{
 in1: rDPL03c213/out8;
 in2: rDPL03c213/out7;
 in3: rDPL03c213/out6;
 in4: rDPL03c213/out5;
 in5: rDPL03c213/out4;
 in6: rDPL03c213/out3;
 in7: rDPL03c213/out2;
 in8: rDPL03c213/out1;

// in9:  rNP1c22/out8;
// in10: rNP1c22/out7;
// in11: rNP1c22/out6;
// in12: rNP1c22/out5;
 in13: rDPL03c214/out4;
 in14: rDPL03c214/out3;
 in15: rDPL03c214/out2;
 in16: rDPL03c214/out1;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;

  ;
}
LANDFEE2(rackLAND2c2s8u1) //slot 8  //TAMEX card 4
{
 in1: rDPL04c207/out8;
 in2: rDPL04c207/out7;
 in3: rDPL04c207/out6;
 in4: rDPL04c207/out5;
 in5: rDPL04c207/out4;
 in6: rDPL04c207/out3;
 in7: rDPL04c207/out2;
 in8: rDPL04c207/out1;

 in9:  rDPL04c208/out8;
 in10: rDPL04c208/out7;
 in11: rDPL04c208/out6;
 in12: rDPL04c208/out5;
 in13: rDPL04c208/out4;
 in14: rDPL04c208/out3;
 in15: rDPL04c208/out2;
 in16: rDPL04c208/out1;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;

  ;
}
LANDFEE2(rackLAND2c2s9u1) //slot 9 //TAMEX card 5
{
 in1: rDPL04c209/out8;
 in2: rDPL04c209/out7;
 in3: rDPL04c209/out6;
 in4: rDPL04c209/out5;
 in5: rDPL04c209/out4;
 in6: rDPL04c209/out3;
 in7: rDPL04c209/out2;
 in8: rDPL04c209/out1;

 in9:  rDPL04c210/out8;
 in10: rDPL04c210/out7;
 in11: rDPL04c210/out6;
 in12: rDPL04c210/out5;
 in13: rDPL04c210/out4;
 in14: rDPL04c210/out3;
 in15: rDPL04c210/out2;
 in16: rDPL04c210/out1;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;

  ;
}
LANDFEE2(rackLAND2c2s10u1) //slot 10 //TAMEX card 6
{
 in1: rDPL04c211/out8;
 in2: rDPL04c211/out7;
 in3: rDPL04c211/out6;
 in4: rDPL04c211/out5;
 in5: rDPL04c211/out4;
 in6: rDPL04c211/out3;
 in7: rDPL04c211/out2;
 in8: rDPL04c211/out1;

 in9:  rDPL04c212/out8;
 in10: rDPL04c212/out7;
 in11: rDPL04c212/out6;
 in12: rDPL04c212/out5;
 in13: rDPL04c212/out4;
 in14: rDPL04c212/out3;
 in15: rDPL04c212/out2;
 in16: rDPL04c212/out1;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;

  ;
}
//                              who is its master, slave, triplex hex adress, who is connected to its upj, downj5 and downj7?

#define TRIPLEX2_TACQ_QDC2_TACQUILA3(rcs,gtbmaster,gtbslave,addr,upj,downj5,downj7,qserial,tserial) \
                                                \
  TRIPLEX2(rcs##u2)                             \
  {                                             \
    SETTING("ADDRESS" => addr);                 \
    in1_16:  .u1/e1_16;                         \
    out1_16: .u3/in1_16;                        \
                                                \
    j6: upj;                                    \
    j5: downj5;                                 \
    j7: downj7;                                 \
  }                                             \
                                                \
  TACQ_QDC2(rcs##u3)                            \
  {                                             \
    /* nonhack : SERIAL(qserial);*/             \
    /* hack */                                  \
    SERIAL(tserial);                            \
    /* endhack */                               \
    in1_16:  .u2/out1_16;                       \
  }                                             \
                                                \
  TACQUILA3(rcs)                                \
  {                                             \
    SERIAL(tserial);                            \
    in1_16: .u1/t1_16;                          \
    /* hack */                                  \
    out1: .s/hackin17;                          \
    hackin17: .s/out1;                          \
    /* endhack */                               \
    gtb_master: gtbmaster;                      \
    gtb_slave:  gtbslave;                       \
  }


//DPL04:
//TT_1 6:
TRIPLEX2_TACQ_QDC2_TACQUILA3(rackLAND2c2s10,
rackLAND1c1s13/gtb2,.s9/gtb_master,
"0xE",
.c1s10u2/j5,,,
"SAM6_GTB1_TAC10","SAM6_GTB1_TAC10");

//TT_1 5:
TRIPLEX2_TACQ_QDC2_TACQUILA3(rackLAND2c2s9,
.s10/gtb_slave,.s8/gtb_master,
"0x6",
.s8u2/j5,,,
"SAM6_GTB1_TAC09","SAM6_GTB1_TAC09");

//TT_1 4:
TRIPLEX2_TACQ_QDC2_TACQUILA3(rackLAND2c2s8,
.s9/gtb_slave,.s7/gtb_master,
"0x4",
.c1s9u2/j7,.s9u2/j6,.c3s1u2/j6,
"SAM6_GTB1_TAC08","SAM6_GTB1_TAC08");


//DPL03:
//Triplex Card 14:
TRIPLEX2_TACQ_QDC2_TACQUILA3(rackLAND2c2s7,
.s8/gtb_slave,.s6/gtb_master,
"0xD",
.s5u2/j7,,,
"SAM6_GTB1_TAC07","SAM6_GTB1_TAC07");

//Triplex Card 13:
TRIPLEX2_TACQ_QDC2_TACQUILA3(rackLAND2c2s6,
.s7/gtb_slave,.s5/gtb_master,
"0x7",
.s5u2/j5,,,
"SAM6_GTB1_TAC06","SAM6_GTB1_TAC06");

//Triplex Card 12:
TRIPLEX2_TACQ_QDC2_TACQUILA3(rackLAND2c2s5,
.s6/gtb_slave,.s4/gtb_master,
"0x6",
.s1u2/j7,.s6u2/j6,.s7u2/j6,
"SAM6_GTB1_TAC05","SAM6_GTB1_TAC05");

//Triplex Card 11:
TRIPLEX2_TACQ_QDC2_TACQUILA3(rackLAND2c2s4,
.s5/gtb_slave,.s3/gtb_master,
"0xD",
.s2u2/j7,,,
"SAM6_GTB1_TAC04","SAM6_GTB1_TAC04");

//Triplex Card 10:
TRIPLEX2_TACQ_QDC2_TACQUILA3(rackLAND2c2s3,
.s4/gtb_slave,.s2/gtb_master,
"0x7",
.s2u2/j5,,,
"SAM6_GTB1_TAC03","SAM6_GTB1_TAC03");

//Triplex Card 9:
TRIPLEX2_TACQ_QDC2_TACQUILA3(rackLAND2c2s2,
.s3/gtb_slave,.s1/gtb_master,
"0x6",
.s1u2/j5,.s3u2/j6,.s4u2/j6,
"SAM6_GTB1_TAC02","SAM6_GTB1_TAC02");

//Triplex Card 8:
TRIPLEX2_TACQ_QDC2_TACQUILA3(rackLAND2c2s1,
.s2/gtb_slave,,
"0x4",
rackLAND2c1s2u2/j7,.s2u2/j6,.s5u2/j6,
"SAM6_GTB1_TAC01","SAM6_GTB1_TAC01");

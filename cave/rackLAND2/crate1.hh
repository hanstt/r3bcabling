LANDFEE2(rackLAND2c1s1u1) //slot 1
{
 in1: rDPL01c201/out8;
 in2: rDPL01c201/out7;
 in3: rDPL01c201/out6;
 in4: rDPL01c201/out5;
 in5: rDPL01c201/out4;
 in6: rDPL01c201/out3;
 in7: rDPL01c201/out2;
 in8: rDPL01c201/out1;

 in9:  rDPL01c202/out8;
 in10: rDPL01c202/out7;
 in11: rDPL01c202/out6;
 in12: rDPL01c202/out5;
 in13: rDPL01c202/out4;
 in14: rDPL01c202/out3;
 in15: rDPL01c202/out2;
 in16: rDPL01c202/out1;
 
 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;

  ;
}
LANDFEE2(rackLAND2c1s2u1) //slot 2
{
in1: rDPL01c203/out8;
 in2: rDPL01c203/out7;
 in3: rDPL01c203/out6;
 in4: rDPL01c203/out5;
 in5: rDPL01c203/out4;
 in6: rDPL01c203/out3;
 in7: rDPL01c203/out2;
 in8: rDPL01c203/out1;

 in9:  rDPL01c204/out8;
 in10: rDPL01c204/out7;
 in11: rDPL01c204/out6;
 in12: rDPL01c204/out5;

 in13: rDPL01c204/out4;
 in14: rDPL01c204/out3;
 in15: rDPL01c204/out2;
 in16: rDPL01c204/out1;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;

  ;
}
LANDFEE2(rackLAND2c1s3u1) //slot 3
{
 in1: rDPL01c205/out8;
 in2: rDPL01c205/out7;
 in3: rDPL01c205/out6;
 in4: rDPL01c205/out5;
 in5: rDPL01c205/out4;
 in6: rDPL01c205/out3;
 in7: rDPL01c205/out2;
 in8: rDPL01c205/out1;

 in9:  rDPL01c206/out8;
 in10: rDPL01c206/out7;
 in11: rDPL01c206/out6;
 in12: rDPL01c206/out5;

 in13: rDPL01c206/out4;
 in14: rDPL01c206/out3;
 in15: rDPL01c206/out2;
 in16: rDPL01c206/out1;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;

  ;
}
LANDFEE2(rackLAND2c1s4u1) //slot 4
{
 in1: rDPL01c207/out8;
 in2: rDPL01c207/out7;
 in3: rDPL01c207/out6;
 in4: rDPL01c207/out5;
 in5: rDPL01c207/out4;
 in6: rDPL01c207/out3;
 in7: rDPL01c207/out2;
 in8: rDPL01c207/out1;

 in9:  rDPL01c208/out8;
 in10: rDPL01c208/out7;
 in11: rDPL01c208/out6;
 in12: rDPL01c208/out5;
 in13: rDPL01c208/out4;
 in14: rDPL01c208/out3;
 in15: rDPL01c208/out2;
 in16: rDPL01c208/out1;


 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;

  ;
}
LANDFEE2(rackLAND2c1s5u1) //slot 5
{
 in1: rDPL01c209/out8;
 in2: rDPL01c209/out7;
 in3: rDPL01c209/out6;
 in4: rDPL01c209/out5;
 in5: rDPL01c209/out4;
 in6: rDPL01c209/out3;
 in7: rDPL01c209/out2;
 in8: rDPL01c209/out1;

 in9:  rDPL01c210/out8;
 in10: rDPL01c210/out7;
 in11: rDPL01c210/out6;
 in12: rDPL01c210/out5;
 in13: rDPL01c210/out4;
 in14: rDPL01c210/out3;
 in15: rDPL01c210/out2;
 in16: rDPL01c210/out1;

 
 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;

  ;
}

LANDFEE2(rackLAND2c1s6u1) //slot 6
{
 in1: rDPL01c211/out8;
 in2: rDPL01c211/out7;
 in3: rDPL01c211/out6;
 in4: rDPL01c211/out5;
 in5: rDPL01c211/out4;
 in6: rDPL01c211/out3;
 in7: rDPL01c211/out2;
 in8: rDPL01c211/out1;

 in9:  rDPL01c212/out8;
 in10: rDPL01c212/out7;
 in11: rDPL01c212/out6;
 in12: rDPL01c212/out5;
 in13: rDPL01c212/out4;
 in14: rDPL01c212/out3;
 in15: rDPL01c212/out2;
 in16: rDPL01c212/out1;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;

  ;
}
LANDFEE2(rackLAND2c1s7u1) //slot 7
{
 in1: rDPL01c213/out8;
 in2: rDPL01c213/out7;
 in3: rDPL01c213/out6;
 in4: rDPL01c213/out5;
 in5: rDPL01c213/out4;
 in6: rDPL01c213/out3;
 in7: rDPL01c213/out2;
 in8: rDPL01c213/out1;

// in9:  rNP1c22/out8;
// in10: rNP1c22/out7;
// in11: rNP1c22/out6;
// in12: rNP1c22/out5;
 in13: rDPL01c214/out4;
 in14: rDPL01c214/out3;
 in15: rDPL01c214/out2;
 in16: rDPL01c214/out1;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;

  ;
}
LANDFEE2(rackLAND2c1s8u1) //slot 8  //TAMEX card 1
{
 in1: rDPL04c201/out8;
 in2: rDPL04c201/out7;
 in3: rDPL04c201/out6;
 in4: rDPL04c201/out5;
 in5: rDPL04c201/out4;
 in6: rDPL04c201/out3;
 in7: rDPL04c201/out2;
 in8: rDPL04c201/out1;

 in9:  rDPL04c202/out8;
 in10: rDPL04c202/out7;
 in11: rDPL04c202/out6;
 in12: rDPL04c202/out5;
 in13: rDPL04c202/out4;
 in14: rDPL04c202/out3;
 in15: rDPL04c202/out2;
 in16: rDPL04c202/out1;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;

  ;
}
LANDFEE2(rackLAND2c1s9u1) //slot 9 //TAMEX card 2
{
 in1: rDPL04c203/out8;
 in2: rDPL04c203/out7;
 in3: rDPL04c203/out6;
 in4: rDPL04c203/out5;
 in5: rDPL04c203/out4;
 in6: rDPL04c203/out3;
 in7: rDPL04c203/out2;
 in8: rDPL04c203/out1;

 in9:  rDPL04c204/out8;
 in10: rDPL04c204/out7;
 in11: rDPL04c204/out6;
 in12: rDPL04c204/out5;

 in13: rDPL04c204/out4;
 in14: rDPL04c204/out3;
 in15: rDPL04c204/out2;
 in16: rDPL04c204/out1;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;

  ;
}
LANDFEE2(rackLAND2c1s10u1) //slot 10 //TAMEX card 3
{
 in1: rDPL04c205/out8;
 in2: rDPL04c205/out7;
 in3: rDPL04c205/out6;
 in4: rDPL04c205/out5;
 in5: rDPL04c205/out4;
 in6: rDPL04c205/out3;
 in7: rDPL04c205/out2;
 in8: rDPL04c205/out1;

 in9:  rDPL04c206/out8;
 in10: rDPL04c206/out7;
 in11: rDPL04c206/out6;
 in12: rDPL04c206/out5;

 in13: rDPL04c206/out4;
 in14: rDPL04c206/out3;
 in15: rDPL04c206/out2;
 in16: rDPL04c206/out1;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;

}
//                              who is its master, slave, triplex hex adress, who is connected to its upj, downj5 and downj7?
#define TRIPLEX2MASTER_TACQ_QDC2_TACQUILA3(rcs,gtbmaster,gtbslave,addr,upj,downj5,downj7,qserial,tserial,hostname) \
                                                \
  TRIPLEX2MASTER(rcs##u2)                       \
  {                                             \
    SETTING("ADDRESS" => addr);                 \
    SETTING("HOSTNAME" => hostname);            \
    in1_16:  .u1/e1_16;                         \
    out1_16: .u3/in1_16;                        \
                                                \
    /*j6: upj; */                               \
    j5: downj5;                                 \
    j7: downj7;                                 \
  }                                             \
                                                \
  TACQ_QDC2(rcs##u3)                            \
  {                                             \
    /* nonhack : SERIAL(qserial);*/             \
    /* hack */                                  \
    SERIAL(tserial);                            \
    /* endhack */                               \
    in1_16:  .u2/out1_16;                       \
  }                                             \
                                                \
  TACQUILA3(rcs)                                \
  {                                             \
    SERIAL(tserial);                            \
    in1_16: .u1/t1_16;                          \
    /* hack */                                  \
    out1: .s/hackin17;                          \
    hackin17: .s/out1;                          \
    /* endhack */                               \
    gtb_master: gtbmaster;                      \
    gtb_slave:  gtbslave;                       \
  }


#define TRIPLEX2_TACQ_QDC2_TACQUILA3(rcs,gtbmaster,gtbslave,addr,upj,downj5,downj7,qserial,tserial) \
                                                \
  TRIPLEX2(rcs##u2)                             \
  {                                             \
    SETTING("ADDRESS" => addr);                 \
    in1_16:  .u1/e1_16;                         \
    out1_16: .u3/in1_16;                        \
                                                \
    j6: upj;                                    \
    j5: downj5;                                 \
    j7: downj7;                                 \
  }                                             \
                                                \
  TACQ_QDC2(rcs##u3)                            \
  {                                             \
    /* nonhack : SERIAL(qserial);*/             \
    /* hack */                                  \
    SERIAL(tserial);                            \
    /* endhack */                               \
    in1_16:  .u2/out1_16;                       \
  }                                             \
                                                \
  TACQUILA3(rcs)                                \
  {                                             \
    SERIAL(tserial);                            \
    in1_16: .u1/t1_16;                          \
    /* hack */                                  \
    out1: .s/hackin17;                          \
    hackin17: .s/out1;                          \
    /* endhack */                               \
    gtb_master: gtbmaster;                      \
    gtb_slave:  gtbslave;                       \
  }



//DPL04:

//TT_1 3:
TRIPLEX2_TACQ_QDC2_TACQUILA3(rackLAND2c1s10,
rackLAND1c1s13/gtb1,.s9/gtb_master,
"0x5",
.s9u2/j5,.c2s10u2/j6,,
"SAM6_GTB0_TAC10","SAM6_GTB0_TAC10");

//TT_1 2:
TRIPLEX2_TACQ_QDC2_TACQUILA3(rackLAND2c1s9,
.s10/gtb_slave,.s8/gtb_master,
"0x3",
.s8u2/j5,.s10u2/j6,.c2s8u2/j6,
"SAM6_GTB0_TAC09","SAM6_GTB0_TAC09");

//TT_1 1:
TRIPLEX2MASTER_TACQ_QDC2_TACQUILA3(rackLAND2c1s8,
.s9/gtb_slave,.s7/gtb_master,
"0x8",
,.s9u2/j6,rackNEULAND1c1s8u2/j6,
"SAM6_GTB0_TAC08","SAM6_GTB0_TAC08",
"laniic6");


//DPL01:
//Triplex Card 7:
TRIPLEX2_TACQ_QDC2_TACQUILA3(rackLAND2c1s7,
.s8/gtb_slave,.s6/gtb_master,
"0xE",
.s5u2/j7,,,
"SAM6_GTB0_TAC07","SAM6_GTB0_TAC07");

//Triplex Card 6:
TRIPLEX2_TACQ_QDC2_TACQUILA3(rackLAND2c1s6,
.s7/gtb_slave,.s5/gtb_master,
"0xE",
.s5u2/j5,,,
"SAM6_GTB0_TAC06","SAM6_GTB0_TAC06");

//Triplex Card 5:
TRIPLEX2_TACQ_QDC2_TACQUILA3(rackLAND2c1s5,
.s6/gtb_slave,.s4/gtb_master,
"0x9",
.s3u2/j7,.s6u2/j6,.s7u2/j6,
"SAM6_GTB0_TAC05","SAM6_GTB0_TAC05");

//Triplex Card 4:
TRIPLEX2_TACQ_QDC2_TACQUILA3(rackLAND2c1s4,
.s5/gtb_slave,.s3/gtb_master,
"0xE",
.s3u2/j5,,,
"SAM6_GTB0_TAC04","SAM6_GTB0_TAC04");

//Triplex Card 3:
TRIPLEX2_TACQ_QDC2_TACQUILA3(rackLAND2c1s3,
.s4/gtb_slave,.s2/gtb_master,
"0x5",
.s2u2/j5,.s4u2/j6,.s5u2/j6,
"SAM6_GTB0_TAC03","SAM6_GTB0_TAC03");

//Triplex Card 2:
TRIPLEX2_TACQ_QDC2_TACQUILA3(rackLAND2c1s2,
.s3/gtb_slave,.s1/gtb_master,
"0x3",
.s1u2/j5,.s3u2/j6,rackLAND2c2s1u2/j6,
"SAM6_GTB0_TAC02","SAM6_GTB0_TAC02");

//Triplex Card 1:
TRIPLEX2MASTER_TACQ_QDC2_TACQUILA3(rackLAND2c1s1,
.s2/gtb_slave,,
"0x8",
,.s2u2/j6,rackNEULAND1c2s8u2/j6,
"SAM6_GTB0_TAC01","SAM6_GTB0_TAC01",
"laniic3");

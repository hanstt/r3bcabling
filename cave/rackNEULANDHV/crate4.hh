// NeuLAND HV Supply
FOUR_POST_RACK(rackNEULANDHV)
{
  LABEL("NeuLAND HV Supply");
}
CAENSY1527(rackNEULANDHVc4)
{
 LABEL("landhv04");
 SETTING("HOSTNAME"=>"Tjosanhejsan");
}
//
//DPL03:
//

CAENA1733(rackNEULANDHVc4s0)  
{ 
LABEL("slot0");
out0_24: rDPL03c301/in1_25; 
}

CAENA1733(rackNEULANDHVc4s2)  
{ 
LABEL("slot2");
out0_24: rDPL03c302/in1_25; 
}

CAENA1733(rackNEULANDHVc4s4)  
{ 
LABEL("slot4");
out0_24: rDPL03c307/in1_25; 
}
CAENA1733(rackNEULANDHVc4s6)  
{ 
LABEL("slot6");
out0_24: rDPL03c308/in1_25; 
}
//
//DPL01:
//

CAENA1733(rackNEULANDHVc4s8)  
{ 
LABEL("slot8");
out0_24: rDPL01c301/in1_25; 
}


CAENA1733(rackNEULANDHVc4s10)  
{ 
LABEL("slot10");
out0_24: rDPL01c302/in1_25; 
}
CAENA1733(rackNEULANDHVc4s12)  
{ 
LABEL("slot12");
out0_24: rDPL01c307/in1_25; 
}
CAENA1733(rackNEULANDHVc4s14)  
{ 
LABEL("slot14");
out0_24: rDPL01c308/in1_25; 
}

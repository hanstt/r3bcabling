//  HV Supply from Uni FFM
CAENSY1527(rackNEULANDHVc3)
{
 LABEL("landhv06");
 SETTING("HOSTNAME"=>"Tjosanhejsan");
}
//
//DPL04:
//

CAENA1733(rackNEULANDHVc3s0)  
{ 
LABEL("slot0");
out0_24: rDPL04c301/in1_25; 
}

CAENA1733(rackNEULANDHVc3s2)  
{ 
LABEL("slot2");
out0_24: rDPL04c302/in1_25; 
}

CAENA1733(rackNEULANDHVc3s4)  
{ 
LABEL("slot4");
out0_24: rDPL04c307/in1_25; 
}
CAENA1733(rackNEULANDHVc3s6)  
{ 
LABEL("slot6");
out0_24: rDPL04c308/in1_25; 
}


//
//DPL02:
//

CAENA1733(rackNEULANDHVc3s8)  
{ 
LABEL("slot8");
out0_24: rDPL02c301/in1_25; 
}

CAENA1733(rackNEULANDHVc3s10)  
{ 
LABEL("slot10");
out0_24: rDPL02c302/in1_25; 
}

CAENA1733(rackNEULANDHVc3s12)  
{ 
LABEL("slot12");
out0_24: rDPL02c307/in1_25; 
}
CAENA1733(rackNEULANDHVc3s14)  
{ 
LABEL("slot14");
out0_24: rDPL02c308/in1_25; 
}

MODULE(DETECTOR_LOS)
{
  LABEL("LOS detector");
}

UNIT(DETECTOR_LOS_U)
{
  CONNECTOR(signal);
  CONNECTOR(hv);
}

MODULE(DETECTOR_ROLU)
{
  LABEL("ROLU detector");
}

UNIT(DETECTOR_ROLU_U)
{
  CONNECTOR(signal);
  CONNECTOR(hv);
}

MODULE(DETECTOR_LYCCA)
{
  LABEL("LYCCA POS detector");
}

UNIT(DETECTOR_LYCCA_U)
{
  CONNECTOR(signal);
//    CONNECTOR(hv);
}

/* LOS detLOSc1s1  */

DETECTOR_LOS(rLOS1c1s1)
{
  ;
}

// TODO: check output order, should be "ROLU"

DETECTOR_LOS_U(rLOS1c1s1u1) { LABEL("LOS01_01"); signal: rPOS1c1s8/in0;  }
DETECTOR_LOS_U(rLOS1c1s1u2) { LABEL("LOS01_02"); signal: rPOS1c1s8/in1;  }
DETECTOR_LOS_U(rLOS1c1s1u3) { LABEL("LOS01_03"); signal: rPOS1c1s8/in2;  }
DETECTOR_LOS_U(rLOS1c1s1u4) { LABEL("LOS01_04"); signal: rPOS1c1s8/in3;  }

/* POS2 detPOS2c1s1  */

DETECTOR_LYCCA(rLOS2c1s1)
{
  ;
}

DETECTOR_LYCCA_U(rLOS2c1s1u1) { LABEL("LOS02_01"); signal: rPOS1c1s9/in1;  }
DETECTOR_LYCCA_U(rLOS2c1s1u2) { LABEL("LOS02_02"); signal: rPOS1c1s9/in2;  }
DETECTOR_LYCCA_U(rLOS2c1s1u3) { LABEL("LOS02_03"); signal: rPOS1c1s9/in3;  }
DETECTOR_LYCCA_U(rLOS2c1s1u4) { LABEL("LOS02_04"); signal: rPOS1c1s9/in4;  }
DETECTOR_LYCCA_U(rLOS2c1s1u5) { LABEL("LOS02_05"); signal: rPOS1c1s9/in5;  }
DETECTOR_LYCCA_U(rLOS2c1s1u6) { LABEL("LOS02_06"); signal: rPOS1c1s9/in6;  }
DETECTOR_LYCCA_U(rLOS2c1s1u7) { LABEL("LOS02_07"); signal: rPOS1c1s9/in7;  }
DETECTOR_LYCCA_U(rLOS2c1s1u8) { LABEL("LOS02_08"); signal: rPOS1c1s9/in8;  }
DETECTOR_LYCCA_U(rLOS2c1s1u9) { LABEL("LOS02_09"); signal: rPOS1c1s9/in9;  }
DETECTOR_LYCCA_U(rLOS2c1s1u10) { LABEL("LOS02_10"); signal: rPOS1c1s9/in10;  }
DETECTOR_LYCCA_U(rLOS2c1s1u11) { LABEL("LOS02_11"); signal: rPOS1c1s9/in11;  }
DETECTOR_LYCCA_U(rLOS2c1s1u12) { LABEL("LOS02_12"); signal: rPOS1c1s9/in12;  }

/* ROLU1 detROLU1c1s1 */

DETECTOR_ROLU(rROLU1c1s1)
{
  ;
}

// TODO: check output order, should be "ROLU"

DETECTOR_ROLU_U(rROLU1c1s1u1) { LABEL("ROL01_01"); signal: rPOS1c1s8/in4; } //hv: r10c3s11/out4;  }
DETECTOR_ROLU_U(rROLU1c1s1u2) { LABEL("ROL01_02"); signal: rPOS1c1s8/in5; } //hv: r10c3s11/out7;  }
DETECTOR_ROLU_U(rROLU1c1s1u3) { LABEL("ROL01_03"); signal: rPOS1c1s8/in6; } //hv: r10c3s11/out9;  }
DETECTOR_ROLU_U(rROLU1c1s1u4) { LABEL("ROL01_04"); signal: rPOS1c1s8/in7; } //hv: r10c3s11/out10;  }


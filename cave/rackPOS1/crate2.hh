VME_CRATE(rPOS1c2)
{
  SETTING("CRATE_NO" => "7");
}

RIO4(rPOS1c2s1)
{
  SETTING("HOSTNAME" => "r4-10");
}

TRIDI(rPOS1c2s2)
{
in_lemo3: rMASTERc2s11u1/out1;
out_lemo1: rMASTERc1s2/in_lemo2;
}

VFTX2_SMA(rPOS1c2s4)
{
   in1_4: .c1s10u1/outr0_3;
   in5_12: .c1s2u1/outr0_7;
   in13_15: .c1s2u2/outl0_2;
   in16 : .c1s1u1/out7; //TODO:check lf channel
   trig1 : .c1s1u1/out5;
//    TODO: clock
}

VULOM4(rPOS1c2s6)
{
  in_ecl1_8 : .c1s10u1/out_ecl0_7;
  out1: rMASTERc1s12/nim_in1;
  out2: rMASTERc1s12/nim_in12;
}

ENV1(rPOS1c2s7)
{
  nim_out3: rACTARc0s12/in4;
}

MESYTEC_MADC32(rPOS1c2s14) //TODO SETTING "ADDRESS" and "VIRTUAL SLOT"
{
  SETTING("ADDRESS" => "0x00500000");
  nim_gate0 : .c1s1u1/out6;
}

// CAEN_V830(rPOS1c2s16)
// {
//   ;
// }

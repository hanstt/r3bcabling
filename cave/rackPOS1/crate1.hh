//POS crate

NIM_CRATE(rPOS1c1)
{
  ;
}

LF4000(rPOS1c1s1)
{
  ;
}

LF4000_8_U(rPOS1c1s1u1)
{
// TODO   in1 : 
  out1 : .s3u1/in;
  out2 : .s3u2/in;
  out5 : .c2s4/trig1;
  out6 : .c2s14/nim_gate0;
  out7 : .c2s4/in16;
  out8 : rACTARc0s2/in_lemo6;
//   out7 :
}

// LF4000_8_U(rPOS1c1s1u2)
// {
//   ;
// }

LF4000_8_U(rPOS1c1s1u3)
{
out1 : rACTARc0s12/in3;
}

N638(rPOS1c1s2)
{
  ;
}

N638_U(rPOS1c1s2u1)
{
  in_ecl0_7: .s9/t1_8;
  outr0_7: .c2s4/in5_12;
}

N638_U(rPOS1c1s2u2)
{
  in_ecl0_7: .s9/t9_16;
  outl0_2: .c2s4/in13_15;
}

GG8000(rPOS1c1s3)
{
  ;
}

GG8000_U(rPOS1c1s3u1)
{  
  in: .s1u1/out1;
}

GG8000_U(rPOS1c1s3u2)
{  
  in: .s1u1/out2;
}

MESYTEC_CFD16(rPOS1c1s8)
{
  in0 : rLOS1c1s1u1/signal; 
  in1 : rLOS1c1s1u2/signal;
  in2 : rLOS1c1s1u3/signal;
  in3 : rLOS1c1s1u4/signal;
  in4 : rROLU1c1s1u1/signal;
  in5 : rROLU1c1s1u2/signal;
  in6 : rROLU1c1s1u3/signal;
  in7 : rROLU1c1s1u4/signal;
  t0_7 : .s10u1/in_ecl0_7;
//   TODO: analog out to "Aladinhuette"
}

MSCF16(rPOS1c1s9)
{
  in1 : rLOS2c1s1u1/signal; 
  in2 : rLOS2c1s1u2/signal;
  in3 : rLOS2c1s1u3/signal;
  in4 : rLOS2c1s1u4/signal;
  in5 : rLOS2c1s1u5/signal;
  in6 : rLOS2c1s1u6/signal;
  in7 : rLOS2c1s1u7/signal;
  in8 : rLOS2c1s1u8/signal;
  in9 : rLOS2c1s1u9/signal;
  in10 : rLOS2c1s1u10/signal;
  in11 : rLOS2c1s1u11/signal;
  in12 : rLOS2c1s1u12/signal;
  t1_8 : .s2u1/in_ecl0_7;
  t9_16 : .s2u2/in_ecl0_7;
}

N638(rPOS1c1s10)
{
  ;
}

N638_U(rPOS1c1s10u1)
{
  in_ecl0_7 : .s8/t0_7;
  outr0_3 : .c2s4/in1_4;
  out_ecl0_7 : .c2s6/in_ecl1_8;
}

N638_U(rPOS1c1s10u2)
{
  ;
}


LANDFEE2(rackNEULAND1c3s8u1)   // slot 8
{
 in1: rDPL01c215/out8;
 in2: rDPL01c215/out7;
 in3: rDPL01c215/out6;
 in4: rDPL01c215/out5;
 in5: rDPL01c215/out4;
 in6: rDPL01c215/out3;
 in7: rDPL01c215/out2;
 in8: rDPL01c215/out1;
 in9:  rDPL01c216/out8;
 in10: rDPL01c216/out7;
 in11: rDPL01c216/out6;
 in12: rDPL01c216/out5;
 in13: rDPL01c216/out4;
 in14: rDPL01c216/out3;
 in15: rDPL01c216/out2;
 in16: rDPL01c216/out1;

  //gtb_out1_16: r54c1s15/gtb;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}

LANDFEE2(rackNEULAND1c3s9u1)   // slot 9
{
 in1: rDPL01c217/out8;
 in2: rDPL01c217/out7;
 in3: rDPL01c217/out6;
 in4: rDPL01c217/out5;
 in5: rDPL01c217/out4;
 in6: rDPL01c217/out3;
 in7: rDPL01c217/out2;
 in8: rDPL01c217/out1;
 in9:  rDPL01c218/out8;
 in10: rDPL01c218/out7;
 in11: rDPL01c218/out6;
 in12: rDPL01c218/out5;
 in13: rDPL01c218/out4;
 in14: rDPL01c218/out3;
 in15: rDPL01c218/out2;
 in16: rDPL01c218/out1;

  //gtb_out1_16: r54c1s15/gtb;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}

LANDFEE2(rackNEULAND1c3s10u1)   // slot 10
{
 in1: rDPL01c219/out8;
 in2: rDPL01c219/out7;
 in3: rDPL01c219/out6;
 in4: rDPL01c219/out5;
 in5: rDPL01c219/out4;
 in6: rDPL01c219/out3;
 in7: rDPL01c219/out2;
 in8: rDPL01c219/out1;
 in9:  rDPL01c220/out8;
 in10: rDPL01c220/out7;
 in11: rDPL01c220/out6;
 in12: rDPL01c220/out5;
 in13: rDPL01c220/out4;
 in14: rDPL01c220/out3;
 in15: rDPL01c220/out2;
 in16: rDPL01c220/out1;

  //gtb_out1_16: r54c1s15/gtb;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}

LANDFEE2(rackNEULAND1c3s11u1)   // slot 11
{
 in1: rDPL01c221/out8;
 in2: rDPL01c221/out7;
 in3: rDPL01c221/out6;
 in4: rDPL01c221/out5;
 in5: rDPL01c221/out4;
 in6: rDPL01c221/out3;
 in7: rDPL01c221/out2;
 in8: rDPL01c221/out1;

 in9:  rDPL01c222/out8;
 in10: rDPL01c222/out7;
 in11: rDPL01c222/out6;
 in12: rDPL01c222/out5;
 in13: rDPL01c222/out4;
 in14: rDPL01c222/out3;
 in15: rDPL01c222/out2;
 in16: rDPL01c222/out1;

  //gtb_out1_16: r54c1s15/gtb;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}

LANDFEE2(rackNEULAND1c3s12u1)   // slot 12
{
 in1: rDPL01c223/out8;
 in2: rDPL01c223/out7;
 in3: rDPL01c223/out6;
 in4: rDPL01c223/out5;
 in5: rDPL01c223/out4;
 in6: rDPL01c223/out3;
 in7: rDPL01c223/out2;
 in8: rDPL01c223/out1;

 in9:  rDPL01c224/out8;
 in10: rDPL01c224/out7;
 in11: rDPL01c224/out6;
 in12: rDPL01c224/out5;
 in13: rDPL01c224/out4;
 in14: rDPL01c224/out3;
 in15: rDPL01c224/out2;
 in16: rDPL01c224/out1;

  //gtb_out1_16: r54c1s15/gtb;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}

LANDFEE2(rackNEULAND1c3s13u1)   // slot 13
{
 in1: rDPL01c225/out8;
 in2: rDPL01c225/out7;
 in3: rDPL01c225/out6;
 in4: rDPL01c225/out5;
 in5: rDPL01c225/out4;
 in6: rDPL01c225/out3;
 in7: rDPL01c225/out2;
 in8: rDPL01c225/out1;
/*
 in9:  rDPL01c226/out8;
 in10: rDPL01c226/out7;
 in11: rDPL01c226/out6;
 in12: rDPL01c226/out5;
*/
 in13: rDPL01c226/out4;
 in14: rDPL01c226/out3;
 in15: rDPL01c226/out2;
 in16: rDPL01c226/out1;

  //gtb_out1_16: r54c1s15/gtb;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}
//                              who is its master, slave, triplex hex adress, who is connected to its upj, downj5 and downj7?

#define TRIPLEX2_TACQ_QDC2_TACQUILA3(rcs,gtbmaster,gtbslave,addr,upj,downj5,downj7,qserial,tserial) \
                                                \
  TRIPLEX2(rcs##u2)                             \
  {                                             \
    SETTING("ADDRESS" => addr);                 \
    in1_16:  .u1/e1_16;                         \
    out1_16: .u3/in1_16;                        \
                                                \
    j6: upj;                                    \
    j5: downj5;                                 \
    j7: downj7;                                 \
  }                                             \
                                                \
  TACQ_QDC2(rcs##u3)                            \
  {                                             \
    /* nonhack : SERIAL(qserial);*/             \
    /* hack */                                  \
    SERIAL(tserial);                            \
    /* endhack */                               \
    in1_16:  .u2/out1_16;                       \
  }                                             \
                                                \
  TACQUILA3(rcs)                                \
  {                                             \
    SERIAL(tserial);                            \
    in1_16: .u1/t1_16;                          \
    /* hack */                                  \
    out1: .s/hackin17;                          \
    hackin17: .s/out1;                          \
    /* endhack */                               \
    gtb_master: gtbmaster;                      \
    gtb_slave:  gtbslave;                       \
 }

 //TT_2 Triplex Card Nr. 21:
 TRIPLEX2_TACQ_QDC2_TACQUILA3(rackNEULAND1c3s8,
 rackLAND1c1s15/gtb2,.s9/gtb_master,
 "0x1",
 .c2s8u2/j7,.s9u2/j6,.s11u2/j6,
 "SAM7_GTB1_TAC09","SAM7_GTB1_TAC09"); 

 //Triplex Card Nr. 22:
 TRIPLEX2_TACQ_QDC2_TACQUILA3(rackNEULAND1c3s9,
 .s8/gtb_slave,.s10/gtb_master,
 "0x6",
 .s8u2/j5,.s10u2/j6,,
 "SAM7_GTB1_TAC08","SAM7_GTB1_TAC08");

 //Triplex Card Nr.23:
 TRIPLEX2_TACQ_QDC2_TACQUILA3(rackNEULAND1c3s10,
 .s9/gtb_slave,.c2s13/gtb_master,
 "0x7",
 .s9u2/j5,,,
 "SAM7_GTB1_TAC07","SAM7_GTB1_TAC07");

 //Triplex Card Nr. 24:
 TRIPLEX2_TACQ_QDC2_TACQUILA3(rackNEULAND1c3s11,
 .c4s13/gtb_slave,.s12/gtb_master,
 "0x7",
 .s8u2/j7,.s12u2/j6,.s13u2/j6,
 "SAM7_GTB0_TAC03","SAM7_GTB0_TAC03");

 //Triplex Card Nr. 25:
 TRIPLEX2_TACQ_QDC2_TACQUILA3(rackNEULAND1c3s12,
 .s11/gtb_slave,.s13/gtb_master,
 "0x6",
 .s11u2/j5,,,
 "SAM7_GTB0_TAC02","SAM7_GTB0_TAC02");

 //Triplex Card Nr. 26:
 TRIPLEX2_TACQ_QDC2_TACQUILA3(rackNEULAND1c3s13,
 .s12/gtb_slave,,
 "0xE",
 .s11u2/j7,,,
 "SAM7_GTB0_TAC01","SAM7_GTB0_TAC01");

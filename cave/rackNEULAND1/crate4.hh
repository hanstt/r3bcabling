LANDFEE2(rackNEULAND1c4s8u1)   // slot 8
{
 in1: rDPL02c215/out8;
 in2: rDPL02c215/out7;
 in3: rDPL02c215/out6;
 in4: rDPL02c215/out5;
 in5: rDPL02c215/out4;
 in6: rDPL02c215/out3;
 in7: rDPL02c215/out2;
 in8: rDPL02c215/out1;
 in9:  rDPL02c216/out8;
 in10: rDPL02c216/out7;
 in11: rDPL02c216/out6;
 in12: rDPL02c216/out5;
 in13: rDPL02c216/out4;
 in14: rDPL02c216/out3;
 in15: rDPL02c216/out2;
 in16: rDPL02c216/out1;

  //gtb_out1_16: r54c1s15/gtb;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}

LANDFEE2(rackNEULAND1c4s9u1)   // slot 9
{
 in1: rDPL02c217/out8;
 in2: rDPL02c217/out7;
 in3: rDPL02c217/out6;
 in4: rDPL02c217/out5;
 in5: rDPL02c217/out4;
 in6: rDPL02c217/out3;
 in7: rDPL02c217/out2;
 in8: rDPL02c217/out1;
 in9:  rDPL02c218/out8;
 in10: rDPL02c218/out7;
 in11: rDPL02c218/out6;
 in12: rDPL02c218/out5;
 in13: rDPL02c218/out4;
 in14: rDPL02c218/out3;
 in15: rDPL02c218/out2;
 in16: rDPL02c218/out1;

  //gtb_out1_16: r54c1s15/gtb;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}

LANDFEE2(rackNEULAND1c4s10u1)   // slot 10
{
 in1: rDPL02c219/out8;
 in2: rDPL02c219/out7;
 in3: rDPL02c219/out6;
 in4: rDPL02c219/out5;
 in5: rDPL02c219/out4;
 in6: rDPL02c219/out3;
 in7: rDPL02c219/out2;
 in8: rDPL02c219/out1;
 in9:  rDPL02c220/out8;
 in10: rDPL02c220/out7;
 in11: rDPL02c220/out6;
 in12: rDPL02c220/out5;
 in13: rDPL02c220/out4;
 in14: rDPL02c220/out3;
 in15: rDPL02c220/out2;
 in16: rDPL02c220/out1;

  //gtb_out1_16: r54c1s15/gtb;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}

LANDFEE2(rackNEULAND1c4s11u1)   // slot 11
{
 in1: rDPL02c221/out8;
 in2: rDPL02c221/out7;
 in3: rDPL02c221/out6;
 in4: rDPL02c221/out5;
 in5: rDPL02c221/out4;
 in6: rDPL02c221/out3;
 in7: rDPL02c221/out2;
 in8: rDPL02c221/out1;

 in9:  rDPL02c222/out8;
 in10: rDPL02c222/out7;
 in11: rDPL02c222/out6;
 in12: rDPL02c222/out5;
 in13: rDPL02c222/out4;
 in14: rDPL02c222/out3;
 in15: rDPL02c222/out2;
 in16: rDPL02c222/out1;

  //gtb_out1_16: r54c1s15/gtb;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}

LANDFEE2(rackNEULAND1c4s12u1)   // slot 12
{
 in1: rDPL02c223/out8;
 in2: rDPL02c223/out7;
 in3: rDPL02c223/out6;
 in4: rDPL02c223/out5;
 in5: rDPL02c223/out4;
 in6: rDPL02c223/out3;
 in7: rDPL02c223/out2;
 in8: rDPL02c223/out1;

 in9:  rDPL02c224/out8;
 in10: rDPL02c224/out7;
 in11: rDPL02c224/out6;
 in12: rDPL02c224/out5;
 in13: rDPL02c224/out4;
 in14: rDPL02c224/out3;
 in15: rDPL02c224/out2;
 in16: rDPL02c224/out1;

  //gtb_out1_16: r54c1s15/gtb;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}

LANDFEE2(rackNEULAND1c4s13u1)   // slot 13
{
 in1: rDPL02c225/out8;
 in2: rDPL02c225/out7;
 in3: rDPL02c225/out6;
 in4: rDPL02c225/out5;
 in5: rDPL02c225/out4;
 in6: rDPL02c225/out3;
 in7: rDPL02c225/out2;
 in8: rDPL02c225/out1;
/*
 in9:  rDPL02c226/out8;
 in10: rDPL02c226/out7;
 in11: rDPL02c226/out6;
 in12: rDPL02c226/out5;
*/
 in13: rDPL02c226/out4;
 in14: rDPL02c226/out3;
 in15: rDPL02c226/out2;
 in16: rDPL02c226/out1;

  //gtb_out1_16: r54c1s15/gtb;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}
//                              who is its master, slave, triplex hex adress, who is connected to its upj, downj5 and downj7?

#define TRIPLEX2MASTER_TACQ_QDC2_TACQUILA3(rcs,gtbmaster,gtbslave,addr,upj,downj5,downj7,qserial,tserial,hostname) \
                                                \
  TRIPLEX2MASTER(rcs##u2)                       \
  {                                             \
    SETTING("ADDRESS" => addr);                 \
    SETTING("HOSTNAME" => hostname);            \
    in1_16:  .u1/e1_16;                         \
    out1_16: .u3/in1_16;                        \
                                                \
    /*j6: upj; */                               \
    j5: downj5;                                 \
    j7: downj7;                                 \
  }                                             \
                                                \
  TACQ_QDC2(rcs##u3)                            \
  {                                             \
    /* nonhack : SERIAL(qserial);*/             \
    /* hack */                                  \
    SERIAL(tserial);                            \
    /* endhack */                               \
    in1_16:  .u2/out1_16;                       \
  }                                             \
                                                \
  TACQUILA3(rcs)                                \
  {                                             \
    SERIAL(tserial);                            \
    in1_16: .u1/t1_16;                          \
    /* hack */                                  \
    out1: .s/hackin17;                          \
    hackin17: .s/out1;                          \
    /* endhack */                               \
    gtb_master: gtbmaster;                      \
    gtb_slave:  gtbslave;                       \
  }


#define TRIPLEX2_TACQ_QDC2_TACQUILA3(rcs,gtbmaster,gtbslave,addr,upj,downj5,downj7,qserial,tserial) \
                                                \
  TRIPLEX2(rcs##u2)                             \
  {                                             \
    SETTING("ADDRESS" => addr);                 \
    in1_16:  .u1/e1_16;                         \
    out1_16: .u3/in1_16;                        \
                                                \
    j6: upj;                                    \
    j5: downj5;                                 \
    j7: downj7;                                 \
  }                                             \
                                                \
  TACQ_QDC2(rcs##u3)                            \
  {                                             \
    /* nonhack : SERIAL(qserial);*/             \
    /* hack */                                  \
    SERIAL(tserial);                            \
    /* endhack */                               \
    in1_16:  .u2/out1_16;                       \
  }                                             \
                                                \
  TACQUILA3(rcs)                                \
  {                                             \
    SERIAL(tserial);                            \
    in1_16: .u1/t1_16;                          \
    /* hack */                                  \
    out1: .s/hackin17;                          \
    hackin17: .s/out1;                          \
    /* endhack */                               \
    gtb_master: gtbmaster;                      \
    gtb_slave:  gtbslave;                       \
 }

 //Triplex Card Nr. 21:
 TRIPLEX2_TACQ_QDC2_TACQUILA3(rackNEULAND1c4s8,
 rackLAND1c1s15/gtb1,.s9/gtb_master,
 "0x3",
 rackLAND2c3s2u2/j7,.s11u2/j6,.s9u2/j6,
 "SAM7_GTB0_TAC09","SAM7_GTB0_TAC09"); 

//  when tacquilas in LAND2crate3 are missing
//  TRIPLEX2MASTER_TACQ_QDC2_TACQUILA3(rackNEULAND1c4s8,
//  rackLAND1c1s15/gtb1,.s9/gtb_master,
//  "0x8",
//  ,.s11u2/j6,.s9u2/j6,
//  "SAM7_GTB0_TAC09","SAM7_GTB0_TAC09",
//  "laniic5");

 //Triplex Card Nr. 22:
 TRIPLEX2_TACQ_QDC2_TACQUILA3(rackNEULAND1c4s9,
 .s8/gtb_slave,.s10/gtb_master,
 "0x1",
 //"0x3",
 .s8u2/j7,,.s10u2/j6,
 "SAM7_GTB0_TAC08","SAM7_GTB0_TAC08");

 //Triplex Card Nr.23:
 TRIPLEX2_TACQ_QDC2_TACQUILA3(rackNEULAND1c4s10,
 .s9/gtb_slave,.s11/gtb_master,
 "0x7",
 //"0x1",
 .s9u2/j7,,,
 "SAM7_GTB0_TAC07","SAM7_GTB0_TAC07");

 //Triplex Card Nr. 24:
 TRIPLEX2_TACQ_QDC2_TACQUILA3(rackNEULAND1c4s11,
 .s10/gtb_slave,.s12/gtb_master,
 "0x5",
 //"0x3",
 .s8u2/j5,.s12u2/j6,.s13u2/j6,
 "SAM7_GTB0_TAC06","SAM7_GTB0_TAC06");

 //Triplex Card Nr. 25:
 TRIPLEX2_TACQ_QDC2_TACQUILA3(rackNEULAND1c4s12,
 .s11/gtb_slave,.s13/gtb_master,
 "0xE",
 //"0x5",
 .s11u2/j5,,,
 "SAM7_GTB0_TAC05","SAM7_GTB0_TAC05");

 //Triplex Card Nr. 26:
 TRIPLEX2_TACQ_QDC2_TACQUILA3(rackNEULAND1c4s13,
 .s12/gtb_slave,.c3s11/gtb_master,
 "0x1",
 //"0x4",
 .s11u2/j7,,,
 "SAM7_GTB0_TAC04","SAM7_GTB0_TAC04");

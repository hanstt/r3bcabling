NIM_CRATE(rMASTERc2)
{
  ;
}

LF4000(rMASTERc2s1)
{
  ;
}

// TCAL not used for Apr 2014
LF4000_8_U(rMASTERc2s1u1)
{
  ;
//   in1 : .c3s21/start;
//   out1 : .s5/in1;
}

LF4000_8_U(rMASTERc2s1u2)
{
  ;
//   in8 : .c3s21/stop;
}

LF4000_8_U(rMASTERc2s1u4)
{
  out3:	rACTARc0s2/in_lemo7; //ok
}

LF4000(rMASTERc2s2)
{
  ;
}

LF4000_4_U(rMASTERc2s2u1)
{
  in1 : .c1s20/nim_out15;
  out1 : .s4u1/start;
}

LF4000_4_U(rMASTERc2s2u2)
{
  in1 : .s4u1/end_marker;
  out1 : .s3u1/start;
  out2 : .s3u2/reset;
  out3 : .c1s3/in1;
}

LF4000_4_U(rMASTERc2s2u3)
{
  in1 : .c1s20/nim_out16;
  out1 : .s4u2/start;
}

LF4000_4_U(rMASTERc2s2u4)
{
  in1 : .s4u2/end_marker;
  out1 : .s3u2/start;
  out2 : .s3u1/reset;
  out3 : .c1s3/in2;
}

CAENN93B(rMASTERc2s3)
{
  ;
}

CAENN93B_U(rMASTERc2s3u1)
{
  start : .s2u2/out1;
  reset : .s2u4/out2;
}

CAENN93B_U(rMASTERc2s3u2)
{
  start : .s2u4/out1;
  reset : .s2u2/out2;
}

CAENN93B(rMASTERc2s4)
{
  ;
}

CAENN93B_U(rMASTERc2s4u1)
{
  start : .s2u1/out1;
  end_marker : .s2u2/in1;
}

CAENN93B_U(rMASTERc2s4u2)
{
  start : .s2u3/out1;
  end_marker : .s2u4/in1;
}
/*
DP1600(rMASTERc2s5)
{
  ;
}*/

LECROY429A(rMASTERc2s6)
{
  ;
}


LECROY429A_4_U(rMASTERc2s6u1)
{
  in1 : .c1s7/out_lemo1;
//   out1 : .c1s7/in_lemo2;
}


LECROY429A_4_U(rMASTERc2s6u2)
{
  in1 : .c1s3/out2;
  out1 : .c1s7/in_lemo2;
}


LECROY429A_4_U(rMASTERc2s6u3)
{
  ;
}


LECROY429A_4_U(rMASTERc2s6u4)
{
  ;
}

CL1001(rMASTERc2s10)
{
  ;
}

LF4000(rMASTERc2s11)
{
  ;
}

LF4000_8_U(rMASTERc2s11u1)
{
  in1 : .c1s2/out_lemo1;
  out1 : rPOS1c2s2/in_lemo3;
  out2 : rACTARc0s2/in_lemo8;
}

LF4000_8_U(rMASTERc2s11u2)
{
  ;
}

LF4000(rMASTERc2s12)
{
  ;
}

LF4000_16_U(rMASTERc2s12u1)
{
  in1 : .c1s3/out1;
  in3 : .c1s5/nim_out3;
  out1 : rACTARc0s2/in_lemo5;
  out2 : rACTARc0s12/in5;
  out6 : rackLAND1c2s5u2/in1; //Alina 09.04.14
}

// LF4000_4_U(rMASTERc2s21u2)
// {
//   out2: r24c0s02/out_lemo7;
// }

LF4000_4_U(rMASTERc2s21u3)
{
	;
}

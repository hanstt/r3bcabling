// TODO: This was based on a one person eye-witness testimony for testing...
//       I.e. remove and write again.

VME_CRATE(rMASTERc1)
{
  ;
}

RIO3(rMASTERc1s1)
{
  SETTING("HOSTNAME" => "r3-30");
}

TRIDI(rMASTERc1s2)
{
	 

  in_ecl1_8 : .s3/io_ecl1_8;
  in_lemo2 : rPOS1c2s2/out_lemo1;
  out_lemo1 : .c2s11u1/in1;
  out_lemo8 : .s5/nim_in12;
}

VULOM4(rMASTERc1s3)
{
  in_ecl1_16: .s12/out_ecl1_16;
  io_ecl9_16 : .s5/out_ecl9_16; 
  io_ecl1_8 : .s2/in_ecl1_8;
  out_ecl1_8 : .s5/in_ecl1_8;
  in1 : .c2s2u2/out3;
  in2 : .c2s2u4/out3;
  out1 : .c2s12u1/in1;
  out2 : .c2s6u2/in1;
}

ENV3(rMASTERc1s5)
{  
  out_ecl9_16 : .s3/io_ecl9_16;
  in_ecl1_8 : .s3/out_ecl1_8;
  nim_out3 : .c2s12u1/in3;
  nim_in12 : .s2/out_lemo8;
  nim_out12 : rACTARc0s12/in6;
}

TRIDI(rMASTERc1s7)
{
  in_lemo2 : .c2s6u2/out1;
  out_lemo1 : .c2s6u1/in1;
}

ENV3(rMASTERc1s12)
{
  out_ecl1_16: .s3/in_ecl1_16;
  nim_in1: rPOS1c2s6/out1;
  nim_in12: rPOS1c2s6/out2;
}

ENV3(rMASTERc1s20)
{
  nim_out15 : .c2s2u1/in1;
  nim_out16 : .c2s2u3/in1;
}

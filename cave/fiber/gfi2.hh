#include "gfi.hh" //declaration of DETECTOR_GFI, DETECTOR_GFI_PSPM,DETECTOR_GFI_PSPM_U,VACUUM_FLANGE36

/* GFI in ALADIN (example on GFI1)
 * Naming scheme for GFI1 detector:
 *
 * DETECTOR_GFI ('the detector', rGFI1c1)
 * DETECTOR_GFI_PM ('the timing PM', rGFIc1s2)
 * DETECTOR_GFI_PSPM ('the position sensitive PM',rGFIc1s1)
 * DETECTOR_GFI_PSPM_U ('one channel from   PSPM',rGFIc1s1u1)
 *
 * Each cable that goes to readout (TacQuila @ s438) has on GFI end a shorter cable
 * that goes from lemo to 8-fold connector.  This connection is a JOINER_8.
 * At the other end is another JOINER_8 before it goes to FEE of TacQuilas.
 *
 * //individual description
 * rGFI1c1 is the detector GFI1.
 * rGFI1c1s1 is the PSPM of GFI1.
 * rGFI1c2 is the vacuum-flange on GFI1.
 * rGFI2c1 is the detector GFI2.
 * rGFI2c1s1 is the PSPM of GFI2.
 * rGFI2c2 is the vacuum-flange on GFI2.
 *
 * //common for GFI1 & GFI2
 * rFIBERc1  is the crate (below the flange) (hosts AMPLIFYERs for both GFI1 & GFI2 @ s438)
 * rFIBERc1sX is the amplifier in slot X of the crate
 * rFIBERc1x are the cable joiners close to the rack (9 cables for 1+2 GFIs => [c11-c19])
 * rFIBERc2x are the cable joiners close to the tacquilas (9 cables for 1+2 GFIs => [c21-c29])
 */

// GFI 2      /*************************************************/

DETECTOR_GFI(rGFI2c1)
{
  ;
}

DETECTOR_GFI_PSPM(rGFI2c1s1)
{
  ;
}
// x
DETECTOR_GFI_PSPM_U(rGFI2c1s1u1)  { LABEL("GFI02_01"); signal:  rGFI2c2/x_in1; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u2)  { LABEL("GFI02_02"); signal:  rGFI2c2/x_in2; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u3)  { LABEL("GFI02_03"); signal:  rGFI2c2/x_in3; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u4)  { LABEL("GFI02_04"); signal:  rGFI2c2/x_in4; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u5)  { LABEL("GFI02_05"); signal:  rGFI2c2/x_in5; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u6)  { LABEL("GFI02_06"); signal:  rGFI2c2/x_in6; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u7)  { LABEL("GFI02_07"); signal:  rGFI2c2/x_in7; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u8)  { LABEL("GFI02_08"); signal:  rGFI2c2/x_in8; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u9)  { LABEL("GFI02_09"); signal:  rGFI2c2/x_in9; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u10) { LABEL("GFI02_10"); signal:  rGFI2c2/x_in10; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u11) { LABEL("GFI02_11"); signal:  rGFI2c2/x_in11; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u12) { LABEL("GFI02_12"); signal:  rGFI2c2/x_in12; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u13) { LABEL("GFI02_13"); signal:  rGFI2c2/x_in13; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u14) { LABEL("GFI02_14"); signal:  rGFI2c2/x_in14; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u15) { LABEL("GFI02_15"); signal:  rGFI2c2/x_in15; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u16) { LABEL("GFI02_16"); signal:  rGFI2c2/x_in16; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u17) { LABEL("GFI02_17"); signal:  rGFI2c2/x_in17;}
DETECTOR_GFI_PSPM_U(rGFI2c1s1u18) { LABEL("GFI02_18"); signal:  rGFI2c2/x_in18;}
// y 
DETECTOR_GFI_PSPM_U(rGFI2c1s1u19) { LABEL("GFI02_19"); signal:  rGFI2c2/y_in1;  }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u20) { LABEL("GFI02_20"); signal:  rGFI2c2/y_in2;  }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u21) { LABEL("GFI02_21"); signal:  rGFI2c2/y_in3;  }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u22) { LABEL("GFI02_22"); signal:  rGFI2c2/y_in4;  }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u23) { LABEL("GFI02_23"); signal:  rGFI2c2/y_in5;  }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u24) { LABEL("GFI02_24"); signal:  rGFI2c2/y_in6;  }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u25) { LABEL("GFI02_25"); signal:  rGFI2c2/y_in7;  }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u26) { LABEL("GFI02_26"); signal:  rGFI2c2/y_in8;  }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u27) { LABEL("GFI02_27"); signal:  rGFI2c2/y_in9;  }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u28) { LABEL("GFI02_28"); signal:  rGFI2c2/y_in10; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u29) { LABEL("GFI02_29"); signal:  rGFI2c2/y_in11; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u30) { LABEL("GFI02_30"); signal:  rGFI2c2/y_in12; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u31) { LABEL("GFI02_31"); signal:  rGFI2c2/y_in13; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u32) { LABEL("GFI02_32"); signal:  rGFI2c2/y_in14; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u33) { LABEL("GFI02_33"); signal:  rGFI2c2/y_in15; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u34) { LABEL("GFI02_34"); signal:  rGFI2c2/y_in16; }

VACUUM_FLANGE36(rGFI2c2)
{
 x_in1_18: rGFI2c1s1u1_18/signal;
 y_in1_16: rGFI2c1s1u19_34/signal;
 
 x_out1_10:   rFIBERc1s3/in1_10; //Amplif. ch11 broken
 x_out11_18:  rFIBERc1s3/in12_19; //Amplif. ch20 broken
 y_out1_12:   rFIBERc1s3/in21_32;

 y_out13_16:  rFIBERc1s4/in1_4;
}


 // done here. cables from PSPM -> flange -> amplifiers.


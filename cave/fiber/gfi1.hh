#include "gfi.hh" //declaration of DETECTOR_GFI, DETECTOR_GFI_PSPM,DETECTOR_GFI_PSPM_U,VACUUM_FLANGE36

/* GFI in ALADIN (example on GFI1)
 * Naming scheme for GFI1 detector:
 *
 * DETECTOR_GFI ('the detector', rGFI1c1)
 * DETECTOR_GFI_PM ('the timing PM', rGFIc1s2)
 * DETECTOR_GFI_PSPM ('the position sensitive PM',rGFIc1s1)
 * DETECTOR_GFI_PSPM_U ('one channel from   PSPM',rGFIc1s1u1)
 *
 * Each cable that goes to readout (TacQuila @ s438) has on GFI end a shorter cable
 * that goes from lemo to 8-fold connector.  This connection is a JOINER_8.
 * At the other end is another JOINER_8 before it goes to FEE of TacQuilas.
 *
 * //individual description
 * rGFI1c1 is the detector GFI1.
 * rGFI1c1s1 is the PSPM of GFI1.
 * rGFI1c2 is the vacuum-flange on GFI1.
 * rGFI2c1 is the detector GFI2.
 * rGFI2c1s1 is the PSPM of GFI2.
 * rGFI2c2 is the vacuum-flange on GFI2.
 *
 * //common for GFI1 & GFI2
 * rFIBERc1  is the crate (below the flange) (hosts AMPLIFYERs for both GFI1 & GFI2 @ s438)
 * rFIBERc1sX is the amplifier in slot X of the crate
 * rFIBERc1x are the cable joiners close to the rack (9 cables for 1+2 GFIs => [c11-c19])
 * rFIBERc2x are the cable joiners close to the tacquilas (9 cables for 1+2 GFIs => [c21-c29])
 */

// GFI 1 /******************************************************/

DETECTOR_GFI(rGFI1c1) //declared in gfi.hh
{
  ;
}

DETECTOR_GFI_PSPM(rGFI1c1s1) //declared in gfi.hh
{
  ;
}

DETECTOR_GFI_PSPM_U(rGFI1c1s1u1)  { LABEL("GFI01_01"); signal: rGFI1c2/x_in1; } //declared in gfi.hh
DETECTOR_GFI_PSPM_U(rGFI1c1s1u2)  { LABEL("GFI01_02"); signal: rGFI1c2/x_in2; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u3)  { LABEL("GFI01_03"); signal: rGFI1c2/x_in3; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u4)  { LABEL("GFI01_04"); signal: rGFI1c2/x_in4; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u5)  { LABEL("GFI01_05"); signal: rGFI1c2/x_in5; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u6)  { LABEL("GFI01_06"); signal: rGFI1c2/x_in6; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u7)  { LABEL("GFI01_07"); signal: rGFI1c2/x_in7; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u8)  { LABEL("GFI01_08"); signal: rGFI1c2/x_in8; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u9)  { LABEL("GFI01_09"); signal: rGFI1c2/x_in9; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u10) { LABEL("GFI01_10"); signal: rGFI1c2/x_in10; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u11) { LABEL("GFI01_11"); signal: rGFI1c2/x_in11; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u12) { LABEL("GFI01_12"); signal: rGFI1c2/x_in12; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u13) { LABEL("GFI01_13"); signal: rGFI1c2/x_in13; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u14) { LABEL("GFI01_14"); signal: rGFI1c2/x_in14; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u15) { LABEL("GFI01_15"); signal: rGFI1c2/x_in15; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u16) { LABEL("GFI01_16"); signal: rGFI1c2/x_in16; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u17) { LABEL("GFI01_17"); signal: rGFI1c2/x_in17; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u18) { LABEL("GFI01_18"); signal: rGFI1c2/x_in18; }

DETECTOR_GFI_PSPM_U(rGFI1c1s1u19) { LABEL("GFI01_19"); signal: rGFI1c2/y_in1; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u20) { LABEL("GFI01_20"); signal: rGFI1c2/y_in2; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u21) { LABEL("GFI01_21"); signal: rGFI1c2/y_in3; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u22) { LABEL("GFI01_22"); signal: rGFI1c2/y_in4; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u23) { LABEL("GFI01_23"); signal: rGFI1c2/y_in5; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u24) { LABEL("GFI01_24"); signal: rGFI1c2/y_in6; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u25) { LABEL("GFI01_25"); signal: rGFI1c2/y_in7; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u26) { LABEL("GFI01_26"); signal: rGFI1c2/y_in8; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u27) { LABEL("GFI01_27"); signal: rGFI1c2/y_in9; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u28) { LABEL("GFI01_28"); signal: rGFI1c2/y_in10; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u29) { LABEL("GFI01_29"); signal: rGFI1c2/y_in11; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u30) { LABEL("GFI01_30"); signal: rGFI1c2/y_in12; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u31) { LABEL("GFI01_31"); signal: rGFI1c2/y_in13; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u32) { LABEL("GFI01_32"); signal: rGFI1c2/y_in14; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u33) { LABEL("GFI01_33"); signal: rGFI1c2/y_in15; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u34) { LABEL("GFI01_34"); signal: rGFI1c2/y_in16; }

VACUUM_FLANGE36(rGFI1c2) //declared in gfi.hh
{
 x_in1_18: rGFI1c1s1u1_18/signal;
 y_in1_16: rGFI1c1s1u19_34/signal;
 
 x_out1_18:   rFIBERc1s18/in1_18;
 y_out1_14:   rFIBERc1s18/in19_32;

 y_out15:  rFIBERc1s4/in31;  
 y_out16:  rFIBERc1s4/in32;


/*
 x_out2:   rGFIc1s18/in3;
 x_out3:   rGFIc1s18/in5;
 x_out4:   rGFIc1s18/in7;
 x_out5:   rGFIc1s18/in9;
 x_out6:   rGFIc1s18/in11;
 x_out7:   rGFIc1s18/in13;
 x_out8:   rGFIc1s18/in15;
 x_out9:   rGFIc1s18/in17;
 x_out10:  rGFIc1s18/in19;
 x_out11:  rGFIc1s18/in21;
 x_out12:  rGFIc1s18/in23;
 x_out13:  rGFIc1s18/in25;
 x_out14:  rGFIc1s18/in27;
 x_out15:  rGFIc1s18/in29;
 
 x_out16:  rGFIc1s4/in10;
 x_out17:  rGFIc1s4/in1;
 x_out18:  rGFIc1s4/in2;

 y_out1:   rGFIc1s9/in6;
 y_out2:   rGFIc1s9/in8;
 y_out3:   rGFIc1s9/in31;
 y_out4:   rGFIc1s9/in12;
 y_out5:   rGFIc1s9/in14;
 y_out6:   rGFIc1s9/in16;
 y_out7:   rGFIc1s9/in18;
 y_out8:   rGFIc1s9/in20;
 y_out9:   rGFIc1s9/in22;
 y_out10:  rGFIc1s9/in24;
 y_out11:  rGFIc1s9/in26;
 y_out12:  rGFIc1s9/in28;
 y_out13:  rGFIc1s9/in30;
 y_out14:  rGFIc1s9/in32;	  
 y_out15:  rGFIc1s9/in2;  
 y_out16:  rGFIc1s9/in4;
*/
}


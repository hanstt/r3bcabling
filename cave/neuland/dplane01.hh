///////////////////// written by hand to have a startup KB March14
//Naming scheme: 
// Detector:
// DPL01 - NeuLAND double plane 1
// rDPL01cXYZ - bar# XYZ=001,..100
// rDPL01cWXYsA - signal A=1,2
// signal joiners: 
// rDPL01c2XY
// HV joiners
// rDPL01c3XY

// Map DPL01 vertical (01-50) -> NNP06
// Map DPL01 horizontal (51-100) -> NNP06
NAME_MAP("DPL01_51_100", "NNP05");
NAME_MAP("DPL01_01_50", "NNP06");

//vertical plane, PMTs at the bottom:

DETECTOR_NEULAND(rDPL01c01s1) { LABEL("DPL01_01_1"); signal: rDPL01c201/in1; hv: rDPL01c301/out1;}
DETECTOR_NEULAND(rDPL01c02s1) { LABEL("DPL01_02_1"); signal: rDPL01c201/in2; hv: rDPL01c301/out2;}
DETECTOR_NEULAND(rDPL01c03s1) { LABEL("DPL01_03_1"); signal: rDPL01c201/in3; hv: rDPL01c301/out3;}
DETECTOR_NEULAND(rDPL01c04s1) { LABEL("DPL01_04_1"); signal: rDPL01c201/in4; hv: rDPL01c301/out4;}
DETECTOR_NEULAND(rDPL01c05s1) { LABEL("DPL01_05_1"); signal: rDPL01c201/in5; hv: rDPL01c301/out5;}
DETECTOR_NEULAND(rDPL01c06s1) { LABEL("DPL01_06_1"); signal: rDPL01c201/in6; hv: rDPL01c301/out6;}
DETECTOR_NEULAND(rDPL01c07s1) { LABEL("DPL01_07_1"); signal: rDPL01c201/in7; hv: rDPL01c301/out7;}
DETECTOR_NEULAND(rDPL01c08s1) { LABEL("DPL01_08_1"); signal: rDPL01c201/in8; hv: rDPL01c301/out8;}

DETECTOR_NEULAND(rDPL01c09s1) { LABEL("DPL01_09_1"); signal: rDPL01c202/in1; hv: rDPL01c301/out9;}
DETECTOR_NEULAND(rDPL01c10s1) { LABEL("DPL01_10_1"); signal: rDPL01c202/in2; hv: rDPL01c301/out10;}
DETECTOR_NEULAND(rDPL01c11s1) { LABEL("DPL01_11_1"); signal: rDPL01c202/in3; hv: rDPL01c301/out11;}
DETECTOR_NEULAND(rDPL01c12s1) { LABEL("DPL01_12_1"); signal: rDPL01c202/in4; hv: rDPL01c301/out12;}
DETECTOR_NEULAND(rDPL01c13s1) { LABEL("DPL01_13_1"); signal: rDPL01c202/in5; hv: rDPL01c301/out13;}
DETECTOR_NEULAND(rDPL01c14s1) { LABEL("DPL01_14_1"); signal: rDPL01c202/in6; hv: rDPL01c301/out14;}
DETECTOR_NEULAND(rDPL01c15s1) { LABEL("DPL01_15_1"); signal: rDPL01c202/in7; hv: rDPL01c301/out15;}
DETECTOR_NEULAND(rDPL01c16s1) { LABEL("DPL01_16_1"); signal: rDPL01c202/in8; hv: rDPL01c301/out16;}

DETECTOR_NEULAND(rDPL01c17s1) { LABEL("DPL01_17_1"); signal: rDPL01c203/in1; hv: rDPL01c301/out17;}
DETECTOR_NEULAND(rDPL01c18s1) { LABEL("DPL01_18_1"); signal: rDPL01c203/in2; hv: rDPL01c301/out18;}
DETECTOR_NEULAND(rDPL01c19s1) { LABEL("DPL01_19_1"); signal: rDPL01c203/in3; hv: rDPL01c301/out19;}
DETECTOR_NEULAND(rDPL01c20s1) { LABEL("DPL01_20_1"); signal: rDPL01c203/in4; hv: rDPL01c301/out20;}
DETECTOR_NEULAND(rDPL01c21s1) { LABEL("DPL01_21_1"); signal: rDPL01c203/in5; hv: rDPL01c301/out21;}
DETECTOR_NEULAND(rDPL01c22s1) { LABEL("DPL01_22_1"); signal: rDPL01c203/in6; hv: rDPL01c301/out22;}
DETECTOR_NEULAND(rDPL01c23s1) { LABEL("DPL01_23_1"); signal: rDPL01c203/in7; hv: rDPL01c301/out23;}
DETECTOR_NEULAND(rDPL01c24s1) { LABEL("DPL01_24_1"); signal: rDPL01c203/in8; hv: rDPL01c301/out24;}

DETECTOR_NEULAND(rDPL01c25s1) { LABEL("DPL01_25_1"); signal: rDPL01c204/in1; hv: rDPL01c301/out25;}
DETECTOR_NEULAND(rDPL01c26s1) { LABEL("DPL01_26_1"); signal: rDPL01c204/in2; hv: rDPL01c303/out1;}
DETECTOR_NEULAND(rDPL01c27s1) { LABEL("DPL01_27_1"); signal: rDPL01c204/in3; hv: rDPL01c303/out2;}
DETECTOR_NEULAND(rDPL01c28s1) { LABEL("DPL01_28_1"); signal: rDPL01c204/in4; hv: rDPL01c303/out3;}

DETECTOR_NEULAND(rDPL01c29s1) { LABEL("DPL01_29_1"); signal: rDPL01c215/in1; hv: rDPL01c303/out4;}
DETECTOR_NEULAND(rDPL01c30s1) { LABEL("DPL01_30_1"); signal: rDPL01c215/in2; hv: rDPL01c303/out5;}
DETECTOR_NEULAND(rDPL01c31s1) { LABEL("DPL01_31_1"); signal: rDPL01c215/in3; hv: rDPL01c303/out6;}
DETECTOR_NEULAND(rDPL01c32s1) { LABEL("DPL01_32_1"); signal: rDPL01c215/in4; hv: rDPL01c303/out7;}
DETECTOR_NEULAND(rDPL01c33s1) { LABEL("DPL01_33_1"); signal: rDPL01c215/in5; hv: rDPL01c303/out8;}
DETECTOR_NEULAND(rDPL01c34s1) { LABEL("DPL01_34_1"); signal: rDPL01c215/in6; hv: rDPL01c303/out9;}
DETECTOR_NEULAND(rDPL01c35s1) { LABEL("DPL01_35_1"); signal: rDPL01c215/in7; hv: rDPL01c303/out10;}
DETECTOR_NEULAND(rDPL01c36s1) { LABEL("DPL01_36_1"); signal: rDPL01c215/in8; hv: rDPL01c303/out11;}

DETECTOR_NEULAND(rDPL01c37s1) { LABEL("DPL01_37_1"); signal: rDPL01c216/in1; hv: rDPL01c303/out12;}
DETECTOR_NEULAND(rDPL01c38s1) { LABEL("DPL01_38_1"); signal: rDPL01c216/in2; hv: rDPL01c303/out13;}
DETECTOR_NEULAND(rDPL01c39s1) { LABEL("DPL01_39_1"); signal: rDPL01c216/in3; hv: rDPL01c303/out14;}
DETECTOR_NEULAND(rDPL01c40s1) { LABEL("DPL01_40_1"); signal: rDPL01c216/in4; hv: rDPL01c303/out15;}
DETECTOR_NEULAND(rDPL01c41s1) { LABEL("DPL01_41_1"); signal: rDPL01c216/in5; hv: rDPL01c303/out16;}
DETECTOR_NEULAND(rDPL01c42s1) { LABEL("DPL01_42_1"); signal: rDPL01c216/in6; hv: rDPL01c303/out17;}
DETECTOR_NEULAND(rDPL01c43s1) { LABEL("DPL01_43_1"); signal: rDPL01c216/in7; hv: rDPL01c303/out18;}
DETECTOR_NEULAND(rDPL01c44s1) { LABEL("DPL01_44_1"); signal: rDPL01c216/in8; hv: rDPL01c303/out19;}

DETECTOR_NEULAND(rDPL01c45s1) { LABEL("DPL01_45_1"); signal: rDPL01c217/in1; hv: rDPL01c303/out20;}
DETECTOR_NEULAND(rDPL01c46s1) { LABEL("DPL01_46_1"); signal: rDPL01c217/in2; hv: rDPL01c303/out21;}
DETECTOR_NEULAND(rDPL01c47s1) { LABEL("DPL01_47_1"); signal: rDPL01c217/in3; hv: rDPL01c303/out22;}
DETECTOR_NEULAND(rDPL01c48s1) { LABEL("DPL01_48_1"); signal: rDPL01c217/in4; hv: rDPL01c303/out23;}
DETECTOR_NEULAND(rDPL01c49s1) { LABEL("DPL01_49_1"); signal: rDPL01c217/in5; hv: rDPL01c303/out24;}
DETECTOR_NEULAND(rDPL01c50s1) { LABEL("DPL01_50_1"); signal: rDPL01c217/in6; hv: rDPL01c303/out25;}

//vertical plane, PMTs at the top:

DETECTOR_NEULAND(rDPL01c01s2) { LABEL("DPL01_01_2"); signal: rDPL01c210/in7; hv: rDPL01c307/out1;}
DETECTOR_NEULAND(rDPL01c02s2) { LABEL("DPL01_02_2"); signal: rDPL01c210/in8; hv: rDPL01c307/out2;}

DETECTOR_NEULAND(rDPL01c03s2) { LABEL("DPL01_03_2"); signal: rDPL01c211/in1; hv: rDPL01c307/out3;}
DETECTOR_NEULAND(rDPL01c04s2) { LABEL("DPL01_04_2"); signal: rDPL01c211/in2; hv: rDPL01c307/out4;}
DETECTOR_NEULAND(rDPL01c05s2) { LABEL("DPL01_05_2"); signal: rDPL01c211/in3; hv: rDPL01c307/out5;}
DETECTOR_NEULAND(rDPL01c06s2) { LABEL("DPL01_06_2"); signal: rDPL01c211/in4; hv: rDPL01c307/out6;}
DETECTOR_NEULAND(rDPL01c07s2) { LABEL("DPL01_07_2"); signal: rDPL01c211/in5; hv: rDPL01c307/out7;}
DETECTOR_NEULAND(rDPL01c08s2) { LABEL("DPL01_08_2"); signal: rDPL01c211/in6; hv: rDPL01c307/out8;}
DETECTOR_NEULAND(rDPL01c09s2) { LABEL("DPL01_09_2"); signal: rDPL01c211/in7; hv: rDPL01c307/out9;}
DETECTOR_NEULAND(rDPL01c10s2) { LABEL("DPL01_10_2"); signal: rDPL01c211/in8; hv: rDPL01c307/out10;}

DETECTOR_NEULAND(rDPL01c11s2) { LABEL("DPL01_11_2"); signal: rDPL01c212/in1; hv: rDPL01c307/out11;}
DETECTOR_NEULAND(rDPL01c12s2) { LABEL("DPL01_12_2"); signal: rDPL01c212/in2; hv: rDPL01c307/out12;}
DETECTOR_NEULAND(rDPL01c13s2) { LABEL("DPL01_13_2"); signal: rDPL01c212/in3; hv: rDPL01c307/out13;}
DETECTOR_NEULAND(rDPL01c14s2) { LABEL("DPL01_14_2"); signal: rDPL01c212/in4; hv: rDPL01c307/out14;}
DETECTOR_NEULAND(rDPL01c15s2) { LABEL("DPL01_15_2"); signal: rDPL01c212/in5; hv: rDPL01c307/out15;}
DETECTOR_NEULAND(rDPL01c16s2) { LABEL("DPL01_16_2"); signal: rDPL01c212/in6; hv: rDPL01c307/out16;}
DETECTOR_NEULAND(rDPL01c17s2) { LABEL("DPL01_17_2"); signal: rDPL01c212/in7; hv: rDPL01c307/out17;}
DETECTOR_NEULAND(rDPL01c18s2) { LABEL("DPL01_18_2"); signal: rDPL01c212/in8; hv: rDPL01c307/out18;}

DETECTOR_NEULAND(rDPL01c19s2) { LABEL("DPL01_19_2"); signal: rDPL01c213/in1; hv: rDPL01c307/out19;}
DETECTOR_NEULAND(rDPL01c20s2) { LABEL("DPL01_20_2"); signal: rDPL01c213/in2; hv: rDPL01c307/out20;}
DETECTOR_NEULAND(rDPL01c21s2) { LABEL("DPL01_21_2"); signal: rDPL01c213/in3; hv: rDPL01c307/out21;}
DETECTOR_NEULAND(rDPL01c22s2) { LABEL("DPL01_22_2"); signal: rDPL01c213/in4; hv: rDPL01c307/out22;}
DETECTOR_NEULAND(rDPL01c23s2) { LABEL("DPL01_23_2"); signal: rDPL01c213/in5; hv: rDPL01c307/out23;}
DETECTOR_NEULAND(rDPL01c24s2) { LABEL("DPL01_24_2"); signal: rDPL01c213/in6; hv: rDPL01c307/out24;}
DETECTOR_NEULAND(rDPL01c25s2) { LABEL("DPL01_25_2"); signal: rDPL01c213/in7; hv: rDPL01c307/out25;}
DETECTOR_NEULAND(rDPL01c26s2) { LABEL("DPL01_26_2"); signal: rDPL01c213/in8; hv: rDPL01c305/out1;}

DETECTOR_NEULAND(rDPL01c27s2) { LABEL("DPL01_27_2"); signal: rDPL01c214/in1; hv: rDPL01c305/out2;}
DETECTOR_NEULAND(rDPL01c28s2) { LABEL("DPL01_28_2"); signal: rDPL01c214/in2; hv: rDPL01c305/out3;}
DETECTOR_NEULAND(rDPL01c29s2) { LABEL("DPL01_29_2"); signal: rDPL01c214/in3; hv: rDPL01c305/out4;}
DETECTOR_NEULAND(rDPL01c30s2) { LABEL("DPL01_30_2"); signal: rDPL01c214/in4; hv: rDPL01c305/out5;}

DETECTOR_NEULAND(rDPL01c31s2) { LABEL("DPL01_31_2"); signal: rDPL01c224/in1; hv: rDPL01c305/out6;}
DETECTOR_NEULAND(rDPL01c32s2) { LABEL("DPL01_32_2"); signal: rDPL01c224/in2; hv: rDPL01c305/out7;}
DETECTOR_NEULAND(rDPL01c33s2) { LABEL("DPL01_33_2"); signal: rDPL01c224/in3; hv: rDPL01c305/out8;}
DETECTOR_NEULAND(rDPL01c34s2) { LABEL("DPL01_34_2"); signal: rDPL01c224/in4; hv: rDPL01c305/out9;}
DETECTOR_NEULAND(rDPL01c35s2) { LABEL("DPL01_35_2"); signal: rDPL01c224/in5; hv: rDPL01c305/out10;}
DETECTOR_NEULAND(rDPL01c36s2) { LABEL("DPL01_36_2"); signal: rDPL01c224/in6; hv: rDPL01c305/out11;}
DETECTOR_NEULAND(rDPL01c37s2) { LABEL("DPL01_37_2"); signal: rDPL01c224/in7; hv: rDPL01c305/out12;}
DETECTOR_NEULAND(rDPL01c38s2) { LABEL("DPL01_38_2"); signal: rDPL01c224/in8; hv: rDPL01c305/out13;}

DETECTOR_NEULAND(rDPL01c39s2) { LABEL("DPL01_39_2"); signal: rDPL01c225/in1; hv: rDPL01c305/out14;}
DETECTOR_NEULAND(rDPL01c40s2) { LABEL("DPL01_40_2"); signal: rDPL01c225/in2; hv: rDPL01c305/out15;}
DETECTOR_NEULAND(rDPL01c41s2) { LABEL("DPL01_41_2"); signal: rDPL01c225/in3; hv: rDPL01c305/out16;}
DETECTOR_NEULAND(rDPL01c42s2) { LABEL("DPL01_42_2"); signal: rDPL01c225/in4; hv: rDPL01c305/out17;}
DETECTOR_NEULAND(rDPL01c43s2) { LABEL("DPL01_43_2"); signal: rDPL01c225/in5; hv: rDPL01c305/out18;}
DETECTOR_NEULAND(rDPL01c44s2) { LABEL("DPL01_44_2"); signal: rDPL01c225/in6; hv: rDPL01c305/out19;}
DETECTOR_NEULAND(rDPL01c45s2) { LABEL("DPL01_45_2"); signal: rDPL01c225/in7; hv: rDPL01c305/out20;}
DETECTOR_NEULAND(rDPL01c46s2) { LABEL("DPL01_46_2"); signal: rDPL01c225/in8; hv: rDPL01c305/out21;}

DETECTOR_NEULAND(rDPL01c47s2) { LABEL("DPL01_47_2"); signal: rDPL01c226/in1; hv: rDPL01c305/out22;}
DETECTOR_NEULAND(rDPL01c48s2) { LABEL("DPL01_48_2"); signal: rDPL01c226/in2; hv: rDPL01c305/out23;}
DETECTOR_NEULAND(rDPL01c49s2) { LABEL("DPL01_49_2"); signal: rDPL01c226/in3; hv: rDPL01c305/out24;}
DETECTOR_NEULAND(rDPL01c50s2) { LABEL("DPL01_50_2"); signal: rDPL01c226/in4; hv: rDPL01c305/out25;}


//horizontal plane, PMTs at the right (look with beam):

DETECTOR_NEULAND(rDPL01c51s1) { LABEL("DPL01_51_1"); signal: rDPL01c204/in5; hv: rDPL01c302/out1;}
DETECTOR_NEULAND(rDPL01c52s1) { LABEL("DPL01_52_1"); signal: rDPL01c204/in6; hv: rDPL01c302/out2;}
DETECTOR_NEULAND(rDPL01c53s1) { LABEL("DPL01_53_1"); signal: rDPL01c204/in7; hv: rDPL01c302/out3;}
DETECTOR_NEULAND(rDPL01c54s1) { LABEL("DPL01_54_1"); signal: rDPL01c204/in8; hv: rDPL01c302/out4;}

DETECTOR_NEULAND(rDPL01c55s1) { LABEL("DPL01_55_1"); signal: rDPL01c205/in1; hv: rDPL01c302/out5;}
DETECTOR_NEULAND(rDPL01c56s1) { LABEL("DPL01_56_1"); signal: rDPL01c205/in2; hv: rDPL01c302/out6;}
DETECTOR_NEULAND(rDPL01c57s1) { LABEL("DPL01_57_1"); signal: rDPL01c205/in3; hv: rDPL01c302/out7;}
DETECTOR_NEULAND(rDPL01c58s1) { LABEL("DPL01_58_1"); signal: rDPL01c205/in4; hv: rDPL01c302/out8;}
DETECTOR_NEULAND(rDPL01c59s1) { LABEL("DPL01_59_1"); signal: rDPL01c205/in5; hv: rDPL01c302/out9;}
DETECTOR_NEULAND(rDPL01c60s1) { LABEL("DPL01_60_1"); signal: rDPL01c205/in6; hv: rDPL01c302/out10;}
DETECTOR_NEULAND(rDPL01c61s1) { LABEL("DPL01_61_1"); signal: rDPL01c205/in7; hv: rDPL01c302/out11;}
DETECTOR_NEULAND(rDPL01c62s1) { LABEL("DPL01_62_1"); signal: rDPL01c205/in8; hv: rDPL01c302/out12;}

DETECTOR_NEULAND(rDPL01c63s1) { LABEL("DPL01_63_1"); signal: rDPL01c206/in1; hv: rDPL01c302/out13;}
DETECTOR_NEULAND(rDPL01c64s1) { LABEL("DPL01_64_1"); signal: rDPL01c206/in2; hv: rDPL01c302/out14;}
DETECTOR_NEULAND(rDPL01c65s1) { LABEL("DPL01_65_1"); signal: rDPL01c206/in3; hv: rDPL01c302/out15;}
DETECTOR_NEULAND(rDPL01c66s1) { LABEL("DPL01_66_1"); signal: rDPL01c206/in4; hv: rDPL01c302/out16;}
DETECTOR_NEULAND(rDPL01c67s1) { LABEL("DPL01_67_1"); signal: rDPL01c206/in5; hv: rDPL01c302/out17;}
DETECTOR_NEULAND(rDPL01c68s1) { LABEL("DPL01_68_1"); signal: rDPL01c206/in6; hv: rDPL01c302/out18;}
DETECTOR_NEULAND(rDPL01c69s1) { LABEL("DPL01_69_1"); signal: rDPL01c206/in7; hv: rDPL01c302/out19;}
DETECTOR_NEULAND(rDPL01c70s1) { LABEL("DPL01_70_1"); signal: rDPL01c206/in8; hv: rDPL01c302/out20;}

DETECTOR_NEULAND(rDPL01c71s1) { LABEL("DPL01_71_1"); signal: rDPL01c207/in1; hv: rDPL01c302/out21;}
DETECTOR_NEULAND(rDPL01c72s1) { LABEL("DPL01_72_1"); signal: rDPL01c207/in2; hv: rDPL01c302/out22;}
DETECTOR_NEULAND(rDPL01c73s1) { LABEL("DPL01_73_1"); signal: rDPL01c207/in3; hv: rDPL01c302/out23;}
DETECTOR_NEULAND(rDPL01c74s1) { LABEL("DPL01_74_1"); signal: rDPL01c207/in4; hv: rDPL01c302/out24;}
DETECTOR_NEULAND(rDPL01c75s1) { LABEL("DPL01_75_1"); signal: rDPL01c207/in5; hv: rDPL01c302/out25;}
DETECTOR_NEULAND(rDPL01c76s1) { LABEL("DPL01_76_1"); signal: rDPL01c207/in6; hv: rDPL01c308/out1;}
DETECTOR_NEULAND(rDPL01c77s1) { LABEL("DPL01_77_1"); signal: rDPL01c207/in7; hv: rDPL01c308/out2;}
DETECTOR_NEULAND(rDPL01c78s1) { LABEL("DPL01_78_1"); signal: rDPL01c207/in8; hv: rDPL01c308/out3;}

DETECTOR_NEULAND(rDPL01c79s1) { LABEL("DPL01_79_1"); signal: rDPL01c208/in1; hv: rDPL01c308/out4;}
DETECTOR_NEULAND(rDPL01c80s1) { LABEL("DPL01_80_1"); signal: rDPL01c208/in2; hv: rDPL01c308/out5;}
DETECTOR_NEULAND(rDPL01c81s1) { LABEL("DPL01_81_1"); signal: rDPL01c208/in3; hv: rDPL01c308/out6;}
DETECTOR_NEULAND(rDPL01c82s1) { LABEL("DPL01_82_1"); signal: rDPL01c208/in4; hv: rDPL01c308/out7;}
DETECTOR_NEULAND(rDPL01c83s1) { LABEL("DPL01_83_1"); signal: rDPL01c208/in5; hv: rDPL01c308/out8;}
DETECTOR_NEULAND(rDPL01c84s1) { LABEL("DPL01_84_1"); signal: rDPL01c208/in6; hv: rDPL01c308/out9;}
DETECTOR_NEULAND(rDPL01c85s1) { LABEL("DPL01_85_1"); signal: rDPL01c208/in7; hv: rDPL01c308/out10;}
DETECTOR_NEULAND(rDPL01c86s1) { LABEL("DPL01_86_1"); signal: rDPL01c208/in8; hv: rDPL01c308/out11;}

DETECTOR_NEULAND(rDPL01c87s1) { LABEL("DPL01_87_1"); signal: rDPL01c209/in1; hv: rDPL01c308/out12;}
DETECTOR_NEULAND(rDPL01c88s1) { LABEL("DPL01_88_1"); signal: rDPL01c209/in2; hv: rDPL01c308/out13;}
DETECTOR_NEULAND(rDPL01c89s1) { LABEL("DPL01_89_1"); signal: rDPL01c209/in3; hv: rDPL01c308/out14;}
DETECTOR_NEULAND(rDPL01c90s1) { LABEL("DPL01_90_1"); signal: rDPL01c209/in4; hv: rDPL01c308/out15;}
DETECTOR_NEULAND(rDPL01c91s1) { LABEL("DPL01_91_1"); signal: rDPL01c209/in5; hv: rDPL01c308/out16;}
DETECTOR_NEULAND(rDPL01c92s1) { LABEL("DPL01_92_1"); signal: rDPL01c209/in6; hv: rDPL01c308/out17;}
DETECTOR_NEULAND(rDPL01c93s1) { LABEL("DPL01_93_1"); signal: rDPL01c209/in7; hv: rDPL01c308/out18;}
DETECTOR_NEULAND(rDPL01c94s1) { LABEL("DPL01_94_1"); signal: rDPL01c209/in8; hv: rDPL01c308/out19;}

DETECTOR_NEULAND(rDPL01c95s1) { LABEL("DPL01_95_1"); signal: rDPL01c210/in1; hv: rDPL01c308/out20;}
DETECTOR_NEULAND(rDPL01c96s1) { LABEL("DPL01_96_1"); signal: rDPL01c210/in2; hv: rDPL01c308/out21;}
DETECTOR_NEULAND(rDPL01c97s1) { LABEL("DPL01_97_1"); signal: rDPL01c210/in3; hv: rDPL01c308/out22;}
DETECTOR_NEULAND(rDPL01c98s1) { LABEL("DPL01_98_1"); signal: rDPL01c210/in4; hv: rDPL01c308/out23;}
DETECTOR_NEULAND(rDPL01c99s1) { LABEL("DPL01_99_1"); signal: rDPL01c210/in5; hv: rDPL01c308/out24;}
DETECTOR_NEULAND(rDPL01c100s1) { LABEL("DPL01_100_1"); signal: rDPL01c210/in6; hv: rDPL01c308/out25;}

//horizontal plane, PMTs at the left (look with beam):

DETECTOR_NEULAND(rDPL01c51s2) { LABEL("DPL01_51_2"); signal: rDPL01c217/in7; hv: rDPL01c304/out1;}
DETECTOR_NEULAND(rDPL01c52s2) { LABEL("DPL01_52_2"); signal: rDPL01c217/in8; hv: rDPL01c304/out2;}

DETECTOR_NEULAND(rDPL01c53s2) { LABEL("DPL01_53_2"); signal: rDPL01c218/in1; hv: rDPL01c304/out3;}
DETECTOR_NEULAND(rDPL01c54s2) { LABEL("DPL01_54_2"); signal: rDPL01c218/in2; hv: rDPL01c304/out4;}
DETECTOR_NEULAND(rDPL01c55s2) { LABEL("DPL01_55_2"); signal: rDPL01c218/in3; hv: rDPL01c304/out5;}
DETECTOR_NEULAND(rDPL01c56s2) { LABEL("DPL01_56_2"); signal: rDPL01c218/in4; hv: rDPL01c304/out6;}
DETECTOR_NEULAND(rDPL01c57s2) { LABEL("DPL01_57_2"); signal: rDPL01c218/in5; hv: rDPL01c304/out7;}
DETECTOR_NEULAND(rDPL01c58s2) { LABEL("DPL01_58_2"); signal: rDPL01c218/in6; hv: rDPL01c304/out8;}
DETECTOR_NEULAND(rDPL01c59s2) { LABEL("DPL01_59_2"); signal: rDPL01c218/in7; hv: rDPL01c304/out9;}
DETECTOR_NEULAND(rDPL01c60s2) { LABEL("DPL01_60_2"); signal: rDPL01c218/in8; hv: rDPL01c304/out10;}

DETECTOR_NEULAND(rDPL01c61s2) { LABEL("DPL01_61_2"); signal: rDPL01c219/in1; hv: rDPL01c304/out11;}
DETECTOR_NEULAND(rDPL01c62s2) { LABEL("DPL01_62_2"); signal: rDPL01c219/in2; hv: rDPL01c304/out12;}
DETECTOR_NEULAND(rDPL01c63s2) { LABEL("DPL01_63_2"); signal: rDPL01c219/in3; hv: rDPL01c304/out13;}
DETECTOR_NEULAND(rDPL01c64s2) { LABEL("DPL01_64_2"); signal: rDPL01c219/in4; hv: rDPL01c304/out14;}
DETECTOR_NEULAND(rDPL01c65s2) { LABEL("DPL01_65_2"); signal: rDPL01c219/in5; hv: rDPL01c304/out15;}
DETECTOR_NEULAND(rDPL01c66s2) { LABEL("DPL01_66_2"); signal: rDPL01c219/in6; hv: rDPL01c304/out16;}
DETECTOR_NEULAND(rDPL01c67s2) { LABEL("DPL01_67_2"); signal: rDPL01c219/in7; hv: rDPL01c304/out17;}
DETECTOR_NEULAND(rDPL01c68s2) { LABEL("DPL01_68_2"); signal: rDPL01c219/in8; hv: rDPL01c304/out18;}

DETECTOR_NEULAND(rDPL01c69s2) { LABEL("DPL01_69_2"); signal: rDPL01c220/in1; hv: rDPL01c304/out19;}
DETECTOR_NEULAND(rDPL01c70s2) { LABEL("DPL01_70_2"); signal: rDPL01c220/in2; hv: rDPL01c304/out20;}
DETECTOR_NEULAND(rDPL01c71s2) { LABEL("DPL01_71_2"); signal: rDPL01c220/in3; hv: rDPL01c304/out21;}
DETECTOR_NEULAND(rDPL01c72s2) { LABEL("DPL01_72_2"); signal: rDPL01c220/in4; hv: rDPL01c304/out22;}
DETECTOR_NEULAND(rDPL01c73s2) { LABEL("DPL01_73_2"); signal: rDPL01c220/in5; hv: rDPL01c304/out23;}
DETECTOR_NEULAND(rDPL01c74s2) { LABEL("DPL01_74_2"); signal: rDPL01c220/in6; hv: rDPL01c304/out24;}
DETECTOR_NEULAND(rDPL01c75s2) { LABEL("DPL01_75_2"); signal: rDPL01c220/in7; hv: rDPL01c304/out25;}
DETECTOR_NEULAND(rDPL01c76s2) { LABEL("DPL01_76_2"); signal: rDPL01c220/in8; hv: rDPL01c306/out1;}

DETECTOR_NEULAND(rDPL01c77s2) { LABEL("DPL01_77_2"); signal: rDPL01c221/in1; hv: rDPL01c306/out2;}
DETECTOR_NEULAND(rDPL01c78s2) { LABEL("DPL01_78_2"); signal: rDPL01c221/in2; hv: rDPL01c306/out3;}
DETECTOR_NEULAND(rDPL01c79s2) { LABEL("DPL01_79_2"); signal: rDPL01c221/in3; hv: rDPL01c306/out4;}
DETECTOR_NEULAND(rDPL01c80s2) { LABEL("DPL01_80_2"); signal: rDPL01c221/in4; hv: rDPL01c306/out5;}
DETECTOR_NEULAND(rDPL01c81s2) { LABEL("DPL01_81_2"); signal: rDPL01c221/in5; hv: rDPL01c306/out6;}
DETECTOR_NEULAND(rDPL01c82s2) { LABEL("DPL01_82_2"); signal: rDPL01c221/in6; hv: rDPL01c306/out7;}
DETECTOR_NEULAND(rDPL01c83s2) { LABEL("DPL01_83_2"); signal: rDPL01c221/in7; hv: rDPL01c306/out8;}
DETECTOR_NEULAND(rDPL01c84s2) { LABEL("DPL01_84_2"); signal: rDPL01c221/in8; hv: rDPL01c306/out9;}

DETECTOR_NEULAND(rDPL01c85s2) { LABEL("DPL01_85_2"); signal: rDPL01c222/in1; hv: rDPL01c306/out10;}
DETECTOR_NEULAND(rDPL01c86s2) { LABEL("DPL01_86_2"); signal: rDPL01c222/in2; hv: rDPL01c306/out11;}
DETECTOR_NEULAND(rDPL01c87s2) { LABEL("DPL01_87_2"); signal: rDPL01c222/in3; hv: rDPL01c306/out12;}
DETECTOR_NEULAND(rDPL01c88s2) { LABEL("DPL01_88_2"); signal: rDPL01c222/in4; hv: rDPL01c306/out13;}
DETECTOR_NEULAND(rDPL01c89s2) { LABEL("DPL01_89_2"); signal: rDPL01c222/in5; hv: rDPL01c306/out14;}
DETECTOR_NEULAND(rDPL01c90s2) { LABEL("DPL01_90_2"); signal: rDPL01c222/in6; hv: rDPL01c306/out15;}
DETECTOR_NEULAND(rDPL01c91s2) { LABEL("DPL01_91_2"); signal: rDPL01c222/in7; hv: rDPL01c306/out16;}
DETECTOR_NEULAND(rDPL01c92s2) { LABEL("DPL01_92_2"); signal: rDPL01c222/in8; hv: rDPL01c306/out17;}


DETECTOR_NEULAND(rDPL01c93s2) { LABEL("DPL01_93_2"); signal: rDPL01c223/in1; hv: rDPL01c306/out18;}
DETECTOR_NEULAND(rDPL01c94s2) { LABEL("DPL01_94_2"); signal: rDPL01c223/in2; hv: rDPL01c306/out19;}
DETECTOR_NEULAND(rDPL01c95s2) { LABEL("DPL01_95_2"); signal: rDPL01c223/in3; hv: rDPL01c306/out20;}
DETECTOR_NEULAND(rDPL01c96s2) { LABEL("DPL01_96_2"); signal: rDPL01c223/in4; hv: rDPL01c306/out21;}
DETECTOR_NEULAND(rDPL01c97s2) { LABEL("DPL01_97_2"); signal: rDPL01c223/in5; hv: rDPL01c306/out22;}
DETECTOR_NEULAND(rDPL01c98s2) { LABEL("DPL01_98_2"); signal: rDPL01c223/in6; hv: rDPL01c306/out23;}
DETECTOR_NEULAND(rDPL01c99s2) { LABEL("DPL01_99_2"); signal: rDPL01c223/in7; hv: rDPL01c306/out24;}
DETECTOR_NEULAND(rDPL01c100s2) { LABEL("DPL01_100_2"); signal: rDPL01c223/in8; hv: rDPL01c306/out25;}


////// signal joiners 8-fold: 
// Nomenclature with respect to pdf document on NeuLAND-plane TAMEX.Channel
// c2XYsZ stands for signal
// XY is (TAMEX times 2)-1 for channel 1-8, XY is (TAMEX times 2) for channel 9-16,, Z is CHANNEL 
// examples:
//"1.1"  --> c201s1
//"1.16" --> c202s8
//"9.1"  --> c218s1
//
 
JOINER_8(rDPL01c201)
{
in1: rDPL01c01s1/signal;
in2: rDPL01c02s1/signal;
in3: rDPL01c03s1/signal;
in4: rDPL01c04s1/signal;
in5: rDPL01c05s1/signal;
in6: rDPL01c06s1/signal;
in7: rDPL01c07s1/signal;
in8: rDPL01c08s1/signal;


out1: rackLAND2c1s1u1/in8;
out2: rackLAND2c1s1u1/in7;
out3: rackLAND2c1s1u1/in6;
out4: rackLAND2c1s1u1/in5;
out5: rackLAND2c1s1u1/in4;
out6: rackLAND2c1s1u1/in3;
out7: rackLAND2c1s1u1/in2;
out8: rackLAND2c1s1u1/in1;
}
JOINER_8(rDPL01c202)
{
in1: rDPL01c09s1/signal;
in2: rDPL01c10s1/signal;
in3: rDPL01c11s1/signal;
in4: rDPL01c12s1/signal;
in5: rDPL01c13s1/signal;
in6: rDPL01c14s1/signal;
in7: rDPL01c15s1/signal;
in8: rDPL01c16s1/signal;


out1: rackLAND2c1s1u1/in16;
out2: rackLAND2c1s1u1/in15;
out3: rackLAND2c1s1u1/in14;
out4: rackLAND2c1s1u1/in13;
out5: rackLAND2c1s1u1/in12;
out6: rackLAND2c1s1u1/in11;
out7: rackLAND2c1s1u1/in10;
out8: rackLAND2c1s1u1/in9;
}
JOINER_8(rDPL01c203)
{
in1: rDPL01c17s1/signal;
in2: rDPL01c18s1/signal;
in3: rDPL01c19s1/signal;
in4: rDPL01c20s1/signal;
in5: rDPL01c21s1/signal;
in6: rDPL01c22s1/signal;
in7: rDPL01c23s1/signal;
in8: rDPL01c24s1/signal;

out1: rackLAND2c1s2u1/in8;
out2: rackLAND2c1s2u1/in7;
out3: rackLAND2c1s2u1/in6;
out4: rackLAND2c1s2u1/in5;
out5: rackLAND2c1s2u1/in4;
out6: rackLAND2c1s2u1/in3;
out7: rackLAND2c1s2u1/in2;
out8: rackLAND2c1s2u1/in1;
}


JOINER_8(rDPL01c204)
{
in1: rDPL01c25s1/signal;
in2: rDPL01c26s1/signal;
in3: rDPL01c27s1/signal;
in4: rDPL01c28s1/signal;

in5: rDPL01c51s1/signal;
in6: rDPL01c52s1/signal;
in7: rDPL01c53s1/signal;
in8: rDPL01c54s1/signal;

out1: rackLAND2c1s2u1/in16;
out2: rackLAND2c1s2u1/in15;
out3: rackLAND2c1s2u1/in14;
out4: rackLAND2c1s2u1/in13;
out5: rackLAND2c1s2u1/in12;
out6: rackLAND2c1s2u1/in11;
out7: rackLAND2c1s2u1/in10;
out8: rackLAND2c1s2u1/in9;
}

JOINER_8(rDPL01c205)
{
in1: rDPL01c55s1/signal;
in2: rDPL01c56s1/signal;
in3: rDPL01c57s1/signal;
in4: rDPL01c58s1/signal;
in5: rDPL01c59s1/signal;
in6: rDPL01c60s1/signal;
in7: rDPL01c61s1/signal;
in8: rDPL01c62s1/signal;

out1: rackLAND2c1s3u1/in8;
out2: rackLAND2c1s3u1/in7;
out3: rackLAND2c1s3u1/in6;
out4: rackLAND2c1s3u1/in5;
out5: rackLAND2c1s3u1/in4;
out6: rackLAND2c1s3u1/in3;
out7: rackLAND2c1s3u1/in2;
out8: rackLAND2c1s3u1/in1;
}

JOINER_8(rDPL01c206)
{
in1: rDPL01c63s1/signal;
in2: rDPL01c64s1/signal;
in3: rDPL01c65s1/signal;
in4: rDPL01c66s1/signal;
in5: rDPL01c67s1/signal;
in6: rDPL01c68s1/signal;
in7: rDPL01c69s1/signal;
in8: rDPL01c70s1/signal;

out1: rackLAND2c1s3u1/in16;
out2: rackLAND2c1s3u1/in15;
out3: rackLAND2c1s3u1/in14;
out4: rackLAND2c1s3u1/in13;
out5: rackLAND2c1s3u1/in12;
out6: rackLAND2c1s3u1/in11;
out7: rackLAND2c1s3u1/in10;
out8: rackLAND2c1s3u1/in9;
}
JOINER_8(rDPL01c207)
{
in1: rDPL01c71s1/signal;
in2: rDPL01c72s1/signal;
in3: rDPL01c73s1/signal;
in4: rDPL01c74s1/signal;
in5: rDPL01c75s1/signal;
in6: rDPL01c76s1/signal;
in7: rDPL01c77s1/signal;
in8: rDPL01c78s1/signal;

out1: rackLAND2c1s4u1/in8;
out2: rackLAND2c1s4u1/in7;
out3: rackLAND2c1s4u1/in6;
out4: rackLAND2c1s4u1/in5;
out5: rackLAND2c1s4u1/in4;
out6: rackLAND2c1s4u1/in3;
out7: rackLAND2c1s4u1/in2;
out8: rackLAND2c1s4u1/in1;
}

JOINER_8(rDPL01c208)
{
in1: rDPL01c79s1/signal;
in2: rDPL01c80s1/signal;
in3: rDPL01c81s1/signal;
in4: rDPL01c82s1/signal;
in5: rDPL01c83s1/signal;
in6: rDPL01c84s1/signal;
in7: rDPL01c85s1/signal;
in8: rDPL01c86s1/signal;

out1: rackLAND2c1s4u1/in16;
out2: rackLAND2c1s4u1/in15;
out3: rackLAND2c1s4u1/in14;
out4: rackLAND2c1s4u1/in13;
out5: rackLAND2c1s4u1/in12;
out6: rackLAND2c1s4u1/in11;
out7: rackLAND2c1s4u1/in10;
out8: rackLAND2c1s4u1/in9;
}

JOINER_8(rDPL01c209)
{
in1: rDPL01c87s1/signal;
in2: rDPL01c88s1/signal;
in3: rDPL01c89s1/signal;
in4: rDPL01c90s1/signal;
in5: rDPL01c91s1/signal;
in6: rDPL01c92s1/signal;
in7: rDPL01c93s1/signal;
in8: rDPL01c94s1/signal;

out1: rackLAND2c1s5u1/in8;
out2: rackLAND2c1s5u1/in7;
out3: rackLAND2c1s5u1/in6;
out4: rackLAND2c1s5u1/in5;
out5: rackLAND2c1s5u1/in4;
out6: rackLAND2c1s5u1/in3;
out7: rackLAND2c1s5u1/in2;
out8: rackLAND2c1s5u1/in1;
}

JOINER_8(rDPL01c210)
{
in1: rDPL01c95s1/signal;
in2: rDPL01c96s1/signal;
in3: rDPL01c97s1/signal;
in4: rDPL01c98s1/signal;
in5: rDPL01c99s1/signal;
in6: rDPL01c100s1/signal;

in7: rDPL01c1s2/signal;
in8: rDPL01c2s2/signal;


out1: rackLAND2c1s5u1/in16;
out2: rackLAND2c1s5u1/in15;
out3: rackLAND2c1s5u1/in14;
out4: rackLAND2c1s5u1/in13;
out5: rackLAND2c1s5u1/in12;
out6: rackLAND2c1s5u1/in11;

out7: rackLAND2c1s5u1/in10;
out8: rackLAND2c1s5u1/in9;
}
JOINER_8(rDPL01c211)
{
in1: rDPL01c03s2/signal;
in2: rDPL01c04s2/signal;
in3: rDPL01c05s2/signal;
in4: rDPL01c06s2/signal;
in5: rDPL01c07s2/signal;
in6: rDPL01c08s2/signal;
in7: rDPL01c09s2/signal;
in8: rDPL01c10s2/signal;


out1: rackLAND2c1s6u1/in8;
out2: rackLAND2c1s6u1/in7;
out3: rackLAND2c1s6u1/in6;
out4: rackLAND2c1s6u1/in5;
out5: rackLAND2c1s6u1/in4;
out6: rackLAND2c1s6u1/in3;
out7: rackLAND2c1s6u1/in2;
out8: rackLAND2c1s6u1/in1;
}

JOINER_8(rDPL01c212)
{
in1: rDPL01c11s2/signal;
in2: rDPL01c12s2/signal;
in3: rDPL01c13s2/signal;
in4: rDPL01c14s2/signal;
in5: rDPL01c15s2/signal;
in6: rDPL01c16s2/signal;
in7: rDPL01c17s2/signal;
in8: rDPL01c18s2/signal;

out1: rackLAND2c1s6u1/in16;
out2: rackLAND2c1s6u1/in15;
out3: rackLAND2c1s6u1/in14;
out4: rackLAND2c1s6u1/in13;
out5: rackLAND2c1s6u1/in12;
out6: rackLAND2c1s6u1/in11;
out7: rackLAND2c1s6u1/in10;
out8: rackLAND2c1s6u1/in9;
}

JOINER_8(rDPL01c213)
{
in1: rDPL01c19s2/signal;
in2: rDPL01c20s2/signal;
in3: rDPL01c21s2/signal;
in4: rDPL01c22s2/signal;
in5: rDPL01c23s2/signal;
in6: rDPL01c24s2/signal;
in7: rDPL01c25s2/signal;
in8: rDPL01c26s2/signal;

out1: rackLAND2c1s7u1/in8;
out2: rackLAND2c1s7u1/in7;
out3: rackLAND2c1s7u1/in6;
out4: rackLAND2c1s7u1/in5;
out5: rackLAND2c1s7u1/in4;
out6: rackLAND2c1s7u1/in3;
out7: rackLAND2c1s7u1/in2;
out8: rackLAND2c1s7u1/in1;
}

JOINER_8(rDPL01c214)
{
in1: rDPL01c27s2/signal;
in2: rDPL01c28s2/signal;
in3: rDPL01c29s2/signal;
in4: rDPL01c30s2/signal;
/* spare channels:
in5: ;
in6: ;
in7: ;
in8: ;S
*/
out1: rackLAND2c1s7u1/in16;
out2: rackLAND2c1s7u1/in15;
out3: rackLAND2c1s7u1/in14;
out4: rackLAND2c1s7u1/in13;
/*
out5: rackLAND2c1s7u1/in12;
out6: rackLAND2c1s7u1/in11;
out7: rackLAND2c1s7u1/in10;
out8: rackLAND2c1s7u1/in9;
*/
}


JOINER_8(rDPL01c215)
{
in1: rDPL01c29s1/signal;
in2: rDPL01c30s1/signal;
in3: rDPL01c31s1/signal;
in4: rDPL01c32s1/signal;
in5: rDPL01c33s1/signal;
in6: rDPL01c34s1/signal;
in7: rDPL01c35s1/signal;
in8: rDPL01c36s1/signal;

out1: rackNEULAND1c3s8u1/in8;
out2: rackNEULAND1c3s8u1/in7;
out3: rackNEULAND1c3s8u1/in6;
out4: rackNEULAND1c3s8u1/in5;
out5: rackNEULAND1c3s8u1/in4;
out6: rackNEULAND1c3s8u1/in3;
out7: rackNEULAND1c3s8u1/in2;
out8: rackNEULAND1c3s8u1/in1;
}

JOINER_8(rDPL01c216)
{
in1: rDPL01c37s1/signal;
in2: rDPL01c38s1/signal;
in3: rDPL01c39s1/signal;
in4: rDPL01c40s1/signal;
in5: rDPL01c41s1/signal;
in6: rDPL01c42s1/signal;
in7: rDPL01c43s1/signal;
in8: rDPL01c44s1/signal;

out1: rackNEULAND1c3s8u1/in16;
out2: rackNEULAND1c3s8u1/in15;
out3: rackNEULAND1c3s8u1/in14;
out4: rackNEULAND1c3s8u1/in13;
out5: rackNEULAND1c3s8u1/in12;
out6: rackNEULAND1c3s8u1/in11;
out7: rackNEULAND1c3s8u1/in10;
out8: rackNEULAND1c3s8u1/in9;
}

JOINER_8(rDPL01c217)
{
in1: rDPL01c45s1/signal;
in2: rDPL01c46s1/signal;
in3: rDPL01c47s1/signal;
in4: rDPL01c48s1/signal;
in5: rDPL01c49s1/signal;
in6: rDPL01c50s1/signal;

in7: rDPL01c51s2/signal;
in8: rDPL01c52s2/signal;

out1: rackNEULAND1c3s9u1/in8;
out2: rackNEULAND1c3s9u1/in7;
out3: rackNEULAND1c3s9u1/in6;
out4: rackNEULAND1c3s9u1/in5;
out5: rackNEULAND1c3s9u1/in4;
out6: rackNEULAND1c3s9u1/in3;
out7: rackNEULAND1c3s9u1/in2;
out8: rackNEULAND1c3s9u1/in1;
}
JOINER_8(rDPL01c218)
{
in1: rDPL01c53s2/signal;
in2: rDPL01c54s2/signal;
in3: rDPL01c55s2/signal;
in4: rDPL01c56s2/signal;
in5: rDPL01c57s2/signal;
in6: rDPL01c58s2/signal;
in7: rDPL01c59s2/signal;
in8: rDPL01c60s2/signal;

out1: rackNEULAND1c3s9u1/in16;
out2: rackNEULAND1c3s9u1/in15;
out3: rackNEULAND1c3s9u1/in14;
out4: rackNEULAND1c3s9u1/in13;
out5: rackNEULAND1c3s9u1/in12;
out6: rackNEULAND1c3s9u1/in11;
out7: rackNEULAND1c3s9u1/in10;
out8: rackNEULAND1c3s9u1/in9;
}

JOINER_8(rDPL01c219)
{
in1: rDPL01c61s2/signal;
in2: rDPL01c62s2/signal;
in3: rDPL01c63s2/signal;
in4: rDPL01c64s2/signal;
in5: rDPL01c65s2/signal;
in6: rDPL01c66s2/signal;
in7: rDPL01c67s2/signal;
in8: rDPL01c68s2/signal;

out1: rackNEULAND1c3s10u1/in8;
out2: rackNEULAND1c3s10u1/in7;
out3: rackNEULAND1c3s10u1/in6;
out4: rackNEULAND1c3s10u1/in5;
out5: rackNEULAND1c3s10u1/in4;
out6: rackNEULAND1c3s10u1/in3;
out7: rackNEULAND1c3s10u1/in2;
out8: rackNEULAND1c3s10u1/in1;
}

JOINER_8(rDPL01c220)
{
in1: rDPL01c69s2/signal;
in2: rDPL01c70s2/signal;
in3: rDPL01c71s2/signal;
in4: rDPL01c72s2/signal;
in5: rDPL01c73s2/signal;
in6: rDPL01c74s2/signal;
in7: rDPL01c75s2/signal;
in8: rDPL01c76s2/signal;

out1: rackNEULAND1c3s10u1/in16;
out2: rackNEULAND1c3s10u1/in15;
out3: rackNEULAND1c3s10u1/in14;
out4: rackNEULAND1c3s10u1/in13;
out5: rackNEULAND1c3s10u1/in12;
out6: rackNEULAND1c3s10u1/in11;
out7: rackNEULAND1c3s10u1/in10;
out8: rackNEULAND1c3s10u1/in9;
}
JOINER_8(rDPL01c221)
{
in1: rDPL01c77s2/signal;
in2: rDPL01c78s2/signal;
in3: rDPL01c79s2/signal;
in4: rDPL01c80s2/signal;
in5: rDPL01c81s2/signal;
in6: rDPL01c82s2/signal;
in7: rDPL01c83s2/signal;
in8: rDPL01c84s2/signal;

out1: rackNEULAND1c3s11u1/in8;
out2: rackNEULAND1c3s11u1/in7;
out3: rackNEULAND1c3s11u1/in6;
out4: rackNEULAND1c3s11u1/in5;
out5: rackNEULAND1c3s11u1/in4;
out6: rackNEULAND1c3s11u1/in3;
out7: rackNEULAND1c3s11u1/in2;
out8: rackNEULAND1c3s11u1/in1;
}

JOINER_8(rDPL01c222)
{
in1: rDPL01c85s2/signal;
in2: rDPL01c86s2/signal;
in3: rDPL01c87s2/signal;
in4: rDPL01c88s2/signal;
in5: rDPL01c89s2/signal;
in6: rDPL01c90s2/signal;
in7: rDPL01c91s2/signal;
in8: rDPL01c92s2/signal;


out1: rackNEULAND1c3s11u1/in16;
out2: rackNEULAND1c3s11u1/in15;
out3: rackNEULAND1c3s11u1/in14;
out4: rackNEULAND1c3s11u1/in13;
out5: rackNEULAND1c3s11u1/in12;
out6: rackNEULAND1c3s11u1/in11;
out7: rackNEULAND1c3s11u1/in10;
out8: rackNEULAND1c3s11u1/in9;
}

JOINER_8(rDPL01c223)
{
in1: rDPL01c93s2/signal;
in2: rDPL01c94s2/signal;
in3: rDPL01c95s2/signal;
in4: rDPL01c96s2/signal;
in5: rDPL01c97s2/signal;
in6: rDPL01c98s2/signal;
in7: rDPL01c99s2/signal;
in8: rDPL01c100s2/signal;

out1: rackNEULAND1c3s12u1/in8;
out2: rackNEULAND1c3s12u1/in7;
out3: rackNEULAND1c3s12u1/in6;
out4: rackNEULAND1c3s12u1/in5;
out5: rackNEULAND1c3s12u1/in4;
out6: rackNEULAND1c3s12u1/in3;
out7: rackNEULAND1c3s12u1/in2;
out8: rackNEULAND1c3s12u1/in1;
}

JOINER_8(rDPL01c224)
{
in1: rDPL01c31s2/signal;
in2: rDPL01c32s2/signal;
in3: rDPL01c33s2/signal;
in4: rDPL01c34s2/signal;
in5: rDPL01c35s2/signal;
in6: rDPL01c36s2/signal;
in7: rDPL01c37s2/signal;
in8: rDPL01c38s2/signal;

out1: rackNEULAND1c3s12u1/in16;
out2: rackNEULAND1c3s12u1/in15;
out3: rackNEULAND1c3s12u1/in14;
out4: rackNEULAND1c3s12u1/in13;
out5: rackNEULAND1c3s12u1/in12;
out6: rackNEULAND1c3s12u1/in11;
out7: rackNEULAND1c3s12u1/in10;
out8: rackNEULAND1c3s12u1/in9;
}

JOINER_8(rDPL01c225)
{
in1: rDPL01c39s2/signal;
in2: rDPL01c40s2/signal;
in3: rDPL01c41s2/signal;
in4: rDPL01c42s2/signal;
in5: rDPL01c43s2/signal;
in6: rDPL01c44s2/signal;
in7: rDPL01c45s2/signal;
in8: rDPL01c46s2/signal;

out1: rackNEULAND1c3s13u1/in8;
out2: rackNEULAND1c3s13u1/in7;
out3: rackNEULAND1c3s13u1/in6;
out4: rackNEULAND1c3s13u1/in5;
out5: rackNEULAND1c3s13u1/in4;
out6: rackNEULAND1c3s13u1/in3;
out7: rackNEULAND1c3s13u1/in2;
out8: rackNEULAND1c3s13u1/in1;
}
JOINER_8(rDPL01c226)
{
in1: rDPL01c47s2/signal;
in2: rDPL01c48s2/signal;
in3: rDPL01c49s2/signal;
in4: rDPL01c50s2/signal;
// spares:
in5: ;
in6: ;
in7: ;
in8: ;

out1: rackNEULAND1c3s13u1/in16;
out2: rackNEULAND1c3s13u1/in15;
out3: rackNEULAND1c3s13u1/in14;
out4: rackNEULAND1c3s13u1/in13;
/*
out5: rackNEULAND1c3s13u1/in12;
out6: rackNEULAND1c3s13u1/in11;
out7: rackNEULAND1c3s13u1/in10;
out8: rackNEULAND1c3s13u1/in9;
*/
}




//HV
// syntax with respect to pdf HV.Cable
//
// 3XY: XY = (HV times 2)-1 for cable 1 to 25, XY = HV times 2 for cable 26 to 50
//
// "1.1-1.25"
HV_CONN_28(rDPL01c301)
{
 
 in1_25: rackNEULANDHVc4s8/out0_24;
 
 out1:  rDPL01c01s1/hv; 
 out2:  rDPL01c02s1/hv;
 out3:  rDPL01c03s1/hv; 
 out4:  rDPL01c04s1/hv;
 out5:  rDPL01c05s1/hv; 
 out6:  rDPL01c06s1/hv;
 out7:  rDPL01c07s1/hv; 
 out8:  rDPL01c08s1/hv;
 
 out9:  rDPL01c09s1/hv; 
 out10:  rDPL01c10s1/hv;
 out11:  rDPL01c11s1/hv; 
 out12:  rDPL01c12s1/hv;
 out13:  rDPL01c13s1/hv; 
 out14:  rDPL01c14s1/hv;
 out15:  rDPL01c15s1/hv; 
 out16:  rDPL01c16s1/hv;

 out17:  rDPL01c17s1/hv; 
 out18:  rDPL01c18s1/hv;
 out19:  rDPL01c19s1/hv; 
 out20:  rDPL01c20s1/hv;
 out21:  rDPL01c21s1/hv; 
 out22:  rDPL01c22s1/hv;
 out23:  rDPL01c23s1/hv; 
 out24:  rDPL01c24s1/hv;
 
 out25:  rDPL01c25s1/hv;
}

// "1.26-1.50"
HV_CONN_28(rDPL01c302)
{
 
 in1_25: rackNEULANDHVc4s10/out0_24;
 
 out1:  rDPL01c51s1/hv; 
 out2:  rDPL01c52s1/hv;
 out3:  rDPL01c53s1/hv; 
 out4:  rDPL01c54s1/hv;
 out5:  rDPL01c55s1/hv; 
 out6:  rDPL01c56s1/hv;
 out7:  rDPL01c57s1/hv; 
 out8:  rDPL01c58s1/hv;
 
 out9:  rDPL01c59s1/hv; 
 out10:  rDPL01c60s1/hv;
 out11:  rDPL01c61s1/hv; 
 out12:  rDPL01c62s1/hv;
 out13:  rDPL01c63s1/hv; 
 out14:  rDPL01c64s1/hv;
 out15:  rDPL01c65s1/hv; 
 out16:  rDPL01c66s1/hv;

 out17:  rDPL01c67s1/hv; 
 out18:  rDPL01c68s1/hv;
 out19:  rDPL01c69s1/hv; 
 out20:  rDPL01c70s1/hv;
 out21:  rDPL01c71s1/hv; 
 out22:  rDPL01c72s1/hv;
 out23:  rDPL01c73s1/hv; 
 out24:  rDPL01c74s1/hv;
 
 out25:  rDPL01c75s1/hv;
}

// "2.1-2.25"
HV_CONN_28(rDPL01c303)
{
 
 in1_25: rackLAND1c4s8/out0_24;
 
 out1:  rDPL01c26s1/hv; 
 out2:  rDPL01c27s1/hv;
 out3:  rDPL01c28s1/hv; 
 out4:  rDPL01c29s1/hv;
 out5:  rDPL01c30s1/hv; 
 out6:  rDPL01c31s1/hv;
 out7:  rDPL01c32s1/hv; 
 out8:  rDPL01c33s1/hv;
 out9:  rDPL01c34s1/hv; 
 out10:  rDPL01c35s1/hv;
 out11:  rDPL01c36s1/hv; 
 out12:  rDPL01c37s1/hv;
 out13:  rDPL01c38s1/hv; 
 out14:  rDPL01c39s1/hv;
 out15:  rDPL01c40s1/hv; 
 out16:  rDPL01c41s1/hv;
 out17:  rDPL01c42s1/hv; 
 out18:  rDPL01c43s1/hv;
 out19:  rDPL01c44s1/hv; 
 out20:  rDPL01c45s1/hv;
 out21:  rDPL01c46s1/hv; 
 out22:  rDPL01c47s1/hv;
 out23:  rDPL01c48s1/hv; 
 out24:  rDPL01c49s1/hv;
 out25:  rDPL01c50s1/hv;
}

// "2.26-2.50"
HV_CONN_28(rDPL01c304)
{
 
 in1_25: rackLAND1c4s10/out0_24;
 
 out1:  rDPL01c51s2/hv; 
 out2:  rDPL01c52s2/hv;
 out3:  rDPL01c53s2/hv; 
 out4:  rDPL01c54s2/hv;
 out5:  rDPL01c55s2/hv; 
 out6:  rDPL01c56s2/hv;
 out7:  rDPL01c57s2/hv; 
 out8:  rDPL01c58s2/hv;
 
 out9:  rDPL01c59s2/hv; 
 out10:  rDPL01c60s2/hv;
 out11:  rDPL01c61s2/hv; 
 out12:  rDPL01c62s2/hv;
 out13:  rDPL01c63s2/hv; 
 out14:  rDPL01c64s2/hv;
 out15:  rDPL01c65s2/hv; 
 out16:  rDPL01c66s2/hv;

 out17:  rDPL01c67s2/hv; 
 out18:  rDPL01c68s2/hv;
 out19:  rDPL01c69s2/hv; 
 out20:  rDPL01c70s2/hv;
 out21:  rDPL01c71s2/hv; 
 out22:  rDPL01c72s2/hv;
 out23:  rDPL01c73s2/hv; 
 out24:  rDPL01c74s2/hv;
 
 out25:  rDPL01c75s2/hv;
 }

// "3.1-3.25"
HV_CONN_28(rDPL01c305)
{
 
 in1_25: rackLAND1c4s12/out0_24;
 
 out1:  rDPL01c26s2/hv; 
 out2:  rDPL01c27s2/hv;
 out3:  rDPL01c28s2/hv; 
 out4:  rDPL01c29s2/hv;
 out5:  rDPL01c30s2/hv; 
 out6:  rDPL01c31s2/hv;
 out7:  rDPL01c32s2/hv; 
 out8:  rDPL01c33s2/hv;
 out9:  rDPL01c34s2/hv; 
 out10:  rDPL01c35s2/hv;
 out11:  rDPL01c36s2/hv; 
 out12:  rDPL01c37s2/hv;
 out13:  rDPL01c38s2/hv; 
 out14:  rDPL01c39s2/hv;
 out15:  rDPL01c40s2/hv; 
 out16:  rDPL01c41s2/hv;
 out17:  rDPL01c42s2/hv; 
 out18:  rDPL01c43s2/hv;
 out19:  rDPL01c44s2/hv; 
 out20:  rDPL01c45s2/hv;
 out21:  rDPL01c46s2/hv; 
 out22:  rDPL01c47s2/hv;
 out23:  rDPL01c48s2/hv; 
 out24:  rDPL01c49s2/hv;
 out25:  rDPL01c50s2/hv;
}
// "3.26-3.50"
HV_CONN_28(rDPL01c306)
{
 
 in1_25: rackLAND1c4s14/out0_24;
 
 out1:  rDPL01c76s2/hv; 
 out2:  rDPL01c77s2/hv;
 out3:  rDPL01c78s2/hv; 
 out4:  rDPL01c79s2/hv;
 out5:  rDPL01c80s2/hv; 
 out6:  rDPL01c81s2/hv;
 out7:  rDPL01c82s2/hv; 
 out8:  rDPL01c83s2/hv;
 out9:  rDPL01c84s2/hv; 
 out10:  rDPL01c85s2/hv;
 out11:  rDPL01c86s2/hv; 
 out12:  rDPL01c87s2/hv;
 out13:  rDPL01c88s2/hv; 
 out14:  rDPL01c89s2/hv;
 out15:  rDPL01c90s2/hv; 
 out16:  rDPL01c91s2/hv;
 out17:  rDPL01c92s2/hv; 
 out18:  rDPL01c93s2/hv;
 out19:  rDPL01c94s2/hv; 
 out20:  rDPL01c95s2/hv;
 out21:  rDPL01c96s2/hv; 
 out22:  rDPL01c97s2/hv;
 out23:  rDPL01c98s2/hv; 
 out24:  rDPL01c99s2/hv;
 out25:  rDPL01c100s2/hv;
}

// "4.1-4.25"
HV_CONN_28(rDPL01c307)
{
 
 in1_25: rackNEULANDHVc4s12/out0_24;
 
 out1:  rDPL01c01s2/hv; 
 out2:  rDPL01c02s2/hv;
 out3:  rDPL01c03s2/hv; 
 out4:  rDPL01c04s2/hv;
 out5:  rDPL01c05s2/hv; 
 out6:  rDPL01c06s2/hv;
 out7:  rDPL01c07s2/hv; 
 out8:  rDPL01c08s2/hv;
 
 out9:  rDPL01c09s2/hv; 
 out10:  rDPL01c10s2/hv;
 out11:  rDPL01c11s2/hv; 
 out12:  rDPL01c12s2/hv;
 out13:  rDPL01c13s2/hv; 
 out14:  rDPL01c14s2/hv;
 out15:  rDPL01c15s2/hv; 
 out16:  rDPL01c16s2/hv;

 out17:  rDPL01c17s2/hv; 
 out18:  rDPL01c18s2/hv;
 out19:  rDPL01c19s2/hv; 
 out20:  rDPL01c20s2/hv;
 out21:  rDPL01c21s2/hv; 
 out22:  rDPL01c22s2/hv;
 out23:  rDPL01c23s2/hv; 
 out24:  rDPL01c24s2/hv;
 
 out25:  rDPL01c25s2/hv;
}
// "4.26-4.50"
HV_CONN_28(rDPL01c308)
{
 
 in1_25: rackNEULANDHVc4s14/out0_24;
 
 out1:  rDPL01c76s1/hv; 
 out2:  rDPL01c77s1/hv;
 out3:  rDPL01c78s1/hv; 
 out4:  rDPL01c79s1/hv;
 out5:  rDPL01c80s1/hv; 
 out6:  rDPL01c81s1/hv;
 out7:  rDPL01c82s1/hv; 
 out8:  rDPL01c83s1/hv;
 out9:  rDPL01c84s1/hv; 
 out10:  rDPL01c85s1/hv;
 out11:  rDPL01c86s1/hv; 
 out12:  rDPL01c87s1/hv;
 out13:  rDPL01c88s1/hv; 
 out14:  rDPL01c89s1/hv;
 out15:  rDPL01c90s1/hv; 
 out16:  rDPL01c91s1/hv;
 out17:  rDPL01c92s1/hv; 
 out18:  rDPL01c93s1/hv;
 out19:  rDPL01c94s1/hv; 
 out20:  rDPL01c95s1/hv;
 out21:  rDPL01c96s1/hv; 
 out22:  rDPL01c97s1/hv;
 out23:  rDPL01c98s1/hv; 
 out24:  rDPL01c99s1/hv;
 out25:  rDPL01c100s1/hv;
}

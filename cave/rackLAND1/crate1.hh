VME_CRATE(rackLAND1c1)
{
  SETTING("CRATE_NO" => "1");
}

RIO4(rackLAND1c1s1)
{
  SETTING("HOSTNAME" => "r3-14");
}

ENV1(rackLAND1c1s8)
{
out_ecl1: rackLAND1c1s13/in_ecl1;
out_ecl2: rackLAND1c1s15/in_ecl1;

nim_in5: rackLAND1c2s3u1/out2; //?

nim_out5: .s/nim_in13; // ecl cannot be distributed to 3 SAMS
nim_in13: .s/nim_out5;

}

SAM5(rackLAND1c1s11)
{
  SETTING("SAM_NO" => "5");
in_ecl1: rackLAND1c1s13/out_ecl1;

//gtb1: rackLAND2c3s1/gtb_master;  // only one tacquila in the crate
gtb1: rackLAND2c3s8/gtb_master;
gtb2: rackNEULAND1c1s13/gtb_master;

}

SAM5(rackLAND1c1s13)
{
  SETTING("SAM_NO" => "6");
in_ecl1: rackLAND1c1s8/out_ecl1;
out_ecl1: rackLAND1c1s11/in_ecl1;

gtb1: rackLAND2c1s10/gtb_master;
gtb2: rackLAND2c2s10/gtb_master;
}

SAM4(rackLAND1c1s15)
{
  SETTING("SAM_NO" => "7");
// ENV output still needs to be defined here!
in_ecl1: .s8/out_ecl2;

gtb1: rackNEULAND1c4s8/gtb_master;
gtb2: rackNEULAND1c3s8/gtb_master;
}

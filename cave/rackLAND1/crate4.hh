CAENSY1527(rackLAND1c4)
{
 LABEL("landhv02");
 SETTING("HOSTNAME"=>"Tjosanhejsan");
}
//
//DPL03:
//
CAENA1733(rackLAND1c4s0)  
{ 
LABEL("slot0");
out0_24: rDPL03c303/in1_25; 
}

CAENA1733(rackLAND1c4s2)  
{ 
LABEL("slot2");
out0_24: rDPL03c304/in1_25; 
}

CAENA1733(rackLAND1c4s4)  
{ 
LABEL("slot4");
out0_24: rDPL03c305/in1_25; 
}
CAENA1733(rackLAND1c4s6)  
{ 
LABEL("slot6");
out0_24: rDPL03c306/in1_25; 
}
//
// DPL01:
//
CAENA1733(rackLAND1c4s8)  
{ 
LABEL("slot8");
out0_24: rDPL01c303/in1_25; 
}
CAENA1733(rackLAND1c4s10)  
{ 
LABEL("slot10");
out0_24: rDPL01c304/in1_25; 
}
CAENA1733(rackLAND1c4s12)  
{ 
LABEL("slot12");
out0_24: rDPL01c305/in1_25; 
}
CAENA1733(rackLAND1c4s14)  
{ 
LABEL("slot14");
out0_24: rDPL01c306/in1_25; 
}

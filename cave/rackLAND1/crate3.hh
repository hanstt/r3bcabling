CAENSY1527(rackLAND1c3)
{
 LABEL("landhv01");
 SETTING("HOSTNAME"=>"Tjosanhejsan");
}
//
//DPL04:
//
CAENA1733(rackLAND1c3s0)  
{ 
LABEL("slot0");
out0_24: rDPL04c303/in1_25; 
}

CAENA1733(rackLAND1c3s2)  
{ 
LABEL("slot2");
out0_24: rDPL04c304/in1_25; 
}

CAENA1733(rackLAND1c3s4)  
{ 
LABEL("slot4");
out0_24: rDPL04c305/in1_25; 
}
CAENA1733(rackLAND1c3s6)  
{ 
LABEL("slot6");
out0_24: rDPL04c306/in1_25; 
}
//
// DPL02:
//
CAENA1733(rackLAND1c3s8)  
{ 
  LABEL("slot8");
 out0_24: rDPL02c303/in1_25; 
}
CAENA1733(rackLAND1c3s10)  
{ 
  LABEL("slot10");
 out0_24: rDPL02c304/in1_25; 
}
CAENA1733(rackLAND1c3s12)  
{ 
  LABEL("slot12");
 out0_24: rDPL02c305/in1_25; 
}
CAENA1733(rackLAND1c3s14)  
{ 
  LABEL("slot14");
 out0_24: rDPL02c306/in1_25; 
}


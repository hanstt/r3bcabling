LANDFEE2(rackNEULAND2c2s8u1)   // slot 8
{
 in1: rDPL01c215/out8;
 in2: rDPL01c215/out7;
 in3: rDPL01c215/out6;
 in4: rDPL01c215/out5;
 in5: rDPL01c215/out4;
 in6: rDPL01c215/out3;
 in7: rDPL01c215/out2;
 in8: rDPL01c215/out1;
 in9:  rDPL01c216/out8;
 in10: rDPL01c216/out7;
 in11: rDPL01c216/out6;
 in12: rDPL01c216/out5;
 in13: rDPL01c216/out4;
 in14: rDPL01c216/out3;
 in15: rDPL01c216/out2;
 in16: rDPL01c216/out1;

  //gtb_out1_16: r54c1s15/gtb;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}

LANDFEE2(rackNEULAND2c2s9u1)   // slot 9
{
 in1: rDPL01c217/out8;
 in2: rDPL01c217/out7;
 in3: rDPL01c217/out6;
 in4: rDPL01c217/out5;
 in5: rDPL01c217/out4;
 in6: rDPL01c217/out3;
 in7: rDPL01c217/out2;
 in8: rDPL01c217/out1;
 in9:  rDPL01c218/out8;
 in10: rDPL01c218/out7;
 in11: rDPL01c218/out6;
 in12: rDPL01c218/out5;
 in13: rDPL01c218/out4;
 in14: rDPL01c218/out3;
 in15: rDPL01c218/out2;
 in16: rDPL01c218/out1;

  //gtb_out1_16: r54c1s15/gtb;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}

LANDFEE2(rackNEULAND2c2s10u1)   // slot 10
{
 in1: rDPL01c219/out8;
 in2: rDPL01c219/out7;
 in3: rDPL01c219/out6;
 in4: rDPL01c219/out5;
 in5: rDPL01c219/out4;
 in6: rDPL01c219/out3;
 in7: rDPL01c219/out2;
 in8: rDPL01c219/out1;
 in9:  rDPL01c220/out8;
 in10: rDPL01c220/out7;
 in11: rDPL01c220/out6;
 in12: rDPL01c220/out5;
 in13: rDPL01c220/out4;
 in14: rDPL01c220/out3;
 in15: rDPL01c220/out2;
 in16: rDPL01c220/out1;

  //gtb_out1_16: r54c1s15/gtb;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}

//                              who is its master, slave, who is connected to its upj, downj5 and downj7?

#define TRIPLEX2_TACQ_QDC2_TACQUILA3(rcs,gtbmaster,gtbslave,upj,downj5,downj7,qserial,tserial) \
                                                \
  TRIPLEX2(rcs##u2)                             \
  {                                             \
    in1_16:  .u1/e1_16;                         \
    out1_16: .u3/in1_16;                        \
                                                \
    j6: upj;                                    \
    j5: downj5;                                 \
    j7: downj7;                                 \
  }                                             \
                                                \
  TACQ_QDC2(rcs##u3)                            \
  {                                             \
    /* nonhack : SERIAL(qserial);*/             \
    /* hack */                                  \
    SERIAL(tserial);                            \
    /* endhack */                               \
    in1_16:  .u2/out1_16;                       \
  }                                             \
                                                \
  TACQUILA3(rcs)                                \
  {                                             \
    SERIAL(tserial);                            \
    in1_16: .u1/t1_16;                          \
    /* hack */                                  \
    out1: .s/hackin17;                          \
    hackin17: .s/out1;                          \
    /* endhack */                               \
    gtb_master: gtbmaster;                      \
    gtb_slave:  gtbslave;                       \
  }
 
 TRIPLEX2_TACQ_QDC2_TACQUILA3(rackNEULAND2c2s8,.s9/gtb_slave,,.c1s11u2/j5,.s10u2/j6,.s9u2/j6,"SAM7_GTB1_TAC01","SAM7_GTB1_TAC01"); 
 TRIPLEX2_TACQ_QDC2_TACQUILA3(rackNEULAND2c2s9,.s10/gtb_slave,.s8/gtb_master,.s8u2/j7,,,"SAM7_GTB1_TAC02","SAM7_GTB1_TAC02");
 TRIPLEX2_TACQ_QDC2_TACQUILA3(rackNEULAND2c2s10,.c1s11/gtb_slave,.s9/gtb_master,.s8u2/j5,,,"SAM7_GTB1_TAC03","SAM7_GTB1_TAC03");

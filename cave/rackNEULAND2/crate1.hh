LANDFEE2(rackNEULAND2c1s11u1)   // slot 11
{
 in1: rDPL01c221/out8;
 in2: rDPL01c221/out7;
 in3: rDPL01c221/out6;
 in4: rDPL01c221/out5;
 in5: rDPL01c221/out4;
 in6: rDPL01c221/out3;
 in7: rDPL01c221/out2;
 in8: rDPL01c221/out1;

 in9:  rDPL01c222/out8;
 in10: rDPL01c222/out7;
 in11: rDPL01c222/out6;
 in12: rDPL01c222/out5;
 in13: rDPL01c222/out4;
 in14: rDPL01c222/out3;
 in15: rDPL01c222/out2;
 in16: rDPL01c222/out1;

  //gtb_out1_16: r54c1s15/gtb;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}

LANDFEE2(rackNEULAND2c1s12u1)   // slot 12
{
 in1: rDPL01c223/out8;
 in2: rDPL01c223/out7;
 in3: rDPL01c223/out6;
 in4: rDPL01c223/out5;
 in5: rDPL01c223/out4;
 in6: rDPL01c223/out3;
 in7: rDPL01c223/out2;
 in8: rDPL01c223/out1;

 in9:  rDPL01c224/out8;
 in10: rDPL01c224/out7;
 in11: rDPL01c224/out6;
 in12: rDPL01c224/out5;
 in13: rDPL01c224/out4;
 in14: rDPL01c224/out3;
 in15: rDPL01c224/out2;
 in16: rDPL01c224/out1;

  //gtb_out1_16: r54c1s15/gtb;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}

LANDFEE2(rackNEULAND2c1s13u1)   // slot 13
{
 in1: rDPL01c225/out8;
 in2: rDPL01c225/out7;
 in3: rDPL01c225/out6;
 in4: rDPL01c225/out5;
 in5: rDPL01c225/out4;
 in6: rDPL01c225/out3;
 in7: rDPL01c225/out2;
 in8: rDPL01c225/out1;
/*
 in9:  rDPL01c226/out8;
 in10: rDPL01c226/out7;
 in11: rDPL01c226/out6;
 in12: rDPL01c226/out5;
*/
 in13: rDPL01c226/out4;
 in14: rDPL01c226/out3;
 in15: rDPL01c226/out2;
 in16: rDPL01c226/out1;

  //gtb_out1_16: r54c1s15/gtb;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}


//                              who is its master, slave, who is connected to its upj, downj5 and downj7?

#define TRIPLEX2_TACQ_QDC2_TACQUILA3(rcs,gtbmaster,gtbslave,upj,downj5,downj7,qserial,tserial) \
                                                \
  TRIPLEX2(rcs##u2)                             \
  {                                             \
    in1_16:  .u1/e1_16;                         \
    out1_16: .u3/in1_16;                        \
                                                \
    j6: upj;                                    \
    j5: downj5;                                 \
    j7: downj7;                                 \
  }                                             \
                                                \
  TACQ_QDC2(rcs##u3)                            \
  {                                             \
    /* nonhack : SERIAL(qserial);*/             \
    /* hack */                                  \
    SERIAL(tserial);                            \
    /* endhack */                               \
    in1_16:  .u2/out1_16;                       \
  }                                             \
                                                \
  TACQUILA3(rcs)                                \
  {                                             \
    SERIAL(tserial);                            \
    in1_16: .u1/t1_16;                          \
    /* hack */                                  \
    out1: .s/hackin17;                          \
    hackin17: .s/out1;                          \
    /* endhack */                               \
    gtb_master: gtbmaster;                      \
    gtb_slave:  gtbslave;                       \
  }
 
 TRIPLEX2_TACQ_QDC2_TACQUILA3(rackNEULAND2c1s11,.s12/gtb_slave,.c2s10/gtb_master,rackNEULAND1c4s4u2/j7,rackNEULAND2c2s8u2/j6,.s12u2/j6,"SAM7_GTB1_TAC04","SAM7_GTB1_TAC04");
 TRIPLEX2_TACQ_QDC2_TACQUILA3(rackNEULAND2c1s12,.s13/gtb_slave,.s11/gtb_master,.s11u2/j7,,.s13u2/j6,"SAM7_GTB1_TAC05","SAM7_GTB1_TAC05");
 TRIPLEX2_TACQ_QDC2_TACQUILA3(rackNEULAND2c1s13,rackNEULAND1c2s11/gtb1,.s12/gtb_master,.s12u2/j7,,,"SAM7_GTB1_TAC06","SAM7_GTB1_TAC06");


MODULE(DETECTOR_ACTAR)
{
  LABEL("DETECTOR_ACTAR");
  LEMO_OUTPUT(out1_66);
  //CONECTOR(out1_66,SIGNAL);
}

//-------------- ACTAR Detector (?rack 1000 / crate 0 / slot 0?) ---------------//

DETECTOR_ACTAR(r1000c0s0) {
out1:  rACTARc0s4/in1   ; 
out2:  rACTARc0s4/in2   ; 
out3:  rACTARc0s4/in3   ; 
out4:  rACTARc0s4/in4   ; 
out5:  rACTARc0s4/in5   ; 
out6:  rACTARc0s4/in6   ; 
out7:  rACTARc0s4/in7   ; 
out8:  rACTARc0s4/in8   ; 
out9:  rACTARc0s4/in9   ; 
out10: rACTARc0s4/in10  ; 
out11: rACTARc0s4/in11  ; 
out12: rACTARc0s4/in12  ; 
out13: rACTARc0s4/in13  ; 
out14: rACTARc0s4/in14  ; 
out15: rACTARc0s4/in15  ; 
out16: rACTARc0s4/in16  ;

out17: rACTARc0s6/in1   ;
out18: rACTARc0s6/in2   ;
out19: rACTARc0s6/in3   ;
out20: rACTARc0s6/in4   ;
out21: rACTARc0s6/in5   ;
out22: rACTARc0s6/in6   ;
out23: rACTARc0s6/in7   ;
out24: rACTARc0s6/in8   ;
out25: rACTARc0s6/in9   ;
out26: rACTARc0s6/in10  ;
out27: rACTARc0s6/in11  ;
out28: rACTARc0s6/in12  ;
out29: rACTARc0s6/in13  ;
out30: rACTARc0s6/in14  ;
out31: rACTARc0s6/in15  ;
out32: rACTARc0s6/in16  ;

out33: rACTARc0s8/in1   ;
out34: rACTARc0s8/in2   ;
out35: rACTARc0s8/in3   ;
out36: rACTARc0s8/in4   ;
out37: rACTARc0s8/in5   ;
out38: rACTARc0s8/in6   ;
out39: rACTARc0s8/in7   ;
out40: rACTARc0s8/in8   ;
out41: rACTARc0s8/in9   ;
out42: rACTARc0s8/in10  ;
out43: rACTARc0s8/in11  ;
out44: rACTARc0s8/in12  ;
out45: rACTARc0s8/in13  ;
out46: rACTARc0s8/in14  ;
out47: rACTARc0s8/in15  ;
out48: rACTARc0s8/in16  ;

out49: rACTARc0s10/in1   ;
out50: rACTARc0s10/in2   ;
out51: rACTARc0s10/in3   ;
out52: rACTARc0s10/in4   ;
out53: rACTARc0s10/in5   ;
out54: rACTARc0s10/in6   ;
out55: rACTARc0s10/in7   ;
out56: rACTARc0s10/in8   ;
out57: rACTARc0s10/in9   ;
out58: rACTARc0s10/in10  ;
out59: rACTARc0s10/in11  ;
out60: rACTARc0s10/in12  ;
out61: rACTARc0s10/in13  ;
out62: rACTARc0s10/in14  ;
out63: rACTARc0s10/in15  ;
out64: rACTARc0s10/in16  ;

out65: rACTARc0s12/in1   ;
out66: rACTARc0s12/in2   ;
}


//--------------- VME CRATE -------------//
VME_CRATE(rACTARc0)
{
  SETTING("CRATE_NO" => "9");
}

RIO4(rACTARc0s1) 
{
  SETTING("HOSTNAME"=>"r4-49");
}

TRIDI(rACTARc0s2) // IN8 HAS TO BE CHECKED !!!
{
in_lemo1:   rACTARc0s4/to;
in_lemo2:   rACTARc0s6/to;
in_lemo3:   rACTARc0s8/to;
in_lemo4:   rACTARc0s10/to;
in_lemo5:   rMASTERc2s12u1/out1;
in_lemo6:   rPOS1c1s1u1/out8;
in_lemo7:   rMASTERc2s1u4/out3;
in_lemo8:   rMASTERc2s11u1/out2;

out_lemo1:  rACTARc0s4/ti;
}

SIS3316(rACTARc0s4)             //ADC1
{
in1:    r1000c0s0/out1;
in2:    r1000c0s0/out2;
in3:    r1000c0s0/out3;
in4:    r1000c0s0/out4;
in5:    r1000c0s0/out5;
in6:    r1000c0s0/out6;
in7:    r1000c0s0/out7;
in8:    r1000c0s0/out8;

in9:    r1000c0s0/out9;
in10:   r1000c0s0/out10;
in11:   r1000c0s0/out11;
in12:   r1000c0s0/out12;
in13:   r1000c0s0/out13;
in14:   r1000c0s0/out14;
in15:   r1000c0s0/out15;
in16:   r1000c0s0/out16;

to:  rACTARc0s2/in_lemo1;
ti:  rACTARc0s2/out_lemo1;
}

SIS3316(rACTARc0s6)             //ADC2
{
in1:    r1000c0s0/out17;
in2:    r1000c0s0/out18;
in3:    r1000c0s0/out19;
in4:    r1000c0s0/out20;
in5:    r1000c0s0/out21;
in6:    r1000c0s0/out22;
in7:    r1000c0s0/out23;
in8:    r1000c0s0/out24;

in9:    r1000c0s0/out25;
in10:   r1000c0s0/out26;
in11:   r1000c0s0/out27;
in12:   r1000c0s0/out28;
in13:   r1000c0s0/out29;
in14:   r1000c0s0/out30;
in15:   r1000c0s0/out31;
in16:   r1000c0s0/out32;

to:  rACTARc0s2/in_lemo2;
}

SIS3316(rACTARc0s8)             //ADC3
{
in1:    r1000c0s0/out33;
in2:    r1000c0s0/out34;
in3:    r1000c0s0/out35;
in4:    r1000c0s0/out36;
in5:    r1000c0s0/out37;
in6:    r1000c0s0/out38;
in7:    r1000c0s0/out39;
in8:    r1000c0s0/out40;

in9:    r1000c0s0/out41;
in10:   r1000c0s0/out42;
in11:   r1000c0s0/out43;
in12:   r1000c0s0/out44;
in13:   r1000c0s0/out45;
in14:   r1000c0s0/out46;
in15:   r1000c0s0/out47;
in16:   r1000c0s0/out48;

to:  rACTARc0s2/in_lemo3;
}

SIS3316(rACTARc0s10)             //ADC4
{
in1:    r1000c0s0/out49;
in2:    r1000c0s0/out50;
in3:    r1000c0s0/out51;
in4:    r1000c0s0/out52;
in5:    r1000c0s0/out53;
in6:    r1000c0s0/out54;
in7:    r1000c0s0/out55;
in8:    r1000c0s0/out56;

in9:    r1000c0s0/out57;
in10:   r1000c0s0/out58;
in11:   r1000c0s0/out59;
in12:   r1000c0s0/out60;
in13:   r1000c0s0/out61;
in14:   r1000c0s0/out62;
in15:   r1000c0s0/out63;
in16:   r1000c0s0/out64;

to:  rACTARc0s2/in_lemo4;
}

SIS3316(rACTARc0s12)             //ADC5
{
in1:    r1000c0s0/out65;
in2:    r1000c0s0/out66;
in3:    rPOS1c1s1u3/out1;
in4:    rPOS1c2s7/nim_out3;
in5:	rMASTERc2s12u1/out2;
in6:    rMASTERc1s5/nim_out12;
in7:    ;
in8:    ;

in9:    ;
in10:   ;
in11:   ;
in12:   ;
in13:   ;
in14:   ;
in15:   ;
in16:   ;
}


/*
 *
 * rackFIBER CRATE 1
 *
 * NIM Crate with Amplifiers for GFI 1&2
 *
 *
 *
 *
 */
  FOUR_POST_RACK(rackFIBER) //declared in   /cabling/modules/camac/camac.hh:RACK(FOUR_POST_RACK)
{
  LABEL("Rack close to GFIs, hosts NIM Crate with Amplifiers for GFI1 & GFI2");
}

 NIM_CRATE(rackFIBERc1) //declared in   /cabling/modules/nim/nim.hh:CRATE(NIM_CRATE)
 {
 ;
 }

PHILLIPS779(rackFIBERc1s18) // declared in  /cabling/modules/nim/amplifier.hh:MODULE(PHILLIPS779) 
 {
 in1_18: rGFI1c2/x_out1_18;
 in19_32: rGFI1c2/y_out1_14;

 out1_8: rackFIBERc11/in1_8;
 out9_16: rackFIBERc12/in1_8;
 out17_24: rackFIBERc13/in1_8;
 out25_32: rackFIBERc14/in1_8;
 }

PHILLIPS779(rackFIBERc1s4)
 {
 in31_32: rGFI1c2/y_out15_16;

 in1_4: rGFI2c2/y_out13_16;

 out31_32: rackFIBERc15/in3_4;
 out3_4: rackFIBERc15/in1_2;

 out1_2: rackFIBERc19/in7_8;
 }

PHILLIPS779(rackFIBERc1s3)
 {
 in1_10: rGFI2c2/x_out1_10;
 // in11: BROKEN
 in12_19: rGFI2c2/x_out11_18;
 // in20: BROKEN
 in21_32: rGFI2c2/y_out1_12;
 
 out1_8: rackFIBERc16/in1_8;
 out9_10: rackFIBERc17/in1_2;
 out12_17: rackFIBERc17/in3_8;
 out18_19: rackFIBERc18/in1_2;
 out21_26: rackFIBERc18/in3_8;
 out27_32: rackFIBERc19/in1_6;
}


/********** JOINER_8 short 8-fold cables close to GFI ************/

JOINER_8(rackFIBERc11)   // declared in    /cabling/modules/misc.hh:CRATE(JOINER_8)
{
 in1_8: rackFIBERc1s18/out1_8;
 out1_8: rackFIBERc21/in1_8;
}

JOINER_8(rackFIBERc12)
{
 in1_8: rackFIBERc1s18/out9_16;
 out1_8: rackFIBERc22/in1_8;
}

JOINER_8(rackFIBERc13)
{
 in1_8: rackFIBERc1s18/out17_24;
 out1_8: rackFIBERc23/in1_8;
}

JOINER_8(rackFIBERc14)
{
 in1_8: rackFIBERc1s18/out25_32;
 out1_8: rackFIBERc24/in1_8;
}

JOINER_8(rackFIBERc15)
{
 in1_2: rackFIBERc1s4/out3_4;
 in3_4: rackFIBERc1s4/out31_32;
// in5_8: NONE

 out1_4: rackFIBERc25/in1_4;
// out5_8:NONE
}

JOINER_8(rackFIBERc16)
{
 in1_8: rackFIBERc1s3/out1_8;
 out1_8: rackFIBERc26/in1_8;
}

JOINER_8(rackFIBERc17)
{
 in1_2: rackFIBERc1s3/out9_10;
 in3_8: rackFIBERc1s3/out12_17;

 out1_8: rackFIBERc27/in1_8;
}

JOINER_8(rackFIBERc18)
{
 in1_2: rackFIBERc1s3/out18_19;
 in3_8: rackFIBERc1s3/out21_26;

 out1_8: rackFIBERc28/in1_8;
}

JOINER_8(rackFIBERc19)
{
 in1_6: rackFIBERc1s3/out27_32;
 in7_8: rackFIBERc1s4/out1_2;
 out1_8: rackFIBERc29/in1_8;
}

/********** JOINER_8 short 8-fold cables close to readout ************/
/********** cable channels 1-8 <=> TacQuila channels 8-1 *************/
JOINER_8(rackFIBERc21)
{
 in1_8: rackFIBERc11/out1_8;

 out1: rackLAND2c3s1u1/in8;
 out2: rackLAND2c3s1u1/in7;
 out3: rackLAND2c3s1u1/in6;
 out4: rackLAND2c3s1u1/in5;
 out5: rackLAND2c3s1u1/in4;
 out6: rackLAND2c3s1u1/in3;
 out7: rackLAND2c3s1u1/in2;
 out8: rackLAND2c3s1u1/in1;
}
JOINER_8(rackFIBERc22)
{
 in1_8: rackFIBERc12/out1_8;

 out1: rackLAND2c3s1u1/in16;
 out2: rackLAND2c3s1u1/in15;
 out3: rackLAND2c3s1u1/in14;
 out4: rackLAND2c3s1u1/in13;
 out5: rackLAND2c3s1u1/in12;
 out6: rackLAND2c3s1u1/in11;
 out7: rackLAND2c3s1u1/in10;
 out8: rackLAND2c3s1u1/in9;
}
JOINER_8(rackFIBERc23)
{
 in1_8: rackFIBERc13/out1_8;

 out1: rackLAND2c3s2u1/in8;
 out2: rackLAND2c3s2u1/in7;
 out3: rackLAND2c3s2u1/in6;
 out4: rackLAND2c3s2u1/in5;
 out5: rackLAND2c3s2u1/in4;
 out6: rackLAND2c3s2u1/in3;
 out7: rackLAND2c3s2u1/in2;
 out8: rackLAND2c3s2u1/in1;
}
JOINER_8(rackFIBERc24)
{
 in1_8: rackFIBERc14/out1_8;

 out1: rackLAND2c3s2u1/in16;
 out2: rackLAND2c3s2u1/in15;
 out3: rackLAND2c3s2u1/in14;
 out4: rackLAND2c3s2u1/in13;
 out5: rackLAND2c3s2u1/in12;
 out6: rackLAND2c3s2u1/in11;
 out7: rackLAND2c3s2u1/in10;
 out8: rackLAND2c3s2u1/in9;
}
JOINER_8(rackFIBERc25)
{
 in1_4: rackFIBERc15/out1_4;

 out1: rackLAND2c3s3u1/in8;
 out2: rackLAND2c3s3u1/in7;
 out3: rackLAND2c3s3u1/in6;
 out4: rackLAND2c3s3u1/in5;
}
JOINER_8(rackFIBERc26)
{
 in1_8: rackFIBERc16/out1_8;

 out1: rackLAND2c3s3u1/in9;
 out2: rackLAND2c3s3u1/in10;
 out3: rackLAND2c3s3u1/in11;
 out4: rackLAND2c3s3u1/in12;
 out5: rackLAND2c3s3u1/in13;
 out6: rackLAND2c3s3u1/in14;
 out7: rackLAND2c3s3u1/in15;
 out8: rackLAND2c3s3u1/in16;
}
JOINER_8(rackFIBERc27)
{
 in1_8: rackFIBERc17/out1_8;

 out1: rackLAND2c3s4u1/in8;
 out2: rackLAND2c3s4u1/in7;
 out3: rackLAND2c3s4u1/in6;
 out4: rackLAND2c3s4u1/in5;
 out5: rackLAND2c3s4u1/in4;
 out6: rackLAND2c3s4u1/in3;
 out7: rackLAND2c3s4u1/in2;
 out8: rackLAND2c3s4u1/in1;
}
JOINER_8(rackFIBERc28)
{
 in1_8: rackFIBERc18/out1_8;

 out1: rackLAND2c3s4u1/in16;
 out2: rackLAND2c3s4u1/in15;
 out3: rackLAND2c3s4u1/in14;
 out4: rackLAND2c3s4u1/in13;
 out5: rackLAND2c3s4u1/in12;
 out6: rackLAND2c3s4u1/in11;
 out7: rackLAND2c3s4u1/in10;
 out8: rackLAND2c3s4u1/in9;
}
JOINER_8(rackFIBERc29)
{
 in1_8: rackFIBERc19/out1_8;

 out1: rackLAND2c3s5u1/in8;
 out2: rackLAND2c3s5u1/in7;
 out3: rackLAND2c3s5u1/in6;
 out4: rackLAND2c3s5u1/in5;
 out5: rackLAND2c3s5u1/in4;
 out6: rackLAND2c3s5u1/in3;
 out7: rackLAND2c3s5u1/in2;
 out8: rackLAND2c3s5u1/in1;
}






///////////////////// from s412 ///////////////////////
/*
 in1: rGFI2c1s1u1/signal;
 in2: rGFI2c1s1u2/signal;
 in3: rGFI2c1s1u3/signal;
 in4: rGFI2c1s1u4/signal;
 in5: rGFI2c1s1u5/signal;
 in6: rGFI2c1s1u6/signal;
 in7: rGFI2c1s1u7/signal;
 in8: rGFI2c1s1u8/signal;
// in9:
 in10: rGFI2c1s1u10/signal;
 in11: rGFI2c1s1u11/signal;
 in12: rGFI2c1s1u12/signal;
 in13: rGFI2c1s1u13/signal;
 in14: rGFI2c1s1u14/signal;
 in15: rGFI2c1s1u15/signal;
 in16: rGFI2c1s1u16/signal;
 
 in17: rGFI2c1s1u33/signal;
 in18: rGFI2c1s1u34/signal;
 
 in19: rGFI2c1s1u19/signal;
 in20: rGFI2c1s1u20/signal;
 in21: rGFI2c1s1u21/signal;
 in22: rGFI2c1s1u22/signal;
 in23: rGFI2c1s1u23/signal;
 in24: rGFI2c1s1u24/signal;
 in25: rGFI2c1s1u25/signal;
 in26: rGFI2c1s1u26/signal;
 in27: rGFI2c1s1u27/signal;
 in28: rGFI2c1s1u28/signal;
 in29: rGFI2c1s1u29/signal;
 in30: rGFI2c1s1u30/signal;
 in31: rGFI2c1s1u31/signal;
 in32: rGFI2c1s1u32/signal;
 
 
 
 out1: r53c1s2/in1;
 out3: r53c1s2/in2;
 out5: r53c1s2/in3;
 out7: r53c1s2/in4;
 //out9: 
 out11: r53c1s2/in6;
 out13: r53c1s2/in7;
 out15: r53c1s2/in8;
 out17: r53c1s2/in9;
 out19: r53c1s2/in10;
 out21: r53c1s2/in11;
 out23: r53c1s2/in12;
 out25: r53c1s2/in13;
 out27: r53c1s2/in14;
 out29: r53c1s2/in15;
 out31: r53c1s2/in16;
 
 
 out2: r53c1s3/in1;
 out4: r53c1s3/in2;
 out6: r53c1s3/in3;
 out8: r53c1s3/in4;
 out10: r53c1s3/in5;
 out12: r53c1s3/in6;
 out14: r53c1s3/in7;
 out16: r53c1s3/in8;
 out18: r53c1s3/in9;
 out20: r53c1s3/in10;
 out22: r53c1s3/in11;
 out24: r53c1s3/in12;
 out26: r53c1s3/in13;
 out28: r53c1s3/in14;
 out30: r53c1s3/in15;
 out32: r53c1s3/in16;
 
 }
 */
/************ no DELAYs used in s438 ***********/
/*
 //GFI 2 odd numbers
 DP1610(r53c1s7)
 {
 in1: rGFI2c1s1u1/signal;
 in2: rGFI2c1s1u3/signal;
 in3: rGFI2c1s1u5/signal;
 in4: rGFI2c1s1u7/signal;
 in5: rGFI2c1s1u9/signal;
 in6: rGFI2c1s1u11/signal;
 in7: rGFI2c1s1u13/signal;
 in8: rGFI2c1s1u15/signal;
 in9: rGFI2c1s1u17/signal;
 in10: rGFI2c1s1u19/signal;
 in11: rGFI2c1s1u21/signal;
 in12: rGFI2c1s1u23/signal;
 in13: rGFI2c1s1u25/signal;
 in14: rGFI2c1s1u27/signal;
 in15: rGFI2c1s1u29/signal;
 in16: rGFI2c1s1u31/signal;
 
 out1: r15c4s18/in0; 
 out2: r15c4s18/in2;
 out3: r15c4s18/in4;
 out4: r15c4s18/in6;
 out5: r15c4s18/in8;
 out6: r15c4s18/in10;
 out7: r15c4s18/in12;
 out8: r15c4s18/in14;
 out9: r15c4s18/in16;
 out10: r15c4s18/in18;
 out11: r15c4s18/in20;
 out12: r15c4s18/in22;
 out13: r15c4s18/in24;
 out14: r15c4s18/in26;
 out15: r15c4s18/in28;
 out16: r15c4s18/in30;
 }


 //GFI 2 even numbers
 DP1610(r53c1s9)
 {
 in1: rGFI2c1s1u2/signal;
 in2: rGFI2c1s1u4/signal;
 in3: rGFI2c1s1u6/signal;
 in4: rGFI2c1s1u8/signal;
 in5: rGFI2c1s1u10/signal;
 in6: rGFI2c1s1u12/signal;
 in7: rGFI2c1s1u14/signal;
 in8: rGFI2c1s1u16/signal;
 in9: rGFI2c1s1u18/signal;
 in10: rGFI2c1s1u20/signal;
 in11: rGFI2c1s1u22/signal;
 in12: rGFI2c1s1u24/signal;
 in13: rGFI2c1s1u26/signal;
 in14: rGFI2c1s1u28/signal;
 in15: rGFI2c1s1u30/signal;
 in16: rGFI2c1s1u32/signal;

 out1: r15c4s19/in5;
  
 out2: r15c4s18/in3;
 out3: r15c4s19/in6;
 out4: r15c4s18/in7;
 out5: r15c4s18/in9;
 out6: r15c4s18/in11;
 out7: r15c4s18/in13;
 out8: r15c4s18/in15;
 out9: r15c4s18/in17;
 out10: r15c4s18/in19;
 out11: r15c4s18/in21;
 out12: r15c4s18/in23;
 out13: r15c4s18/in25;
 out14: r15c4s18/in27;
 out15: r15c4s18/in29;
 out16: r15c4s18/in31;
 }


 //GFI 1 and 2 , ch 33,34
 DP1610(r53c1s13)
 {
 in1: rGFI1c1s1u33/signal;
 in2: rGFI1c1s1u34/signal;
 in15: rGFI2c1s1u33/signal;
 in16: rGFI2c1s1u34/signal;

 out1: r15c4s19/in0;
 out2: r15c4s19/in1;
 out15: r15c4s19/in2;
 out16: r15c4s19/in3;

 }

//GFI 1 odd numbers
 DP1610(r53c1s17)
 {
 in1: rGFI1c1s1u1/signal;
 in2: rGFI1c1s1u3/signal;
 in3: rGFI1c1s1u5/signal;
 in4: rGFI1c1s1u7/signal;
 in5: rGFI1c1s1u9/signal;
 in6: rGFI1c1s1u11/signal;
 in7: rGFI1c1s1u13/signal;
 in8: rGFI1c1s1u15/signal;
 in9: rGFI1c1s1u17/signal;
 in10: rGFI1c1s1u19/signal;
 in11: rGFI1c1s1u21/signal;
 in12: rGFI1c1s1u23/signal;
 in13: rGFI1c1s1u25/signal;
 in14: rGFI1c1s1u27/signal;
 in15: rGFI1c1s1u29/signal;
 in16: rGFI1c1s1u31/signal;
 
 out1: r15c4s17/in0; 
 out2: r15c4s17/in2;
 out3: r15c4s17/in4;
 out4: r15c4s17/in6;
 out5: r15c4s17/in8;
 out6: r15c4s17/in10;
 out7: r15c4s17/in12;
 out8: r15c4s17/in14;
 
 out9: r15c4s19/in4;
 
 out10: r15c4s17/in18;
 out11: r15c4s17/in20;
 out12: r15c4s17/in22;
 out13: r15c4s17/in24;
 out14: r15c4s17/in26;
 out15: r15c4s17/in28;
 out16: r15c4s17/in30;
 }


 //GFI 1 even numbers
 DP1610(r53c1s19)
 {
 in1: rGFI1c1s1u2/signal;
 in2: rGFI1c1s1u4/signal;
 in3: rGFI1c1s1u6/signal;
 in4: rGFI1c1s1u8/signal;
 in5: rGFI1c1s1u10/signal;
 in6: rGFI1c1s1u12/signal;
 in7: rGFI1c1s1u14/signal;
 in8: rGFI1c1s1u16/signal;
 in9: rGFI1c1s1u18/signal;
 in10: rGFI1c1s1u20/signal;
 in11: rGFI1c1s1u22/signal;
 in12: rGFI1c1s1u24/signal;
 in13: rGFI1c1s1u26/signal;
 in14: rGFI1c1s1u28/signal;
 in15: rGFI1c1s1u30/signal;
 in16: rGFI1c1s1u32/signal;

 out1: r15c4s17/in1;
 out2: r15c4s17/in3;
 
 out3: r15c4s19/in15;
 
 out4: r15c4s17/in7;
 out5: r15c4s17/in9;
 out6: r15c4s17/in11;
 out7: r15c4s17/in13;
 out8: r15c4s17/in15;
 out9: r15c4s17/in17;
 out10: r15c4s17/in19;
 out11: r15c4s17/in21;
 out12: r15c4s17/in23;
 out13: r15c4s17/in25;
 out14: r15c4s17/in27;
 out15: r15c4s17/in29;
 out16: r15c4s17/in31;
 }
*/

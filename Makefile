PARTS = master

all: $(PARTS)
	@echo "A-ok, good mapping!"

GENDIR = gen

ifneq (clean,$(MAKECMDGOALS))
depfiles:=$(addprefix $(GENDIR)/,$(addsuffix .d,$(PARTS)))
hfiles:=$(shell for i in `cat $(depfiles)`; do echo $$i; done | grep hh)
missing_files:=$(shell for i in $(hfiles); do [ -f $$i ] || echo $$i; done)
ifneq (,$(missing_files))
$(error Missing files "$(missing_files)", try "make clean" or add the files.)
endif
-include $(depfiles)
endif

GENERATED = $(GENDIR)/cfg_elecloc.hh \
	    $(GENDIR)/cfg_sig_beam.hh \
	    $(GENDIR)/scaler_listing.hh \
	    $(GENDIR)/setup_db.kumac \
	    $(GENDIR)/HV.hvc

TREE_OR_HOME_SRC_DIR = $(firstword $(wildcard $(addsuffix /$(1),$(shell pwd)/.. $(HOME))))

YTCON_DIR = $(call TREE_OR_HOME_SRC_DIR,ytcontools)

YTCON = $(YTCON_DIR)/ytcon

EXTRACT = extract.pl

MAP = map.pl

GENERATOR = generator.pl

#########################################################

.PHONY: master
master: $(GENDIR)/master.ytc $(GENERATED)

$(GENDIR)/master.d:
	@echo "  DEPS   $@"
	@mkdir -p $(GENDIR)
	@cpp -MM -MG master/master.cc | \
	  sed -e 's,master.o[ :]*,$(GENDIR)/master.ytc $(GENDIR)/master.d: ,g' \
	> $(GENDIR)/master.d

$(GENDIR)/master.ytc: $(GENDIR)/master.d ytcon
	@echo "  CHECK  $@"
	@mkdir -p $(GENDIR)
	@cpp -I . master/master.cc | $(YTCON) > $@

#########################################################

$(GENDIR)/cfg_elecloc.hh: $(GENDIR)/master.ytc $(EXTRACT)
	@echo " EXTRACT $@"
	@./$(EXTRACT) CFG_ELECLOC < $< > $@

$(GENDIR)/cfg_sig_beam.hh: $(GENDIR)/master.ytc $(EXTRACT) $(MAP)
	@echo " EXTRACT $@"
	@./$(EXTRACT) CFG_SIG_BEAM < $< | ./$(MAP) > $@

$(GENDIR)/scaler_listing.hh: $(GENDIR)/master.ytc $(EXTRACT)
	@echo " EXTRACT $@"
	@./$(EXTRACT) SCALER_LISTING < $< > $@

$(GENDIR)/setup_db.kumac: $(GENDIR)/cfg_sig_beam.hh $(GENDIR)/cfg_elecloc.hh make_land_kumac.pl
	@echo "   GEN   $@"
	@cat $(GENDIR)/cfg_sig_beam.hh $(GENDIR)/cfg_elecloc.hh | \
	  cpp -P | perl ./make_land_kumac.pl > $@

$(GENDIR)/HV.hvc: $(GENDIR)/master.ytc $(GENERATOR)
	@echo "   GEN   $@"
	./generator.pl

#########################################################

.PHONY: ytcon
ytcon:
	@$(MAKE) -C $(dir $(YTCON))

clean:
	rm -rf $(GENDIR)

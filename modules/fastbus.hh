CRATE(FASTBUS_CRATE)
{
  LABEL("Slowbus crate");
}

MODULE(NGF)
{
  LABEL("VME interface module");

  LEMO_INPUT(in1_4);  // nim
  LEMO_OUTPUT(out1_4); // nim
  LEMO_OUTPUT(out_ttl1_4);
  LEMO_INPUT(reset);

  LEMO_INPUT(in_ecl1_4);  // nim
  LEMO_OUTPUT(out_ecl1_4); // nim
}

MODULE(LECROY1810)
{
  MODEL_AS(DAQ_MONITOR);

  LABEL("CAT");

  CONTROL("CLASS","lecroy1810");

  LEMO_INPUT(tdc_stop);
  LEMO_INPUT(adc_gate);
}

MODULE(LECROY1875)
{
  MODEL_AS(TDC);

  CONTROL("CLASS","lecroy1875");

  CONTROL("FIRST_CHANNEL","in0");
  CONTROL("ELECLOC_NAME","TDC_F");

  ECL_INPUT(in0_63,SIGNAL);
}

MODULE(LECROY1885F)
{
  MODEL_AS(QDC);

  CONTROL("CLASS","lecroy1885");

  CONTROL("FIRST_CHANNEL","in0");
  CONTROL("ELECLOC_NAME","QDC_F");

  ECL_INPUT(in0_95,SIGNAL);
  ECL_INPUT(gate);
}

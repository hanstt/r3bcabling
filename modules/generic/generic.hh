/////////////////////////////////////////////////////////////////////
//
// Generic modules.  Their names are used by the database generator
// to find starting points.
//
/////////////////////////////////////////////////////////////////////

DEVICE(DAQ_EVENTWISE)
{
  // Data from a module like this is usually collected each event
  LABEL("DAQ eventwise");

}

DEVICE(DAQ_MONITOR)
{
  // Data from a module like this is usually collected once in a while
  LABEL("DAQ monitor");
}

DEVICE(SLOW_CONTROL)
{
  // A module like this is used for slow control
  LABEL("Slow control");
}

DEVICE(CFD)
{
  MODEL_AS(SLOW_CONTROL);

  LABEL("CFD");
}

DEVICE(TDC)
{
  MODEL_AS(DAQ_EVENTWISE);

  LABEL("TDC");
}

DEVICE(QDC)
{
  MODEL_AS(DAQ_EVENTWISE);

  LABEL("QDC");
}

DEVICE(ADC)
{
  MODEL_AS(DAQ_EVENTWISE);

  LABEL("ADC");
}

DEVICE(SCALER)
{
  MODEL_AS(DAQ_MONITOR);

  LABEL("SCALER");
}

MODULE(DETECTOR)
{
  LABEL("Detector (1 ch)");
}

MODULE(HV)
{
  MODEL_AS(SLOW_CONTROL);

  LABEL("HV");
}

MODULE(CONTROLLER)
{
  MODEL_AS(SLOW_CONTROL);

  LABEL("CONTROLLER");
}

MODULE(PROCESSOR)
{
  LABEL("PROCESSOR");

  HAS_SETTING("HOSTNAME"=>"[_a-zA-Z0-9-]+");

  CONNECTOR(vsb_out);
}

CRATE(COMPUTER)
{
  LABEL("COMPUTER");

  HAS_SETTING("HOSTNAME"=>"[_a-zA-Z0-9-]+");

}

MODULE(SCOPE)
{
  MODEL_AS(SLOW_CONTROL);
  
  // A scope is a slow-control item of sorts, but has no
  // controlling processor.  (It has a user.)
  CONTROL("PROCESSOR","NONE");

  CONTROL("FIRST_CHANNEL","in");

  LEMO_INPUT(in,SIGNAL);
}

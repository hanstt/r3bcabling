UNIT(LANDFEE2)
{
  LABEL("Splitter");

  HANDLER("MULTI_IN_OUT","in=t,e");

  LEMO_INPUT(in1_8);
  LEMO_INPUT(in9_16);

  CONNECTOR(t1_16); // out to TACQUILA
  CONNECTOR(e1_16); // out to TRIPLEX
}

// this is ONLY the root node of the triplex tree
// it contains an extra I2C -> Eth interface
UNIT(TRIPLEX2MASTER) // aka Hammerhai 
{
  LABEL("Triplex");

  HAS_SETTING("ADDRESS" => "0x[a-fA-F0-9]+");
  HAS_SETTING("HOSTNAME" => "[a-zA-Z][a-zA-Z0-9]+");

  HANDLER("MULTI_IN_OUT","in=out");

  CONNECTOR(in1_16);  // from LANDFEE
  CONNECTOR(out1_16); // to QDC

  /*CONNECTOR(j6); // up tree*/
  CONNECTOR(j5); // down tree
  CONNECTOR(j7); // down tree
}

UNIT(TRIPLEX2) // aka Hammerhai
{
  LABEL("Triplex");

  HAS_SETTING("ADDRESS" => "0x[a-fA-F0-9]+");

  HANDLER("MULTI_IN_OUT","in=out");

  CONNECTOR(in1_16);  // from LANDFEE
  CONNECTOR(out1_16); // to QDC

  CONNECTOR(j6); // up tree
  CONNECTOR(j5); // down tree
  CONNECTOR(j7); // down tree
}

UNIT(TACQ_QDC2)
{
  LABEL("TAC_QDC");
  MODEL_AS(QDC);

  CONTROL("CLASS","tacquila");
  //CONTROL("FIRST_CHANNEL","in1");
  CONTROL("FIRST_CHANNEL","in0");

  CONNECTOR(in1_16,SIGNAL); // from TRIPLEX
}

/////////////////////////////////////////////////////////////////////

/* To achieve the effect of land02 SIG_BEAM labels having the time
 * labels for all channels in the QDC part, and the 17th channel
 * in the time part for the first channel, some dirty hacks have been
 * employed. Also see hacks in TRIPLEX2_TACQ_QDC2_TACQUILA3.
 *
 * To remove the hack: remove lines between 'hack' and 'endhack'.
 * Uncomment parts marked: 'nonhack: KEEPCODE'
 */

MODULE(TACQUILA3)
{
  MODEL_AS(TDC);
  //MODEL_AS(QDC);

  LABEL("Tacquila");

  CONTROL("CLASS","tacquila");

  /* nonhack: CONTROL("FIRST_CHANNEL","in1"); */
  CONNECTOR(in1_16 /* nonhack: ,SIGNAL*/ );
  CONNECTOR(in17); // 17th Channel (Common Start/Stop) // or ECL_INPUT??

  /* hack */
  //CONTROL("FIRST_CHANNEL","hackin1");
  CONTROL("FIRST_CHANNEL","hackin0");
  CONNECTOR(hackin17,SIGNAL);
  CONNECTOR(out1);
  HANDLER("MULTI_IN_OUT","in=out");
  /* endhack */

  CONNECTOR(gtb_master); // towards host
  CONNECTOR(gtb_slave);  // towards end
}

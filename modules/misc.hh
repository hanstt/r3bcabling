CRATE(TERMINAL)
{
  HANDLER("MULTI_IN_OUT","in=e,t");

  /* Signals from detectors. (back side) */

  BNC_INPUT(in1_8);

  /* Energy signals.  Delayed 400 ns.
   */

  LEMO_OUTPUT(e1_8);

  /* Time signals.
   */

  LEMO_OUTPUT(t1_8);
}

MODULE(CAPACITOR_BOX)
{
  HANDLER("MULTI_IN_OUT","in=out");

  LEMO_INPUT(in1_8);
  LEMO_OUTPUT(out1_8);
}

CRATE(CABLE_DELAY_8)
{
  HANDLER("MULTI_IN_OUT","in=out");

  HAS_SETTING("DELAY" => "%fns");

  LEMO_INPUT(in1_8);
  LEMO_OUTPUT(out1_8);
}

CRATE(CABLE_DELAY_12)
{
  HANDLER("MULTI_IN_OUT","in=out");

  HAS_SETTING("DELAY" => "%fns");

  LEMO_INPUT(in1_12);
  LEMO_OUTPUT(out1_12);
}

CRATE(CABLE_DELAY_16)
{
  HANDLER("MULTI_IN_OUT","in=out");

  HAS_SETTING("DELAY" => "%fns");

  LEMO_INPUT(in1_16);
  LEMO_OUTPUT(out1_16);
}

CRATE(CABLE_DELAY_32)
{
  HANDLER("MULTI_IN_OUT","in=out");

  HAS_SETTING("DELAY" => "%fns");

  LEMO_INPUT(in1_32);
  LEMO_OUTPUT(out1_32);
}

CRATE(CABLE_DELAY)
{
  LABEL("DELAY");
}

MODULE(CABLE_DELAY_SLOT)
{
  HAS_SETTING("DELAY" => "%fns");

  LEMO_INPUT(in);
  LEMO_OUTPUT(out);
}

CRATE(JOINER_8)
{
  HANDLER("MULTI_IN_OUT","in=out");

  LEMO_INPUT(in1_8);
  LEMO_OUTPUT(out1_8);
}

CRATE(HV_CONN_16)
{
  HANDLER("MULTI_IN_OUT","in=out");

  // TODO: this is not LEMO connectors

  LEMO_INPUT(in1_16);   
  LEMO_OUTPUT(out1_16);
}
CRATE(HV_CONN_28)
{
  HANDLER("MULTI_IN_OUT","in=out");

  // TODO: this is not LEMO connectors

  LEMO_INPUT(in1_28);   
  LEMO_OUTPUT(out1_28);
}

CRATE(PATCH_PANEL_8)
{
  HANDLER("MULTI_IN_OUT","in=out");

  BNC_INPUT(in1_8);
  BNC_OUTPUT(out1_8);
}

CRATE(PATCH_PANEL_8_new)
{
  HANDLER("MULTI_IN_OUT","front=back");

  BNC_INPUT(front1_8);
  BNC_OUTPUT(back1_8);
}

CRATE(PATCH_PANEL_16)
{
  HANDLER("MULTI_IN_OUT","in=out");

  BNC_INPUT(in1_16);
  BNC_OUTPUT(out1_16);
}

CRATE(PATCH_PANEL_16_new)
{
  HANDLER("MULTI_IN_OUT","front=back");

  BNC_INPUT(front1_61);
  BNC_OUTPUT(back1_16);
}

CRATE(PATCH_PANEL_24)
{
  HANDLER("MULTI_IN_OUT","in=out");

  BNC_INPUT(in1_24);
  BNC_OUTPUT(out1_24);
}

CRATE(PATCH_PANEL_24_new)
{
  HANDLER("MULTI_IN_OUT","front=back");

  BNC_INPUT(front1_24);
  BNC_OUTPUT(back1_24);
}

CRATE(PATCH_PANEL_48)
{
  HANDLER("MULTI_IN_OUT","in=out");

  BNC_INPUT(in1_48);
  BNC_OUTPUT(out1_48);
}

CRATE(PATCH_PANEL_48_new)
{
  HANDLER("MULTI_IN_OUT","front=back");

  BNC_INPUT(front1_48);
  BNC_OUTPUT(back1_48);
}

/* This should provide a way to include all the small devices 
 * used in the setup for filtering signals (capacitors, etc.)
 * which usually don't stand in a slot of a crate (like MLAND 
 * ARM or GATE labeled electronic device )
 */
MODULE(ELECTRONIC_FILTER)
{
	LABEL("Filter");

	CONNECTOR(in);
	CONNECTOR(out);
}

MODULE(OSCILLOSCOPE)
{
  LABEL("Oscilloscope");
  CONNECTOR(ch1_4);
}

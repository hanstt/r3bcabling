#include "processor.hh"
#include "caen.hh"
#include "gsi.hh"
#include "mesytec.hh"

CRATE(VME_CRATE)
{
  LABEL("VME crate");
  HAS_SETTING("CRATE_NO" => "[0-9]+");
}

MODULE(DISPLAY)
{
  LABEL("Display");
}

MODULE(SIS36)
{
    LABEL("SIS36");
    
    ECL_OUTPUT(control_io1_16);
}

MODULE(HDCAN1)
{
  LABEL("ECL-LVDS converter");
  
  HANDLER("MULTI_IN_OUT","in=out");

  ECL_INPUT(in1_32);
  LVDS_OUTPUT(out1_32);
}

MODULE(HDTTL1)
{
  LABEL("LVDS-NIM converter");

  HANDLER("MULTI_IN_OUT","in=out");

  LVDS_INPUT(in1_32);
  LEMO_OUTPUT(out1_32);
}

MODULE(SIS3316)
{
    LABEL("SIS3316");
    
    LEMO_INPUT(in1_16);
    LEMO_INPUT(ti);
    LEMO_INPUT(to);
}

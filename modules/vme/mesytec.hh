MODULE(MESYTEC_MADC32)
{
  MODEL_AS(ADC);

  LABEL("Mesytec 32-channel ADC");

  HAS_SETTING("ADDRESS" => "0x[a-fA-F0-9]+");
  HAS_SETTING("VIRTUAL_SLOT" => "[0-9]+");

  CONTROL("CLASS","mesy_madc32");
  CONTROL("FIRST_CHANNEL", "in1");

  ECL_INPUT(in1_32, SIGNAL);

  LEMO_INPUT(nim_gate0);
  LEMO_INPUT(nim_gate1);
  LEMO_OUTPUT(nim_busy);
}

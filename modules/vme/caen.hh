MODULE(CAEN_V538A)
{
  LABEL("8 CH ACL-NIM NIM-ECL");

  LEMO_INPUT(in0_7);
  ECL_INPUT(ecl_in0_7);

  LEMO_OUTPUT(out0a_7b);
  //LEMO_OUTPUT(outb0_7);
  ECL_OUTPUT(ecl_out0a_7b);
  //ECL_OUTPUT(ecl_outb0_7);
}

MODULE(CAEN_V775)
{
  MODEL_AS(TDC);

  // TODO: move into a parent class (multiple MODEL_AS needed)
  HAS_SETTING("ADDRESS" => "0x[a-fA-F0-9]+");
  HAS_SETTING("VIRTUAL_SLOT" => "[0-9]+");

  LABEL("32 CH TDC");

  CONTROL("CLASS","caen_v775");

  CONTROL("FIRST_CHANNEL","in0");

  LEMO_INPUT(com_in);
  LEMO_INPUT(com_out);
  ECL_INPUT(rst);

  ECL_INPUT(in0_15,SIGNAL);
  ECL_INPUT(in16_31,SIGNAL);

  ECL_OUTPUT(busy);
}

MODULE(CAEN_V785)
{
  MODEL_AS(ADC);

  // TODO: move into a parent class (multiple MODEL_AS needed)
  HAS_SETTING("ADDRESS" => "0x[a-fA-F0-9]+");
  HAS_SETTING("VIRTUAL_SLOT" => "[0-9]+");

  LABEL("32 CH PEAK SENSING ADC");

  CONTROL("CLASS","caen_v785");

  CONTROL("FIRST_CHANNEL","in0");

  LEMO_INPUT(gate_in);
  LEMO_INPUT(gate_out);

  ECL_INPUT(rst);

  ECL_INPUT(in0_15,SIGNAL);
  ECL_INPUT(in16_31,SIGNAL);

  ECL_OUTPUT(busy);
}

MODULE(CAEN_V792)
{
  MODEL_AS(QDC);

  // TODO: move into a parent class (multiple MODEL_AS needed)
  HAS_SETTING("ADDRESS" => "0x[a-fA-F0-9]+");
  HAS_SETTING("VIRTUAL_SLOT" => "[0-9]+");

  LABEL("32 CH INTEGRATING QDC");

  CONTROL("CLASS","caen_v792");

  CONTROL("FIRST_CHANNEL","in0");

  LEMO_INPUT(gate_in);
  LEMO_INPUT(gate_out);

  ECL_INPUT(rst);

  ECL_INPUT(in0_15,SIGNAL);
  ECL_INPUT(in16_31,SIGNAL);

  ECL_OUTPUT(busy);
}

MODULE(CAEN_V830)
{
  MODEL_AS(SCALER);

  LABEL("32 CH MULTI LATCHING SCALER");
  HAS_SETTING("ADDRESS" => "0x[a-fA-F0-9]+");
  HAS_SETTING("VIRTUAL_SLOT" => "[0-9]+");

  CONTROL("CLASS","caen_v830");

  CONTROL("FIRST_CHANNEL","in0");

  LEMO_INPUT(trig_in);
  LEMO_INPUT(trig_out);

  LEMO_INPUT(test);

  ECL_INPUT(in0_15,SIGNAL);
  ECL_INPUT(in16_31,SIGNAL);

  ECL_INPUT(clr);
  ECL_OUTPUT(drdy);
  ECL_INPUT(trig);
  ECL_INPUT(veto);
  ECL_OUTPUT(busy);
}

MODULE(CAEN_V862)
{
  MODEL_AS(QDC);

  // TODO: move into a parent class (multiple MODEL_AS needed)
  HAS_SETTING("ADDRESS" => "0x[a-fA-F0-9]+");
  HAS_SETTING("VIRTUAL_SLOT" => "[0-9]+");

  LABEL("32 CH INDIVIDUAL GATE QDC");

  CONTROL("CLASS","caen_v862");

  CONTROL("FIRST_CHANNEL","in0");

  LEMO_INPUT(gate_in);
  LEMO_INPUT(gate_out);
 
  // Pro ECL input 2 eingangssignale. a ist unteres, b oberes flachband
  ECL_INPUT(ina0_15);
  ECL_INPUT(ina16_31);
  
  ECL_INPUT(inb0_15);
  ECL_INPUT(inb16_31);
  
  ECL_OUTPUT(busy);
}

MODULE(CAEN_V965)
{
  MODEL_AS(QDC);

  // TODO: move into a parent class (multiple MODEL_AS needed)
  HAS_SETTING("ADDRESS" => "0x[a-fA-F0-9]+");
  HAS_SETTING("VIRTUAL_SLOT" => "[0-9]+");

  LABEL("16 CH INTEGRATING QDC");

  CONTROL("CLASS","caen_v965");

  CONTROL("FIRST_CHANNEL","in0");

  LEMO_INPUT(gate_in);
  LEMO_INPUT(gate_out);

  LEMO_INPUT(in0_7,SIGNAL);
  LEMO_INPUT(in8_15,SIGNAL);

  LEMO_OUTPUT(busy);
  LEMO_OUTPUT(rst);
  LEMO_OUTPUT(veto);
  LEMO_OUTPUT(fclr);	

}

MODULE(CAEN_V993)
{
  LABEL("DUAL TIMER");
}

UNIT(CAEN_V993_U)
{
  LABEL("DUAL TIMER");

  LEMO_INPUT(start);
  LEMO_INPUT(veto);
  LEMO_INPUT(reset);

  LEMO_OUTPUT(out1_2);
  LEMO_OUTPUT(end_marker);
  LEMO_OUTPUT(not_out);

  ECL_INPUT(ecl_start);
  ECL_INPUT(ecl_veto);
  ECL_INPUT(ecl_reset);

  ECL_OUTPUT(ecl_out);
  ECL_OUTPUT(ecl_end_marker);
}

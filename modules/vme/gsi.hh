// TODO: Define this properly!
MODULE(ENV1)
{
  LABEL("ECL-NIM converter");

  HANDLER("SPLIT_BOX","in_ecl,nim_in=out_ecl,nim_out");

  ECL_INPUT(in_ecl1_16);
  ECL_OUTPUT(out_ecl1_16);
  LEMO_INPUT(nim_in1_16);
  LEMO_OUTPUT(nim_out1_16);
}

// TODO: Define this properly!
MODULE(ENV3)
{
  LABEL("ECL-NIM converter");

  HANDLER("SPLIT_BOX","in_ecl,nim_in=out_ecl,nim_out");

  ECL_INPUT(in_ecl1_16);
  ECL_OUTPUT(out_ecl1_16);
  LEMO_INPUT(nim_in1_16);
  LEMO_OUTPUT(nim_out1_16);
}

MODULE(SAM4)
{
  HAS_SETTING("SAM_NO" => "[0-9]+");

  ECL_INPUT(in_ecl1_8);//control bus
  LEMO_INPUT(in1_2);
  
  // GTB not really ECL! Just to make it easier...
  ECL_INPUT(gtb1);
  ECL_INPUT(gtb2);
}

MODULE(SAM5)
{
  HAS_SETTING("SAM_NO" => "[0-9]+");

  ECL_INPUT(in_ecl1_8); //control bus
  LEMO_INPUT(in1_2);
  
  // GTB not really ECL! Just to make it easier...
  ECL_INPUT(gtb1);
  ECL_INPUT(gtb2);

  //Hack to allow daisy chains
  ECL_OUTPUT(out_ecl1_8);
}

MODULE(TRIDI)
{
  LABEL("TRIDI");

  ECL_INPUT(in_ecl1_8);
  ECL_OUTPUT(out_ecl1_8);

  LEMO_INPUT(in_lemo1_8);
  LEMO_OUTPUT(out_lemo1_8);

  // Since the output goes to two inputs via ecl
  ECL_OUTPUT(outB_ecl1_8);
}

// TODO: Define this properly!
MODULE(TRIVA1)
{
  LABEL("Trigger module");

  ECL_INPUT(in1_8);
  ECL_OUTPUT(out1_8);
}

// TODO: Define this properly!
MODULE(TRIVA3)
{
  LABEL("Trigger module");

  ECL_INPUT(in1_8);
  ECL_OUTPUT(out1_8);
}

// TODO: Define this properly!
MODULE(TRIVA5)
{
  LABEL("Trigger module");

  ECL_INPUT(in1_8);
  ECL_OUTPUT(out1_8);
  
  // Two Triggerbus channels. Not defined yet...need better definition 
  
  ECL_INPUT(tribu1); // upper
  ECL_OUTPUT(tribu2);// lower
}

//TODO: Define this properly!
MODULE(TRIVA7)
{
  LABEL("Trigger module");

  ECL_INPUT(in1_8);
  ECL_OUTPUT(out1_8);
  
  // Two Triggerbus channels. Not defined yet...need better definition 
  
  ECL_INPUT(tribu1); // upper
  ECL_OUTPUT(tribu2);// lower
}

// TODO: Define this properly!
MODULE(VULOM1)
{
  LABEL("Vulom1");

  ECL_INPUT(in_ecl1_16);
  ECL_OUTPUT(out_ecl1_16);
  ECL_INPUT(io_ecl1_16);
  LEMO_INPUT(in1_2);
  LEMO_OUTPUT(out1_2);
}

MODULE(VULOM4)
{
  LABEL("Vulom4");

  ECL_INPUT(in_ecl1_16);
  ECL_OUTPUT(out_ecl1_16);
  ECL_INPUT(io_ecl1_16);
  LEMO_INPUT(in1_2);
  LEMO_OUTPUT(out1_2);
}

MODULE(VUPROM2)
{
  MODEL_AS(TDC);

  HAS_SETTING("ADDRESS" => "0x[a-fA-F0-9]+");
  HAS_SETTING("VIRTUAL_SLOT" => "[0-9]+");

  LABEL("VUPROM2");

  CONTROL("CLASS","vme_vuprom");
  CONTROL("FIRST_CHANNEL","in1");

  LVDS_INPUT(in1_32,SIGNAL);
  LVDS_INPUT(in33_64,SIGNAL);
  LVDS_INPUT(in65_96,SIGNAL);
  // Written input/output but only IN for us
  LVDS_INPUT(in97_128,SIGNAL);
  LVDS_INPUT(in129_160,SIGNAL);
  LVDS_INPUT(in161_192,SIGNAL);
  LVDS_INPUT(in193_224,SIGNAL);
  //LVDS_INPUT(io1_32);
  //LVDS_INPUT(io33_64);
  //LVDS_INPUT(io65_96);
  //LVDS_INPUT(io97_128);

  LVDS_OUTPUT(out1_32);

  LEMO_INPUT(in_nim);
  LEMO_OUTPUT(out_nim);
}

MODULE(VUPROM3)
{
  MODEL_AS(TDC);

  HAS_SETTING("ADDRESS" => "0x[a-fA-F0-9]+");
  HAS_SETTING("VIRTUAL_SLOT" => "[0-9]+");

  LABEL("VUPROM2");

  CONTROL("CLASS","vme_vuprom");
  CONTROL("FIRST_CHANNEL","in1");

  LVDS_INPUT(in1_32,SIGNAL);
  LVDS_INPUT(in33_64,SIGNAL);
  LVDS_INPUT(in65_96,SIGNAL);
  // Written input/output but only IN for us
  LVDS_INPUT(in97_128,SIGNAL);
  LVDS_INPUT(in129_160,SIGNAL);
  LVDS_INPUT(in161_192,SIGNAL);
  LVDS_INPUT(in193_224,SIGNAL);
  //LVDS_INPUT(io1_32);
  //LVDS_INPUT(io33_64);
  //LVDS_INPUT(io65_96);
  //LVDS_INPUT(io97_128);

  LVDS_OUTPUT(out1_32);

  LEMO_INPUT(in_nim);
  LEMO_OUTPUT(out_nim);
}

// 16 SMA 7ps channels.
MODULE(VFTX2_SMA)
{
  LABEL("VFTX2 16 SMA");

  SMA_INPUT(in1_16);
  LEMO_INPUT(trig1_2);
  LEMO_INPUT(reset);
  LVDS_INPUT(clock);
}

// 16 ECL 7ps channels.
MODULE(VFTX2_16)
{
  LABEL("VFTX2 16 SMA");

  ECL_INPUT(in1_16);
  LEMO_INPUT(trig1_2);
  LEMO_INPUT(reset);
  LVDS_INPUT(clock);
}

// 32 10ps channels.
MODULE(VFTX2_32)
{
  LABEL("VFTX2 32");

  ECL_INPUT(in1_32);
  LEMO_INPUT(trig1_2);
  LEMO_INPUT(reset);
  LVDS_INPUT(clock);
}

// 28 10ps channels with ToT.
MODULE(VFTX2_28)
{
  LABEL("VFTX2 28");

  ECL_INPUT(in1_32);
  LEMO_INPUT(trig1_2);
  LEMO_INPUT(reset);
  LVDS_INPUT(clock);
}

MODULE(LECROY2132)
{
  LABEL("Serial controller");

  MODEL_AS(SLOW_CONTROL);

  CONTROL("CLASS","lecroy2132");
  CONTROL("CTRL_FCN","init");
  CONTROL("CTRL_FCN","test");

  // TODO: check connector name
  CONNECTOR(j0); // to first 1440 (1445) 
}

MODULE(LECROY2050)
{
  LABEL("");
}

MODULE(LECROY4448)
{
  MODEL_AS(DAQ_EVENTWISE);

  LABEL("Coincidence Register");

  CONTROL("CLASS","lecroy4448");
  CONTROL("CTRL_FCN","init");
  CONTROL("DAQ_FCN","ARGS:read_clear");

  ECL_INPUT(in1_16);
  ECL_INPUT(in17_32);
  ECL_INPUT(in33_48);

  LEMO_INPUT(gate);
  LEMO_INPUT(clear);
}

MODULE(LECROY4434)
{
  MODEL_AS(SCALER);

  CONTROL("CLASS","lecroy4434");
  CONTROL("CTRL_FCN","init");
  CONTROL("CTRL_FCN","test");
  CONTROL("DAQ_FCN","ARGS:read_clear");

  CONTROL("FIRST_CHANNEL","in1");

  ECL_INPUT(in1_16,SIGNAL);
  ECL_INPUT(in17_32,SIGNAL);

  LEMO_INPUT(load);
  LEMO_INPUT(clear);
  LEMO_INPUT(veto);
}

MODULE(LECROY2365)
{
  MODEL_AS(SLOW_CONTROL);

  LABEL("Logic matrix");

  CONTROL("CLASS","lecroy2365");
  CONTROL("CTRL_FCN","init");
  CONTROL("CTRL_FCN","test");

  ECL_INPUT(in0_15);
  ECL_OUTPUT(in_cont0_15); // Not a real connector, used when ecl cable continues
  ECL_OUTPUT(out0a_7a);
  ECL_OUTPUT(out0b_7b);

  HANDLER("MULTI_IN_OUT","in=in_cont");
}

MODULE(LECROY2228A)
{
  MODEL_AS(TDC);

  HAS_SETTING("GAIN" => "%fps/ch");

  CONTROL("CLASS","lecroy2228a");
  CONTROL("CTRL_FCN","init");
  //CONTROL("CTRL_FCN","test");
  CONTROL("CTRL_FCN","clear");
  CONTROL("DAQ_FCN","ARGS:qnd_read_clear");

  CONTROL("FIRST_CHANNEL","stop0");
  //CONTROL("ELECLOC_NAME","TDCS4418");

  LEMO_INPUT(common_start);
  LEMO_INPUT(common_stop);
  LEMO_INPUT(clear);
  LEMO_INPUT(stop0_7,SIGNAL);
}

MODULE(LECROY2277)
{
  MODEL_AS(TDC);

  HAS_SETTING("GAIN" => "%fps/ch");

  CONTROL("CLASS","lecroy2277");
  CONTROL("CTRL_FCN","init");
  //CONTROL("CTRL_FCN","test");
  CONTROL("CTRL_FCN","clear");
  //CONTROL("DAQ_FCN","ARGS:qnd_read_clear");

  CONTROL("FIRST_CHANNEL","in0");
  //CONTROL("ELECLOC_NAME","TDCS4418");

  ECL_INPUT(common);
  ECL_OUTPUT(common_bridge);
  ECL_INPUT(in0_31,SIGNAL);
}

CRATE(CAENSY1527)
{
  LABEL("High Voltage mainframe");
  // TODO: This crate type has its own controller, is this ok?
  HAS_SETTING("HOSTNAME"=>"[_a-zA-Z0-9-]+");
}

MODULE(CAENA1733)
{
  LABEL("High Voltage slot");

  MODEL_AS(HV);

  CONTROL("CLASS","caena1733");

  CONTROL("FIRST_CHANNEL","out0");

  CONTROL("SETUP_PARAMETER","HIGH_VOLTAGE:out0_27,0V-3000V");

  LEMO_OUTPUT(out0_27,TENSION); // TODO: not LEMO
}

CRATE(LECROY1440)
{
  LABEL("High Voltage mainframe");
}

MODULE(LECROY1442)
{
  // Slots 16,17
  LABEL("Power supply");
}

MODULE(LECROY1441)
{
  // Slots 18
  LABEL("Power supply");
}

MODULE(LECROY1445)
{
  // Slots 19
  LABEL("Controller");

  HAS_SETTING("CRATE_NO" => 
	      "0","1","2","3","4","5","6","7","8",
	      "9","10","11","12","13","14","15","16");

  CONNECTOR(j1); // from controller
  CONNECTOR(j2); // to next 1440 (1445)
}

MODULE(LECROY1443N)
{
  LABEL("High Voltage slot");

  MODEL_AS(HV);

  CONTROL("FIRST_CHANNEL","out0");

  CONTROL("SETUP_PARAMETER","HIGH_VOLTAGE:out0_15,0V-2500V");

  LEMO_OUTPUT(out0_15,TENSION); // TODO: not LEMO
}

//For the XB HV CTRL:
CRATE(ISEG_MICC)
{
  LABEL("PMT Active-Base High-Voltage Control: Mainframe");
}
MODULE(ISEG_MICP)
{
  LABEL("PMT Active-Base High-Voltage Control: Module");
  CONNECTOR(ctrl_out0_15);
}

#include "amplifier.hh"
#include "clock.hh"
#include "constant_fraction.hh"
#include "delay.hh"
#include "gate.hh"
#include "fan_in_fan_out.hh"
#include "hv.hh"
#include "logic.hh"
#include "misc.hh"
#include "nim_ecl_nim.hh"

CRATE(NIM_CRATE)
{
  LABEL("NIM crate");
}

MODULE(OCTAL_SPLIT)
{
  LABEL("Octal Split");

  HANDLER("MULTI_IN_OUT","in=outa,outb");

  LEMO_INPUT(in1_8);
  LEMO_OUTPUT(outa1_8);
  LEMO_OUTPUT(outb1_8);
}

UNIT(OCTAL_SPLIT_U)
{
  LEMO_INPUT(in);

  LEMO_OUTPUT(out1_2);
}

MODULE(PS710)
{
  LABEL("OCTAL DISCRIMINATOR");
}

UNIT(PS710_U)
{
  LEMO_INPUT(in);
  LEMO_OUTPUT(out1_3);
}

MODULE(LA8000)
{
  LABEL("LEVEL ADAPTER");
}

UNIT(LA8000_U)
{
  LEMO_INPUT(in_nim);
  LEMO_INPUT(in_ttl);

  LEMO_OUTPUT(not_out_nim);	//updated:
  LEMO_OUTPUT(not_out_ttl);	//23.Nov.06
	
  LEMO_OUTPUT(out_nim);
  LEMO_OUTPUT(out_ttl);
}

MODULE(LA8010)
{
  LABEL("LEVEL ADAPTER");
}

UNIT(LA8010_U)
{
  LEMO_INPUT(in_nim);
  LEMO_INPUT(in_ttl);

  LEMO_OUTPUT(not_out_nim);	//updated:
  LEMO_OUTPUT(not_out_ttl);	//23.Nov.06
	
  LEMO_OUTPUT(out_nim);
  LEMO_OUTPUT(out_ttl);
}

MODULE(PEGEL)
{
  LABEL("Nim to ttl level adapter");


  LEMO_INPUT(in_nim1_8);
  LEMO_INPUT(in_ttl1_8);

  LEMO_OUTPUT(not_out_nim1_4);	
  LEMO_OUTPUT(not_out_ttl1_4);	
	
  LEMO_OUTPUT(out_nim1_4);
  LEMO_OUTPUT(out_ttl1_4);
}

MODULE(TO4000)
{
  LABEL("TTL OPTOCOUPLED INPUT ISOLATED");
}

UNIT(TO4000_U)
{
  BNC_INPUT(in);
  BNC_OUTPUT(out);
}

MODULE(TC590)
{
  LEMO_INPUT(in);
}

MODULE(CAEN_V93B)
{
  LABEL("DUAL TIMER");
}

UNIT(CAEN_V93B_U)
{
  LABEL("DUAL TIMER");

  LEMO_INPUT(start);
  LEMO_INPUT(veto);
  LEMO_INPUT(reset);

  LEMO_OUTPUT(out1_2);
  LEMO_OUTPUT(end_marker);
  LEMO_OUTPUT(not_out);

  ECL_INPUT(ecl1);
  
  ECL_OUTPUT(ecl_out1_2);
}

MODULE(CAEN_N145)
{
LABEL("DUAL SCALER");

LEMO_OUTPUT(com);

}
UNIT(CAEN_N145_U)
{


LEMO_INPUT(nim_in);
LEMO_INPUT(ttl_in);
LEMO_INPUT(gate_in1);  //only section 1-4
LEMO_OUTPUT(gate_in2); //only section 1+3
LEMO_OUTPUT(carry);   //only section 2+4
LEMO_INPUT(load);     //only section 5
LEMO_INPUT(reset);   //only section 2+4
LEMO_OUTPUT(marker);  //only section 5
LEMO_OUTPUT(out1_2);
}

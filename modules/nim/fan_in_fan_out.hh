/* General model for a fan-in-fan-out.
 */

#define UNIT_FAN_IN_FAN_OUT(Nin,Nout,Nnotout)          \
UNIT(FAN_IN_FAN_OUT_##Nin##_##Nout##_##Nnotout)   \
{                                                 \
  LEMO_INPUT(in1_##Nin);                          \
  LEMO_OUTPUT(out1_##Nout);                       \
  LEMO_OUTPUT(notout1_##Nnotout);                 \
}

UNIT_FAN_IN_FAN_OUT(4,4,2);
UNIT_FAN_IN_FAN_OUT(8,8,4);
UNIT_FAN_IN_FAN_OUT(16,16,8);

/* 4-fold unit of a fan-in-fan-out. */

MODULE(LF8000)
{
  LABEL("LF8000");
}

UNIT(LF8000_4_U)
{
  NAME("Fan in fan out.");

  LEMO_INPUT(in1_4);

  MODEL_AS(FAN_IN_FAN_OUT_4_4_2); // 4 in, 4 out, 2 !out
}

UNIT(LF8000_8_U)
{
  NAME("Fan in fan out.");

  LEMO_INPUT(in1_8);

  MODEL_AS(FAN_IN_FAN_OUT_8_8_4); // 4 in, 4 out, 2 !out
}

UNIT(LF8000_16_U)
{
  NAME("Fan in fan out.");

  LEMO_INPUT(in1_16);

  MODEL_AS(FAN_IN_FAN_OUT_16_16_8); // 4 in, 4 out, 2 !out
}

MODULE(LF4000)
{
  LABEL("LF4000");
}

UNIT(LF4000_4_U)
{
  NAME("Fan in fan out.");

  LEMO_INPUT(in1_4);
  LEMO_OUTPUT(out1_4);
  LEMO_OUTPUT(not_out1_2);

  MODEL_AS(FAN_IN_FAN_OUT_4_4_2); // 4 in, 4 out, 2 !out
}

UNIT(LF4000_8_U)
{
  NAME("Fan in fan out.");

  LEMO_INPUT(in1_8);
  LEMO_OUTPUT(out1_8);
  LEMO_OUTPUT(not_out1_4);

  MODEL_AS(FAN_IN_FAN_OUT_8_8_4);
}

UNIT(LF4000_16_U)
{
  NAME("Fan in fan out.");

  LEMO_INPUT(in1_16);
  LEMO_OUTPUT(out1_18);
  LEMO_OUTPUT(not_out1_8);

  MODEL_AS(FAN_IN_FAN_OUT_16_16_8);
}

MODULE(LF4002) // Most probably identical to LF4000.
{
  LABEL("LF4002");
}

UNIT(LF4002_4_U)
{
  NAME("Fan in fan out.");

  LEMO_INPUT(in1_4);
  LEMO_OUTPUT(out1_4);
  LEMO_OUTPUT(not_out1_2);

  MODEL_AS(FAN_IN_FAN_OUT_4_4_2); // 4 in, 4 out, 2 !out
}

UNIT(LF4002_8_U)
{
  NAME("Fan in fan out.");

  LEMO_INPUT(in1_8);
  LEMO_OUTPUT(out1_8);
  LEMO_OUTPUT(not_out1_4);

  MODEL_AS(FAN_IN_FAN_OUT_8_8_4);
}

UNIT(LF4002_16_U)
{
  NAME("Fan in fan out.");

  LEMO_INPUT(in1_16);
  LEMO_OUTPUT(out1_18);
  LEMO_OUTPUT(not_out1_8);

  MODEL_AS(FAN_IN_FAN_OUT_16_16_8);
}

MODULE(LECROY429A)
{
  LABEL("LECROY429A");
}

UNIT(LECROY429A_U)	// old scheme, to be gradually replaced by new scheme:
{
  MODEL_AS(FAN_IN_FAN_OUT_4_4_2);
}

UNIT(LECROY429A_4_U)	// new scheme
{
  LEMO_INPUT(in1_4);
  LEMO_OUTPUT(out1_4);
  LEMO_OUTPUT(not_out1_2);		
	
  MODEL_AS(FAN_IN_FAN_OUT_4_4_2);
}

UNIT(LECROY429A_8_U)  // new scheme
{
  LEMO_INPUT(in1_8);
  LEMO_OUTPUT(out1_8);
  LEMO_OUTPUT(not_out1_4);
				
  MODEL_AS(FAN_IN_FAN_OUT_8_8_4);
}

UNIT(LECROY429A_16_U)  // new scheme
{
  LEMO_INPUT(in1_16);
  LEMO_OUTPUT(out1_16);
  LEMO_OUTPUT(not_out1_8);
				
  MODEL_AS(FAN_IN_FAN_OUT_16_16_8);
}

MODULE(LECROY428F)
{
  LABEL("LECROY429F");
}

UNIT(LECROY428F_U)	{

  LEMO_INPUT(in1_4);
  LEMO_OUTPUT(out1_4);
  LEMO_OUTPUT(not_out1_2);		
	
  MODEL_AS(FAN_IN_FAN_OUT_4_4_2);
}

MODULE(PHILLIPS748)	// Octal Linear/Logic Fan-Out
	// All eight units have an 'offset' screw.
{
	LABEL("PHILLIPS748");
}

UNIT(PHILLIPS748_U) // the units are not joinable.
{
	LEMO_INPUT(in);
	LEMO_OUTPUT(out1_4);
}

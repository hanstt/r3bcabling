MODULE(CAEN2255B)
{
  LABEL("Dual Timer");
}

UNIT(CAEN2255B_U)
{
  LEMO_INPUT(start);
  LEMO_INPUT(reset);

  LEMO_OUTPUT(end_marker);
  LEMO_OUTPUT(out1_2);
}

MODULE(CAENN93B)
{
  LABEL("Dual Timer");
}

UNIT(CAENN93B_U)
{
  LEMO_INPUT(start);
  LEMO_INPUT(reset);

  LEMO_OUTPUT(end_marker);
  LEMO_OUTPUT(out1_2);
  LEMO_OUTPUT(not_out);
}

MODULE(LRS222)
{
  LABEL("Gate and delay generator");
}

UNIT(LRS222_U)
{
  LEMO_INPUT(start);
  LEMO_INPUT(stop);

  LEMO_INPUT(blank);
  LEMO_INPUT(or);

  LEMO_OUTPUT(out);
  LEMO_OUTPUT(out_neg);
  LEMO_OUTPUT(out_ttl);
  LEMO_OUTPUT(delayed);
}

MODULE(CL1001)
{
  LABEL("UNIVERSAL CLOCK GENERATOR");

  HAS_SETTING("SOURCE" => "10MHz","EXTERNAL");
  HAS_SETTING("FORM"   => "PULSE","SYMMETRIC");
  HAS_SETTING("STATE"  => "ON","STOP");

  LEMO_OUTPUT(out_10MHz);
  LEMO_OUTPUT(out_1MHz);
  LEMO_OUTPUT(out_100kHz);
  LEMO_OUTPUT(out_10kHz);
  LEMO_OUTPUT(out_1kHz);
  LEMO_OUTPUT(out_100Hz); 
  LEMO_OUTPUT(out_10Hz); 
  LEMO_OUTPUT(out_1Hz); 
  LEMO_OUTPUT(out_0d1Hz);

  LEMO_OUTPUT(out_ttl_10MHz);
  LEMO_OUTPUT(out_ttl_1MHz);
  LEMO_OUTPUT(out_ttl_100kHz);
  LEMO_OUTPUT(out_ttl_10kHz);
  LEMO_OUTPUT(out_ttl_1kHz);
  LEMO_OUTPUT(out_ttl_100Hz); 
  LEMO_OUTPUT(out_ttl_10Hz); 
  LEMO_OUTPUT(out_ttl_1Hz); 
  LEMO_OUTPUT(out_ttl_0d1Hz);
}

MODULE(CLOCK_MSE)
{
  LABEL("CLOCK GENERATOR");

  
  LEMO_OUTPUT(out_1MHz);
  LEMO_OUTPUT(out_100kHz);
  LEMO_OUTPUT(out_10kHz);
  LEMO_OUTPUT(out_1kHz);
  LEMO_OUTPUT(out_100Hz); 
  LEMO_OUTPUT(out_10Hz); 
  LEMO_OUTPUT(out_1Hz); 
 

  
}

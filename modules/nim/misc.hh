MODULE(RD2000)
{
  LABEL("Rate divider");
}

UNIT(RD2000_U)
{
  LEMO_INPUT(in);
  LEMO_INPUT(inhibit);

  HAS_SETTING("DOWNSCALE" => "%d");

  LEMO_OUTPUT(divn_1);              // 1/n
  LEMO_OUTPUT(divn_2);              // 1/n
  LEMO_OUTPUT(divn_3);              // 1/n
  LEMO_OUTPUT(not_divn);            // 1-1/n
  LEMO_OUTPUT(one_minusdivn_1);     // !(1-1/n)
  LEMO_OUTPUT(not_one_minusdivn_1); // !(1/n)
}

MODULE(ORTEC462)
{
  HAS_SETTING("PERIOD" => "%fus");
  HAS_SETTING("RANGE"  => "%fus");

  BNC_OUTPUT(start);
  BNC_OUTPUT(stop);

  BNC_INPUT (dispersion_in);
  BNC_OUTPUT(dispersion_out);
}

MODULE(FREQFILT)
{
LABEL("Frequency filter");

LEMO_INPUT(in1_8);
LEMO_OUTPUT(out1_8);
}

MODULE(MSCF16)
{
  HAS_SETTING("INCAP" => "%dnC");
  HAS_SETTING("POLARITY" => "POS","NEG");

  HANDLER("MULTI_IN_OUT","in=t,e");
  //HANDLER("SPLIT_BOX","in=t,e");

  LEMO_INPUT(in1_16);	// Inputs

  LEMO_INPUT(rc);	// RC control
  LEMO_INPUT(mc);	// Multiplicity chain

  ECL_OUTPUT(t1_16);	// ECL Time outputs
  ECL_OUTPUT(e1_16);	// ECL Amplitude outputs

  LEMO_OUTPUT(or);
  LEMO_OUTPUT(mult);

  LEMO_OUTPUT(analog_sum);
}

MODULE(MESYTEC_CFD16)
{
  LABEL("Mesytec 16-channel CFD");
//   HAS_SETTING("INCAP" => "%dnC");
  HAS_SETTING("POLARITY" => "POS","NEG");

  HANDLER("MULTI_IN_OUT","in=t,e");

  LEMO_INPUT(in0_15);	// Inputs

  LEMO_INPUT(rc);	// RC control
  LEMO_INPUT(veto);	// Veto trigger

  ECL_OUTPUT(t0_15);	// ECL Time outputs
  ECL_OUTPUT(e0_15);	// ECL Amplitude outputs

  LEMO_OUTPUT(or);
  LEMO_OUTPUT(mult);
  LEMO_OUTPUT(cur);
}

MODULE(GG8000)
{
  LABEL("Octal Gate Generator");
}

UNIT(GG8000_U)
{
  //HAS_SETTING("DELAY");

  LEMO_INPUT(in);

  LEMO_OUTPUT(out_neg);
  LEMO_OUTPUT(out1_2);
}

MODULE(SH8000)
{
  LABEL("SH8000");


}

UNIT(SH8000_U)
{

  LEMO_INPUT(in);
  LEMO_INPUT(not_out);
  LEMO_INPUT(out1_2);
}

MODULE(PHI794)
{
  LABEL("Quad Gate Generator");
}

UNIT(PHI794_U)
{
  LEMO_INPUT(programming); // TODO: this is actually an analog input
  LEMO_INPUT(in_or);
  LEMO_INPUT(reset);
  LEMO_INPUT(veto);
  LEMO_INPUT(trig);

  LEMO_OUTPUT(out_ttl);
  LEMO_OUTPUT(delay);
  LEMO_OUTPUT(gate);
  LEMO_OUTPUT(gate_inv);
  ECL_OUTPUT(ecl);
}

MODULE(CF8000)
{
  LABEL("OCT. CF. DISCR.");

  LEMO_OUTPUT(out_or);
}

UNIT(CF8000_U)
{
  LABEL("OCT. CF. DISCR. UNIT");

  HAS_SETTING("DELAY" => "%fns");
  HAS_SETTING("FRACTION" => "%f");

  HANDLER("MULTI_IN_OUT","in=outA,outB1,outB2,out_ecl,out_e");

  LEMO_INPUT(in);
  LEMO_OUTPUT(outA);
  LEMO_OUTPUT(outB1_2);

  ECL_OUTPUT(out_ecl); // back
  LEMO_OUTPUT(out_e);  // back
}
 
MODULE(CF8201)
{
  HANDLER("MULTI_IN_OUT","in=out,ta_ecl,tb_ecl");

  LABEL("OCT. CF. DISCR.");

  // TODO: This should more look like a CF8100_SERIES

  LEMO_INPUT(in1_8);
  LEMO_OUTPUT(out1_8);
  LEMO_OUTPUT(out_or);
  ECL_OUTPUT(ta_ecl1_8);
  ECL_OUTPUT(tb_ecl1_8);
}

#!/usr/bin/perl
use strict;
use warnings;


######################################
# Read the name mapping from file                                                                                         
######################################
my $mapping; # Hash reference
open(IN,"gen/master.ytc") || die "Could not open gen/master.ytc";
while (my $line = <IN>) {                          
  if ($line =~ /\s*NAME_MAP\((\w+)\s*,\s*(\w+)\)/) {
    my $from = $1;
    my $to = $2;
    if ($from =~ /DPL/) {           ### NeuLAND name mapping
      $mapping->{$from} = $to;
      $mapping->{$to} = $from;
    }
    #print "  Mapping: $from -> $mapping->{$from}\n";
    #print "  Mapping: $to -> $mapping->{$to}\n";
  }
}
close(IN);
#print "done\n";

#############################################################
# Read from standard input (cfg_sig_beam or similar usually)
#############################################################
while (my $line=<>) {
  if ($line =~ /DPL(\d{2})_(\d{2,3})_(\d{1})/) {            ### NeuLAND name mapping
    chomp($line);
    my $pln = $1;
    my $pdl = $2;
    my $pmt = $3;
    my $find = "DPL$pln";
    #print "Found plane $pln, paddle $pdl, pmt $pmt ($find)\n";
    #print ">> ".$line."\n";
    foreach my $key (keys %$mapping) {
      if ($key =~ /DPL(\d{2})_(\d{2,3})_(\d{2,3})/) {
        my $pln_map = $1;
        my $pdl_from = $2;
        my $pdl_to = $3;
        if ($pln == $pln_map && $pdl >= $pdl_from && $pdl <= $pdl_to) {
          my $replace = $mapping->{$key};
          my $map_pdl;
          $find = sprintf("%s_%02d", $find,$pdl);
          if ($pdl > 50) { 
            $map_pdl = $pdl - 50; 
          } else {
            $map_pdl = $pdl;
          }
          $replace = sprintf("%s_%02d", $replace,$map_pdl);
          #print "  Found matching mapping for $find -> $replace ($key)\n";
          $line =~ s/$find/$replace/g;
          print "$line /* Mapped $find -> $replace */\n";
        }
      }
    }
  }
}


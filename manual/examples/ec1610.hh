/* Nim-ecl-nim converter, 16 channels.
 */
MODULE(EC1610)
{
  HANDLER("MULTI_IN_OUT","in,in_ecl=out,out_ecl");
  
  LEMO_INPUT(in1_16);
  ECL_INPUT(in_ecl1_16);
  
  LEMO_OUTPUT(out1_16);
  ECL_OUTPUT(out_ecl1_16);
}

MODULE(CF8000)
{
  LABEL("OCT. CF. DISCR.");
  
  LEMO_OUTPUT(out_or);
}

UNIT(CF8000_U)
{
  LABEL("OCT. CF. DISCR. UNIT");
  
  HAS_SETTING("DELAY" => "%fns");
  HAS_SETTING("FRACTION" => "%f");
  
  HANDLER("MULTI_IN_OUT","in=outA,outB1,outB2,out_ecl,out_e");
  
  LEMO_INPUT(in);
  LEMO_OUTPUT(outA);
  LEMO_OUTPUT(outB1_2);
  
  ECL_OUTPUT(out_ecl); // back
  LEMO_OUTPUT(out_e);  // back
}

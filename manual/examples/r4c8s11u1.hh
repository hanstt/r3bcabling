CF8000_U(r4c8s11u1)
{
  SETTING("DELAY"    => "4ns");
  SETTING("FRACTION" => "0.4");
 in:    "11" <- "11" , r5c1s2/out1;
 outA:                 r5c5(DELAY)/in1;
 outB2:                r4c8s13u1(LOGIC_UNIT)/inA;
}

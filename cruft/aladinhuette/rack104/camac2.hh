  
CAMAC_CRATE(r104c2)
{
  ;
}

CF8104(r104c2s4)
{
  SERIAL("S305");
  
 in1_8: "SPLF_F01D" <- , r104c1s4(SPLIT)/t1_8;  
 tb1_8: "DL_F01D"  -> , r104c5s1(DELAY)/in1_8;
}

CF8104(r104c2s5)
{
  SERIAL("S246");
  
 in1_8: "SPLF_F01E" <- , r104c1s4(SPLIT)/t9_16;  
 tb1_8: "DL_F01E"  -> , r104c5s1(DELAY)/in9_16;
}

CF8104(r104c2s6)
{
  SERIAL("S291");
  
 in1_8: "SPLF_F02D" <- , r104c1s5(SPLIT)/t1_8;  
 tb1_8: "DL_F02D"  -> , r104c5s3(DELAY)/in1_8;
}

CF8104(r104c2s7)
{
  SERIAL("S257");
  
 in1_8: "SPLF_F02E" <- , r104c1s5(SPLIT)/t9_16;  
 tb1_8: "DL_F02E"  -> , r104c5s3(DELAY)/in9_16;
}

CF8104(r104c2s8)
{
  SERIAL("S390");
  
 in1_8: "SPLF_F03D" <- , r104c1s6(SPLIT)/t1_8;  
 tb1_8: "DL_F03D"  -> , r104c5s5(DELAY)/in1_8;
}

CF8104(r104c2s9)
{
  SERIAL("S287");
  
 in1_8: "SPLF_F03E" <- , r104c1s6(SPLIT)/t9_16;  
 tb1_8: "DL_F03E"  -> , r104c5s5(DELAY)/in9_16;
}

CF8104(r104c2s10)
{
  SERIAL("S299");
  
 in1_8: "SPLF_F04D" <- , r104c1s7(SPLIT)/t1_8;  
 tb1_8: "DL_F04D"  -> , r104c5s7(DELAY)/in1_8;
}

CF8104(r104c2s11)
{
  SERIAL("I308");
  
 in1_8: "SPLF_F04E" <- , r104c1s7(SPLIT)/t9_16;  
 tb1_8: "DL_F04E"  -> , r104c5s7(DELAY)/in9_16;
}

CF8104(r104c2s12)
{
  SERIAL("I495");
  
 in1_8: "SPLF_F05D" <- , r104c1s8(SPLIT)/t1_8;  
 tb1_8: "DL_F05D"  -> , r104c5s9(DELAY)/in1_8;
}

CF8104(r104c2s13)
{
  SERIAL("I258");
  
 in1_8: "SPLF_F05E" <- , r104c1s8(SPLIT)/t9_16;  
 tb1_8: "DL_F05E"  -> , r104c5s9(DELAY)/in9_16;
}

CF8104(r104c2s14)
{
  SERIAL("I276");
  
 in1_8: "SPLF_F06D" <- , r104c1s9(SPLIT)/t1_8;  
 tb1_8: "DL_F06D"  -> , r104c5s11(DELAY)/in1_8;
}

CF8104(r104c2s15)
{
  SERIAL("I267");
  
 in1_8: "SPLF_F06E" <- , r104c1s9(SPLIT)/t9_16;  
 tb1_8: "DL_F06E"  -> , r104c5s11(DELAY)/in9_16;
}


CF8104(r104c2s16)
{
  SERIAL("S295");
  
 in1_8: "SPLF_F07D" <- , r104c1s10(SPLIT)/t1_8;  
 tb1_8: "DL_F07D"  -> , r104c5s13(DELAY)/in1_8;
}

CF8104(r104c2s17)
{
  SERIAL("I466");
  
 in1_8: "SPLF_F07E" <- , r104c1s10(SPLIT)/t9_16;  
 tb1_8: "DL_F07E"  -> , r104c5s13(DELAY)/in9_16;
}

CF8104(r104c2s18)
{
  SERIAL("I256");
  
 in1_8: "SPLF_F08D" <- , r104c1s11(SPLIT)/t1_8;  
 tb1_8: "DL_F08D"  -> , r104c5s15(DELAY)/in1_8;
}

CF8104(r104c2s19)
{
  SERIAL("I275");
  
 in1_8: "SPLF_F08E" <- , r104c1s11(SPLIT)/t9_16;  
 tb1_8: "DL_F08E"  -> , r104c5s15(DELAY)/in9_16;
}

CF8104(r104c2s20)
{
  SERIAL("I286");
  
 in1_8: "SPLF_F09D" <- , r104c1s12(SPLIT)/t1_8;  
 tb1_8: "DL_F09D"  -> , r105c4s19(DELAY)/in1_8;
}

CF8104(r104c2s21)
{
  SERIAL("I235");
  
 in1_8: "SPLF_F09E" <- , r104c1s12(SPLIT)/t9_16;  
 tb1_8: "DL_F09E"  -> , r105c4s19(DELAY)/in9_16;
}

GTBC4(r104c2s24)
{
  SETTING("CRATE_NO" => "2");

 gtb_master: r104c4s24/gtb_slave;
}

PSD1600(r104c1s4)
{
 in1_8: "F4t" <- , rTWAFc41/out1_8;
 in9_16:"F4b" <- , rTWAFc42/out1_8;
 
 e1_8:  "QDC_F01D" -> , r105c2s16/in0_7;
 e9_16: "QDC_F01E" -> , r105c2s16/in8_15;

 t1_8:  "CFD_F01D" -> , r104c2s4/in1_8;
 t9_16: "CFD_F01E" -> , r104c2s5/in1_8;

}

PSD1600(r104c1s5)
{
 in1_8: "F5t" <- , rTWAFc51/out1_8;
 in9_16:"F5b" <- , rTWAFc52/out1_8;
 
 e1_8:  "QDC_F02D" -> , r105c2s16/in16_23;
 e9_16: "QDC_F02E" -> , r105c2s16/in24_31;
 
 t1_8:  "CFD_F02D" -> , r104c2s6/in1_8;
 t9_16: "CFD_F02E" -> , r104c2s7/in1_8;

}

PSD1600(r104c1s6)
{
 in1_8: "F6t" <- , rTWAFc61/out1_8;
 in9_16:"F6b" <- , rTWAFc62/out1_8;
 
 e1_8:  "QDC_F03D" -> , r105c2s16/in32_39;
 e9_16: "QDC_F03E" -> , r105c2s16/in40_47;

 t1_8:  "CFD_F03D" -> , r104c2s8/in1_8;
 t9_16: "CFD_F03E" -> , r104c2s9/in1_8;

}

PSD1600(r104c1s7)
{
 in1_8: "F7t" <- , rTWAFc71/out1_8;
 in9_16:"F7b" <- , rTWAFc72/out1_8;
 
 e1_8:  "QDC_F04D" -> , r105c2s16/in48_55;
 e9_16: "QDC_F04E" -> , r105c2s16/in56_63;
 
 t1_8:  "CFD_F04D" -> , r104c2s10/in1_8;
 t9_16: "CFD_F04E" -> , r104c2s11/in1_8;

}

PSD1600(r104c1s8)
{
 in1_8: "F8t" <- , rTWAFc81/out1_8;
 in9_16:"F8b" <- , rTWAFc82/out1_8;
 
 e1_8:  "QDC_F05D" -> , r105c2s16/in64_71;
 e9_16: "QDC_F05E" -> , r105c2s16/in72_79;

 t1_8:  "CFD_F05D" -> , r104c2s12/in1_8;
 t9_16: "CFD_F05E" -> , r104c2s13/in1_8;

}

PSD1600(r104c1s9)
{
 in1_8: "F9t" <- , rTWAFc91/out1_8;
 in9_16:"F9b" <- , rTWAFc92/out1_8;
 
 e1_8:  "QDC_F06D" -> , r105c2s16/in80_87;
 e9_16: "QDC_F06E" -> , r105c2s16/in88_95;
 
 t1_8:  "CFD_F06D" -> , r104c2s14/in1_8;
 t9_16: "CFD_F06E" -> , r104c2s15/in1_8;

}

PSD1600(r104c1s10)
{
 in1_8: "F10t" <- , rTWAFc101/out1_8;
 in9_16:"F10b" <- , rTWAFc102/out1_8;
 
 e1_8:  "QDC_F07D" -> , r105c2s15/in0_7;
 e9_16: "QDC_F07E" -> , r105c2s15/in8_15;

 t1_8:  "CFD_F07D" -> , r104c2s16/in1_8;
 t9_16: "CFD_F07E" -> , r104c2s17/in1_8;

}

PSD1600(r104c1s11)
{
 in1_8: "F11t" <- , rTWAFc111/out1_8;
 in9_16:"F11b" <- , rTWAFc112/out1_8;
 
 e1_8:  "QDC_F08D" -> , r105c2s15/in16_23;
 e9_16: "QDC_F08E" -> , r105c2s15/in24_31;
 
 t1_8:  "CFD_F08D" -> , r104c2s18/in1_8;
 t9_16: "CFD_F08E" -> , r104c2s19/in1_8;

}

PSD1600(r104c1s12)
{
 in1_8: "F12t" <- , rTWAFc121/out1_8;
 in9_16:"F12b" <- , rTWAFc122/out1_8;
 
 e1_8:  "QDC_F09D" -> , r105c2s15/in32_39;
 e9_16: "QDC_F09E" -> , r105c2s15/in40_47;
 
 t1_8:  "CFD_F09D" -> , r104c2s20/in1_8;
 t9_16: "CFD_F09E" -> , r104c2s21/in1_8;

}

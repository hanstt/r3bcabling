CAMAC_CRATE(r104c5)
{
  ;
}

//FRONT

DL1610(r104c5s1)
{
 in1_8:  <- "CFD_F01D" , r104c2s4/tb1_8;
 in9_16: <- "CFD_F01E" , r104c2s5/tb1_8;

 outa1_8:    r105c2s10(TDC)/in0_7;
 outa9_16:   r105c2s10(TDC)/in8_15;
}

DL1610(r104c5s3)
{
 in1_8:  <- "CFD_F02D" , r104c2s6/tb1_8;
 in9_16: <- "CFD_F02E" , r104c2s7/tb1_8;

 outa1_8:    r105c2s10(TDC)/in16_23;
 outa9_16:   r105c2s10(TDC)/in24_31;
}

DL1610(r104c5s5)
{
 in1_8:  <- "CFD_F03D" , r104c2s8/tb1_8;
 in9_16: <- "CFD_F03E" , r104c2s9/tb1_8;

 outa1_8:    r105c2s10(TDC)/in32_39;
 outa9_16:   r105c2s10(TDC)/in40_47;
}

DL1610(r104c5s7)
{
 in1_8:  <- "CFD_F04D" , r104c2s10/tb1_8;
 in9_16: <- "CFD_F04E" , r104c2s11/tb1_8;

 outa1_8:    r105c2s10(TDC)/in48_55;
 outa9_16:   r105c2s10(TDC)/in56_63;
}

DL1610(r104c5s9)
{
 in1_8:  <- "CFD_F05D" , r104c2s12/tb1_8;
 in9_16: <- "CFD_F05E" , r104c2s13/tb1_8;

 outa1_8:    r105c2s9(TDC)/in0_7;
 outa9_16:   r105c2s9(TDC)/in8_15;
}

DL1610(r104c5s11)
{
 in1_8:  <- "CFD_F06D" , r104c2s14/tb1_8;
 in9_16: <- "CFD_F06E" , r104c2s15/tb1_8;

 outa1_8:    r105c2s9(TDC)/in16_23;
 outa9_16:   r105c2s9(TDC)/in24_31;
}

DL1610(r104c5s13)
{
 in1_8:  <- "CFD_F07D" , r104c2s16/tb1_8;
 in9_16: <- "CFD_F07E" , r104c2s17/tb1_8;

 outa1_8:    r105c2s9(TDC)/in32_39;
 outa9_16:   r105c2s9(TDC)/in40_47;
}

DL1610(r104c5s15)
{
 in1_8:  <- "CFD_F08D" , r104c2s18/tb1_8;
 in9_16: <- "CFD_F08E" , r104c2s19/tb1_8;

 outa1_8:   r105c2s9(TDC)/in48_55;
 outa9_16:  r105c2s9(TDC)/in56_63;
}

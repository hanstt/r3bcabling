
CAMAC_CRATE(r104c4)
{
  ;
}

CF8104(r104c4s4)
{
  SERIAL("I285");
  
 in1_8: "SPLF_B01D" <- , r105c1s4(SPLIT)/t1_8;  
 tb1_8: "DL_B01D"  -> , r105c4s1(DELAY)/in1_8;
}

CF8104(r104c4s5)
{
  SERIAL("S384");
  
 in1_8: "SPLF_B01E" <- , r105c1s4(SPLIT)/t9_16;  
 tb1_8: "DL_B01E"  -> , r105c4s1(DELAY)/in9_16;
}

CF8104(r104c4s6)
{
  SERIAL("S319");
  
 in1_8: "SPLF_B02D" <- , r105c1s5(SPLIT)/t1_8;  
 tb1_8: "DL_B02D"  -> , r105c4s3(DELAY)/in1_8;
}

CF8104(r104c4s7)
{
  SERIAL("S251");
  
 in1_8: "SPLF_B02E" <- , r105c1s5(SPLIT)/t9_16;  
 tb1_8: "DL_B02E"  -> , r105c4s3(DELAY)/in9_16;
}

CF8104(r104c4s8)
{
  SERIAL("S304");
  
 in1_8: "SPLF_B03D" <- , r105c1s6(SPLIT)/t1_8;  
 tb1_8: "DL_B03D"  -> , r105c4s5(DELAY)/in1_8;
}

CF8104(r104c4s9)
{
  SERIAL("S253");
  
 in1_8: "SPLF_B03E" <- , r105c1s6(SPLIT)/t9_16;  
 tb1_8: "DL_B03E"  -> , r105c4s5(DELAY)/in9_16;
}

CF8104(r104c4s10)
{
  SERIAL("S303");
  
 in1_8: "SPLF_B04D" <- , r105c1s7(SPLIT)/t1_8;  
 tb1_8: "DL_B04D"  -> , r105c4s7(DELAY)/in1_8;
}

CF8104(r104c4s11)
{
  SERIAL("S311");
  
 in1_8: "SPLF_B04E" <- , r105c1s7(SPLIT)/t9_16;  
 tb1_8: "DL_B04E"  -> , r105c4s7(DELAY)/in9_16;
}

CF8104(r104c4s12)
{
  SERIAL("S261");
  
 in1_8: "SPLF_B05D" <- , r105c1s8(SPLIT)/t1_8;  
 tb1_8: "DL_B05D"  -> , r105c4s9(DELAY)/in1_8;
}

CF8104(r104c4s13)
{
  SERIAL("I318");
  
 in1_8: "SPLF_B05E" <- , r105c1s8(SPLIT)/t9_16;  
 tb1_8: "DL_B05E"  -> , r105c4s9(DELAY)/in9_16;
}

CF8104(r104c4s14)
{
  SERIAL("I292");
  
 in1_8: "SPLF_B06D" <- , r105c1s9(SPLIT)/t1_8;  
 tb1_8: "DL_B06D"  -> , r105c4s11(DELAY)/in1_8;
}

CF8104(r104c4s15)
{
  SERIAL("S265");
  
 in1_8: "SPLF_B06E" <- , r105c1s9(SPLIT)/t9_16;  
 tb1_8: "DL_B06E"  -> , r105c4s11(DELAY)/in9_16;
}


CF8104(r104c4s16)
{
  SERIAL("S289");
  
 in1_8: "SPLF_B07D" <- , r105c1s10(SPLIT)/t1_8;  
 tb1_8: "DL_B07D"  -> , r105c4s13(DELAY)/in1_8;
}

CF8104(r104c4s17)
{
  SERIAL("S277");
  
 in1_8: "SPLF_B07E" <- , r105c1s10(SPLIT)/t9_16;  
 tb1_8: "DL_B07E"  -> , r105c4s13(DELAY)/in9_16;
}

CF8104(r104c4s18)
{
  SERIAL("S269");
  
 in1_8: "SPLF_B08D" <- , r105c1s11(SPLIT)/t1_8;  
 tb1_8: "DL_B08D"  -> , r105c4s15(DELAY)/in1_8;
}

CF8104(r104c4s19)
{
  SERIAL("S309");
  
 in1_8: "SPLF_B08E" <- , r105c1s11(SPLIT)/t9_16;  
 tb1_8: "DL_B08E"  -> , r105c4s15(DELAY)/in9_16;
}

CF8104(r104c4s20)
{
  SERIAL("I272");
  
 in1_8: "SPLF_B09D" <- , r105c1s12(SPLIT)/t1_8;  
 tb1_8: "DL_B09D"  -> , r105c4s17(DELAY)/in1_8;
}

CF8104(r104c4s21)
{
  SERIAL("I297");
  
 in1_8: "SPLF_B09E" <- , r105c1s12(SPLIT)/t9_16;  
 tb1_8: "DL_B09E"  -> , r105c4s17(DELAY)/in9_16;
}

GTBC4(r104c4s24)
{
  SETTING("CRATE_NO" => "1");

 gtb_master: r110c1s4/gtb;
 gtb_slave:  r104c2s24/gtb_master;
}

PC(r110c1)
{
  SETTING("HOSTNAME"=>"x86-25");
}

PCIGTB3(r110c1s4)
{
 gtb: r104c4s24/gtb_master;
}


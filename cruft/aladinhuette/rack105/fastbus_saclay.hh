
FASTBUS_CRATE(r105c2)
{
  ;
}

NGF(r105c2s0)
{
  ;
  // in1: r105c4s5u2/out2;
  // out3: r105c4s5u1/in2;
}

TRIVA3(r105c2s1)
{
  ;
}

RIO3(r105c2s2)
{
  SETTING("HOSTNAME"=>"r2f-5");
}

LECROY1875(r105c2s10)
{
  SERIAL("TDC10");

 in0_7:   "DL_F01D" <- , r104c5s1/outa1_8;
 in8_15:  "DL_F01E" <- , r104c5s1/outa9_16;
 in16_23: "DL_F02D" <- , r104c5s3/outa1_8;
 in24_31: "DL_F02E" <- , r104c5s3/outa9_16;
 in32_39: "DL_F03D" <- , r104c5s5/outa1_8;
 in40_47: "DL_F03E" <- , r104c5s5/outa9_16;
 in48_55: "DL_F04D" <- , r104c5s7/outa1_8;
 in56_63: "DL_F04E" <- , r104c5s7/outa9_16;
}

LECROY1875(r105c2s9)
{
  SERIAL("TDC9");

 in0_7:   "DL_F05D" <- , r104c5s9/outa1_8;
 in8_15:  "DL_F05E" <- , r104c5s9/outa9_16;
 in16_23: "DL_F06D" <- , r104c5s11/outa1_8;
 in24_31: "DL_F06E" <- , r104c5s11/outa9_16;
 in32_39: "DL_F07D" <- , r104c5s13/outa1_8;
 in40_47: "DL_F07E" <- , r104c5s13/outa9_16;
 in48_55: "DL_F08D" <- , r104c5s15/outa1_8;
 in56_63: "DL_F08E" <- , r104c5s15/outa9_16;
}

LECROY1875(r105c2s8)
{
  SERIAL("TDC8");

 in0_7:   "DL_F09D" <- , r105c4s19/outa1_8;
 in8_15:  "DL_F09E" <- , r105c4s19/outa9_16;
 in16_23: "DL_B01D" <- , r105c4s1/outa1_8;
 in24_31: "DL_B01E" <- , r105c4s1/outa9_16;
 in32_39: "DL_B02D" <- , r105c4s3/outa1_8;
 in40_47: "DL_B02E" <- , r105c4s3/outa9_16;
 in48_55: "DL_B03D" <- , r105c4s5/outa1_8;
 in56_63: "DL_B03E" <- , r105c4s5/outa9_16;
}


LECROY1875(r105c2s7)
{
  SERIAL("TDC7");

 in0_7:   "DL_B04D" <- , r105c4s7/outa1_8;
 in8_15:  "DL_B04E" <- , r105c4s7/outa9_16;
 in16_23: "DL_B05D" <- , r105c4s9/outa1_8;
 in24_31: "DL_B05E" <- , r105c4s9/outa9_16;
 in32_39: "DL_B06D" <- , r105c4s11/outa1_8;
 in40_47: "DL_B06E" <- , r105c4s11/outa9_16;
 in48_55: "DL_B07D" <- , r105c4s13/outa1_8;
 in56_63: "DL_B07E" <- , r105c4s13/outa9_16;
}

LECROY1875(r105c2s6)
{
  SERIAL("TDC6");

 in0_7:   "DL_B08D" <- , r105c4s15/outa1_8;
 in8_15:  "DL_B08E" <- , r105c4s15/outa9_16;
 in16_23: "DL_B09D" <- , r105c4s17/outa1_8;
 in24_31: "DL_B09E" <- , r105c4s17/outa9_16;
}

LECROY1885F(r105c2s16)
{
  SERIAL("QDC16");

 in0_7:   "SPLB_F01D" <- , r104c1s4/e1_8;
 in8_15:  "SPLB_F01E" <- , r104c1s4/e9_16;
 in16_23: "SPLB_F02D" <- , r104c1s5/e1_8;
 in24_31: "SPLB_F02E" <- , r104c1s5/e9_16;
 in32_39: "SPLB_F03D" <- , r104c1s6/e1_8;
 in40_47: "SPLB_F03E" <- , r104c1s6/e9_16;
 in48_55: "SPLB_F04D" <- , r104c1s7/e1_8;
 in56_63: "SPLB_F04E" <- , r104c1s7/e9_16;
 in64_71: "SPLB_F05D" <- , r104c1s8/e1_8;
 in72_79: "SPLB_F05E" <- , r104c1s8/e9_16;
 in80_87: "SPLB_F06D" <- , r104c1s9/e1_8;
 in88_95: "SPLB_F06E" <- , r104c1s9/e9_16;
}

LECROY1885F(r105c2s15)
{
  SERIAL("QDC15");

 in0_7:   "SPLB_F07D" <- , r104c1s10/e1_8;
 in8_15:  "SPLB_F07E" <- , r104c1s10/e9_16;
 in16_23: "SPLB_F08D" <- , r104c1s11/e1_8;
 in24_31: "SPLB_F08E" <- , r104c1s11/e9_16;
 in32_39: "SPLB_F09D" <- , r104c1s12/e1_8;
 in40_47: "SPLB_F09E" <- , r104c1s12/e9_16;
 in48_55: "SPLB_B01D" <- , r105c1s4/e1_8;
 in56_63: "SPLB_B01E" <- , r105c1s4/e9_16;
 in64_71: "SPLB_B02D" <- , r105c1s5/e1_8;
 in72_79: "SPLB_B02E" <- , r105c1s5/e9_16;
 in80_87: "SPLB_B03D" <- , r105c1s6/e1_8;
 in88_95: "SPLB_B03E" <- , r105c1s6/e9_16;
}

LECROY1885F(r105c2s14)
{
  SERIAL("QDC14");

 in0_7:   "SPLB_B04D" <- , r105c1s7/e1_8;
 in8_15:  "SPLB_B04E" <- , r105c1s7/e9_16;
 in16_23: "SPLB_B05D" <- , r105c1s8/e1_8;
 in24_31: "SPLB_B05E" <- , r105c1s8/e9_16;
 in32_39: "SPLB_B06D" <- , r105c1s9/e1_8;
 in40_47: "SPLB_B06E" <- , r105c1s9/e9_16;
 in48_55: "SPLB_B07D" <- , r105c1s10/e1_8;
 in56_63: "SPLB_B07E" <- , r105c1s10/e9_16;
 in64_71: "SPLB_B08D" <- , r105c1s11/e1_8;
 in72_79: "SPLB_B08E" <- , r105c1s11/e9_16;
 in80_87: "SPLB_B09D" <- , r105c1s12/e1_8;
 in88_95: "SPLB_B09E" <- , r105c1s12/e9_16;
}






// CAT module
LECROY1810(r105c2s25)
{
  ;
  //  tdc_stop: r105c2s3u2/out1;
  //  adc_gate: r105c2s3u1/out1; // aka ARM
}

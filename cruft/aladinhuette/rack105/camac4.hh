CAMAC_CRATE(r105c4)
{
  ;
}

//BACK

DL1610(r105c4s1)
{
 in1_8:  <- "CFD_B01D" , r104c4s4/tb1_8;
 in9_16: <- "CFD_B01E" , r104c4s5/tb1_8;

 outa1_8:  r105c2s8(TDC)/in16_23;
 outa9_16: r105c2s8(TDC)/in24_31;
}

DL1610(r105c4s3)
{
 in1_8:  <- "CFD_B02D" , r104c4s6/tb1_8;
 in9_16: <- "CFD_B02E" , r104c4s7/tb1_8;

 outa1_8:  r105c2s8(TDC)/in32_39;
 outa9_16: r105c2s8(TDC)/in40_47;
}

DL1610(r105c4s5)
{
 in1_8:  <- "CFD_B03D" , r104c4s8/tb1_8;
 in9_16: <- "CFD_B03E" , r104c4s9/tb1_8;

 outa1_8:  r105c2s8(TDC)/in48_55;
 outa9_16: r105c2s8(TDC)/in56_63;
}

DL1610(r105c4s7)
{
 in1_8:  <- "CFD_B04D" , r104c4s10/tb1_8;
 in9_16: <- "CFD_B04E" , r104c4s11/tb1_8;

 outa1_8:  r105c2s7(TDC)/in0_7;
 outa9_16: r105c2s7(TDC)/in8_15;
}

DL1610(r105c4s9)
{
 in1_8:  <- "CFD_B05D" , r104c4s12/tb1_8;
 in9_16: <- "CFD_B05E" , r104c4s13/tb1_8;

 outa1_8:  r105c2s7(TDC)/in16_23;
 outa9_16: r105c2s7(TDC)/in24_31;
}

DL1610(r105c4s11)
{
 in1_8:  <- "CFD_B06D" , r104c4s14/tb1_8;
 in9_16: <- "CFD_B06E" , r104c4s15/tb1_8;

 outa1_8:  r105c2s7(TDC)/in32_39;
 outa9_16: r105c2s7(TDC)/in40_47;
}

DL1610(r105c4s13)
{
 in1_8:  <- "CFD_B07D" , r104c4s16/tb1_8;
 in9_16: <- "CFD_B07E" , r104c4s17/tb1_8;

 outa1_8:  r105c2s7(TDC)/in48_55;
 outa9_16: r105c2s7(TDC)/in56_63;
}

DL1610(r105c4s15)
{
 in1_8:  <- "CFD_B08D" , r104c4s18/tb1_8;
 in9_16: <- "CFD_B08E" , r104c4s19/tb1_8;

 outa1_8:  r105c2s6(TDC)/in0_7;
 outa9_16: r105c2s6(TDC)/in8_15;
}

DL1610(r105c4s17)
{
 in1_8:  <- "CFD_B09D" , r104c4s20/tb1_8;
 in9_16: <- "CFD_B09E" , r104c4s21/tb1_8;

 outa1_8:  r105c2s6(TDC)/in16_23;
 outa9_16: r105c2s6(TDC)/in24_31;
}

DL1610(r105c4s19)
{
 in1_8:  <- "CFD_F09D" , r104c2s20/tb1_8;
 in9_16: <- "CFD_F09E" , r104c2s21/tb1_8;

 outa1_8:  r105c2s8(TDC)/in0_7;
 outa9_16: r105c2s8(TDC)/in8_15;
}

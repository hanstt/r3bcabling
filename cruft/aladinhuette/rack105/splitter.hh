PSD1600(r105c1s4)
{
 in1_8: "B4t" <- , rTWABc41/out1_8;
 in9_16:"B4b" <- , rTWABc42/out1_8;
 
 e1_8:  "QDC_B01D" -> , r105c2s15/in48_55;
 e9_16: "QDC_B01E" -> , r105c2s15/in56_63;
 
 t1_8:  "CFD_B01D" -> , r104c4s4/in1_8;
 t9_16: "CFD_B01E" -> , r104c4s5/in1_8;

}

PSD1600(r105c1s5)
{
 in1_8: "B5t" <- , rTWABc51/out1_8;
 in9_16:"B5b" <- , rTWABc52/out1_8;
 
 e1_8:  "QDC_B02D" -> , r105c2s15/in64_71;
 e9_16: "QDC_B02E" -> , r105c2s15/in72_79;
 
 t1_8:  "CFD_B02D" -> , r104c4s6/in1_8;
 t9_16: "CFD_B02E" -> , r104c4s7/in1_8;

}


PSD1600(r105c1s6)
{
 in1_8: "B6t" <- , rTWABc61/out1_8;
 in9_16:"B6b" <- , rTWABc62/out1_8;
 
 e1_8:  "QDC_B03D" -> , r105c2s15/in80_87;
 e9_16: "QDC_B03E" -> , r105c2s15/in88_95;

 t1_8:  "CFD_B03D" -> , r104c4s8/in1_8;
 t9_16: "CFD_B03E" -> , r104c4s9/in1_8;

}

PSD1600(r105c1s7)
{
 in1_8: "B7t" <- , rTWABc71/out1_8;
 in9_16:"B7b" <- , rTWABc72/out1_8;
 
 e1_8:  "QDC_B04D" -> , r105c2s14/in0_7;
 e9_16: "QDC_B04E" -> , r105c2s14/in8_15;
 
 t1_8:  "CFD_B04D" -> , r104c4s10/in1_8;
 t9_16: "CFD_B04E" -> , r104c4s11/in1_8;

}

PSD1600(r105c1s8)
{
 in1_8: "B8t" <- , rTWABc81/out1_8;
 in9_16:"B8b" <- , rTWABc82/out1_8;
 
 e1_8:  "QDC_B05D" -> , r105c2s14/in16_23;
 e9_16: "QDC_B05E" -> , r105c2s14/in24_31;

 t1_8:  "CFD_B05D" -> , r104c4s12/in1_8;
 t9_16: "CFD_B05E" -> , r104c4s13/in1_8;

}

PSD1600(r105c1s9)
{
 in1_8: "B9t" <- , rTWABc91/out1_8;
 in9_16:"B9b" <- , rTWABc92/out1_8;
 
 e1_8:  "QDC_B06D" -> , r105c2s14/in32_39;
 e9_16: "QDC_B06E" -> , r105c2s14/in40_47;
 
 t1_8:  "CFD_B06D" -> , r104c4s14/in1_8;
 t9_16: "CFD_B06E" -> , r104c4s15/in1_8;

}

PSD1600(r105c1s10)
{
 in1_8: "B10t" <- , rTWABc101/out1_8;
 in9_16:"B10b" <- , rTWABc102/out1_8;
 
 e1_8:  "QDC_B07D" -> , r105c2s14/in48_55;
 e9_16: "QDC_B07E" -> , r105c2s14/in56_63;

 t1_8:  "CFD_B07D" -> , r104c4s16/in1_8;
 t9_16: "CFD_B07E" -> , r104c4s17/in1_8;

}

PSD1600(r105c1s11)
{
 in1_8: "B11t" <- , rTWABc111/out1_8;
 in9_16:"B11b" <- , rTWABc112/out1_8;
 
 e1_8:  "QDC_B08D" -> , r105c2s14/in64_71;
 e9_16: "QDC_B08E" -> , r105c2s14/in72_79;
 
 t1_8:  "CFD_B08D" -> , r104c4s18/in1_8;
 t9_16: "CFD_B08E" -> , r104c4s19/in1_8;

}

PSD1600(r105c1s12)
{
 in1_8: "B12t" <- , rTWABc121/out1_8;
 in9_16:"B12b" <- , rTWABc122/out1_8;
 
 e1_8:  "QDC_B09D" -> , r105c2s14/in80_87;
 e9_16: "QDC_B09E" -> , r105c2s14/in88_95;
 
 t1_8:  "CFD_B09D" -> , r104c4s20/in1_8;
 t9_16: "CFD_B09E" -> , r104c4s21/in1_8;

}

/*
 * First VME crate in messcontainer. Messhuette.
 * (actually below Camac Crate 2, so VME crate 3)
 */


VME_CRATE(r1c3)
{
  ;
}

RIO3(r1c3s1)
{
   SETTING("HOSTNAME"=>"r3-15");

   vsb_out: r1c2s24/vsb_in;
}

ENV1(r1c3s4)
{
  //in_ecl1_8:r2c1s17/out_ecl1_8;
 in_ecl9_16:    .s6/io_ecl1_8;


 // out_ecl1_8:.s6/in_ecl1_8;
 out_ecl9_16:.s5/in1_8;

}
TRIVA3(r1c3s5)
{
  
  
 in1_8:  .s4/out_ecl9_16;
 out4: .s11/in_ecl4;
}

//trigger bus  r1c4s3/trigger bus in


VULOM1(r1c3s6)
{
 in_ecl1_8:r2c2s1/out_ecl1_8;
 in_ecl9_16:r2c2s1/out_ecl9_16;
 io_ecl1_8:r1c3s4/in_ecl9_16;
 io_ecl9_16: r1c3s11/out_ecl1_8;
 out_ecl1_8: r1c3s9/in_ecl1_8;
 out_ecl9_16: r1c3s9/in_ecl9_16;
 //in1:r3c7s1u1/out_ttl;
 in1: r1c0s9u1/out;
 in2  : r1c0s9u2/out;
 out1: r2c5s3u4/in1;
}


ENV1(r1c3s9)
{
 in_ecl1_8: r1c3s6/out_ecl1_8;
 in_ecl9_16: r1c3s6/out_ecl9_16;

 nim_out1:r2c4s5u1/in2;
 nim_out2:r2c4s5u1/in3;
 nim_out3:r2c4s5u2/in1;
 nim_out4:r2c4s5u3/in1;
 nim_out15: r1c3s21/in2;
 //nim_out10:r2c4s5u4/in1;
 //nim_out11:r2c4s5u4/in2;
 //nim_out12:r2c4s7u2/in1;
 //nim_out13:r2c4s7u3/in1;
}

ENV1(r1c3s11)
{
 in_ecl4:r1c3s5/out4;
 out_ecl1_8:r1c3s6/io_ecl9_16;
 nim_in1: r2c2s1/out2;
 nim_in2: r2c2s23u1/out2;
 nim_in7: r3c5s5u2/out6;
 //nim_out1:r1c4s6/nim_in2;
 //nim_out4:r2c1s15u2/in1;

}
TRIDI(r1c3s21)
{
 in2: r1c3s9/nim_out15;
 in3: r1c3s21/out2;
 in4: r1c3s21/out8;
 in5: r1c3s21/out7;
 in6: r2c4s5u3/out1;
 in7: r2c4s5u2/out3;
 in8: r2c4s5u1/out1;

 out2: r1c3s21/in3;
 out3: r2c2s23u2/start;
 out7: r1c3s21/in5;
 out8: r1c3s21/in4;

}

MODULE(DETECTOR_ZST)
{
  LABEL("ZST detector");

  //CONNECTOR(hv);
}

UNIT(DETECTOR_ZST_U)
{
  CONNECTOR(signal);
}

MODULE(DETECTOR_SCI)
{
  LABEL("SCIntillator detector");
}

UNIT(DETECTOR_SCI_U)
{
  CONNECTOR(hv);
  CONNECTOR(signal);
}

MODULE(DETECTOR_FINGER2)
{
  LABEL("Finger detector (2nd)");
}

UNIT(DETECTOR_FINGER2_U)
{
  CONNECTOR(hv);
  CONNECTOR(signal);
}

MODULE(DETECTOR_SEETRAM)
{
  LABEL("SEETRAM detector");

  CONNECTOR(signal);
}

/* FRS production */

DETECTOR_SEETRAM(rSEETRAMc1s1)
{
  LABEL("SEETRAM");
  signal: r0c10/in16;
}

/* S2 */

DETECTOR_FINGER2(rS2FNGc1s1)
{
  CONTROL("DAQ_ACTIVE","OFF");
}

DETECTOR_FINGER2_U(rS2FNGc1s1u1)  { LABEL("FGR1_01"); signal: r0c10/in1;  }
DETECTOR_FINGER2_U(rS2FNGc1s1u2)  { LABEL("FGR1_02"); signal: r0c10/in2;  }
DETECTOR_FINGER2_U(rS2FNGc1s1u3)  { LABEL("FGR1_03"); signal: r0c10/in3;  }
DETECTOR_FINGER2_U(rS2FNGc1s1u4)  { LABEL("FGR1_04"); signal: r0c10/in4;  }
DETECTOR_FINGER2_U(rS2FNGc1s1u5)  { LABEL("FGR1_05"); signal: r0c10/in5;  }
DETECTOR_FINGER2_U(rS2FNGc1s1u6)  { LABEL("FGR1_06"); signal: r0c10/in6;  }
DETECTOR_FINGER2_U(rS2FNGc1s1u7)  { LABEL("FGR1_07"); signal: r0c10/in7;  }
DETECTOR_FINGER2_U(rS2FNGc1s1u8)  { LABEL("FGR1_08"); signal: r0c10/in8;  }
DETECTOR_FINGER2_U(rS2FNGc1s1u9)  { LABEL("FGR1_09"); signal: r0c10/in9;  }
DETECTOR_FINGER2_U(rS2FNGc1s1u10) { LABEL("FGR1_10"); signal: r0c10/in10; }
DETECTOR_FINGER2_U(rS2FNGc1s1u11) { LABEL("FGR1_11"); signal: r0c10/in11; }
DETECTOR_FINGER2_U(rS2FNGc1s1u12) { LABEL("FGR1_12"); signal: r0c10/in12; }
DETECTOR_FINGER2_U(rS2FNGc1s1u13) { LABEL("FGR1_13"); signal: r0c10/in13; }
DETECTOR_FINGER2_U(rS2FNGc1s1u14) { LABEL("FGR1_14"); signal: r0c10/in14; }
DETECTOR_FINGER2_U(rS2FNGc1s1u15) { LABEL("FGR1_15"); signal: /*r0c10/in15*/; }
DETECTOR_FINGER2_U(rS2FNGc1s1u16) { LABEL("FGR1_16"); signal: /*r0c10/in16*/; }

DETECTOR_SCI(rS2SCIc1s1) 
{ 
  CONTROL("DAQ_ACTIVE","ON");
  ; 
}

DETECTOR_SCI_U(rS2SCIc1s1u1) { LABEL("SCI01_01"); signal: r0c9/in14; }
DETECTOR_SCI_U(rS2SCIc1s1u2) { LABEL("SCI01_02"); signal: r0c9/in16; }

/* S8 */

DETECTOR_ZST(rS8ZST1c1s1)
{
  CONTROL("DAQ_ACTIVE","OFF");
}

DETECTOR_ZST_U(rS8ZST1c1s1u1) { LABEL("ZST3_1"); signal: r0c9/in1; }
DETECTOR_ZST_U(rS8ZST1c1s1u2) { LABEL("ZST3_2"); signal: r0c9/in2; }
DETECTOR_ZST_U(rS8ZST1c1s1u3) { LABEL("ZST3_3"); signal: r0c9/in3; }
DETECTOR_ZST_U(rS8ZST1c1s1u4) { LABEL("ZST3_4"); signal: r0c9/in4; }
DETECTOR_ZST_U(rS8ZST1c1s1u5) { LABEL("ZST3_5"); signal: r0c9/in5; }

DETECTOR_ZST(rS8ZST2c1s1)
{
  CONTROL("DAQ_ACTIVE","OFF");
}

DETECTOR_ZST_U(rS8ZST2c1s1u1) { LABEL("ZST4_1"); signal: r0c9/in6; }
DETECTOR_ZST_U(rS8ZST2c1s1u2) { LABEL("ZST4_2"); signal: r0c9/in7; }
DETECTOR_ZST_U(rS8ZST2c1s1u3) { LABEL("ZST4_3"); signal: r0c9/in8; }
DETECTOR_ZST_U(rS8ZST2c1s1u4) { LABEL("ZST4_4"); signal: r0c9/in9; }
DETECTOR_ZST_U(rS8ZST2c1s1u5) { LABEL("ZST4_5"); signal: r0c9/in10; }

DETECTOR_SCI(rS8SCIc1s1) 
{ 
  CONTROL("DAQ_ACTIVE","ON");  
  ;
}

// SCI02_02 and SCI02_01 swapped ??
DETECTOR_SCI_U(rS8SCIc1s1u1) { LABEL("SCI02_02"); signal: r0c9/in11; }
DETECTOR_SCI_U(rS8SCIc1s1u2) { LABEL("SCI02_01"); signal: r0c9/in12; }

///////////////////////////////////////////////////////////////////////////////////

PATCH_PANEL_16(r0c8) //left bottom crate in the messhuette, cables coming from Cave C
{;
 in1_16: rSECLINEc1/out1_16; // Needs to be checked inside the cave
}

PATCH_PANEL_16(r0c9) //left bottom crate in the messhuette
{
in1_5:   rS8ZST1c1s1u1_5/signal;
in6_10:  rS8ZST2c1s1u1_5/signal;

out1:  ;// -> "Ste3_r", r2c4s3u4/in;
out2:  ;// -> "Ste3_o", r2c4s3u4/in;
out3:  ;// -> "Ste3_l", r2c4s3u4/in;
out4:  ;// -> "Ste3_u", r2c4s3u4/in;
out5:   -> "Ste3_a", r2c4s1u1/in;
				// TODO: check how ordering of detector was affected
out6:    ;// -> "Ste4_r", r2c4s3u5/in;
out7:    ;// -> "Ste4_o", r2c4s3u8/in;
out8:    ;// -> "Ste4_l", r2c4s3u7/in;
out9:    ;// -> "Ste4_u", r2c4s3u6/in;
				 //out6_9:  r2c4s3u5_8/in;
out10:   -> "Ste4_a", r2c4s1u2/in;

in11:    rS8SCIc1s1u1/signal;
in12:    rS8SCIc1s1u2/signal;
in14:    rS2SCIc1s1u1/signal;
in16:    rS2SCIc1s1u2/signal;

				 // S8
out11:   r2c3s19/in8;
out12:   r2c3s19/in7;

				 // S2
out14:   r2c3s19/in5;
out16:   r2c3s19/in6;
}

PATCH_PANEL_16(r0c10)
{
 in1_14:  rS2FNGc1s1u1_14/signal;
 in16:   rSEETRAMc1s1/signal;
 
 out1_8:  r2c3s15u1_8/in;

 out9_14: r2c3s5u1_6/in;
 //out15: r2c6s5u7/out1;
 out16: r3c7s1u2/in_ttl;
}

// Messhuette Rack 3, NimCrate 7, implented mar07, modified 20070531 (shift A)
// Containing vme and camac scaling logics 
NIM_CRATE(r3c7)
{
	;
}

LA8000(r3c7s1) //LA8010 actually (not defined)
{
  ;
}

LA8000_U(r3c7s1u1)
{
 in_nim:  r3c5s7/out_1Hz;
}

LA8000_U(r3c7s1u2)
{
 in_ttl:  r0c10/out16;
}
LA8000_U(r3c7s1u5)
{
 in_ttl:  r3c5s7/out_ttl_100kHz;
}

VARIABLE_DELAY(r3c7s5)
{
  ;
  //  SETTING("DELAY" => "16ns");
  //
  //	in: 	r2c2s1/out10; //delayed CB Sum for offspill purpose
  //	out:	r2c2s1/in13;
}

VARIABLE_DELAY(r3c7s7)
{
  SETTING("DELAY" => "28ns");

	out: 	r2c11/front48;
	in:	r3c2/out1;
}

/*
GG8000(r3c7s7)
{
	;// for creating a gate for the p-test crystal charge readout.
}
GG8000_U(r3c7s7u6)
{
 in: r3c5s15u1/out9; //master gate
 out2: r3c5s23/in10; //gate for Si4418/Q in r4c3s23
}
*/
/*
DP1600(r3c7s9)
{
SETTING("DELAY" => "340ns"); //officially 300ns, but written label says 340ns

 in16: ;//r2c11/front35;//GFImon2; p-test crystal modified out2;
 out16:;//Si4418/Q r4c3s23/in0. ECL connector inverted, because signal has positive polarity.
}
*/
HEILDELBERGL14(r3c7s9)
{
  ;
}
HEILDELBERGL14_U(r3c7s9u1)
{
  SETTING("A"   => "OFF");
  SETTING("B"   => "OFF");
  SETTING("C"   => "OFF");
  SETTING("D" => "OFF");

  veto:	r2c2s23u1/not_out;
  inA:	r2c2s21u4/out3;
  out1:	r2c2s19u3/in2;
}

LECROY622(r3c7s11)
{
  SERIAL("42457");
}
LECROY622_U(r3c7s11u1)
{
  SETTING("OUT" => "OR");

 out4: r3c7s23/in5;
}
LECROY622_U(r3c7s11u2)
{
  SETTING("OUT" => "OR");
 in1: r3c5s7/out_100kHz;
 out4: r3c7s23/in6;
}
LECROY622_U(r3c7s11u3)
{
  SETTING("OUT" => "OR");

}
LECROY622_U(r3c7s11u4)
{
  SETTING("OUT" => "OR");
}


//copied from pos.hh
LF4000(r3c7s17)
{
  SERIAL("3736");
}

LF4000_4_U(r3c7s17u1)
{
 in1:      r3c5s15u1/out10;
 out1_4:   .s19u1_4/in;
}

LF4000_4_U(r3c7s17u2)
{
 in1:      r4c7s13u5/out1;
 out1_4:   .s21u1_4/inB;
}

LF4000_4_U(r3c7s17u4)
{
 in1:	;

 out1:;
 out3:;
}


GG8000(r3c7s19)
{
 SERIAL("8377");
}

GG8000_U(r3c7s19u1)
{
 in:    .s17u1/out1;
 out2:  .s21u1/inA;
}

GG8000_U(r3c7s19u2)
{
 in:    .s17u1/out2;
 out2:  .s21u2/inA;
}

GG8000_U(r3c7s19u3)
{
 in:    .s17u1/out3;
 out2:  .s21u3/inA;
}
GG8000_U(r3c7s19u4)
{
 in:    .s17u1/out4;
 out2:  .s21u4/inA;
}

GG8000_U(r3c7s19u5)
{
  //delay 3.5 us
  //width 4.5 us
  in: .c6s21u3/out4;
  out2: .c6s17u3/inB;
}

GG8000_U(r3c7s19u8)
{
  //delay 3.5 us
  //width 4.5 us
  in:   r4c8s21u1/out8;
  out2: r4c3s16/stop3;
}

CO4001(r3c7s21)
{
 SERIAL("3389");
}

CO4001_U(r3c7s21u1)
{
 inA:    .s19u1/out2;
 inB:    .s17u2/out1;
 out1:   .s23/in1;
}

CO4001_U(r3c7s21u2)
{
 inA:    .s19u2/out2;
 inB:    .s17u2/out2;
 out1:   .s23/in2;
}

CO4001_U(r3c7s21u3)
{
 inA:    .s19u3/out2;
 inB:    .s17u2/out3;
 out1:   .s23/in3;
}

CO4001_U(r3c7s21u4)
{
 inA:    .s19u4/out2;
 inB:    .s17u2/out4;
 out1:   .s23/in4;
}
//copied from pos.hh

//copied from pos.hh
EC8000(r3c7s23)
{
 out_ecl1_8:  r1c2s12/in16_23;
 in1_4:       .s21u1_4/out1;
 in5: .s11u1/out4;
 in6: .s11u2/out4;
}
//copied from pos.hh

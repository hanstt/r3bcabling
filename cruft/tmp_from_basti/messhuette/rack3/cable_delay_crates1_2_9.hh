// checked 31/05/2007 by shift D

// The two cable delay crates in the top of rack 3, and the one at the bottom.
// contents of delay.hh has to be moved here...
//
CABLE_DELAY(r3c1)
{
	;
}
//copied from delay.hh
CABLE_DELAY_SLOT(r3c1s1)
{
  SETTING("DELAY"=>"20ns");
 in:  r3c2/out16;
 out: r2c2s1/in1;
}

CABLE_DELAY_SLOT(r3c1s2)
{
  SETTING("DELAY"=>"20ns");
 in:  r3c2/out27; // connected in the upper connector
 out: r3c6s15/in1;
}

CABLE_DELAY_SLOT(r3c1s3)
{
  SETTING("DELAY"=>"20ns");
 out:  r3c6s15/in4;
 in:   r3c2/out7;
}

/*CABLE_DELAY_SLOT(r3c1s4)
{
  SETTING("DELAY"=>"50ns");
 in:  r2c2s13u8/out3;
 out: r2c2s1/in2;
 }*/

CABLE_DELAY_SLOT(r3c1s5)
{

  SETTING("DELAY"=>"50ns");
 in:  .s6/out;
 out: .c2/in6;
}

CABLE_DELAY_SLOT(r3c1s6)
{
  SETTING("DELAY"=>"50ns");
 in:  .c5s15u1/out16;
 out: .s5/in;
}

CABLE_DELAY_SLOT(r3c1s7)
{
  SETTING("DELAY"=>"100ns");
// in:  r4c8s13u3/out3;
// out: r2c2s5u5/in;
}

CABLE_DELAY_SLOT(r3c1s8)
{
  SETTING("DELAY"=>"100ns");
 in:  r4c8s19u3/out3;
 out: r2c2s1/in14;
}
//copied from delay.hh


/*
CABLE_DELAY(r3c2)
{
	;
}
*/

//copied from delay.hh
CABLE_DELAY_32(r3c2)
{
  SETTING("DELAY"=>"50ns");

 in1:   .c2/out2;        out1:  .c7s7/in; 
 in2:   .c2/out10;       out2:   .c2/in1;
 in3:   r5c6/out5;       out3:   r4c7s23/in1;
 in4:   r3c5s1/out7;     out4:   r4c3s9/in6;
 in5:   r3c5s1/out8;     out5:   r4c3s9/in7;
 in6:   r3c1s5/out;      out6:   .c2/in31;
 in7:   r3c5s15u1/out9;	 out7:   r3c1s3/in;

 // in8:   r2c2s21u1/overlap;     out8:   r2c2s19u1/in1;
 in9:   r2c3s23u1/out2;  out9:   r2c5s1u1/trig; 
 in10:  r3c2/out12;      out10:  .c2/in2;

 in12:  r3c5s15u1/out1;  out12:  .c2/in10;
 in14:	r4c8s13u3/out3;  out14:	 .c2/in15;
 in15:	.c2/out14;	 out15:	 .c2/in16;
 in16:	.c2/out15;	 out16:	 r3c1s1/in;

 in17:  ;                out17:  ;	// BROCKEN

 in18:  r2c3s21u7/outB1; out18:  r2c4s11/in11;
 in19:  r2c3s21u8/outA;  out19:  r2c4s11/in12;
 in20:  .c2/out22;       out20:  .c2/in21;
 in21:  .c2/out20;       out21:  .c2/in22;
 in22:  .c2/out21;       out22:  .c2/in20;

//  in23:	r2c1s21u4/out2;	 out23:	 .c2/in24;
//  in24:	.c2/out23;	 out24:	 r2c2s21u1/inB;
//  in25:	r2c3s21u2/outA;	 out25:	 r2c2s1/in9;
 in26:  r1c2s4/stop;     out26:  r3c6s17u2/inB;
 in27:  r3c5s15u1/out12; out27:  r3c1s2/in;
 in28:  r3c5s15u1/out6;  out28:  r3c5s23/in6;

 in29:  r3c5s5u2/out7;	 out29:  .c2/in30;
 in30:  .c2/out29;	 out30:  .c5s11u1/common0;

 in31:  .c2/out6;        out31:  r3c5s23/in16; 
}
//copied from delay.hh


// VARIABLE_DELAY(r3c9s1)
// {
//  in:   r2c2s15u4/out2;
//  out:  .s2/in;
// }

// VARIABLE_DELAY(r3c9s2)
// {
//  in:   .s1/out;
//  out:  .s3/in;
// }

// VARIABLE_DELAY(r3c9s3)
// {
//  in:   .s2/out;
//  out:  .s4/in;
// }


// VARIABLE_DELAY(r3c9s4)
// {
//  in:   .s3/out;
//  out:  r4c3s23/in7;
// }
//copied from delay.hh

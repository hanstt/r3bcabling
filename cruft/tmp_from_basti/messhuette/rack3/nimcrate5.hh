
// checked 31/05/2007 by shift D

// Messhuette Rack 3, NimCrate 5, implented mar07
// Containing time calibrator and gates 
NIM_CRATE(r3c5)
{
	;
}

DP1600(r3c5s1)
{
  //  DELAY("340");

 // in1:   r2c2s19u4/out1;
 // out1:  .s1/in2;
 // in2:   .s1/out1;
 // out2:  r2c2s19u1/in2;
 in7:   r2c4s23u2/out_e;
 out7:  r3c2/in4;
 in8:   r2c4s23u1/out_e;
 out8:  r3c2/in5;
 in9:   .s5u2/out1;
 out9:  .s3/in1;
 in10:  .s5u2/out2;
 out10: .s3/in9;
 in11:  r2c2s19u4/out4;
 out11: .s19u8/in;
 in13:  r2c3s21u7/out_e;//r2c4s23u3/out_e;
 out13: .s1/in15;
 in14:  r2c3s21u8/out_e;//r2c4s23u4/out_e;
 out14: .s1/in16;
 in15:  .s1/out13;
 out15: r4c3s9/in4;
 in16:  .s1/out14;
 out16: r4c3s9/in5;
}


//copied from tcal.hh
/*
 * Time calibration distribution.
 */
EC1601(r3c5s3)
{
  SERIAL("7518");

  // Fan out 2 LEMO signals to 16 ECL 

 in1:     .s1/out9;//.s5u2/out1;
 in2_8:   .s3/out1_7;

 out1_7:  .s3/in2_8;

 in9:     .s1/out10;//.s5u2/out2;
 in10_16: .s3/out9_15;

 out9_15: .s3/in10_16;

 out_ecl1_16:  .s9/inA1_16;
}

LF4000(r3c5s5) // green diod between u1 - 2 and u3 - 4. On or two units? 
{
  SERIAL("7555");
}

LF4000_8_U(r3c5s5u1)
{
 ;
  //in1:  .s7/out_1kHz;
  //out8: r2c1s19u2/inA;
}

LF4000_8_U(r3c5s5u2)
{
 in1:   r3c6s17u2/out1; //  "TCal Stop" <- , r1c2s3/stop;
 in2:   r2c5s3u1/out1; //  "Random TCal Stop" <- , r1c2s3/stop;

 out1: .s1/in9; //.s3/in1; 340ns Delay is in between
 out2: .s1/in10; //.s3/in9; same as above
 out3: r2c11/front45; //out to out?
 out4: .c6s21u3/in1;//.s23/in8; // test input for Phillips TDC
 //out5: r1c0s17/in;
 out6: r1c3s11/nim_in7;
 out7: .c2/in29;//DELAYED.s11u1/common0;
 out8: r2c3s1/in5;//r2c4s9/in1; 
}

/*
OSCILLOSCOPE(floor)
{
 ch2:  r3c5s5u2/out8
}
*/
CL1001(r3c5s7)
{
	SERIAL("6565");
	
  SETTING("SOURCE" => "10MHz");
  SETTING("FORM"   => "PULSE");
  SETTING("STATE"  => "ON");

 out_10MHz:  LABEL("CLK10MHz")  ;
 out_1MHz:   LABEL("CLK1MHz")   r2c4s3u3/in1; //r2c2s15u8/in;//r2c3s1/in8;
 out_100kHz: LABEL("CLK100kHz") r3c7s11u2/in1;
 out_10kHz:  LABEL("CLK10kHz")  ;
 out_1kHz:   LABEL("CLK1kHz")   ;
 out_100Hz:  LABEL("CLK100Hz")  ; 
 out_10Hz:   LABEL("CLK10Hz")   ; 
 out_1Hz:    LABEL("CLK1Hz")    r3c7s1u1/in_nim; 
 out_0d1Hz:  LABEL("CLK0d1Hz")  ;

 out_ttl_10MHz:  LABEL("CLK10MHz")  ;
 out_ttl_1MHz:   LABEL("CLK1MHz")   r3c6s7u4/in_ttl;
 out_ttl_100kHz: LABEL("CLK100kHz") r3c7s1u5/in_ttl;
 out_ttl_10kHz:  LABEL("CLK10kHz")  ;
 out_ttl_1kHz:   LABEL("CLK1kHz")   ;
 out_ttl_100Hz:  LABEL("CLK100Hz")  ; 
 out_ttl_10Hz:   LABEL("CLK10Hz")   ; 
 out_ttl_1Hz:    LABEL("CLK1Hz")    ; 
 out_ttl_0d1Hz:  LABEL("CLK0d1Hz")  ;
}

CO1600(r3c5s9)
{
	SETTING("A" => "OR");
	SETTING("B" => "OR");
	SETTING("C" => "OR");
	SETTING("OUT" => "OR");

 inA1_16:   .s3/out_ecl1_16;

 out1a_16a: r4c7s9/inA1_16;
 //outb1_16b: ; 
 //out1c_8c:  r2c4s17/in1_8; 
 //out9c_16c: r2c4s11/in_ecl9_16; 
}
//copied from tcal.hh

//copied from gate.hh

N638(r3c5s11) { ; }

N638_U(r3c5s11u1)
{
  common0:  .c2/out30;//.s5u2/out7;

  out_ecl0: r2c4s11/in_ecl12;
  out_ecl1: r2c4s11/in_ecl11;
  out_ecl2: r2c4s17/in5;
  out_ecl3: r2c4s17/in3;
}

/*
LF4000(r3c5s11)
{
  ;
}

LF4000_8_U(r3c5s11u1)
{
 in1:  r3c2/out22;

 // out1: r3c2/in9;
 // out2: .s23/in11;
}

LF4000_8_U(r3c5s11u2)
{
 in1:  ;

 out1: ;
 out2: ;
 out3: ;
 out4: ;
}

LF4000_8_U(r3c5s11u3)
{
 in1:  .s15u1/out13;

 out1: .s19u7/in;
 out2: ;//r3c6s15/in2;
 out3: ;//.s19u2/in;//r3c6s15/in3;
 out4: .s19u6/in;
}

LF4000_8_U(r3c5s11u4)
{
 in1:  .s19u2/out1;

 out1: ;
 out2: ;
 out3: ;//.c6s15/in2;
 out4: ;//.c6s15/in3;
}
//copied from gate.hh
*/
//copied from gate.hh
LF4000(r3c5s15)
{
  SERIAL("?305");
}

LF4000_16_U(r3c5s15u1)
{
 in1:   r2c5s3u4/out3;

 out1:  .c2/in12;
 out2:  .s19u4/in;
 out3:  r4c3s16/common_start;
 out4:  .s17u1/in;
 out5:  .s17u3/in;
 out6:  .c2/in28;
 out7:  ;
 out8:  ;
 out9:  r3c2/in7;//.c7s7u6/in;
 out10: .c7s17u1/in1;
 out11: .c6s17u1/inA;
 out12: .c2/in27;
 out13: ;
 out14: .s17u6/in;
 out15: .c6s21u1/in2;
 out16: r3c1s6/in;
}

GG8000(r3c5s17)
{
  SERIAL("76??");
}

GG8000_U(r3c5s17u1)
{
 in:   .s15u1/out4;
 out2: r4c3s13/gate;
}

//GG8000_U(r3c5s17u2)
//{
// in:     .s15u1/out2;
// out2:   .s23/in3;
//}


GG8000_U(r3c5s17u3)
{
 in:   .s15u1/out5;
 out2: .s23/in5;
}

GG8000_U(r3c5s17u6)
{
 in:   .s15u1/out14;
 out2: .s17u7/in;
}

GG8000_U(r3c5s17u7)
{
 in:   .s17u6/out2;
 out2: r2c4s9/in5;
}

// GG8000_U(r3c5s17u8)
// {
//  in:   r2c1s21u2/out4;
// }

GG8000(r3c5s19)
{
  SERIAL("????");
}

GG8000_U(r3c5s19u1)
{
 in:   ;
 out2: ;
}

GG8000_U(r3c5s19u2)
{
 in:   ;
 out1: ;
}

GG8000_U(r3c5s19u4)
{
 in:   r3c5s15u1/out2;
 out2: r3c6s21u1/in1;
}

GG8000_U(r3c5s19u6)
{
 in:   ;
 out2: ;
}

GG8000_U(r3c5s19u7)
{
 in:   ;
 out2: ;
}

GG8000_U(r3c5s19u8)
{
 in:   .s1/out11;
 out2: r2c5s3u4/in2;
}



EC1601(r3c5s23)
{
 in1:  r3c6s17u1/out2;
 in3:  r3c6s21u1/out1;
 in4:  r3c6s17u1/out3;
 in5:  r3c5s17u3/out2;
 in6:  r3c2/out28;
 in7:  ;
 //in7:  ;
 in8:  ;//.s5u2/out4;
 in9:  ;
 in10: ;//.c7s7u6/out2;
 in11: ;
 in12: ;
 in13: ;
 in14: ;//.c2/out31;
 in15: ;
 in16: .c2/out31;

 out_ecl1:	-> "green"  , r4c3s23/gate;
 //out_ecl1:  "A" -> "brown"  , 
 out_ecl3:   -> "yellow" , r4c3s9/gate;
 //out_ecl3: ;// "C" -> "orange" , 
 out_ecl4:   -> "red" , r4c3s6/gate;
 //out_ecl4:	r4c3s6/gate;

//gate to PIN ADC taken out for EXL experiment
 //out_ecl5:  "E" -> "green"  , r4c3s3/gate;
 //out_ecl6:  "F" -> "blue"   , r4c3s2/common_start;
 //out_ecl7:  "G" -> "clear"  , r4c3s2/clear;
 //out_ecl8:  "H" -> "gray"   , r4c3s2/test;
 //out_ecl9:  "I" -> "brown"  , r4c3s14/gate;
 //out_ecl10: ;// "J" -> "red"       , /;
 //out_ecl11: "K" -> "orange" , r4c3s1/common_start;
 //out_ecl12: "L" -> "yellow" , r4c3s19/start;
 //out_ecl13: "M" -> "green"  , r4c3s23/gate;
 //out_ecl14: "N" -> "blue"   , r4c3s14/start;
 //out_ecl15: ;//"O" -> "purple" , /;
 //out_ecl16: ;//"P" -> "gray"   , /;

 //out7: r4c3s13/gate;
 //out_nim9:  r4c3s16/common_start;
 //out14: r4c3s17/common_start;
 //out16: r4c3s17/common_start;
}
//copied from gate.hh

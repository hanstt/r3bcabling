//checked 31/05/2007 by shift D

// Messhuette Rack 4, NimCrate 9, implented mar07
// 
NIM_CRATE(r4c9)
{
	;
}

//copied from other.hh
EC1601(r4c9s3)
{
 in1:  r4c8s23u5/outA;
 in2:  r4c8s23u6/outA;
 in3:  r4c8s23u7/outA;
 in4:  r4c8s23u8/outA;
 in5:  r4c8s9u1/outB2;//r4c8s13u1/out3;
 in6:  r4c8s13u3/out2;
 //in7:  r4c9s15u1/out3;
 in8:  r4c8s19u3/out2;
 //in9:  r4c9s23u5/outA;
 //in10: r4c9s23u6/outA;
 //in11: r4c9s23u7/outA;
 //in12: r4c9s23u8/outA;
 //in13: r4c9s15u2/out3;
 in14: r4c8s23u1/outB1;
 in15: r4c8s23u2/outB1;
 in16: r4c8s23u3/outB1;

 //out5: ;//r3c6s7u6/in_nim; // goes to spill speaker

 out_ecl1_16:  r1c2s14/in1_16;
}
//copied from other.hh

//copied from trigger.hh
/*
TC590(r4c9s7)
{
	in:		;//r3c6s7u6/out_ttl;
}
*/
//copied from trigger.hh

//copied from trigger.hh
/*
CO4000(r4c9s15)
{
 SERIAL("7636");
}

CO4000_U(r4c9s15u1)
{
 LABEL("SCI01");
 SETTING( "A" => "OR" );
 SETTING( "B" => "OR" );
 SETTING( "C" => "OR" );
 SETTING( "OUT" => "AND" );

 //inA: .s21u3/outB2;
 //inB: .s21u2/outB2;
 //out1: .s15u4/inA;
 //out2: .s15u3/inB;
 //out3: .s3/in7;
}

//CO4000_U(r4c9s15u2)
//{
// LABEL("SCI02");
// SETTING( "A" => "OR" );
// SETTING( "B" => "OR" );
// SETTING( "C" => "OR" );
// SETTING( "OUT" => "AND" );

 //inA: .s21u5/outB2;
 //inB: .s21u4/outB2;
 //out1: .s15u3/inA;
 //out2: .s15u4/inB;
 //out3: .s3/in13;
//}

//CO4000_U(r4c9s15u3)
//{
// SETTING( "A" => "OR" );
// SETTING( "B" => "OR" );
// SETTING( "C" => "OR" );
// SETTING( "OUT" => "AND" );

 //inA: .s15u2/out1;
 //inB: .s15u1/out2;
 //out2: r2c2s1/in12;
//}

//CO4000_U(r4c9s15u4)
//{
// SETTING( "A" => "OR" );
// SETTING( "B" => "OR" );
// SETTING( "C" => "OR" );
// SETTING( "OUT" => "AND" );

 //inA: .s15u1/out1;
 //inB: .s15u2/out2;
 //out2: r2c2s1/in13;
//}

//copied from trigger.hh

//copied from pos.hh
/*
EC1601(r4c9s17)
{
 in_ecl1_16:  r4c7s9/out1c_16c;

// out_ecl1_8:  r4c3s9/stop0_7;

 //in1_4:       .c10s17/out1_4;
}
*/

//copied from pos.hh

//copied from other.hh
/* pulled out
CF8000(r4c9s19)
{
  LABEL("7419");
}

CF8000_U(r4c9s19u2)
{
 in:     r5c2/t2;
 outB1:  .s15u1/inA;
 outA:   r4c9s17/in2;
  ;
}
CF8000_U(r4c9s19u3)
{
 in:     r5c2/t3;
 outB1:  .s15u2/inB;
 outA:   r4c9s17/in3;
  ;
}
CF8000_U(r4c9s19u4)
{
 in:     r5c2/t4;
 outB1:  .s15u2/inA;
 outA:   r4c9s17/in4;
  ;
}
*/
/*
CF8000(r4c9s21)
{
  LABEL("2515");
}

CF8000_U(r4c9s21u1)
{
	;// BROKEN
}

CF8000_U(r4c9s21u2)
{
 //in:     r5c2/t1;
 //outB2:  .s15u1/inB;
 //outA:   .c10s17/in1;
;}

CF8000_U(r4c9s21u3)
{
 //in:     r5c2/t2;
 //outB2:  .s15u1/inA;
 //outA:   .c10s17/in2;
;}

CF8000_U(r4c9s21u4)
{
 //in:     r5c2/t3;
 //outB1:	r4c8s19u4/inA;
 //outB2:	.s15u2/inB;
 //outA:   .c10s17/in3;
;}

CF8000_U(r4c9s21u5)
{
 //in:     r5c2/t4;
 //outB1:	r4c8s19u4/inB;
 //outB2:	.s15u2/inA;
 //outA:   .c10s17/in4;
;}

CF8000_U(r4c9s21u6)
{
	;
}

CF8000_U(r4c9s21u7)
{
	;
}

CF8000_U(r4c9s21u8)
{
	;
}

CF8000(r4c9s23)
{
	;
}

CF8000_U(r4c9s23u5)
{
 //in:     r5c2s2/out5;
 //outA:   r4c9s3/in9;
 //outB2:  r4c8s13u4/inA;
;}

CF8000_U(r4c9s23u6)
{
 //in:     r5c2s2/out6;
 //outA:   r4c9s3/in10;
 //outB2:  r4c8s13u4/inB;
;}

CF8000_U(r4c9s23u7)
{
 //in:     r5c2s2/out7;
 //outA:   r4c9s3/in11;
 //outB2:  r4c8s13u4/inC;
;}

CF8000_U(r4c9s23u8)
{
 //in:     r5c2s2/out8;
 //outA:   r4c9s3/in12;
 //outB2:  r4c8s13u4/inD;
;}
//copied from other.hh
*/

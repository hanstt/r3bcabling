// checked 31/05/2007 by shift D


// Messhuette Rack 4, NimCrate 7, implented mar07
// Mainly logic electronics for delays, gates, tcal, and 'other' things 
NIM_CRATE(r4c7)
{
	;
}

//copied from other.hh
/*
LRS222(r4c7s1)
{
  SERIAL("3558");
}

LRS222_U(r4c7s1u1)
{
 start:   r3c5s17u8/out2;
}

LRS222_U(r4c7s1u2)
{
 start:   r4c8s21u1/out1;
 out_neg: r2c11/in9;
 //out_ttl: speaker
} 
*/
//copied from other.hh

//copied from tcal.hh
CO1600(r4c7s9)
{
	SERIAL("7155");

	SETTING( "A"	=>	"OR" );
	SETTING( "B"	=>	"OR" );
	SETTING( "C"	=>	"OR" );
	SETTING( "OUT"	=>	"OR" );
	
 inA1_16:  r3c5s9/out1a_16a;

 out1a_2a:   r4c7s23/in_ecl1_2;
 out1c_16c:   r4c7s21/in_ecl1_16; 
}
//copied from tcal.hh

//copied from other.hh
// VARIABLE_DELAY(r4c7s11)
// {
// 	SERIAL("2701");

//   SETTING("DELAY" => "39.5ns");

//  in:     r5c3s2/out2;
//  out:    r4c8s11u5/in;
// }

GG8000(r4c7s13)
{
  SERIAL("00507");
  //put in at 2006-9-11 instead of a shaper. shaper is used in RPC setup
}

GG8000_U(r4c7s13u1)
{
 in: .s15/out2;
 out1: .s15/in4;
}

GG8000_U(r4c7s13u2)
{
 in: .s15/out5;
 out1: .s15/in7;
}

GG8000_U(r4c7s13u3)
{
 in: .s15/out8;
 out1: .s15/in10;
 // out2: r4c3s16/stop3;// 3300 ns delay in the total chain (between r4c7s15/in1 and this unit)
}

GG8000_U(r4c7s13u4)
{
 in: .s15/out11;
 out1: .s15/in13;
}

GG8000_U(r4c7s13u5)
{
 in: .s15/out14;
 out1:  r3c7s17u2/in1;
 out2:  r4c3s16/stop1;
}

/* For walk tests.
 */
DP1600(r4c7s15)
{
 SERIAL("2887");
 
 in1 :    r4c8s21u1/out5;
 in2:     .s15/out1;
 in4:     .s13u1/out1;
 in5:     .s15/out4;
 in7:     .s13u2/out1;  
 in8:     .s15/out7;
 in10:    .s13u3/out1;
 in11:    .s15/out10;
 in13:    .s13u4/out1;
 in14:    .s15/out13;

 out1:    .s15/in2;
 out2:    .s13u1/in;
 out4:    .s15/in5;
 out5:    .s13u2/in;
 out7:    .s15/in8;
 out8:    .s13u3/in;
 out10:   .s15/in11;
 out11:   .s13u4/in;
 out13:   .s15/in14;
 out14:   .s13u5/in;
}
//copied from other.hh

//copied from delay.hh
DV8000(r4c7s17)
{
  ;
}

DV8000_U(r4c7s17u1)
{
 out1:   .s17u2/in;
 in:     r5c6/out1;
}

DV8000_U(r4c7s17u2)
{
 out1:   r4c3s17/stop1;
 in:     .s17u1/out1;
}

DV8000_U(r4c7s17u3)
{
 out1:   .s17u4/in;
 in:     r5c6/out2;
}

DV8000_U(r4c7s17u4)
{
 out1:   r4c3s17/stop2;
 in:     .s17u3/out1; 
}

DV8000_U(r4c7s17u5)
{
 out1:  .s17u6/in;
 in:    r5c6/out3;
}

DV8000_U(r4c7s17u6)
{
 out1:   .s17u7/in;
 in:     .s17u5/out1;
}

DV8000_U(r4c7s17u7)
{
 in:    .s17u6/out1;
}
//copied from delay.hh

/*
//copied from gate.hh
SH8000(r4c7s19)
{
  SERIAL("3357");
}

SH8000_U(r4c7s19u1)
{
 //in:   "3"  <- "3"  , rRPCRc1s1/t1;
 //out2: .s21/in1;
;}

SH8000_U(r4c7s19u2)
{
 //in:   "5"  <- "5"  , rRPCRc1s1/t3;
 //out2: .s21/in4;
;}

SH8000_U(r4c7s19u3)
{
 //in:   "6"  <- "6"  , rRPCRc1s1/t4;
 //out2: .s21/in5;
;}

SH8000_U(r4c7s19u4)
{
	// BROCKEN
 in:   ;
 out2: ;
}
//copied from gate.hh
*/

//copied from other.hh
EC1601(r4c7s21)
{
	// Time for RPC
 in_ecl1_16: r4c7s9/out1c_16c;
							
 in1:	 r5c5/out1;  //.s19u1/out2;
 in2:	 r5c5/out2;  //  "4"  <- "4"  , rRPCRc1s1/t2;
 in3:	 r5c5/out3;
 in4:	 r5c5/out4;  //.s19u2/out2;
  //in5:   .s19u3/out2;
  //in6:   .s19u5/out2;
  //in7:   .s19u6/out2;
  //in8:   .s19u7/out2;
  //in9:   .s19u8/out2;
  //in10:	 ;
  //in11:	 ;
  //in12:	 ;
  //in13:   rDMY1c1s1u1/
  //in14:	 ;
  //in15:	 ;
  //in16:	 ;

  out_ecl1_8: 	r4c3s19/stop0_7;
//  out_ecl9_16:	r4c3s22/in9_16;
}
//copied from other.hh

//copied from pos.hh
EC1610(r4c7s23)
{
 in_ecl1_2: r4c7s9/out1a_2a;

 in_ecl5: r2c4s11/out_ecl3;
 in_ecl6: r2c4s11/out_ecl5;
 in_ecl8: r2c4s19/outa4;
 in_ecl7: r2c4s19/outa3;

 in1: r3c2/out3;
 in2: r5c6/out6;

 out5_8: r4c3s17/stop3_6;
 out9: r4c3s16/common_stop;
 out10: r4c3s17/common_stop;
 out14: r5c5/out5;
 out15: r5c5/out7;
 out16: r5c5/out6;
}
//copied from pos.hh

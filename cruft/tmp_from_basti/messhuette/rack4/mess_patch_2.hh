//outputs checked 31/05/2007 by shift D

//2nd Patch_Panel_48 in the Messhuette; used for "Stelzer1", "ROLU" and "PSP1_3".
//Label "TERMINAL10"

//copied from psp.hh
PATCH_PANEL_48(r4c11)
{
 in9_15:  "KABEL7 (49-55)" <- , rBEAMR1c1/out1_7;
 //        KABEL7 (56) goes to r5c3(TERMINAL)/in1
 in25_31: "KABEL9 (1-7)"   <- , rBEAMR1c1/out9_15;
 //        KABEL9 (8)  goes to r5c3(TERMINAL)/in2
 in41_45: "KABEL5 (33-37)" <- , rBEAMR1c1/out17_21;
 in46:    "KABEL5 (38)"    <- , rBEAMR1c1/out23;
 in47: ; //KABEL5 (39) connected, broken at other end
 //        KABEL5 (40) goes to r5c3(TERMINAL)/in3

 out9:   r4c10s7u1/in; 
 out10:  r4c10s7u2/in;
 out11:  r4c10s7u3/in;
 out12:  r4c10s7u4/in;
 out13:  r4c10s5u1/in;

 // out25:  r4c3s13/in5;
 //out26:  r4c3s13/in6;
 //out27:  r4c3s13/in7;
 //out28:  r4c3s13/in8;
 out29:  r4c10s5u3/in;

 //out41:  r4c10s7u1/in;
 //out42:  r4c10s7u2/in;
 //out43:  r4c10s7u3/in;
 //out44:  r4c10s7u4/in;
 out45:  r4c10s15u3/in;
}
//copied from psp.hh

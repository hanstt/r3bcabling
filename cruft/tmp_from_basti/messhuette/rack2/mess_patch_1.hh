/* Patch panel to talk to cave electronics.
 */

/* This file refers to the panel in the Messhuette. The file to the complementary panel inside Cave C is /cave/rack13/cave_patch.hh
*/

PATCH_PANEL_48_new(r2c11)
{
	// front
 front1: r3c6s7u8/out_nim;
 front2: r4c8s11u5/outB1;
 front3: r1c0s5u1/in;
 front4: r1c0s5u2/in;

 front8: r2c2s23u2/out1;
 front9: r2c11/front38;
 //front10: r2c7/front39;
 //front11: r2c7/front40;
 
	// back
 back25_36:   r13c11/back1_12;
				
	// front
 front10: r2c11/front39;
 front11: r2c11/front40;

 front25: r2c2s15u4/in;

 front27: r2c3s19/in1;
 front28: r2c3s19/in4;

 front38: r2c11/front9;
 front39: r2c11/front10;
 front40: r2c11/front11;
 front41: r2c2s1/in15;
 front42: ;

 front45: r3c5s5u2/out3;
 front46: r3c6s5u1/out1; // cable completely broken!!
 front47: r3c6s1u1/out3;
 front48: r3c7s7/out;

	// back
 back37_48:   r13c11/back13_24;

}


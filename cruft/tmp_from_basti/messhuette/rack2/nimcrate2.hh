// Messhuette Rack 2, NimCrate 2, implented mar07, modified 20070530 (DR)
// Modified 20100830 
// 
NIM_CRATE(r2c2)
{
	;//mainly trigger electronics taken from trigger.hh
}

//copied from trigger.hh
EC1601(r2c2s1)
{
 in1:   r3c1s1/out;
 in2:	r4c8s21u1/out2; //NTF multiplicity trigger
 in3:   r2c2s13u1/out1;
 in4:   r2c2s13u4/out1;
 in5:   r2c2s13u5/out3;
 in6:   .s13u5/out1;
 in7:   .s13u7/out3;
 in8:   .s13u7/out1;
  
 // in9:   COMING FROM CAVE C;
 in10:  r2c2s1/out9;
 // in11:  Cable goes out
 in12:  r2c2s1/out11;
 in13:  r2c3s23u4/out3;
 in14:  r3c1s8/out; 
 in15:  r2c11/front41; 
 //in16: Cable goes out
 
 // out1:  r2c2s23u2/start; 
 out2:  r1c3s11/nim_in1;
 // out3:  r2c2s1/in4;
 out9:  r2c2s1/in10;
 out11: r2c2s1/in12;
 //out7:	r3c7s7/in;
 //out9:  r2c1s1/in1;
 //out10: r3c7s5/in;
 /*out11: r2c1s1/in3;
 out12: r2c1s1/in4;*/
 
 out_ecl1_16:  r1c3s6/in_ecl1_16;
 // out_ecl1_16:  r1c2s22/in0_15;
 //out_ecl9_16: matrices
 }



/* Delay of raw trigger signals.
 */

DV8000(r2c2s5)
{
  SERIAL("2651");
}
DV8000_U(r2c2s5u4)
{
 in:  r4c8s19u2/out2;
}
  

/* Begin/End of spill.  Make sure it does not get lost.
 *
 * Each signal start a clock, which gives a signal out (BoS/EoS
 * pending).  This signal goes to a coincidence unit.  It is anded with
 * not deadtime.  Meaning that output of the coincidence unit will pulse
 * each time either BoS/EoS raw comes, or whenever deadtime is released.
 * This output is used to trigger a BoS/EoS event.
 *
 * The anding with not deadtime make sure the event does not get lost if
 * it happened while a previous event was being read out.
 */


/* CO4001_U(r2c2s9u2)
{
  SETTING("A"   => "AND");
  SETTING("B"   => "OR");
  SETTING("C"   => "OR");
  SETTING("OUT" => "AND");

 inA:    .s11u2/out2;
 inB:    .c1s15u3/out4;

 out3:   .c1s7/in8;
}

CAEN2255B(r2c2s11)
{
  SERIAL("680r2");
}

CAEN2255B_U(r2c2s11u1)
{
   LABEL("BoS pending");
  
 start: r3c6s5u1/out2;
 reset: .c4s7u2/out2;

 out2:  .s9u1/inA;
}
*/

/* Threshold for multiplicity triggers.
 */

PS710(r2c2s13)
{
  ;
}

PS710_U(r2c2s13u1)
{
  LABEL("LAND");
 in:      r2c3s19/out2;
 out1:    r2c2s1/in3;
  // out2:    .s1/in3;
  // out3:    r2c3s17/in9;
}

/*
PS710_U(r2c2s13u2)
{
  LABEL("LANDcosm");
 in:    .s15u1/out2;
 out2:  r2c2s1/in4;
 
}
*/
PS710_U(r2c2s13u3)
{
  LABEL("VETO");
 in:    .s15u3/out1;
  // out2:  .s1/in5;
}

PS710_U(r2c2s13u4)
{
  LABEL("TFW");
 in:    .s15u4/out1;
 out1:  r2c2s1/in4;
}

PS710_U(r2c2s13u5)
{
 in:    .s15u7/out1; //.c3s19/out2;
 out1:  r2c2s1/in6;
 out3: r2c2s1/in5;
}

PS710_U(r2c2s13u7)
{
 in:    .s15u6/out1;//.s15u4/out1;
 out1:  .s1/in8;
 out3:  .s1/in7;//r2c2s1/in10;
}

/*
PS710_U(r2c2s13u8)
{
 in:    .c3s19/out4;
 out3:  r3c1s4/in;
}
*/


OCTAL_SPLIT(r2c2s15)
{
  ;
}

/*
OCTAL_SPLIT_U(r2c2s15u1)
{
  LABEL("LAND mult");
  // in:   Cable goes out     
  // in:     rm_LAND;  it is actually there
 out1:   .s13u1/in;
  // out2:   .s13u2/in;
}
OCTAL_SPLIT_U(r2c2s15u2)
{
  LABEL("VETO mult");
 in:     r2c11/front26;
 out1:   .s13u3/in;
 out2:   ;
}
*/

OCTAL_SPLIT_U(r2c2s15u3)
{
  LABEL("NTF mult");
 in:     .c3s19/out4; // r2c3s17/analog_out; 
 out1:   .s13u3/in; //c3s19/in4;
  // out2:   .c3s19/in3;
}

OCTAL_SPLIT_U(r2c2s15u4)
{
  LABEL("XB163_1"); //XB163_1/XBSUM_1
  CONTROL("DAQ_ACTIVE","ON");
 in:     r2c11/front25; //.c3s19/out1; //r2c11/front29; //STDET
 out1:   .s13u4/in;//.s13u7/in;
  // out2:   r3c9s1/in;
}


OCTAL_SPLIT_U(r2c2s15u6)
{
  LABEL("CB mult");
 in:    r2c3s19/out3; 
 out1:  .s13u7/in;
}

OCTAL_SPLIT_U(r2c2s15u7)
{
  LABEL("TFW mult");
 in:     r2c3s19/out1;
 out1:   .s13u5/in;
}

/*
OCTAL_SPLIT_U(r2c2s15u8)
{
  //LABEL("1MHz");
  //CONTROL("DAQ_ACTIVE","OFF");
 in:     ; //r3c5s7/out_1MHz; //1MHz
 out1:   ;//r2c3s1/in8;
 out2:   ;//r2c5s1u1/reset;
}
*/


/*
CO4000(r2c2s19)
{
  ;
}
CO4000_U(r2c2s19u1)
{
  SETTING("A"   => "AND");
  SETTING("B"   => "AND");
  SETTING("C"   => "OR");
  SETTING("OUT" => "AND");

 inA:  ; // r3c5s7/out_10MHz;
 inB:  r3c6s3u1/out1;

 out2: r2c1s1/in8;
}

CO4000_U(r2c2s19u4)
{
  SETTING("A"   => "AND");
  SETTING("B"   => "AND");
  SETTING("C"   => "OR");
  SETTING("OUT" => "AND");

 inA:  r3c5s7/out_100Hz;
 inB:  r3c6s3u2/out1;

 not_out: r2c1s1/in7;
}
*/

LECROY429A(r2c2s19)
{;
}
/*
LECROY429A_4_U(r2c2s19u1)
{
 in1:     r3c2/out8;
 in2:     r3c5s1/out2;
 in3:     r2c4s5u4/out3;
 out3:    r1c4s10/com_in;
}
*/

LECROY429A_4_U(r2c2s19u3)
{
 in1:     .c1s23u7/out2; // r2c1s23u7/out2;
 in2:     r3c7s9u1/out1;
 out4:    .s23u1/start;
}

LECROY429A_4_U(r2c2s19u4) // Checked 7 Sep!
{
 in1:     r1c2s4/start;
 in3:     .c4s3u2/out2;
 out3:    .c1s23u5/in;
 out4:    r3c5s1/in11; 
}

CO4000(r2c2s21)
{
 ;
}
/*
CO4000_U(r2c2s21u1)
{
  SETTING("A"   => "AND");
  SETTING("B"   => "AND");
  SETTING("C"   => "OR");
  SETTING("OUT" => "AND");
	
 inA:  r2c4s7u4/out1;
 inB:  r3c2/out24;
 overlap:  r3c2/in8;
}

*/

/*
CO4000_U(r2c2s21u3)
{
  SETTING("A"   => "OR");
  SETTING("B"   => "OR");
  SETTING("C"   => "OR");
  SETTING("OUT" => "AND");
	
 inA: ;//r2c11/out30;
 inB: ;//r2c11/out31;
 out1: ;//.s1/in7;
}
*/

CO4000_U(r2c2s21u4)
{ 
  SETTING("A"   => "AND");
  SETTING("B"   => "OR");
  SETTING("C"   => "OR");
  SETTING("OUT" => "AND");

 inA:   r4c8s21u1/out3;
 out1:  .c1s23u7/in;
 out2:  .c4s9/in7; // NIM input
 out3:	r3c7s9u1/inA;
}


CAENN93B(r2c2s23)
{
  ;
}

CAENN93B_U(r2c2s23u1)
{
	LABEL("Pile-up rejection");
 start: .s19u3/out4; //r4c8s21u1/not_out1;//Yellow sabotage cable 
 out2:  r1c3s11/nim_in2; //.s1/in15;
 not_out: r3c7s9u1/veto;
}

CAENN93B_U(r2c2s23u2)
{
	LABEL("Pile-up rejection");
 start: r1c3s21/out3; //r2c1s1/out1a;
 out1:  r2c11/front8;
 out2:  r3c6s7u6/in_nim; // in
}


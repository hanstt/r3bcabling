// Messhuette Rack 2, NimCrate 5, implented mar07
// Containing Bias supply of the psp detectors 
NIM_CRATE(r2c5)
{
  ;
}
PHI794(r2c5s1)
{
 ;
}
PHI794_U(r2c5s1u1)
{
  // In reality trig is from cable delay, but I (HTJ) cannot fix that
  // from here, as that cable delay in/out is marked as used.
  // trig: r3c2/out9;
  // instead documenting from source before cable delay
 trig: r3c2/out9;//r2c4s3u2/out1;
 reset: r2c5s3u2/out1;//r2c4s3u3/out2;//r2c2s15u8/out2;
 
 delay: r2c5s3u1/in1;
}


PHI794_U(r2c5s1u3)
{

 trig: r2c5s1u4/delay;//r2c4s3u2/out1;
 // reset: r2c5s3u2/out;//r2c4s3u3/out2;//r2c2s15u8/out2;
 
 delay: r2c5s1u4/trig;
}


PHI794_U(r2c5s1u4)
{

 trig: r2c5s1u3/delay;//r2c4s3u2/out1;
 // reset: r2c5s3u2/out;//r2c4s3u3/out2;//r2c2s15u8/out2;
 
 delay: r2c5s1u3/trig;
}

LF4000(r2c5s3)
{
SERIAL("7307");;
}
LF4000_4_U(r2c5s3u1)
{
 in1: r2c5s1u1/delay;

 out1: r3c5s5u2/in2;
}
LF4000_4_U(r2c5s3u2)
{
 in1: r2c3s23u2/overlap;

 out1: r2c5s1u1/reset;
}

LF4000_4_U(r2c5s3u4)
{
 in1: r1c3s6/out1;
 in2: r3c5s19u8/out2;

 out1: ;// gate to cave..
 out3: r3c5s15u1/in1;
 out4:; // to cave, not labeled
}

CABLE_DELAY_SLOT(r2c5s21)
{
 in: r2c4s23u1/outA;

 out: r2c4s11/in3;
}

CABLE_DELAY_SLOT(r2c5s23)
{
 in: r2c4s23u2/outA;

 out: r2c4s11/in5;
}

// Messhuette Rack 2, NimCrate 6, implented mar07, modified 20070531 (DR)
// Empty nim crate. 
NIM_CRATE(r2c6)
{
	;
}

/* Removed 26 Jul 2007 to be replaced by a standart NIM-ECL converter
N638(r2c6s3){
  ;
}

N638_U(r2c6s3u1)
{
 in_ecl0:  r1c4s17/control_io8;
 outl0: .c6s5u8/in;
}

N638_U(r2c6s3u2)
{
 in_ecl0:  r1c4s10/drdy;
 //outr0: ;
}
*/

/*EC1601(r2c6s3)
{
  in_ecl4:  r1c4s17/control_io8;
  //  in4:	    r1c2s4/gate;

  out4:	    .c6s5u8/in;
}

GG8000(r2c6s5)
{
  ;
}

GG8000_U(r2c6s5u7)
{
 in :r4c8s21u4/out3;

 // out1: r0c10/out15;
}

GG8000_U(r2c6s5u8)
{
 in: r2c6s3/out4;

 out2: r2c4s7u1/in4;
}
*/

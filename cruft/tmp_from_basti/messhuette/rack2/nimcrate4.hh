// Messhuette Rack 2, NimCrate 4, implented mar07, modified 20070530 (DR)
// 
NIM_CRATE(r2c4)
{
	;
}
//copied from frs.hh
CF8000(r2c4s1)
{
  SERIAL("2646");

  //out_ecl1_8: ;
}

CF8000_U(r2c4s1u1) { in: r0c9/out5;  outA: .s11/in9;  }
CF8000_U(r2c4s1u2) { in: r0c9/out10; outA: .s11/in10; }

// Used in r4c9s23
// CF8000(r2c4s3)
// {
//   ;
// }

// CF8000_U(r2c4s3u1) { in: r0c9/out1; out_ecl: r2c4s15/in1; }
// CF8000_U(r2c4s3u2) { in: r0c9/out2; out_ecl: r2c4s15/in2; }
// CF8000_U(r2c4s3u3) { in: r0c9/out3; out_ecl: r2c4s15/in3; }
// CF8000_U(r2c4s3u4) { in: r0c9/out4; out_ecl: r2c4s15/in4; }
// CF8000_U(r2c4s3u5) { in: r0c9/out6; out_ecl: r2c4s15/in5; }
// CF8000_U(r2c4s3u6) { in: r0c9/out9; out_ecl: r2c4s15/in6; }
// CF8000_U(r2c4s3u7) { in: r0c9/out8; out_ecl: r2c4s15/in7; }
// CF8000_U(r2c4s3u8) { in: r0c9/out7; out_ecl: r2c4s15/in8; }


//TODO: u1 needs to be documented...
LF4000_4_U(r2c4s3u2)
{
 in1: r1c2s2/p2;

 out1:  r2c3s23u1/inA;  // r2c5s1u1/trig;
 out2:  r2c2s19u4/in3;
}

LF4000_4_U(r2c4s3u3)
{
 in1:  r3c5s7/out_1MHz;
 in2:  ;

 out1:  r2c3s1/in8;
 // out2:  r2c5s1u1/reset;
 out3:  r2c3s23u2/inB;
 out4:  r3c6s17u3/inA;
}

LF4000_4_U(r2c4s5u1)
{
 in2:  r1c3s9/nim_out1;
 in3:  r1c3s9/nim_out2;

 out1:  r1c3s21/in8;
 out2:  .s7u1/in1;
 //out4:  .s7u4/in1;
}

LF4000_4_U(r2c4s5u2)
{
 in1:  r1c3s9/nim_out3;

 out1: r2c4s7u1/in2;
 out3: r1c3s21/in7;
 //out3:  .c11/front43;
 //out4:  .s7u4/in2;
}

LF4000_4_U(r2c4s5u3)
{
 in1:  r1c3s9/nim_out4;

 out1:  r1c3s21/in6;
 //out2:  .s7u1/in2;
 //out3:  .c11/front44;
 //out4:  .s7u4/in3;
}

// LF4000_4_U(r2c4s5u4)
// {
//  in1:  r1c3s9/nim_out10;
//  in2:  r1c3s9/nim_out11;
//  in3:  .s7u2/out4;
//  in4:  .s7u3/out4;

//  out1:  .s7u1/in3;
//  out3:  r2c2s19u1/in3;
//  out4:  .s7u4/in4;
// }

LF4000(r2c4s7)
{
  ;
}

LF4000_4_U(r2c4s7u1)
{
 in1:  .s5u1/out2;
 in2:  .s5u2/out1;
 //in3:  .s5u4/out1;
 //in4:  .c6s5u8/out2;

 out1: ;  // gate to XB via "fast" cable
 //out4:  r1c4s13/gate_in;
}

// LF4000_4_U(r2c4s7u2)
// {
//  in1:  r1c3s9/nim_out12;

//  out2:  .c2s11u1/reset;
//  out4:  .s5u4/in3;
// }

// LF4000_4_U(r2c4s7u3)
// {
//  in1:  r1c3s9/nim_out13;

//  out2:  .c2s11u2/reset;
//  out4:  .s5u4/in4;
// }

// LF4000_4_U(r2c4s7u4)
// {
//  in1:  .s5u1/out4;
//  in2:  .s5u2/out4;
//  in3:  .s5u3/out4;
//  in4:  .s5u4/out4;

//  out1:  .c2s21u1/inA;
//  out4:  r1c4s15/trig_in;
// }

EC1610(r2c4s9)
{
  SERIAL("8654");

 // out_ecl1:   r1c2s9/gate;
 out_ecl1:   r1c2s6/in1;
 out_ecl5:   r1c2s6/common;
 out_ecl7:   r1c2s6/in0;

 // in1:        r3c5s19u7/out2;
 in1:        r3c6s21u4/out4;//r3c5s5u2/out8;
 in5:        r3c5s17u7/out2;
 in7:        r2c2s21u4/out2;

 out1:       ;//.c3s1/in5;
}

EC1610(r2c4s11)
{
  SERIAL("8653");

 in3: r2c5s21/out;
 in5: r2c5s23/out;
 in9:  r2c4s1u1/outA;
 in10: r2c4s1u2/outA;
 in11: r3c2/out18;//r2c4s23u3/outA;
 in12: r3c2/out19;//r2c4s23u4/outA;

 out1: r2c3s1/in1;
 out2: r2c3s1/in2;
 out3: r2c3s1/in11;
 out5: r2c3s1/in12;
 out9: r2c3s1/in3;
 out10:r2c3s1/in4;
 out11:r2c3s1/in13; 
 out12:r2c3s1/in14;


 out_ecl3: r4c7s23/in_ecl5;
 out_ecl5: r4c7s23/in_ecl6;
 //out_ecl1_8:  .s17/in1_8;
 out_ecl9_16: .s19/in1_8;

 in_ecl1_8: .s17/outb1_8;
 //in_ecl9_16: r3c5s9/out9c_16c;
 in_ecl11:  r3c5s11u1/out_ecl1;
 in_ecl12:  r3c5s11u1/out_ecl0;
							
 in16: BROKEN;  
 out16: BROKEN;
}


DL8010(r2c4s15)
{
  SERIAL("7144");

  SETTING("DELAY" => "320ns");

 //in1_8:   .s3u1_8/out_ecl;
 //outa1_8: r4c3s2/stop1_8;
 outb1_8: r1c2s12/in24_31;
}

DL8010(r2c4s17)
{
  SETTING("DELAY" => "370ns");

 //in1_8:   r3c5s9/out1c_8c;//.s11/out_ecl9_16;
 in3:	  r3c5s11u1/out_ecl3;
 in5:	  r3c5s11u1/out_ecl2;
 outa3_4: ;//r4c3s11/stop4_5;
 outb1_8: .s11/in_ecl1_8;//r1c2s12/in32_39;
}

DL8010(r2c4s19)
{
  SERIAL("7353");

  SETTING("DELAY" => "370ns");

 in1_8:   .s11/out_ecl9_16;
 outa3: r4c7s23/in_ecl7;
 outa4: r4c7s23/in_ecl8;
  //outa3_4: r4c3s11/stop6_7;
  outb1_8: r1c2s12/in40_47;
}
 
OCTAL_SPLIT(r2c4s21)
{
  ;
}

OCTAL_SPLIT_U(r2c4s21u1)
{
;
}

OCTAL_SPLIT_U(r2c4s21u2)
{
;
}

OCTAL_SPLIT_U(r2c4s21u3)
{
;
}

OCTAL_SPLIT_U(r2c4s21u4)
{
;
}


CF8000(r2c4s23)
{
  SERIAL("2648");
}

CF8000_U(r2c4s23u1)
{
 in:    .c3s19/out5;

 outB1: r2c3s23u3/inB;
 outA:  r2c5s21/in;
 //out_ecl: r3c5s1/in8; //There are two lemo outputs in the back part of the CF8000
 out_e: r3c5s1/in8;
}

CF8000_U(r2c4s23u2)
{
 in:    .c3s19/out6;

 outB1: r2c3s23u3/inA;
 outA:  r2c5s23/in;
 out_e: r3c5s1/in7;
}

//CF8000_U(r2c4s23u3)
//{
// in:    .c3s19/out7;
//
// outB1:  r2c3s23u4/inA;
// outA:   .s11/in11;
// out_e:  r3c5s1/in13;
//}

//CF8000_U(r2c4s23u4)
//{
// in:    .c3s19/out8;
//
// outB1:  r2c3s23u4/inB;
// outA:  .s11/in12;
// out_e:  r3c5s1/in14;
//}
//copied from frs.hh

#include "gfi.hh"

/* Naming scheme for GFI 1 detector:
 *
 * DETECTOR_GFI ('the detector', r50c1), XXX is the crystal number.
 * DETECTOR_GFI_PM ('the timing PM', r50c1s2)
 * DETECTOR_GFI_PSPM ('the position sensitive PM',r50c1s1u1)
 * DETECTOR_GFI_PSPM_U ('one channel from PSPM',r50c1s1u1)
 *
 * Each cable that goes to fastbus has on GFI end a shorter cable
 * that goes from lemo to 8-fold connector.  This connection is a joiner 8.
 * At the other end is another joiner 8 before it goes to paddle card into QDC.
 *
 * rGFI1c1 is the detector.
 * rGFI1c2 is the vacuum-flange.
 *
 * r51c1  is the crate (below the flange)
 * r51c1x are the cable joiners close to the rack
 * r51c2x are the cable joiners close to the fastbus
 */

// GFI 1 /******************************************************/

DETECTOR_GFI(rGFI1c1)
{
  ;
}

DETECTOR_GFI_PSPM(rGFI1c1s1)
{
  ;
}

DETECTOR_GFI_PSPM_U(rGFI1c1s1u1)  { LABEL("GFI01_01"); signal: rGFI1c2/x_in1; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u2)  { LABEL("GFI01_02"); signal: rGFI1c2/x_in2; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u3)  { LABEL("GFI01_03"); signal: rGFI1c2/x_in3; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u4)  { LABEL("GFI01_04"); signal: rGFI1c2/x_in4; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u5)  { LABEL("GFI01_05"); signal: rGFI1c2/x_in5; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u6)  { LABEL("GFI01_06"); signal: rGFI1c2/x_in6; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u7)  { LABEL("GFI01_07"); signal: rGFI1c2/x_in7; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u8)  { LABEL("GFI01_08"); signal: rGFI1c2/x_in8; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u9)  { LABEL("GFI01_09"); signal: rGFI1c2/x_in9; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u10) { LABEL("GFI01_10"); signal: rGFI1c2/x_in10; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u11) { LABEL("GFI01_11"); signal: rGFI1c2/x_in11; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u12) { LABEL("GFI01_12"); signal: rGFI1c2/x_in12; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u13) { LABEL("GFI01_13"); signal: rGFI1c2/x_in13; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u14) { LABEL("GFI01_14"); signal: rGFI1c2/x_in14; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u15) { LABEL("GFI01_15"); signal: rGFI1c2/x_in15; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u16) { LABEL("GFI01_16"); signal: rGFI1c2/x_in16; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u17) { LABEL("GFI01_17"); signal: rGFI1c2/x_in17; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u18) { LABEL("GFI01_18"); signal: rGFI1c2/x_in18; }

DETECTOR_GFI_PSPM_U(rGFI1c1s1u19) { LABEL("GFI01_19"); signal: rGFI1c2/y_in1; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u20) { LABEL("GFI01_20"); signal: rGFI1c2/y_in2; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u21) { LABEL("GFI01_21"); signal: rGFI1c2/y_in3; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u22) { LABEL("GFI01_22"); signal: rGFI1c2/y_in4; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u23) { LABEL("GFI01_23"); signal: rGFI1c2/y_in5; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u24) { LABEL("GFI01_24"); signal: rGFI1c2/y_in6; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u25) { LABEL("GFI01_25"); signal: rGFI1c2/y_in7; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u26) { LABEL("GFI01_26"); signal: rGFI1c2/y_in8; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u27) { LABEL("GFI01_27"); signal: rGFI1c2/y_in9; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u28) { LABEL("GFI01_28"); signal: rGFI1c2/y_in10; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u29) { LABEL("GFI01_29"); signal: rGFI1c2/y_in11; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u30) { LABEL("GFI01_30"); signal: rGFI1c2/y_in12; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u31) { LABEL("GFI01_31"); signal: rGFI1c2/y_in13; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u32) { LABEL("GFI01_32"); signal: rGFI1c2/y_in14; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u33) { LABEL("GFI01_33"); signal: rGFI1c2/y_in15; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u34) { LABEL("GFI01_34"); signal: rGFI1c2/y_in16; }
// TODO: this is not a PSPM...
DETECTOR_GFI_PSPM_U(rGFI1c1s1u35) { LABEL("GFI01_35"); signal: r13c11/front17; }

VACUUM_FLANGE36(rGFI1c2)
{
 x_in1_18: rGFI1c1s1u1_18/signal;
 y_in1_16: rGFI1c1s1u19_34/signal;

 y_out15:   r51c1s19/in1;  //cables 1<->33 exchanged during s318 beamtime.
 x_out2:   r51c1s19/in3;
 x_out3:   r51c1s19/in5;
 x_out4:   r51c1s19/in7;
 x_out5:   r51c1s19/in9;
 x_out6:   r51c1s19/in11;
 x_out7:   r51c1s19/in13;
 x_out8:   r51c1s19/in15;
 x_out9:   r51c1s19/in17;
 x_out10:  r51c1s19/in19;
 x_out11:  r51c1s19/in21;
 x_out12:  r51c1s19/in23;
 x_out13:  r51c1s19/in25;
 x_out14:  r51c1s19/in27;
 x_out15:  r51c1s19/in29;
 x_out16:  r51c1s19/in31;

 x_out17:  r51c1s19/in2;
 x_out18:  r51c1s19/in4;

 y_out1:   r51c1s19/in6;
 y_out2:   r51c1s19/in8;
 y_out3:   r51c1s19/in10;
 y_out4:   r51c1s19/in12;
 y_out5:   r51c1s19/in14;
 y_out6:   r51c1s19/in16;
 y_out7:   r51c1s19/in18;
 y_out8:   r51c1s19/in20;
 y_out9:   r51c1s19/in22;
 y_out10:  r51c1s19/in24;
 y_out11:  r51c1s19/in26;
 y_out12:  r51c1s19/in28;
 y_out13:  r51c1s19/in30;
 y_out14:  r51c1s19/in32;	  
                                 
// x_out1: r51c2s21u2/in;           //cables 1<->33 exchanged during s318 beamtime.
// y_out16: r51c2s21u4/in;
 x_out1: r51c1s11/in1;           //cables 1<->33 exchanged during s318 beamtime.
 y_out16: r51c1s11/in2;

}

PHILLIPS779(r51c1s19)
{
  SETTING("AMPLIFICATION"=>"10");

 in1:    rGFI1c2/y_out15;
 in3:    rGFI1c2/x_out2;
 in5:    rGFI1c2/x_out3;
 in7:    rGFI1c2/x_out4;
 in9:    rGFI1c2/x_out5;
 in11:   rGFI1c2/x_out6;
 in13:   rGFI1c2/x_out7;
 in15:   rGFI1c2/x_out8;
 in17:   rGFI1c2/x_out9;
 in19:   rGFI1c2/x_out10;
 in21:   rGFI1c2/x_out11;
 in23:   rGFI1c2/x_out12;
 in25:   rGFI1c2/x_out13;
 in27:   rGFI1c2/x_out14;
 in29:   rGFI1c2/x_out15;
 in31:   rGFI1c2/x_out16;

 in2:    rGFI1c2/x_out17;
 in4:    rGFI1c2/x_out18;

 in6:    rGFI1c2/y_out1;
 in8:    rGFI1c2/y_out2;
 in10:   rGFI1c2/y_out3;
 in12:   rGFI1c2/y_out4;
 in14:   rGFI1c2/y_out5;
 in16:   rGFI1c2/y_out6;
 in18:   rGFI1c2/y_out7;
 in20:   rGFI1c2/y_out8;
 in22:   rGFI1c2/y_out9;
 in24:   rGFI1c2/y_out10;
 in26:   rGFI1c2/y_out11;
 in28:   rGFI1c2/y_out12;
 in30:   rGFI1c2/y_out13;
 in32:   rGFI1c2/y_out14;

 out1:  .s17/in1;
 out3:  .s17/in2;
 out5:  .s17/in3;
 out7:  .s17/in4;
 out9:  .s17/in5;
 out11: .s17/in6;
 out13: .s17/in7;
 out15: .s17/in8;
 out17: .s17/in9;
 out19: .s17/in10;
 out21: .s17/in11;
 out23: .s17/in12;
 out25: .s17/in13;
 out27: .s17/in14;
 out29: .s17/in15;
 out31: .s17/in16;

 out2:  .s23/in1;
 out4:  .s23/in2;
 out6:  .s23/in3;
 out8:  .s23/in4;
 out10: .s23/in5;
 out12: .s23/in6;
 out14: .s23/in7;
 out16: .s23/in8;
 out18: .s23/in9;
 out20: .s23/in10;
 out22: .s23/in11;
 out24: .s23/in12;
 out26: .s23/in13;
 out28: .s23/in14;
// out30: .s17/in15; //these 2 channels were not in .s13 as the others
// out32: .s17/in16; //
 out30: .s23/in15;
 out32: .s23/in16;
}

//PHILLIPS779(r51c1s11)
//{
//  SETTING("AMPLIFICATION"=>"10");

// in1:    rGFI1c2/x_out1;
// in2:    rGFI1c2/y_out16;

// out1:  .s13/in1;
// out2:  .s13/in2;
//}

DP1610(r51c1s17)
{
  SETTING("DELAY"=>"500ns");

 in1:  .s19/out1;
 in2:  .s19/out3;
 in3:  .s19/out5;
 in4:  .s19/out7;
 in5:  .s19/out9;
 in6:  .s19/out11;
 in7:  .s19/out13;
 in8:  .s19/out15;
 in9:  .s19/out17;
 in10: .s19/out19;
 in11: .s19/out21;
 in12: .s19/out23;
 in13: .s19/out25;
 in14: .s19/out27;
 in15: .s19/out29;
 in16: .s19/out31;

 out1_8:  r51c11/in1_8;
 out9_16: r51c12/in1_8;
}

JOINER_8(r51c11)
{
 in1_8:  r51c1s17/out1_8;
 out1_8: r51c21/in1_8;
}

JOINER_8(r51c21)
{
 in1_8:  r51c11/out1_8;
 out1_8: r13c2s10/in0_7;
}

JOINER_8(r51c12)
{
 in1_8:  r51c1s17/out9_16;
 out1_8: r51c22/in1_8;
}

JOINER_8(r51c22)
{
 in1_8:  r51c12/out1_8;
 out1_8: r13c2s10/in8_15;
}

DP1610(r51c1s23)
{
  SETTING("DELAY"=>"500ns");

 in1:  .s19/out2;
 in2:  .s19/out4;
 in3:  .s19/out6;
 in4:  .s19/out8;
 in5:  .s19/out10;
 in6:  .s19/out12;
 in7:  .s19/out14;
 in8:  .s19/out16;
 in9:  .s19/out18;
 in10: .s19/out20;
 in11: .s19/out22;
 in12: .s19/out24;
 in13: .s19/out26;
 in14: .s19/out28;
// in15: ;
// in16: ;
 in15: .s19/out30;
 in16: .s19/out32;

 out1_8:  r51c13/in1_8;
 out9_12: r51c14/in1_4; //
 out13:  r51c15/in3; //
 out14_16:  r51c14/in6_8; // 
//out9_16: r51c14/in1_8; // r51c14(5) has a bad shielding,=>connecting to r53c19(3) 18.08.2010
}

JOINER_8(r51c13)
{
 in1_8:  r51c1s23/out1_8;
 out1_8: r51c23/in1_8;
}

JOINER_8(r51c23)
{
 in1_8:  r51c13/out1_8;
 out1_8: r13c2s10/in16_23;
}

JOINER_8(r51c14)
{
// in1_8:  r51c1s23/out9_16; //

 in1_4:  r51c1s23/out9_12;
 in6_8:  r51c1s23/out14_16;

 out1_8: r51c24/in1_8;
}

JOINER_8(r51c24)
{
 in1_8:  r51c14/out1_8;
 out1_8: r13c2s10/in24_31;
}


//DP1610(r51c1s13)
//{
//  SETTING("DELAY"=>"500ns");

// in1:  .s11/out1;
// in2:  .s11/out2;

// out1_2:  r51c15/in1_2;
//}


//DP1610(r51c2s17)
//{
//  SETTING("DELAY"=>"350ns");

// in1: .s21u2/out1;
// in2: .s21u4/out1;

// //in7: .s21u2/out2; //not used in land02
// //in8: .s21u4/out2; //not used in land02

// in15: .s9/out30;
// in16: .s9/out32;
 
//out1_2: r51c15/in1_2;
////out7_8: r51c15/in7_8; //not used in land02
//out15_16: r51c14/in7_8;
//}

//FA8000(r51c2s21)
//{
//  ;
//}

//FA8000_U(r51c2s21u2)
//{
//  SETTING("AMPLIFICATION"=>"10");
// in:    rGFI1c2/x_out1;
// out1:  r51c2s17/in1;
// //out2:  r51c2s17/in7; //not used in land02
//}

//FA8000_U(r51c2s21u4)
//{
//  SETTING("AMPLIFICATION"=>"10");
// in:    rGFI1c2/y_out16;
// out1:  r51c2s17/in2;
// //out2:  r51c2s17/in8; //not used in land02
//}

JOINER_8(r51c15)
{
 in1_2:  r51c1s13/out1_2;
 in3:  r51c1s23/out13;
 //in7_8:  r51c2s17/out7_8; //not used in land02
	 
 out1_8: r51c25/in1_8;
}

JOINER_8(r51c25)
{
 in1_8:  r51c15/out1_8;
 out1_6: r13c2s10/in32_37;
 out7:   r13c11/front10;
}









// GFI 2      /*************************************************/

DETECTOR_GFI(rGFI2c1)
{
  ;
}

DETECTOR_GFI_PSPM(rGFI2c1s1)
{
  ;
}

DETECTOR_GFI_PSPM_U(rGFI2c1s1u1)  { LABEL("GFI02_01"); signal: r51c1s3/in1; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u2)  { LABEL("GFI02_02"); signal: r51c1s3/in2; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u3)  { LABEL("GFI02_03"); signal: r51c1s3/in3; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u4)  { LABEL("GFI02_04"); signal: r51c1s3/in4; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u5)  { LABEL("GFI02_05"); signal: r51c1s3/in5; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u6)  { LABEL("GFI02_06"); signal: r51c1s3/in6; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u7)  { LABEL("GFI02_07"); signal: r51c1s3/in7; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u8)  { LABEL("GFI02_08"); signal: r51c1s3/in8; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u9)  { LABEL("GFI02_09"); signal: r51c1s3/in9; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u10) { LABEL("GFI02_10"); signal: r51c1s3/in10; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u11) { LABEL("GFI02_11"); signal: r51c1s3/in11; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u12) { LABEL("GFI02_12"); signal: r51c1s3/in12; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u13) { LABEL("GFI02_13"); signal: r51c1s3/in13; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u14) { LABEL("GFI02_14"); signal: r51c1s3/in14; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u15) { LABEL("GFI02_15"); signal: r51c1s3/in15; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u16) { LABEL("GFI02_16"); signal: r51c1s3/in16; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u17) { LABEL("GFI02_17"); signal: r51c1s3/in17; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u18) { LABEL("GFI02_18"); signal: r51c1s3/in18; }

DETECTOR_GFI_PSPM_U(rGFI2c1s1u19) { LABEL("GFI02_19"); signal: r51c1s3/in19; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u20) { LABEL("GFI02_20"); signal: r51c1s3/in20; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u21) { LABEL("GFI02_21"); signal: r51c1s3/in21; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u22) { LABEL("GFI02_22"); signal: r51c1s3/in22; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u23) { LABEL("GFI02_23"); signal: r51c1s3/in23; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u24) { LABEL("GFI02_24"); signal: r51c1s3/in24; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u25) { LABEL("GFI02_25"); signal: r51c1s3/in25; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u26) { LABEL("GFI02_26"); signal: r51c1s3/in26; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u27) { LABEL("GFI02_27"); signal: r51c1s3/in27; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u28) { LABEL("GFI02_28"); signal: r51c1s3/in28; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u29) { LABEL("GFI02_29"); signal: r51c1s3/in29; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u30) { LABEL("GFI02_30"); signal: r51c1s3/in30; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u31) { LABEL("GFI02_31"); signal: r51c1s3/in31; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u32) { LABEL("GFI02_32"); signal: r51c1s3/in32; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u33) { LABEL("GFI02_33"); signal: r51c1s11/in5; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u34) { LABEL("GFI02_34"); signal: r51c1s11/in6; }
// TODO: unhack this!
DETECTOR_GFI_PSPM_U(rGFI2c1s1u35) { LABEL("GFI02_35"); signal: r13c11/front18; }


// Amplifier and delay for GFI 2

PHILLIPS779(r51c1s3)
{
 in1_32: rGFI2c1s1u1_32/signal;

 out1:  .s5/in1;
 out3:  .s5/in2;
 out5:  .s5/in3;
 out7:  .s5/in4;
 out9:  .s5/in5;
 out11: .s5/in6;
 out13: .s5/in7;
 out15: .s5/in8;
 out17: .s5/in9;
 out19: .s5/in10;
 out21: .s5/in11;
 out23: .s5/in12;
 out25: .s5/in13;
 out27: .s5/in14;
 out29: .s5/in15;
 out31: .s5/in16;

 out2:  .s7/in1;
 out4:  .s7/in2;
 out6:  .s7/in3;
 out8:  .s7/in4;
 out10: .s7/in5;
 out12: .s7/in6;
 out14: .s7/in7;
 out16: .s7/in8;
 out18: .s7/in9;
 out20: .s7/in10;
 out22: .s7/in11;
 out24: .s7/in12;
 out26: .s7/in13;
 out28: .s7/in14;
 out30: .s7/in15;
 out32: .s7/in16;
}

DP1610(r51c1s5)
{
  SETTING("DELAY"=>"500ns");

 in1:  .s3/out1; 
 in2:  .s3/out3; 
 in3:  .s3/out5; 
 in4:  .s3/out7; 
 in5:  .s3/out9; 
 in6:  .s3/out11;
 in7:  .s3/out13;
 in8:  .s3/out15;
 in9:  .s3/out17;
 in10: .s3/out19;
 in11: .s3/out21;
 in12: .s3/out23;
 in13: .s3/out25;
 in14: .s3/out27;
 in15: .s3/out29;
 in16: .s3/out31;

 out1:  r53c11/in1;
 out2:  r53c11/in3;
 out3:  r53c11/in5;
// out4:  r53c11/in7; //changed on 18.08.2010
 out4:  r53c19/in5; // with this line
 out5:  r53c12/in1;
 out6:  r53c12/in3;
 out7:  r53c12/in5;
 out8:  r53c12/in7;
 out9:  r53c13/in1;
 out10: r53c13/in3;
 out11: r53c13/in5;
 out12: r53c13/in7;
 out13: r53c14/in1;
 out14: r53c14/in3;
 out15: r53c14/in5;
 out16: r53c14/in7;
}

DP1610(r51c1s7)
{
  SETTING("DELAY"=>"500ns");

 in1:  .s3/out2;
 in2:  .s3/out4;
 in3:  .s3/out6;
 in4:  .s3/out8;
 in5:  .s3/out10;
 in6:  .s3/out12;
 in7:  .s3/out14;
 in8:  .s3/out16;
 in9:  .s3/out18;
 in10: .s3/out20;
 in11: .s3/out22;
 in12: .s3/out24;
 in13: .s3/out26;
 in14: .s3/out28;
 in15: .s3/out30;
 in16: .s3/out32;

 out1:  r53c11/in2;
 out2:  r53c11/in4;
 out3:  r53c11/in6;
 out4:  r53c11/in8;
 out5:  r53c12/in2;
 out6:  r53c12/in4;
 out7:  r53c12/in6;
 out8:  r53c12/in8;
 out9:  r53c13/in2;
 out10: r53c13/in4;
 out11: r53c13/in6;
 out12: r53c13/in8;
 out13: r53c14/in2;
 out14: r53c14/in4;
 out15: r53c14/in6;
 out16: r53c14/in8;
}

JOINER_8(r53c11)
{
 in1:    r51c1s5/out1;
 in3:    r51c1s5/out2;
 in5:    r51c1s5/out3;
// in7:    r51c1s5/out4; // commented out on 18.08.2010 because signal goes from r51c1s5->r53c19 directly

 in2:    r51c1s7/out1;
 in4:    r51c1s7/out2;
 in6:    r51c1s7/out3;
 in8:    r51c1s7/out4;

 // out1_8: r53c21/in1_8;
 out1_6: r53c21/in1_6;
// out7: r53c19/in5; // commented out on 18.08.2010 because signal goes from r51c1s5->r53c19 directly
 out8: r53c21/in8;
}

JOINER_8(r53c21)
{
  // in1_8:  r53c11/out1_8;
 in1_6:  r53c11/out1_6;
 in8:    r53c11/out8;
 out1_8: r13c2s23/in0_7;
}
JOINER_8(r53c12)
{
 in1:    r51c1s5/out5;
 in3:    r51c1s5/out6;
 in5:    r51c1s5/out7;
 in7:    r51c1s5/out8;

 in2:    r51c1s7/out5;
 in4:    r51c1s7/out6;
 in6:    r51c1s7/out7;
 in8:    r51c1s7/out8;

 out1_8: r53c22/in1_8;
}

JOINER_8(r53c22)
{
 in1_8:  r53c12/out1_8;
 out1_8: r13c2s23/in8_15;
}
JOINER_8(r53c13)
{
 in1:    r51c1s5/out9;
 in3:    r51c1s5/out10;
 in5:    r51c1s5/out11;
 in7:    r51c1s5/out12;

 in2:    r51c1s7/out9;
 in4:    r51c1s7/out10;
 in6:    r51c1s7/out11;
 in8:    r51c1s7/out12;

 out1_8: r53c23/in1_8;
}

JOINER_8(r53c23)
{
 in1_8:  r53c13/out1_8;
 out1_8: r13c2s23/in16_23;
}
JOINER_8(r53c14)
{
 in1:    r51c1s5/out13;
 in3:    r51c1s5/out14;
 in5:    r51c1s5/out15;
 in7:    r51c1s5/out16;

 in2:    r51c1s7/out13;
 in4:    r51c1s7/out14;
 in6:    r51c1s7/out15;
 in8:    r51c1s7/out16;

// out1_8: r53c24/in1_8;
  out1_6: r53c24/in1_6;
  out7: r53c30/in1;
  out8: r53c24/in8;
}

JOINER_8(r53c24)
{
 //in1_8:  r53c14/out1_8;
  in1_6: r53c14/out1_6;
  in8: r53c14/out8;
  //out1_8: r13c2s23/in24_31;
  out1_6: r13c2s23/in24_29;
  out8: r13c2s23/in31;
}
JOINER_8(r53c30)
{
 in1:  r53c14/out7;
 out1: r13c2s23/in72;
}

// Amplifier and delay for GFI 1 & 2 (channels 33, 34)


PHILLIPS779(r51c1s11)
{
  SETTING("AMPLIFICATION"=>"10");

 in1:    rGFI1c2/x_out1;
 in2:    rGFI1c2/y_out16;

 in5:    rGFI2c1s1u33/signal;
 in6:    rGFI2c1s1u34/signal;


 out1:  .s13/in1;
 out2:  .s13/in2;

 out5:  .s13/in5;
 out6:  .s13/in6;
}

DP1610(r51c1s13)
{
  SETTING("DELAY"=>"500ns");

 in1:  .s11/out1;
 in2:  .s11/out2;

 in5_6:  .s11/out5_6;
// in5_6: .s13u1_2/out1;
// in7_8: .s13u3_4/out1;



 out1_2:  r51c15/in1_2;

 out5_6: r53c19/in1_2;
// out5_8: r53c19/in1_4;
}

JOINER_8(r53c19)
{
// in1_4:  r51c2s15/out5_8;
 in1_2:  r51c1s13/out5_6;
// in5:    r53c11/out7; // commented out on 18.08.2010 because signal goes from r51c1s5->r53c19 directly
 in5:  r51c1s5/out4; // and added this

 out1_8: r53c29/in1_8;
}

JOINER_8(r53c29)
{
 in1_8:  r53c19/out1_8;

 out1_6: r13c2s23/in64_69;
 out7:   r13c11/front11;	 
}

//final

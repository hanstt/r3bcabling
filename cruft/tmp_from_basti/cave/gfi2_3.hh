// // GFI 2
// 
// DETECTOR_GFI(rGFI2c1)
// {
//   ;
// }
// 
// DETECTOR_GFI_PSPM(rGFI2c1s1)
// {
//   ;
// }
// 
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u1)  { LABEL("GFI02_01"); signal: r53c1s3/in1; }
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u2)  { LABEL("GFI02_02"); signal: r53c1s3/in2; }
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u3)  { LABEL("GFI02_03"); signal: r53c1s3/in3; }
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u4)  { LABEL("GFI02_04"); signal: r53c1s3/in4; }
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u5)  { LABEL("GFI02_05"); signal: r53c1s3/in5; }
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u6)  { LABEL("GFI02_06"); signal: r53c1s3/in6; }
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u7)  { LABEL("GFI02_07"); signal: r53c1s3/in7; }
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u8)  { LABEL("GFI02_08"); signal: r53c1s3/in8; }
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u9)  { LABEL("GFI02_09"); signal: r53c1s3/in9; }
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u10) { LABEL("GFI02_10"); signal: r53c1s3/in10; }
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u11) { LABEL("GFI02_11"); signal: r53c1s3/in11; }
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u12) { LABEL("GFI02_12"); signal: r53c1s3/in12; }
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u13) { LABEL("GFI02_13"); signal: r53c1s3/in13; }
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u14) { LABEL("GFI02_14"); signal: r53c1s3/in14; }
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u15) { LABEL("GFI02_15"); signal: r53c1s3/in15; }
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u16) { LABEL("GFI02_16"); signal: r53c1s3/in16; }
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u17) { LABEL("GFI02_17"); signal: r53c1s3/in17; }
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u18) { LABEL("GFI02_18"); signal: r53c1s3/in18; }
// 
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u19) { LABEL("GFI02_19"); signal: r53c1s3/in19; }
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u20) { LABEL("GFI02_20"); signal: r53c1s3/in20; }
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u21) { LABEL("GFI02_21"); signal: r53c1s3/in21; }
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u22) { LABEL("GFI02_22"); signal: r53c1s3/in22; }
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u23) { LABEL("GFI02_23"); signal: r53c1s3/in23; }
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u24) { LABEL("GFI02_24"); signal: r53c1s3/in24; }
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u25) { LABEL("GFI02_25"); signal: r53c1s3/in25; }
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u26) { LABEL("GFI02_26"); signal: r53c1s3/in26; }
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u27) { LABEL("GFI02_27"); signal: r53c1s3/in27; }
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u28) { LABEL("GFI02_28"); signal: r53c1s3/in28; }
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u29) { LABEL("GFI02_29"); signal: r53c1s3/in29; }
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u30) { LABEL("GFI02_30"); signal: r53c1s3/in30; }
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u31) { LABEL("GFI02_31"); signal: r53c1s3/in31; }
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u32) { LABEL("GFI02_32"); signal: r53c1s3/in32; }
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u33) { LABEL("GFI02_33"); signal: r53c2s13u1/in; }
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u34) { LABEL("GFI02_34"); signal: r53c2s13u2/in; }
// // TODO: unhack this!
// DETECTOR_GFI_PSPM_U(rGFI2c1s1u35) { LABEL("GFI02_35"); signal: r13c11/front18; }
// 
// // GFI 3
// 
// DETECTOR_GFI(rGFI3c1)
// {
//   ;
// }
// 
// DETECTOR_GFI_PSPM(rGFI3c1s1)
// {
//   ;
// }
// 
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u1)  { LABEL("GFI03_01"); signal: r53c1s21/in1; }
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u2)  { LABEL("GFI03_02"); signal: r53c1s21/in2; }
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u3)  { LABEL("GFI03_03"); signal: r53c1s21/in3; }
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u4)  { LABEL("GFI03_04"); signal: r53c1s21/in4; }
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u5)  { LABEL("GFI03_05"); signal: r53c1s21/in5; }
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u6)  { LABEL("GFI03_06"); signal: r53c1s21/in6; }
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u7)  { LABEL("GFI03_07"); signal: r53c1s21/in7; }
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u8)  { LABEL("GFI03_08"); signal: r53c1s21/in8; }
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u9)  { LABEL("GFI03_09"); signal: r53c1s21/in9; }
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u10) { LABEL("GFI03_10"); signal: r53c1s21/in10; }
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u11) { LABEL("GFI03_11"); signal: r53c1s21/in11; }
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u12) { LABEL("GFI03_12"); signal: r53c1s21/in12; }
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u13) { LABEL("GFI03_13"); signal: r53c1s21/in13; }
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u14) { LABEL("GFI03_14"); signal: r53c1s21/in14; }
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u15) { LABEL("GFI03_15"); signal: r53c1s21/in15; }
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u16) { LABEL("GFI03_16"); signal: r53c1s21/in16; }
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u17) { LABEL("GFI03_17"); signal: r53c1s21/in17; }
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u18) { LABEL("GFI03_18"); signal: r53c1s21/in18; }
// 
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u19) { LABEL("GFI03_19"); signal: r53c1s21/in19; }
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u20) { LABEL("GFI03_20"); signal: r53c1s21/in20; }
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u21) { LABEL("GFI03_21"); signal: r53c1s21/in21; }
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u22) { LABEL("GFI03_22"); signal: r53c1s21/in22; }
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u23) { LABEL("GFI03_23"); signal: r53c1s21/in23; }
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u24) { LABEL("GFI03_24"); signal: r53c1s21/in24; }
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u25) { LABEL("GFI03_25"); signal: r53c1s21/in25; }
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u26) { LABEL("GFI03_26"); signal: r53c1s21/in26; }
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u27) { LABEL("GFI03_27"); signal: r53c1s21/in27; }
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u28) { LABEL("GFI03_28"); signal: r53c1s21/in28; }
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u29) { LABEL("GFI03_29"); signal: r53c1s21/in29; }
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u30) { LABEL("GFI03_30"); signal: r53c1s21/in30; }
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u31) { LABEL("GFI03_31"); signal: r53c1s21/in31; }
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u32) { LABEL("GFI03_32"); signal: r53c1s21/in32; }
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u33) { LABEL("GFI03_33"); signal: r53c2s13u3/in; }
// DETECTOR_GFI_PSPM_U(rGFI3c1s1u34) { LABEL("GFI03_34"); signal: r53c2s13u4/in; }
// 
// 
// 
// 
// // Amplifier and delay for GFI 2
// 
// PHILLIPS779(r53c1s3)
// {
//  in1_32: rGFI2c1s1u1_32/signal;
// 
//  out1:  .s5/in1;
//  out3:  .s5/in2;
//  out5:  .s5/in3;
//  out7:  .s5/in4;
//  out9:  .s5/in5;
//  out11: .s5/in6;
//  out13: .s5/in7;
//  out15: .s5/in8;
//  out17: .s5/in9;
//  out19: .s5/in10;
//  out21: .s5/in11;
//  out23: .s5/in12;
//  out25: .s5/in13;
//  out27: .s5/in14;
//  out29: .s5/in15;
//  out31: .s5/in16;
// 
//  out2:  .s7/in1;
//  out4:  .s7/in2;
//  out6:  .s7/in3;
//  out8:  .s7/in4;
//  out10: .s7/in5;
//  out12: .s7/in6;
//  out14: .s7/in7;
//  out16: .s7/in8;
//  out18: .s7/in9;
//  out20: .s7/in10;
//  out22: .s7/in11;
//  out24: .s7/in12;
//  out26: .s7/in13;
//  out28: .s7/in14;
//  out30: .s7/in15;
//  out32: .s7/in16;
// }
// 
// DP1610(r53c1s5)
// {
//   SETTING("DELAY"=>"500ns");
// 
//  in1:  .s3/out1; 
//  in2:  .s3/out3; 
//  in3:  .s3/out5; 
//  in4:  .s3/out7; 
//  in5:  .s3/out9; 
//  in6:  .s3/out11;
//  in7:  .s3/out13;
//  in8:  .s3/out15;
//  in9:  .s3/out17;
//  in10: .s3/out19;
//  in11: .s3/out21;
//  in12: .s3/out23;
//  in13: .s3/out25;
//  in14: .s3/out27;
//  in15: .s3/out29;
//  in16: .s3/out31;
// 
//  out1:  r53c11/in1;
//  out2:  r53c11/in3;
//  out3:  r53c11/in5;
//  out4:  r53c11/in7;
//  out5:  r53c12/in1;
//  out6:  r53c12/in3;
//  out7:  r53c12/in5;
//  out8:  r53c12/in7;
//  out9:  r53c13/in1;
//  out10: r53c13/in3;
//  out11: r53c13/in5;
//  out12: r53c13/in7;
//  out13: r53c14/in1;
//  out14: r53c14/in3;
//  out15: r53c14/in5;
//  out16: r53c14/in7;
// }
// 
// DP1610(r53c1s7)
// {
//   SETTING("DELAY"=>"500ns");
// 
//  in1:  .s3/out2;
//  in2:  .s3/out4;
//  in3:  .s3/out6;
//  in4:  .s3/out8;
//  in5:  .s3/out10;
//  in6:  .s3/out12;
//  in7:  .s3/out14;
//  in8:  .s3/out16;
//  in9:  .s3/out18;
//  in10: .s3/out20;
//  in11: .s3/out22;
//  in12: .s3/out24;
//  in13: .s3/out26;
//  in14: .s3/out28;
//  in15: .s3/out30;
//  in16: .s3/out32;
// 
//  out1:  r53c11/in2;
//  out2:  r53c11/in4;
//  out3:  r53c11/in6;
//  out4:  r53c11/in8;
//  out5:  r53c12/in2;
//  out6:  r53c12/in4;
//  out7:  r53c12/in6;
//  out8:  r53c12/in8;
//  out9:  r53c13/in2;
//  out10: r53c13/in4;
//  out11: r53c13/in6;
//  out12: r53c13/in8;
//  out13: r53c14/in2;
//  out14: r53c14/in4;
//  out15: r53c14/in6;
//  out16: r53c14/in8;
// }
// 
// JOINER_8(r53c11)
// {
//  in1:    r53c1s5/out1;
//  in3:    r53c1s5/out2;
//  in5:    r53c1s5/out3;
//  in7:    r53c1s5/out4;
// 
//  in2:    r53c1s7/out1;
//  in4:    r53c1s7/out2;
//  in6:    r53c1s7/out3;
//  in8:    r53c1s7/out4;
// 
//  // out1_8: r53c21/in1_8;
//  out1_6: r53c21/in1_6;
//  out7: r53c19/in5;
//  out8: r53c21/in8;
// }
// 
// JOINER_8(r53c21)
// {
//   // in1_8:  r53c11/out1_8;
//  in1_6:  r53c11/out1_6;
//  in8:    r53c11/out8;
//  out1_8: r13c2s23/in0_7;
// }
// JOINER_8(r53c12)
// {
//  in1:    r53c1s5/out5;
//  in3:    r53c1s5/out6;
//  in5:    r53c1s5/out7;
//  in7:    r53c1s5/out8;
// 
//  in2:    r53c1s7/out5;
//  in4:    r53c1s7/out6;
//  in6:    r53c1s7/out7;
//  in8:    r53c1s7/out8;
// 
//  out1_8: r53c22/in1_8;
// }
// 
// JOINER_8(r53c22)
// {
//  in1_8:  r53c12/out1_8;
//  out1_8: r13c2s23/in8_15;
// }
// JOINER_8(r53c13)
// {
//  in1:    r53c1s5/out9;
//  in3:    r53c1s5/out10;
//  in5:    r53c1s5/out11;
//  in7:    r53c1s5/out12;
// 
//  in2:    r53c1s7/out9;
//  in4:    r53c1s7/out10;
//  in6:    r53c1s7/out11;
//  in8:    r53c1s7/out12;
// 
//  out1_8: r53c23/in1_8;
// }
// 
// JOINER_8(r53c23)
// {
//  in1_8:  r53c13/out1_8;
//  out1_8: r13c2s23/in16_23;
// }
// JOINER_8(r53c14)
// {
//  in1:    r53c1s5/out13;
//  in3:    r53c1s5/out14;
//  in5:    r53c1s5/out15;
//  in7:    r53c1s5/out16;
// 
//  in2:    r53c1s7/out13;
//  in4:    r53c1s7/out14;
//  in6:    r53c1s7/out15;
//  in8:    r53c1s7/out16;
// 
// // out1_8: r53c24/in1_8;
//   out1_6: r53c24/in1_6;
//   out7: r53c30/in1;
//   out8: r53c24/in8;
// }
// 
// JOINER_8(r53c24)
// {
//  //in1_8:  r53c14/out1_8;
//   in1_6: r53c14/out1_6;
// 	in8: r53c14/out8;
// 	//out1_8: r13c2s23/in24_31;
// 	out1_6: r13c2s23/in24_29;
// 	out8: r13c2s23/in31;
// }
// JOINER_8(r53c30)
// {
//  in1:  r53c14/out7;
//  out1: r13c2s23/in72;
// }
// 
// // Amplifier and delay for GFI 3
// 
// DP1610(r53c1s19)
// {
//   SETTING("DELAY"=>"500ns");
// 
//  in1:  .s21/out1;
//  in2:  .s21/out3;
//  in3:  .s21/out5;
//  in4:  .s21/out7;
//  in5:  .s21/out9;
//  in6:  .s21/out11;
//  in7:  .s21/out13;
//  in8:  .s21/out15;
//  in9:  .s21/out17;
//  in10: .s21/out19;
//  in11: .s21/out21;
//  in12: .s21/out23;
//  in13: .s21/out25;
//  in14: .s21/out27;
//  in15: .s21/out29;
//  in16: .s21/out31;
// 
//  out1:  r53c15/in1;
//  out2:  r53c15/in3;
//  out3:  r53c15/in5;
//  out4:  r53c15/in7;
//  out5:  r53c16/in1;
//  out6:  r53c16/in3;
//  out7:  r53c16/in5;
//  out8:  r53c16/in7;
//  out9:  r53c17/in1;
//  out10: r53c17/in3;
//  out11: r53c17/in5;
//  out12: r53c17/in7;
//  out13: r53c18/in1;
//  out14: r53c18/in3;
//  out15: r53c18/in5;
//  out16: r53c18/in7;
// }
// 
// PHILLIPS779(r53c1s21)
// {
//  in1_32: rGFI3c1s1u1_32/signal;
// 
//  out1:  .s19/in1;
//  out3:  .s19/in2;
//  out5:  .s19/in3;
//  out7:  .s19/in4;
//  out9:  .s19/in5;
//  out11: .s19/in6;
//  out13: .s19/in7;
//  out15: .s19/in8;
//  out17: .s19/in9;
//  out19: .s19/in10;
//  out21: .s19/in11;
//  out23: .s19/in12;
//  out25: .s19/in13;
//  out27: .s19/in14;
//  out29: .s19/in15;
//  out31: .s19/in16;
// 
//  out2:  .s23/in1;
//  out4:  .s23/in2;
//  out6:  .s23/in3;
//  out8:  .s23/in4;
//  out10: .s23/in5;
//  out12: .s23/in6;
//  out14: .s23/in7;
//  out16: .s23/in8;
//  out18: .s23/in9;
//  out20: .s23/in10;
//  out22: .s23/in11;
//  out24: .s23/in12;
//  out26: .s23/in13;
//  out28: .s23/in14;
//  out30: .s23/in15;
//  out32: .s23/in16;
// }
// 
// 
// DP1610(r53c1s23)
// {
//   SETTING("DELAY"=>"500ns");
// 
//  in1:  .s21/out2;
//  in2:  .s21/out4;
//  in3:  .s21/out6;
//  in4:  .s21/out8;
//  in5:  .s21/out10;
//  in6:  .s21/out12;
//  in7:  .s21/out14;
//  in8:  .s21/out16;
//  in9:  .s21/out18;
//  in10: .s21/out20;
//  in11: .s21/out22;
//  in12: .s21/out24;
//  in13: .s21/out26;
//  in14: .s21/out28;
//  in15: .s21/out30;
//  in16: .s21/out32;
// 
//  out1:  r53c15/in2;
//  out2:  r53c15/in4;
//  out3:  r53c15/in6;
//  out4:  r53c15/in8;
//  out5:  r53c16/in2;
//  out6:  r53c16/in4;
//  out7:  r53c16/in6;
//  out8:  r53c16/in8;
//  out9:  r53c17/in2;
//  out10: r53c17/in4;
//  out11: r53c17/in6;
//  out12: r53c17/in8;
//  out13: r53c18/in2;
//  out14: r53c18/in4;
//  out15: r53c18/in6;
//  out16: r53c18/in8;
// }
// 
// JOINER_8(r53c15)
// {
//  in1:    r53c1s19/out1;
//  in3:    r53c1s19/out2;
//  in5:    r53c1s19/out3;
//  in7:    r53c1s19/out4;
// 
//  in2:    r53c1s23/out1;
//  in4:    r53c1s23/out2;
//  in6:    r53c1s23/out3;
//  in8:    r53c1s23/out4;
// 
//  out1_8: r53c25/in1_8;
// }
// 
// JOINER_8(r53c25)
// {
//  in1_8:  r53c15/out1_8;
//  out1_8: r13c2s23/in32_39;
// }
// JOINER_8(r53c16)
// {
//  in1:    r53c1s19/out5;
//  in3:    r53c1s19/out6;
//  in5:    r53c1s19/out7;
//  in7:    r53c1s19/out8;
// 
//  in2:    r53c1s23/out5;
//  in4:    r53c1s23/out6;
//  in6:    r53c1s23/out7;
//  in8:    r53c1s23/out8;
// 
//  out1_8: r53c26/in1_8;
// }
// 
// JOINER_8(r53c26)
// {
//  in1_8:  r53c16/out1_8;
//  out1_8: r13c2s23/in40_47;
// }
// JOINER_8(r53c17)
// {
//  in1:    r53c1s19/out9;
//  in3:    r53c1s19/out10;
//  in5:    r53c1s19/out11;
//  in7:    r53c1s19/out12;
// 
//  in2:    r53c1s23/out9;
//  in4:    r53c1s23/out10;
//  in6:    r53c1s23/out11;
//  in8:    r53c1s23/out12;
// 
//  out1_8: r53c27/in1_8;
// }
// 
// JOINER_8(r53c27)
// {
//  in1_8:  r53c17/out1_8;
//  out1_8: r13c2s23/in48_55;
// }
// JOINER_8(r53c18)
// {
//  in1:    r53c1s19/out13;
//  in3:    r53c1s19/out14;
//  in5:    r53c1s19/out15;
//  in7:    r53c1s19/out16;
// 
//  in2:    r53c1s23/out13;
//  in4:    r53c1s23/out14;
//  in6:    r53c1s23/out15;
//  in8:    r53c1s23/out16;
// 
//  out1_8: r53c28/in1_8;
// }
// 
// JOINER_8(r53c28)
// {
//  in1_8:  r53c18/out1_8;
//  out1_8: r13c2s23/in56_63;
// }
// 
// 
// // Amplifier and delay for GFI 2 & 3 (channels 33, 34)
// 
// FA8000(r53c2s13)
// {
//   ;
// }
// 
// FA8000_U(r53c2s13u1)
// {
//  in:   rGFI2c1s1u33/signal;
//  out1: .s15/in5;
// }
// 
// FA8000_U(r53c2s13u2)
// {
//  in:   rGFI2c1s1u34/signal;
//  out1: .s15/in6;
// }
// 
// FA8000_U(r53c2s13u3)
// {
//  in:   rGFI3c1s1u33/signal;
//  out1: .s15/in7;
// }
// 
// FA8000_U(r53c2s13u4)
// {
//  in:   rGFI3c1s1u34/signal;
//  out1: .s15/in8;
// }
// 
// DP1610(r53c2s15)
// {
//   SETTING("DELAY"=>"500ns");
// 
//  in5_6: .s13u1_2/out1;
//  in7_8: .s13u3_4/out1;
// 
//  out5_8: r53c19/in1_4;
// }
// 
// JOINER_8(r53c19)
// {
//  in1_4:  r53c2s15/out5_8;
//  in5:    r53c11/out7;
//  out1_8: r53c29/in1_8;
// }
// 
// JOINER_8(r53c29)
// {
//  in1_8:  r53c19/out1_8;
//  out1_6: r13c2s23/in64_69;
//  out7:   r13c11/front11;	 
// }

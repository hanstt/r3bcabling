/*
 * Crate 12.  Gamma amplifiers.  
 * (soft gamma -> hard gamma)
 * Photon torpedoes.
 */

NIM_CRATE(r15c12)
{
  ;
}

TFA2000_6(r15c12s3)     { ; }

TFA2000_6_U(r15c12s3u1) { in: r13c3s17/t1;   out_neg: .s11u1/in;  }
TFA2000_6_U(r15c12s3u2) { in: r13c3s17/t2;   out_neg: .s11u2/in;  }
TFA2000_6_U(r15c12s3u3) { in: r13c3s17/t3;   out_neg: .s11u3/in;  }
TFA2000_6_U(r15c12s3u4) { in: r13c3s17/t4;   out_neg: .s11u4/in;  }
TFA2000_6_U(r15c12s3u5) { in: r13c3s17/t5;   out_neg: .s11u5/in;  }
TFA2000_6_U(r15c12s3u6) { in: r13c3s17/t6;   out_neg: .s11u6/in;  }

TFA2000_6(r15c12s7)     { ; }

TFA2000_6_U(r15c12s7u1) { in: r13c3s17/t7;   out_neg: .s11u7/in;  }
TFA2000_6_U(r15c12s7u2) { in: r13c3s17/t8;   out_neg: .s11u8/in;  }
TFA2000_6_U(r15c12s7u3) { in: r13c3s17/t9;   out_neg: .s11u9/in;  }
TFA2000_6_U(r15c12s7u4) { in: r13c3s17/t10;  out_neg: .s11u10/in; }
TFA2000_6_U(r15c12s7u5) { in: r13c3s17/t11;  out_neg: .s11u11/in; }
TFA2000_6_U(r15c12s7u6) { in: r13c3s17/t12;  out_neg: .s11u12/in; }

HDA2000_12(r15c12s11) { ; }

HDA2000_12_U(r15c12s11u1)  { in: .s3u1/out_neg;  out: r14c6s5/in1; }
HDA2000_12_U(r15c12s11u2)  { in: .s3u2/out_neg;  out: r14c6s5/in2; }
HDA2000_12_U(r15c12s11u3)  { in: .s3u3/out_neg;  out: r14c6s5/in3; }
HDA2000_12_U(r15c12s11u4)  { in: .s3u4/out_neg;  out: r14c6s5/in4; }
HDA2000_12_U(r15c12s11u5)  { in: .s3u5/out_neg;  out: r14c6s5/in5; }
HDA2000_12_U(r15c12s11u6)  { in: .s3u6/out_neg;  out: r14c6s5/in6; }
HDA2000_12_U(r15c12s11u7)  { in: .s7u1/out_neg;  out: r14c6s5/in7; }
HDA2000_12_U(r15c12s11u8)  { in: .s7u2/out_neg;  out: r14c6s5/in8; }
HDA2000_12_U(r15c12s11u9)  { in: .s7u3/out_neg;  out: r14c6s6/in1; }
HDA2000_12_U(r15c12s11u10) { in: .s7u4/out_neg;  out: r14c6s6/in2; }
HDA2000_12_U(r15c12s11u11) { in: .s7u5/out_neg;  out: r14c6s6/in3; }
HDA2000_12_U(r15c12s11u12) { in: .s7u6/out_neg;  out: r14c6s6/in4; }


TFA2000_6(r15c12s15)     { ; }

TFA2000_6_U(r15c12s15u1) { in: r13c3s17/t13;  out_neg: .s23u1/in;  }
TFA2000_6_U(r15c12s15u2) { in: r13c3s17/t14;  out_neg: .s23u2/in;  }
TFA2000_6_U(r15c12s15u3) { in: r13c3s17/t15;  out_neg: .s23u3/in;  }
TFA2000_6_U(r15c12s15u4) { in: r13c3s17/t16;  out_neg: .s23u4/in;  }
TFA2000_6_U(r15c12s15u5) { ; } //in: r13c3s21/t1;   out_neg: .s23u5/in;  }
TFA2000_6_U(r15c12s15u6) { ; } //in: r13c3s21/t2;   out_neg: .s23u6/in;  }

TFA2000_6(r15c12s19)     { ; }

TFA2000_6_U(r15c12s19u1) { ; } //in: r13c3s21/t3;   out_neg: .s23u7/in;  }
TFA2000_6_U(r15c12s19u2) { ; } //in: r13c3s21/t4;   out_neg: .s23u8/in;  }
TFA2000_6_U(r15c12s19u3) { ; } //in: r13c3s21/t5;   out_neg: .s23u9/in;  }
TFA2000_6_U(r15c12s19u4) { ; } //in: r13c3s21/t6;   out_neg: .s23u10/in; }
TFA2000_6_U(r15c12s19u5) { ; } //in: r13c3s21/t7;   out_neg: .s23u11/in; }
TFA2000_6_U(r15c12s19u6) { ; } //in: r13c3s21/t8;   out_neg: .s23u12/in; }

HDA2000_12(r15c12s23) { ; }

HDA2000_12_U(r15c12s23u1)  { in: .s15u1/out_neg;  out: r14c6s6/in5; }
HDA2000_12_U(r15c12s23u2)  { in: .s15u2/out_neg;  out: r14c6s6/in6; }
HDA2000_12_U(r15c12s23u3)  { in: .s15u3/out_neg;  out: r14c6s6/in7; }
HDA2000_12_U(r15c12s23u4)  { in: .s15u4/out_neg;  out: r14c6s6/in8; }
HDA2000_12_U(r15c12s23u5)  { ; } //in: .s15u5/out_neg;  out: r14c6s7/in1; }
HDA2000_12_U(r15c12s23u6)  { ; } //in: .s15u6/out_neg;  out: r14c6s7/in2; }
HDA2000_12_U(r15c12s23u7)  { ; } //in: .s19u1/out_neg;  out: r14c6s7/in3; }
HDA2000_12_U(r15c12s23u8)  { ; } //in: .s19u2/out_neg;  out: r14c6s7/in4; }
HDA2000_12_U(r15c12s23u9)  { ; } //in: .s19u3/out_neg;  out: r14c6s7/in5; }
HDA2000_12_U(r15c12s23u10) { ; } //in: .s19u4/out_neg;  out: r14c6s7/in6; }
HDA2000_12_U(r15c12s23u11) { ; } //in: .s19u5/out_neg;  out: r14c6s7/in7; }
HDA2000_12_U(r15c12s23u12) { ; } //in: .s19u6/out_neg;  out: r14c6s7/in8; }





/*
 * Crate 15.  Gamma amplifiers.  
 */

NIM_CRATE(r15c15)
{
  ;
}

TFA2000_8(r15c15s3)     { ; }

// Note: according to old labling, units 1 and 4 were broken.
// Testing showed that 3 is broken.
TFA2000_8_U(r15c15s3u1) { in: r13c3s16/t1;   out_neg: .s11u1/in;  }
TFA2000_8_U(r15c15s3u2) { in: r13c3s16/t2;   out_neg: .s11u2/in;  }
TFA2000_8_U(r15c15s3u3) { BROKEN; }
TFA2000_8_U(r15c15s3u4) { /* spare */; }
TFA2000_8_U(r15c15s3u5) { in: r13c3s16/t3;   out_neg: .s11u3/in;  }
TFA2000_8_U(r15c15s3u6) { in: r13c3s16/t4;   out_neg: .s11u4/in;  }
TFA2000_8_U(r15c15s3u7) { in: r13c3s16/t5;   out_neg: .s11u5/in;  }
TFA2000_8_U(r15c15s3u8) { in: r13c3s16/t6;   out_neg: .s11u6/in;  }

TFA2000_6(r15c15s7)     { ; }

TFA2000_6_U(r15c15s7u1) { in: r13c3s16/t7;   out_neg: .s11u7/in;  }
TFA2000_6_U(r15c15s7u2) { in: r13c3s16/t8;   out_neg: .s11u8/in;  }
TFA2000_6_U(r15c15s7u3) { in: r13c3s12/t9;   out_neg: .s11u9/in;  }
TFA2000_6_U(r15c15s7u4) { in: r13c3s12/t10;  out_neg: .s11u10/in; }
TFA2000_6_U(r15c15s7u5) { in: r13c3s12/t11;  out_neg: .s11u11/in; }
TFA2000_6_U(r15c15s7u6) { in: r13c3s12/t12;  out_neg: .s11u12/in; }

HDA2000_12(r15c15s11)      { ; }

HDA2000_12_U(r15c15s11u1)  { in: .s3u1/out_neg;  out: r14c10s21/in1; }
HDA2000_12_U(r15c15s11u2)  { in: .s3u2/out_neg;  out: r14c10s21/in2; }
HDA2000_12_U(r15c15s11u3)  { in: .s3u5/out_neg;  out: r14c10s21/in3; }
HDA2000_12_U(r15c15s11u4)  { in: .s3u6/out_neg;  out: r14c10s21/in4; }
HDA2000_12_U(r15c15s11u5)  { in: .s3u7/out_neg;  out: r14c10s21/in5; }
HDA2000_12_U(r15c15s11u6)  { in: .s3u8/out_neg;  out: r14c10s21/in6; }
HDA2000_12_U(r15c15s11u7)  { in: .s7u1/out_neg;  out: r14c10s21/in7; }
HDA2000_12_U(r15c15s11u8)  { in: .s7u2/out_neg;  out: r14c10s21/in8; }
HDA2000_12_U(r15c15s11u9)  { in: .s7u3/out_neg;  out: r14c10s20/in1; }
HDA2000_12_U(r15c15s11u10) { in: .s7u4/out_neg;  out: r14c10s20/in2; }
HDA2000_12_U(r15c15s11u11) { in: .s7u5/out_neg;  out: r14c10s20/in3; }
HDA2000_12_U(r15c15s11u12) { in: .s7u6/out_neg;  out: r14c10s20/in4; }


TFA2000_6(r15c15s15)     { ; }

TFA2000_6_U(r15c15s15u1) { in: r13c3s12/t13; out_neg: .s23u1/in;  }
TFA2000_6_U(r15c15s15u2) { in: r13c3s12/t14; out_neg: .s23u2/in;  }
TFA2000_6_U(r15c15s15u3) { in: r13c3s12/t15; out_neg: .s23u3/in;  }
TFA2000_6_U(r15c15s15u4) { in: r13c3s12/t16; out_neg: .s23u4/in;  }
TFA2000_6_U(r15c15s15u5) { BROKEN; }
TFA2000_6_U(r15c15s15u6) { BROKEN; }

HDA2000_12(r15c15s23)      { ; }

HDA2000_12_U(r15c15s23u1)  { in: .s15u1/out_neg;  out: r14c10s20/in5; }
HDA2000_12_U(r15c15s23u2)  { in: .s15u2/out_neg;  out: r14c10s20/in6; }
HDA2000_12_U(r15c15s23u3)  { in: .s15u3/out_neg;  out: r14c10s20/in7; }
HDA2000_12_U(r15c15s23u4)  { in: .s15u4/out_neg;  out: r14c10s20/in8; }
HDA2000_12_U(r15c15s23u5)  { /*in: .c13s17u1/out_neg;*/ ; } // out: r14c10s18/in1; }
HDA2000_12_U(r15c15s23u6)  { /*in: .c13s17u2/out_neg;*/ ; } // out: r14c10s18/in2; }
HDA2000_12_U(r15c15s23u7)  { /*in: .c13s17u3/out_neg;*/ ; } // out: r14c10s18/in3; }
HDA2000_12_U(r15c15s23u8)  { /*in: .c13s17u4/out_neg;*/ ; } // out: r14c10s18/in4; }
HDA2000_12_U(r15c15s23u9)  { /*in: .c13s17u5/out_neg;*/ ; } // out: r14c10s18/in5; }
HDA2000_12_U(r15c15s23u10) { /*in: .c13s17u6/out_neg;*/ ; } // out: r14c10s18/in6; }
HDA2000_12_U(r15c15s23u11) { /*in: .c13s17u7/out_neg;*/ ; } // out: r14c10s18/in7; }
HDA2000_12_U(r15c15s23u12) { /*in: .c13s17u8/out_neg;*/ ; } // out: r14c10s18/in8; }




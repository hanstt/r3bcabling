/*
 * Crate 14.  Gamma amplifiers.  
 */

NIM_CRATE(r15c14)
{
  ;
}

TFA2000_6(r15c14s3)     { ; }

TFA2000_6_U(r15c14s3u1) { in: r13c3s16/t9;   out_neg: .s11u1/in;  }
TFA2000_6_U(r15c14s3u2) { in: r13c3s16/t10;  out_neg: .s11u2/in;  }
TFA2000_6_U(r15c14s3u3) { in: r13c3s16/t11;  out_neg: .s11u3/in;  }
TFA2000_6_U(r15c14s3u4) { in: r13c3s16/t12;  out_neg: .s11u4/in;  }
TFA2000_6_U(r15c14s3u5) { in: r13c3s16/t13;  out_neg: .s11u5/in;  }
TFA2000_6_U(r15c14s3u6) { in: r13c3s16/t14;  out_neg: .s11u6/in;  }

TFA2000_6(r15c14s7)     { ; }

TFA2000_6_U(r15c14s7u1) { in: r13c3s16/t15;  out_neg: .s11u7/in;  }
TFA2000_6_U(r15c14s7u2) { in: r13c3s16/t16;  out_neg: .s11u8/in;  }
TFA2000_6_U(r15c14s7u3) { in: r13c3s13/t1;   out_neg: .s11u9/in;  }
TFA2000_6_U(r15c14s7u4) { BROKEN; } // replaced by r15c13s13u5
TFA2000_6_U(r15c14s7u5) { in: r13c3s13/t3;   out_neg: .s11u11/in; }
TFA2000_6_U(r15c14s7u6) { in: r13c3s13/t4;   out_neg: .s11u12/in; }

HDA2000_12(r15c14s11) { ; }

HDA2000_12_U(r15c14s11u1)  { in: .s3u1/out_neg;  out: r14c6s10/in1; }
HDA2000_12_U(r15c14s11u2)  { in: .s3u2/out_neg;  out: r14c6s10/in2; }
HDA2000_12_U(r15c14s11u3)  { in: .s3u3/out_neg;  out: r14c6s10/in3; }
HDA2000_12_U(r15c14s11u4)  { in: .s3u4/out_neg;  out: r14c6s10/in4; }
HDA2000_12_U(r15c14s11u5)  { in: .s3u5/out_neg;  out: r14c6s10/in5; }
HDA2000_12_U(r15c14s11u6)  { in: .s3u6/out_neg;  out: r14c6s10/in6; }
HDA2000_12_U(r15c14s11u7)  { in: .s7u1/out_neg;  out: r14c6s10/in7; }
HDA2000_12_U(r15c14s11u8)  { in: .s7u2/out_neg;  out: r14c6s10/in8; }
HDA2000_12_U(r15c14s11u9)  { in: .s7u3/out_neg;  out: r14c8s5/in1; }
HDA2000_12_U(r15c14s11u10) { in:.c13s13u5/out_neg;out:r14c8s5/in2; } 
HDA2000_12_U(r15c14s11u11) { in: .s7u5/out_neg;  out: r14c8s5/in3; }
HDA2000_12_U(r15c14s11u12) { in: .s7u6/out_neg;  out: r14c8s5/in4; }


TFA2000_6(r15c14s15)     { ; }

TFA2000_6_U(r15c14s15u1) { in: r13c3s13/t5;   out_neg: .s23u1/in;  }
TFA2000_6_U(r15c14s15u2) { in: r13c3s13/t6;   out_neg: .s23u2/in;  }
TFA2000_6_U(r15c14s15u3) { in: r13c3s13/t7;   out_neg: .s23u3/in;  }
TFA2000_6_U(r15c14s15u4) { in: r13c3s13/t8;   out_neg: .s23u4/in;  }
TFA2000_6_U(r15c14s15u5) { in: r13c3s13/t9;   out_neg: .s23u5/in;  }
TFA2000_6_U(r15c14s15u6) { in: r13c3s13/t10;  out_neg: .s23u6/in;  }

TFA2000_6(r15c14s19)     { ; }

TFA2000_6_U(r15c14s19u1) { in: r13c3s13/t11;  out_neg: .s23u7/in;  }
TFA2000_6_U(r15c14s19u2) { in: r13c3s13/t12;  out_neg: .s23u8/in;  }
TFA2000_6_U(r15c14s19u3) { in: r13c3s13/t13;  out_neg: .s23u9/in;  }
TFA2000_6_U(r15c14s19u4) { in: r13c3s13/t14;  out_neg: .s23u10/in; }
TFA2000_6_U(r15c14s19u5) { in: r13c3s13/t15;  out_neg: .s23u11/in; }
TFA2000_6_U(r15c14s19u6) { in: r13c3s13/t16;  out_neg: .s23u12/in; }

HDA2000_12(r15c14s23) { ; }

HDA2000_12_U(r15c14s23u1)  { in: .s15u1/out_neg;  out: r14c8s5/in5; }
HDA2000_12_U(r15c14s23u2)  { in: .s15u2/out_neg;  out: r14c8s5/in6; }
HDA2000_12_U(r15c14s23u3)  { in: .s15u3/out_neg;  out: r14c8s5/in7; }
HDA2000_12_U(r15c14s23u4)  { in: .s15u4/out_neg;  out: r14c8s5/in8; }
HDA2000_12_U(r15c14s23u5)  { in: .s15u5/out_neg; } // out: r14c10s17/in1; }
HDA2000_12_U(r15c14s23u6)  { in: .s15u6/out_neg; } // out: r14c10s17/in2; }
HDA2000_12_U(r15c14s23u7)  { in: .s19u1/out_neg; } // out: r14c10s17/in3; }
HDA2000_12_U(r15c14s23u8)  { in: .s19u2/out_neg; } // out: r14c10s17/in4; }
HDA2000_12_U(r15c14s23u9)  { in: .s19u3/out_neg; } // out: r14c10s17/in5; }
HDA2000_12_U(r15c14s23u10) { in: .s19u4/out_neg; } // out: r14c10s17/in6; }
HDA2000_12_U(r15c14s23u11) { in: .s19u5/out_neg; } // out: r14c10s17/in7; }
HDA2000_12_U(r15c14s23u12) { in: .s19u6/out_neg; } // out: r14c10s17/in8; }





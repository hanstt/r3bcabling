/*
 * Crate 18.  Gamma amplifiers.  
 * (soft gamma -> hard gamma)
 * Photon torpedoes.
 */

NIM_CRATE(r15c18)
{
  ;
}

TFA2000_6(r15c18s3)     { ; }

TFA2000_6_U(r15c18s3u1) { in: r13c3s12/t1;   out_neg: .s11u1/in;  }
TFA2000_6_U(r15c18s3u2) { in: r13c3s12/t2;   out_neg: .s11u2/in;  }
TFA2000_6_U(r15c18s3u3) { in: r13c3s12/t3;   out_neg: .s11u3/in;  }
TFA2000_6_U(r15c18s3u4) { in: r13c3s12/t4;   out_neg: .s11u4/in;  }
TFA2000_6_U(r15c18s3u5) { in: r13c3s18/t16;  out_neg: .s11u11/in; }
TFA2000_6_U(r15c18s3u6) { in: r13c3s12/t6;   out_neg: .s11u6/in;  }

TFA2000_6(r15c18s7)     { ; }

TFA2000_6_U(r15c18s7u1) { in: r13c3s12/t7;   out_neg: .s11u7/in;  }
TFA2000_6_U(r15c18s7u2) { in: r13c3s12/t8;   out_neg: .s11u8/in;  }
#if USING_XB
TFA2000_6_U(r15c18s7u3) { in: r13c3s12/t5;    out_neg: .s11u5/in;  }
TFA2000_6_U(r15c18s7u4) { in: r13c3s18/t12;   out_neg: .s11u9/in; }
TFA2000_6_U(r15c18s7u5) { /*in: r13c3s19/t2 ;*/   out_neg: .c17s11u2/in; }
TFA2000_6_U(r15c18s7u6) { in: r13c3s18/t14;   out_neg: .s11u10/in; }
#endif

HDA2000_12(r15c18s11)      { ; }

HDA2000_12_U(r15c18s11u1)  { in: .s3u1/out_neg;  out: r14c10s19/in1; }
HDA2000_12_U(r15c18s11u2)  { in: .s3u2/out_neg;  out: r14c10s19/in2; }
HDA2000_12_U(r15c18s11u3)  { in: .s3u3/out_neg;  out: r14c10s19/in3; }
HDA2000_12_U(r15c18s11u4)  { in: .s3u4/out_neg;  out: r14c10s19/in4; }
HDA2000_12_U(r15c18s11u5)  { in: .s7u3/out_neg;  out: r14c10s19/in5; }
HDA2000_12_U(r15c18s11u6)  { in: .s3u6/out_neg;  out: r14c10s19/in6; }
HDA2000_12_U(r15c18s11u7)  { in: .s7u1/out_neg;  out: r14c10s19/in7; }
HDA2000_12_U(r15c18s11u8)  { in: .s7u2/out_neg;  out: r14c10s19/in8; }
#if USING_XB
HDA2000_12_U(r15c18s11u9)  { in: .s7u4/out_neg;  out: r14c10s23/in1; }
HDA2000_12_U(r15c18s11u10) { in: .s7u6/out_neg;  out: r14c10s23/in2; }
HDA2000_12_U(r15c18s11u11) { in: .s3u5/out_neg;  out: r14c10s23/in3; }
HDA2000_12_U(r15c18s11u12) { in: ;  out: r14c10s23/in4; }
#endif

TFA2000_6(r15c18s15)     { ; }

#if USING_CV
TFA2000_6_U(r15c18s15u1) { in: r13c3s18/t1; out_neg: r14c10s22/in1;  }
TFA2000_6_U(r15c18s15u2) { in: r13c3s18/t3; out_neg: r14c10s22/in3;  }
TFA2000_6_U(r15c18s15u3) { in: r13c3s18/t4; out_neg: r14c10s22/in2;  }
TFA2000_6_U(r15c18s15u4) { in: r13c3s18/t5; out_neg: r14c10s22/in5;  }
TFA2000_6_U(r15c18s15u5) { in: r13c3s18/t6; out_neg: r14c10s22/in6;  }
TFA2000_6_U(r15c18s15u6) { in: r13c3s18/t7; out_neg: r14c10s22/in7;  }
#endif
#if USING_XB
TFA2000_6_U(r15c18s15u1) { in: r13c3s18/t1; out_neg: .s23u1/in;  }
TFA2000_6_U(r15c18s15u2) { in: r13c3s18/t9; out_neg: .s23u2/in;  }
TFA2000_6_U(r15c18s15u3) { in: r13c3s18/t3; out_neg: .s23u3/in;  }
TFA2000_6_U(r15c18s15u4) { in: r13c3s18/t4; out_neg: .s23u4/in;  }
TFA2000_6_U(r15c18s15u5) { in: r13c3s18/t5; out_neg: .s23u5/in;  }
TFA2000_6_U(r15c18s15u6) { in: r13c3s18/t6; out_neg: .s23u6/in;  }
#endif

TFA2000_6(r15c18s19)     { ; }

#if USING_CV
TFA2000_6_U(r15c18s19u1) { in: r13c3s18/t8;  out_neg: r14c10s22/in8; }
TFA2000_6_U(r15c18s19u2) { in: r13c3s18/t9;  out_neg: r14c10s23/in1; }
TFA2000_6_U(r15c18s19u3) { in: r13c3s18/t10; out_neg: r14c10s23/in2; }
TFA2000_6_U(r15c18s19u4) { in: r13c3s18/t11; out_neg: r14c10s23/in3; }
TFA2000_6_U(r15c18s19u5) { in: r13c3s18/t13; out_neg: r14c10s23/in5; }
TFA2000_6_U(r15c18s19u6) { in: r13c3s18/t15; out_neg: r14c10s23/in7; }
#endif
#if USING_XB
TFA2000_6_U(r15c18s19u1) { in: r13c3s18/t7 ; out_neg: .s23u7/in;  }
TFA2000_6_U(r15c18s19u2) { in: r13c3s18/t8 ; out_neg: .s23u8/in;  }
TFA2000_6_U(r15c18s19u3) { in: r13c3s18/t10; out_neg: .s23u9/in;  }
TFA2000_6_U(r15c18s19u4) { in: r13c3s18/t11; out_neg: .s23u10/in; }
TFA2000_6_U(r15c18s19u5) { in: r13c3s18/t15; out_neg: .s23u11/in; }
TFA2000_6_U(r15c18s19u6) { in: r13c3s18/t13; out_neg: .s23u12/in; }
#endif

HDA2000_12(r15c18s23)      { ; }

#if USING_XB
HDA2000_12_U(r15c18s23u1)  { in: .s15u1/out_neg;  out: r14c10s23/in5; }
HDA2000_12_U(r15c18s23u2)  { in: .s15u2/out_neg;  out: r14c10s23/in6; }
HDA2000_12_U(r15c18s23u3)  { in: .s15u3/out_neg;  out: r14c10s23/in7; }
HDA2000_12_U(r15c18s23u4)  { in: .s15u4/out_neg;  out: r14c10s23/in8; }
HDA2000_12_U(r15c18s23u5)  { in: .s15u5/out_neg;  out: r14c10s22/in1; }
HDA2000_12_U(r15c18s23u6)  { in: .s15u6/out_neg;  out: r14c10s22/in2; }
HDA2000_12_U(r15c18s23u7)  { in: .s19u1/out_neg;  out: r14c10s22/in3; }
HDA2000_12_U(r15c18s23u8)  { in: .s19u2/out_neg;  out: r14c10s22/in4; }
HDA2000_12_U(r15c18s23u9)  { in: .s19u3/out_neg;  out: r14c10s22/in5; }
HDA2000_12_U(r15c18s23u10) { in: .s19u4/out_neg;  out: r14c10s22/in6; }
HDA2000_12_U(r15c18s23u11) { in: .s19u5/out_neg;  out: r14c10s22/in7; }
HDA2000_12_U(r15c18s23u12) { in: .s19u6/out_neg;  out: r14c10s22/in8; }
#endif





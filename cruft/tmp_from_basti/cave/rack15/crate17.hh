/*
 * Crate 17.  Gamma amplifiers.  
 */

NIM_CRATE(r15c17)
{
  ;
}

TFA2000_6(r15c17s3)     { ; }

TFA2000_6_U(r15c17s3u1) { /*in: r13c3s19/t1;*/   out_neg: .s11u1/in;  }
TFA2000_6_U(r15c17s3u2) { BROKEN /* DEAD */;  }
TFA2000_6_U(r15c17s3u3) { /*in: r13c3s19/t3;*/   out_neg: .s11u3/in;  }
TFA2000_6_U(r15c17s3u4) { /*in: r13c3s19/t4;*/   out_neg: .s11u4/in;  }
TFA2000_6_U(r15c17s3u5) { /*in: r13c3s19/t5;*/   out_neg: .s11u5/in;  }
TFA2000_6_U(r15c17s3u6) { /*in: r13c3s19/t6;*/   out_neg: .s11u6/in;  }

TFA2000_6(r15c17s7)     { ; }

TFA2000_6_U(r15c17s7u1) { /*in: r13c3s19/t7;*/   out_neg: .s11u7/in;  }
TFA2000_6_U(r15c17s7u2) { /*in: r13c3s19/t8;*/   out_neg: .s11u8/in;  }
TFA2000_6_U(r15c17s7u3) { /*in: r13c3s19/t9;*/   out_neg: .s11u9/in;  }
TFA2000_6_U(r15c17s7u4) { /*in: r13c3s19/t10;*/  out_neg: .s11u10/in; }
TFA2000_6_U(r15c17s7u5) { /*in: r13c3s19/t11;*/  out_neg: .s11u11/in; }
TFA2000_6_U(r15c17s7u6) { /*in: r13c3s19/t12;*/  out_neg: .s11u12/in; }

HDA2000_12(r15c17s11) { ; }

HDA2000_12_U(r15c17s11u1)  { in: .s3u1/out_neg; } // out: r14c10s13/in1; }
HDA2000_12_U(r15c17s11u2)  { in: .c18s7u5/out_neg; } // out: r14c10s13/in2; }
HDA2000_12_U(r15c17s11u3)  { in: .s3u3/out_neg; } // out: r14c10s13/in3; }
HDA2000_12_U(r15c17s11u4)  { in: .s3u4/out_neg; } // out: r14c10s13/in4; }
HDA2000_12_U(r15c17s11u5)  { in: .s3u5/out_neg; } // out: r14c10s13/in5; }
HDA2000_12_U(r15c17s11u6)  { in: .s3u6/out_neg; } // out: r14c10s13/in6; }
HDA2000_12_U(r15c17s11u7)  { in: .s7u1/out_neg; } // out: r14c10s13/in7; }
HDA2000_12_U(r15c17s11u8)  { in: .s7u2/out_neg; } // out: r14c10s13/in8; }
HDA2000_12_U(r15c17s11u9)  { in: .s7u3/out_neg; } // out: r14c10s15/in1; }
HDA2000_12_U(r15c17s11u10) { in: .s7u4/out_neg; } // out: r14c10s15/in2; }
HDA2000_12_U(r15c17s11u11) { in: .s7u5/out_neg; } // out: r14c10s15/in3; }
HDA2000_12_U(r15c17s11u12) { in: .s7u6/out_neg; } // out: r14c10s15/in4; }


// SOME OUT: CL @ 12/08/2010 --> NTF IN NOW....

TFA2000_6(r15c17s15)     { ; }

TFA2000_6_U(r15c17s15u1) { /*in: r13c3s19/t13;*/  out_neg: .s23u1/in;  }
TFA2000_6_U(r15c17s15u2) { /*in: r13c3s19/t14;*/  out_neg: .s23u2/in;  }
TFA2000_6_U(r15c17s15u3) { /*in: r13c3s19/t15;*/  out_neg: .s23u3/in;  }
TFA2000_6_U(r15c17s15u4) { /*in: r13c3s19/t16;*/  out_neg: .s23u4/in;  }
TFA2000_6_U(r15c17s15u5) { /*in: r13c3s20/t9;*/   out_neg: .s23u5/in;  }
TFA2000_6_U(r15c17s15u6) { /*in: r13c3s20/t10;*/  out_neg: .s23u6/in;  }

TFA2000_6(r15c17s19)     { ; }

TFA2000_6_U(r15c17s19u1) { /*in: r13c3s20/t11;*/  out_neg: .s23u7/in;  }
TFA2000_6_U(r15c17s19u2) { /*in: r13c3s20/t12;*/  out_neg: .s23u8/in;  }
TFA2000_6_U(r15c17s19u3) { /*in: r13c3s20/t13;*/  out_neg: .s23u9/in;  }
TFA2000_6_U(r15c17s19u4) { /*in: r13c3s20/t14;*/  out_neg: .s23u10/in; }
TFA2000_6_U(r15c17s19u5) { /*in: r13c3s20/t15;*/  out_neg: .s23u11/in; }
TFA2000_6_U(r15c17s19u6) { /*in: r13c3s20/t16;*/  out_neg: .s23u12/in; }

HDA2000_12(r15c17s23) { ; }

HDA2000_12_U(r15c17s23u1)  { in: .s15u1/out_neg; } // out: r14c10s15/in5; }
HDA2000_12_U(r15c17s23u2)  { in: .s15u2/out_neg; } // out: r14c10s15/in6; }
HDA2000_12_U(r15c17s23u3)  { in: .s15u3/out_neg; } // out: r14c10s15/in7; }
HDA2000_12_U(r15c17s23u4)  { in: .s15u4/out_neg; } // out: r14c10s15/in8; }
HDA2000_12_U(r15c17s23u5)  { in: .s15u5/out_neg;  out: r14c10s16/in1; }
HDA2000_12_U(r15c17s23u6)  { in: .s15u6/out_neg;  out: r14c10s16/in2; }
HDA2000_12_U(r15c17s23u7)  { in: .s19u1/out_neg;  out: r14c10s16/in3; }
HDA2000_12_U(r15c17s23u8)  { in: .s19u2/out_neg;  out: r14c10s16/in4; }
HDA2000_12_U(r15c17s23u9)  { in: .s19u3/out_neg;  out: r14c10s16/in5; }
HDA2000_12_U(r15c17s23u10) { in: .s19u4/out_neg;  out: r14c10s16/in6; }
HDA2000_12_U(r15c17s23u11) { in: .s19u5/out_neg;  out: r14c10s16/in7; }
HDA2000_12_U(r15c17s23u12) { in: .s19u6/out_neg;  out: r14c10s16/in8; }





/*
 * Crate 13.  Gamma amplifiers.  
 */

NIM_CRATE(r15c13)
{
  ;
}

TFA2000_6(r15c13s1)     { ; }

TFA2000_6_U(r15c13s1u1) { in: r13c3s15/t1;   out_neg: .s9u1/in;  }
TFA2000_6_U(r15c13s1u2) { in: r13c3s15/t2;   out_neg: .s9u2/in;  }
TFA2000_6_U(r15c13s1u3) { in: r13c3s15/t3;   out_neg: .s9u3/in;  }
TFA2000_6_U(r15c13s1u4) { in: r13c3s15/t4;   out_neg: .s9u4/in;  }
TFA2000_6_U(r15c13s1u5) { in: r13c3s15/t5;   out_neg: .s9u5/in;  }
TFA2000_6_U(r15c13s1u6) { in: r13c3s15/t6;   out_neg: .s9u6/in;  }

TFA2000_6(r15c13s5)     { ; }

TFA2000_6_U(r15c13s5u1) { in: r13c3s15/t7;   out_neg: .s9u7/in;  }
TFA2000_6_U(r15c13s5u2) { in: r13c3s15/t8;   out_neg: .s9u8/in;  }
TFA2000_6_U(r15c13s5u3) { in: r13c3s15/t9;   out_neg: .s9u9/in;  }
TFA2000_6_U(r15c13s5u4) { in: r13c3s15/t10;  out_neg: .s9u10/in; }
TFA2000_6_U(r15c13s5u5) { in: r13c3s15/t11;  out_neg: .s9u11/in; }
TFA2000_6_U(r15c13s5u6) { in: r13c3s15/t12;  out_neg: .s9u12/in; }

HDA2000_12(r15c13s9) { ; }
//CHANGES DUE TO DTF INSERTED...CL @ 11/08/2010
HDA2000_12_U(r15c13s9u1)  { in: .s1u1/out_neg;  } //out: r14c6s8/in1; }
HDA2000_12_U(r15c13s9u2)  { in: .s1u2/out_neg;  } //out: r14c6s8/in2; }
HDA2000_12_U(r15c13s9u3)  { in: .s1u3/out_neg;  } //out: r14c6s8/in3; }
HDA2000_12_U(r15c13s9u4)  { in: .s1u4/out_neg;  } //out: r14c6s8/in4; }
HDA2000_12_U(r15c13s9u5)  { in: .s1u5/out_neg;  } //out: r14c6s8/in5; }
HDA2000_12_U(r15c13s9u6)  { in: .s1u6/out_neg;  } //out: r14c6s8/in6; }
HDA2000_12_U(r15c13s9u7)  { in: .s5u1/out_neg;  } //out: r14c6s8/in7; }
HDA2000_12_U(r15c13s9u8)  { in: .s5u2/out_neg;  } //out: r14c6s8/in8; }
HDA2000_12_U(r15c13s9u9)  { in: .s5u3/out_neg;  out: r14c6s9/in1; }
HDA2000_12_U(r15c13s9u10) { in: .s5u4/out_neg;  out: r14c6s9/in2; }
HDA2000_12_U(r15c13s9u11) { in: .s5u5/out_neg;  out: r14c6s9/in3; }
HDA2000_12_U(r15c13s9u12) { in: .s5u6/out_neg;  out: r14c6s9/in4; }


TFA2000_6(r15c13s13)     { ; }

TFA2000_6_U(r15c13s13u1) { in: r13c3s15/t13; out_neg: .s23u1/in;  }
TFA2000_6_U(r15c13s13u2) { in: r13c3s15/t14; out_neg: .s23u2/in;  }
TFA2000_6_U(r15c13s13u3) { in: r13c3s15/t15; out_neg: .s23u3/in;  }
TFA2000_6_U(r15c13s13u4) { in: r13c3s15/t16; out_neg: .s23u4/in;  }
TFA2000_6_U(r15c13s13u5) { in: r13c3s13/t2;  out_neg: .c14s11u10/in;  } // replaces r15c14s7u4
TFA2000_6_U(r15c13s13u6) { BROKEN; }

TFA2000_8(r15c13s17)	{ ; }

TFA2000_8_U(r15c13s17u1) { ; } //in: r13c3s21/t9;  out_neg: .c15s23u5/in;  }
TFA2000_8_U(r15c13s17u2) { ; } //in: r13c3s21/t10; out_neg: .c15s23u6/in;  }
TFA2000_8_U(r15c13s17u3) { ; } //in: r13c3s21/t11; out_neg: .c15s23u7/in;  }
TFA2000_8_U(r15c13s17u4) { ; } //in: r13c3s21/t12; out_neg: .c15s23u8/in;  }
TFA2000_8_U(r15c13s17u5) { ; } //in: r13c3s21/t13; out_neg: .c15s23u9/in;  }
TFA2000_8_U(r15c13s17u6) { ; } //in: r13c3s21/t14; out_neg: .c15s23u10/in;  }
TFA2000_8_U(r15c13s17u7) { ; } //in: r13c3s21/t15; out_neg: .c15s23u11/in;  }
TFA2000_8_U(r15c13s17u8) { ; } //in: r13c3s21/t16; out_neg: .c15s23u12/in;  }


HDA2000_12(r15c13s23) { ; }

HDA2000_12_U(r15c13s23u1)  { in: .s13u1/out_neg;  out: r14c6s9/in5; }
HDA2000_12_U(r15c13s23u2)  { in: .s13u2/out_neg;  out: r14c6s9/in6; }
HDA2000_12_U(r15c13s23u3)  { in: .s13u3/out_neg;  out: r14c6s9/in7; }
HDA2000_12_U(r15c13s23u4)  { in: .s13u4/out_neg;  out: r14c6s9/in8; }
HDA2000_12_U(r15c13s23u5)  { in: ;                out: ; }
HDA2000_12_U(r15c13s23u6)  { in: ;                out: ; }


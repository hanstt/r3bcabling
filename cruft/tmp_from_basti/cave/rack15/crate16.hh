/*
 * Crate 16.  Gamma amplifiers.  
 */

NIM_CRATE(r15c16)
{
  ;
}

TFA2000_6(r15c16s3)     { ; }

TFA2000_6_U(r15c16s3u1) { in: r13c3s14/t1;   out_neg: .s11u1/in;  }
TFA2000_6_U(r15c16s3u2) { in: r13c3s14/t2;   out_neg: .s11u2/in;  }
TFA2000_6_U(r15c16s3u3) { in: r13c3s14/t3;   out_neg: .s11u3/in;  }
TFA2000_6_U(r15c16s3u4) { in: r13c3s14/t4;   out_neg: .s11u4/in;  }
TFA2000_6_U(r15c16s3u5) { in: r13c3s14/t5;   out_neg: .s11u5/in;  }
TFA2000_6_U(r15c16s3u6) { in: r13c3s14/t6;   out_neg: .s11u6/in;  }

TFA2000_6(r15c16s7)     { ; }

TFA2000_6_U(r15c16s7u1) { in: r13c3s14/t7;   out_neg: .s11u7/in;  }
TFA2000_6_U(r15c16s7u2) { in: r13c3s14/t8;   out_neg: .s11u8/in;  }
TFA2000_6_U(r15c16s7u3) { in: r13c3s14/t9;   out_neg: .s11u9/in;  }
TFA2000_6_U(r15c16s7u4) { in: r13c3s14/t10;  out_neg: .s11u10/in; }
TFA2000_6_U(r15c16s7u5) { in: r13c3s14/t11;  out_neg: .s11u11/in; }
TFA2000_6_U(r15c16s7u6) { in: r13c3s14/t12;  out_neg: .s11u12/in; }

HDA2000_12(r15c16s11) { ; }

HDA2000_12_U(r15c16s11u1)  { in: .s3u1/out_neg;  out: r14c10s10/in1; }
HDA2000_12_U(r15c16s11u2)  { in: .s3u2/out_neg;  out: r14c10s10/in2; }
HDA2000_12_U(r15c16s11u3)  { in: .s3u3/out_neg;  out: r14c10s10/in3; }
HDA2000_12_U(r15c16s11u4)  { in: .s3u4/out_neg;  out: r14c10s10/in4; }
HDA2000_12_U(r15c16s11u5)  { in: .s3u5/out_neg;  out: r14c10s10/in5; }
HDA2000_12_U(r15c16s11u6)  { in: .s3u6/out_neg;  out: r14c10s10/in6; }
HDA2000_12_U(r15c16s11u7)  { in: .s7u1/out_neg;  out: r14c10s10/in7; }
HDA2000_12_U(r15c16s11u8)  { in: .s7u2/out_neg;  out: r14c10s10/in8; }
HDA2000_12_U(r15c16s11u9)  { in: .s7u3/out_neg;  out: r14c10s11/in1; }
HDA2000_12_U(r15c16s11u10) { in: .s7u4/out_neg;  out: r14c10s11/in2; }
HDA2000_12_U(r15c16s11u11) { in: .s7u5/out_neg;  out: r14c10s11/in3; }
HDA2000_12_U(r15c16s11u12) { in: .s7u6/out_neg;  out: r14c10s11/in4; }


HDA2000_12(r15c16s15) { ; }

HDA2000_12_U(r15c16s15u1)  { in: .s19u1/out_neg;  out: r14c10s11/in5; }
HDA2000_12_U(r15c16s15u2)  { in: .s19u2/out_neg;  out: r14c10s11/in6; }
HDA2000_12_U(r15c16s15u3)  { in: .s19u3/out_neg;  out: r14c10s11/in7; }
HDA2000_12_U(r15c16s15u4)  { in: .s19u4/out_neg;  out: r14c10s11/in8; }
HDA2000_12_U(r15c16s15u5)  { in: .s19u5/out_neg;  out: r14c10s12/in1; }
HDA2000_12_U(r15c16s15u6)  { in: .s19u6/out_neg;  out: r14c10s12/in2; }
HDA2000_12_U(r15c16s15u7)  { in: .s23u1/out_neg;  out: r14c10s12/in3; }
HDA2000_12_U(r15c16s15u8)  { in: .s23u2/out_neg;  out: r14c10s12/in4; }
HDA2000_12_U(r15c16s15u9)  { in: .s23u3/out_neg;  out: r14c10s12/in5; }
HDA2000_12_U(r15c16s15u10) { in: .s23u4/out_neg;  out: r14c10s12/in6; }
HDA2000_12_U(r15c16s15u11) { in: .s23u5/out_neg;  out: r14c10s12/in7; }
HDA2000_12_U(r15c16s15u12) { in: .s23u6/out_neg;  out: r14c10s12/in8; }

TFA2000_6(r15c16s19)     { ; }

TFA2000_6_U(r15c16s19u1) { in: r13c3s14/t13;  out_neg: .s15u1/in;  }
TFA2000_6_U(r15c16s19u2) { in: r13c3s14/t14;  out_neg: .s15u2/in;  }
TFA2000_6_U(r15c16s19u3) { in: r13c3s14/t15;  out_neg: .s15u3/in;  }
TFA2000_6_U(r15c16s19u4) { in: r13c3s14/t16;  out_neg: .s15u4/in;  }
TFA2000_6_U(r15c16s19u5) { /*in: r13c3s20/t1;*/   out_neg: .s15u5/in;  }
TFA2000_6_U(r15c16s19u6) { /*in: r13c3s20/t2;*/   out_neg: .s15u6/in;  }

TFA2000_6(r15c16s23)     { ; }

TFA2000_6_U(r15c16s23u1) { /*in: r13c3s20/t3;*/   out_neg: .s15u7/in;  }
TFA2000_6_U(r15c16s23u2) { /*in: r13c3s20/t4;*/   out_neg: .s15u8/in;  }
TFA2000_6_U(r15c16s23u3) { /*in: r13c3s20/t5;*/   out_neg: .s15u9/in;  }
TFA2000_6_U(r15c16s23u4) { /*in: r13c3s20/t6;*/   out_neg: .s15u10/in; }
TFA2000_6_U(r15c16s23u5) { /*in: r13c3s20/t7;*/   out_neg: .s15u11/in; }
TFA2000_6_U(r15c16s23u6) { /*in: r13c3s20/t8;*/   out_neg: .s15u12/in; }





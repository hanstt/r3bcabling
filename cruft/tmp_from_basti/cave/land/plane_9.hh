// Do not edit, this file is auto-generated
DETECTOR_LAND(rNP9c01s1) { LABEL("N09_01_1"); signal: rNP9c21/in1; hv: rNP9c31/out1; }
DETECTOR_LAND(rNP9c02s1) { LABEL("N09_02_1"); signal: rNP9c21/in3; hv: rNP9c31/out2; }
DETECTOR_LAND(rNP9c03s1) { LABEL("N09_03_1"); signal: rNP9c21/in5; hv: rNP9c31/out3; }
DETECTOR_LAND(rNP9c04s1) { LABEL("N09_04_1"); signal: rNP9c21/in7; hv: rNP9c31/out4; }
DETECTOR_LAND(rNP9c05s1) { LABEL("N09_05_1"); signal: rNP9c22/in1; hv: rNP9c31/out5; }
DETECTOR_LAND(rNP9c06s1) { LABEL("N09_06_1"); signal: rNP9c22/in3; hv: rNP9c31/out6; }
DETECTOR_LAND(rNP9c07s1) { LABEL("N09_07_1"); signal: rNP9c22/in5; hv: rNP9c31/out7; }
DETECTOR_LAND(rNP9c08s1) { LABEL("N09_08_1"); signal: rNP9c22/in7; hv: rNP9c31/out8; }
DETECTOR_LAND(rNP9c09s1) { LABEL("N09_09_1"); signal: rNP9c21/in2; hv: rNP9c31/out9; }
DETECTOR_LAND(rNP9c10s1) { LABEL("N09_10_1"); signal: rNP9c21/in4; hv: rNP9c31/out10; }
DETECTOR_LAND(rNP9c11s1) { LABEL("N09_11_1"); signal: rNP9c21/in6; hv: rNP9c31/out11; }
DETECTOR_LAND(rNP9c12s1) { LABEL("N09_12_1"); signal: rNP9c21/in8; hv: rNP9c31/out12; }
DETECTOR_LAND(rNP9c13s1) { LABEL("N09_13_1"); signal: rNP9c22/in2; hv: rNP9c31/out13; }
DETECTOR_LAND(rNP9c14s1) { LABEL("N09_14_1"); signal: rNP9c22/in4; hv: rNP9c31/out14; }
//DETECTOR_LAND(rNP9c15s1) { LABEL("N09_15_1"); signal: rNP9c25/in8; hv: rNP9c31/out15; }
DETECTOR_LAND(rNP9c15s1) { LABEL("N09_15_1"); signal: rNP9c25/in5; hv: rNP9c31/out15; }
//DETECTOR_LAND(rNP9c16s1) { LABEL("N09_16_1"); signal: rNP9c25/in6; hv: rNP9c31/out16; }
DETECTOR_LAND(rNP9c16s1) { LABEL("N09_16_1"); signal: rNP9c25/in7; hv: rNP9c31/out16; }
DETECTOR_LAND(rNP9c17s1) { LABEL("N09_17_1"); signal: rNP9c22/in6; hv: rNP9c32/out1; }
DETECTOR_LAND(rNP9c18s1) { LABEL("N09_18_1"); signal: rNP9c22/in8; hv: rNP9c32/out2; }
DETECTOR_LAND(rNP9c19s1) { LABEL("N09_19_1"); signal: rNP9c26/in8; hv: rNP9c32/out3; }
DETECTOR_LAND(rNP9c20s1) { LABEL("N09_20_1"); signal: rNP9c26/in6; hv: rNP9c32/out4; }
//DETECTOR_LAND(rNP9c01s2) { LABEL("N09_01_2"); signal: rNP9c25/in7; hv: rNP9c33/out1; }
DETECTOR_LAND(rNP9c01s2) { LABEL("N09_01_2"); signal: rNP9c25/in6; hv: rNP9c33/out1; }
//DETECTOR_LAND(rNP9c02s2) { LABEL("N09_02_2"); signal: rNP9c25/in5; hv: rNP9c33/out2; }
DETECTOR_LAND(rNP9c02s2) { LABEL("N09_02_2"); signal: rNP9c25/in8; hv: rNP9c33/out2; }
DETECTOR_LAND(rNP9c03s2) { LABEL("N09_03_2"); signal: rNP9c23/in1; hv: rNP9c33/out3; }
DETECTOR_LAND(rNP9c04s2) { LABEL("N09_04_2"); signal: rNP9c23/in3; hv: rNP9c33/out4; }
DETECTOR_LAND(rNP9c05s2) { LABEL("N09_05_2"); signal: rNP9c26/in5; hv: rNP9c33/out5; }
DETECTOR_LAND(rNP9c06s2) { LABEL("N09_06_2"); signal: rNP9c26/in7; hv: rNP9c33/out6; }
DETECTOR_LAND(rNP9c07s2) { LABEL("N09_07_2"); signal: rNP9c23/in5; hv: rNP9c33/out7; }
DETECTOR_LAND(rNP9c08s2) { LABEL("N09_08_2"); signal: rNP9c23/in7; hv: rNP9c33/out8; }
DETECTOR_LAND(rNP9c09s2) { LABEL("N09_09_2"); signal: rNP9c24/in1; hv: rNP9c33/out9; }
DETECTOR_LAND(rNP9c10s2) { LABEL("N09_10_2"); signal: rNP9c24/in3; hv: rNP9c33/out10; }
DETECTOR_LAND(rNP9c11s2) { LABEL("N09_11_2"); signal: rNP9c24/in5; hv: rNP9c33/out11; }
DETECTOR_LAND(rNP9c12s2) { LABEL("N09_12_2"); signal: rNP9c24/in7; hv: rNP9c33/out12; }
DETECTOR_LAND(rNP9c13s2) { LABEL("N09_13_2"); signal: rNP9c23/in2; hv: rNP9c33/out13; }
DETECTOR_LAND(rNP9c14s2) { LABEL("N09_14_2"); signal: rNP9c23/in4; hv: rNP9c33/out14; }
DETECTOR_LAND(rNP9c15s2) { LABEL("N09_15_2"); signal: rNP9c23/in6; hv: rNP9c33/out15; }
DETECTOR_LAND(rNP9c16s2) { LABEL("N09_16_2"); signal: rNP9c23/in8; hv: rNP9c33/out16; }
DETECTOR_LAND(rNP9c17s2) { LABEL("N09_17_2"); signal: rNP9c24/in2; hv: rNP9c32/out5; }
DETECTOR_LAND(rNP9c18s2) { LABEL("N09_18_2"); signal: rNP9c24/in4; hv: rNP9c32/out6; }
DETECTOR_LAND(rNP9c19s2) { LABEL("N09_19_2"); signal: rNP9c24/in6; hv: rNP9c32/out7; }
DETECTOR_LAND(rNP9c20s2) { LABEL("N09_20_2"); signal: rNP9c24/in8; hv: rNP9c32/out8; }
JOINER_8(rNP9c21)
{
 in1: rNP9c01s1/signal;
 in2: rNP9c09s1/signal;
 in3: rNP9c02s1/signal;
 in4: rNP9c10s1/signal;
 in5: rNP9c03s1/signal;
 in6: rNP9c11s1/signal;
 in7: rNP9c04s1/signal;
 in8: rNP9c12s1/signal;
 out1_8: r13c3s7/in1_8;
}
JOINER_8(rNP9c22)
{
 in1: rNP9c05s1/signal;
 in2: rNP9c13s1/signal;
 in3: rNP9c06s1/signal;
 in4: rNP9c14s1/signal;
 in5: rNP9c07s1/signal;
 in6: rNP9c17s1/signal;
 in7: rNP9c08s1/signal;
 in8: rNP9c18s1/signal;
 out1_8: r13c3s7/in9_16;
}
JOINER_8(rNP9c23)
{
 in1: rNP9c03s2/signal;
 in2: rNP9c13s2/signal;
 in3: rNP9c04s2/signal;
 in4: rNP9c14s2/signal;
 in5: rNP9c07s2/signal;
 in6: rNP9c15s2/signal;
 in7: rNP9c08s2/signal;
 in8: rNP9c16s2/signal;
 out1_8: r13c3s8/in1_8;
}
JOINER_8(rNP9c24)
{
 in1: rNP9c09s2/signal;
 in2: rNP9c17s2/signal;
 in3: rNP9c10s2/signal;
 in4: rNP9c18s2/signal;
 in5: rNP9c11s2/signal;
 in6: rNP9c19s2/signal;
 in7: rNP9c12s2/signal;
 in8: rNP9c20s2/signal;
 out1_8: r13c3s8/in9_16;
}
JOINER_8(rNP9c25)
{
 in5: rNP9c15s1/signal;
 in6: rNP9c01s2/signal;
 in7: rNP9c16s1/signal;
 in8: rNP9c02s2/signal;
 // out5_8: r13c3s9/in5_8;
 //out5_8: r13c3s10/in5_8; // using an cable from plane 10
out5_8: r13c3s10/in1_4; // DR: wild guess... (not really!)
}
JOINER_8(rNP9c26)
{
 in8: rNP9c19s1/signal;
 in7:  rNP9c06s2/signal;
 in6: rNP9c20s1/signal;
 in5:  rNP9c05s2/signal;
 out5_8: r13c3s9/in13_16;
}
HV_CONN_16(rNP9c31)
{
 in1_16: r16c2s4/out0_15;
 out1: rNP9c01s1/hv;
 out2: rNP9c02s1/hv;
 out3: rNP9c03s1/hv;
 out4: rNP9c04s1/hv;
 out5: rNP9c05s1/hv;
 out6: rNP9c06s1/hv;
 out7: rNP9c07s1/hv;
 out8: rNP9c08s1/hv;
 out9: rNP9c09s1/hv;
 out10: rNP9c10s1/hv;
 out11: rNP9c11s1/hv;
 out12: rNP9c12s1/hv;
 out13: rNP9c13s1/hv;
 out14: rNP9c14s1/hv;
 out15: rNP9c15s1/hv;
 out16: rNP9c16s1/hv;
}
HV_CONN_16(rNP9c32)
{
 in1_8: r16c2s6/out0_7;
 out1: rNP9c17s1/hv;
 out2: rNP9c18s1/hv;
 out3: rNP9c19s1/hv;
 out4: rNP9c20s1/hv;
 out5: rNP9c17s2/hv;
 out6: rNP9c18s2/hv;
 out7: rNP9c19s2/hv;
 out8: rNP9c20s2/hv;
}
HV_CONN_16(rNP9c33)
{
 in1_16: r16c2s5/out0_15;
 out1: rNP9c01s2/hv;
 out2: rNP9c02s2/hv;
 out3: rNP9c03s2/hv;
 out4: rNP9c04s2/hv;
 out5: rNP9c05s2/hv;
 out6: rNP9c06s2/hv;
 out7: rNP9c07s2/hv;
 out8: rNP9c08s2/hv;
 out9: rNP9c09s2/hv;
 out10: rNP9c10s2/hv;
 out11: rNP9c11s2/hv;
 out12: rNP9c12s2/hv;
 out13: rNP9c13s2/hv;
 out14: rNP9c14s2/hv;
 out15: rNP9c15s2/hv;
 out16: rNP9c16s2/hv;
}

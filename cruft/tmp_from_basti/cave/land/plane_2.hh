// Do not edit, this file is auto-generated
DETECTOR_LAND(rNP2c01s1) { LABEL("N02_01_1"); signal: rNP2c21/in1; hv: rNP2c31/out1; }
DETECTOR_LAND(rNP2c02s1) { LABEL("N02_02_1"); signal: rNP2c21/in3; hv: rNP2c31/out2; }
DETECTOR_LAND(rNP2c03s1) { LABEL("N02_03_1"); signal: rNP2c21/in5; hv: rNP2c31/out3; }
DETECTOR_LAND(rNP2c04s1) { LABEL("N02_04_1"); signal: rNP2c21/in7; hv: rNP2c31/out4; }
DETECTOR_LAND(rNP2c05s1) { LABEL("N02_05_1"); signal: rNP2c22/in1; hv: rNP2c31/out5; }
DETECTOR_LAND(rNP2c06s1) { LABEL("N02_06_1"); signal: rNP2c22/in3; hv: rNP2c31/out6; }
DETECTOR_LAND(rNP2c07s1) { LABEL("N02_07_1"); signal: rNP2c22/in5; hv: rNP2c31/out7; }
DETECTOR_LAND(rNP2c08s1) { LABEL("N02_08_1"); signal: rNP2c22/in7; hv: rNP2c31/out8; }
DETECTOR_LAND(rNP2c09s1) { LABEL("N02_09_1"); signal: rNP2c21/in2; hv: rNP2c31/out9; }
DETECTOR_LAND(rNP2c10s1) { LABEL("N02_10_1"); signal: rNP2c21/in4; hv: rNP2c31/out10; }
DETECTOR_LAND(rNP2c11s1) { LABEL("N02_11_1"); signal: rNP2c21/in6; hv: rNP2c31/out11; }
DETECTOR_LAND(rNP2c12s1) { LABEL("N02_12_1"); signal: rNP2c21/in8; hv: rNP2c31/out12; }
DETECTOR_LAND(rNP2c13s1) { LABEL("N02_13_1"); signal: rNP2c22/in2; hv: rNP2c31/out13; }
DETECTOR_LAND(rNP2c14s1) { LABEL("N02_14_1"); signal: rNP2c22/in4; hv: rNP2c31/out14; }
DETECTOR_LAND(rNP2c15s1) { LABEL("N02_15_1"); signal: rNP2c25/in4; hv: rNP2c31/out15; }
DETECTOR_LAND(rNP2c16s1) { LABEL("N02_16_1"); signal: rNP2c25/in2; hv: rNP2c31/out16; }
DETECTOR_LAND(rNP2c17s1) { LABEL("N02_17_1"); signal: rNP2c22/in6; hv: rNP2c32/out1; }
DETECTOR_LAND(rNP2c18s1) { LABEL("N02_18_1"); signal: rNP2c22/in8; hv: rNP2c32/out2; }
DETECTOR_LAND(rNP2c19s1) { LABEL("N02_19_1"); signal: rNP2c26/in4; hv: rNP2c32/out3; }
DETECTOR_LAND(rNP2c20s1) { LABEL("N02_20_1"); signal: rNP2c26/in2; hv: rNP2c32/out4; }
DETECTOR_LAND(rNP2c01s2) { LABEL("N02_01_2"); signal: rNP2c25/in3; hv: rNP2c33/out1; }
DETECTOR_LAND(rNP2c02s2) { LABEL("N02_02_2"); signal: rNP2c25/in1; hv: rNP2c33/out2; }
DETECTOR_LAND(rNP2c03s2) { LABEL("N02_03_2"); signal: rNP2c23/in1; hv: rNP2c33/out3; }
DETECTOR_LAND(rNP2c04s2) { LABEL("N02_04_2"); signal: rNP2c23/in3; hv: rNP2c33/out4; }
DETECTOR_LAND(rNP2c05s2) { LABEL("N02_05_2"); signal: rNP2c26/in3; hv: rNP2c33/out5; }
DETECTOR_LAND(rNP2c06s2) { LABEL("N02_06_2"); signal: rNP2c26/in1; hv: rNP2c33/out6; }
DETECTOR_LAND(rNP2c07s2) { LABEL("N02_07_2"); signal: rNP2c23/in5; hv: rNP2c33/out7; }
DETECTOR_LAND(rNP2c08s2) { LABEL("N02_08_2"); signal: rNP2c23/in7; hv: rNP2c33/out8; }
DETECTOR_LAND(rNP2c09s2) { LABEL("N02_09_2"); signal: rNP2c24/in1; hv: rNP2c33/out9; }
DETECTOR_LAND(rNP2c10s2) { LABEL("N02_10_2"); signal: rNP2c24/in3; hv: rNP2c33/out10; }
DETECTOR_LAND(rNP2c11s2) { LABEL("N02_11_2"); signal: rNP2c24/in5; hv: rNP2c33/out11; }
DETECTOR_LAND(rNP2c12s2) { LABEL("N02_12_2"); signal: rNP2c24/in7; hv: rNP2c33/out12; }
DETECTOR_LAND(rNP2c13s2) { LABEL("N02_13_2"); signal: rNP2c23/in2; hv: rNP2c33/out13; }
DETECTOR_LAND(rNP2c14s2) { LABEL("N02_14_2"); signal: rNP2c23/in4; hv: rNP2c33/out14; }
DETECTOR_LAND(rNP2c15s2) { LABEL("N02_15_2"); signal: rNP2c23/in6; hv: rNP2c33/out15; }
DETECTOR_LAND(rNP2c16s2) { LABEL("N02_16_2"); signal: rNP2c23/in8; hv: rNP2c33/out16; }
DETECTOR_LAND(rNP2c17s2) { LABEL("N02_17_2"); signal: rNP2c24/in2; hv: rNP2c32/out5; }
DETECTOR_LAND(rNP2c18s2) { LABEL("N02_18_2"); signal: rNP2c24/in4; hv: rNP2c32/out6; }
DETECTOR_LAND(rNP2c19s2) { LABEL("N02_19_2"); signal: rNP2c24/in6; hv: rNP2c32/out7; }
DETECTOR_LAND(rNP2c20s2) { LABEL("N02_20_2"); signal: rNP2c24/in8; hv: rNP2c32/out8; }
JOINER_8(rNP2c21)
{
 in1: rNP2c01s1/signal;
 in2: rNP2c09s1/signal;
 in3: rNP2c02s1/signal;
 in4: rNP2c10s1/signal;
 in5: rNP2c03s1/signal;
 in6: rNP2c11s1/signal;
 in7: rNP2c04s1/signal;
 in8: rNP2c12s1/signal;
 out1_8: r13c1s4/in1_8;
}
JOINER_8(rNP2c22)
{
 in1: rNP2c05s1/signal;
 in2: rNP2c13s1/signal;
 in3: rNP2c06s1/signal;
 in4: rNP2c14s1/signal;
 in5: rNP2c07s1/signal;
 in6: rNP2c17s1/signal;
 in7: rNP2c08s1/signal;
 in8: rNP2c18s1/signal;
 out1_8: r13c1s4/in9_16;
}
JOINER_8(rNP2c23)
{
 in1: rNP2c03s2/signal;
 in2: rNP2c13s2/signal;
 in3: rNP2c04s2/signal;
 in4: rNP2c14s2/signal;
 in5: rNP2c07s2/signal;
 in6: rNP2c15s2/signal;
 in7: rNP2c08s2/signal;
 in8: rNP2c16s2/signal;
 out1_8: r13c1s5/in1_8;
}
JOINER_8(rNP2c24)
{
 in1: rNP2c09s2/signal;
 in2: rNP2c17s2/signal;
 in3: rNP2c10s2/signal;
 in4: rNP2c18s2/signal;
 in5: rNP2c11s2/signal;
 in6: rNP2c19s2/signal;
 in7: rNP2c12s2/signal;
 in8: rNP2c20s2/signal;
 out1_8: r13c1s5/in9_16;
}
JOINER_8(rNP2c25)
{
 in4: rNP2c15s1/signal;
 in3: rNP2c01s2/signal;
 in2: rNP2c16s1/signal;
 in1: rNP2c02s2/signal;
 out1_4: r13c1s3/in1_4;
}
JOINER_8(rNP2c26)
{
 in4: rNP2c19s1/signal;
 in3: rNP2c05s2/signal;
 in2: rNP2c20s1/signal;
 in1: rNP2c06s2/signal;
 out1_4: r13c1s3/in9_12;
}
HV_CONN_16(rNP2c31)
{
 in1_16: r16c1s3/out0_15;
 out1: rNP2c01s1/hv;
 out2: rNP2c02s1/hv;
 out3: rNP2c03s1/hv;
 out4: rNP2c04s1/hv;
 out5: rNP2c05s1/hv;
 out6: rNP2c06s1/hv;
 out7: rNP2c07s1/hv;
 out8: rNP2c08s1/hv;
 out9: rNP2c09s1/hv;
 out10: rNP2c10s1/hv;
 out11: rNP2c11s1/hv;
 out12: rNP2c12s1/hv;
 out13: rNP2c13s1/hv;
 out14: rNP2c14s1/hv;
 out15: rNP2c15s1/hv;
 out16: rNP2c16s1/hv;
}
HV_CONN_16(rNP2c32)
{
 in1_8: r16c1s2/out8_15;
 out1: rNP2c17s1/hv;
 out2: rNP2c18s1/hv;
 out3: rNP2c19s1/hv;
 out4: rNP2c20s1/hv;
 out5: rNP2c17s2/hv;
 out6: rNP2c18s2/hv;
 out7: rNP2c19s2/hv;
 out8: rNP2c20s2/hv;
}
HV_CONN_16(rNP2c33)
{
 in1_16: r16c1s4/out0_15;
 out1: rNP2c01s2/hv;
 out2: rNP2c02s2/hv;
 out3: rNP2c03s2/hv;
 out4: rNP2c04s2/hv;
 out5: rNP2c05s2/hv;
 out6: rNP2c06s2/hv;
 out7: rNP2c07s2/hv;
 out8: rNP2c08s2/hv;
 out9: rNP2c09s2/hv;
 out10: rNP2c10s2/hv;
 out11: rNP2c11s2/hv;
 out12: rNP2c12s2/hv;
 out13: rNP2c13s2/hv;
 out14: rNP2c14s2/hv;
 out15: rNP2c15s2/hv;
 out16: rNP2c16s2/hv;
}

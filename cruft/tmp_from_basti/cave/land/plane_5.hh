// Do not edit, this file is auto-generated
DETECTOR_LAND(rNP5c01s1) { LABEL("N05_01_1"); signal: rNP5c21/in1; hv: rNP5c31/out1; }
DETECTOR_LAND(rNP5c02s1) { LABEL("N05_02_1"); signal: rNP5c21/in3; hv: rNP5c31/out2; }
DETECTOR_LAND(rNP5c03s1) { LABEL("N05_03_1"); signal: rNP5c21/in5; hv: rNP5c31/out3; }
DETECTOR_LAND(rNP5c04s1) { LABEL("N05_04_1"); signal: rNP5c21/in7; hv: rNP5c31/out4; }
DETECTOR_LAND(rNP5c05s1) { LABEL("N05_05_1"); signal: rNP5c22/in1; hv: rNP5c31/out5; }
DETECTOR_LAND(rNP5c06s1) { LABEL("N05_06_1"); signal: rNP5c22/in3; hv: rNP5c31/out6; }
DETECTOR_LAND(rNP5c07s1) { LABEL("N05_07_1"); signal: rNP5c22/in5; hv: rNP5c31/out7; }
DETECTOR_LAND(rNP5c08s1) { LABEL("N05_08_1"); signal: rNP5c22/in7; hv: rNP5c31/out8; }
DETECTOR_LAND(rNP5c09s1) { LABEL("N05_09_1"); signal: rNP5c21/in2; hv: rNP5c31/out9; }
DETECTOR_LAND(rNP5c10s1) { LABEL("N05_10_1"); signal: rNP5c21/in4; hv: rNP5c31/out10; }
DETECTOR_LAND(rNP5c11s1) { LABEL("N05_11_1"); signal: rNP5c21/in6; hv: rNP5c31/out11; }
DETECTOR_LAND(rNP5c12s1) { LABEL("N05_12_1"); signal: rNP5c21/in8; hv: rNP5c31/out12; }
DETECTOR_LAND(rNP5c13s1) { LABEL("N05_13_1"); signal: rNP5c22/in2; hv: rNP5c31/out13; }
DETECTOR_LAND(rNP5c14s1) { LABEL("N05_14_1"); signal: rNP5c22/in4; hv: rNP5c31/out14; }
DETECTOR_LAND(rNP5c15s1) { LABEL("N05_15_1"); signal: rNP5c25/in8; hv: rNP5c31/out15; }
DETECTOR_LAND(rNP5c16s1) { LABEL("N05_16_1"); signal: rNP5c25/in6; hv: rNP5c31/out16; }
DETECTOR_LAND(rNP5c17s1) { LABEL("N05_17_1"); signal: rNP5c22/in6; hv: rNP5c32/out1; }
DETECTOR_LAND(rNP5c18s1) { LABEL("N05_18_1"); signal: rNP5c22/in8; hv: rNP5c32/out2; }
DETECTOR_LAND(rNP5c19s1) { LABEL("N05_19_1"); signal: rNP5c26/in8; hv: rNP5c32/out3; }
DETECTOR_LAND(rNP5c20s1) { LABEL("N05_20_1"); signal: rNP5c26/in6; hv: rNP5c32/out4; }
DETECTOR_LAND(rNP5c01s2) { LABEL("N05_01_2"); signal: rNP5c25/in7; hv: rNP5c33/out1; }
DETECTOR_LAND(rNP5c02s2) { LABEL("N05_02_2"); signal: rNP5c25/in5; hv: rNP5c33/out2; }
DETECTOR_LAND(rNP5c03s2) { LABEL("N05_03_2"); signal: rNP5c23/in1; hv: rNP5c33/out3; }
DETECTOR_LAND(rNP5c04s2) { LABEL("N05_04_2"); signal: rNP5c23/in3; hv: rNP5c33/out4; }
DETECTOR_LAND(rNP5c05s2) { LABEL("N05_05_2"); signal: rNP5c26/in5; hv: rNP5c33/out5; }
DETECTOR_LAND(rNP5c06s2) { LABEL("N05_06_2"); signal: rNP5c26/in7; hv: rNP5c33/out6; }
DETECTOR_LAND(rNP5c07s2) { LABEL("N05_07_2"); signal: rNP5c23/in5; hv: rNP5c33/out7; }
DETECTOR_LAND(rNP5c08s2) { LABEL("N05_08_2"); signal: rNP5c23/in7; hv: rNP5c33/out8; }
DETECTOR_LAND(rNP5c09s2) { LABEL("N05_09_2"); signal: rNP5c24/in1; hv: rNP5c33/out9; }
DETECTOR_LAND(rNP5c10s2) { LABEL("N05_10_2"); signal: rNP5c24/in3; hv: rNP5c33/out10; }
DETECTOR_LAND(rNP5c11s2) { LABEL("N05_11_2"); signal: rNP5c24/in5; hv: rNP5c33/out11; }
DETECTOR_LAND(rNP5c12s2) { LABEL("N05_12_2"); signal: rNP5c24/in7; hv: rNP5c33/out12; }
DETECTOR_LAND(rNP5c13s2) { LABEL("N05_13_2"); signal: rNP5c23/in2; hv: rNP5c33/out13; }
DETECTOR_LAND(rNP5c14s2) { LABEL("N05_14_2"); signal: rNP5c23/in4; hv: rNP5c33/out14; }
DETECTOR_LAND(rNP5c15s2) { LABEL("N05_15_2"); signal: rNP5c23/in6; hv: rNP5c33/out15; }
DETECTOR_LAND(rNP5c16s2) { LABEL("N05_16_2"); signal: rNP5c23/in8; hv: rNP5c33/out16; }
DETECTOR_LAND(rNP5c17s2) { LABEL("N05_17_2"); signal: rNP5c24/in2; hv: rNP5c32/out5; }
DETECTOR_LAND(rNP5c18s2) { LABEL("N05_18_2"); signal: rNP5c24/in4; hv: rNP5c32/out6; }
DETECTOR_LAND(rNP5c19s2) { LABEL("N05_19_2"); signal: rNP5c24/in6; hv: rNP5c32/out7; }
DETECTOR_LAND(rNP5c20s2) { LABEL("N05_20_2"); signal: rNP5c24/in8; hv: rNP5c32/out8; }
JOINER_8(rNP5c21)
{
 in1: rNP5c01s1/signal;
 in2: rNP5c09s1/signal;
 in3: rNP5c02s1/signal;
 in4: rNP5c10s1/signal;
 in5: rNP5c03s1/signal;
 in6: rNP5c11s1/signal;
 in7: rNP5c04s1/signal;
 in8: rNP5c12s1/signal;
 out1_8: r13c1s11/in1_8;
}
JOINER_8(rNP5c22)
{
 in1: rNP5c05s1/signal;
 in2: rNP5c13s1/signal;
 in3: rNP5c06s1/signal;
 in4: rNP5c14s1/signal;
 in5: rNP5c07s1/signal;
 in6: rNP5c17s1/signal;
 in7: rNP5c08s1/signal;
 in8: rNP5c18s1/signal;
 out1_8: r13c1s11/in9_16;
}
JOINER_8(rNP5c23)
{
 in1: rNP5c03s2/signal;
 in2: rNP5c13s2/signal;
 in3: rNP5c04s2/signal;
 in4: rNP5c14s2/signal;
 in5: rNP5c07s2/signal;
 in6: rNP5c15s2/signal;
 in7: rNP5c08s2/signal;
 in8: rNP5c16s2/signal;
 out1_8: r13c1s12/in1_8;
}
JOINER_8(rNP5c24)
{
 in1: rNP5c09s2/signal;
 in2: rNP5c17s2/signal;
 in3: rNP5c10s2/signal;
 in4: rNP5c18s2/signal;
 in5: rNP5c11s2/signal;
 in6: rNP5c19s2/signal;
 in7: rNP5c12s2/signal;
 in8: rNP5c20s2/signal;
 out1_8: r13c1s12/in9_16;
}
JOINER_8(rNP5c25)
{
 in8: rNP5c15s1/signal;
 in7: rNP5c01s2/signal;
 in6: rNP5c16s1/signal;
 in5: rNP5c02s2/signal;
 out5_8: r13c1s13/in5_8;
}
JOINER_8(rNP5c26)
{
 in8: rNP5c19s1/signal;
 in7:  rNP5c06s2/signal;
 in6: rNP5c20s1/signal;
 in5:  rNP5c05s2/signal;
 out5_8: r13c1s13/in13_16;
}
HV_CONN_16(rNP5c31)
{
 in1_16: r16c1s10/out0_15;
 out1: rNP5c01s1/hv;
 out2: rNP5c02s1/hv;
 out3: rNP5c03s1/hv;
 out4: rNP5c04s1/hv;
 out5: rNP5c05s1/hv;
 out6: rNP5c06s1/hv;
 out7: rNP5c07s1/hv;
 out8: rNP5c08s1/hv;
 out9: rNP5c09s1/hv;
 out10: rNP5c10s1/hv;
 out11: rNP5c11s1/hv;
 out12: rNP5c12s1/hv;
 out13: rNP5c13s1/hv;
 out14: rNP5c14s1/hv;
 out15: rNP5c15s1/hv;
 out16: rNP5c16s1/hv;
}
HV_CONN_16(rNP5c32)
{
 in1_8: r16c1s12/out0_7;
 out1: rNP5c17s1/hv;
 out2: rNP5c18s1/hv;
 out3: rNP5c19s1/hv;
 out4: rNP5c20s1/hv;
 out5: rNP5c17s2/hv;
 out6: rNP5c18s2/hv;
 out7: rNP5c19s2/hv;
 out8: rNP5c20s2/hv;
}
HV_CONN_16(rNP5c33)
{
 in1_16: r16c1s11/out0_15;
 out1: rNP5c01s2/hv;
 out2: rNP5c02s2/hv;
 out3: rNP5c03s2/hv;
 out4: rNP5c04s2/hv;
 out5: rNP5c05s2/hv;
 out6: rNP5c06s2/hv;
 out7: rNP5c07s2/hv;
 out8: rNP5c08s2/hv;
 out9: rNP5c09s2/hv;
 out10: rNP5c10s2/hv;
 out11: rNP5c11s2/hv;
 out12: rNP5c12s2/hv;
 out13: rNP5c13s2/hv;
 out14: rNP5c14s2/hv;
 out15: rNP5c15s2/hv;
 out16: rNP5c16s2/hv;
}

// Do not edit, this file is auto-generated
DETECTOR_LAND(rNP7c01s1) { LABEL("N07_01_1"); signal: rNP7c21/in1; hv: rNP7c31/out1; }
DETECTOR_LAND(rNP7c02s1) { LABEL("N07_02_1"); signal: rNP7c21/in3; hv: rNP7c31/out2; }
DETECTOR_LAND(rNP7c03s1) { LABEL("N07_03_1"); signal: rNP7c21/in5; hv: rNP7c31/out3; }
DETECTOR_LAND(rNP7c04s1) { LABEL("N07_04_1"); signal: rNP7c21/in7; hv: rNP7c31/out4; }
DETECTOR_LAND(rNP7c05s1) { LABEL("N07_05_1"); signal: rNP7c22/in1; hv: rNP7c31/out5; }
DETECTOR_LAND(rNP7c06s1) { LABEL("N07_06_1"); signal: rNP7c22/in3; hv: rNP7c31/out6; }
DETECTOR_LAND(rNP7c07s1) { LABEL("N07_07_1"); signal: rNP7c22/in5; hv: rNP7c31/out7; }
DETECTOR_LAND(rNP7c08s1) { LABEL("N07_08_1"); signal: rNP7c22/in7; hv: rNP7c31/out8; }
DETECTOR_LAND(rNP7c09s1) { LABEL("N07_09_1"); signal: rNP7c21/in2; hv: rNP7c31/out9; }
DETECTOR_LAND(rNP7c10s1) { LABEL("N07_10_1"); signal: rNP7c21/in4; hv: rNP7c31/out10; }
DETECTOR_LAND(rNP7c11s1) { LABEL("N07_11_1"); signal: rNP7c21/in6; hv: rNP7c31/out11; }
DETECTOR_LAND(rNP7c12s1) { LABEL("N07_12_1"); signal: rNP7c21/in8; hv: rNP7c31/out12; }
DETECTOR_LAND(rNP7c13s1) { LABEL("N07_13_1"); signal: rNP7c22/in2; hv: rNP7c31/out13; }
DETECTOR_LAND(rNP7c14s1) { LABEL("N07_14_1"); signal: rNP7c22/in4; hv: rNP7c31/out14; }
DETECTOR_LAND(rNP7c15s1) { LABEL("N07_15_1"); signal: rNP7c25/in8; hv: rNP7c31/out15; }
DETECTOR_LAND(rNP7c16s1) { LABEL("N07_16_1"); signal: rNP7c25/in6; hv: rNP7c31/out16; }
DETECTOR_LAND(rNP7c17s1) { LABEL("N07_17_1"); signal: rNP7c22/in6; hv: rNP7c32/out1; }
DETECTOR_LAND(rNP7c18s1) { LABEL("N07_18_1"); signal: rNP7c22/in8; hv: rNP7c32/out2; }
DETECTOR_LAND(rNP7c19s1) { LABEL("N07_19_1"); signal: rNP7c26/in8; hv: rNP7c32/out3; }
DETECTOR_LAND(rNP7c20s1) { LABEL("N07_20_1"); signal: rNP7c26/in6; hv: rNP7c32/out4; }
DETECTOR_LAND(rNP7c01s2) { LABEL("N07_01_2"); signal: rNP7c25/in7; hv: rNP7c33/out1; }
DETECTOR_LAND(rNP7c02s2) { LABEL("N07_02_2"); signal: rNP7c25/in5; hv: rNP7c33/out2; }
DETECTOR_LAND(rNP7c03s2) { LABEL("N07_03_2"); signal: rNP7c23/in1; hv: rNP7c33/out3; }
DETECTOR_LAND(rNP7c04s2) { LABEL("N07_04_2"); signal: rNP7c23/in3; hv: rNP7c33/out4; }
DETECTOR_LAND(rNP7c05s2) { LABEL("N07_05_2"); signal: rNP7c26/in5; hv: rNP7c33/out5; }
DETECTOR_LAND(rNP7c06s2) { LABEL("N07_06_2"); signal: rNP7c26/in7; hv: rNP7c33/out6; }
DETECTOR_LAND(rNP7c07s2) { LABEL("N07_07_2"); signal: rNP7c23/in5; hv: rNP7c33/out7; }
DETECTOR_LAND(rNP7c08s2) { LABEL("N07_08_2"); signal: rNP7c23/in7; hv: rNP7c33/out8; }
DETECTOR_LAND(rNP7c09s2) { LABEL("N07_09_2"); signal: rNP7c24/in1; hv: rNP7c33/out9; }
DETECTOR_LAND(rNP7c10s2) { LABEL("N07_10_2"); signal: rNP7c24/in3; hv: rNP7c33/out10; }
DETECTOR_LAND(rNP7c11s2) { LABEL("N07_11_2"); signal: rNP7c24/in5; hv: rNP7c33/out11; }
DETECTOR_LAND(rNP7c12s2) { LABEL("N07_12_2"); signal: rNP7c24/in7; hv: rNP7c33/out12; }
DETECTOR_LAND(rNP7c13s2) { LABEL("N07_13_2"); signal: rNP7c23/in2; hv: rNP7c33/out13; }
DETECTOR_LAND(rNP7c14s2) { LABEL("N07_14_2"); signal: rNP7c23/in4; hv: rNP7c33/out14; }
DETECTOR_LAND(rNP7c15s2) { LABEL("N07_15_2"); signal: rNP7c23/in6; hv: rNP7c33/out15; }
DETECTOR_LAND(rNP7c16s2) { LABEL("N07_16_2"); signal: rNP7c23/in8; hv: rNP7c33/out16; }
DETECTOR_LAND(rNP7c17s2) { LABEL("N07_17_2"); signal: rNP7c24/in2; hv: rNP7c32/out5; }
DETECTOR_LAND(rNP7c18s2) { LABEL("N07_18_2"); signal: rNP7c24/in4; hv: rNP7c32/out6; }
DETECTOR_LAND(rNP7c19s2) { LABEL("N07_19_2"); signal: rNP7c24/in6; hv: rNP7c32/out7; }
DETECTOR_LAND(rNP7c20s2) { LABEL("N07_20_2"); signal: rNP7c24/in8; hv: rNP7c32/out8; }
JOINER_8(rNP7c21)
{
 in1: rNP7c01s1/signal;
 in2: rNP7c09s1/signal;
 in3: rNP7c02s1/signal;
 in4: rNP7c10s1/signal;
 in5: rNP7c03s1/signal;
 in6: rNP7c11s1/signal;
 in7: rNP7c04s1/signal;
 in8: rNP7c12s1/signal;
 out1_8: r13c3s2/in1_8;
}
JOINER_8(rNP7c22)
{
 in1: rNP7c05s1/signal;
 in2: rNP7c13s1/signal;
 in3: rNP7c06s1/signal;
 in4: rNP7c14s1/signal;
 in5: rNP7c07s1/signal;
 in6: rNP7c17s1/signal;
 in7: rNP7c08s1/signal;
 in8: rNP7c18s1/signal;
 out1_8: r13c3s2/in9_16;
}
JOINER_8(rNP7c23)
{
 in1: rNP7c03s2/signal;
 in2: rNP7c13s2/signal;
 in3: rNP7c04s2/signal;
 in4: rNP7c14s2/signal;
 in5: rNP7c07s2/signal;
 in6: rNP7c15s2/signal;
 in7: rNP7c08s2/signal;
 in8: rNP7c16s2/signal;
 out1_8: r13c3s3/in1_8;
}
JOINER_8(rNP7c24)
{
 in1: rNP7c09s2/signal;
 in2: rNP7c17s2/signal;
 in3: rNP7c10s2/signal;
 in4: rNP7c18s2/signal;
 in5: rNP7c11s2/signal;
 in6: rNP7c19s2/signal;
 in7: rNP7c12s2/signal;
 in8: rNP7c20s2/signal;
 out1_8: r13c3s3/in9_16;
}
JOINER_8(rNP7c25)
{
 in8: rNP7c15s1/signal;
 in7: rNP7c01s2/signal;
 in6: rNP7c16s1/signal;
 in5: rNP7c02s2/signal;
 out5_8: r13c3s4/in5_8;
}
JOINER_8(rNP7c26)
{
 in8: rNP7c19s1/signal;
 in7:  rNP7c06s2/signal;
 in6: rNP7c20s1/signal;
 in5:  rNP7c05s2/signal;
 out5_8: r13c3s4/in13_16;
}
HV_CONN_16(rNP7c31)
{
 in1_16: r16c1s15/out0_15;
 out1: rNP7c01s1/hv;
 out2: rNP7c02s1/hv;
 out3: rNP7c03s1/hv;
 out4: rNP7c04s1/hv;
 out5: rNP7c05s1/hv;
 out6: rNP7c06s1/hv;
 out7: rNP7c07s1/hv;
 out8: rNP7c08s1/hv;
 out9: rNP7c09s1/hv;
 out10: rNP7c10s1/hv;
 out11: rNP7c11s1/hv;
 out12: rNP7c12s1/hv;
 out13: rNP7c13s1/hv;
 out14: rNP7c14s1/hv;
 out15: rNP7c15s1/hv;
 out16: rNP7c16s1/hv;
}
HV_CONN_16(rNP7c32)
{
 in1_8: r16c2s1/out0_7;
 out1: rNP7c17s1/hv;
 out2: rNP7c18s1/hv;
 out3: rNP7c19s1/hv;
 out4: rNP7c20s1/hv;
 out5: rNP7c17s2/hv;
 out6: rNP7c18s2/hv;
 out7: rNP7c19s2/hv;
 out8: rNP7c20s2/hv;
}
HV_CONN_16(rNP7c33)
{
 in1_16: r16c2s0/out0_15;
 out1: rNP7c01s2/hv;
 out2: rNP7c02s2/hv;
 out3: rNP7c03s2/hv;
 out4: rNP7c04s2/hv;
 out5: rNP7c05s2/hv;
 out6: rNP7c06s2/hv;
 out7: rNP7c07s2/hv;
 out8: rNP7c08s2/hv;
 out9: rNP7c09s2/hv;
 out10: rNP7c10s2/hv;
 out11: rNP7c11s2/hv;
 out12: rNP7c12s2/hv;
 out13: rNP7c13s2/hv;
 out14: rNP7c14s2/hv;
 out15: rNP7c15s2/hv;
 out16: rNP7c16s2/hv;
}

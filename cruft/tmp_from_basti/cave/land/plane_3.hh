// Do not edit, this file is auto-generated
DETECTOR_LAND(rNP3c01s1) { LABEL("N03_01_1"); signal: rNP3c21/in1; hv: rNP3c31/out1; }
DETECTOR_LAND(rNP3c02s1) { LABEL("N03_02_1"); signal: rNP3c21/in3; hv: rNP3c31/out2; }
DETECTOR_LAND(rNP3c03s1) { LABEL("N03_03_1"); signal: rNP3c21/in5; hv: rNP3c31/out3; }
DETECTOR_LAND(rNP3c04s1) { LABEL("N03_04_1"); signal: rNP3c21/in7; hv: rNP3c31/out4; }
DETECTOR_LAND(rNP3c05s1) { LABEL("N03_05_1"); signal: rNP3c22/in1; hv: rNP3c31/out5; }
DETECTOR_LAND(rNP3c06s1) { LABEL("N03_06_1"); signal: rNP3c22/in3; hv: rNP3c31/out6; }
DETECTOR_LAND(rNP3c07s1) { LABEL("N03_07_1"); signal: rNP3c22/in5; hv: rNP3c31/out7; }
DETECTOR_LAND(rNP3c08s1) { LABEL("N03_08_1"); signal: rNP3c22/in7; hv: rNP3c31/out8; }
DETECTOR_LAND(rNP3c09s1) { LABEL("N03_09_1"); signal: rNP3c21/in2; hv: rNP3c31/out9; }
DETECTOR_LAND(rNP3c10s1) { LABEL("N03_10_1"); signal: rNP3c21/in4; hv: rNP3c31/out10; }
DETECTOR_LAND(rNP3c11s1) { LABEL("N03_11_1"); signal: rNP3c21/in6; hv: rNP3c31/out11; }
DETECTOR_LAND(rNP3c12s1) { LABEL("N03_12_1"); signal: rNP3c21/in8; hv: rNP3c31/out12; }
DETECTOR_LAND(rNP3c13s1) { LABEL("N03_13_1"); signal: rNP3c22/in2; hv: rNP3c31/out13; }
DETECTOR_LAND(rNP3c14s1) { LABEL("N03_14_1"); signal: rNP3c22/in4; hv: rNP3c31/out14; }
DETECTOR_LAND(rNP3c15s1) { LABEL("N03_15_1"); signal: rNP3c25/in8; hv: rNP3c31/out15; }
DETECTOR_LAND(rNP3c16s1) { LABEL("N03_16_1"); signal: rNP3c25/in6; hv: rNP3c31/out16; }
DETECTOR_LAND(rNP3c17s1) { LABEL("N03_17_1"); signal: rNP3c22/in6; hv: rNP3c32/out1; }
DETECTOR_LAND(rNP3c18s1) { LABEL("N03_18_1"); signal: rNP3c22/in8; hv: rNP3c32/out2; }
DETECTOR_LAND(rNP3c19s1) { LABEL("N03_19_1"); signal: rNP3c26/in8; hv: rNP3c32/out3; }
DETECTOR_LAND(rNP3c20s1) { LABEL("N03_20_1"); signal: rNP3c26/in6; hv: rNP3c32/out4; }
DETECTOR_LAND(rNP3c01s2) { LABEL("N03_01_2"); signal: rNP3c25/in7; hv: rNP3c33/out1; }
DETECTOR_LAND(rNP3c02s2) { LABEL("N03_02_2"); signal: rNP3c25/in5; hv: rNP3c33/out2; }
DETECTOR_LAND(rNP3c03s2) { LABEL("N03_03_2"); signal: rNP3c23/in1; hv: rNP3c33/out3; }
DETECTOR_LAND(rNP3c04s2) { LABEL("N03_04_2"); signal: rNP3c23/in3; hv: rNP3c33/out4; }
DETECTOR_LAND(rNP3c05s2) { LABEL("N03_05_2"); signal: rNP3c26/in5; hv: rNP3c33/out5; }
DETECTOR_LAND(rNP3c06s2) { LABEL("N03_06_2"); signal: rNP3c26/in7; hv: rNP3c33/out6; }
DETECTOR_LAND(rNP3c07s2) { LABEL("N03_07_2"); signal: rNP3c23/in5; hv: rNP3c33/out7; }
DETECTOR_LAND(rNP3c08s2) { LABEL("N03_08_2"); signal: rNP3c23/in7; hv: rNP3c33/out8; }
DETECTOR_LAND(rNP3c09s2) { LABEL("N03_09_2"); signal: rNP3c24/in1; hv: rNP3c33/out9; }
DETECTOR_LAND(rNP3c10s2) { LABEL("N03_10_2"); signal: rNP3c24/in3; hv: rNP3c33/out10; }
DETECTOR_LAND(rNP3c11s2) { LABEL("N03_11_2"); signal: rNP3c24/in5; hv: rNP3c33/out11; }
DETECTOR_LAND(rNP3c12s2) { LABEL("N03_12_2"); signal: rNP3c24/in7; hv: rNP3c33/out12; }
DETECTOR_LAND(rNP3c13s2) { LABEL("N03_13_2"); signal: rNP3c23/in2; hv: rNP3c33/out13; }
DETECTOR_LAND(rNP3c14s2) { LABEL("N03_14_2"); signal: rNP3c23/in4; hv: rNP3c33/out14; }
DETECTOR_LAND(rNP3c15s2) { LABEL("N03_15_2"); signal: rNP3c23/in6; hv: rNP3c33/out15; }
DETECTOR_LAND(rNP3c16s2) { LABEL("N03_16_2"); signal: rNP3c23/in8; hv: rNP3c33/out16; }
DETECTOR_LAND(rNP3c17s2) { LABEL("N03_17_2"); signal: rNP3c24/in2; hv: rNP3c32/out5; }
DETECTOR_LAND(rNP3c18s2) { LABEL("N03_18_2"); signal: rNP3c24/in4; hv: rNP3c32/out6; }
DETECTOR_LAND(rNP3c19s2) { LABEL("N03_19_2"); signal: rNP3c24/in6; hv: rNP3c32/out7; }
DETECTOR_LAND(rNP3c20s2) { LABEL("N03_20_2"); signal: rNP3c24/in8; hv: rNP3c32/out8; }
JOINER_8(rNP3c21)
{
 in1: rNP3c01s1/signal;
 in2: rNP3c09s1/signal;
 in3: rNP3c02s1/signal;
 in4: rNP3c10s1/signal;
 in5: rNP3c03s1/signal;
 in6: rNP3c11s1/signal;
 in7: rNP3c04s1/signal;
 in8: rNP3c12s1/signal;
 out1_8: r13c1s6/in1_8;
}
JOINER_8(rNP3c22)
{
 in1: rNP3c05s1/signal;
 in2: rNP3c13s1/signal;
 in3: rNP3c06s1/signal;
 in4: rNP3c14s1/signal;
 in5: rNP3c07s1/signal;
 in6: rNP3c17s1/signal;
 in7: rNP3c08s1/signal;
 in8: rNP3c18s1/signal;
 out1_8: r13c1s6/in9_16;
}
JOINER_8(rNP3c23)
{
 in1: rNP3c03s2/signal;
 in2: rNP3c13s2/signal;
 in3: rNP3c04s2/signal;
 in4: rNP3c14s2/signal;
 in5: rNP3c07s2/signal;
 in6: rNP3c15s2/signal;
 in7: rNP3c08s2/signal;
 in8: rNP3c16s2/signal;
 out1_8: r13c1s7/in1_8;
}
JOINER_8(rNP3c24)
{
 in1: rNP3c09s2/signal;
 in2: rNP3c17s2/signal;
 in3: rNP3c10s2/signal;
 in4: rNP3c18s2/signal;
 in5: rNP3c11s2/signal;
 in6: rNP3c19s2/signal;
 in7: rNP3c12s2/signal;
 in8: rNP3c20s2/signal;
 out1_8: r13c1s7/in9_16;
}
JOINER_8(rNP3c25)
{
 in8: rNP3c15s1/signal;
 in7: rNP3c01s2/signal;
 in6: rNP3c16s1/signal;
 in5: rNP3c02s2/signal;
 out5_8: r13c1s8/in5_8;
}
JOINER_8(rNP3c26)
{
 in8: rNP3c19s1/signal;
 in7:  rNP3c06s2/signal;
 in6: rNP3c20s1/signal;
 in5:  rNP3c05s2/signal;
 out5_8: r13c1s8/in13_16;
}
HV_CONN_16(rNP3c31)
{
 in1_16: r16c1s5/out0_15;
 out1: rNP3c01s1/hv;
 out2: rNP3c02s1/hv;
 out3: rNP3c03s1/hv;
 out4: rNP3c04s1/hv;
 out5: rNP3c05s1/hv;
 out6: rNP3c06s1/hv;
 out7: rNP3c07s1/hv;
 out8: rNP3c08s1/hv;
 out9: rNP3c09s1/hv;
 out10: rNP3c10s1/hv;
 out11: rNP3c11s1/hv;
 out12: rNP3c12s1/hv;
 out13: rNP3c13s1/hv;
 out14: rNP3c14s1/hv;
 out15: rNP3c15s1/hv;
 out16: rNP3c16s1/hv;
}
HV_CONN_16(rNP3c32)
{
 in1_8: r16c1s7/out0_7;
 out1: rNP3c17s1/hv;
 out2: rNP3c18s1/hv;
 out3: rNP3c19s1/hv;
 out4: rNP3c20s1/hv;
 out5: rNP3c17s2/hv;
 out6: rNP3c18s2/hv;
 out7: rNP3c19s2/hv;
 out8: rNP3c20s2/hv;
}
HV_CONN_16(rNP3c33)
{
 in1_16: r16c1s6/out0_15;
 out1: rNP3c01s2/hv;
 out2: rNP3c02s2/hv;
 out3: rNP3c03s2/hv;
 out4: rNP3c04s2/hv;
 out5: rNP3c05s2/hv;
 out6: rNP3c06s2/hv;
 out7: rNP3c07s2/hv;
 out8: rNP3c08s2/hv;
 out9: rNP3c09s2/hv;
 out10: rNP3c10s2/hv;
 out11: rNP3c11s2/hv;
 out12: rNP3c12s2/hv;
 out13: rNP3c13s2/hv;
 out14: rNP3c14s2/hv;
 out15: rNP3c15s2/hv;
 out16: rNP3c16s2/hv;
}

// Do not edit, this file is auto-generated
DETECTOR_LAND(rNP8c01s1) { LABEL("N08_01_1"); signal: rNP8c21/in1; hv: rNP8c31/out1; }
DETECTOR_LAND(rNP8c02s1) { LABEL("N08_02_1"); signal: rNP8c21/in3; hv: rNP8c31/out2; }
DETECTOR_LAND(rNP8c03s1) { LABEL("N08_03_1"); signal: rNP8c21/in5; hv: rNP8c31/out3; }
DETECTOR_LAND(rNP8c04s1) { LABEL("N08_04_1"); signal: rNP8c21/in7; hv: rNP8c31/out4; }
DETECTOR_LAND(rNP8c05s1) { LABEL("N08_05_1"); signal: rNP8c22/in1; hv: rNP8c31/out5; }
DETECTOR_LAND(rNP8c06s1) { LABEL("N08_06_1"); signal: rNP8c22/in3; hv: rNP8c31/out6; }
DETECTOR_LAND(rNP8c07s1) { LABEL("N08_07_1"); signal: rNP8c22/in5; hv: rNP8c31/out7; }
DETECTOR_LAND(rNP8c08s1) { LABEL("N08_08_1"); signal: rNP8c22/in7; hv: rNP8c31/out8; }
DETECTOR_LAND(rNP8c09s1) { LABEL("N08_09_1"); signal: rNP8c21/in2; hv: rNP8c31/out9; }
DETECTOR_LAND(rNP8c10s1) { LABEL("N08_10_1"); signal: rNP8c21/in4; hv: rNP8c31/out10; }
DETECTOR_LAND(rNP8c11s1) { LABEL("N08_11_1"); signal: rNP8c21/in6; hv: rNP8c31/out11; }
DETECTOR_LAND(rNP8c12s1) { LABEL("N08_12_1"); signal: rNP8c21/in8; hv: rNP8c31/out12; }
DETECTOR_LAND(rNP8c13s1) { LABEL("N08_13_1"); signal: rNP8c22/in2; hv: rNP8c31/out13; }
DETECTOR_LAND(rNP8c14s1) { LABEL("N08_14_1"); signal: rNP8c22/in4; hv: rNP8c31/out14; }
DETECTOR_LAND(rNP8c15s1) { LABEL("N08_15_1"); signal: rNP8c25/in4; hv: rNP8c31/out15; }
DETECTOR_LAND(rNP8c16s1) { LABEL("N08_16_1"); signal: rNP8c25/in2; hv: rNP8c31/out16; }
DETECTOR_LAND(rNP8c17s1) { LABEL("N08_17_1"); signal: rNP8c22/in6; hv: rNP8c32/out1; }
DETECTOR_LAND(rNP8c18s1) { LABEL("N08_18_1"); signal: rNP8c22/in8; hv: rNP8c32/out2; }
DETECTOR_LAND(rNP8c19s1) { LABEL("N08_19_1"); signal: rNP8c26/in4; hv: rNP8c32/out3; }
DETECTOR_LAND(rNP8c20s1) { LABEL("N08_20_1"); signal: rNP8c26/in2; hv: rNP8c32/out4; }
DETECTOR_LAND(rNP8c01s2) { LABEL("N08_01_2"); signal: rNP8c25/in3; hv: rNP8c33/out1; }
DETECTOR_LAND(rNP8c02s2) { LABEL("N08_02_2"); signal: rNP8c25/in1; hv: rNP8c33/out2; }
DETECTOR_LAND(rNP8c03s2) { LABEL("N08_03_2"); signal: rNP8c23/in1; hv: rNP8c33/out3; }
DETECTOR_LAND(rNP8c04s2) { LABEL("N08_04_2"); signal: rNP8c23/in3; hv: rNP8c33/out4; }
DETECTOR_LAND(rNP8c05s2) { LABEL("N08_05_2"); signal: rNP8c26/in3; hv: rNP8c33/out5; }
DETECTOR_LAND(rNP8c06s2) { LABEL("N08_06_2"); signal: rNP8c26/in1; hv: rNP8c33/out6; }
DETECTOR_LAND(rNP8c07s2) { LABEL("N08_07_2"); signal: rNP8c23/in5; hv: rNP8c33/out7; }
DETECTOR_LAND(rNP8c08s2) { LABEL("N08_08_2"); signal: rNP8c23/in7; hv: rNP8c33/out8; }
DETECTOR_LAND(rNP8c09s2) { LABEL("N08_09_2"); signal: rNP8c24/in1; hv: rNP8c33/out9; }
DETECTOR_LAND(rNP8c10s2) { LABEL("N08_10_2"); signal: rNP8c24/in3; hv: rNP8c33/out10; }
DETECTOR_LAND(rNP8c11s2) { LABEL("N08_11_2"); signal: rNP8c24/in5; hv: rNP8c33/out11; }
DETECTOR_LAND(rNP8c12s2) { LABEL("N08_12_2"); signal: rNP8c24/in7; hv: rNP8c33/out12; }
DETECTOR_LAND(rNP8c13s2) { LABEL("N08_13_2"); signal: rNP8c23/in2; hv: rNP8c33/out13; }
DETECTOR_LAND(rNP8c14s2) { LABEL("N08_14_2"); signal: rNP8c23/in4; hv: rNP8c33/out14; }
DETECTOR_LAND(rNP8c15s2) { LABEL("N08_15_2"); signal: rNP8c23/in6; hv: rNP8c33/out15; }
DETECTOR_LAND(rNP8c16s2) { LABEL("N08_16_2"); signal: rNP8c23/in8; hv: rNP8c33/out16; }
DETECTOR_LAND(rNP8c17s2) { LABEL("N08_17_2"); signal: rNP8c24/in2; hv: rNP8c32/out5; }
DETECTOR_LAND(rNP8c18s2) { LABEL("N08_18_2"); signal: rNP8c24/in4; hv: rNP8c32/out6; }
DETECTOR_LAND(rNP8c19s2) { LABEL("N08_19_2"); signal: rNP8c24/in6; hv: rNP8c32/out7; }
DETECTOR_LAND(rNP8c20s2) { LABEL("N08_20_2"); signal: rNP8c24/in8; hv: rNP8c32/out8; }
JOINER_8(rNP8c21)
{
 in1: rNP8c01s1/signal;
 in2: rNP8c09s1/signal;
 in3: rNP8c02s1/signal;
 in4: rNP8c10s1/signal;
 in5: rNP8c03s1/signal;
 in6: rNP8c11s1/signal;
 in7: rNP8c04s1/signal;
 in8: rNP8c12s1/signal;
 out1_8: r13c3s5/in1_8;
}
JOINER_8(rNP8c22)
{
 in1: rNP8c05s1/signal;
 in2: rNP8c13s1/signal;
 in3: rNP8c06s1/signal;
 in4: rNP8c14s1/signal;
 in5: rNP8c07s1/signal;
 in6: rNP8c17s1/signal;
 in7: rNP8c08s1/signal;
 in8: rNP8c18s1/signal;
 out1_8: r13c3s5/in9_16;
}
JOINER_8(rNP8c23)
{
 in1: rNP8c03s2/signal;
 in2: rNP8c13s2/signal;
 in3: rNP8c04s2/signal;
 in4: rNP8c14s2/signal;
 in5: rNP8c07s2/signal;
 in6: rNP8c15s2/signal;
 in7: rNP8c08s2/signal;
 in8: rNP8c16s2/signal;
 out1_8: r13c3s6/in1_8;
}
JOINER_8(rNP8c24)
{
 in1: rNP8c09s2/signal;
 in2: rNP8c17s2/signal;
 in3: rNP8c10s2/signal;
 in4: rNP8c18s2/signal;
 in5: rNP8c11s2/signal;
 in6: rNP8c19s2/signal;
 in7: rNP8c12s2/signal;
 in8: rNP8c20s2/signal;
 out1_8: r13c3s6/in9_16;
}
JOINER_8(rNP8c25)
{
 in4: rNP8c15s1/signal;
 in3: rNP8c01s2/signal;
 in2: rNP8c16s1/signal;
 in1: rNP8c02s2/signal;
 out1_4: r13c3s4/in1_4;
}
JOINER_8(rNP8c26)
{
 in4: rNP8c19s1/signal;
 in3: rNP8c05s2/signal;
 in2: rNP8c20s1/signal;
 in1: rNP8c06s2/signal;
 out1_4: r13c3s4/in9_12;
}
HV_CONN_16(rNP8c31)
{
 in1_16: r16c2s2/out0_15;
 out1: rNP8c01s1/hv;
 out2: rNP8c02s1/hv;
 out3: rNP8c03s1/hv;
 out4: rNP8c04s1/hv;
 out5: rNP8c05s1/hv;
 out6: rNP8c06s1/hv;
 out7: rNP8c07s1/hv;
 out8: rNP8c08s1/hv;
 out9: rNP8c09s1/hv;
 out10: rNP8c10s1/hv;
 out11: rNP8c11s1/hv;
 out12: rNP8c12s1/hv;
 out13: rNP8c13s1/hv;
 out14: rNP8c14s1/hv;
 out15: rNP8c15s1/hv;
 out16: rNP8c16s1/hv;
}
HV_CONN_16(rNP8c32)
{
 in1_8: r16c2s1/out8_15;
 out1: rNP8c17s1/hv;
 out2: rNP8c18s1/hv;
 out3: rNP8c19s1/hv;
 out4: rNP8c20s1/hv;
 out5: rNP8c17s2/hv;
 out6: rNP8c18s2/hv;
 out7: rNP8c19s2/hv;
 out8: rNP8c20s2/hv;
}
HV_CONN_16(rNP8c33)
{
 in1_16: r16c2s3/out0_15;
 out1: rNP8c01s2/hv;
 out2: rNP8c02s2/hv;
 out3: rNP8c03s2/hv;
 out4: rNP8c04s2/hv;
 out5: rNP8c05s2/hv;
 out6: rNP8c06s2/hv;
 out7: rNP8c07s2/hv;
 out8: rNP8c08s2/hv;
 out9: rNP8c09s2/hv;
 out10: rNP8c10s2/hv;
 out11: rNP8c11s2/hv;
 out12: rNP8c12s2/hv;
 out13: rNP8c13s2/hv;
 out14: rNP8c14s2/hv;
 out15: rNP8c15s2/hv;
 out16: rNP8c16s2/hv;
}

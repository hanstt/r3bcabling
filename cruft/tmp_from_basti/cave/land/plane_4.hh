// Do not edit, this file is auto-generated
DETECTOR_LAND(rNP4c01s1) { LABEL("N04_01_1"); signal: rNP4c21/in1; hv: rNP4c31/out1; }
DETECTOR_LAND(rNP4c02s1) { LABEL("N04_02_1"); signal: rNP4c21/in3; hv: rNP4c31/out2; }
DETECTOR_LAND(rNP4c03s1) { LABEL("N04_03_1"); signal: rNP4c21/in5; hv: rNP4c31/out3; }
DETECTOR_LAND(rNP4c04s1) { LABEL("N04_04_1"); signal: rNP4c21/in7; hv: rNP4c31/out4; }
DETECTOR_LAND(rNP4c05s1) { LABEL("N04_05_1"); signal: rNP4c22/in1; hv: rNP4c31/out5; }
DETECTOR_LAND(rNP4c06s1) { LABEL("N04_06_1"); signal: rNP4c22/in3; hv: rNP4c31/out6; }
DETECTOR_LAND(rNP4c07s1) { LABEL("N04_07_1"); signal: rNP4c22/in5; hv: rNP4c31/out7; }
DETECTOR_LAND(rNP4c08s1) { LABEL("N04_08_1"); signal: rNP4c22/in7; hv: rNP4c31/out8; }
DETECTOR_LAND(rNP4c09s1) { LABEL("N04_09_1"); signal: rNP4c21/in2; hv: rNP4c31/out9; }
DETECTOR_LAND(rNP4c10s1) { LABEL("N04_10_1"); signal: rNP4c21/in4; hv: rNP4c31/out10; }
DETECTOR_LAND(rNP4c11s1) { LABEL("N04_11_1"); signal: rNP4c21/in6; hv: rNP4c31/out11; }
DETECTOR_LAND(rNP4c12s1) { LABEL("N04_12_1"); signal: rNP4c21/in8; hv: rNP4c31/out12; }
DETECTOR_LAND(rNP4c13s1) { LABEL("N04_13_1"); signal: rNP4c22/in2; hv: rNP4c31/out13; }
DETECTOR_LAND(rNP4c14s1) { LABEL("N04_14_1"); signal: rNP4c22/in4; hv: rNP4c31/out14; }
DETECTOR_LAND(rNP4c15s1) { LABEL("N04_15_1"); signal: rNP4c25/in4; hv: rNP4c31/out15; }
DETECTOR_LAND(rNP4c16s1) { LABEL("N04_16_1"); signal: rNP4c25/in2; hv: rNP4c31/out16; }
DETECTOR_LAND(rNP4c17s1) { LABEL("N04_17_1"); signal: rNP4c22/in6; hv: rNP4c32/out1; }
DETECTOR_LAND(rNP4c18s1) { LABEL("N04_18_1"); signal: rNP4c22/in8; hv: rNP4c32/out2; }
DETECTOR_LAND(rNP4c19s1) { LABEL("N04_19_1"); signal: rNP4c26/in4; hv: rNP4c32/out3; }
DETECTOR_LAND(rNP4c20s1) { LABEL("N04_20_1"); signal: rNP4c26/in2; hv: rNP4c32/out4; }
DETECTOR_LAND(rNP4c01s2) { LABEL("N04_01_2"); signal: rNP4c25/in3; hv: rNP4c33/out1; }
DETECTOR_LAND(rNP4c02s2) { LABEL("N04_02_2"); signal: rNP4c25/in1; hv: rNP4c33/out2; }
DETECTOR_LAND(rNP4c03s2) { LABEL("N04_03_2"); signal: rNP4c23/in1; hv: rNP4c33/out3; }
DETECTOR_LAND(rNP4c04s2) { LABEL("N04_04_2"); signal: rNP4c23/in3; hv: rNP4c33/out4; }
DETECTOR_LAND(rNP4c05s2) { LABEL("N04_05_2"); signal: rNP4c26/in3; hv: rNP4c33/out5; }
DETECTOR_LAND(rNP4c06s2) { LABEL("N04_06_2"); signal: rNP4c26/in1; hv: rNP4c33/out6; }
DETECTOR_LAND(rNP4c07s2) { LABEL("N04_07_2"); signal: rNP4c23/in5; hv: rNP4c33/out7; }
DETECTOR_LAND(rNP4c08s2) { LABEL("N04_08_2"); signal: rNP4c23/in7; hv: rNP4c33/out8; }
DETECTOR_LAND(rNP4c09s2) { LABEL("N04_09_2"); signal: rNP4c24/in1; hv: rNP4c33/out9; }
DETECTOR_LAND(rNP4c10s2) { LABEL("N04_10_2"); signal: rNP4c24/in3; hv: rNP4c33/out10; }
DETECTOR_LAND(rNP4c11s2) { LABEL("N04_11_2"); signal: rNP4c24/in5; hv: rNP4c33/out11; }
DETECTOR_LAND(rNP4c12s2) { LABEL("N04_12_2"); signal: rNP4c24/in7; hv: rNP4c33/out12; }
DETECTOR_LAND(rNP4c13s2) { LABEL("N04_13_2"); signal: rNP4c23/in2; hv: rNP4c33/out13; }
DETECTOR_LAND(rNP4c14s2) { LABEL("N04_14_2"); signal: rNP4c23/in4; hv: rNP4c33/out14; }
DETECTOR_LAND(rNP4c15s2) { LABEL("N04_15_2"); signal: rNP4c23/in6; hv: rNP4c33/out15; }
DETECTOR_LAND(rNP4c16s2) { LABEL("N04_16_2"); signal: rNP4c23/in8; hv: rNP4c33/out16; }
DETECTOR_LAND(rNP4c17s2) { LABEL("N04_17_2"); signal: rNP4c24/in2; hv: rNP4c32/out5; }
DETECTOR_LAND(rNP4c18s2) { LABEL("N04_18_2"); signal: rNP4c24/in4; hv: rNP4c32/out6; }
DETECTOR_LAND(rNP4c19s2) { LABEL("N04_19_2"); signal: rNP4c24/in6; hv: rNP4c32/out7; }
DETECTOR_LAND(rNP4c20s2) { LABEL("N04_20_2"); signal: rNP4c24/in8; hv: rNP4c32/out8; }
JOINER_8(rNP4c21)
{
 in1: rNP4c01s1/signal;
 in2: rNP4c09s1/signal;
 in3: rNP4c02s1/signal;
 in4: rNP4c10s1/signal;
 in5: rNP4c03s1/signal;
 in6: rNP4c11s1/signal;
 in7: rNP4c04s1/signal;
 in8: rNP4c12s1/signal;
 out1_8: r13c1s9/in1_8;
}
JOINER_8(rNP4c22)
{
 in1: rNP4c05s1/signal;
 in2: rNP4c13s1/signal;
 in3: rNP4c06s1/signal;
 in4: rNP4c14s1/signal;
 in5: rNP4c07s1/signal;
 in6: rNP4c17s1/signal;
 in7: rNP4c08s1/signal;
 in8: rNP4c18s1/signal;
 out1_8: r13c1s9/in9_16;
}
JOINER_8(rNP4c23)
{
 in1: rNP4c03s2/signal;
 in2: rNP4c13s2/signal;
 in3: rNP4c04s2/signal;
 in4: rNP4c14s2/signal;
 in5: rNP4c07s2/signal;
 in6: rNP4c15s2/signal;
 in7: rNP4c08s2/signal;
 in8: rNP4c16s2/signal;
 out1_8: r13c1s10/in1_8;
}
JOINER_8(rNP4c24)
{
 in1: rNP4c09s2/signal;
 in2: rNP4c17s2/signal;
 in3: rNP4c10s2/signal;
 in4: rNP4c18s2/signal;
 in5: rNP4c11s2/signal;
 in6: rNP4c19s2/signal;
 in7: rNP4c12s2/signal;
 in8: rNP4c20s2/signal;
 out1_8: r13c1s10/in9_16;
}
JOINER_8(rNP4c25)
{
 in4: rNP4c15s1/signal;
 in3: rNP4c01s2/signal;
 in2: rNP4c16s1/signal;
 in1: rNP4c02s2/signal;
 out1_4: r13c1s8/in1_4;
}
JOINER_8(rNP4c26)
{
 in4: rNP4c19s1/signal;
 in3: rNP4c05s2/signal;
 in2: rNP4c20s1/signal;
 in1: rNP4c06s2/signal;
 out1_4: r13c1s8/in9_12;
}
HV_CONN_16(rNP4c31)
{
 in1_16: r16c1s8/out0_15;
 out1: rNP4c01s1/hv;
 out2: rNP4c02s1/hv;
 out3: rNP4c03s1/hv;
 out4: rNP4c04s1/hv;
 out5: rNP4c05s1/hv;
 out6: rNP4c06s1/hv;
 out7: rNP4c07s1/hv;
 out8: rNP4c08s1/hv;
 out9: rNP4c09s1/hv;
 out10: rNP4c10s1/hv;
 out11: rNP4c11s1/hv;
 out12: rNP4c12s1/hv;
 out13: rNP4c13s1/hv;
 out14: rNP4c14s1/hv;
 out15: rNP4c15s1/hv;
 out16: rNP4c16s1/hv;
}
HV_CONN_16(rNP4c32)
{
 in1_8: r16c1s7/out8_15;
 out1: rNP4c17s1/hv;
 out2: rNP4c18s1/hv;
 out3: rNP4c19s1/hv;
 out4: rNP4c20s1/hv;
 out5: rNP4c17s2/hv;
 out6: rNP4c18s2/hv;
 out7: rNP4c19s2/hv;
 out8: rNP4c20s2/hv;
}
HV_CONN_16(rNP4c33)
{
 in1_16: r16c1s9/out0_15;
 out1: rNP4c01s2/hv;
 out2: rNP4c02s2/hv;
 out3: rNP4c03s2/hv;
 out4: rNP4c04s2/hv;
 out5: rNP4c05s2/hv;
 out6: rNP4c06s2/hv;
 out7: rNP4c07s2/hv;
 out8: rNP4c08s2/hv;
 out9: rNP4c09s2/hv;
 out10: rNP4c10s2/hv;
 out11: rNP4c11s2/hv;
 out12: rNP4c12s2/hv;
 out13: rNP4c13s2/hv;
 out14: rNP4c14s2/hv;
 out15: rNP4c15s2/hv;
 out16: rNP4c16s2/hv;
}

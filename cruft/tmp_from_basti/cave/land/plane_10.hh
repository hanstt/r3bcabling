// Do not edit, this file is auto-generated
DETECTOR_LAND(rNP10c01s1) { LABEL("N10_01_1"); signal: rNP10c21/in1; hv: rNP10c31/out1; }
DETECTOR_LAND(rNP10c02s1) { LABEL("N10_02_1"); signal: rNP10c21/in3; hv: rNP10c31/out2; }
DETECTOR_LAND(rNP10c03s1) { LABEL("N10_03_1"); signal: rNP10c21/in5; hv: rNP10c31/out3; }
DETECTOR_LAND(rNP10c04s1) { LABEL("N10_04_1"); signal: rNP10c21/in7; hv: rNP10c31/out4; }
DETECTOR_LAND(rNP10c05s1) { LABEL("N10_05_1"); signal: rNP10c22/in1; hv: rNP10c31/out5; }
DETECTOR_LAND(rNP10c06s1) { LABEL("N10_06_1"); signal: rNP10c22/in3; hv: rNP10c31/out6; }
DETECTOR_LAND(rNP10c07s1) { LABEL("N10_07_1"); signal: rNP10c22/in5; hv: rNP10c31/out7; }
DETECTOR_LAND(rNP10c08s1) { LABEL("N10_08_1"); signal: rNP10c22/in7; hv: rNP10c31/out8; }
DETECTOR_LAND(rNP10c09s1) { LABEL("N10_09_1"); signal: rNP10c21/in2; hv: rNP10c31/out9; }
DETECTOR_LAND(rNP10c10s1) { LABEL("N10_10_1"); signal: rNP10c21/in4; hv: rNP10c31/out10; }
DETECTOR_LAND(rNP10c11s1) { LABEL("N10_11_1"); signal: rNP10c21/in6; hv: rNP10c31/out11; }
DETECTOR_LAND(rNP10c12s1) { LABEL("N10_12_1"); signal: rNP10c22/in8; hv: rNP10c31/out12; }
DETECTOR_LAND(rNP10c13s1) { LABEL("N10_13_1"); signal: rNP10c22/in2; hv: rNP10c31/out13; }
DETECTOR_LAND(rNP10c14s1) { LABEL("N10_14_1"); signal: rNP10c22/in4; hv: rNP10c31/out14; }
DETECTOR_LAND(rNP10c15s1) { LABEL("N10_15_1"); signal: rNP10c25/in4; hv: rNP10c31/out15; }
DETECTOR_LAND(rNP10c16s1) { LABEL("N10_16_1"); signal: rNP10c25/in2; hv: rNP10c31/out16; }
DETECTOR_LAND(rNP10c17s1) { LABEL("N10_17_1"); signal: rNP10c22/in6; hv: rNP10c32/out1; }
DETECTOR_LAND(rNP10c18s1) { LABEL("N10_18_1"); signal: rNP10c21/in8; hv: rNP10c32/out2; }
DETECTOR_LAND(rNP10c19s1) { LABEL("N10_19_1"); signal: rNP10c26/in4; hv: rNP10c32/out3; }
DETECTOR_LAND(rNP10c20s1) { LABEL("N10_20_1"); signal: rNP10c26/in2; hv: rNP10c32/out4; }
DETECTOR_LAND(rNP10c01s2) { LABEL("N10_01_2"); signal: rNP10c25/in3; hv: rNP10c33/out1; }
DETECTOR_LAND(rNP10c02s2) { LABEL("N10_02_2"); signal: rNP10c25/in1; hv: rNP10c33/out2; }
DETECTOR_LAND(rNP10c03s2) { LABEL("N10_03_2"); signal: rNP10c23/in1; hv: rNP10c33/out3; }
DETECTOR_LAND(rNP10c04s2) { LABEL("N10_04_2"); signal: rNP10c23/in3; hv: rNP10c33/out4; }
DETECTOR_LAND(rNP10c05s2) { LABEL("N10_05_2"); signal: rNP10c26/in3; hv: rNP10c33/out5; }
DETECTOR_LAND(rNP10c06s2) { LABEL("N10_06_2"); signal: rNP10c26/in1; hv: rNP10c33/out6; }
DETECTOR_LAND(rNP10c07s2) { LABEL("N10_07_2"); signal: rNP10c23/in5; hv: rNP10c33/out7; }
DETECTOR_LAND(rNP10c08s2) { LABEL("N10_08_2"); signal: rNP10c23/in7; hv: rNP10c33/out8; }
DETECTOR_LAND(rNP10c09s2) { LABEL("N10_09_2"); signal: rNP10c24/in1; hv: rNP10c33/out9; }
DETECTOR_LAND(rNP10c10s2) { LABEL("N10_10_2"); signal: rNP10c24/in3; hv: rNP10c33/out10; }
DETECTOR_LAND(rNP10c11s2) { LABEL("N10_11_2"); signal: rNP10c24/in5; hv: rNP10c33/out11; }
DETECTOR_LAND(rNP10c12s2) { LABEL("N10_12_2"); signal: rNP10c24/in7; hv: rNP10c33/out12; }
DETECTOR_LAND(rNP10c13s2) { LABEL("N10_13_2"); signal: rNP10c23/in2; hv: rNP10c33/out13; }
DETECTOR_LAND(rNP10c14s2) { LABEL("N10_14_2"); signal: rNP10c23/in4; hv: rNP10c33/out14; }
DETECTOR_LAND(rNP10c15s2) { LABEL("N10_15_2"); signal: rNP10c23/in6; hv: rNP10c33/out15; }
DETECTOR_LAND(rNP10c16s2) { LABEL("N10_16_2"); signal: rNP10c23/in8; hv: rNP10c33/out16; }
DETECTOR_LAND(rNP10c17s2) { LABEL("N10_17_2"); signal: rNP10c24/in2; hv: rNP10c32/out5; }
DETECTOR_LAND(rNP10c18s2) { LABEL("N10_18_2"); signal: rNP10c24/in4; hv: rNP10c32/out6; }
DETECTOR_LAND(rNP10c19s2) { LABEL("N10_19_2"); signal: rNP10c24/in6; hv: rNP10c32/out7; }
DETECTOR_LAND(rNP10c20s2) { LABEL("N10_20_2"); signal: rNP10c24/in8; hv: rNP10c32/out8; }
JOINER_8(rNP10c21)
{
 in1: rNP10c01s1/signal;
 in2: rNP10c09s1/signal;
 in3: rNP10c02s1/signal;
 in4: rNP10c10s1/signal;
 in5: rNP10c03s1/signal;
 in6: rNP10c11s1/signal;
 in7: rNP10c04s1/signal;
 in8:  rNP10c18s1/signal;
 // out1_8: r13c3s10/in1_8;
}
JOINER_8(rNP10c22)
{
 in1: rNP10c05s1/signal;
 in2: rNP10c13s1/signal;
 in3: rNP10c06s1/signal;
 in4: rNP10c14s1/signal;
 in5: rNP10c07s1/signal;
 in6: rNP10c17s1/signal;
 in7: rNP10c08s1/signal;
 in8:  rNP10c12s1/signal;
 // out1_8: r13c3s10/in9_16;
}
JOINER_8(rNP10c23)
{
 in1: rNP10c03s2/signal;
 in2: rNP10c13s2/signal;
 in3: rNP10c04s2/signal;
 in4: rNP10c14s2/signal;
 in5: rNP10c07s2/signal;
 in6: rNP10c15s2/signal;
 in7: rNP10c08s2/signal;
 in8: rNP10c16s2/signal;
 // out1_8: r13c3s11/in1_8;
}
JOINER_8(rNP10c24)
{
 in1: rNP10c09s2/signal;
 in2: rNP10c17s2/signal;
 in3: rNP10c10s2/signal;
 in4: rNP10c18s2/signal;
 in5: rNP10c11s2/signal;
 in6: rNP10c19s2/signal;
 in7: rNP10c12s2/signal;
 in8: rNP10c20s2/signal;
 // out1_8: r13c3s11/in9_16;
}
JOINER_8(rNP10c25)
{
 in4: rNP10c15s1/signal;
 in3: rNP10c01s2/signal;
 in2: rNP10c16s1/signal;
 in1: rNP10c02s2/signal;
 // out1_4: r13c3s9/in1_4;
}
JOINER_8(rNP10c26)
{
 in4: rNP10c19s1/signal;
 in3: rNP10c05s2/signal;
 in2: rNP10c20s1/signal;
 in1: rNP10c06s2/signal;
 // out1_4: r13c3s9/in9_12;
}
HV_CONN_16(rNP10c31)
{
 in1_16: r16c2s7/out0_15;
 out1: rNP10c01s1/hv;
 out2: rNP10c02s1/hv;
 out3: rNP10c03s1/hv;
 out4: rNP10c04s1/hv;
 out5: rNP10c05s1/hv;
 out6: rNP10c06s1/hv;
 out7: rNP10c07s1/hv;
 out8: rNP10c08s1/hv;
 out9: rNP10c09s1/hv;
 out10: rNP10c10s1/hv;
 out11: rNP10c11s1/hv;
 out12: rNP10c12s1/hv;
 out13: rNP10c13s1/hv;
 out14: rNP10c14s1/hv;
 out15: rNP10c15s1/hv;
 out16: rNP10c16s1/hv;
}
HV_CONN_16(rNP10c32)
{
 in1_8: r16c2s6/out8_15;
 out1: rNP10c17s1/hv;
 out2: rNP10c18s1/hv;
 out3: rNP10c19s1/hv;
 out4: rNP10c20s1/hv;
 out5: rNP10c17s2/hv;
 out6: rNP10c18s2/hv;
 out7: rNP10c19s2/hv;
 out8: rNP10c20s2/hv;
}
HV_CONN_16(rNP10c33)
{
 in1_16: r16c2s8/out0_15;
 out1: rNP10c01s2/hv;
 out2: rNP10c02s2/hv;
 out3: rNP10c03s2/hv;
 out4: rNP10c04s2/hv;
 out5: rNP10c05s2/hv;
 out6: rNP10c06s2/hv;
 out7: rNP10c07s2/hv;
 out8: rNP10c08s2/hv;
 out9: rNP10c09s2/hv;
 out10: rNP10c10s2/hv;
 out11: rNP10c11s2/hv;
 out12: rNP10c12s2/hv;
 out13: rNP10c13s2/hv;
 out14: rNP10c14s2/hv;
 out15: rNP10c15s2/hv;
 out16: rNP10c16s2/hv;
}

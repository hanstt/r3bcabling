MODULE(DETECTOR_VETO)
{
  CONNECTOR(signal);
  CONNECTOR(hv);
}


DETECTOR_VETO(rVP1c01s1) { LABEL("V1_01_1"); signal: rVP1c21/in1; hv: rVP1c31/out1;   }
DETECTOR_VETO(rVP1c02s1) { LABEL("V1_02_1"); signal: rVP1c21/in3; hv: rVP1c31/out2;   }
DETECTOR_VETO(rVP1c03s1) { LABEL("V1_03_1"); signal: rVP1c21/in5; hv: rVP1c31/out3;   }
DETECTOR_VETO(rVP1c04s1) { LABEL("V1_04_1"); signal: rVP1c21/in7; hv: rVP1c31/out4;   }
DETECTOR_VETO(rVP1c05s1) { LABEL("V1_05_1"); signal: rVP1c22/in1; hv: rVP1c31/out5;   }
DETECTOR_VETO(rVP1c06s1) { LABEL("V1_06_1"); signal: rVP1c22/in3; hv: rVP1c31/out6;   }
DETECTOR_VETO(rVP1c07s1) { LABEL("V1_07_1"); signal: rVP1c22/in5; hv: rVP1c31/out7;   }
DETECTOR_VETO(rVP1c08s1) { LABEL("V1_08_1"); signal: rVP1c22/in7; hv: rVP1c31/out8;   }
DETECTOR_VETO(rVP1c09s1) { LABEL("V1_09_1"); signal: rVP1c21/in2; hv: rVP1c31/out9;   }
DETECTOR_VETO(rVP1c10s1) { LABEL("V1_10_1"); signal: rVP1c21/in4; hv: rVP1c31/out10;  }
DETECTOR_VETO(rVP1c11s1) { LABEL("V1_11_1"); signal: rVP1c21/in6; hv: rVP1c31/out11;  }
DETECTOR_VETO(rVP1c12s1) { LABEL("V1_12_1"); signal: rVP1c21/in8; hv: rVP1c31/out12;  }
DETECTOR_VETO(rVP1c13s1) { LABEL("V1_13_1"); signal: rVP1c22/in2; hv: rVP1c31/out13;  }
DETECTOR_VETO(rVP1c14s1) { LABEL("V1_14_1"); signal: rVP1c22/in4; hv: rVP1c31/out14;  }
DETECTOR_VETO(rVP1c15s1) { LABEL("V1_15_1"); signal: rVP1c25/in1; hv: rVP1c31/out15;  } 
DETECTOR_VETO(rVP1c16s1) { LABEL("V1_16_1"); signal: rVP1c25/in3; hv: rVP1c31/out16;  } 
DETECTOR_VETO(rVP1c17s1) { LABEL("V1_17_1"); signal: rVP1c22/in6; hv: rVP1c33/out1;   }
DETECTOR_VETO(rVP1c18s1) { LABEL("V1_18_1"); signal: rVP1c22/in8; hv: rVP1c33/out2;   }
DETECTOR_VETO(rVP1c19s1) { LABEL("V1_19_1"); signal: rVP1c25/in5; hv: rVP1c33/out3;   } 
DETECTOR_VETO(rVP1c20s1) { LABEL("V1_20_1"); signal: rVP1c25/in7; hv: rVP1c33/out4;   } 

DETECTOR_VETO(rVP1c01s2) { LABEL("V1_01_2"); signal: rVP1c25/in2; hv: rVP1c32/out1;   } 
DETECTOR_VETO(rVP1c02s2) { LABEL("V1_02_2"); signal: rVP1c25/in4; hv: rVP1c32/out2;   } 
DETECTOR_VETO(rVP1c03s2) { LABEL("V1_03_2"); signal: rVP1c23/in1; hv: rVP1c32/out3;   }
DETECTOR_VETO(rVP1c04s2) { LABEL("V1_04_2"); signal: rVP1c23/in3; hv: rVP1c32/out4;   }
DETECTOR_VETO(rVP1c05s2) { LABEL("V1_05_2"); signal: rVP1c25/in6; hv: rVP1c32/out5;   } 
DETECTOR_VETO(rVP1c06s2) { LABEL("V1_06_2"); signal: rVP1c25/in8; hv: rVP1c32/out6;   } 
DETECTOR_VETO(rVP1c07s2) { LABEL("V1_07_2"); signal: rVP1c23/in5; hv: rVP1c32/out7;   }
DETECTOR_VETO(rVP1c08s2) { LABEL("V1_08_2"); signal: rVP1c23/in7; hv: rVP1c32/out8;   }
DETECTOR_VETO(rVP1c09s2) { LABEL("V1_09_2"); signal: rVP1c24/in1; hv: rVP1c32/out9;   }
DETECTOR_VETO(rVP1c10s2) { LABEL("V1_10_2"); signal: rVP1c24/in3; hv: rVP1c32/out10;  }
DETECTOR_VETO(rVP1c11s2) { LABEL("V1_11_2"); signal: rVP1c24/in5; hv: rVP1c32/out11;  }
DETECTOR_VETO(rVP1c12s2) { LABEL("V1_12_2"); signal: rVP1c24/in7; hv: rVP1c32/out12;  }
DETECTOR_VETO(rVP1c13s2) { LABEL("V1_13_2"); signal: rVP1c23/in2; hv: rVP1c32/out13;  }
DETECTOR_VETO(rVP1c14s2) { LABEL("V1_14_2"); signal: rVP1c23/in4; hv: rVP1c32/out14;  }
DETECTOR_VETO(rVP1c15s2) { LABEL("V1_15_2"); signal: rVP1c23/in6; hv: rVP1c32/out15;  }
DETECTOR_VETO(rVP1c16s2) { LABEL("V1_16_2"); signal: rVP1c23/in8; hv: rVP1c32/out16;  }
DETECTOR_VETO(rVP1c17s2) { LABEL("V1_17_2"); signal: rVP1c24/in2; hv: rVP1c33/out5;   }
DETECTOR_VETO(rVP1c18s2) { LABEL("V1_18_2"); signal: rVP1c24/in4; hv: rVP1c33/out6;   }
DETECTOR_VETO(rVP1c19s2) { LABEL("V1_19_2"); signal: rVP1c24/in6; hv: rVP1c33/out7;   }
DETECTOR_VETO(rVP1c20s2) { LABEL("V1_20_2"); signal: rVP1c24/in8; hv: rVP1c33/out8;   }



JOINER_8(rVP1c21)
{
 in1: rVP1c01s1/signal;
 in2: rVP1c09s1/signal;
 in3: rVP1c02s1/signal;
 in4: rVP1c10s1/signal;
 in5: rVP1c03s1/signal;
 in6: rVP1c11s1/signal;
 in7: rVP1c04s1/signal;
 in8: rVP1c12s1/signal;

 out1_8: r13c1s17/in1_8;
}

JOINER_8(rVP1c22)
{
 in1: rVP1c05s1/signal;
 in2: rVP1c13s1/signal;
 in3: rVP1c06s1/signal;
 in4: rVP1c14s1/signal;
 in5: rVP1c07s1/signal;
 in6: rVP1c17s1/signal;
 in7: rVP1c08s1/signal;
 in8: rVP1c18s1/signal;

 out1_8: r13c1s17/in9_16;
}

JOINER_8(rVP1c23)
{
 in1: rVP1c03s2/signal;
 in2: rVP1c13s2/signal;
 in3: rVP1c04s2/signal;
 in4: rVP1c14s2/signal;
 in5: rVP1c07s2/signal;
 in6: rVP1c15s2/signal;
 in7: rVP1c08s2/signal;
 in8: rVP1c16s2/signal;

 out1_8: r13c1s18/in1_8;
}

JOINER_8(rVP1c24)
{
 in1: rVP1c09s2/signal;
 in2: rVP1c17s2/signal;
 in3: rVP1c10s2/signal;
 in4: rVP1c18s2/signal;
 in5: rVP1c11s2/signal;
 in6: rVP1c19s2/signal;
 in7: rVP1c12s2/signal;
 in8: rVP1c20s2/signal;

 out1_8: r13c1s18/in9_16;
}

JOINER_8(rVP1c25)
{
 in1: rVP1c15s1/signal;
 in2: rVP1c01s2/signal;
 in3: rVP1c16s1/signal;
 in4: rVP1c02s2/signal;
 in5: rVP1c19s1/signal;
 in6: rVP1c05s2/signal;
 in7: rVP1c20s1/signal;
 in8: rVP1c06s2/signal;

 out1_8: r13c1s19u1/in1_8;
}

PRESPLIT_CROSS(r13c1s19u1)
{
 in1_8: rVP1c25/out1_8;
 in1_9: ; // UNCONNECTED

 out1_8:  r13c1s19/in1_8;
 out9_16: r13c1s19/in9_16;
}



HV_CONN_16(rVP1c31)
{
 in1_16: r16c2s9/out0_15;

 out1:  rVP1c01s1/hv;
 out2:  rVP1c02s1/hv;
 out3:  rVP1c03s1/hv;
 out4:  rVP1c04s1/hv;
 out5:  rVP1c05s1/hv;
 out6:  rVP1c06s1/hv;
 out7:  rVP1c07s1/hv;
 out8:  rVP1c08s1/hv;
 out9:  rVP1c09s1/hv;
 out10: rVP1c10s1/hv;
 out11: rVP1c11s1/hv;
 out12: rVP1c12s1/hv;
 out13: rVP1c13s1/hv;
 out14: rVP1c14s1/hv;
 out15: rVP1c15s1/hv;
 out16: rVP1c16s1/hv;
}

HV_CONN_16(rVP1c32)
{
 in1_16: r16c2s10/out0_15;

 out1:  rVP1c01s2/hv;
 out2:  rVP1c02s2/hv;
 out3:  rVP1c03s2/hv;
 out4:  rVP1c04s2/hv;
 out5:  rVP1c05s2/hv;
 out6:  rVP1c06s2/hv;
 out7:  rVP1c07s2/hv;
 out8:  rVP1c08s2/hv;
 out9:  rVP1c09s2/hv;
 out10: rVP1c10s2/hv;
 out11: rVP1c11s2/hv;
 out12: rVP1c12s2/hv;
 out13: rVP1c13s2/hv;
 out14: rVP1c14s2/hv;
 out15: rVP1c15s2/hv;
 out16: rVP1c16s2/hv;
}

HV_CONN_16(rVP1c33)
{
 in1_8: r16c2s12/out0_7;

 out1:  rVP1c17s1/hv;
 out2:  rVP1c18s1/hv;
 out3:  rVP1c19s1/hv;
 out4:  rVP1c20s1/hv;
 out5:  rVP1c17s2/hv;
 out6:  rVP1c18s2/hv;
 out7:  rVP1c19s2/hv;
 out8:  rVP1c20s2/hv;
}



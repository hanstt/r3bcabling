/*
 * Splitter 2 (upper).
 */

/********************************************
 * LAND
 */

SPLIT_BOX(r13c3s1)
{
 in1_8:  "53" <- , rNP6c23/out1_8;
 in9_16: "54" <- , rNP6c24/out1_8;

 e1_16:  "N62" -> , r13c2s18/in48_63;

 t1_8:   "N63" -> , r12c4s1/in1_8;
 t9_16:  "N64" -> , r12c4s2/in1_8;
}

SPLIT_BOX(r13c3s2)
{
 in1_8:  "41" <- , rNP7c21/out1_8;
 in9_16: "42" <- , rNP7c22/out1_8;

 e1_16:  "N71" -> , r13c2s18/in64_79;

 t1_8:   "N71" -> , r12c4s3/in1_8;
 t9_16:  "N72" -> , r12c4s4/in1_8;
}

SPLIT_BOX(r13c3s3)
{
 in1_8:  "43" <- , rNP7c23/out1_8;
 in9_16: "44" <- , rNP7c24/out1_8;

 e1_16:  "N72" -> , r13c2s18/in80_95;

 t1_8:   "N73" -> , r12c4s5/in1_8;
 t9_16:  "N74" -> , r12c4s6/in1_8;
}

SPLIT_BOX(r13c3s4)
{
 in1_4:   "45" <- , rNP8c25/out1_4;
 in5_8:   "45" <- , rNP7c25/out5_8;
 in9_12:  "35" <- , rNP8c26/out1_4;
 in13_16: "35" <- , rNP7c26/out5_8;

 e1_16:  "N73" -> , r13c2s20/in48_63;

 t1_8:   "N75" -> , r12c4s7/in1_8;
 t9_16:  "N85" -> , r12c4s8/in1_8;
}

SPLIT_BOX(r13c3s5)
{
 in1_8:  "31" <- , rNP8c21/out1_8;
 in9_16: "32" <- , rNP8c22/out1_8;

 e1_16:  "N81" -> , r13c2s20/in64_79;

 t1_8:   "N81" -> , r12c4s9/in1_8;
 t9_16:  "yellow" -> , r12c4s10/in1_8;
}

SPLIT_BOX(r13c3s6)
{
 in1_8:  "33" <- , rNP8c23/out1_8;
 in9_16: "34" <- , rNP8c24/out1_8;

 e1_16:  "N82" -> , r13c2s20/in80_95;

 t1_8:   "N83" -> , r12c4s11/in1_8;
 t9_16:  "N84" -> , r12c4s12/in1_8;
}

SPLIT_BOX(r13c3s7)
{
 in1_8:  "21" <- , rNP9c21/out1_8;
 in9_16: "22" <- , rNP9c22/out1_8;

 e1_16:  "N91" -> , r13c2s16/in48_63;

 t1_8:   "N91" -> , r12c4s13/in1_8;
 t9_16:  "N92" -> , r12c4s14/in1_8;
}

SPLIT_BOX(r13c3s8)
{
 in1_8:  "23" <- , rNP9c23/out1_8;
 in9_16: "23" <- , rNP9c24/out1_8; // TODO: check label (probably "24")   

 e1_16:  "N93" -> , r13c2s16/in64_79; // TODO: check name

 t1_8:   "N93" -> , r12c5s7/in1_8;
 t9_16:  "N94" -> , r12c5s8/in1_8;
}

SPLIT_BOX(r13c3s9)
{
 in1_4:   ; // "25" <- , rNP10c25/out1_4;
 in5_8:   ; // "25" <- , rNP9c25/out5_8;
 in9_12:  ; // "15" <- , rNP10c26/out1_4;
 in13_16: "15" <- , rNP9c26/out5_8;

 e1_16:  "N93" -> , r13c2s16/in80_95;

 t1_8:   "N95" -> , r12c5s9/in1_8;
 t9_16:  "N105"-> , r12c5s10/in1_8;
}

SPLIT_BOX(r13c3s10)
{
  //in1_4:   "11" <- , rNP9c25/out1_4;
 in1_4:   "11" <- , rNP9c25/out5_8;

 // in1_8:  ; // "11" <- , rNP10c21/out1_8;
 //in9_16: ; // "12" <- , rNP10c22/out1_8;

 e1_16:  "N101"-> , r13c2s15/in64_79;

 t1_8:   "N101"-> , r12c5s11/in1_8;
 t9_16:  "N102"-> , r12c5s12/in1_8;
}

SPLIT_BOX(r13c3s11)
{
 in1_8:  ; // "13" <- , rNP10c23/out1_8;
 in9_16: ; // "14" <- , rNP10c24/out1_8;

 e1_16:  "N102"-> , r13c2s15/in80_95;

 t1_8:   "N103"-> , r12c5s13/in1_8;
 t9_16:  "N104"-> , r12c5s14/in1_8;
}

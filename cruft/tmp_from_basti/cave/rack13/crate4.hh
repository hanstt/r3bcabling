/* 
 * Crate 4 (below fastbus). Basically written from the scratch, so all connected modules will have to be updated as well.
 */

NIM_CRATE(r13c4)
{
	;
}

//////////////////////////////////////////////////////////////////////////////////

LECROY429A(r13c4s1) // FIFO "2x8"
{
	SERIAL("41094");	// invent.no. 3650
	LABEL("TCAL");
}

LECROY429A_8_U(r13c4s1u1)
{
 in1:	r13c11/front21;	

 out1:	r12c5s23/in;	// goes to the LiFO Amp input
 out2:	r12c4s23/in;
 out3:	r12c3s23/in;	// should go to r12c3s23/in, but not connected...
 out4:	r12c2s23/in;
 out5:	r14c7s20/in;
 out6:  r11c2s12/in1; 
 out7:  r14c6s23/in;
 out8:  r13c2s0/in1; 
}

//////////////////////////////////////////////////////////////////////////////////

EC1610(r13c4s5) // 16ch. NIM-ECL Converter
	// will be used next year for sending triggers to the SIDEREMS, Driftchambers, GFI,...
{
	;
}

//////////////////////////////////////////////////////////////////////////////////

CLOCK_MSE(r13c4s7) // Clock Generator
{
 out_1MHz:	.s9u1/in_ttl;
 out_100kHz:	.s9u3/in_ttl;
 out_10Hz:	.s9u6/in_ttl;
}

//////////////////////////////////////////////////////////////////////////////////

LA8000(r13c4s9)	// Level Adapter
{
	;	// invent.no. 3682
}

LA8000_U(r13c4s9u1)
{
 in_ttl:	.s7/out_1MHz;

 out_nim:	.s11u1/in1;
}

LA8000_U(r13c4s9u3)
{
 in_ttl:	.s7/out_100kHz;
}

LA8000_U(r13c4s9u6)
{
 in_ttl:	.s7/out_10Hz;

 out_nim:	.s11u2/in1;
}

LA8000_U(r13c4s9u8)
{
	;
}

//////////////////////////////////////////////////////////////////////////////////

LF4000(r13c4s11) // FIFO "4x4"
{
	;
}

LF4000_4_U(r13c4s11u1)
{
 in1:		.s9u1/out_nim;

 out1:		.s13/in1;
 out2:		r14c9s21/in7;
 out4:		.s13/in2;
}

LF4000_4_U(r13c4s11u2)
{
 in1:		.s9u6/out_nim;

 out2:		r14c9s21/in6;
}

LF4000_4_U(r13c4s11u3)
{
	;
}

LF4000_4_U(r13c4s11u4)
{
  ;
}

//////////////////////////////////////////////////////////////////////////////////

EC1601(r13c4s13) // NIM-ECL-NIM Converter
{
 in1:		.s11u1/out1;
 in2:		.s11u1/out4;
 in3_8:		.s13/out1_6;
 in9:		.s13/out8;
 in10:		.s13/out7;
 in11_16:	.s13/out9_14;

 out1_6:	.s13/in3_8;
 out7:		.s13/in10;
 out8:		.s13/in9;
 out9_14:	.s13/in11_16;
}

//////////////////////////////////////////////////////////////////////////////////

LF4002(r13c4s17) // FIFO "1x16".
{
	LABEL("OR_TDET");
}

LF4002_16_U(r13c4s17u1)
{
 in1_4:		r14c10s10_13/or;
 in5:		r14c8s5/or;
 in6_14:	r14c10s15_23/or;
 in16:		r14c6s16u1/out8; // 2x8 FIFO

 out1:		r14c9s21/in2; // Nim-Ecl-Nim
 out16:		r13c11/front6; // OR_TDET @ PatchPanel
}


//////////////////////////////////////////////////////////////////////////////////

// Phillips Octal Linear Fan Out 748 SERIAL("15783")

PHILLIPS748(r13c4s19)
{
	LABEL("Multiplicity");
	SERIAL("15783");
}

PHILLIPS748_U(r13c4s19u1)
{
 in:		r12c11s9/analog_out;

 out1:		r14c8s19/in1;
 out2:		.c11/front46; //MLAND @ lower right of PP
 out3:		.c11/front1; //MLAND @ upper left of PP
}

PHILLIPS748_U(r13c4s19u2)
{
 out1:		r14c8s19/in2;
}

PHILLIPS748_U(r13c4s19u3)
{
 in:		r14c9s1/analog_out;

 out1:		r14c8s19/in3;
 out4:		r13c11/front3; //MTOF @ PP
}

PHILLIPS748_U(r13c4s19u4)
{
 out1:		r14c8s19/in4;
}

PHILLIPS748_U(r13c4s19u5)
{
 out1:		r14c8s19/in5;	
}

PHILLIPS748_U(r13c4s19u6)
{
 out1:		r14c8s19/in6; 
}

PHILLIPS748_U(r13c4s19u7)
{
 in:		.s21/analog_out;

 out2:		.c11/front2; //MVETO @ upper left of PP
}

//////////////////////////////////////////////////////////////////////////////////

SU1200(r13c4s21)
{
	SETTING("SELECT"=>"1-6");
	SETTING("POLARITY"=>"NEG");

	LABEL("M_VETO");

 in1_5:		r14c7s2_6/m;

 analog_out:	.s19u7/in;
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

/*The code below comes originally from cabling/cave/crate4nim.hh
 *
 *
 *
 * 
 NIM_CRATE(r13c4)
 {
 ;  
 }

 SU1200(r13c4s21)
 {
 LABEL("M_VETO");

in1:    r14c7s2/m;
in2:    r14c7s3/m;
in3:    r14c7s4/m;
in4:    r14c7s5/m;
in5:    r14c7s6/m;

}

LF8000(r13c4s23) // or is it LF4000
{
;
}

LF8000_16_U(r13c4s23u1)
{
LABEL("TDET_OR");

// TODO: in15: is input from r14c6s16u1 connected here???

in1_14: r14c10s10_23/or;
}
 *
 *
 *
 * 
 */

/*
 * Splitter 1 (upper).
 */

/********************************************
 * TFW / VETO?
 */

SPLIT_BOX(r13c1s15)
{
  in1_8:  r26c7/out1_8;
  in9_16: r26c8/out1_8;
 //in1_8:  rNTFRc3/out1_8;
 //in9_16: rNTFRc4/out1_8;
 
 e1_16:  "TOFWALL" -> , r13c2s13/in48_63;
 
 t1_8:   "CB1" -> , r14c6s1/in1_8;
 t9_16:  "CB2" -> , r14c6s2/in1_8;
}

SPLIT_BOX(r13c1s16)
{
 in1_8:  r26c5/out1_8;
 in9_16: r26c6/out1_8;

 e1_16:  "TFW8" -> , r13c2s13/in80_95;

 t1_8:   ""    -> , r14c6s4/in1_8;
 t9_16:  "CB2" -> , r14c6s3/in1_8; // c4s3 after c4s4??
}

SPLIT_BOX(r13c1s17)
{
 in1_8:  rVP1c21/out1_8; // "V11" <- ,
 in9_16: rVP1c22/out1_8; // "V12" <- ,

 e1_16:  "V11+V12" -> , r13c2s15/in48_63;

 t1_8:   "V11" -> , r14c7s2/in1_8;
 t9_16:  "V12" -> , r14c7s3/in1_8;
}

SPLIT_BOX(r13c1s18)
{
 in1_8:  rVP1c23/out1_8; // "V13" <- ,
 in9_16: rVP1c24/out1_8; // "V14" <- ,

 e1_16:  "V12" -> , r13c2s11/in32_47;

 t1_8:   "V13" -> , r14c7s4/in1_8;
 t9_16:  "V14" -> , r14c7s5/in1_8;
}

SPLIT_BOX(r13c1s19)
{
 in1_8:  r13c1s19u1/out1_8; // "V15" <- ,... (not quite, a splitter before)
 in9_16: r13c1s19u1/out9_16; // 

 e1_16:  "V13" -> , r13c2s13/in0_15;

 t1_8:   "V15" -> , r14c7s6/in1_8;
 t9_16:  "V25" -> , r14c7s7/in1_8;
}

SPLIT_BOX(r13c1s20)
{
 in1_8:  r26c3/out1_8;
 in9_16: r26c4/out1_8;

 e1_16:  "Yellow" -> , r13c2s13/in16_31;

 t1_8:   "V21" -> , r14c7s8/in1_8;
 t9_16:  "V22" -> , r14c7s9/in1_8;
}

SPLIT_BOX(r13c1s21)
{
 in1_8:  r26c1/out1_8;
 in9_16: r26c2/out1_8;
 //in1_8:  rNTFRc1/out1_8;
 //in9_16: rNTFRc2/out1_8;

 e1_16:  "V22" -> , r13c2s13/in32_47;

 t1_8:   "V23" -> , r14c7s10/in1_8;
 t9_16:  "V24" -> , r14c7s11/in1_8;
}


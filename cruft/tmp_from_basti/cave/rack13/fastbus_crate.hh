/*
 * Fastbus crate 1 in Cave C (right).
 */

FASTBUS_CRATE(r13c2)
{
  ;
}

NGF(r13c2s0)
{
 in1: r13c4s1u1/out8;
 out3: r14c9s13u2/in2; //You forgot this one boys!
}

// This is not really correct, but as it is hardly used, lets pretend
// the NGF is in slot 0, and what is inside the NGF is in the following
// fastbus slots

TRIVA3(r13c2s1)
{
  ;
 //in1_8:  r14c9s23/out_ecl1_8;
 //out1_8: r14c9s23/in_ecl9_16;
 out5:   r12c11s15/in_ecl14;// DT
}

RIO3(r13c2s2)
{
  SETTING("HOSTNAME"=>"r2-6");
}

LECROY1875(r13c2s3)
{
  SERIAL("L_T81855");

 in0_15:  "N72 S21/3" <- , r14c6s11/outa1_16;
 in16_23: r14c6s20/outa1_8;
 in24_31: "N71 S20/3" <- , r14c6s12/outa9_16;
 in32_47: "Slot 6-2"  <- , r14c7s15/outa1_16;
 in48_63: "S20/4"     <- , r14c7s16/outa1_16;
}

LECROY1875(r13c2s4)
{
  SERIAL("L_T87044");
/* NEW DTF INPUT , CL @ 10/08/2010 */  
// in0_15:  "Slot 4-1" <-      , r14c10s1/outb1_16;
 in0_15:  "Slot 4-1" <-      , r14c6s14/outa1_16;
/////////////////////////////////////
 in16_31: "Slot 4-2" <-      , r14c10s1/outb1_16;
 in32_47: "Slot 4-3" <-      , r14c10s2/outb1_16;

 in48_63:                      r14c7s13/outa1_16;
  //in48_63: "Slot 4-4" <-     
}

LECROY1875(r13c2s5)
{
// CL, 05/08/2010: NOT IN THE DAQ anymore --> old XB channels...  
// DR, 23/08/2010: PUT BACK INTO DAQ FOR LAND VETO 
 SERIAL("L_T87024");
 in0_15:  "Slot 5-1" <-      , r14c6s13/outa1_16;
// in16_31: "Slot 5-2" <-      , r14c6s14/outa1_16;
 in32_47: "Slot 5-3" <-      , r14c6s15/outa1_16;
 in48_63:                      r14c7s14/outa1_16;
  //in48_63: "Slot 5-4" <-     

}

LECROY1875(r13c2s6)
{ 
// CL, 05/08/2010: NOT IN THE DAQ anymore --> old XB channels...  
  SERIAL("L_T81800");
// in0_15:  "Slot 6-1" <-      , r14c10s4/outb1_16;   // r14c6s11/outa1_16;                  
// in16_31: "Slot 6-2" <-      , r14c10s5/outb1_16;
 in32_47: "Slot 6-3" <-      , r14c10s6/outb1_16;      
 in48_63: "Slot 6-4" <-      , r14c10s7/outb1_16;      

}

LECROY1885F(r13c2s7)
{
  SERIAL("L_Q47342");

  //in0_15:  "FB IN 7 0-15"  <- , r13c3s18/e1_16;
 in16_31: "FB IN 7 16-31" <- , r13c3s20/e1_16; //NTF
 in32_47: "FB IN 7 32-47" <- , r13c3s19/e1_16; //NTF
 in48_63: "FB IN 7 48-63" <- , r13c3s21/e1_16; //DTF
 in64_79: "FB IN 7 80-95" <- , r13c3s17/e1_16;
 //in80_95: ;
 gate:	  r14c9s7u1/out_ecl5;
}

LECROY1885F(r13c2s8)
{
  SERIAL("L_Q11111"); // TODO: figure this out

  // Yellow tape labels

 in0_15:   <- , r13c3s12/e1_16;
 in16_31:  <- , r13c3s13/e1_16;
 in32_47:  <- , r13c3s14/e1_16;
 in48_63:  <- , r13c3s15/e1_16;
 in64_79:  <- , r13c3s16/e1_16;
 //in80_95: ;
 gate:	  r14c9s7u1/out_ecl4;
}

LECROY1885F(r13c2s10)
{
  SERIAL("L_Q46848");

  // GFI 1

#if USING_GFI
 in0_7:     r51c21/out1_8;
 in8_15:    r51c22/out1_8;

 in16_23:   r51c23/out1_8;
 in24_31:   r51c24/out1_8;

 in32_37:   r51c25/out1_6;
#endif

 in48_63:   r13c3s18/e1_16; // CV

 gate:	  r14c9s7u1/out_ecl2;
}

// Moved from slot 14
LECROY1885F(r13c2s11)
{
  SERIAL("L_Q47315");

 in0_15:  "N53 (N55+N65) S14/1" <- , r13c1s13/e1_16;
 in16_31: "N61 (N61+N62) S14/2" <- , r13c1s14/e1_16;
 in32_47: "V12 (N13+N14) S14/3" <- , r13c1s18/e1_16;
 //in48_63: ;
 //in64_79: ;
 //in80_95: ;
 gate:	  r14c9s7u1/out_ecl6;
}

LECROY1885F(r13c2s13)
{
  SERIAL("L_Q17320");

 in0_15:  "V13 (V15+V25) S13/1" <- , r13c1s19/e1_16;
 in16_31: "yellow"              <- , r13c1s20/e1_16;
 in32_47: "V22 (V23+V24) S13/3" <- , r13c1s21/e1_16;

 in48_63: "TOFWALL" <- , r13c1s15/e1_16;
 //in64_79: "TFW 8"   <- , r13c1s16/e1_16; 
 in80_95: "TFW 8"   <- , r13c1s16/e1_16; 
}

// Slot 14 empty

LECROY1885F(r13c2s15)
{
  SERIAL("L_Q15372");

 in0_15:  "N42"  <- , r13c1s10/e1_16;
 in16_31: "N51"  <- , r13c1s11/e1_16;
 in32_47: "N52"  <- , r13c1s12/e1_16;
 in48_63: "V11"  <- , r13c1s17/e1_16;
 in64_79: "N101" <- , r13c3s10/e1_16;
 in80_95: "N102" <- , r13c3s11/e1_16;
  ;
}

LECROY1885F(r13c2s16)
{
  SERIAL("L_Q46962");

 in0_15:  "N32" <- , r13c1s7/e1_16;
 in16_31: "N33" <- , r13c1s8/e1_16;
 in32_47: "N41" <- , r13c1s9/e1_16;
 in48_63: "N91" <- , r13c3s7/e1_16;
 in64_79: "N92" <- , r13c3s8/e1_16;
 in80_95: "N93" <- , r13c3s9/e1_16;
}

LECROY1885F(r13c2s18)
{
  SERIAL("L_Q46993");

 in0_15:  "N11" <- , r13c1s1/e1_16;
 in16_31: "N12" <- , r13c1s2/e1_16;
 in32_47: "N13" <- , r13c1s3/e1_16;
 in48_63: "N62" <- , r13c3s1/e1_16;
 in64_79: "N71" <- , r13c3s2/e1_16;
 in80_95: "N72" <- , r13c3s3/e1_16;
}

LECROY1885F(r13c2s20)
{
  SERIAL("L_Q15358");

 in0_15:  "N21" <- , r13c1s4/e1_16;
 in16_31: "N22" <- , r13c1s5/e1_16;
 in32_47: "N31" <- , r13c1s6/e1_16;
 in48_63: "N73" <- , r13c3s4/e1_16;
 in64_79: "N81" <- , r13c3s5/e1_16;
 in80_95: "N82" <- , r13c3s6/e1_16;
}

LECROY1885F(r13c2s23)
{
  SERIAL("L_Q47264"); // TODO: check

  // GFI 2 & 3

#if USING_GFI
 in0_7:     r53c21/out1_8; // GFI 2
 in8_15:    r53c22/out1_8;
 in16_23:   r53c23/out1_8;
// in24_31:   r53c24/out1_8;
  in24_29:   r53c24/out1_6;
//	in30: broken     
	in31:     r53c24/out8;
// in32_39:   r53c25/out1_8; // GFI 3
// in40_47:   r53c26/out1_8;
// in48_55:   r53c27/out1_8;
// in56_63:   r53c28/out1_8;

 in64_69:   r53c29/out1_6;
 in72   :   r53c30/out1;
#endif

 in91: BROKEN; // shows bad results with ngf/util/fbtest

 gate:	  r14c9s7u1/out_ecl3;
}

// CAT module
LECROY1810(r13c2s25)
{
 tdc_stop: r14c9s13u3/out4;
 adc_gate: r14c9s13u1/out1; // aka ARM
}





/*
 * Signals handled by FASTBUS.  (worst case)
 *
 * LAND, 10 planes         => 400 ch : t, e
 * VETO, 1  plane          => 40  ch : t, e
 * TFW,  2  planes (14+18) => 64  ch : t, e
 * GFI,  3  detectors      => 102 ch :    e
 * TDET, 160 ch (156 CsI)  => 160 ch : t, e
 * 
 * LAND 400 t => 6 * 64 + 16
 * LAND 400 e =>              4 * 96 + 16
 *
 * VETO 40  t =>          40
 * VETO 40  e =>                       40
 *
 * TFW  64  t => 1 * 64
 * TFW  64  e =>                       64
 *
 * GFI  102 e =>              1 * 96 + 6
 *
 * TDET 160 t => 2 * 64 + 32
 * TDET 160 e =>              1 * 96 + 64
 *
 * All (times) have common stop, so times may be put
 * in any TDC (all 50 ps/ch).  This is
 * minimum (6+1+2)*64+16+40+32 => 10*64+24, i.e. 11 modules, 40 spare channels
 *
 * For the energies, there are: TDET gate, GFI gate, CAVE gate (LAND+VETO+TFW)
 *
 * Meaning that energies must be in three sets of QDCs:
 *
 * LAND+VETO+TFW: 4*96+16+40+64 => 6 modules, 72 spare channels
 * TDET:                           2 modules, 32 spare channels
 * GFI:                            2 modules, 90 spare channels
 *
 * In total: 11 TDC, 10 QDC
 *
 * Current allocation:
 *
 * slot4:  TDC: 
 * slot5:  TDC: 
 * slot6:  TDC: 
 * slot7:  QDC: 
 * slot8:  QDC: 
 * slot9:  QDC: 
 * slot10: QDC: 
 * slot11:  
 * slot12: TDC: 
 * slot13: QDC: 
 * slot14: QDC: 
 * slot15: QDC: LAND
 * slot16: QDC: LAND
 * slot17: QDC: LAND
 * slot18: QDC: LAND
 * slot19: TDC: 
 * slot20: TDC: 
 * slot21: TDC: 
 * slot22: TDC: 
 * slot23: TDC: 
 * slot24: TDC: 
 * slot25: TDC: 
 *
 */

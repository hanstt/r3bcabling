/*
 * Splitter 2 (upper). Updated by Sean 16/08/06
 */

/********************************************
 * Gamma detector
 */

SPLIT_BOX(r13c3s12)
{
#if USING_XB && !USE_NEW_XB_CABLING
 in1_8:  r21c1/out1_8;
 in9_16: r21c7/out1_8;        
#endif
#if USING_CSI
 in1_8:  r29c8/out1_8;
 in9_16: r29c9/out1_8;
#endif

 e1_16:             r13c2s8/in0_15;

 t1_4:              r15c18s3u1_4/in;
 t5:  	            r15c18s7u3/in;
 t6:	              r15c18s3u6/in;
 t7_8:              r15c18s7u1_2/in;
 t9_12:             r15c15s7u3_6/in;
 t13_16:            r15c15s15u1_4/in;
}

SPLIT_BOX(r13c3s13)
{
#if USING_XB && !USE_NEW_XB_CABLING
 in1_8:  r21c2/out1_8;       
 in9_16: r21c6/out1_8;      
#endif
#if USING_CSI
 in1_8:  r29c6/out1_8;
 in9_16: r29c7/out1_8;
#endif

 e1_16:             r13c2s8/in16_31;

 t1:                r15c14s7u3/in;
 t2:                r15c13s13u5/in; // r15c14s7u5 is broken
 t3_4:              r15c14s7u5_6/in;
 t5_8:              r15c14s15u1_4/in;
 t9_10:             r15c14s15u5_6/in;
 t11_16:            r15c14s19u1_6/in;
}

SPLIT_BOX(r13c3s14)
{
#if USING_XB && !USE_NEW_XB_CABLING
 in1_8:  r21c3/out1_8;
 in9_16: r21c5/out1_8;        
#endif
#if USING_CSI
 in1_8:  r29c4/out1_8;
 in9_16: r29c5/out1_8;
#endif

 e1_16:             r13c2s8/in32_47;

 t1_6:              r15c16s3u1_6/in;
 t7_8:              r15c16s7u1_2/in;
 t9_12:             r15c16s7u3_6/in;
 t13_16:            r15c16s19u1_4/in;
}

SPLIT_BOX(r13c3s15)
{
#if USING_XB && !USE_NEW_XB_CABLING
 in1_8:    r21c4/out1_8;      
 in9_16:   r21c11/out1_8;      
#endif
#if USING_CSI
 in1_8:  r29c2/out1_8;
 in9_16: r29c3/out1_8;
#endif

 e1_16:             r13c2s8/in48_63;

 t1_6:              r15c13s1u1_6/in;
 t7_8:              r15c13s5u1_2/in;
 t9_12:             r15c13s5u3_6/in;
 t13_16:            r15c13s13u1_4/in;
}

SPLIT_BOX(r13c3s16)
{
#if USING_XB && !USE_NEW_XB_CABLING
 in1_8:  r21c9/out1_8;          
 in9_16: r21c18/out1_8;         
#endif
#if USING_CSI
 in1_8:  r28c9/out1_8;
 in9_16: r29c1/out1_8;
#endif

 e1_16:             r13c2s8/in64_79;

 t1_2:              r15c15s3u1_2/in; // r15c15s3u3 is broken
 t3_6:              r15c15s3u5_8/in; // r15c15s3u4 is spare
 t7_8:              r15c15s7u1_2/in;
 t9_14:             r15c14s3u1_6/in;
 t15_16:            r15c14s7u1_2/in;
}

SPLIT_BOX(r13c3s17)
{
#if USING_XB && !USE_NEW_XB_CABLING
 in1_8:  r21c8/out1_8;       
 in9_16: r21c15/out1_8;      
#endif
#if USING_CSI
 in1_8:  r28c7/out1_8;
 in9_16: r28c8/out1_8;
#endif

 e1_16:             r13c2s7/in64_79;

 t1_6:              r15c12s3u1_6/in;
 t7_8:              r15c12s7u1_2/in;
 t9_12:             r15c12s7u3_6/in;
 t13_16:            r15c12s15u1_4/in;
}

SPLIT_BOX(r13c3s18)                   
{                                    
#if USING_XB && !USE_NEW_XB_CABLING
 in1_8:  r21c17/out1_8;    
 in9_16: r21c10/out1_8;   
#endif
#if USING_CV
 in1_8:  rBEAMR1c3/out1_8;
 in10_13: rBEAMR1c3/out9_12;
#endif

 e1_16:             r13c2s10/in48_63;

#if USING_XB
 t1:                r15c18s15u1/in;
 t2:                ;// r15c18s19u3/in; //cry73->cha77
 t3:                r15c18s15u3/in;
 t4:                r15c18s15u4/in;
 t5:                r15c18s15u5/in;
 t6:                r15c18s15u6/in;
 t7:                r15c18s19u1/in;
 t8:                r15c18s19u2/in;
 t9:                r15c18s15u2/in;
 t10:               r15c18s19u3/in; // cha77 used by cry73
 t11:               r15c18s19u4/in;
 t12:               r15c18s7u4/in;
 t13:               r15c18s19u6/in;
 t14:               r15c18s7u6/in;
 t15:               r15c18s19u5/in;
 t16:               r15c18s3u5/in;// found this place for #80.

/* t1_4:              r15c18s7u3_6/in;
 t5_8:              r15c18s15u1_4/in;
 t9_10:             r15c18s15u5_6/in;
 t11_16:            r15c18s19u1_6/in;*/
#endif
#if USING_CV
 t1:                r15c18s15u1/in;
 t3:                r15c18s15u2/in;
 t4:                r15c18s15u3/in;
 t5:                r15c18s15u4/in;
 t6:                r15c18s15u5/in;
 t7:                r15c18s15u6/in;
 t8:                r15c18s19u1/in;
 t9:                r15c18s19u2/in;
 t10:               r15c18s19u3/in;
 t11:               r15c18s19u4/in;
 t13:               r15c18s19u5/in;
 t15:               r15c18s19u6/in;
#endif
}

SPLIT_BOX(r13c3s19)
{
// NEW: NTF, CL@12/08/2010

/*#if USING_XB && !USE_NEW_XB_CABLING
 in1_8:  r21c12/out1_8;      
 in9_16: r21c16/out1_8;
#endif
#if USING_CSI
 in1_8:  r28c5/out1_8;
 in9_16: r28c6/out1_8;
#endif

 e1_16:             r13c2s7/in16_31;

 t1:	              r15c17s3u1/in;
 t2:  	            r15c18s7u5/in;
 t3_6:              r15c17s3u3_6/in;
 t7_8:              r15c17s7u1_2/in;
 t9_12:             r15c17s7u3_6/in;
 t13_16:            r15c17s15u1_4/in;
*/

   in1_8:             rNTFRc3/out1_8;
   in9_16:            rNTFRc4/out1_8;
   
   t1_8:	      r14c10s17/in1_8;
   t9_16:	      r14c10s18/in1_8;
   
   e1_16:             r13c2s7/in32_47;

}

SPLIT_BOX(r13c3s20)
{
// NEW: NTF, CL@12/08/2010

/*
#if USING_XB && !USE_NEW_XB_CABLING
 in1_8:  r21c19/out1_8;   
 in9_16: r21c14/out1_8; 
#endif
#if USING_CSI
 in1_8:  r28c3/out1_8;
 in9_16: r28c4/out1_8;
#endif

 e1_16:             r13c2s7/in32_47;

 t1_2:              r15c16s19u5_6/in;
 t3_8:              r15c16s23u1_6/in;
 t9_10:             r15c17s15u5_6/in;
 t11_16:            r15c17s19u1_6/in;
*/
 
   in1_8:             rNTFRc1/out1_8;
   in9_16:            rNTFRc2/out1_8;

   t1_8:	      r14c10s15/in1_8;
   t9_16:	      r14c10s13/in1_8;
  
   e1_16:             r13c2s7/in16_31;

}

SPLIT_BOX(r13c3s21)
{
/* NEW USE: DTF
 * NO XB ANY MORE!!!!
 * C Langer, 05/08/2010
 */
  
/*  
#if USING_XB && !USE_NEW_XB_CABLING
 in1_8:  r21c13/out1_8;   
 in9_16: r21c20/out1_8;
#endif
#if USING_CSI
 in1_8:  r28c1/out1_8;
 in9_16: r28c2/out1_8;
#endif

 e1_16:             r13c2s7/in48_63;

 t1_2:              r15c12s15u5_6/in;
 t3_8:              r15c12s19u1_6/in;
 t9_16:             r15c13s17u1_8/in;
*/
 
 in1_8:		    rDTFRc1/out1_8;   
 in9_16:	    rDTFRc2/out1_8;

/*
 t1:		    r14c6s7/in1;
 t2:		    r14c6s7/in3;
 t3:		    r14c6s7/in5;
 t4:		    r14c6s7/in7;
 t5:		    r14c6s8/in1;
 t6:		    r14c6s8/in3;
 t7:		    r14c6s8/in5;
 t8:		    r14c6s8/in7;
 t9:		    r14c6s7/in2;
 t10:		    r14c6s7/in4;
 t11:		    r14c6s7/in6;
 t12:		    r14c6s7/in8;
 t13:		    r14c6s8/in2;
 t14:		    r14c6s8/in4;
 t15:		    r14c6s8/in6;
 t16:		    r14c6s8/in8;
*/

 t1_8:		    r14c6s7/in1_8;
 t9_16:		    r14c6s8/in1_8;

 e1_16:             r13c2s7/in48_63;

// t1_8:		    r14c6s7		    
}

/*
 * Splitter 1 (upper).
 */

/********************************************
 * LAND
 */

SPLIT_BOX(r13c1s1)
{
 in1_8:  "N101" <- , rNP1c21/out1_8;
 in9_16: "N102" <- , rNP1c22/out1_8;

 e1_16:  "N11" -> , r13c2s18/in0_15;

 t1_8:   "N11" -> , r12c2s1/in1_8;
 t9_16:  "N12" -> , r12c2s2/in1_8;
}

SPLIT_BOX(r13c1s2)
{
 in1_8:  "N103" <- , rNP1c23/out1_8;
 in9_16: "N104" <- , rNP1c24/out1_8;

 e1_16:  "N12" -> , r13c2s18/in16_31;

 t1_8:   "N13" -> , r12c2s3/in1_8;
 t9_16:  "N14" -> , r12c2s4/in1_8;
}

SPLIT_BOX(r13c1s3)
{
 in1_4:   "N105" <- , rNP2c25/out1_4;
 in5_8:   "N105" <- , rNP1c25/out5_8;
 in9_12:  "N95" <- , rNP2c26/out1_4;
 in13_16: "N95" <- , rNP1c26/out5_8;

 e1_16:  "N13" -> , r13c2s18/in32_47;

 t1_8:   "N15" -> , r12c2s5/in1_8;
 t9_16:  "N25" -> , r12c2s6/in1_8;
}

SPLIT_BOX(r13c1s4)
{
 in1_8:  "N91" <- , rNP2c21/out1_8;
 in9_16: "N92" <- , rNP2c22/out1_8;

 e1_16:  "N21" -> , r13c2s20/in0_15;

 t1_8:   "N21" -> , r12c2s7/in1_8;
 t9_16:  "N22" -> , r12c2s8/in1_8;
}

SPLIT_BOX(r13c1s5)
{
 in1_8:  "N93" <- , rNP2c23/out1_8;
 in9_16: "N94" <- , rNP2c24/out1_8;

 e1_16:  "N22" -> , r13c2s20/in16_31;

 t1_8:   "N23" -> , r12c2s9/in1_8;
 t9_16:  "N24" -> , r12c2s10/in1_8;
}

SPLIT_BOX(r13c1s6)
{
 in1_8:  "N81" <- , rNP3c21/out1_8;
 in9_16: "N82" <- , rNP3c22/out1_8;

 e1_16:  "N31" -> , r13c2s20/in32_47;

 t1_8:   "N31" -> , r12c2s11/in1_8;
 t9_16:  "N32" -> , r12c2s12/in1_8;
}

SPLIT_BOX(r13c1s7)
{
 in1_8:  "N83" <- , rNP3c23/out1_8;
 in9_16: "N84" <- , rNP3c24/out1_8;

 e1_16:  "N32" -> , r13c2s16/in0_15;

 t1_8:   "N33" -> , r12c2s13/in1_8;
 t9_16:  "N34" -> , r12c2s14/in1_8;
}

SPLIT_BOX(r13c1s8)
{
 in1_4:   "N85" <- , rNP4c25/out1_4;
 in5_8:   "N85" <- , rNP3c25/out5_8;
 in9_12:  "N75" <- , rNP4c26/out1_4;
 in13_16: "N75" <- , rNP3c26/out5_8;

 e1_16:  "N33" -> , r13c2s16/in16_31;

 t1_8:   "N35" -> , r12c3s1/in1_8;
 t9_16:  "N45" -> , r12c3s2/in1_8;
}

SPLIT_BOX(r13c1s9)
{
 in1_8:  "N71" <- , rNP4c21/out1_8;
 in9_16: "N72" <- , rNP4c22/out1_8;

 e1_16:  "N41" -> , r13c2s16/in32_47;

 t1_8:   "N41" -> , r12c3s3/in1_8;
 t9_16:  "N44" -> , r12c3s4/in1_8;
}

SPLIT_BOX(r13c1s10)
{
 in1_8:  "N73" <- , rNP4c23/out1_8;
 in9_16: "N74" <- , rNP4c24/out1_8;

 e1_16:  "N42" -> , r13c2s15/in0_15;

 t1_8:   "N43" -> , r12c3s5/in1_8;
 t9_16:  "N44" -> , r12c3s6/in1_8;
}

SPLIT_BOX(r13c1s11)
{
 in1_8:  "N61" <- , rNP5c21/out1_8;
 in9_16: "N62" <- , rNP5c22/out1_8;


 e1_16:  "N51" -> , r13c2s15/in16_31;

 t1_8:   "N51" -> , r12c3s7/in1_8;
 t9_16:  "N52" -> , r12c3s8/in1_8;
}

SPLIT_BOX(r13c1s12)
{
 in1_8:  "N63" <- , rNP5c23/out1_8;
 in9_16: "N64" <- , rNP5c24/out1_8;

 e1_16:  "N52" -> , r13c2s15/in32_47;

 t1_8:   "N53" -> , r12c3s9/in1_8;
 t9_16:  "N54" -> , r12c3s10/in1_8;
}

SPLIT_BOX(r13c1s13)
{
 in1_4:   "N65" <- , rNP6c25/out1_4;
 in5_8:   "N65" <- , rNP5c25/out5_8;
 in9_12:  "N55" <- , rNP6c26/out1_4;
 in13_16: "N55" <- , rNP5c26/out5_8;

 e1_16:  "N53" -> , r13c2s11/in0_15;

 t1_8:   "N55" -> , r12c3s11/in1_8;
 t9_16:  "N56" -> , r12c3s12/in1_8;
}

SPLIT_BOX(r13c1s14)
{
 in1_8:  "N51" <- , rNP6c21/out1_8;
 in9_16: "N52" <- , rNP6c22/out1_8;

 e1_16:  "N61" -> , r13c2s11/in16_31;

 t1_8:   "N61" -> , r12c3s13/in1_8;
 t9_16:  "N62" -> , r12c3s14/in1_8;
}

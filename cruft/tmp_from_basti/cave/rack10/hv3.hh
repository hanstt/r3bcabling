
/* 
 * The LeCroy 1440 HV mainframes.
 *
 * r16c1 r16c2 r12c3
 */

// Crate 3: 

LECROY1440(r10c3)
{
  ;
}

LECROY1442(r10c3s16) { ; } // Front, leftmost
LECROY1442(r10c3s17) { ; } // Front, second from left
LECROY1441(r10c3s18) { ; } // Front, third from left
LECROY1445(r10c3s19) // Front, rightmost
{
  SETTING("CRATE_NO" => "3");

 j1: r16c2s19/j2; // upstream (controller)
 j2: ; // next (none)
}

// CsI

#if USING_CS
LECROY1443N(r10c3s0)  { out0_15: /*L1*/ r28c11/in1_16; }
LECROY1443N(r10c3s1)  { out0_15: /*L2*/ r28c12/in1_16; }
LECROY1443N(r10c3s2)  { out0_15: /*L3*/ r28c13/in1_16; }
LECROY1443N(r10c3s3)  { out0_15: /*L4*/ r28c14/in1_16; }
LECROY1443N(r10c3s4)  { out0_15: /*L5*/ r28c15/in1_16; }
LECROY1443N(r10c3s5)  { out0_15: /*R1*/ r29c11/in1_16; }
LECROY1443N(r10c3s6)  { out0_15: /*R2*/ r29c12/in1_16; }
LECROY1443N(r10c3s7)  { out0_15: /*R3*/ r29c13/in1_16; }
LECROY1443N(r10c3s8)  { out0_15: /*R4*/ r29c14/in1_16; }
LECROY1443N(r10c3s9)  { out0_15: /*R5*/ r29c15/in1_16; }
#endif

// CsI veto

#if USING_CV
LECROY1443N(r10c3s10) { out0_15: /*L6*/ r28c16/in1_16; }
#endif

// RPC Scintillators
// Yet it hasn't been plugged to the detectors but should be done ASAP.

LECROY1443N(r10c3s10) 
{ 
 out0_1: rSCI1c1s1u1_2/hv;
 out2_3: rSCI2c1s1u1_2/hv;
}

LECROY1443N(r10c3s11)
{
 	out0: rPOS1c1s1u1/hv;
 	out1: rPOS1c1s1u2/hv;
 	out2: rPOS1c1s1u3/hv;
 	out3: rPOS1c1s1u4/hv;
 	out4: rROLU1c1s1u1/hv;
 	out5: ;
 	out6: ;
 	out7: rROLU1c1s1u2/hv;
 	out8: ;
 	out9: rROLU1c1s1u3/hv;
 	out10: rROLU1c1s1u4/hv;
 	out11: rROLU2c1s1u1/hv;
 	out12: rROLU2c1s1u2/hv;
 	out13: rROLU2c1s1u3/hv;
 	out14: rROLU2c1s1u4/hv;
 	out15: ;
}






// TFW

LECROY1443N(r10c3s13) { out0_15: r26c14/in1_16; }
//LECROY1443N(r10c3s13) { out0_15: rNTFRc12/in1_16; }
LECROY1443N(r10c3s15) { out0_15: r26c12/in1_16; }

// NTF

LECROY1443N(r10c3s3) { out0_15: rNTFRc11/in1_16; }
LECROY1443N(r10c3s4) { out0_15: rNTFRc12/in1_16; }

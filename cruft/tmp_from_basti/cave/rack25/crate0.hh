// ----------- NIM CRATE for PSP ---------------------- //
// Rack 25 is a "virtual rack". It does not exist, only the crates are there.

NIM_CRATE(r25c0)
{
  ;
}

MSCF16(r25c0s07)             //module 17
{
  in11:    rPSP3c4s1/e;    // PSP 3.1
  in12:    rPSP3c4s2/e;    // PSP 3.2
  in13:    rPSP3c4s3/e;    // PSP 3.3
  in14:    rPSP3c4s4/e;    // PSP 3.4
  in15:    rPSP3c4s5/e;    // PSP 3.5

  e1_16:  r23c3s20/in1_16;   // PSP MADC
}



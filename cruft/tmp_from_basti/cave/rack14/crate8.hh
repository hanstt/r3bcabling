/* 
 * Crate 8 (1 CFD, scalers, right side of fastbus)
 */

CAMAC_CRATE(r14c8)
{
  ;
}

CF8101(r14c8s5) //TODO: moved to r14c8s5. change it.
{
  SERIAL("LCF4006");

 in1_4:   r15c14s11u9_12/out;
 in5_8:   r15c14s23u1_4/out; 
 tb1_8:   -> , .c10s3(DELAY)/in1_8;

 m:       ;
 sum:     r14c9s5/in5;
 or:      r13c4s17u1/in5;
 test:    r12c5s23/out15;
 mux_tb:  .c10s8/in4a;   mux_e: .c10s8/in5a;   mux_mon: .c10s8/in6a;
}

SC4800(r14c8s9)
{
  SERIAL("SC6758"); // TODO: check

 in0_7:    "9/1" <- , r12c5s13/th1_8;
 in8_15:   "9/2" <- , r12c5s14/th1_8;
 in16_23:  "9/3" <- , r14c7s2/th1_8;
 in24_31:  "9/4" <- , r14c7s3/th1_8;
 in32_39:  "9/5" <- , r14c7s4/th1_8;
 in40_47:  "9/6" <- , r14c7s5/th1_8;
}
SC4800(r14c8s10)
{
  SERIAL("SC6759"); // TODO: check

 in0_7:    "10/1" <- , r14c7s6/th1_8;
 in8_15:   "10/2" <- , r14c7s7/th1_8;
 in16_23:  "10/3" <- , r14c7s8/th1_8;
 in24_31:  "10/4" <- , r14c7s9/th1_8;
 in32_39:  "10/5" <- , r14c7s10/th1_8;
 in40_47:  "10/6" <- , r14c7s11/th1_8;
}
SC4800(r14c8s11)
{
  SERIAL("SC6755"); // TODO: check

 in0_7:     r14c10s1/outa1_8;   
 in8_15:    r14c10s1/outa9_16;  
 in16_23:   r14c10s2/outa1_8;   
 in24_31:   r14c10s2/outa9_16;  
 in32_39:   r14c10s3/outa1_8;   
 in40_47:   r14c10s3/outa9_16;  
}
SC4800(r14c8s12)
{
  SERIAL("SC6932"); // TODO: check

 in0_7:     r14c10s4/outa1_8;   
 in8_15:    r14c10s4/outa9_16;  
 in16_23:   r14c10s5/outa1_8;   
 in24_31:   r14c10s5/outa9_16;  
 in32_39:   r14c10s6/outa1_8;   
 in40_47:   r14c10s6/outa9_16;  
}
SC4800(r14c8s13)
{
  SERIAL("SC6933"); // TODO: check

 in0_7:     r14c6s15/outb1_8;   
 in8_15:    r14c6s15/outb9_16;  
 in16_23:   r14c6s14/outb1_8;   
 in24_31:   r14c6s14/outb9_16;  
 in32_39:   r14c6s13/outb1_8;   
 in40_47:   r14c6s13/outb9_16;  
}
SC4800(r14c8s14)
{
  SERIAL("SC6931"); // TODO: check

 in0_7:     r14c10s7/outa1_8;   
 in8_15:    r14c10s7/outa9_16;  
 in16_23:   r14c6s11/outb1_8;   
 in24_31:   r14c6s11/outb9_16;  
 in32_39:   r14c6s20/outb1_8;   
 in40_47:   r14c6s12/outb9_16;  
}

// Slot 15 empty
// Slot 16 empty
// Slot 17 empty
// Slot 18 empty

IOL_BOX(r14c8s18)
{
  ;
}

// TODO: In reality it's a LE1600...
CF8101(r14c8s19)
{
 in1: r13c4s19u1/out1; // for total LAND multiplicity
 in2: r13c4s19u2/out1; // for total TFW multiplicity
 in3: r13c4s19u3/out1; // what for ?
 in4: r13c4s19u4/out1; // what for ?
 in5: r13c4s19u5/out1; // what for ?
 in6: r13c4s19u6/out1; // what for ?
 
 ta1_8: r14c8s20/in0_7;
 //tb1_8: r14c8s21/in32_39;
}

LECROY2365(r14c8s20)
{
 in0_7:    r14c8s19/ta1_8;
 in8_15:   r14c9s21/out_ecl1_8;
  
 out0a_7a: r14c9s23/in_ecl1_8;
 out0b_7b: r14c8s21/in24_31;
}

SC4800(r14c8s21)
{

 in24_31:  r14c8s20/out0b_7b;
 //in32_39:  r14c8s19/tb1_8;
 in40_47:  r14c9s21/out_ecl9_16;
}

/*
 *SU1200(r14c8s20)
 *{
 *  //CONN<in 1-2 M22,23>
 *  //OTHER<Scaler>
 *  // :   /;   // in 1-2 M22,23 from Scaler
 *  ;
 *
 * in1: ;
 * in2: ;
 * in3: //50ohms;
 *
 * analog_out: ;
 *}
 */
// Slot 21 strange

// Slot 22 empty
// Slot 23 empty TODO: DSM3320

// Slot 24: unnamed CAV1000 partner

CAV1000(r14c8s25)
{
  SETTING("CRATE_NO" => "8");

 vsb_in:  r14c7s24/vsb_out;
 vsb_out: r14c10s24/vsb_in;
 
}


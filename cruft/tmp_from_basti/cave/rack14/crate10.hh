/* 
 * Crate 10
 */

CAMAC_CRATE(r14c10)
{
  ;
}

DL1600(r14c10s1)
{
/* in1_8:      .s10/tb1_8;
 in9_16:     .s11/tb1_8;
*/
 in1_8:      .s15/ta1_8;
 in9_16:     .s13/ta1_8;
 outb1_16:   r13c2s4(TDC)/in16_31;

 
 outa1_8:    r14c8s11/in0_7;    
 outa9_16:   r14c8s11/in8_15;   

}
DL1600(r14c10s2)
{

 SERIAL("6219");
/*	
 in1_8:      .s12/tb1_8;
 in9_16:     .s13/tb1_8;
*/

 in1_8:      .s17/ta1_8;
 in9_16:     .s18/ta1_8;
 outb1_16:   r13c2s4(TDC)/in32_47;
 
 outa1_8:    r14c8s11/in16_23;  
 outa9_16:   r14c8s11/in24_31;  

}
DL1600(r14c10s3)
{
 in1_8:      .c8s5/tb1_8;
 in9_16:     .s15/tb1_8;

 outa1_8:    r14c8s11/in32_39;  
 outa9_16:   r14c8s11/in40_47;  

// outb1_16:   r13c2s4(TDC)/in32_47;
}
DL1600(r14c10s4)
{
 in1_8:      .s16/tb1_8;
 in9_16:     .s17/tb1_8;

 outa1_8:    r14c8s12/in0_7;    
 outa9_16:   r14c8s12/in8_15;   

// outb1_16:   r13c2s6(TDC)/in0_15;
}
DL1600(r14c10s5)
{
 in1_8:      .s18/tb1_8;
 in9_16:     .s19/tb1_8;

 outa1_8:    r14c8s12/in16_23; 
 outa9_16:   r14c8s12/in24_31;  

// outb1_16:   r13c2s6(TDC)/in16_31;
}
DL1600(r14c10s6)
{
 in1_8:      .s20/tb1_8;
 in9_16:     .s21/tb1_8;

 outa1_8:    r14c8s12/in32_39;  
 outa9_16:   r14c8s12/in40_47;  

 outb1_16:   r13c2s6(TDC)/in32_47;
}
DL1600(r14c10s7)
{
 in1_8:      .s22/tb1_8;
 in9_16:     .s23/tb1_8;

 outa1_8:    r14c8s14/in0_7;    
 outa9_16:   r14c8s14/in8_15;   

 outb1_16:   r13c2s6(TDC)/in48_63;
}

RMX3(r14c10s8)
{
  SERIAL("L_MPX11");

 in1a:  .s10/mux_tb;   in2a:  .s10/mux_e;   in3a:  .s10/mux_mon; 
 in1b:  .s11/mux_tb;   in2b:  .s11/mux_e;   in3b:  .s11/mux_mon; 
 in1c:  .s12/mux_tb;   in2c:  .s12/mux_e;   in3c:  .s12/mux_mon; 
 in1d:  .s13/mux_tb;   in2d:  .s13/mux_e;   in3d:  .s13/mux_mon; 
 in4a:  .c8s5/mux_tb;   in5a:  .c8s5/mux_e;   in6a:  .c8s5/mux_mon; 
 in4b:  .s15/mux_tb;   in5b:  .s15/mux_e;   in6b:  .s15/mux_mon; 
 in4c:  .s16/mux_tb;   in5c:  .s16/mux_e;   in6c:  .s16/mux_mon; 
 in4d:  .s17/mux_tb;   in5d:  .s17/mux_e;   in6d:  .s17/mux_mon; 
 in7a:  .s18/mux_tb;   in8a:  .s18/mux_e;   in9a:  .s18/mux_mon; 
 in7b:  .s19/mux_tb;   in8b:  .s19/mux_e;   in9b:  .s19/mux_mon;
 in7c:  .s20/mux_tb;   in8c:  .s20/mux_e;   in9c:  .s20/mux_mon;
 in7d:  .s21/mux_tb;   in8d:  .s21/mux_e;   in9d:  .s21/mux_mon;
 in10a: .s22/mux_tb;   in11a: .s22/mux_e;   in12a: .s22/mux_mon;
 in10b: .s23/mux_tb;   in11b: .s23/mux_e;   in12b: .s23/mux_mon;

 out1_3:    r14c10s9/in1a_3a;
 out4_6:    r14c10s9/in1b_3b;
 out7_9:    r14c10s9/in1c_3c;
 out10_12:  r14c10s9/in1d_3d;
}

RM2400(r14c10s9)
{
  SERIAL("L_MPX12");

 in1a_3a: r14c10s8/out1_3;
 in1b_3b: r14c10s8/out4_6;
 in1c_3c: r14c10s8/out7_9;
 in1d_3d: r14c10s8/out10_12;

 out1:    r14c7s19/in4d;
 out2:    r14c7s19/in5d;
 out3:    r14c7s19/in6d;
}

CF8101(r14c10s10)
{
  SERIAL("LCF4002");

 in1_8:   r15c16s11u1_8/out;   
// tb1_8:   -> , .s1(DELAY)/in1_8;

 m:       ;
 sum:     r14c9s5/in1;
 or:      r13c4s17u1/in1;
 test:    r14c6s23/out2;
 mux_tb:  .s8/in1a;   mux_e: .s8/in2a;   mux_mon: .s8/in3a;
}
CF8101(r14c10s11)
{
  SERIAL("LCF4001");

 in1_4:   r15c16s11u9_12/out;
 in5_8:   r15c16s15u1_4/out; 
// tb1_8:   -> , .s1(DELAY)/in9_16;

 m:       ;
 sum:     r14c9s5/in2;
 or:      r13c4s17u1/in2;
 test:    r14c6s23/out4;
 mux_tb:  .s8/in1b;   mux_e: .s8/in2b;   mux_mon: .s8/in3b;
}
CF8101(r14c10s12)
{
  SERIAL("LCF4004");

 in1_8:   r15c16s15u5_12/out;   
// tb1_8:   -> , .s2(DELAY)/in1_8;

 m:       ;
 sum:     r14c9s5/in3;
 or:			r13c4s17u1/in3;
 test:    r14c7s20/out16;
 mux_tb:  .s8/in1c;   mux_e: .s8/in2c;   mux_mon: .s8/in3c;
}
CF8101(r14c10s13)
{
  SERIAL("LCF4003");

 in1_8:	  r13c3s20/t9_16;
 ta1_8:   .s1(DELAY)/in9_16;

// in1_8:   r15c17s11u1_8/out;   
// tb1_8:   -> , .s2(DELAY)/in9_16;

 m:       ;
 sum:     r14c9s5/in4;
 or:      r13c4s17u1/in4;
 test:    r12c5s23/out13;
 mux_tb:  .s8/in1d;   mux_e: .s8/in2d;   mux_mon: .s8/in3d;
}
//CF8101(r14c10s14) moved to r14c8s5
CF8101(r14c10s15)
{
  SERIAL("LCF4005");

/* in1_4:   r15c17s11u9_12/out;
 in5_8:   r15c17s23u1_4/out; 
*/
// NEW NTF 
 in1_8:	  r13c3s20/t1_8;
 ta1_8:	  .s1(DELAY)/in1_8;
/////////

 tb1_8:   -> , .s3(DELAY)/in9_16;

 m:       ;
 sum:     r14c9s5/in6;
 or:      r13c4s17u1/in6;
 test:    r14c6s23/out3;
 mux_tb:  .s8/in4b;   mux_e: .s8/in5b;   mux_mon: .s8/in6b;
}
CF8101(r14c10s16)
{
  SERIAL("LCF4008");

 in1_8:   r15c17s23u5_12/out;   
 tb1_8:   -> , .s4(DELAY)/in1_8;

 m:       ;
 sum:     r14c9s5/in7;
 or:      r13c4s17u1/in7;
 test:    r12c5s23/out16;
 mux_tb:  .s8/in4c;   mux_e: .s8/in5c;   mux_mon: .s8/in6c;
}
CF8101(r14c10s17)
{
  SERIAL("LCF4007");

// in1_8:   r15c14s23u5_12/out;   
 tb1_8:   -> , .s4(DELAY)/in9_16;

 in1_8:	  r13c3s19/t1_8;
 ta1_8:   .s2(DELAY)/in1_8;
 
 m:       ;
 sum:     r14c9s5/in8;
 or:      r13c4s17u1/in8;
 test:    r14c6s23/out5;
 mux_tb:  .s8/in4d;   mux_e: .s8/in5d;   mux_mon: .s8/in6d;
}
CF8101(r14c10s18)
{
  SERIAL("LCF4010");

// in1_8:   r15c15s23u5_12/out;   
 tb1_8:   -> , .s5(DELAY)/in1_8;

 in1_8:	  r13c3s19/t9_16;
 ta1_8:   .s2(DELAY)/in9_16;

 m:       ;
 sum:     r14c9s5/in9;
 or:      r13c4s17u1/in9;
 test:    r14c6s23/out6;
 mux_tb:  .s8/in7a;   mux_e: .s8/in8a;   mux_mon: .s8/in9a;
}
CF8101(r14c10s19)
{
  SERIAL("LCF4009");

 in1_8:   r15c18s11u1_8/out;   
 tb1_8:   -> , .s5(DELAY)/in9_16;

 m:       ;
 sum:     r14c9s5/in10;
 or:      r13c4s17u1/in10;
 test:    r14c6s23/out7;
 mux_tb:  .s8/in7b;   mux_e: .s8/in8b;   mux_mon: .s8/in9b;
}
CF8101(r14c10s20)
{
  SERIAL("LCF4012");

 in1_4:   r15c15s11u9_12/out;
 in5_8:   r15c15s23u1_4/out; 
 tb1_8:   -> , .s6(DELAY)/in1_8;

 m:       ;
 sum:     r14c9s5/in11;
 or:      r13c4s17u1/in11;
 test:    r14c6s23/out8;
 mux_tb:  .s8/in7c;   mux_e: .s8/in8c;   mux_mon: .s8/in9c;
}
CF8101(r14c10s21)
{
  SERIAL("LCF4011");

 in1_8:   r15c15s11u1_8/out;
 tb1_8:   -> , .s6(DELAY)/in9_16;

 m:       ;
 sum:     r14c9s5/in12;
 or:      r13c4s17u1/in12;
 test:    r14c6s23/out9;
 mux_tb:  .s8/in7d;   mux_e: .s8/in8d;   mux_mon: .s8/in9d;
}
CF8101(r14c10s22)
{
  SERIAL("LCF4014");

#if USING_XB
 in1_8:   r15c18s23u5_12/out;   
#endif
#if USING_CV
 in1:     r15c18s15u1/out_neg;
 in2:     r15c18s15u3/out_neg;   
 in3:     r15c18s15u2/out_neg;   
 in4: BROKEN; // input pin broken
 in5:     r15c18s15u4/out_neg;   
 in6:     r15c18s15u5/out_neg;   
 in7:     r15c18s15u6/out_neg;   
 in8:     r15c18s19u1/out_neg;   
#endif
 tb1_8:   -> , .s7(DELAY)/in1_8;

 m:       ;
 sum:     .c6s18/in9;
 or:      r13c4s17u1/in13;
 test:    r12c5s23/out14;
 mux_tb:  .s8/in10a;  mux_e: .s8/in11a;  mux_mon: .s8/in12a;
}
CF8101(r14c10s23)
{
  SERIAL("LCF4013");

#if USING_XB
 in1_4:   r15c18s11u9_12/out;
 in5_8:   r15c18s23u1_4/out;
#endif
#if USING_CV
 in1:     r15c18s19u2/out_neg;   
 in2:     r15c18s19u3/out_neg;   
 in3:     r15c18s19u4/out_neg;   
 in4: ;
 in5:     r15c18s19u5/out_neg;   
 in6: ;
 in7:     r15c18s19u6/out_neg;   
 in8: ;
#endif
 tb1_8:   -> , .s7(DELAY)/in9_16;

 m:       ;
 sum:     .c6s18/in8;
 or:      r13c4s17u1/in14;
 test:    r14c6s23/out11;
 mux_tb:  .s8/in10b;  mux_e: .s8/in11b;  mux_mon: .s8/in12b;
}

CBV1000(r14c10s24)
{
  SETTING("CRATE_NO" => "10");

 vsb_in:  r14c8s25/vsb_out;
}

DMS1000(r14c10s25)
{
  ;
}










/*
 * Crate 7
 */

CAMAC_CRATE(r14c7)
{
  ;
}

RMX3(r14c7s1)
{
  SERIAL("L_MPX7");

 in1a_3d:   "CR2 mux out" <- , r12c2s22/out1_12;   
 in4a_6d:   "CR3 mux out" <- , r12c3s22/out1_12;   
 in7a_9d:   "CR4 mux out" <- , r12c4s22/out1_12;   
 in10a_12d: "CR5 mux out" <- , r12c5s22/out1_12;   

 out1_3:    r14c7s19/in1a_3a;   
 out4_6:    r14c7s19/in1b_3b;   
 out7_9:    r14c7s19/in1c_3c;   
 out10_12:  r14c7s19/in1d_3d;   
}

CF8103(r14c7s2)
{
  SERIAL("LCF6545");

 in1_8: "V11 CF"    <- , r13c1s17(SPLIT)/t1_8;
 th1_8: "9/3"       -> , r14c8s9(SCALER)/in16_23;   
 tb1_8: "CR7 SL2"   -> , .s12(DELAY)/in1_8;

 m:      r13c4s21/in1;
 test:   .s20/out1;
 mux_tb: .s17/in1a;   mux_e: .s17/in5a;   mux_mon: .s17/in9a;
}
CF8103(r14c7s3)
{
  SERIAL("LCF6202");

 in1_8: "V12 CF"    <- , r13c1s17(SPLIT)/t9_16;
 th1_8: "9/4"       -> , r14c8s9(SCALER)/in24_31;
 tb1_8: "CR7 SL3"   -> , .s12(DELAY)/in9_16;

 m:      r13c4s21/in2;
 test:   .s20/out2;
 mux_tb: .s17/in1b;   mux_e: .s17/in5b;   mux_mon: .s17/in9b;
}
CF8103(r14c7s4)
{
  SERIAL("LCF6404");

 in1_8: "V13 CF"    <- , r13c1s18(SPLIT)/t1_8;
 th1_8: "9/5"       -> , r14c8s9(SCALER)/in32_39;
 tb1_8: "CR7 SL4"   -> , .s13(DELAY)/in1_8;

 m:      r13c4s21/in3;
 test:   .s20/out3;
 mux_tb: .s17/in1c;   mux_e: .s17/in5c;   mux_mon: .s17/in9c;
}
CF8103(r14c7s5)
{
  SERIAL("LCF6338");

 in1_8: "V14 CF"    <- , r13c1s18(SPLIT)/t9_16;
 th1_8: "9/6"       -> , r14c8s9(SCALER)/in40_47;
 tb1_8: "CR7 SL5"   -> , .s13(DELAY)/in9_16;

 m:      r13c4s21/in4;
 test:   .s20/out4;
 mux_tb: .s17/in1d;   mux_e: .s17/in5d;   mux_mon: .s17/in9d;
}
CF8103(r14c7s6)
{
  SERIAL("LCF6548");

 in1_8: "V15 CF"    <- , r13c1s19(SPLIT)/t1_8;
 th1_8: "10/1"      -> , r14c8s10(SCALER)/in0_7;  
 tb1_8: "CR7 SL6"   -> , .s14(DELAY)/in1_8;

 m:      r13c4s21/in5;
 test:   .s20/out5;
 mux_tb: .s17/in2a;   mux_e: .s17/in6a;   mux_mon: .s17/in10a;
}
CF8103(r14c7s7)
{
  SERIAL("LCF6547");

 in1_8: "V25 CF"    <- , r13c1s19(SPLIT)/t9_16;
 th1_8: "10/2"      -> , r14c8s10(SCALER)/in8_15; 
 tb1_8: "CR7 SL7"   -> , .s14(DELAY)/in9_16;

 //m:      .c9s1/in1;
 test:   .s20/out6;
 mux_tb: .s17/in2b;   mux_e: .s17/in6b;   mux_mon: .s17/in10b;
}
CF8103(r14c7s8)
{
  SERIAL("LCF6204");

 in1_8: "V21 CF"    <- , r13c1s20(SPLIT)/t1_8;
 th1_8: "10/3"      -> , r14c8s10(SCALER)/in16_23;
 tb1_8: "CR7 SL8"   -> , .s15(DELAY)/in1_8;

 m:      .c9s1/in1;
 or:	 r13c11/front27;
 test:   .s20/out7;
 mux_tb: .s17/in2c;   mux_e: .s17/in6c;   mux_mon: .s17/in10c;
}
CF8103(r14c7s9)
{
  SERIAL("LCF6855");

 in1_8: "V22 CF"    <- , r13c1s20(SPLIT)/t9_16; 
 th1_8: "10/4"      -> , r14c8s10(SCALER)/in24_31;
 tb1_8: "CR7 SL9"   -> , .s15(DELAY)/in9_16;

 m:      .c9s1/in2;
 test:   .s20/out8;
 mux_tb: .s17/in2d;   mux_e: .s17/in6d;   mux_mon: .s17/in10d;
}
CF8103(r14c7s10)
{
  SERIAL("LCF6147");

 in1_8: "V23 CF"    <- , r13c1s21(SPLIT)/t1_8;
 th1_8: "10/5"      -> , r14c8s10(SCALER)/in32_39;
 tb1_8: "CR7 SL10"  -> , .s16(DELAY)/in1_8;

 m:      .c9s1/in4;
 test:   .s20/out9;
 mux_tb: .s17/in3a;   mux_e: .s17/in7a;   mux_mon: .s17/in11a;
}
CF8103(r14c7s11)
{
  SERIAL("LCF6405");

 in1_8: "V24 CF"    <- , r13c1s21(SPLIT)/t9_16; 
 th1_8: "10/6"      -> , r14c8s10(SCALER)/in40_47;
 tb1_8: "CR7 SL10"  -> , .s16(DELAY)/in9_16;

 m:      .c9s1/in5;
 test:   .s20/out10;
 mux_tb: .s17/in3b;   mux_e: .s17/in7b;   mux_mon: .s17/in11b;
}

DL1610(r14c7s12)
{
 in1_8:  <- "CR7 SL2"  , .s2/tb1_8;
 in9_16: <- "CR7 SL3"  , .s3/tb1_8;

 outa1_16:             r11c2s5/in48_63;
}
DL1610(r14c7s13)
{
 in1_8:  <- "CR7 SL4"  , .s4/tb1_8;
 in9_16: <- "CR7 SL5"  , .s5/tb1_8;

 outa1_16:             r13c2s4(TDC)/in48_63;
}
DL1610(r14c7s14)
{
 in1_8:  <- "CR7 SL6"  , .s6/tb1_8;
 in9_16: <- "CR7 SL7"  , .s7/tb1_8;

 outa1_16:             r13c2s5(TDC)/in48_63;
}
DL1600(r14c7s15)
{
 in1_8:  <- "CR7 SL8"  , .s8/tb1_8;
 in9_16: <- "CR7 SL9"  , .s9/tb1_8;

 outa1_16:             r13c2s3(TDC)/in32_47;
}
DL1610(r14c7s16)
{
 in1_8:  <- "CR7 SL10" , .s10/tb1_8;
 in9_16: <- "CR7 SL11" , .s11/tb1_8;

 outa1_16:             r13c2s3(TDC)/in48_63;
}

RMX3(r14c7s17)
{
  SERIAL("L_MPX9");

 in1a:  .s2/mux_tb;    in5a:  .s2/mux_e;    in9a:  .s2/mux_mon;  
 in1b:  .s3/mux_tb;    in5b:  .s3/mux_e;    in9b:  .s3/mux_mon; 
 in1c:  .s4/mux_tb;    in5c:  .s4/mux_e;    in9c:  .s4/mux_mon; 
 in1d:  .s5/mux_tb;    in5d:  .s5/mux_e;    in9d:  .s5/mux_mon; 
 in2a:  .s6/mux_tb;    in6a:  .s6/mux_e;    in10a: .s6/mux_mon; 
 in2b:  .s7/mux_tb;    in6b:  .s7/mux_e;    in10b: .s7/mux_mon; 
 in2c:  .s8/mux_tb;    in6c:  .s8/mux_e;    in10c: .s8/mux_mon; 
 in2d:  .s9/mux_tb;    in6d:  .s9/mux_e;    in10d: .s9/mux_mon; 
 in3a:  .s10/mux_tb;   in7a:  .s10/mux_e;   in11a: .s10/mux_mon;
 in3b:  .s11/mux_tb;   in7b:  .s11/mux_e;   in11b: .s11/mux_mon;

 out1_4:   r14c7s18/in1a_1d;
 out5_7:   r14c7s18/in2a_2c;
 out9_12:  r14c7s18/in3a_3d;
}

RMX3(r14c7s18)
{
  SERIAL("L_MPX10");

 in1a_1d:   r14c7s17/out1_4;   
 in2a_2c:   r14c7s17/out5_7;   
 in3a_3d:   r14c7s17/out9_12;   

 out1_3:    r14c7s19/in4c_6c;   
}

RMX2(r14c7s19)
{
  SERIAL("L_MPX8");

 in1a:    .s1/out1;
 in1b:    .s1/out4;
 in1c:    .s1/out7;
 in1d:    .s1/out10;

 in2a:    .s1/out2;
 in2b:    .s1/out5;
 in2c:    .s1/out8;
 in2d:    .s1/out11;

 in3a:    .s1/out3;
 in3b:    .s1/out6;
 in3c:    .s1/out9;
 in3d:    .s1/out12;

 out1:    .s19/in4a;
 out2:    .s19/in5a;
 out3:    .s19/in6a;

 in4a:    .s19/out1;
 in4b:    r14c6s22/out1; 
 in4c:    .s18/out1; 
 
 in5a:    .s19/out2;
 in5b:    r14c6s22/out2;
 in5c:    .s18/out2; 
 
 in6a:    .s19/out3;
 in6b:    r14c6s22/out3; 
 in6c:    .s18/out3; 

 in4d:    r14c10s9/out1;
 in5d:    r14c10s9/out2;
 in6d:    r14c10s9/out3;

 out4: r13c11/front14;//rLXG0807c1s1/in; // MUX output -> panel?
 out5: r13c11/front15;//rLXG0807c1s2/in; // MUX output -> panel?
 out6: r13c11/front16;//rLXG0807c1s3/in; // MUX output -> panel?
}

SCOPE(rLXG0807c1s1)
{
  LABEL("SCOPE_TB");
 //in: r14c7s19/out4;
}

SCOPE(rLXG0807c1s2)
{
  LABEL("SCOPE_E");
 //in: r14c7s19/out5;
}

SCOPE(rLXG0807c1s3)
{
  LABEL("SCOPE_MON");
 //in: r14c7s19/out6;
}

LIFO(r14c7s20)
{
 in:	r13c4s1u1/out5;
	
 out1:   .s2/test;
 out2:   .s3/test;
 out3:   .s4/test;
 out4:   .s5/test;
 out5:   .s6/test;
 out6:   .s7/test;
 out7:   .s8/test;
 out8:   .s9/test;
 out9:   .s10/test;
 out10:  .s11/test;

 out11:  .c6s6/test;
 out12:  .c6s7/test;
 out13:  .c6s10/test;
 out14:  .c6s8/test;
 out15:  .c6s9/test;

 out16:  .c10s12/test;
}

DATAWAY_DISPLAY(r14c7s21)
{
  ;
}

/*
 * Was beam scaler, removed.
 *SC4800(r14c7s22)
 *{
 *  SERIAL("SC6929"); // TODO: check
 *
 *  // Unused
 *  ;
 *}
 */

// Slot 23 empty

CBV1000(r14c7s24)
{
  SETTING("CRATE_NO" => "7");

 vsb_in:  r14c6s24/vsb_out;
 vsb_out: r14c8s25/vsb_in;
}

DMS1000(r14c7s25)
{
  ;
}


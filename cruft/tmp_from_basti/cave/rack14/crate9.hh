/* 
 * Crate 9
 */

NIM_CRATE(r14c9)
{
  ;
}

SU1200(r14c9s1)
{
  LABEL("M_TFW");

 in1:    r14c7s8/m;
 in2:    r14c7s9/m;
 in3:    r14c6s3/m;
 in4:    r14c7s10/m;
 in5:    r14c7s11/m;

 in7:    r14c6s1/m;
 in8:    r14c6s2/m;
 // TFW has been taken out of the trigger
 // only NTF is available
 // in9:    r14c6s3/m;
 // in10:   r14c6s4/m;
 analog_out:	r13c4s19u3/in; // to LIFO, then CFD
 gate_out1: .s21/in1; // at which threshold?
}

///////////////////////////////////////////////////////////////

SU1200(r14c9s5)
{
 in1:    r14c10s10/sum;
 in2:    r14c10s11/sum;
 in3:    r14c10s12/sum;
 in4:    r14c10s13/sum;
 in5:    r14c8s5/sum;
 in6:    r14c10s15/sum;
 in7:    r14c10s16/sum;
 in8:    r14c10s17/sum;
 in9:    r14c10s18/sum;
 in10:   r14c10s19/sum;
 in11:   r14c10s20/sum;
 in12:   r14c10s21/sum;

 analog_out:   .c6s18/in7;
}

///////////////////////////////////////////////////////////////

N638(r14c9s7)
{
  ;
}

N638_U(r14c9s7u1)
{
 in2:     .s9u4/out1;
 in3:     .s9u5/out1;
 in4:     .s9u3/out1;
 in5:     .s9u3/out2;
 in6:     .s9u1/out2;

 outl4:	  r13c11/front9;
 outl6:	  r13c11/front8;

 out_ecl2: r13c2s10/gate;
 out_ecl3: r13c2s23/gate;
 out_ecl4: r13c2s8/gate;
 out_ecl5: r13c2s7/gate;
 out_ecl6: r13c2s11/gate;//TODO:chained gate
}

///////////////////////////////////////////////////////////////

GG8000(r14c9s9)
{
	;
}

GG8000_U(r14c9s9u1)
{
	LABEL("LAND/VETO/TFW gate");

in:   .s13u2/out1; 
out2: .s7u1/in6;
}

GG8000_U(r14c9s9u2)
{
  ;
}

GG8000_U(r14c9s9u3)
{
	LABEL("GFI1 gate");

in:   .s13u2/out3; 
out1: .s7u1/in4;
out2: .s7u1/in5;
}

GG8000_U(r14c9s9u4)
{
	LABEL("GFI23 gate");

in:   .s13u2/out2; 
out1: .s7u1/in2;
}


GG8000_U(r14c9s9u5)
{
	LABEL("TDET gate");

in:   .s13u2/out4; 
out1: .s7u1/in3;
out2: ;//.s7u1/in5;
}

GG8000_U(r14c9s9u8)
{
  LABEL("CBP gate");
  in:	  .s13u4/out1;

  out1:	  r12c11s15/in16;
}

///////////////////////////////////////////////////////////////

LF4000(r14c9s13)
{
	;
}

LF4000_4_U(r14c9s13u1)
{
	LABEL("ARM (global)");

in1: r13c11/front47; // from messhuette
in2: ;//.s17u1/out1;

out1: r13c2s25/adc_gate; // FASTBUS/CAT
out2: r11c2s25/adc_gate; // FASTBUS/CAT
 out3: .s13u4/in3;
 out4: .s13u2/in3;
}

LF4000_4_U(r14c9s13u2)
{
	LABEL("GATE (global)");

in1: ; // from messhuette
 in2: r13c2s0/out3; //something weird here
 in3: .s13u1/out4;

out1: r14c9s9u1/in;
out2: r14c9s9u4/in;
out3: r14c9s9u3/in;
out4: r14c9s9u5/in;
}

LF4000_4_U(r14c9s13u3)
{
	LABEL("TDC stop (global)");

in1: r13c11/front24; // from messhuette
	//in2: .s17u6/out1;
	//in4: .s15u1/delayed;

out2: r11c2s25/tdc_stop; // FASTBUS/CAT
out4: r13c2s25/tdc_stop; // FASTBUS/CAT
}

LF4000_4_U(r14c9s13u4)
{
  LABEL("CB proton gate");
 in1: r11c2s12/out3;
 in3: .s13u1/out3;
out1: .s9u8/in;
}
///////////////////////////////////////////////////////////////

LRS222(r14c9s15)
{
	;
}

LRS222_U(r14c9s15u1)
{
start:   .s17u4/out1;
stop:    .s17u7/out1;

//delayed: .s13u3/in4; // Gives pulse at stop (if start ws before)
}

LRS222_U(r14c9s15u2)
{
	BROKEN;
}

///////////////////////////////////////////////////////////////

GG8000(r14c9s17)
{
	;
}

GG8000_U(r14c9s17u1)
{
	// ARM from local cave trigger

in:   .s19u1/out1;
out1: ;//.s13u1/in2;
}

GG8000_U(r14c9s17u2)
{
	// GATE from local cave trigger
  ;
  //in:   .s19u1/out2;
  //out1: .s13u2/in2;
}

GG8000_U(r14c9s17u4)
{
	// TDC stop source from local cave trigger (randomized)

in:   .s19u3/out2;
out1: .s15u1/start;
}

GG8000_U(r14c9s17u5)
{
 in: .s19u1/out2;
 out2: .s23/in12;
}
GG8000_U(r14c9s17u6)
{
	// TDC stop local cave trigger (normal)

in:   .s19u2/out4;
//out1: .s13u3/in2;
}

// This and the next one are suppose to generate a clock (around 300 ns
// for randomizing the stop of trigger 3 (used for e.g. cosmics)

GG8000_U(r14c9s17u7)
{
in:      .s17u8/out_neg;
out_neg: .s17u8/in;
out1:    .s15u1/stop;
}

GG8000_U(r14c9s17u8)
{
 in:      .s17u7/out_neg;
 out_neg: .s17u7/in;
}

///////////////////////////////////////////////////////////////

LF4000(r14c9s19)
{
	;
}

LF4000_4_U(r14c9s19u1)
{
	// ARM/GATE source from local cave trigger

in1: .s19u2/out1;
in2: .s19u3/out1;

out1: .s17u1/in;
out2: .s17u5/in;
}

LF4000_4_U(r14c9s19u2)
{
	// TDC stop source from local cave trigger (normal)

in1_2: .s23/out1_2;

out1:  .s19u1/in1;

out4:  .s17u6/in;
}

LF4000_4_U(r14c9s19u3)
{
	// TDC stop source from local cave trigger (randomized)

in1:   .s23/out3;

out1:  .s19u1/in2;

out2:  .s17u4/in;
}

LF4000_4_U(r14c9s19u4)
{
	;
}

///////////////////////////////////////////////////////////////

EC1601(r14c9s21)
{
in1:  .s1/gate_out1;
in2:  r13c4s17u1/out1; // OR_TDET
in6:  r13c4s11u2/out2; // Slow clock (10 Hz)
in7:  r13c4s11u1/out2; // Fast clock (1 MHz)
in8:  .s23/out12;
in9:  .s21/out1;
in10: .s21/out2;
in14: .s21/out6;
in15: .s21/out7;
in16: .s21/out8;
      
out_ecl1_8:  .c8s20/in8_15;
out_ecl9_16: .c8s21/in40_47;

// Chain such that 9_16 sees what 1_8 sees

out1:       .s21/in9;
out2:	    .s21/in10;
out6_8:     .s21/in14_16;
}

///////////////////////////////////////////////////////////////

EC1601(r14c9s23)
{

in12:	    .s17u5/out2;
  
out1_2:     .s19u2/in1_2; // Normal triggers 
out3:       .s19u3/in1;   // Trigger with 'random' TDC stop
out12:      .s21/in8;     // Deadtime from trigger module

in_ecl1_8:  .c8s20/out0a_7a;
//in_ecl9_16: r13c2s1/out1_8;

//out_ecl1_8: r13c2s1/in1_8;

}


/*
	 CO4001(r14c9s9)
	 {
	 ;
	 }

	 CO4001_U(r14c9s9u1)
	 {
	 SETTING("A"   => "AND");
	 SETTING("B"   => "OR");
	 SETTING("C"   => "OR");
	 SETTING("OUT" => "AND");

inA:     .s23u2/out1;
}

CO4001_U(r14c9s9u3)
{
SETTING("A"   => "AND");
SETTING("B"   => "OR");
SETTING("C"   => "OR");
SETTING("OUT" => "AND");

inA:     .s11u3/overlap;
inC:     .s13u1/not_out2;
}

CO4001(r14c9s11)
{
;
}

CO4001_U(r14c9s11u3)
{
SETTING("A"   => "OR");
SETTING("B"   => "OR");
SETTING("C"   => "OR");
SETTING("OUT" => "OR");

inB:     .s19u8/out2;

overlap: .s9u3/inA;
}

LF4000(r14c9s13)
{
;
}

LF4000_4_U(r14c9s13u1)
{
in3:     .s21u4/inA; // in->in???

out2:    .s7/in8;
out3:    .s17/in2;
not_out2: .s9u3/inC;
}

LF4000_4_U(r14c9s13u3)
{
in1: ;
}

LF4000_4_U(r14c9s13u4)
{
in3: ;

out1: .s7/in5;
out2: .s7/in3;
}

// Slot 15 empty

EC1601(r14c9s17)
{
in1:     .s17/out16;
in2:     .s13u1/out3;

in4: ;

out6_15: .s17/in7_16;  // loop
in7_16:  .s17/out6_15;

out16:   .s17/in1;
}

SH8000(r14c9s19)
{
	;
}

SH8000_U(r14c9s19u7)
{
in:      .s7/out8;
not_out: .s19u8/in;
}

SH8000_U(r14c9s19u8)
{
in:      .s19u7/not_out;
out2:    .s11u3/inB;
}

CO4001(r14c9s21)
{
	;
}

CO4001_U(r14c9s21u4)
{
inA:  .s13u1/in3; // in->in ???

out1: .s23u2/in1;
}

LF4000(r14c9s23)
{
	//  SETTING("" => "2x8");
	;
}

LF4000_8_U(r14c9s23u1)
{
in1:  .c6s5/or;
in2:  .c6s6/or;
in3:  .c6s7/or;
in4:  .c6s8/or;
in5:  .c6s9/or;
in6:  .c6s10/or;
}

LF4000_8_U(r14c9s23u2)
{
in1:  .s21u4/out1;

out1: .s9u1/inA;
}

*/

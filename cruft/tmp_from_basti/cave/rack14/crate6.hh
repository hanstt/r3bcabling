/*
 * Crate 6
 */

CAMAC_CRATE(r14c6)
{
  ;
}

CF8103(r14c6s1)
{
  SERIAL("LCF6323");

 in1_8:  "CB1 Up" <- , r13c1s15(SPLIT)/t1_8;
 tb1_8:  "CR6 SL1"-> , .s11(DELAY)/in1_8;

 m:      .c9s1/in7;
 test:   .s23/out12;
 mux_tb: .s21/in1a;   mux_e: .s21/in5a;   mux_mon: .s21/in9a;
}
CF8103(r14c6s2)
{
  SERIAL("LCF6556");

 in1_8:  "CB2 Up" <- , r13c1s15(SPLIT)/t9_16;
 tb1_8:  "CR6 SL2"-> , .s11(DELAY)/in9_16;

 m:      .c9s1/in8;
 test:   .s23/out13;
 mux_tb: .s21/in1b;   mux_e: .s21/in5b;   mux_mon: .s21/in9b;
}
CF8103(r14c6s3)
{
  SERIAL("LCF6124");

 in1_8:  "CB3 Up" <- , r13c1s16(SPLIT)/t9_16;
 tb1_8:  "CR6 SL3"-> , .s12(DELAY)/in9_16;

  m:      .c9s1/in3; // TFW triggering out
 test:   .s23/out14;
 mux_tb: .s21/in1c;   mux_e: .s21/in5c;   mux_mon: .s21/in9c;
}
CF8103(r14c6s4)
{
  SERIAL("LCF6421");

 in1_8:  "CB6 UP" <- , r13c1s16(SPLIT)/t1_8; 
 //tb1_8:  "CR6 SL4"-> , .s12(DELAY)/in1_8; // CHANGED BY JUSTYNA, SEE SLOT 12 AND 20, 30/06/2010
 tb1_8:  "CR6 SL4"-> , .s20(DELAY)/in1_8;

 // m:      .c9s1/in10; // TFW triggering out
 test:   .s23/out15;
 mux_tb: .s21/in1d;   mux_e: .s21/in5d;   mux_mon: .s21/in9d;
}

CF8101(r14c6s5)
{
  //SERIAL("CFD_LCF_0982");
  SERIAL("LCF0982");

 in1_8:     r15c12s11u1_8/out;  
 ta1_8:     .s13(DELAY)/in1_8;
  
 sum:    .s18/in1;
 or:     .c6s16u1/in1;
 test:   .s23/out16;
 mux_tb: .s21/in2a;   mux_e: .s21/in4a;   mux_mon: .s21/in7a;
}
CF8101(r14c6s6)
{
  //SERIAL("LFD_LLC_0983");
  SERIAL("LCF0983");

 in1_4:     r15c12s11u9_12/out;  
 in5_8:     r15c12s23u1_4/out;  
 ta1_8:     .s13(DELAY)/in9_16;

 sum:    .s18/in2;
 or:     .c6s16u1/in2;
 test:   .c7s20/out11; 
 mux_tb: .s21/in2b;   mux_e: .s21/in4b;   mux_mon: .s21/in7b;
}
CF8101(r14c6s7)
{
  //SERIAL("CFD_LCF_6851");
  SERIAL("LCF6851");

// in1_8:     r15c12s23u5_12/out;  

 in1_8:     r13c3s21/t1_8;
/*
 in1:	    r13c3s21/t1;
 in2:	    r13c3s21/t9;
 in3:	    r13c3s21/t2;
 in4:	    r13c3s21/t10;
 in5:	    r13c3s21/t3;
 in6:	    r13c3s21/t11;
 in7:	    r13c3s21/t4;
 in8:	    r13c3s21/t12;
*/
 ta1_8:     .s14(DELAY)/in1_8;

 sum:    .s18/in4;
 or:     .c6s16u1/in3;
 test:   .c7s20/out12;  
 mux_tb: .s21/in2c;   mux_e: .s21/in4c;   mux_mon: .s21/in7c;
}
CF8101(r14c6s8)
{
  //SERIAL("CFD_LCF_6852");
  SERIAL("LCF6852");

// in1_8:     r15c13s9u1_8/out;  
 
in1_8:     r13c3s21/t9_16;

/* 
 in1:	    r13c3s21/t5;
 in2:	    r13c3s21/t13;
 in3:	    r13c3s21/t6;
 in4:	    r13c3s21/t14;
 in5:	    r13c3s21/t7;
 in6:	    r13c3s21/t15;
 in7:	    r13c3s21/t8;
 in8:	    r13c3s21/t16;
*/
ta1_8:     .s14(DELAY)/in9_16;

 sum:    .s18/in6;
 or:     .c6s16u1/in4;
 test:   .c7s20/out14; 
 mux_tb: .s21/in2d;   mux_e: .s21/in4d;   mux_mon: .s21/in7d;
}
CF8101(r14c6s9)
{
  //SERIAL("CFD_LCF_6335");
  SERIAL("LCF6335");

 in1_4:     r15c13s9u9_12/out;  
 in5_8:     r15c13s23u1_4/out;  
 ta1_8:     .s15(DELAY)/in1_8;

 sum:    .s18/in5;
 or:     .c6s16u1/in5;
 test:   .c7s20/out15; 
 mux_tb: .s21/in3a;   mux_e: .s21/in6a;   mux_mon: .s21/in8a;
}
CF8101(r14c6s10)
{
  //SERIAL("CFD_LCF_6854");
  SERIAL("LCF6854");

 in1_8:     r15c14s11u1_8/out;  
 ta1_8:                .s15(DELAY)/in9_16;

 sum:    .s18/in3;
 or:     .c6s16u1/in6;
 test:   .c7s20/out13; 
 mux_tb: .s21/in3b;   mux_e: .s21/in6b;   mux_mon: .s21/in8b;
}

DL1600(r14c6s11)
{
 in1_8:  <- "CR6 SL1"  , .s1/tb1_8;
 in9_16: <- "CR6 SL2"  , .s2/tb1_8;

 outa1_16:             r13c2s3(TDC)/in0_15;

 outb1_8:   <- "TFW" , r14c8s14/in16_23;   
 outb9_16:  <- "TFW" , r14c8s14/in24_31;   
}
DL1600(r14c6s12)
{
// in1_8:  <- "CR6 SL4"  , .s4/tb1_8;   // CHANGED BY JUSTYNA ON 30/06/2010 TO SLOT 20, SAME CRATE
 in9_16: <- "CR6 SL3"  , .s3/tb1_8;

 outa9_16:             r13c2s3(TDC)/in24_31;

 //outb1_8:   <- "TFW" , r14c8s14/in32_39;   
 outb9_16:  <- "TFW" , r14c8s14/in40_47;
}
DL1600(r14c6s13)
{
 in1_8:  <-          , .s5/ta1_8;
 in9_16: <-          , .s6/ta1_8;

 outa1_16:             r13c2s5(TDC)/in0_15;

 outb1_8:   <- "TFW" , r14c8s13/in32_39;   
 outb9_16:  <- "TFW" , r14c8s13/in40_47;   
}
DL1600(r14c6s14)
{
 in1_8:  <-          , .s7/ta1_8;
 in9_16: <-          , .s8/ta1_8;

// outa1_16:             r13c2s5(TDC)/in16_31;
 outa1_16:             r13c2s4(TDC)/in0_15;

 outb1_8:   <- "DTF" , r14c8s13/in16_23;   
 outb9_16:  <- "DTF" , r14c8s13/in24_31;   
}
DL1600(r14c6s15)
{
 in1_8:  <-          , .s9/ta1_8;
 in9_16: <-          , .s10/ta1_8;

 outa1_16:             r13c2s5(TDC)/in32_47;

 outb1_8:   <- "TFW" , r14c8s13/in0_7;     
 outb9_16:  <- "TFW" , r14c8s13/in8_15;    
}

LECROY429A(r14c6s16)
{
  ;
}

LECROY429A_8_U(r14c6s16u1)
{
 in1:  .c6s5/or;
 in2:  .c6s6/or;
 in3:  .c6s7/or;
 in4:  .c6s8/or;
 in5:  .c6s9/or;
 in6:  .c6s10/or;

 out8:	r13c4s17u1/in16; // to the "OR_TDET" FIFO
}

SU1200(r14c6s18)
{
  LABEL("TDET_SUM");

 in1:      .c6s5/sum;
 in2:      .c6s6/sum;
 in3:      .c6s10/sum;
 in4:      .c6s7/sum;
 in5:      .c6s9/sum;
 in6:      .c6s8/sum;
 //in7_9:      .c7/;

 in7:      r14c9s5(SUM)/analog_out; // Others from .c10s10_s21

 in8:      .c10s23/sum;
 in9:      .c10s22/sum;
 
 analog_out: r13c11/front5;
}

DL1600(r14c6s20)
{
  in1_8:  <- "CR6 SL4"  , .s4/tb1_8;   // CHANGED BY JUSTYNA ON 30/06/2010 FROM SLOT 12, SAME CRATE
  
	outa1_8:             r13c2s3(TDC)/in16_23;
	
	outb1_8:   <- "TFW" , r14c8s14/in32_39;
}

RMX3(r14c6s21)
{
  SERIAL("L_MPX5"); // DUMMY

 in1a:  .s1/mux_tb;    in5a:  .s1/mux_e;    in9a:  .s1/mux_mon; 
 in1b:  .s2/mux_tb;    in5b:  .s2/mux_e;    in9b:  .s2/mux_mon; 
 in1c:  .s3/mux_tb;    in5c:  .s3/mux_e;    in9c:  .s3/mux_mon; 
 in1d:  .s4/mux_tb;    in5d:  .s4/mux_e;    in9d:  .s4/mux_mon; 
 in2a:  .s5/mux_tb;    in4a:  .s5/mux_e;    in7a:  .s5/mux_mon; 
 in2b:  .s6/mux_tb;    in4b:  .s6/mux_e;    in7b:  .s6/mux_mon; 
 in2c:  .s7/mux_tb;    in4c:  .s7/mux_e;    in7c:  .s7/mux_mon; 
 in2d:  .s8/mux_tb;    in4d:  .s8/mux_e;    in7d:  .s8/mux_mon; 
 in3a:  .s9/mux_tb;    in6a:  .s9/mux_e;    in8a:  .s9/mux_mon; 
 in3b:  .s10/mux_tb;   in6b:  .s10/mux_e;   in8b:  .s10/mux_mon;

 out1:   .s22/in1a;
 out2:   .s22/in1b;
 out3:   .s22/in1c;
 out10:  .s22/in1d;
 out5:   .s22/in2a;
 out4:   .s22/in2b;
 out6:   .s22/in2c;
 out11:  .s22/in2d;
 out9:   .s22/in3a;
 out7:   .s22/in3b;
 out8:   .s22/in3c;
 out12:  .s22/in3d;
}

RMX2(r14c6s22) // TODO: check model
{
  SERIAL("L_MPX6"); // DUMMY

 in1a:  .s21/out1;
 in1b:  .s21/out2;
 in1c:  .s21/out3;
 in1d:  .s21/out10;
 in2a:  .s21/out5;
 in2b:  .s21/out4;
 in2c:  .s21/out6;
 in2d:  .s21/out11;
 in3a:  .s21/out9;
 in3b:  .s21/out7;
 in3c:  .s21/out8;
 in3d:  .s21/out12;

  // out1:   r14c7s19/in14;   // Out 1 to CR 7 pos19 In14
  // out2:   r14c7s19/in19;   // Out 2 to CR7 pos 19 In 19

 out1:  r14c7s19/in4b;
 out2:  r14c7s19/in5b;
 out3:  r14c7s19/in6b;
}

LIFO(r14c6s23)
{ //TODO: All connections here are messed up. Needs one-by-one check.
 in: r13c4s1u1/out7; //from tcal start

  //OTHER<CR 10 pos 10-23   Test signal>
  // :   /;   // To CR 10 pos 10-23   Test signal
 out1:   ;//.c10s13/test; //not really sure which CFD this is really going
 out2:   .c10s10/test; // signal too weak for s14
 out3:   .c10s15/test;
 out4:   .c10s11/test; // signal too weak for s16
 out5:   .c10s17/test;
 out6:   .c10s18/test;
 out7:   .c10s19/test;
 out8:   .c10s20/test;
 out9:   .c10s21/test;
 //out10:  .c10s22/test; signal too weak for s22. not sure if it's really out10.
 out11:  .c10s23/test;

 out12:  .s1/test;
 out13:  .s2/test;
 out14:  .s3/test;
 out15:  .s4/test;

 out16:  .s5/test;
}

CBV1000(r14c6s24)
{
  SETTING("CRATE_NO" => "6");

 vsb_in:  r12c2s24/vsb_out;
 vsb_out: r14c7s24/vsb_in;
}

DMS1000(r14c6s25)
{
  ;
}


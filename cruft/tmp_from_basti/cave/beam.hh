#include "rack25/crate0.hh" //this is the single NIM crate standing on top of rBEAMR1, next to the XB

MODULE(DETECTOR_POS)
{
  LABEL("POS detector");
}

UNIT(DETECTOR_POS_U)
{
  CONNECTOR(signal);
  CONNECTOR(hv);
}

MODULE(DETECTOR_ROLU)
{
  LABEL("ROLU detector");
}

UNIT(DETECTOR_ROLU_U)
{
  CONNECTOR(signal);
  CONNECTOR(hv);
}

MODULE(DETECTOR_PSP)
{
  LABEL("PSP detector");
}

UNIT(DETECTOR_PSP_U)
{
  CONNECTOR(signal);
  // CONNECTOR(hv);
}

MODULE(PSP_AMPLIFIER)
{
  HANDLER("MULTI_IN_OUT","signal=e,t");

  CONNECTOR(signal);

  // 

  CONNECTOR(hv); // input

  // These are not really connectors, but anyhow...

  CONNECTOR(e);  // output
  CONNECTOR(t);  // output
}

// Mesytec 8 channel preamp with individual bias
MODULE(PSP_MSI_8P)
{
  HANDLER("MULTI_IN_OUT","signal=e");
  CONNECTOR(signal); // signal input
  CONNECTOR(hv);     // bias input
  CONNECTOR(pulser); // pulser input
  CONNECTOR(e);      // preamp output
  CONNECTOR(lv);     // low voltage power
}

MODULE(DETECTOR_PIXEL)
{
  LABEL("PIXEL detector");

  CONNECTOR(signal);
  CONNECTOR(hv);
}

/* Beamline (before target) is POS1, ....
 *
 * Patch panel rack for beamline is rBEAMR1.
 */

/* POS1 rPOS1c1s1  */

DETECTOR_POS(rPOS1c1s1)
{
  ;
}

//FW: take out all HV in order to reduce problems... Should be re-included as soon as time allows.

DETECTOR_POS_U(rPOS1c1s1u1) { LABEL("POS01_01"); signal: rBEAMR1c2/in1; /*hv: r10c3s11/out0;*/  }
DETECTOR_POS_U(rPOS1c1s1u2) { LABEL("POS01_02"); signal: rBEAMR1c2/in2; /*hv: r10c3s11/out1;*/  }
DETECTOR_POS_U(rPOS1c1s1u3) { LABEL("POS01_03"); signal: rBEAMR1c2/in3; /*hv: r10c3s11/out2;*/  }
DETECTOR_POS_U(rPOS1c1s1u4) { LABEL("POS01_04"); signal: rBEAMR1c2/in4; /*hv: r10c3s11/out3;*/  }

/* ROLU1 rROLU1c1s1 */

DETECTOR_ROLU(rROLU1c1s1)
{
  ;
}

DETECTOR_ROLU_U(rROLU1c1s1u1) { LABEL("ROL01_01"); signal: rBEAMR1c2/in5; /*hv: r10c3s11/out4;*/  }
DETECTOR_ROLU_U(rROLU1c1s1u2) { LABEL("ROL01_02"); signal: rBEAMR1c2/in6; /*hv: r10c3s11/out7;*/  }
DETECTOR_ROLU_U(rROLU1c1s1u3) { LABEL("ROL01_03"); signal: rBEAMR1c2/in7; /*hv: r10c3s11/out9;*/  }
DETECTOR_ROLU_U(rROLU1c1s1u4) { LABEL("ROL01_04"); signal: rBEAMR1c2/in8; /*hv: r10c3s11/out10;*/  }

/* ROLU2 rROLU2c1s1 */
/* ROLU2 not used in s412, FW.
DETECTOR_ROLU(rROLU2c1s1)
{
  ;
}

DETECTOR_ROLU_U(rROLU2c1s1u1) { LABEL("ROL02_01"); //signal: rBEAMR1c2/in17; 
hv: r10c3s11/out11;  }
DETECTOR_ROLU_U(rROLU2c1s1u2) { LABEL("ROL02_02"); //signal: rBEAMR1c2/in18; 
hv: r10c3s11/out12;  }
DETECTOR_ROLU_U(rROLU2c1s1u3) { LABEL("ROL02_03"); //signal: rBEAMR1c2/in19; 
hv: r10c3s11/out13;  }
DETECTOR_ROLU_U(rROLU2c1s1u4) { LABEL("ROL02_04"); //signal: rBEAMR1c2/in20; 
hv: r10c3s11/out14;  }
*/


PATCH_PANEL_24(rBEAMR1c2)
{
  // POS 1 & ROL 1

 in1: rPOS1c1s1u1/signal; //POS01_01
 in2: rPOS1c1s1u2/signal; //POS01_02
 in3: rPOS1c1s1u3/signal; //POS01_03
 in4: rPOS1c1s1u4/signal; //POS01_04

 in5: rROLU1c1s1u1/signal;
 in6: rROLU1c1s1u2/signal;
 in7: rROLU1c1s1u3/signal;
 in8: rROLU1c1s1u4/signal;

//FW: this has changed for s412:
//out1_8: "KABEL 4 (25-32)" -> , r5c1/in1_8; //FW: actually now it is Kabel 0, length 110ns.
out1_8: "KABEL 0 110ns" -> , r13c6/in1_8; //now: r13c6/in1_8

 //in17: rROLU2c1s1u1/signal;
 //in18: rROLU2c1s1u2/signal;
 //in19: rROLU2c1s1u3/signal;
 //in20: rROLU2c1s1u4/signal;

 //out17_20: "KABEL 1 (1-4)" -> , r13c7/in5_8;
				 
}

//// RPC taken out by FW for the s412 experiment.
///* RPC */
//MODULE(DETECTOR_RPC)
//{
//  LABEL("RPC detector");
//}
//
//UNIT(DETECTOR_RPC_U)
//{
//  CONNECTOR(signal);
//  CONNECTOR(hv);
//}
//
///* This module I've never seen, perhaps it's completely wrong,
// * HTJ.
// */
//
//MODULE(PREAMP_RPC)
//{
//  HANDLER("MULTI_IN_OUT","in=t,e");
//
//  LEMO_INPUT(in1_8);
//  LEMO_OUTPUT(t1_8);
//  LEMO_OUTPUT(e1_8);
//
//  LEMO_OUTPUT(or); // used for trigger...
//}
//
//
//DETECTOR_RPC(rRPC1c1s1){ ; }
//DETECTOR_RPC(rRPC2c1s1){ ; }
//DETECTOR_RPC(rRPC3c1s1){ ; }
//DETECTOR_RPC(rRPC4c1s1){ ; }
//
//DETECTOR_RPC_U(rRPC1c1s1u1) { LABEL("RPC01_01"); //signal: rRPCRc1s1/in1; 
//hv: ;  }
//DETECTOR_RPC_U(rRPC1c1s1u2) { LABEL("RPC01_02"); //signal: rRPCLc1s1/in1; 
//hv: ;  }
//DETECTOR_RPC_U(rRPC2c1s1u1) { LABEL("RPC02_01"); //signal: rRPCRc1s1/in2; 
//hv: ;  }
//DETECTOR_RPC_U(rRPC2c1s1u2) { LABEL("RPC02_02"); //signal: rRPCLc1s1/in2; 
//hv: ;  }
//DETECTOR_RPC_U(rRPC3c1s1u1) { LABEL("RPC03_01"); //signal: rRPCRc1s1/in3; 
//hv: ;  }
//DETECTOR_RPC_U(rRPC3c1s1u2) { LABEL("RPC03_02"); //signal: rRPCLc1s1/in3; 
//hv: ;  }
//DETECTOR_RPC_U(rRPC4c1s1u1) { LABEL("RPC04_01"); //signal: rRPCRc1s1/in4; 
//hv: ;  }
//DETECTOR_RPC_U(rRPC4c1s1u2) { LABEL("RPC04_02"); //signal: rRPCLc1s1/in4; 
//hv: ;  }
//
//PREAMP_RPC(rRPCRc1s1)
//{
// //in1: rRPC1c1s1u1/signal;
// //in2: rRPC2c1s1u1/signal;
// //in3: rRPC3c1s1u1/signal;
// //in4: rRPC4c1s1u1/signal;
//
// //t1: r4c7s19u1/in;
// //t2: r4c7s21/in2;
// //t3_4: r4c7s19u2_3/in;
// //t3_4: r4c7s21/in11_12;
// //e1_4: r5c8/in1_4;
//
// or: ;
//}
//
//PREAMP_RPC(rRPCLc1s1)
//{
// //in1: rRPC1c1s1u2/signal;
// //in2: rRPC2c1s1u2/signal;
// //in3: rRPC3c1s1u2/signal;
// //in4: rRPC4c1s1u2/signal;
//
// //t1: r4c7s19u5/in;
// //t2: r4c7s19u6/in;
// //t3: r4c7s19u7/in;
// //t4: r4c7s19u8/in;
// //e1_4: r5c8/in5_8;
//
// or: ;
//}
//
/* RPCs Plastic Scintillators */
// Has not been plugged yet but should be soon.
//
//DETECTOR_SCI(rSCI1c1s1) { ; }
//
//DETECTOR_SCI_U(rSCI1c1s1u1) { LABEL("SCI01_01"); //signal: r13c7/in1;
//hv: r10c3s10/out0; }
//DETECTOR_SCI_U(rSCI1c1s1u2) { LABEL("SCI01_02"); //signal: r13c7/in2; 
//hv: r10c3s10/out1; }
//
//DETECTOR_SCI(rSCI2c1s2) { ; }
//
//DETECTOR_SCI_U(rSCI2c1s1u1) { LABEL("SCI02_01"); //signal: r13c7/in3; 
//hv: r10c3s10/out2; }
//DETECTOR_SCI_U(rSCI2c1s1u2) { LABEL("SCI02_02"); //signal: r13c7/in4; 
//hv: r10c3s10/out3; }

/*
	 out for cable length reason
PATCH_PANEL_24(rBEAMR2c1)
{
  // tentatively: 

  // RPC t 
 in1_4:   rRPCRc1s1/t1_4;
 in5_8:   rRPCLc1s1/t1_4;
	
 out1_8: r5c7/in1_8;

  // RPC e 
 in13_16:  rRPCRc1s1/e1_4;
 in17_20: rRPCLc1s1/e1_4;
 
 out13_20: r5c8/in1_8;
 
  // SCI 
 in9:     rSCI1c1s1u1/signal;
 in10:    rSCI1c1s1u2/signal;
 in11:    rSCI2c1s1u1/signal;
 in12:    rSCI2c1s1u2/signal;

 out9_12: r13c7/in1_4;
}
*/

/* PSP, PIXEL, PIN 1 */

DETECTOR_PSP(rPSP1c1s1)
{
  CONTROL("DAQ_ACTIVE","ON");
  ;
}

DETECTOR_PSP_U(rPSP1c1s1u1) { LABEL("PSP01_01"); signal: rPSP1c4s1/signal; hv: ;  }
DETECTOR_PSP_U(rPSP1c1s1u2) { LABEL("PSP01_02"); signal: rPSP1c4s2/signal; hv: ;  }
DETECTOR_PSP_U(rPSP1c1s1u3) { LABEL("PSP01_03"); signal: rPSP1c4s3/signal; hv: ;  }
DETECTOR_PSP_U(rPSP1c1s1u4) { LABEL("PSP01_04"); signal: rPSP1c4s4/signal; hv: ;  }
DETECTOR_PSP_U(rPSP1c1s1u5) { LABEL("PSP01_05"); signal: rPSP1c4s5/signal; hv: ;  }

//FW: the psp amplification will change to the same scheme as for PSP3. Thus take out the old docu for PSP1 and PSP2:
/*
PSP_AMPLIFIER(rPSP1c4s1) { signal: rPSP1c1s1u1/signal; e: rBEAMR1c3s9u1/in;  t: ; }
PSP_AMPLIFIER(rPSP1c4s2) { signal: rPSP1c1s1u2/signal; e: rBEAMR1c3s9u2/in;  t: ; }
PSP_AMPLIFIER(rPSP1c4s3) { signal: rPSP1c1s1u3/signal; e: rBEAMR1c3s9u3/in;  t: ; }
PSP_AMPLIFIER(rPSP1c4s4) { signal: rPSP1c1s1u4/signal; e: rBEAMR1c3s9u4/in;  t: ; }
PSP_AMPLIFIER(rPSP1c4s5) { signal: rPSP1c1s1u5/signal; e: rBEAMR1c3s13/in;  t: rBEAMR1c1/in6; }

EM7175(rBEAMR1c3s9)                   //AMP1003(r4c10s3)
{
  SERIAL("215"); ///5282
}

EM7175_U(rBEAMR1c3s9u1)
{
  SETTING("POLARITY" => "POS");
  SETTING("GAIN" => "200");
  
 in:  rPSP1c4s1/e;
 out: rBEAMR1c1/in1;
}

EM7175_U(rBEAMR1c3s9u2)
{
  SETTING("POLARITY" => "POS");
  SETTING("GAIN" => "200");

 in:  rPSP1c4s2/e;
 out: rBEAMR1c1/in2;
}

EM7175_U(rBEAMR1c3s9u3)
{
  SETTING("POLARITY" => "POS");
  SETTING("GAIN" => "200");

 in:  rPSP1c4s3/e;
 out: rBEAMR1c1/in3;
}

EM7175_U(rBEAMR1c3s9u4)
{
  SETTING("POLARITY" => "POS");
  SETTING("GAIN" => "200");

 in:  rPSP1c4s4/e;
 out: rBEAMR1c1/in4;
}

// Cathode
TC248(rBEAMR1c3s13)
{
  SERIAL("2296");
 in:       rPSP1c4s5/e;
 out_uni:  rBEAMR1c1/in5;
  // out_time: .s21/in2;
}
*/

//FW: will the pixel stay like this?
DETECTOR_PIXEL(rPSP1c3s1)
{
  LABEL("STR1_01");
  CONTROL("DAQ_ACTIVE","ON");

 hv:     ;
 signal: rBEAMR1c1/in8;
}


/* PSP, PIXEL, PIN 2 */

DETECTOR_PSP(rPSP2c1s1)
{
  CONTROL("DAQ_ACTIVE","ON");
  ;
}

DETECTOR_PSP_U(rPSP2c1s1u1) { LABEL("PSP02_01"); signal: rPSP2c4s1/signal; hv: ;  }
DETECTOR_PSP_U(rPSP2c1s1u2) { LABEL("PSP02_02"); signal: rPSP2c4s2/signal; hv: ;  }
DETECTOR_PSP_U(rPSP2c1s1u3) { LABEL("PSP02_03"); signal: rPSP2c4s3/signal; hv: ;  }
DETECTOR_PSP_U(rPSP2c1s1u4) { LABEL("PSP02_04"); signal: rPSP2c4s4/signal; hv: ;  }
DETECTOR_PSP_U(rPSP2c1s1u5) { LABEL("PSP02_05"); signal: rPSP2c4s5/signal; hv: ;  }

//FW: the psp amplification will change to the same scheme as for PSP3. Thus take out the old docu for PSP1 and PSP2:
/*
PSP_AMPLIFIER(rPSP2c4s1) { signal: rPSP2c1s1u1/signal; e: rBEAMR1c3s11u1/in;   t: ; }
PSP_AMPLIFIER(rPSP2c4s2) { signal: rPSP2c1s1u2/signal; e: rBEAMR1c3s11u2/in;  t: ; }
PSP_AMPLIFIER(rPSP2c4s3) { signal: rPSP2c1s1u3/signal; e: rBEAMR1c3s11u3/in;  t: ; }
PSP_AMPLIFIER(rPSP2c4s4) { signal: rPSP2c1s1u4/signal; e: rBEAMR1c3s11u4/in;  t: ; }
PSP_AMPLIFIER(rPSP2c4s5) { signal: rPSP2c1s1u5/signal; e: rBEAMR1c3s15/in;  t: rBEAMR1c1/in14; }


EM7175(rBEAMR1c3s11)
{
  SERIAL("213");
}

EM7175_U(rBEAMR1c3s11u1)
{
  SETTING("POLARITY" => "POS");
  SETTING("GAIN" => "200");

 in:  rPSP2c4s1/e;
 out: rBEAMR1c1/in9;
}

EM7175_U(rBEAMR1c3s11u2)
{
  SETTING("POLARITY" => "POS");
  SETTING("GAIN" => "200");

 in:  rPSP2c4s2/e;
 out: rBEAMR1c1/in10;
}

EM7175_U(rBEAMR1c3s11u3)
{
  SETTING("POLARITY" => "POS");
  SETTING("GAIN" => "200");

 in:  rPSP2c4s3/e;
 out: rBEAMR1c1/in11;
}

EM7175_U(rBEAMR1c3s11u4)
{
  SETTING("POLARITY" => "POS");
  SETTING("GAIN" => "200");

 in:  rPSP2c4s4/e;
 out: rBEAMR1c1/in12;
}

// Cathode
TC248(rBEAMR1c3s15)
{
  SERIAL("2291");
 in:       rPSP2c4s5/e;
 out_uni:  rBEAMR1c1/in13;
  // out_time: .s21/in3;
}
*/

//FW: will the pixel stay like this?
DETECTOR_PIXEL(rPSP2c3s1)
{
  LABEL("STR2_01");
  CONTROL("DAQ_ACTIVE","ON");

 hv:     ;
 signal: rBEAMR1c1/in16;
}

/* PSP, PIXEL, PIN 3 */

DETECTOR_PSP(rPSP3c1s1)
{
  CONTROL("DAQ_ACTIVE","ON");
  ;
}

DETECTOR_PSP_U(rPSP3c1s1u1) { LABEL("PSP03_01"); signal: rPSP3c4s1/signal; hv: ;  }
DETECTOR_PSP_U(rPSP3c1s1u2) { LABEL("PSP03_02"); signal: rPSP3c4s2/signal; hv: ;  }
DETECTOR_PSP_U(rPSP3c1s1u3) { LABEL("PSP03_03"); signal: rPSP3c4s3/signal; hv: ;  }
DETECTOR_PSP_U(rPSP3c1s1u4) { LABEL("PSP03_04"); signal: rPSP3c4s4/signal; hv: ;  }
DETECTOR_PSP_U(rPSP3c1s1u5) { LABEL("PSP03_05"); signal: rPSP3c4s5/signal; hv: ;  }

PSP_MSI_8P(rPSP3c4s1) { signal: rPSP3c1s1u1/signal; e: r25c0s07/in11;  t: ; }
PSP_MSI_8P(rPSP3c4s2) { signal: rPSP3c1s1u2/signal; e: r25c0s07/in12;  t: ; }
PSP_MSI_8P(rPSP3c4s3) { signal: rPSP3c1s1u3/signal; e: r25c0s07/in13;  t: ; }
PSP_MSI_8P(rPSP3c4s4) { signal: rPSP3c1s1u4/signal; e: r25c0s07/in14;  t: ; }
PSP_MSI_8P(rPSP3c4s5) { signal: rPSP3c1s1u5/signal; e: r25c0s07/in15;  t: ; }

//FW: will the pixel stay like this?
DETECTOR_PIXEL(rPSP3c3s1)
{
  LABEL("STR3_01");
  CONTROL("DAQ_ACTIVE","OFF");

 hv:     ;
 signal: rBEAMR1c1/in24;
}

//FW: this whole patch panel needs to be checked!
PATCH_PANEL_24(rBEAMR1c1)
{
 // PSP 1, 2, 3

 //CHECK:
 //FW: new Preamp structure...
 /*in1:  rBEAMR1c3s9u1/out; //rPSP1c4s1/e;
 in2:  rBEAMR1c3s9u2/out; //rPSP1c4s2/e;
 in3:  rBEAMR1c3s9u3/out; //rPSP1c4s3/e;
 in4:  rBEAMR1c3s9u4/out; //rPSP1c4s4/e;
 in5:  rBEAMR1c3s13/out_uni; //rPSP1c4s5/e;
 in6:  rPSP1c4s5/t;*/

 in8:  rPSP1c3s1/signal; // PIX 1

 //CHECK:
 //FW: new Preamp structure...
 /*in9:  rBEAMR1c3s11u1/out;   //rPSP2c4s1/e;
 in10: rBEAMR1c3s11u2/out;   //rPSP2c4s2/e;
 in11: rBEAMR1c3s11u3/out;   //rPSP2c4s3/e;
 in12: rBEAMR1c3s11u4/out;   //rPSP2c4s4/e;
 in13: rBEAMR1c3s15/out_uni; //rPSP2c4s5/e;
 in14: rPSP2c4s5/t;*/

 in16:  rPSP2c3s1/signal; // PIX 2

 /*in17: rPSP3c4s1/e;
 in18: rPSP3c4s2/e;
 in19: rPSP3c4s3/e;
 in20: rPSP3c4s4/e;
 in21: rPSP3c4s5/e;
 in23: rPSP3c4s5/t;*/

 in24:  rPSP3c3s1/signal; // PIX 3

 //FW: taken out temporarily until checked:
 //Cable labels might be wrong
 //out1_7:   "KABEL7 (49-55)" -> , r4c11/in9_15;
 out8:     "KABEL7 (56)"    -> , r13c8/in1;
 //out9_15:  "KABEL9 (1-7)"   -> , r4c11/in25_31;
 out16:    "KABEL9 (8)"     -> , r13c8/in2;
 //out17_21:        "(33-37)" -> , r4c11/in41_45;
 //out23:              "(38)" -> , r4c11/in46;
 // cable (39) is broken
 out24:              "(40)" -> , r13c8/in3;
 
}




/* POS2 r40c12  */
/*
DETECTOR_POS_U(r40c12s1) { LABEL("POS02_01"); signal: r41c2/in1; hv: ;  }
DETECTOR_POS_U(r40c12s2) { LABEL("POS02_02"); signal: r41c2/in2; hv: ;  }
DETECTOR_POS_U(r40c12s3) { LABEL("POS02_03"); signal: r41c2/in3; hv: ;  }
DETECTOR_POS_U(r40c12s4) { LABEL("POS02_04"); signal: r41c2/in4; hv: ;  }
*/
/* ROL2 r40c22 */
/*
DETECTOR_ROLU_U(r40c22s1) { LABEL("ROL02_01"); signal: r41c2/in5; hv: ;  }
DETECTOR_ROLU_U(r40c22s2) { LABEL("ROL02_02"); signal: r41c2/in6; hv: ;  }
DETECTOR_ROLU_U(r40c22s3) { LABEL("ROL02_03"); signal: r41c2/in7; hv: ;  }
DETECTOR_ROLU_U(r40c22s4) { LABEL("ROL02_04"); signal: r41c2/in8; hv: ;  }
*/
/* PSP1 r40c31 */
/*
DETECTOR_PSP(r40c31s1) { LABEL("PSP01_01"); signal: r41c3/in1; hv: ;  }
DETECTOR_PSP(r40c31s2) { LABEL("PSP01_02"); signal: r41c3/in2; hv: ;  }
DETECTOR_PSP(r40c31s3) { LABEL("PSP01_03"); signal: r41c3/in3; hv: ;  }
DETECTOR_PSP(r40c31s4) { LABEL("PSP01_04"); signal: r41c3/in4; hv: ;  }
DETECTOR_PSP(r40c31s5) { LABEL("PSP01_05"); signal: r41c3/in5; hv: ;  }
*/
/* PSP2 r40c31 */
/*
DETECTOR_PSP(r40c32s1) { LABEL("PSP02_01"); signal: r41c4/in1; hv: ;  }
DETECTOR_PSP(r40c32s2) { LABEL("PSP02_02"); signal: r41c4/in2; hv: ;  }
DETECTOR_PSP(r40c32s3) { LABEL("PSP02_03"); signal: r41c4/in3; hv: ;  }
DETECTOR_PSP(r40c32s4) { LABEL("PSP02_04"); signal: r41c4/in4; hv: ;  }
DETECTOR_PSP(r40c32s5) { LABEL("PSP02_05"); signal: r41c4/in5; hv: ;  }
*/

/* STR1 r40c41 */
/* PSP2 r40c32 */
/* STR2 r40c42 */
/* PIN1 r40c51 */
/* PSP3 r40c33 */
/* STR3 r40c43 */
/* PIN2 r40c52 */







// TODO: all patch panels below are not real yet.  Just here to make
// it work.
/*
PATCH_PANEL_8(r41c1)
{
  // POS 1 & ROL 1

 in1: r40c11s1/signal;
 in2: r40c11s2/signal;
 in3: r40c11s3/signal;
 in4: r40c11s4/signal;

 in5: r40c21s1/signal;
 in6: r40c21s2/signal;
 in7: r40c21s3/signal;
 in8: r40c21s4/signal;

 out1_8: "KABEL ??" -> , r13c6/in1_8;
}
*/
/*
PATCH_PANEL_8(r41c2)
{
  // POS 2 & ROL 2

 in1: r40c12s1/signal;
 in2: r40c12s2/signal;
 in3: r40c12s3/signal;
 in4: r40c12s4/signal;

 in5: r40c22s1/signal;
 in6: r40c22s2/signal;
 in7: r40c22s3/signal;
 in8: r40c22s4/signal;

 out1_8: "KABEL ??" -> , r13c7/in1_8;
}
*/

/*
PATCH_PANEL_8(r41c3)
{
  // PSP 1 (phony)

 in1: r40c31s1/signal;
 in2: r40c31s2/signal;
 in3: r40c31s3/signal;
 in4: r40c31s4/signal;
 in5: r40c31s5/signal;

 out1_5: r4c11/in9_13;
}
*/
/*
PATCH_PANEL_8(r41c4)
{
  // PSP 2 (phony)

 in1: r40c32s1/signal;
 in2: r40c32s2/signal;
 in3: r40c32s3/signal;
 in4: r40c32s4/signal;
 in5: r40c32s5/signal;

 out1_5: r4c11/in25_29;
}
*/
/*

PATCH_PANEL_48(r4c11)
{
in9_13: r41c3/out1_5;
in25_29: r41c4/out1_5;

 out9:   r4c10s3u1/in;
 out10:  r4c10s3u2/in;
 out11:  r4c10s3u3/in;
 out12:  r4c10s3u4/in;
 out13:  r4c10s9/in;

 out25:  r4c10s7u1/in;
 out26:  r4c10s7u2/in;
 out27:  r4c10s7u3/in;
 out28:  r4c10s7u4/in;
 out29:  r4c10s11/in;

}
*/

//Taken out by FW for s412 - does it exist?
///* Patch panel placed on the secondary beam line */
//PATCH_PANEL_16(rSECLINEc1)
//{
//	out1_16: r0c8/in1_16;
//}

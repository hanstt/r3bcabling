/*
 * Fastbus crate 2 in Cave C (ALADIN crate) (left).
 */

FASTBUS_CRATE(r11c2)
{
  ;
}

// Display module in slot 0

/////////////////////////////////////////////////

LECROY1875(r11c2s1)
{
  SERIAL("L_T87047");

 in0_15:  r12c2s21/outa1_16;
 in16_31: r12c3s21/outa1_16;
 in32_47: r12c4s21/outa1_16;
 in48_63: r12c5s18/outa1_16;
}

// LECROY1875(r11c2s2)
// {
//   SERIAL("L_T48854");

//  in0_15:  r12c2s20/outa1_16;
//  in16_31: r12c3s20/outa1_16;
//  in32_47: r12c4s20/outa1_16;
//  in48_63: r12c5s17/outa1_16;
// }

LECROY1875(r11c2s3)
{
  SERIAL("L_T81808");

 in0_15:  r12c2s19/outa1_16;
 in16_31: r12c3s19/outa1_16;
 in32_47: r12c4s19/outa1_16;
 in48_63: r12c5s16/outa1_16;
}

LECROY1875(r11c2s4)
{
  SERIAL("L_T81859");

 in0_15:  r12c2s18/outa1_16;
 in16_31: r12c3s18/outa1_16;
 in32_47: r12c5s19/outa1_16;
 in48_63: r12c5s15/outa1_16;
}

LECROY1875(r11c2s5)
{
  SERIAL("L_T81795");

 in0_15:  r12c2s17/outa1_16;
 in16_31: r12c3s17/outa1_16;
 in32_47: r12c4s17/outa1_16;
 in48_63: r14c7s12/outa1_16;
}

LECROY1875(r11c2s6)
{
  SERIAL("L_T81806");

 in0_15:  r12c2s16/outa1_16;
 in16_31: r12c3s16/outa1_16;
 in32_47: r12c4s16/outa1_16;
  ;
}

LECROY1875(r11c2s7)
{
  SERIAL("L_T48834");

 in0_15:  r12c2s15/outa1_16;
 in16_31: r12c3s15/outa1_16;
 in32_47: r12c4s15/outa1_16;
  ;
}

LECROY1875(r11c2s11)
{
  SERIAL("L_T48854");

 in0_15:  r12c2s20/outa1_16;
 in16_31: r12c3s20/outa1_16;
 in32_47: r12c4s20/outa1_16;
 in48_63: r12c5s17/outa1_16;
}

/////////////////////////////////////////////////

NGF(r11c2s12)
{
 in1: r13c4s1u1/out6;

 out3: r14c9s13u4/in1;
}

// This is not really correct, but as it is hardly used, lets pretend
// the NGF is in slot 0, and what is inside the NGF is in the following
// fastbus slots

TRIVA3(r11c2s13)
{
  out5: r12c11s15/in_ecl15;//DT!!
}

RIO3(r11c2s15)
{
//  SETTING("HOSTNAME"=>"r2-14");
  SETTING("HOSTNAME"=>"r2-17"); //installed 30.06.06 O.K.
}

/////////////////////////////////////////////////
//Fastbus QDC for old XB proton readout

LECROY1885F(r11c2s17)
{
  SERIAL("L_Q75266");

  //in0_7:   "Cable 1" <- "Cable 1",  r23c0s11/out8_1;  // Cable "1"
  //in8_15:  "Cable 2" <- "Cable 2",  r23c0s11/out16_9; // Cable "2"
  //in16_23: "Cable 3" <- "Cable 3",  r23c0s5/out8_1;   // Cable "3"
  //in24_31: "Cable 4" <- "Cable 4",  r23c0s5/out16_9;  // Cable "4"
  //in32_33: "Cable 5" <- "Cable 5",  r22c0s13/out8_7;  // Cable "5"
  //in35_39: "Cable 5" <- "Cable 5",  r22c0s13/out5_1;  // Cable "5"
  //in40_47: "Cable 6" <- "Cable 6",  r22c0s13/out16_9; // Cable "6"
  //in48_55: "Cable 7" <- "Cable 7",  r22c0s19/out8_1;  // Cable "7"
  //in56_63: "Cable 8" <- "Cable 8",  r22c0s19/out16_9; // Cable "8"
  //in64   : "Cable 5, ch6" <- "Cable 5, ch6", r22c0s13/out6; //Broken channel on QDC made this cable to be moved to ch64 instead of ch34 

	   
  //in0_15:   "1/2"
  //in16_31:  "3/4"
  //in32_47:  "5/6"
  //in48_63:  "7/8"
  
  gate:	r12c11s15/out_ecl16;
}


// CAT module
LECROY1810(r11c2s25)
{
  tdc_stop: r14c9s13u3/out2;
  adc_gate: r14c9s13u1/out2; // aka ARM
}


/*
 * VME crate 1 in cave.
 *
 */


VME_CRATE(r11c5)
{
  ;
}

E7(r11c5s10)
{
  SETTING("HOSTNAME"=>"e7_9");

 vsb_out: r11c1s24/vsb_in;
}

// TODO: check TRIVA number (it is an old gray GSI trigger module)
TRIVA1(r11c5s20) 
{
  in1_8: r12c11s15/out_ecl1_8;  
}

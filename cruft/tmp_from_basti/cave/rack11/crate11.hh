/* 
 * Crate 11 (far left, scalers)
 */

CAMAC_CRATE(r11c1)
{
  ;
}

SC4800(r11c1s1)
{
  SERIAL("SC6794"); // TODO: check

 in0_7:    "1/1" <- , r12c2s1/th1_8;
 in8_15:   "1/2" <- , r12c2s2/th1_8;
 in16_23:  "1/3" <- , r12c2s3/th1_8;
 in24_31:  "1/4" <- , r12c2s4/th1_8;
 in32_39:  "1/5" <- , r12c2s5/th1_8;
 in40_47:  "1/6" <- , r12c2s6/th1_8;
}
SC4800(r11c1s2)
{
  SERIAL("SC6444"); // TODO: check

 in0_7:    "2/1" <- , r12c2s7/th1_8;
 in8_15:   "2/2" <- , r12c2s8/th1_8;
 in16_23:  "2/3" <- , r12c2s9/th1_8;
 in24_31:  "2/4" <- , r12c2s10/th1_8;
 in32_39:  "2/5" <- , r12c2s11/th1_8;
 in40_47:  "2/6" <- , r12c2s12/th1_8;
}
SC4800(r11c1s3)
{
  SERIAL("SC6785"); // TODO: check

 in0_7:    "3/1" <- , r12c2s13/th1_8;
 in8_15:   "3/2" <- , r12c2s14/th1_8;
 in16_23:  "3/3" <- , r12c3s1/th1_8; 
 in24_31:  "3/4" <- , r12c3s2/th1_8;
 in32_39:  "3/5" <- , r12c3s3/th1_8;
 in40_47:  "3/6" <- , r12c3s4/th1_8;
}
SC4800(r11c1s4)
{
  SERIAL("SC6796"); // TODO: check

 in0_7:    "4/1" <- , r12c3s5/th1_8;
 in8_15:   "4/2" <- , r12c3s6/th1_8;
 in16_23:  "4/3" <- , r12c3s7/th1_8;
 in24_31:  "4/4" <- , r12c3s8/th1_8;
 in32_39:  "4/5" <- , r12c3s9/th1_8;
 in40_47:  "4/6" <- , r12c3s10/th1_8;
}
SC4800(r11c1s5)
{
  SERIAL("SC6756"); // TODO: check

 in0_7:    "5/1" <- , r12c3s11/th1_8;
 in8_15:   "5/2" <- , r12c3s12/th1_8;
 in16_23:  "5/3" <- , r12c3s13/th1_8;
 in24_31:  "5/4" <- , r12c3s14/th1_8;
 in32_39:  "5/5" <- , r12c4s1/th1_8;
 in40_47:  "5/6" <- , r12c4s2/th1_8;
}
SC4800(r11c1s6)
{
  SERIAL("SC6445"); // TODO: check

 in0_7:    "6/1" <- , r12c4s3/th1_8;
 in8_15:   "6/2" <- , r12c4s4/th1_8;
 in16_23:  "6/3" <- , r12c4s5/th1_8;
 in24_31:  "6/4" <- , r12c4s6/th1_8;
 in32_39:  "6/5" <- , r12c4s7/th1_8;
 in40_47:  "6/6" <- , r12c4s8/th1_8;
}
SC4800(r11c1s7)
{
  SERIAL("SC6446"); // TODO: check

 in0_7:    "7/1" <- , r12c4s9/th1_8;
 in8_15:   "7/2" <- , r12c4s10/th1_8;
 in16_23:  "7/3" <- , r12c4s11/th1_8;
 in24_31:  "7/4" <- , r12c4s12/th1_8;
 in32_39:  "7/5" <- , r12c4s13/th1_8;
 in40_47:  "7/6" <- , r12c4s14/th1_8;
}
SC4800(r11c1s8)
{
  SERIAL("SC0001"); // TODO: check

 in0_7:    "8/1" <- , r12c5s7/th1_8;
 in8_15:   "8/2" <- , r12c5s8/th1_8;
 in16_23:  "8/3" <- , r12c5s9/th1_8;
 in24_31:  "8/4" <- , r12c5s10/th1_8;
 in32_39:  "8/5" <- , r12c5s11/th1_8;
 in40_47:  "8/6" <- , r12c5s12/th1_8;
}

CBV1000(r11c1s24)
{
  SETTING("CRATE_NO" => "11");

 vsb_in:  r11c5s10/vsb_out;
 vsb_out: r12c5s24/vsb_in;
}

DMS1000(r11c1s25)
{
  ;
}


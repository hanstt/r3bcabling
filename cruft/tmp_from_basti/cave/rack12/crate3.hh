/* 
 * Crate 3
 */

CAMAC_CRATE(r12c3)
{
  ;
}

CF8103(r12c3s1)
{
  SERIAL("LCF6200");

 in1_8: "N35 CFN5"  <- , r13c1s8(SPLIT)/t1_8;
 th1_8: "3/3"       -> , r11c1s3(SCALER)/in16_23;   
 tb1_8: "CR3 SL1"   -> , .s15(DELAY)/in1_8;

 m:    .c11s5/in5;
 test:   .s23/out1;
 mux_tb: .s22/in1a;   mux_e: .s22/in5a;   mux_mon: .s22/in9a;
}
CF8103(r12c3s2)
{
  SERIAL("LCF6201");

 in1_8: "N45N5"     <- , r13c1s8(SPLIT)/t9_16;
 th1_8: "3/4"       -> , r11c1s3(SCALER)/in24_31;
 tb1_8: "CR3 SL2"   -> , .s15(DELAY)/in9_16;

 m:    .c11s5/in7;
 test:   .s23/out2;
 mux_tb: .s22/in1b;   mux_e: .s22/in5b;   mux_mon: .s22/in9b;
}
CF8103(r12c3s3)
{
  SERIAL("LCF6402");

 in1_8: "N41N1"     <- , r13c1s9(SPLIT)/t1_8;
 th1_8: "3/5"       -> , r11c1s3(SCALER)/in32_39;
 tb1_8: "CR3 SL3"   -> , .s16(DELAY)/in1_8;

 m:    .c11s5/in8;
 test:   .s23/out3;
 mux_tb: .s22/in1c;   mux_e: .s22/in5c;   mux_mon: .s22/in9c;
}
CF8103(r12c3s4)
{
  SERIAL("LCF6028");

 in1_8: "N42N2"     <- , r13c1s9(SPLIT)/t9_16;
 th1_8: "3/6"       -> , r11c1s3(SCALER)/in40_47;
 tb1_8: "CR3 SL4"   -> , .s16(DELAY)/in9_16;

 m:    .c11s5/in9;
 test:   .s23/out4;
 mux_tb: .s22/in1d;   mux_e: .s22/in5d;   mux_mon: .s22/in9d;
}
CF8103(r12c3s5)
{
  SERIAL("LCF6123");

 in1_8: "N43N3"     <- , r13c1s10(SPLIT)/t1_8; 
 th1_8: "4/1"       -> , r11c1s4(SCALER)/in0_7;
 tb1_8: "CR3 SL5"   -> , .s17(DELAY)/in1_8;

 m:    .c11s5/in10;
 test:   .s23/out5;
 mux_tb: .s22/in2a;   mux_e: .s22/in6a;   mux_mon: .s22/in10a;
}
CF8103(r12c3s6)
{
  SERIAL("LCF6029");

 in1_8: "N44N4"     <- , r13c1s10(SPLIT)/t9_16;
 th1_8: "4/2"       -> , r11c1s4(SCALER)/in8_15;
 tb1_8: "CR3 SL6"   -> , .s17(DELAY)/in9_16;

 m:    .c11s5/in11;
 test:   .s23/out6;
 mux_tb: .s22/in2b;   mux_e: .s22/in6b;   mux_mon: .s22/in10b;
}
CF8103(r12c3s7)
{
  SERIAL("LCF6325");

 in1_8: "N51N1"     <- , r13c1s11(SPLIT)/t1_8; 
 th1_8: "4/3"       -> , r11c1s4(SCALER)/in16_23;
 tb1_8: "CR3 SL7"   -> , .s18(DELAY)/in1_8;

 m:    .c11s7/in1;
 test:   .s23/out7;
 mux_tb: .s22/in2c;   mux_e: .s22/in6c;   mux_mon: .s22/in10c;
}
CF8103(r12c3s8)
{
  SERIAL("LCF6400");

 in1_8: "N52N2"     <- , r13c1s11(SPLIT)/t9_16;
 th1_8: "4/4"       -> , r11c1s4(SCALER)/in24_31;
 tb1_8: "CR3 SL8"   -> , .s18(DELAY)/in9_16;

 m:    .c11s7/in2;
 test:   .s23/out8;
 mux_tb: .s22/in2d;   mux_e: .s22/in6d;   mux_mon: .s22/in10d;
}
CF8103(r12c3s9)
{
  SERIAL("LCF6332");

 in1_8: "N53N3"     <- , r13c1s12(SPLIT)/t1_8;
 th1_8: "4/5"       -> , r11c1s4(SCALER)/in32_39;
 tb1_8: "CR3 SL9"   -> , .s19(DELAY)/in1_8;

 m:    .c11s7/in3;
 test:   .s23/out9;
 mux_tb: .s22/in3a;   mux_e: .s22/in7a;   mux_mon: .s22/in11a;
}
CF8103(r12c3s10)
{
  SERIAL("LCF6329");

 in1_8: "N54N4"     <- , r13c1s12(SPLIT)/t9_16;
 th1_8: "4/6"       -> , r11c1s4(SCALER)/in40_47;
 tb1_8: "CR3 SL10"  -> , .s19(DELAY)/in9_16;

 m:    .c11s7/in4;
 test:   .s23/out10;
 mux_tb: .s22/in3b;   mux_e: .s22/in7b;   mux_mon: .s22/in11b;
}
CF8103(r12c3s11)
{
  SERIAL("LCF6326");

 in1_8: "N55N5"     <- , r13c1s13(SPLIT)/t1_8; 
 th1_8: "5/1"       -> , r11c1s5(SCALER)/in0_7;
 tb1_8: "CR3 SL11"  -> , .s20(DELAY)/in1_8;

 m:    .c11s7/in5;
 test:   .s23/out11;
 mux_tb: .s22/in3c;   mux_e: .s22/in7c;   mux_mon: .s22/in11c;
}
CF8103(r12c3s12)
{
  SERIAL("LCF6125");

 in1_8: "N65N5"     <- , r13c1s13(SPLIT)/t9_16;
 th1_8: "5/2"       -> , r11c1s5(SCALER)/in8_15;
 tb1_8: "CR3 SL12"  -> , .s20(DELAY)/in9_16;

 m:    .c11s7/in7;
 test:   .s23/out12;
 mux_tb: .s22/in3d;   mux_e: .s22/in7d;   mux_mon: .s22/in11d;
}
CF8103(r12c3s13)
{
  SERIAL("LCF6330");

 in1_8: "N61N1"     <- , r13c1s14(SPLIT)/t1_8;
 th1_8: "5/3"       -> , r11c1s5(SCALER)/in16_23;  
 tb1_8: "CR3 SL13"  -> , .s21(DELAY)/in1_8;

 m:    .c11s7/in8;
 test:   .s23/out13;
 mux_tb: .s22/in4a;   mux_e: .s22/in8a;   mux_mon: .s22/in12a;
}
CF8103(r12c3s14)
{
  SERIAL("LCF6336");

 in1_8: "N62N2"     <- , r13c1s14(SPLIT)/t9_16;
 th1_8: "5/4"       -> , r11c1s5(SCALER)/in24_31; 
 tb1_8: "CR3 SL14"  -> , .s21(DELAY)/in9_16;

 m:    .c11s7/in9;
 test:   .s23/out14;
 mux_tb: .s22/in4b;   mux_e: .s22/in8b;   mux_mon: .s22/in12b;
}

DL1610(r12c3s15)
{
 in1_8:  <- "CR3 SL1"  , .s1/tb1_8;
 in9_16: <- "CR3 SL2"  , .s2/tb1_8;

 outa1_16: <-          , r11c2s7/in16_31;
}
DL1610(r12c3s16)
{
 in1_8:  <- "CR3 SL3"  , .s3/tb1_8;
 in9_16: <- "CR3 SL4"  , .s4/tb1_8;

 outa1_16: <-          , r11c2s6/in16_31;
}
DL1610(r12c3s17)
{
 in1_8:  <- "CR3 SL5"  , .s5/tb1_8;
 in9_16: <- "CR3 SL6"  , .s6/tb1_8;

 outa1_16: <-          , r11c2s5/in16_31;
}
DL1610(r12c3s18)
{
 in1_8:  <- "CR3 SL7"  , .s7/tb1_8;
 in9_16: <- "CR3 SL8"  , .s8/tb1_8;

 outa1_16: <-          , r11c2s4/in16_31;
}
DL1610(r12c3s19)
{
 in1_8:  <- "CR3 SL9"  , .s9/tb1_8;
 in9_16: <- "CR3 SL12" , .s10/tb1_8;

 outa1_16: <-          , r11c2s3/in16_31;
}
DL1610(r12c3s20)
{
 in1_8:  <- "CR3 SL11" , .s11/tb1_8;
 in9_16: <- "CR3 SL12" , .s12/tb1_8;

// outa1_16: <-          , r11c2s2/in16_31;
 outa1_16: <-          , r11c2s11/in16_31;
}
DL1610(r12c3s21)
{
 in1_8:  <- "CR3 SL13" , .s13/tb1_8;
 in9_16: <- "CR3 SL14" , .s14/tb1_8;

 outa1_16: <-          , r11c2s1/in16_31;
}

RMX3(r12c3s22)
{
  SERIAL("L_MPX2");

 in1a:  .s1/mux_tb;    in5a:  .s1/mux_e;    in9a:  .s1/mux_mon; 
 in1b:  .s2/mux_tb;    in5b:  .s2/mux_e;    in9b:  .s2/mux_mon; 
 in1c:  .s3/mux_tb;    in5c:  .s3/mux_e;    in9c:  .s3/mux_mon; 
 in1d:  .s4/mux_tb;    in5d:  .s4/mux_e;    in9d:  .s4/mux_mon; 
 in2a:  .s5/mux_tb;    in6a:  .s5/mux_e;    in10a: .s5/mux_mon; 
 in2b:  .s6/mux_tb;    in6b:  .s6/mux_e;    in10b: .s6/mux_mon; 
 in2c:  .s7/mux_tb;    in6c:  .s7/mux_e;    in10c: .s7/mux_mon; 
 in2d:  .s8/mux_tb;    in6d:  .s8/mux_e;    in10d: .s8/mux_mon; 
 in3a:  .s9/mux_tb;    in7a:  .s9/mux_e;    in11a: .s9/mux_mon; 
 in3b:  .s10/mux_tb;   in7b:  .s10/mux_e;   in11b: .s10/mux_mon;
 in3c:  .s11/mux_tb;   in7c:  .s11/mux_e;   in11c: .s11/mux_mon;
 in3d:  .s12/mux_tb;   in7d:  .s12/mux_e;   in11d: .s12/mux_mon;
 in4a:  .s13/mux_tb;   in8a:  .s13/mux_e;   in12a: .s13/mux_mon;
 in4b:  .s14/mux_tb;   in8b:  .s14/mux_e;   in12b: .s14/mux_mon;

 out1_12: r14c7s1/in4a_6d;
}

LIFO(r12c3s23)
{
 in: r13c4s1u1/out3; // TCAL

 out1:  .s1/test;
 out2:  .s2/test;
 out3:  .s3/test;
 out4:  .s4/test;
 out5:  .s5/test;
 out6:  .s6/test;
 out7:  .s7/test;
 out8:  .s8/test;
 out9:  .s9/test;
 out10: .s10/test;
 out11: .s11/test;
 out12: .s12/test;
 out13: .s13/test;
 out14: .s14/test;
}

CBV1000(r12c3s24)
{
  SETTING("CRATE_NO" => "3");

 vsb_in:  r12c4s24/vsb_out;
 vsb_out: r12c2s24/vsb_in;
}

DMS1000(r12c3s25)
{
  ;
}





/*
 * Crate 5
 */

CAMAC_CRATE(r12c5)
{
  ;  
}

// Slot 1 empty

DATAWAY_DISPLAY(r12c5s2)
{
  ;
}

// Slot 3 empty
// Slot 4 empty
// Slot 5 empty

AT8000(r12c5s6)
{
  ;
}
CF8103(r12c5s7)
{
  SERIAL("LCF6549");

 in1_8: "N93 CF"    <- , r13c3s8(SPLIT)/t1_8; 
 th1_8: "8/1"       -> , r11c1s8/in0_7;   
 tb1_8: "CR2 SL1"   -> , .s15(DELAY)/in1_8;

 m:    .c11s13/in3;
 test:   .s23/out1;
 mux_tb: "1" -> , .s22/in1a;   mux_e: "5" -> , .s22/in5a;   mux_mon: "9" -> , .s22/in9a;
}
CF8103(r12c5s8)
{
  SERIAL("LCF6550");

 in1_8: "N44"       <- , r13c3s8(SPLIT)/t9_16;
 th1_8: "8/2"       -> , r11c1s8/in8_15;  
 tb1_8: "CR2 SL2"   -> , .s15(DELAY)/in9_16;

 m:    .c11s13/in4;
 test:   .s23/out2;
 mux_tb: "2" -> , .s22/in1b;   mux_e: "6" -> , .s22/in5b;   mux_mon: "10"-> , .s22/in9b;
}
CF8103(r12c5s9)
{
  SERIAL("LCF6327");

 in1_8: "N95 CF"    <- , r13c3s9(SPLIT)/t1_8; 
 th1_8: "8/3"       -> , r11c1s8/in16_23; 
 tb1_8: "CR2 SL3"   -> , .s16(DELAY)/in1_8;

 m:    .c11s13/in5;
 test:   .s23/out3;
 mux_tb: "3" -> , .s22/in1c;   mux_e: "7" -> , .s22/in5c;   mux_mon: "11"-> , .s22/in9c;
}
CF8103(r12c5s10)
{
  SERIAL("LCF6331");

 in1_8: "N105 CF"   <- , r13c3s9(SPLIT)/t9_16; 
 th1_8: "8/4"       -> , r11c1s8/in24_31; 
 tb1_8: "CR2 SL4"   -> , .s16(DELAY)/in9_16;

 m:    .c11s13/in7;
 test:   .s23/out4;
 mux_tb: "4" -> , .s22/in1d;   mux_e: "8" -> , .s22/in5d;   mux_mon: "12"-> , .s22/in9d;
}
CF8103(r12c5s11)
{
  SERIAL("LCF6203");

 in1_8: "N47"       <- , r13c3s10(SPLIT)/t1_8; 
 th1_8: "8/5"       -> , r11c1s8/in32_39; 
 tb1_8: "CR2 SL5"   -> , .s17(DELAY)/in1_8;

 m:    .c11s13/in8;
 test:   .s23/out5;
 mux_tb: "5" -> , .s22/in2a;   mux_e: "9" -> , .s22/in6a;   mux_mon: "1" -> , .s22/in10a;
}
CF8103(r12c5s12)
{
  SERIAL("LCF6410");

 in1_8: "N102 CF"   <- , r13c3s10(SPLIT)/t9_16; 
 th1_8: "8/6"       -> , r11c1s8/in40_47; 
 tb1_8: "CR2 SL6"   -> , .s17(DELAY)/in9_16;

 m:    .c11s13/in9;
 test:   .s23/out6;
 mux_tb: "6" -> , .s22/in2b;   mux_e: "10"-> , .s22/in6b;   mux_mon: "2" -> , .s22/in10b;
}
CF8103(r12c5s13)
{
  SERIAL("LCF6397");

 in1_8: "N103 CF"   <- , r13c3s11(SPLIT)/t1_8; 
 th1_8: "9/1"       -> , r14c8s9/in0_7;   
 tb1_8: "CR2 SL7"   -> , .s18(DELAY)/in1_8;

 m:    .c11s13/in10;
 test:   .s23/out7;
 mux_tb: "7" -> , .s22/in2c;   mux_e: "11"-> , .s22/in6c;   mux_mon: "3" -> , .s22/in10c;
}
CF8103(r12c5s14)
{
  SERIAL("LCF6403");

 in1_8: "N50"       <- , r13c3s11(SPLIT)/t9_16;
 th1_8: "9/2"       -> , r14c8s9/in8_15;  
 tb1_8: "CR2 SL8"   -> , .s18(DELAY)/in9_16;

 m:    .c11s13/in11;
 test:   .s23/out8;
 mux_tb: "8" -> , .s22/in2d;   mux_e: "12"-> , .s22/in6d;   mux_mon: "4" -> , .s22/in10d;
}
DL1610(r12c5s15)
{
 in1_8:   <- "CR2 SL7"  , .s7/tb1_8;
 in9_16:  <- "CR2 SL8"  , .s8/tb1_8;

 outa1_16: -> "N92 S22/4", r11c2s4/in48_63;
}
DL1610(r12c5s16)
{
 in1_8:   <- "CR2 SL9"  , .s9/tb1_8;
 in9_16:  <- "CR2 SL10" , .s10/tb1_8;

 outa1_16: -> "N93 S23/4", r11c2s3/in48_63;
}
DL1610(r12c5s17)
{
 in1_8:   <- "CR2 SL11" , .s11/tb1_8;
 in9_16:  <- "CR2 SL12" , .s12/tb1_8;

// outa1_16: -> "N101 S24/4",r11c2s2/in48_63;
 outa1_16: -> "N101 S24/4",r11c2s11/in48_63;
}
DL1610(r12c5s18)
{
 in1_8:   <- "CR2 SL13" , .s13/tb1_8;
 in9_16:  <- "CR2 SL14" , .s14/tb1_8;

 outa1_16: -> "N102 S25/4",r11c2s1/in48_63;
}

DL1610(r12c5s19)
{
 in1_8:   <- "CR4 SL7" , r12c4s7/tb1_8;
 in9_16:  <- "CR4 SL8" , r12c4s8/tb1_8;

 outa1_16: -> ,  r11c2s4(TDC)/in32_47;
}


// Slot 20 empty

LECROY2132(r12c5s21)
{
  SERIAL("HVC_LAND"); // DUMMY, for LAND HV (if needed)
  /* 
   * Controller High Voltage
   */
 j0: r16c1s19/j1; // to HV module
}

RMX3(r12c5s22)
{
  SERIAL("L_MPX4");

 in1a:  <- "1", .s7/mux_tb;    in5a:  <- "5", .s7/mux_e;    in9a:  <- "9", .s7/mux_mon; 
 in1b:  <- "2", .s8/mux_tb;    in5b:  <- "6", .s8/mux_e;    in9b:  <-"10", .s8/mux_mon; 
 in1c:  <- "3", .s9/mux_tb;    in5c:  <- "7", .s9/mux_e;    in9c:  <-"11", .s9/mux_mon;  
 in1d:  <- "4", .s10/mux_tb;   in5d:  <- "8", .s10/mux_e;   in9d:  <-"12", .s10/mux_mon; 
 in2a:  <- "5", .s11/mux_tb;   in6a:  <- "9", .s11/mux_e;   in10a: <- "1", .s11/mux_mon; 
 in2b:  <- "6", .s12/mux_tb;   in6b:  <-"10", .s12/mux_e;   in10b: <- "2", .s12/mux_mon; 
 in2c:  <- "7", .s13/mux_tb;   in6c:  <-"11", .s13/mux_e;   in10c: <- "3", .s13/mux_mon; 
 in2d:  <- "8", .s14/mux_tb;   in6d:  <-"12", .s14/mux_e;   in10d: <- "4", .s14/mux_mon; 
 /*
 in1a:  .s7/mux_tb;    in4a:  .s8/mux_e;    in6a:  .s7/mux_mon; 
 in1b:  .s8/mux_tb;    in4b:  .s9/mux_e;    in6b:  .s8/mux_mon; 
 in1c:  .s9/mux_tb;    in4c:  .s0/mux_e;    in6c:  .s9/mux_mon; 
 in1d:  .s10/mux_tb;   in4d:  .s10/mux_e;   in6d:  .s10/mux_mon; 
 in2a:  .s11/mux_tb;   in5a:  .s11/mux_e;   in10a: .s11/mux_mon; 
 in2b:  .s12/mux_tb;   in5b:  .s12/mux_e;   in10b: .s12/mux_mon; 
 in2c:  .s13/mux_tb;   in5c:  .s13/mux_e;   in10c: .s13/mux_mon; 
 in2d:  .s14/mux_tb;   in5d:  .s14/mux_e;   in10d: .s14/mux_mon; 
 */
 out1_12: r14c7s1/in10a_12d;
}

LIFO(r12c5s23)
{
 in: 		r13c4s1u1/out1; // TCAL

 out1:  .s7/test;
 out2:  .s8/test;
 out3:  .s9/test;
 out4:	.s10/test;
 out5:	.s11/test;
 out6:	.s12/test;
 out7:	.s13/test;
 out8:	.s14/test;

 out13: r14c10s13/test;
 out14: r14c10s22/test;
 out15: r14c8s5/test;
 out16: r14c10s16/test;

}

CBV1000(r12c5s24)
{
  SETTING("CRATE_NO" => "5");

 vsb_in:  r11c1s24/vsb_out;
 vsb_out: r12c4s24/vsb_in;
}

DMS1000(r12c5s25)
{
  ;
}


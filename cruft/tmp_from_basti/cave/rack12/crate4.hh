/* 
 * Crate 4
 */

CAMAC_CRATE(r12c4)
{
  ;
}

CF8103(r12c4s1)
{
  SERIAL("LCF6406");
  
 in1_8: "N63 CFN3"  <- , r13c3s1(SPLIT)/t1_8; 
 th1_8: "5/5"       -> , r11c1s5(SCALER)/in32_39;   
 tb1_8: "CR4 SL1"   -> , .s15(DELAY)/in1_8;

 m:    .c11s7/in10;
 test:   .s23/out1;
 mux_tb: .s22/in1a;   mux_e: .s22/in5a;   mux_mon: .s22/in9a;
}
CF8103(r12c4s2)
{
  SERIAL("LCF6399");
  
 in1_8: "N64 CFN4"  <- , r13c3s1(SPLIT)/t9_16; 
 th1_8: "5/6"       -> , r11c1s5(SCALER)/in40_47;
 tb1_8: "CR4 SL2"   -> , .s15(DELAY)/in9_16;

 m:    .c11s7/in11;
 test:   .s23/out2;
 mux_tb: .s22/in1b;   mux_e: .s22/in5b;   mux_mon: .s22/in9b;
}
CF8103(r12c4s3)
{
  SERIAL("LCF6398");
  
 in1_8: "N71 CFN1"  <- , r13c3s2(SPLIT)/t1_8; 
 th1_8: "6/1"       -> , r11c1s6(SCALER)/in0_7;
 tb1_8: "CR4 SL3"   -> , .s16(DELAY)/in1_8;

 m:    .c11s11/in1;
 test:   .s23/out3;
 mux_tb: .s22/in1c;   mux_e: .s22/in5c;   mux_mon: .s22/in9c;
}
CF8103(r12c4s4)
{
  SERIAL("LCF6558");
  
 in1_8: "N72 CFN2"  <- , r13c3s2(SPLIT)/t9_16; 
 th1_8: "6/2"       -> , r11c1s6(SCALER)/in8_15;
 tb1_8: "CR4 SL4"   -> , .s16(DELAY)/in9_16;

 m:    .c11s11/in2;
 test:   .s23/out4;
 mux_tb: .s22/in1d;   mux_e: .s22/in5d;   mux_mon: .s22/in9d;
}
CF8103(r12c4s5)
{
  SERIAL("LCF6555");
  
 in1_8: "N73 CFN3"  <- , r13c3s3(SPLIT)/t1_8; 
 th1_8: "6/3"       -> , r11c1s6(SCALER)/in16_23;
 tb1_8: "CR4 SL5"   -> , .s17(DELAY)/in1_8;

 m:    .c11s11/in3;
 test:   .s23/out5;
 mux_tb: .s22/in2a;   mux_e: .s22/in6a;   mux_mon: .s22/in10a;
}
CF8103(r12c4s6)
{
  SERIAL("LCF6333");
  
 in1_8: "N74 CFN4"  <- , r13c3s3(SPLIT)/t9_16;
 th1_8: "6/4"       -> , r11c1s6(SCALER)/in24_31;
 tb1_8: "CR4 SL6"   -> , .s17(DELAY)/in9_16;

 m:    .c11s11/in4;
 test:   .s23/out6;
 mux_tb: .s22/in2b;   mux_e: .s22/in6b;   mux_mon: .s22/in10b;
}
CF8103(r12c4s7)
{
  SERIAL("LCF6551");
  
 in1_8: "N75 CFN5"  <- , r13c3s4(SPLIT)/t1_8; 
 th1_8: "6/5"       -> , r11c1s6(SCALER)/in32_39;
 tb1_8: "CR4 SL7"   -> , r12c5s19(DELAY)/in1_8;

 m:    .c11s11/in5;
 test:   .s23/out7;
 mux_tb: .s22/in2c;   mux_e: .s22/in6c;   mux_mon: .s22/in10c;
}
CF8103(r12c4s8)
{
  SERIAL("LCF6149");
  
 in1_8: "N86 CFN6"  <- , r13c3s4(SPLIT)/t9_16;
 th1_8: "6/6"       -> , r11c1s6(SCALER)/in40_47;
 tb1_8: "CR4 SL8"   -> , r12c5s19(DELAY)/in9_16;

 m:    .c11s11/in7;
 test:   .s23/out8;
 mux_tb: .s22/in2d;   mux_e: .s22/in6d;   mux_mon: .s22/in10d;
}
CF8103(r12c4s9)
{
  SERIAL("LCF6557");
  
 in1_8: "N81 CFN1"  <- , r13c3s5(SPLIT)/t1_8; 
 th1_8: "7/1"       -> , r11c1s7(SCALER)/in0_7;
 tb1_8: "CR4 SL9"   -> , .s19(DELAY)/in1_8;

 m:    .c11s11/in8;
 test:   .s23/out9;
 mux_tb: .s22/in3a;   mux_e: .s22/in7a;   mux_mon: .s22/in11a;
}
CF8103(r12c4s10)
{
  SERIAL("LCF6408");
  
 in1_8: ""          <- , r13c3s5(SPLIT)/t9_16;
 th1_8: "7/2"       -> , r11c1s7(SCALER)/in8_15;
 tb1_8: "CR4 SL10"  -> , .s19(DELAY)/in9_16;

 m:    .c11s11/in9;
 test:   .s23/out10;
 mux_tb: .s22/in3b;   mux_e: .s22/in7b;   mux_mon: .s22/in11b;
}
CF8103(r12c4s11)
{
  SERIAL("LCF6407");
  
 in1_8: "N83 CFN3"  <- , r13c3s6(SPLIT)/t1_8;
 th1_8: "7/3"       -> , r11c1s7(SCALER)/in16_23;
 tb1_8: "CR4 SL11"  -> , .s20(DELAY)/in1_8;

 m:    .c11s11/in10;
 test:   .s23/out11;
 mux_tb: .s22/in3c;   mux_e: .s22/in7c;   mux_mon: .s22/in11c;
}
CF8103(r12c4s12)
{
  SERIAL("LCF6423");
  
 in1_8: "N84 CFN4"  <- , r13c3s6(SPLIT)/t9_16;
 th1_8: "7/4"       -> , r11c1s7(SCALER)/in24_31;
 tb1_8: "CR4 SL12"  -> , .s20(DELAY)/in9_16;

 m:    .c11s11/in11;
 test:   .s23/out12;
 mux_tb: .s22/in3d;   mux_e: .s22/in7d;   mux_mon: .s22/in11d;
}
CF8103(r12c4s13)
{
  SERIAL("LCF6553");
  
 in1_8: "N91 CFN1"  <- , r13c3s7(SPLIT)/t1_8;
 th1_8: "7/5"       -> , r11c1s7(SCALER)/in32_39;  
 tb1_8: "CR4 SL13"  -> , .s21(DELAY)/in1_8;

 m:    .c11s13/in1;
 test:   .s23/out13;
 mux_tb: .s22/in4a;   mux_e: .s22/in8a;   mux_mon: .s22/in12a;
}
CF8103(r12c4s14)
{
  SERIAL("LCF6552");
  
 in1_8: "N92 CFN2"  <- , r13c3s7(SPLIT)/t9_16; 
 th1_8: "7/6"       -> , r11c1s7(SCALER)/in40_47; 
 tb1_8: "CR4 SL14"  -> , .s21(DELAY)/in9_16;

 m:    .c11s13/in2;
 test:   .s23/out14;
 mux_tb: .s22/in4b;   mux_e: .s22/in8b;   mux_mon: .s22/in12b;
}

DL1610(r12c4s15)
{
 in1_8:  <- "CR4 SL1"  , .s1/tb1_8;
 in9_16: <- "CR4 SL2"  , .s2/tb1_8;

 outa1_16:               r11c2s7(TDC)/in32_47;   
}
DL1610(r12c4s16)
{
 in1_8:  <- "CR4 SL3"  , .s3/tb1_8;
 in9_16: <- "CR4 SL4"  , .s4/tb1_8;

 outa1_16:               r11c2s6(TDC)/in32_47;   
}
DL1610(r12c4s17)
{
 in1_8:  <- "CR4 SL5"  , .s5/tb1_8;
 in9_16: <- "CR4 SL6"  , .s6/tb1_8;

 outa1_16:               r11c2s5(TDC)/in32_47;   
}
//DL1610(r12c4s18) //broken CAMAC slot: module moved to r12c5s19
//{
// in1_8:  <- "CR4 SL7"  , .s7/tb1_8;
// in9_16: <- "CR4 SL8"  , .s8/tb1_8;
// 
// outa1_16:               r11c2s4(TDC)/in32_47;   
//}
DL1610(r12c4s19)
{
 in1_8:   <- "CR4 SL9"  , .s9/tb1_8;
 in9_16:  <- "CR4 SL12" , .s10/tb1_8;

 outa1_16:-> "N81  TDC S23/3", r11c2s3(TDC)/in32_47;
}
DL1610(r12c4s20)
{
 in1_8:   <- "CR4 SL11" , .s11/tb1_8;
 in9_16:  <- "CR4 SL12" , .s12/tb1_8;

// outa1_16:-> "N82 TDC S25/3", r11c2s2(TDC)/in32_47; 
 outa1_16:-> "N82 TDC S25/3", r11c2s11(TDC)/in32_47; 
}
DL1610(r12c4s21)
{
 in1_8:   <- "CR4 SL13" , .s13/tb1_8;
 in9_16:  <- "CR4 SL14" , .s14/tb1_8;

 outa1_16:-> "N91 TDC S25/3", r11c2s1(TDC)/in32_47; 
}

RMX3(r12c4s22)
{
  SERIAL("L_MPX3");
  
 in1a:  .s1/mux_tb;    in5a:  .s1/mux_e;    in9a:  .s1/mux_mon; 
 in1b:  .s2/mux_tb;    in5b:  .s2/mux_e;    in9b:  .s2/mux_mon; 
 in1c:  .s3/mux_tb;    in5c:  .s3/mux_e;    in9c:  .s3/mux_mon; 
 in1d:  .s4/mux_tb;    in5d:  .s4/mux_e;    in9d:  .s4/mux_mon; 
 in2a:  .s5/mux_tb;    in6a:  .s5/mux_e;    in10a: .s5/mux_mon; 
 in2b:  .s6/mux_tb;    in6b:  .s6/mux_e;    in10b: .s6/mux_mon; 
 in2c:  .s7/mux_tb;    in6c:  .s7/mux_e;    in10c: .s7/mux_mon; 
 in2d:  .s8/mux_tb;    in6d:  .s8/mux_e;    in10d: .s8/mux_mon; 
 in3a:  .s9/mux_tb;    in7a:  .s9/mux_e;    in11a: .s9/mux_mon; 
 in3b:  .s10/mux_tb;   in7b:  .s10/mux_e;   in11b: .s10/mux_mon;
 in3c:  .s11/mux_tb;   in7c:  .s11/mux_e;   in11c: .s11/mux_mon;
 in3d:  .s12/mux_tb;   in7d:  .s12/mux_e;   in11d: .s12/mux_mon;
 in4a:  .s13/mux_tb;   in8a:  .s13/mux_e;   in12a: .s13/mux_mon;
 in4b:  .s14/mux_tb;   in8b:  .s14/mux_e;   in12b: .s14/mux_mon;

 out1_12: r14c7s1/in7a_9d;
}

LIFO(r12c4s23)
{
 in: 		r13c4s1u1/out2; // TCAL

 out1:  .s1/test;
 out2:  .s2/test;
 out3:  .s3/test;
 out4:  .s4/test;
 out5:  .s5/test;
 out6:  .s6/test;
 out7:  .s7/test;
 out8:  .s8/test;
 out9:  .s9/test;
 out10: .s10/test;
 out11: .s11/test;
 out12: .s12/test;
 out13: .s13/test;
 out14: .s14/test;
}

CBV1000(r12c4s24)
{
  SETTING("CRATE_NO" => "4");

 vsb_in:  r12c5s24/vsb_out;
 vsb_out: r12c3s24/vsb_in;
}

DMS1000(r12c4s25)
{
  ;
}
















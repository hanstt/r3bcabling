/* 
 * Crate 11 (sum units for land multiplicty)
 */

CAMAC_CRATE(r12c11)
{
  ;
}

// Slot 1 empty

SU1200(r12c11s3)
{
 in1:         r12c2s1/m;
 in2:         r12c2s2/m;
 in3:         r12c2s3/m;
 in4:         r12c2s4/m;
 in5:         r12c2s5/m;
 in7:         r12c2s6/m;
 in8:         r12c2s7/m;
 in9:         r12c2s8/m;
 in10:        r12c2s9/m;
 in11:        r12c2s10/m;

 analog_out:  r12c11s9/in1;  
}

SU1200(r12c11s5)
{
 in1:         r12c2s11/m;  
 in2:         r12c2s12/m;  
 in3:         r12c2s13/m;  
 in4:         r12c2s14/m;  
 in5:         r12c3s1/m;   
 in7:         r12c3s2/m;
 in8:         r12c3s3/m;
 in9:         r12c3s4/m;
 in10:        r12c3s5/m;
 in11:        r12c3s6/m;

 analog_out:  r12c11s9/in2;  
}

SU1200(r12c11s7)
{
 in1:         r12c3s7/m;  
 in2:         r12c3s8/m;  
 in3:         r12c3s9/m;  
 in4:         r12c3s10/m;  
 in5:         r12c3s11/m;  
 in7:         r12c3s12/m; 
 in8:         r12c3s13/m; 
 in9:         r12c3s14/m; 
 in10:        r12c4s1/m;
 in11:        r12c4s2/m;

 analog_out:  r12c11s9/in3;
}

SU1200(r12c11s9)
{
  LABEL("M_LAND");

 in1:         .c11s3/analog_out;
 in2:         .c11s5/analog_out;
 in3:         .c11s7/analog_out;
 in4:         .c11s11/analog_out;
 in5:         .c11s13/analog_out;

 analog_out: r13c4s19u1/in;  // Analog out to LIFO, then an external CFD
}

SU1200(r12c11s11)
{
 in1:       r12c4s3/m;
 in2:       r12c4s4/m;
 in3:       r12c4s5/m;
 in4:       r12c4s6/m;
 in5:       r12c4s7/m;
 in7:       r12c4s8/m;  
 in8:       r12c4s9/m;  
 in9:       r12c4s10/m;  
 in10:      r12c4s11/m;  
 in11:      r12c4s12/m;  
 analog_out:  r12c11s9/in4;  
}

SU1200(r12c11s13)
{
 in1:        r12c4s13/m;
 in2:        r12c4s14/m;
 in3:        r12c5s7/m;  
 in4:        r12c5s8/m;  
 in5:        r12c5s9/m;  
 in7:        r12c5s10/m;
 in8:        r12c5s11/m;
 in9:        r12c5s12/m;
 in10:       r12c5s13/m;
 in11:       r12c5s14/m;
 
 analog_out:  r12c11s9/in5;
}

EC1601(r12c11s15)
{
  in1:		r13c11/front22;
  in2:		r13c11/front23;
  in16:		r14c9s9u8/out1;
		
  out14:	r13c11/front20;
  out15:	r13c11/front19;

  in_ecl9:	;// TODO: r13c2s23/cip;
  in_ecl10:	;// TODO: gate chain...
  in_ecl14:     r13c2s1/out5;
  in_ecl15:     r11c2s13/out5;
		
  out_ecl1_8:	r11c5s20/in1_8;
  out_ecl16:	r11c2s17/gate;
}
// Slot 17 empty
// Slot 19 empty
// Slot 21 empty
// Slot 23 empty
// Slot 25 empty

/* 
 * Crate 2
 */

CAMAC_CRATE(r12c2)
{
  ;
}

CF8103(r12c2s1)
{
  SERIAL("LCF6343");

 in1_8: "N11 CFTN1" <- , r13c1s1(SPLIT)/t1_8; 
 th1_8: "1/1"       -> , r11c1s1(SCALER)/in0_7;   
 tb1_8: "CR2 SL1"   -> , .s15(DELAY)/in1_8;

 m:    .c11s3/in1;
 test:   .s23/out6;
 mux_tb: .s22/in1a;   mux_e: .s22/in5a;   mux_mon: .s22/in9a;
}
CF8103(r12c2s2)
{
  SERIAL("LCF6341");

 in1_8: "N12 CFTN2" <- , r13c1s1(SPLIT)/t9_16;
 th1_8: "1/2"       -> , r11c1s1(SCALER)/in8_15;
 tb1_8: "CR2 SL2"   -> , .s15(DELAY)/in9_16;

 m:    .c11s3/in2;
 test:   .s23/out13;
 mux_tb: .s22/in1b;   mux_e: .s22/in5b;   mux_mon: .s22/in9b;
}
CF8103(r12c2s3)
{
  SERIAL("LCF6342");

 in1_8: "N13 CFTN3" <- , r13c1s2(SPLIT)/t1_8; 
 th1_8: "1/3"       -> , r11c1s1(SCALER)/in16_23;
 tb1_8: "CR2 SL3"   -> , .s16(DELAY)/in1_8;

 m:    .c11s3/in3;
 test:   .s23/out4;
 mux_tb: .s22/in1c;   mux_e: .s22/in5c;   mux_mon: .s22/in9c;
}
CF8103(r12c2s4)
{
  SERIAL("LCF6409");

 in1_8: "N14 CFTN4" <- , r13c1s2(SPLIT)/t9_16;
 th1_8: "1/4"       -> , r11c1s1(SCALER)/in24_31;
 tb1_8: "CR2 SL4"   -> , .s16(DELAY)/in9_16;

 m:    .c11s3/in4;
 test:   .s23/out3;
 mux_tb: .s22/in1d;   mux_e: .s22/in5d;   mux_mon: .s22/in9d;
}
CF8103(r12c2s5)
{
  SERIAL("LCF6339");

 in1_8: "N15 CFTN5" <- , r13c1s3(SPLIT)/t1_8; 
 th1_8: "1/5"       -> , r11c1s1(SCALER)/in32_39;
 tb1_8: "CR2 SL5"   -> , .s17(DELAY)/in1_8;

 m:    .c11s3/in5;
 test:   .s23/out5;
 mux_tb: .s22/in2a;   mux_e: .s22/in6a;   mux_mon: .s22/in10a;
}
CF8103(r12c2s6)
{
  SERIAL("LCF6340");

 in1_8: "N25 CFTN5" <- , r13c1s3(SPLIT)/t9_16;
 th1_8: "1/6"       -> , r11c1s1(SCALER)/in40_47;
 tb1_8: "CR2 SL6"   -> , .s17(DELAY)/in9_16;

 m:    .c11s3/in7;
 test:   .s23/out10;
 mux_tb: .s22/in2b;   mux_e: .s22/in6b;   mux_mon: .s22/in10b;
}

//=======================================================
// Continue here Felix (18:26 Mittwoche 18 July)
//=======================================================

CF8103(r12c2s7)
{
  SERIAL("LCF6546");

 in1_8: "N21 CFN1"  <- , r13c1s4(SPLIT)/t1_8; 
 th1_8: "2/1"       -> , r11c1s2(SCALER)/in0_7;
 tb1_8: "CR2 SL7"   -> , .s18(DELAY)/in1_8;

 m:    .c11s3/in8;
 test:   .s23/out11;
 mux_tb: .s22/in2c;   mux_e: .s22/in6c;   mux_mon: .s22/in10c;
}

CF8103(r12c2s8)
{
  SERIAL("LCF6322");

 in1_8: "N22 CFN2"  <- , r13c1s4(SPLIT)/t9_16;
 th1_8: "2/2"       -> , r11c1s2(SCALER)/in8_15;
 tb1_8: "CR2 SL8"   -> , .s18(DELAY)/in9_16;

 m:    .c11s3/in9;
 test:   .s23/out9;
 mux_tb: .s22/in2d;   mux_e: .s22/in6d;   mux_mon: .s22/in10d;
}

CF8103(r12c2s9)
{
  SERIAL("LCF6237");

 in1_8: "N23 CFN3"  <- , r13c1s5(SPLIT)/t1_8; 
 th1_8: "2/3"       -> , r11c1s2(SCALER)/in16_23;
 tb1_8: "CR2 SL9"   -> , .s19(DELAY)/in1_8;

 m:    .c11s3/in10;
 test:   .s23/out2;
 mux_tb: .s22/in3a;   mux_e: .s22/in7a;   mux_mon: .s22/in11a;
}

CF8103(r12c2s10)
{
  SERIAL("LCF6334");

 in1_8: "N24 CFN4"  <- , r13c1s5(SPLIT)/t9_16;
 th1_8: "2/4"       -> , r11c1s2(SCALER)/in24_31;
 tb1_8: "CR2 SL10"  -> , .s19(DELAY)/in9_16;

 m:    .c11s3/in11;
 test:   .s23/out1;
 mux_tb: .s22/in3b;   mux_e: .s22/in7b;   mux_mon: .s22/in11b;
}

CF8103(r12c2s11)
{
  SERIAL("LCF6337");

 in1_8: "N31 CFN1"  <- , r13c1s6(SPLIT)/t1_8; 
 th1_8: "2/5"       -> , r11c1s2(SCALER)/in32_39;
 tb1_8: "CR2 SL11"  -> , .s20(DELAY)/in1_8;

 m:    .c11s5/in1;
 test:   .s23/out8;
 mux_tb: .s22/in3c;   mux_e: .s22/in7c;   mux_mon: .s22/in11c;
}

CF8103(r12c2s12)
{
  SERIAL("LCF6442");//TODO: really?

 in1_8: "N32 CFN2"  <- , r13c1s6(SPLIT)/t9_16;
 th1_8: "2/6"       -> , r11c1s2(SCALER)/in40_47;
 tb1_8: "CR2 SL12"  -> , .s20(DELAY)/in9_16;

 m:    .c11s5/in2;
 test:   .s23/out14;
 mux_tb: .s22/in3d;   mux_e: .s22/in7d;   mux_mon: .s22/in11d;
}

CF8103(r12c2s13)
{
  SERIAL("LCF6205");

 in1_8: "N33 CFN3"  <- , r13c1s7(SPLIT)/t1_8; 
 th1_8: "3/1"       -> , r11c1s3(SCALER)/in0_7;  
 tb1_8: "CR2 SL13"  -> , .s21(DELAY)/in1_8;

 m:    .c11s5/in3;
 test:   .s23/out12;
 mux_tb: .s22/in4a;   mux_e: .s22/in8a;   mux_mon: .s22/in12a;
}

CF8103(r12c2s14)
{
  SERIAL("LCF6324");

 in1_8: "N34 CFN4"  <- , r13c1s7(SPLIT)/t9_16;
 th1_8: "3/2"       -> , r11c1s3(SCALER)/in8_15; 
 tb1_8: "CR2 SL14"  -> , .s21(DELAY)/in9_16;

 m:    .c11s5/in4;
 test:   .s23/out7;
 mux_tb: .s22/in4b;   mux_e: .s22/in8b;   mux_mon: .s22/in12b;
}

DL1610(r12c2s15)
{
 in1_8:  <- "CR2 SL1"  , .s1/tb1_8;
 in9_16: <- "CR2 SL2"  , .s2/tb1_8;

 outa1_16:               r11c2s7(TDC)/in0_15;
}

DL1610(r12c2s16)
{
 in1_8:  <- "CR2 SL3"  , .s3/tb1_8;
 in9_16: <- "CR2 SL4"  , .s4/tb1_8;

 outa1_16:               r11c2s6(TDC)/in0_15;
}

DL1610(r12c2s17)
{
 in1_8:  <- "CR2 SL5"  , .s5/tb1_8;
 in9_16: <- "CR2 SL6"  , .s6/tb1_8;

 outa1_16: <-          , r11c2s5(TDC)/in0_15;
}

DL1610(r12c2s18)
{
 in1_8:  <- "CR2 SL7"  , .s7/tb1_8;
 in9_16: <- "CR2 SL8"  , .s8/tb1_8;

 outa1_16:               r11c2s4(TDC)/in0_15;
}

DL1610(r12c2s19)
{
 in1_8:  <- "CR2 SL9"  , .s9/tb1_8;
 in9_16: <- "CR2 SL10" , .s10/tb1_8;

 outa1_16:               r11c2s3(TDC)/in0_15;
}

DL1610(r12c2s20)
{
 in1_8:  <- "CR2 SL11" , .s11/tb1_8;
 in9_16: <- "CR2 SL12" , .s12/tb1_8;

// outa1_16: <-          , r11c2s2(TDC)/in0_15;
 outa1_16: <-          , r11c2s11(TDC)/in0_15;
}

DL1610(r12c2s21)
{
 in1_8:  <- "CR2 SL13" , .s13/tb1_8;
 in9_16: <- "CR2 SL14" , .s14/tb1_8;

 outa1_16: <-          , r11c2s1(TDC)/in0_15;
}

RMX3(r12c2s22)
{
  SERIAL("L_MPX1");

 in1a:  .s1/mux_tb;    in5a:  .s1/mux_e;    in9a:  .s1/mux_mon; 
 in1b:  .s2/mux_tb;    in5b:  .s2/mux_e;    in9b:  .s2/mux_mon; 
 in1c:  .s3/mux_tb;    in5c:  .s3/mux_e;    in9c:  .s3/mux_mon; 
 in1d:  .s4/mux_tb;    in5d:  .s4/mux_e;    in9d:  .s4/mux_mon; 
 in2a:  .s5/mux_tb;    in6a:  .s5/mux_e;    in10a: .s5/mux_mon; 
 in2b:  .s6/mux_tb;    in6b:  .s6/mux_e;    in10b: .s6/mux_mon; 
 in2c:  .s7/mux_tb;    in6c:  .s7/mux_e;    in10c: .s7/mux_mon; 
 in2d:  .s8/mux_tb;    in6d:  .s8/mux_e;    in10d: .s8/mux_mon; 
 in3a:  .s9/mux_tb;    in7a:  .s9/mux_e;    in11a: .s9/mux_mon; 
 in3b:  .s10/mux_tb;   in7b:  .s10/mux_e;   in11b: .s10/mux_mon;
 in3c:  .s11/mux_tb;   in7c:  .s11/mux_e;   in11c: .s11/mux_mon;
 in3d:  .s12/mux_tb;   in7d:  .s12/mux_e;   in11d: .s12/mux_mon;
 in4a:  .s13/mux_tb;   in8a:  .s13/mux_e;   in12a: .s13/mux_mon;
 in4b:  .s14/mux_tb;   in8b:  .s14/mux_e;   in12b: .s14/mux_mon;

 out1_12: r14c7s1/in1a_3d;
}

LIFO(r12c2s23)
{
 in: 		r13c4s1u1/out4; // TCAL

 out1:  .s10/test;
 out2:  .s9/test;
 out3:  .s4/test;
 out4:  .s3/test;
 out5:  .s5/test;
 out6:  .s1/test;
 out7:  .s14/test;
 out8:  .s11/test;
 out9:  .s8/test;
 out10: .s6/test;
 out11: .s7/test;
 out12: .s13/test;
 out13: .s2/test;
 out14: .s12/test;
}

CBV1000(r12c2s24)
{
  SETTING("CRATE_NO" => "2");

 vsb_in:  r12c3s24/vsb_out;
 vsb_out: r14c6s24/vsb_in;
}

DMS1000(r12c2s25)
{
  ;
}


NIM_CRATE(r23c2)
{
  ;
}

HV_CTRL_XB_NIM(r23c2s23)
{
 ctrl_in1_8: r22c2s7/ctrl_out32_39;
 ctrl_out1: r20c073s2/hv_ctrl; power_out1: r20c073s2/power;
 ctrl_out2: r20c074s2/hv_ctrl; power_out2: r20c074s2/power;
 ctrl_out3: r20c075s2/hv_ctrl; power_out3: r20c075s2/power;
 ctrl_out4: r20c076s2/hv_ctrl; power_out4: r20c076s2/power;
 //ctrl_out5: r20c077s2/hv_ctrl; power_out5: r20c077s2/power;	//crystal #077 being a spare crystal to be tested in a proton beam
 ctrl_out6: r20c078s2/hv_ctrl; power_out6: r20c078s2/power;
 ctrl_out7: r20c079s2/hv_ctrl; power_out7: r20c079s2/power;
 ctrl_out8: r20c080s2/hv_ctrl; power_out8: r20c080s2/power;
}

HV_CTRL_XB_NIM(r23c2s19)
{
 ctrl_in1_8: r22c2s9/ctrl_out0_7;
 ctrl_out1: r20c161s2/hv_ctrl; power_out1: r20c161s2/power; // 81 <-> 161
 ctrl_out2: r20c162s2/hv_ctrl; power_out2: r20c162s2/power; // 82 <-> 162
 ctrl_out3: r20c083s2/hv_ctrl; power_out3: r20c083s2/power;
 ctrl_out4: r20c084s2/hv_ctrl; power_out4: r20c084s2/power;
 ctrl_out5: r20c085s2/hv_ctrl; power_out5: r20c085s2/power;
 ctrl_out6: r20c086s2/hv_ctrl; power_out6: r20c086s2/power;
 ctrl_out7: r20c087s2/hv_ctrl; power_out7: r20c087s2/power;
 ctrl_out8: r20c088s2/hv_ctrl; power_out8: r20c088s2/power;
}

HV_CTRL_XB_NIM(r23c2s15)
{
 ctrl_in1_8: r22c2s9/ctrl_out8_15;
 ctrl_out1: r20c089s2/hv_ctrl; power_out1: r20c089s2/power;
 ctrl_out2: r20c090s2/hv_ctrl; power_out2: r20c090s2/power;
 ctrl_out3: r20c091s2/hv_ctrl; power_out3: r20c091s2/power;
 ctrl_out4: r20c092s2/hv_ctrl; power_out4: r20c092s2/power;
 ctrl_out5: r20c093s2/hv_ctrl; power_out5: r20c093s2/power;
 ctrl_out6: r20c094s2/hv_ctrl; power_out6: r20c094s2/power;
 ctrl_out7: r20c095s2/hv_ctrl; power_out7: r20c095s2/power;
 ctrl_out8: r20c096s2/hv_ctrl; power_out8: r20c096s2/power;
}

HV_CTRL_XB_NIM(r23c2s11)
{
 ctrl_in1_8: r22c2s9/ctrl_out16_23;
 ctrl_out1: r20c097s2/hv_ctrl; power_out1: r20c097s2/power;
 ctrl_out2: r20c098s2/hv_ctrl; power_out2: r20c098s2/power;
 ctrl_out3: r20c099s2/hv_ctrl; power_out3: r20c099s2/power;
 ctrl_out4: r20c100s2/hv_ctrl; power_out4: r20c100s2/power;	//crystals 100-103 re-installed
 ctrl_out5: r20c101s2/hv_ctrl; power_out5: r20c101s2/power;
 ctrl_out6: r20c102s2/hv_ctrl; power_out6: r20c102s2/power;
 ctrl_out7: r20c103s2/hv_ctrl; power_out7: r20c103s2/power;
 ctrl_out8: r20c104s2/hv_ctrl; power_out8: r20c104s2/power;
}

HV_CTRL_XB_NIM(r23c2s7)
{
 ctrl_in1_8: r22c2s9/ctrl_out24_31;
 ctrl_out1: r20c105s2/hv_ctrl; power_out1: r20c105s2/power;
 ctrl_out2: r20c106s2/hv_ctrl; power_out2: r20c106s2/power;
 ctrl_out3: r20c107s2/hv_ctrl; power_out3: r20c107s2/power;
 ctrl_out4: r20c108s2/hv_ctrl; power_out4: r20c108s2/power;
 ctrl_out5: r20c109s2/hv_ctrl; power_out5: r20c109s2/power;
 ctrl_out6: r20c110s2/hv_ctrl; power_out6: r20c110s2/power;
 ctrl_out7: r20c111s2/hv_ctrl; power_out7: r20c111s2/power;
 ctrl_out8: r20c112s2/hv_ctrl; power_out8: r20c112s2/power;
}



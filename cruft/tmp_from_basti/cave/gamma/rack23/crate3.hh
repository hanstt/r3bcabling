//--------------- VME CRATE -------------//

VME_CRATE(r23c3)
{
  SETTING("CRATE_NO" => "4");
}

RIO4(r23c3s1) 
{
    SETTING("HOSTNAME"=>"r4-12");
}
      
TRIVA5(r23c3s3) 
{
  ;
}

MADC32(r23c3s05)
{
  SERIAL("CBL01");

  SETTING("ADDRESS" => "0x00730000");
  SETTING("VIRTUAL_SLOT" => "3");

  in1_16:  r23c0s17/e1_16; // module 9
  in17_32: r23c0s15/e1_16; // module 10
  nim_gate0: .s19/nim_out1;
  nim_busy: .c0s17/rc;
}

MADC32(r23c3s07)
{
  SERIAL("CBL02");

  SETTING("ADDRESS" => "0x00720000");
  SETTING("VIRTUAL_SLOT" => "2");

  in1_16:  r23c0s11/e1_16; // module 11
  in17_32: r23c0s09/e1_16; // module 12
  nim_gate0: .s19/nim_out2;
  nim_busy: ;
}

MADC32(r23c3s09)
{
  SERIAL("CBL03");

  SETTING("ADDRESS" => "0x00710000");
  SETTING("VIRTUAL_SLOT" => "1");

  in1_16:  r23c0s07/e1_16; // module 13
  in17_32: r23c0s05/e1_16; // module 14
  nim_gate0: .s19/nim_out3;
  nim_busy: ;
}

MADC32(r23c3s11)
{
  SERIAL("CBL04");

  SETTING("ADDRESS" => "0x00700000");
  SETTING("VIRTUAL_SLOT" => "0");

  in1_16:  r23c0s03/e1_16; // module 15
  in17_32: r23c0s01/e1_16; // module 16
  nim_gate0: .s19/nim_out4;
  nim_busy: .c0s01/rc;
}

// FIXME This slot does not exist
MADC32(r23c3s20)
{
  SERIAL("CBL06");

  SETTING("ADDRESS" => "0x00760000");
  SETTING("VIRTUAL_SLOT" => "4");

  in1_16:  r25c0s07/e1_16; // module 17
  // ToDo: Missing gate input
}

HDCAN1(r23c3s13)
{
  in1_16:  r23c0s15/t1_16; // module 10
  in17_32: r23c0s17/t1_16; // module 9

  out1_32: r23c3s17/in1_32;
}

HDCAN1(r23c3s14)
{
  in1_16:  r23c0s09/t1_16; // module 12
  in17_32: r23c0s11/t1_16; // module 11

  out1_32: r23c3s17/in33_64;
}

HDCAN1(r23c3s15)
{
  in1_16:  r23c0s05/t1_16; // module 14
  in17_32: r23c0s07/t1_16; // module 13

  out1_32: r23c3s17/in65_96;
}

HDCAN1(r23c3s16)
{
  in1_16:  r23c0s01/t1_16; // module 16
  in17_32: r23c0s03/t1_16; // module 15

  out1_32: r23c3s17/in97_128;
}

VUPROM2(r23c3s17)
{
  SERIAL("CBL05");

  SETTING("ADDRESS" => "0x05000000");
  SETTING("VIRTUAL_SLOT" => "4");

  in1_32:	  .s13/out1_32;
  in33_64:	  .s14/out1_32;
  in65_96:	  .s15/out1_32;
  in97_128:	  .s16/out1_32;

  in_nim: .s19/nim_out5;
  out:	  ;
}

ENV1(r23c3s19)
{
  nim_in1:    .s21/out8;

  in_ecl2_9:  .s19/out_ecl1_8; // Not exactly true as in9 is not connected
  out_ecl1_8: .s19/in_ecl2_9; 
  
  nim_out1:   .s5/nim_gate0;
  nim_out2:   .s7/nim_gate0;
  nim_out3:   .s9/nim_gate0;
  nim_out4:   .s11/nim_gate0;
  nim_out5:   .s17/in_nim;
}


TRIDI(r23c3s21)
{
  in1: r23c0s19u1/out1;

  out8: .s19/nim_in1;
}




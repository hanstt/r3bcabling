NIM_CRATE(r22c0)		// Delays for the left half CB proton signals
{
  ;
}

MSCF16(r22c0s1)             //module 1, right
{
  in1:	  r20c027s2/out2;
  in2:	  r20c061s2/out2;
  in3:	  r20c064s2/out2;
  in4:	  r20c028s2/out2;
  in5:	  r20c062s2/out2;
  in6:	  r20c026s2/out2;
  in7:	  r20c044s2/out2;
  in8:	  r20c043s2/out2;
	
  in9:	  r20c012s2/out2;	
  in10:	  r20c014s2/out2;
  in11:	  r20c045s2/out2;
  in12:	  r20c042s2/out2;
  in13:	  r20c004s2/out2;
  in14:	  r20c063s2/out2;
  in15:	  r20c065s2/out2;
  in16:	  r20c013s2/out2;

  t1_16:  r24c0s18/in1_16;
  e1_16:  r24c0s11/in1_16;
	
  or: r22c0s19u2/in1;
  rc: r24c0s11/nim_busy;
}


MSCF16(r22c0s3)             //module 2, right
{
  in1:	  r20c024s2/out2;
  in2:	  r20c080s2/out2;
  in3:	  r20c078s2/out2;
  in4:	  r20c060s2/out2;
  in5:	  r20c010s2/out2;
  in6:	  r20c057s2/out2;
  in7:	  r20c041s2/out2;
  in8:	  r20c040s2/out2;
	  
  in9:	  r20c079s2/out2;	
  in10:	  r20c059s2/out2;
  in11:	  r20c025s2/out2;
  in12:	  r20c039s2/out2;
  in13:	  r20c003s2/out2;
  in14:	  r20c023s2/out2;
  in15:	  r20c058s2/out2;
  in16:	  r20c011s2/out2;

  t1_16:  r24c0s18/in17_32;
  e1_16:  r24c0s11/in17_32;
	
  or: r22c0s19u2/in2;
  // rc: chained with r22c0s1
}

MSCF16(r22c0s07)             //module 3, right
{
  in1:	  r20c061s2/out1;
  in2:	  r20c060s2/out1;
  in3:	  r20c062s2/out1;

  in5:	  r20c080s2/out1;
  in6:	  r20c041s2/out1;
  in7:	  r20c043s2/out1;
  in8:	  r20c042s2/out1;
  in9:	  r20c059s2/out1;
  in10:	  r20c063s2/out1; 
  in13:	  r20c025s2/out1;
  in14:	  r20c026s2/out1;
  in15:	  r20c058s2/out1;
  in16:	  r20c064s2/out1;

  t1_16:  r24c0s17/in1_16;
  e1_16:  r24c0s09/in1_16;
	
  or: r22c0s19u1/in1;
  rc: r24c0s12/nim_busy;
}

MSCF16(r22c0s09)             //module 4, right
{
  in1:	  r20c024s2/out1;
  in3:	  r20c027s2/out1;


  in5:	  r20c040s2/out1;
  in6:	  r20c044s2/out1;

 
  in9:	  r20c079s2/out1;	
  in10:	  r20c012s2/out1;
  in11:	  r20c011s2/out1;
  in12:	  r20c013s2/out1;
  in13:	  r20c023s2/out1;
  in14:	  r20c028s2/out1;
  in15:	  r20c057s2/out1;
  in16:	  r20c065s2/out1;

  t1_16:  r24c0s17/in17_32;
  e1_16:  r24c0s09/in17_32;
	
  or: r22c0s19u1/in2;
  // rc: chained with r22c0s7
}

MSCF16(r22c0s11)             //module 5, right
{
  in1:	  r20c039s2/out1;
  in2:	  r20c045s2/out1;
  in3:	  r20c078s2/out1;
  
  in5:	  r20c003s2/out1;
  in6:	  r20c004s2/out1;
  in7:	  r20c010s2/out1;
  in8:	  r20c014s2/out1;
  in9:	  r20c022s2/out1;	
  in10:	  r20c029s2/out1;
  in11:	  r20c056s2/out1;
  in12:	  r20c066s2/out1;
  in13:	  r20c038s2/out1;
  in14:	  r20c046s2/out1;
  in15:	  r20c009s2/out1;
  in16:	  r20c015s2/out1;

  t1_16:  r24c0s16/in1_16;
  e1_16:  r24c0s07/in1_16;
	
  or:r22c0s19u1/in3;
  // rc: chained with r22c0s7
}



MSCF16(r22c0s13)             //module 6, right
{
  in1:	  r20c002s2/out1;
  in2:	  r20c005s2/out1;
  in3:	  r20c001s2/out1;

  in5:	  r20c055s2/out1;
  in6:	  r20c067s2/out1;
  in7:	  r20c021s2/out1;
  in8:	  r20c030s2/out1; 
  in9:	  r20c008s2/out1;	
  in10:	  r20c016s2/out1;
  in11:	  r20c006s2/out1;
  in12:	  r20c007s2/out1;
  in13:	  r20c076s2/out1;
  in14:	  r20c037s2/out1;
  in15:	  r20c047s2/out1;

  t1_16:  r24c0s16/in17_32;
  e1_16:  r24c0s07/in17_32;\
	
  or: r22c0s19u1/in4;
  // rc: chained with r22c0s7
}



MSCF16(r22c0s15)             //module 7, right
{
  in1:	  r20c054s2/out1;
  in2:	  r20c068s2/out1;
  in3:	  r20c020s2/out1;
  in4:	  r20c031s2/out1;
  in5:	  r20c017s2/out1;
  in6:	  r20c019s2/out1;
  in7:	  r20c018s2/out1;
  in8:	  r20c075s2/out1;
  in9:	  r20c036s2/out1;	
  in10:	  r20c048s2/out1;


  in13:	  r20c032s2/out1;
  in14:	  r20c035s2/out1;
  in15:	  r20c053s2/out1;
  in16:	  r20c069s2/out1;

  t1_16:  r24c0s15/in1_16;
  e1_16:  r24c0s05/in1_16;
	
  or: r22c0s19u1/in5;
  // rc: chained with r22c0s7
}

MSCF16(r22c0s17)             //module 8, right
{
  in1:	  r20c033s2/out1;
  in2:	  r20c034s2/out1;

  in5:	  r20c052s2/out1;
  in6:	  r20c070s2/out1;

 
  in9:	  r20c050s2/out1;	
  in10:	  r20c049s2/out1;
  in11:	  r20c051s2/out1;
  in12:	  r20c074s2/out1;
  in13:	  r20c071s2/out1;
  in14:	  r20c073s2/out1;
  in15:	  r20c072s2/out1;

  t1_16:  r24c0s15/in17_32;
  e1_16:  r24c0s05/in17_32;
	
  or: r22c0s19u1/in6;
  // rc: chained with r22c0s7
}


LF4000(r22c0s19)
{
  ;
}

LF4000_8_U(r22c0s19u1)
{
  in1: r22c0s07/or;
  in2: r22c0s09/or;
  in3: r22c0s11/or;
  in4: r22c0s13/or;
  in5: r22c0s15/or;
  in6: r22c0s17/or;

  out1: r24c0s21/in1;
}

LF4000_8_U(r22c0s19u2)
{
  in1: r22c0s01/or;
  in2: r22c0s03/or;

  out1: ; // not connected for the moment
}



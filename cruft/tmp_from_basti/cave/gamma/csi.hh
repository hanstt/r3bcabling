MODULE(DETECTOR_CSI)
{
  HAS_SETTING("PHI" => "%f");
  HAS_SETTING("THETA" => "%f");

  HAS_SETTING("DIM" => "%f");  // Unit??

  HAS_SETTING("CENTER" => "");

  CONNECTOR(signal,SIGNAL); // Lemo out?
  CONNECTOR(hv,TENSION); // Lemo in?
}

/* Naming scheme for the CsI gamma detector:
 *
 * DETECTOR_CSI ('the pm tube', r27cXXXs1), XXX is the crystal number,
 *
 * JOINER_8(r28c1-10) are the (signal) cable joiners sitting on the left  frame.
 * JOINER_8(r29c1-10) are the same for the right frame.
 *
 * HV_CONN_16(r28c11-15) are the HV connectors sitting on the left  frame.
 * HV_CONN_16(r29c11-15) are the same for the right frame.
 *
 * r27 is thus the detector itself.
 * r28/r29 is the frames of the detector.
 */

DETECTOR_CSI(r27c001s1) { LABEL("CS001_1"); signal: /*L*/r28c1/in1;   hv: /*L*/r28c11/out1;   SETTING("THETA"=>"13.7 "); SETTING("PHI"=>" 15.0"); SETTING("DIM"=>"0.97");}
DETECTOR_CSI(r27c002s1) { LABEL("CS002_1"); signal: /*L*/r28c1/in2;   hv: /*L*/r28c11/out2;   SETTING("THETA"=>"13.7 "); SETTING("PHI"=>" 45.0"); SETTING("DIM"=>"0.97");}
DETECTOR_CSI(r27c003s1) { LABEL("CS003_1"); signal: /*L*/r28c1/in3;   hv: /*L*/r28c11/out3;   SETTING("THETA"=>"13.7 "); SETTING("PHI"=>" 75.0"); SETTING("DIM"=>"0.97");}
DETECTOR_CSI(r27c004s1) { LABEL("CS004_1"); signal: /*R*/r29c1/in1;   hv: /*R*/r29c11/out1;   SETTING("THETA"=>"13.7 "); SETTING("PHI"=>"105.0"); SETTING("DIM"=>"0.97");}
DETECTOR_CSI(r27c005s1) { LABEL("CS005_1"); signal: /*R*/r29c1/in2;   hv: /*R*/r29c11/out2;   SETTING("THETA"=>"13.7 "); SETTING("PHI"=>"135.0"); SETTING("DIM"=>"0.97");}
DETECTOR_CSI(r27c006s1) { LABEL("CS006_1"); signal: /*R*/r29c1/in3;   hv: /*R*/r29c11/out3;   SETTING("THETA"=>"13.7 "); SETTING("PHI"=>"165.0"); SETTING("DIM"=>"0.97");}
DETECTOR_CSI(r27c007s1) { LABEL("CS007_1"); signal: /*R*/r29c1/in4;   hv: /*R*/r29c11/out4;   SETTING("THETA"=>"13.7 "); SETTING("PHI"=>"195.0"); SETTING("DIM"=>"0.97");}
DETECTOR_CSI(r27c008s1) { LABEL("CS008_1"); signal: /*R*/r29c1/in5;   hv: /*R*/r29c11/out5;   SETTING("THETA"=>"13.7 "); SETTING("PHI"=>"225.0"); SETTING("DIM"=>"0.97");}
DETECTOR_CSI(r27c009s1) { LABEL("CS009_1"); signal: /*R*/r29c1/in6;   hv: /*R*/r29c11/out6;   SETTING("THETA"=>"13.7 "); SETTING("PHI"=>"255.0"); SETTING("DIM"=>"0.97");}
DETECTOR_CSI(r27c010s1) { LABEL("CS010_1"); signal: /*L*/r28c1/in4;   hv: /*L*/r28c11/out4;   SETTING("THETA"=>"13.7 "); SETTING("PHI"=>"285.0"); SETTING("DIM"=>"0.97");}
DETECTOR_CSI(r27c011s1) { LABEL("CS011_1"); signal: /*L*/r28c1/in5;   hv: /*L*/r28c11/out5;   SETTING("THETA"=>"13.7 "); SETTING("PHI"=>"315.0"); SETTING("DIM"=>"0.97");}
DETECTOR_CSI(r27c012s1) { LABEL("CS012_1"); signal: /*L*/r28c1/in6;   hv: /*L*/r28c11/out6;   SETTING("THETA"=>"13.7 "); SETTING("PHI"=>"345.0"); SETTING("DIM"=>"0.97");}
DETECTOR_CSI(r27c013s1) { LABEL("CS013_1"); signal: /*L*/r28c1/in7;   hv: /*L*/r28c11/out7;   SETTING("THETA"=>"21.85"); SETTING("PHI"=>" 15.0"); SETTING("DIM"=>"0.93");}
DETECTOR_CSI(r27c014s1) { LABEL("CS014_1"); signal: /*L*/r28c1/in8;   hv: /*L*/r28c11/out8;   SETTING("THETA"=>"21.85"); SETTING("PHI"=>" 45.0"); SETTING("DIM"=>"0.93");}
DETECTOR_CSI(r27c015s1) { LABEL("CS015_1"); signal: /*L*/r28c2/in1;   hv: /*L*/r28c11/out9;   SETTING("THETA"=>"21.85"); SETTING("PHI"=>" 75.0"); SETTING("DIM"=>"0.93");}
DETECTOR_CSI(r27c016s1) { LABEL("CS016_1"); signal: /*R*/r29c1/in7;   hv: /*R*/r29c11/out7;   SETTING("THETA"=>"21.85"); SETTING("PHI"=>"105.0"); SETTING("DIM"=>"0.93");}
DETECTOR_CSI(r27c017s1) { LABEL("CS017_1"); signal: /*R*/r29c1/in8;   hv: /*R*/r29c11/out8;   SETTING("THETA"=>"21.85"); SETTING("PHI"=>"135.0"); SETTING("DIM"=>"0.93");}
DETECTOR_CSI(r27c018s1) { LABEL("CS018_1"); signal: /*R*/r29c2/in1;   hv: /*R*/r29c11/out9;   SETTING("THETA"=>"21.85"); SETTING("PHI"=>"165.0"); SETTING("DIM"=>"0.93");}
DETECTOR_CSI(r27c019s1) { LABEL("CS019_1"); signal: /*R*/r29c2/in2;   hv: /*R*/r29c11/out10;  SETTING("THETA"=>"21.85"); SETTING("PHI"=>"195.0"); SETTING("DIM"=>"0.93");}
DETECTOR_CSI(r27c020s1) { LABEL("CS020_1"); signal: /*R*/r29c2/in3;   hv: /*R*/r29c11/out11;  SETTING("THETA"=>"21.85"); SETTING("PHI"=>"225.0"); SETTING("DIM"=>"0.93");}
DETECTOR_CSI(r27c021s1) { LABEL("CS021_1"); signal: /*R*/r29c2/in4;   hv: /*R*/r29c11/out12;  SETTING("THETA"=>"21.85"); SETTING("PHI"=>"255.0"); SETTING("DIM"=>"0.93");}
DETECTOR_CSI(r27c022s1) { LABEL("CS022_1"); signal: /*L*/r28c2/in2;   hv: /*L*/r28c11/out10;  SETTING("THETA"=>"21.85"); SETTING("PHI"=>"285.0"); SETTING("DIM"=>"0.93");}
DETECTOR_CSI(r27c023s1) { LABEL("CS023_1"); signal: /*L*/r28c2/in3;   hv: /*L*/r28c11/out11;  SETTING("THETA"=>"21.85"); SETTING("PHI"=>"315.0"); SETTING("DIM"=>"0.93");}
DETECTOR_CSI(r27c024s1) { LABEL("CS024_1"); signal: /*L*/r28c2/in4;   hv: /*L*/r28c11/out12;  SETTING("THETA"=>"21.85"); SETTING("PHI"=>"345.0"); SETTING("DIM"=>"0.93");}
DETECTOR_CSI(r27c025s1) { LABEL("CS025_1"); signal: /*L*/r28c2/in5;   hv: /*L*/r28c11/out13;  SETTING("THETA"=>"28.1 "); SETTING("PHI"=>" 15.0"); SETTING("DIM"=>"0.88");}
DETECTOR_CSI(r27c026s1) { LABEL("CS026_1"); signal: /*L*/r28c2/in6;   hv: /*L*/r28c11/out14;  SETTING("THETA"=>"28.1 "); SETTING("PHI"=>" 45.0"); SETTING("DIM"=>"0.88");}
DETECTOR_CSI(r27c027s1) { LABEL("CS027_1"); signal: /*L*/r28c2/in7;   hv: /*L*/r28c11/out15;  SETTING("THETA"=>"28.1 "); SETTING("PHI"=>" 75.0"); SETTING("DIM"=>"0.88");}
DETECTOR_CSI(r27c028s1) { LABEL("CS028_1"); signal: /*R*/r29c2/in5;   hv: /*R*/r29c11/out13;  SETTING("THETA"=>"28.1 "); SETTING("PHI"=>"105.0"); SETTING("DIM"=>"0.88");}
DETECTOR_CSI(r27c029s1) { LABEL("CS029_1"); signal: /*R*/r29c2/in6;   hv: /*R*/r29c11/out14;  SETTING("THETA"=>"28.1 "); SETTING("PHI"=>"135.0"); SETTING("DIM"=>"0.88");}
DETECTOR_CSI(r27c030s1) { LABEL("CS030_1"); signal: /*R*/r29c2/in7;   hv: /*R*/r29c11/out15;  SETTING("THETA"=>"28.1 "); SETTING("PHI"=>"165.0"); SETTING("DIM"=>"0.88");}
DETECTOR_CSI(r27c031s1) { LABEL("CS031_1"); signal: /*R*/r29c2/in8;   hv: /*R*/r29c11/out16;  SETTING("THETA"=>"28.1 "); SETTING("PHI"=>"195.0"); SETTING("DIM"=>"0.88");}
DETECTOR_CSI(r27c032s1) { LABEL("CS032_1"); signal: /*R*/r29c3/in1;   hv: /*R*/r29c12/out1;   SETTING("THETA"=>"28.1 "); SETTING("PHI"=>"225.0"); SETTING("DIM"=>"0.88");}
DETECTOR_CSI(r27c033s1) { LABEL("CS033_1"); signal: /*R*/r29c3/in2;   hv: /*R*/r29c12/out2;   SETTING("THETA"=>"28.1 "); SETTING("PHI"=>"255.0"); SETTING("DIM"=>"0.88");}
DETECTOR_CSI(r27c034s1) { LABEL("CS034_1"); signal: /*L*/r28c2/in8;   hv: /*L*/r28c11/out16;  SETTING("THETA"=>"28.1 "); SETTING("PHI"=>"285.0"); SETTING("DIM"=>"0.88");}
DETECTOR_CSI(r27c035s1) { LABEL("CS035_1"); signal: /*L*/r28c3/in1;   hv: /*L*/r28c12/out1;   SETTING("THETA"=>"28.1 "); SETTING("PHI"=>"315.0"); SETTING("DIM"=>"0.88");}
DETECTOR_CSI(r27c036s1) { LABEL("CS036_1"); signal: /*L*/r28c3/in2;   hv: /*L*/r28c12/out2;   SETTING("THETA"=>"28.1 "); SETTING("PHI"=>"345.0"); SETTING("DIM"=>"0.88");}
DETECTOR_CSI(r27c037s1) { LABEL("CS037_1"); signal: /*L*/r28c3/in3;   hv: /*L*/r28c12/out3;   SETTING("THETA"=>"33.8 "); SETTING("PHI"=>" 15.0"); SETTING("DIM"=>"0.83");}
DETECTOR_CSI(r27c038s1) { LABEL("CS038_1"); signal: /*L*/r28c3/in4;   hv: /*L*/r28c12/out4;   SETTING("THETA"=>"33.8 "); SETTING("PHI"=>" 45.0"); SETTING("DIM"=>"0.83");}
DETECTOR_CSI(r27c039s1) { LABEL("CS039_1"); signal: /*L*/r28c3/in5;   hv: /*L*/r28c12/out5;   SETTING("THETA"=>"33.8 "); SETTING("PHI"=>" 75.0"); SETTING("DIM"=>"0.83");}
DETECTOR_CSI(r27c040s1) { LABEL("CS040_1"); signal: /*R*/r29c3/in3;   hv: /*R*/r29c12/out3;   SETTING("THETA"=>"33.8 "); SETTING("PHI"=>"105.0"); SETTING("DIM"=>"0.83");}
DETECTOR_CSI(r27c041s1) { LABEL("CS041_1"); signal: /*R*/r29c3/in4;   hv: /*R*/r29c12/out4;   SETTING("THETA"=>"33.8 "); SETTING("PHI"=>"135.0"); SETTING("DIM"=>"0.83");}
DETECTOR_CSI(r27c042s1) { LABEL("CS042_1"); signal: /*R*/r29c3/in5;   hv: /*R*/r29c12/out5;   SETTING("THETA"=>"33.8 "); SETTING("PHI"=>"165.0"); SETTING("DIM"=>"0.83");}
DETECTOR_CSI(r27c043s1) { LABEL("CS043_1"); signal: /*R*/r29c3/in6;   hv: /*R*/r29c12/out6;   SETTING("THETA"=>"33.8 "); SETTING("PHI"=>"195.0"); SETTING("DIM"=>"0.83");}
DETECTOR_CSI(r27c044s1) { LABEL("CS044_1"); signal: /*R*/r29c3/in7;   hv: /*R*/r29c12/out7;   SETTING("THETA"=>"33.8 "); SETTING("PHI"=>"225.0"); SETTING("DIM"=>"0.83");}
DETECTOR_CSI(r27c045s1) { LABEL("CS045_1"); signal: /*R*/r29c3/in8;   hv: /*R*/r29c12/out8;   SETTING("THETA"=>"33.8 "); SETTING("PHI"=>"255.0"); SETTING("DIM"=>"0.83");}
DETECTOR_CSI(r27c046s1) { LABEL("CS046_1"); signal: /*L*/r28c3/in6;   hv: /*L*/r28c12/out6;   SETTING("THETA"=>"33.8 "); SETTING("PHI"=>"285.0"); SETTING("DIM"=>"0.83");}
DETECTOR_CSI(r27c047s1) { LABEL("CS047_1"); signal: /*L*/r28c3/in7;   hv: /*L*/r28c12/out7;   SETTING("THETA"=>"33.8 "); SETTING("PHI"=>"315.0"); SETTING("DIM"=>"0.83");}
DETECTOR_CSI(r27c048s1) { LABEL("CS048_1"); signal: /*L*/r28c3/in8;   hv: /*L*/r28c12/out8;   SETTING("THETA"=>"33.8 "); SETTING("PHI"=>"345.0"); SETTING("DIM"=>"0.83");}
DETECTOR_CSI(r27c049s1) { LABEL("CS049_1"); signal: /*L*/r28c4/in1;   hv: /*L*/r28c12/out9;   SETTING("THETA"=>"39.3 "); SETTING("PHI"=>" 15.0"); SETTING("DIM"=>"0.77");}
DETECTOR_CSI(r27c050s1) { LABEL("CS050_1"); signal: /*L*/r28c4/in2;   hv: /*L*/r28c12/out10;  SETTING("THETA"=>"39.3 "); SETTING("PHI"=>" 45.0"); SETTING("DIM"=>"0.77");}
DETECTOR_CSI(r27c051s1) { LABEL("CS051_1"); signal: /*L*/r28c4/in3;   hv: /*L*/r28c12/out11;  SETTING("THETA"=>"39.3 "); SETTING("PHI"=>" 75.0"); SETTING("DIM"=>"0.77");}
DETECTOR_CSI(r27c052s1) { LABEL("CS052_1"); signal: /*R*/r29c4/in1;   hv: /*R*/r29c12/out9;   SETTING("THETA"=>"39.3 "); SETTING("PHI"=>"105.0"); SETTING("DIM"=>"0.77");}
DETECTOR_CSI(r27c053s1) { LABEL("CS053_1"); signal: /*R*/r29c4/in2;   hv: /*R*/r29c12/out10;  SETTING("THETA"=>"39.3 "); SETTING("PHI"=>"135.0"); SETTING("DIM"=>"0.77");}
DETECTOR_CSI(r27c054s1) { LABEL("CS054_1"); signal: /*R*/r29c4/in3;   hv: /*R*/r29c12/out11;  SETTING("THETA"=>"39.3 "); SETTING("PHI"=>"165.0"); SETTING("DIM"=>"0.77");}
DETECTOR_CSI(r27c055s1) { LABEL("CS055_1"); signal: /*R*/r29c4/in4;   hv: /*R*/r29c12/out12;  SETTING("THETA"=>"39.3 "); SETTING("PHI"=>"195.0"); SETTING("DIM"=>"0.77");}
DETECTOR_CSI(r27c056s1) { LABEL("CS056_1"); signal: /*R*/r29c4/in5;   hv: /*R*/r29c12/out13;  SETTING("THETA"=>"39.3 "); SETTING("PHI"=>"225.0"); SETTING("DIM"=>"0.77");}
DETECTOR_CSI(r27c057s1) { LABEL("CS057_1"); signal: /*R*/r29c4/in6;   hv: /*R*/r29c12/out14;  SETTING("THETA"=>"39.3 "); SETTING("PHI"=>"255.0"); SETTING("DIM"=>"0.77");}
DETECTOR_CSI(r27c058s1) { LABEL("CS058_1"); signal: /*L*/r28c4/in4;   hv: /*L*/r28c12/out12;  SETTING("THETA"=>"39.3 "); SETTING("PHI"=>"285.0"); SETTING("DIM"=>"0.77");}
DETECTOR_CSI(r27c059s1) { LABEL("CS059_1"); signal: /*L*/r28c4/in5;   hv: /*L*/r28c12/out13;  SETTING("THETA"=>"39.3 "); SETTING("PHI"=>"315.0"); SETTING("DIM"=>"0.77");}
DETECTOR_CSI(r27c060s1) { LABEL("CS060_1"); signal: /*L*/r28c4/in6;   hv: /*L*/r28c12/out14;  SETTING("THETA"=>"39.3 "); SETTING("PHI"=>"345.0"); SETTING("DIM"=>"0.77");}
DETECTOR_CSI(r27c061s1) { LABEL("CS061_1"); signal: /*L*/r28c4/in7;   hv: /*L*/r28c12/out15;  SETTING("THETA"=>"44.75"); SETTING("PHI"=>" 15.0"); SETTING("DIM"=>"0.71");}
DETECTOR_CSI(r27c062s1) { LABEL("CS062_1"); signal: /*L*/r28c4/in8;   hv: /*L*/r28c12/out16;  SETTING("THETA"=>"44.75"); SETTING("PHI"=>" 45.0"); SETTING("DIM"=>"0.71");}
DETECTOR_CSI(r27c063s1) { LABEL("CS063_1"); signal: /*L*/r28c5/in1;   hv: /*L*/r28c13/out1;   SETTING("THETA"=>"44.75"); SETTING("PHI"=>" 75.0"); SETTING("DIM"=>"0.71");}
DETECTOR_CSI(r27c064s1) { LABEL("CS064_1"); signal: /*R*/r29c4/in7;   hv: /*R*/r29c12/out15;  SETTING("THETA"=>"44.75"); SETTING("PHI"=>"105.0"); SETTING("DIM"=>"0.71");}
DETECTOR_CSI(r27c065s1) { LABEL("CS065_1"); signal: /*R*/r29c4/in8;   hv: /*R*/r29c12/out16;  SETTING("THETA"=>"44.75"); SETTING("PHI"=>"135.0"); SETTING("DIM"=>"0.71");}
DETECTOR_CSI(r27c066s1) { LABEL("CS066_1"); signal: /*R*/r29c5/in1;   hv: /*R*/r29c13/out1;   SETTING("THETA"=>"44.75"); SETTING("PHI"=>"165.0"); SETTING("DIM"=>"0.71");}
DETECTOR_CSI(r27c067s1) { LABEL("CS067_1"); signal: /*R*/r29c5/in2;   hv: /*R*/r29c13/out2;   SETTING("THETA"=>"44.75"); SETTING("PHI"=>"195.0"); SETTING("DIM"=>"0.71");}
DETECTOR_CSI(r27c068s1) { LABEL("CS068_1"); signal: /*R*/r29c5/in3;   hv: /*R*/r29c13/out3;   SETTING("THETA"=>"44.75"); SETTING("PHI"=>"225.0"); SETTING("DIM"=>"0.71");}
DETECTOR_CSI(r27c069s1) { LABEL("CS069_1"); signal: /*R*/r29c5/in4;   hv: /*R*/r29c13/out4;   SETTING("THETA"=>"44.75"); SETTING("PHI"=>"255.0"); SETTING("DIM"=>"0.71");}
DETECTOR_CSI(r27c070s1) { LABEL("CS070_1"); signal: /*L*/r28c5/in2;   hv: /*L*/r28c13/out2;   SETTING("THETA"=>"44.75"); SETTING("PHI"=>"285.0"); SETTING("DIM"=>"0.71");}
DETECTOR_CSI(r27c071s1) { LABEL("CS071_1"); signal: /*L*/r28c5/in3;   hv: /*L*/r28c13/out3;   SETTING("THETA"=>"44.75"); SETTING("PHI"=>"315.0"); SETTING("DIM"=>"0.71");}
DETECTOR_CSI(r27c072s1) { LABEL("CS072_1"); signal: /*L*/r28c5/in4;   hv: /*L*/r28c13/out4;   SETTING("THETA"=>"44.75"); SETTING("PHI"=>"345.0"); SETTING("DIM"=>"0.71");}
DETECTOR_CSI(r27c073s1) { LABEL("CS073_1"); signal: /*L*/r28c5/in5;   hv: /*L*/r28c13/out5;   SETTING("THETA"=>"50.3 "); SETTING("PHI"=>" 15.0"); SETTING("DIM"=>"0.64");}
DETECTOR_CSI(r27c074s1) { LABEL("CS074_1"); signal: /*L*/r28c5/in6;   hv: /*L*/r28c13/out6;   SETTING("THETA"=>"50.3 "); SETTING("PHI"=>" 45.0"); SETTING("DIM"=>"0.64");}
DETECTOR_CSI(r27c075s1) { LABEL("CS075_1"); signal: /*L*/r28c5/in7;   hv: /*L*/r28c13/out7;   SETTING("THETA"=>"50.3 "); SETTING("PHI"=>" 75.0"); SETTING("DIM"=>"0.64");}
DETECTOR_CSI(r27c076s1) { LABEL("CS076_1"); signal: /*R*/r29c5/in5;   hv: /*R*/r29c13/out5;   SETTING("THETA"=>"50.3 "); SETTING("PHI"=>"105.0"); SETTING("DIM"=>"0.64");}
DETECTOR_CSI(r27c077s1) { LABEL("CS077_1"); signal: /*R*/r29c5/in6;   hv: /*R*/r29c13/out6;   SETTING("THETA"=>"50.3 "); SETTING("PHI"=>"135.0"); SETTING("DIM"=>"0.64");}
DETECTOR_CSI(r27c078s1) { LABEL("CS078_1"); signal: /*R*/r29c5/in7;   hv: /*R*/r29c13/out7;   SETTING("THETA"=>"50.3 "); SETTING("PHI"=>"165.0"); SETTING("DIM"=>"0.64");}
DETECTOR_CSI(r27c079s1) { LABEL("CS079_1"); signal: /*R*/r29c5/in8;   hv: /*R*/r29c13/out8;   SETTING("THETA"=>"50.3 "); SETTING("PHI"=>"195.0"); SETTING("DIM"=>"0.64");}
DETECTOR_CSI(r27c080s1) { LABEL("CS080_1"); signal: /*R*/r29c6/in1;   hv: /*R*/r29c13/out9;   SETTING("THETA"=>"50.3 "); SETTING("PHI"=>"225.0"); SETTING("DIM"=>"0.64");}
DETECTOR_CSI(r27c081s1) { LABEL("CS081_1"); signal: /*R*/r29c6/in2;   hv: /*R*/r29c13/out10;  SETTING("THETA"=>"50.3 "); SETTING("PHI"=>"255.0"); SETTING("DIM"=>"0.64");}
DETECTOR_CSI(r27c082s1) { LABEL("CS082_1"); signal: /*L*/r28c5/in8;   hv: /*L*/r28c13/out8;   SETTING("THETA"=>"50.3 "); SETTING("PHI"=>"285.0"); SETTING("DIM"=>"0.64");}
DETECTOR_CSI(r27c083s1) { LABEL("CS083_1"); signal: /*L*/r28c6/in1;   hv: /*L*/r28c13/out9;   SETTING("THETA"=>"50.3 "); SETTING("PHI"=>"315.0"); SETTING("DIM"=>"0.64");}
DETECTOR_CSI(r27c084s1) { LABEL("CS084_1"); signal: /*L*/r28c6/in2;   hv: /*L*/r28c13/out10;  SETTING("THETA"=>"50.3 "); SETTING("PHI"=>"345.0"); SETTING("DIM"=>"0.64");}
DETECTOR_CSI(r27c085s1) { LABEL("CS085_1"); signal: /*L*/r28c6/in3;   hv: /*L*/r28c13/out11;  SETTING("THETA"=>"56.0 "); SETTING("PHI"=>" 15.0"); SETTING("DIM"=>"0.56");}
DETECTOR_CSI(r27c086s1) { LABEL("CS086_1"); signal: /*L*/r28c6/in4;   hv: /*L*/r28c13/out12;  SETTING("THETA"=>"56.0 "); SETTING("PHI"=>" 45.0"); SETTING("DIM"=>"0.56");}
DETECTOR_CSI(r27c087s1) { LABEL("CS087_1"); signal: /*L*/r28c6/in5;   hv: /*L*/r28c13/out13;  SETTING("THETA"=>"56.0 "); SETTING("PHI"=>" 75.0"); SETTING("DIM"=>"0.56");}
DETECTOR_CSI(r27c088s1) { LABEL("CS088_1"); signal: /*R*/r29c6/in3;   hv: /*R*/r29c13/out11;  SETTING("THETA"=>"56.0 "); SETTING("PHI"=>"105.0"); SETTING("DIM"=>"0.56");}
DETECTOR_CSI(r27c089s1) { LABEL("CS089_1"); signal: /*R*/r29c6/in4;   hv: /*R*/r29c13/out12;  SETTING("THETA"=>"56.0 "); SETTING("PHI"=>"135.0"); SETTING("DIM"=>"0.56");}
DETECTOR_CSI(r27c090s1) { LABEL("CS090_1"); signal: /*R*/r29c6/in5;   hv: /*R*/r29c13/out13;  SETTING("THETA"=>"56.0 "); SETTING("PHI"=>"165.0"); SETTING("DIM"=>"0.56");}
DETECTOR_CSI(r27c091s1) { LABEL("CS091_1"); signal: /*R*/r29c6/in6;   hv: /*R*/r29c13/out14;  SETTING("THETA"=>"56.0 "); SETTING("PHI"=>"195.0"); SETTING("DIM"=>"0.56");}
DETECTOR_CSI(r27c092s1) { LABEL("CS092_1"); signal: /*R*/r29c6/in7;   hv: /*R*/r29c13/out15;  SETTING("THETA"=>"56.0 "); SETTING("PHI"=>"225.0"); SETTING("DIM"=>"0.56");}
DETECTOR_CSI(r27c093s1) { LABEL("CS093_1"); signal: /*R*/r29c6/in8;   hv: /*R*/r29c13/out16;  SETTING("THETA"=>"56.0 "); SETTING("PHI"=>"255.0"); SETTING("DIM"=>"0.56");}
DETECTOR_CSI(r27c094s1) { LABEL("CS094_1"); signal: /*L*/r28c6/in6;   hv: /*L*/r28c13/out14;  SETTING("THETA"=>"56.0 "); SETTING("PHI"=>"285.0"); SETTING("DIM"=>"0.56");}
DETECTOR_CSI(r27c095s1) { LABEL("CS095_1"); signal: /*L*/r28c6/in7;   hv: /*L*/r28c13/out15;  SETTING("THETA"=>"56.0 "); SETTING("PHI"=>"315.0"); SETTING("DIM"=>"0.56");}
DETECTOR_CSI(r27c096s1) { LABEL("CS096_1"); signal: /*L*/r28c6/in8;   hv: /*L*/r28c13/out16;  SETTING("THETA"=>"56.0 "); SETTING("PHI"=>"345.0"); SETTING("DIM"=>"0.56");}
DETECTOR_CSI(r27c097s1) { LABEL("CS097_1"); signal: /*L*/r28c7/in1;   hv: /*L*/r28c14/out1;   SETTING("THETA"=>"61.95"); SETTING("PHI"=>" 15.0"); SETTING("DIM"=>"0.47");}
DETECTOR_CSI(r27c098s1) { LABEL("CS098_1"); signal: /*L*/r28c7/in2;   hv: /*L*/r28c14/out2;   SETTING("THETA"=>"61.95"); SETTING("PHI"=>" 45.0"); SETTING("DIM"=>"0.47");}
DETECTOR_CSI(r27c099s1) { LABEL("CS099_1"); signal: /*L*/r28c7/in3;   hv: /*L*/r28c14/out3;   SETTING("THETA"=>"61.95"); SETTING("PHI"=>" 75.0"); SETTING("DIM"=>"0.47");}
DETECTOR_CSI(r27c100s1) { LABEL("CS100_1"); signal: /*R*/r29c7/in1;   hv: /*R*/r29c14/out1;   SETTING("THETA"=>"61.95"); SETTING("PHI"=>"105.0"); SETTING("DIM"=>"0.47");}
DETECTOR_CSI(r27c101s1) { LABEL("CS101_1"); signal: /*R*/r29c7/in2;   hv: /*R*/r29c14/out2;   SETTING("THETA"=>"61.95"); SETTING("PHI"=>"135.0"); SETTING("DIM"=>"0.47");}
DETECTOR_CSI(r27c102s1) { LABEL("CS102_1"); signal: /*R*/r29c7/in3;   hv: /*R*/r29c14/out3;   SETTING("THETA"=>"61.95"); SETTING("PHI"=>"165.0"); SETTING("DIM"=>"0.47");}
DETECTOR_CSI(r27c103s1) { LABEL("CS103_1"); signal: /*R*/r29c7/in4;   hv: /*R*/r29c14/out4;   SETTING("THETA"=>"61.95"); SETTING("PHI"=>"195.0"); SETTING("DIM"=>"0.47");}
DETECTOR_CSI(r27c104s1) { LABEL("CS104_1"); signal: /*R*/r29c7/in5;   hv: /*R*/r29c14/out5;   SETTING("THETA"=>"61.95"); SETTING("PHI"=>"225.0"); SETTING("DIM"=>"0.47");}
DETECTOR_CSI(r27c105s1) { LABEL("CS105_1"); signal: /*R*/r29c7/in6;   hv: /*R*/r29c14/out6;   SETTING("THETA"=>"61.95"); SETTING("PHI"=>"255.0"); SETTING("DIM"=>"0.47");}
DETECTOR_CSI(r27c106s1) { LABEL("CS106_1"); signal: /*L*/r28c7/in4;   hv: /*L*/r28c14/out4;   SETTING("THETA"=>"61.95"); SETTING("PHI"=>"285.0"); SETTING("DIM"=>"0.47");}
DETECTOR_CSI(r27c107s1) { LABEL("CS107_1"); signal: /*L*/r28c7/in5;   hv: /*L*/r28c14/out5;   SETTING("THETA"=>"61.95"); SETTING("PHI"=>"315.0"); SETTING("DIM"=>"0.47");}
DETECTOR_CSI(r27c108s1) { LABEL("CS108_1"); signal: /*L*/r28c7/in6;   hv: /*L*/r28c14/out6;   SETTING("THETA"=>"61.95"); SETTING("PHI"=>"345.0"); SETTING("DIM"=>"0.47");}
DETECTOR_CSI(r27c109s1) { LABEL("CS109_1"); signal: /*L*/r28c7/in7;   hv: /*L*/r28c14/out7;   SETTING("THETA"=>"68.2 "); SETTING("PHI"=>" 15.0"); SETTING("DIM"=>"0.37");}
DETECTOR_CSI(r27c110s1) { LABEL("CS110_1"); signal: /*L*/r28c7/in8;   hv: /*L*/r28c14/out8;   SETTING("THETA"=>"68.2 "); SETTING("PHI"=>" 45.0"); SETTING("DIM"=>"0.37");}
DETECTOR_CSI(r27c111s1) { LABEL("CS111_1"); signal: /*L*/r28c8/in1;   hv: /*L*/r28c14/out9;   SETTING("THETA"=>"68.2 "); SETTING("PHI"=>" 75.0"); SETTING("DIM"=>"0.37");}
DETECTOR_CSI(r27c112s1) { LABEL("CS112_1"); signal: /*R*/r29c7/in7;   hv: /*R*/r29c14/out7;   SETTING("THETA"=>"68.2 "); SETTING("PHI"=>"105.0"); SETTING("DIM"=>"0.37");}
DETECTOR_CSI(r27c113s1) { LABEL("CS113_1"); signal: /*R*/r29c7/in8;   hv: /*R*/r29c14/out8;   SETTING("THETA"=>"68.2 "); SETTING("PHI"=>"135.0"); SETTING("DIM"=>"0.37");}
DETECTOR_CSI(r27c114s1) { LABEL("CS114_1"); signal: /*R*/r29c8/in1;   hv: /*R*/r29c14/out9;   SETTING("THETA"=>"68.2 "); SETTING("PHI"=>"165.0"); SETTING("DIM"=>"0.37");}
DETECTOR_CSI(r27c115s1) { LABEL("CS115_1"); signal: /*R*/r29c8/in2;   hv: /*R*/r29c14/out10;  SETTING("THETA"=>"68.2 "); SETTING("PHI"=>"195.0"); SETTING("DIM"=>"0.37");}
DETECTOR_CSI(r27c116s1) { LABEL("CS116_1"); signal: /*R*/r29c8/in3;   hv: /*R*/r29c14/out11;  SETTING("THETA"=>"68.2 "); SETTING("PHI"=>"225.0"); SETTING("DIM"=>"0.37");}
DETECTOR_CSI(r27c117s1) { LABEL("CS117_1"); signal: /*R*/r29c8/in4;   hv: /*R*/r29c14/out12;  SETTING("THETA"=>"68.2 "); SETTING("PHI"=>"255.0"); SETTING("DIM"=>"0.37");}
DETECTOR_CSI(r27c118s1) { LABEL("CS118_1"); signal: /*L*/r28c8/in2;   hv: /*L*/r28c14/out10;  SETTING("THETA"=>"68.2 "); SETTING("PHI"=>"285.0"); SETTING("DIM"=>"0.37");}
DETECTOR_CSI(r27c119s1) { LABEL("CS119_1"); signal: /*L*/r28c8/in3;   hv: /*L*/r28c14/out11;  SETTING("THETA"=>"68.2 "); SETTING("PHI"=>"315.0"); SETTING("DIM"=>"0.37");}
DETECTOR_CSI(r27c120s1) { LABEL("CS120_1"); signal: /*L*/r28c8/in4;   hv: /*L*/r28c14/out12;  SETTING("THETA"=>"68.2 "); SETTING("PHI"=>"345.0"); SETTING("DIM"=>"0.37");}
DETECTOR_CSI(r27c121s1) { LABEL("CS121_1"); signal: /*L*/r28c8/in5;   hv: /*L*/r28c14/out13;  SETTING("THETA"=>"74.9 "); SETTING("PHI"=>" 15.0"); SETTING("DIM"=>"0.26");}
DETECTOR_CSI(r27c122s1) { LABEL("CS122_1"); signal: /*L*/r28c8/in6;   hv: /*L*/r28c14/out14;  SETTING("THETA"=>"74.9 "); SETTING("PHI"=>" 45.0"); SETTING("DIM"=>"0.26");}
DETECTOR_CSI(r27c123s1) { LABEL("CS123_1"); signal: /*L*/r28c8/in7;   hv: /*L*/r28c14/out15;  SETTING("THETA"=>"74.9 "); SETTING("PHI"=>" 75.0"); SETTING("DIM"=>"0.26");}
DETECTOR_CSI(r27c124s1) { LABEL("CS124_1"); signal: /*R*/r29c8/in5;   hv: /*R*/r29c14/out13;  SETTING("THETA"=>"74.9 "); SETTING("PHI"=>"105.0"); SETTING("DIM"=>"0.26");}
DETECTOR_CSI(r27c125s1) { LABEL("CS125_1"); signal: /*R*/r29c8/in6;   hv: /*R*/r29c14/out14;  SETTING("THETA"=>"74.9 "); SETTING("PHI"=>"135.0"); SETTING("DIM"=>"0.26");}
DETECTOR_CSI(r27c126s1) { LABEL("CS126_1"); signal: /*R*/r29c8/in7;   hv: /*R*/r29c14/out15;  SETTING("THETA"=>"74.9 "); SETTING("PHI"=>"165.0"); SETTING("DIM"=>"0.26");}
DETECTOR_CSI(r27c127s1) { LABEL("CS127_1"); signal: /*R*/r29c8/in8;   hv: /*R*/r29c14/out16;  SETTING("THETA"=>"74.9 "); SETTING("PHI"=>"195.0"); SETTING("DIM"=>"0.26");}
DETECTOR_CSI(r27c128s1) { LABEL("CS128_1"); signal: /*R*/r29c9/in1;   hv: /*R*/r29c15/out1;   SETTING("THETA"=>"74.9 "); SETTING("PHI"=>"225.0"); SETTING("DIM"=>"0.26");}
DETECTOR_CSI(r27c129s1) { LABEL("CS129_1"); signal: /*R*/r29c9/in2;   hv: /*R*/r29c15/out2;   SETTING("THETA"=>"74.9 "); SETTING("PHI"=>"255.0"); SETTING("DIM"=>"0.26");}
DETECTOR_CSI(r27c130s1) { LABEL("CS130_1"); signal: /*L*/r28c8/in8;   hv: /*L*/r28c14/out16;  SETTING("THETA"=>"74.9 "); SETTING("PHI"=>"285.0"); SETTING("DIM"=>"0.26");}
DETECTOR_CSI(r27c131s1) { LABEL("CS131_1"); signal: /*L*/r28c9/in1;   hv: /*L*/r28c15/out1;   SETTING("THETA"=>"74.9 "); SETTING("PHI"=>"315.0"); SETTING("DIM"=>"0.26");}
DETECTOR_CSI(r27c132s1) { LABEL("CS132_1"); signal: /*L*/r28c9/in2;   hv: /*L*/r28c15/out2;   SETTING("THETA"=>"74.9 "); SETTING("PHI"=>"345.0"); SETTING("DIM"=>"0.26");}
DETECTOR_CSI(r27c133s1) { LABEL("CS133_1"); signal: /*L*/r28c9/in3;   hv: /*L*/r28c15/out3;   SETTING("THETA"=>"80.95"); SETTING("PHI"=>" 15.0"); SETTING("DIM"=>"0.16");}
DETECTOR_CSI(r27c134s1) { LABEL("CS134_1"); signal: /*L*/r28c9/in4;   hv: /*L*/r28c15/out4;   SETTING("THETA"=>"80.95"); SETTING("PHI"=>" 45.0"); SETTING("DIM"=>"0.16");}
DETECTOR_CSI(r27c135s1) { LABEL("CS135_1"); signal: /*L*/r28c9/in5;   hv: /*L*/r28c15/out5;   SETTING("THETA"=>"80.95"); SETTING("PHI"=>" 75.0"); SETTING("DIM"=>"0.16");}
DETECTOR_CSI(r27c136s1) { LABEL("CS136_1"); signal: /*R*/r29c9/in3;   hv: /*R*/r29c15/out3;   SETTING("THETA"=>"80.95"); SETTING("PHI"=>"105.0"); SETTING("DIM"=>"0.16");}
DETECTOR_CSI(r27c137s1) { LABEL("CS137_1"); signal: /*R*/r29c9/in4;   hv: /*R*/r29c15/out4;   SETTING("THETA"=>"80.95"); SETTING("PHI"=>"135.0"); SETTING("DIM"=>"0.16");}
DETECTOR_CSI(r27c138s1) { LABEL("CS138_1"); signal: /*R*/r29c9/in5;   hv: /*R*/r29c15/out5;   SETTING("THETA"=>"80.95"); SETTING("PHI"=>"165.0"); SETTING("DIM"=>"0.16");}
DETECTOR_CSI(r27c139s1) { LABEL("CS139_1"); signal: /*R*/r29c9/in6;   hv: /*R*/r29c15/out6;   SETTING("THETA"=>"80.95"); SETTING("PHI"=>"195.0"); SETTING("DIM"=>"0.16");}
DETECTOR_CSI(r27c140s1) { LABEL("CS140_1"); signal: /*R*/r29c9/in7;   hv: /*R*/r29c15/out7;   SETTING("THETA"=>"80.95"); SETTING("PHI"=>"225.0"); SETTING("DIM"=>"0.16");}
DETECTOR_CSI(r27c141s1) { LABEL("CS141_1"); signal: /*R*/r29c9/in8;   hv: /*R*/r29c15/out8;   SETTING("THETA"=>"80.95"); SETTING("PHI"=>"255.0"); SETTING("DIM"=>"0.16");}
DETECTOR_CSI(r27c142s1) { LABEL("CS142_1"); signal: /*L*/r28c9/in6;   hv: /*L*/r28c15/out6;   SETTING("THETA"=>"80.95"); SETTING("PHI"=>"285.0"); SETTING("DIM"=>"0.16");}
DETECTOR_CSI(r27c143s1) { LABEL("CS143_1"); signal: /*L*/r28c9/in7;   hv: /*L*/r28c15/out7;   SETTING("THETA"=>"80.95"); SETTING("PHI"=>"315.0"); SETTING("DIM"=>"0.16");}
DETECTOR_CSI(r27c144s1) { LABEL("CS144_1"); signal: /*L*/r28c9/in8;   hv: /*L*/r28c15/out8;   SETTING("THETA"=>"80.95"); SETTING("PHI"=>"345.0"); SETTING("DIM"=>"0.16");}

// Outer veto wall:

//DETECTOR_CSI(r27c145s1) { LABEL("CS1_145"); signal: /*L*/r28c10/in1;  hv: /*L*/r28c15/out9;   SETTING("PHI"=>"50.3 "); SETTING("THETA"=>" 30.0"); SETTING("DIM"=>"0.64");}
//DETECTOR_CSI(r27c146s1) { LABEL("CS1_146"); signal: /*L*/r28c10/in2;  hv: /*L*/r28c15/out10;  SETTING("PHI"=>"50.3 "); SETTING("THETA"=>" 60.0"); SETTING("DIM"=>"0.64");}
//DETECTOR_CSI(r27c147s1) { LABEL("CS1_147"); signal: /*L*/r28c10/in3;  hv: /*L*/r28c15/out11;  SETTING("PHI"=>"50.3 "); SETTING("THETA"=>" 90.0"); SETTING("DIM"=>"0.64");}
//DETECTOR_CSI(r27c148s1) { LABEL("CS1_148"); signal: /*R*/r29c10/in1;  hv: /*R*/r29c15/out9;   SETTING("PHI"=>"50.3 "); SETTING("THETA"=>"120.0"); SETTING("DIM"=>"0.64");}
//DETECTOR_CSI(r27c149s1) { LABEL("CS1_149"); signal: /*R*/r29c10/in2;  hv: /*R*/r29c15/out10;  SETTING("PHI"=>"50.3 "); SETTING("THETA"=>"150.0"); SETTING("DIM"=>"0.64");}
//DETECTOR_CSI(r27c150s1) { LABEL("CS1_150"); signal: /*R*/r29c10/in3;  hv: /*R*/r29c15/out11;  SETTING("PHI"=>"50.3 "); SETTING("THETA"=>"180.0"); SETTING("DIM"=>"0.64");}
//DETECTOR_CSI(r27c151s1) { LABEL("CS1_151"); signal: /*R*/r29c10/in4;  hv: /*R*/r29c15/out12;  SETTING("PHI"=>"50.3 "); SETTING("THETA"=>"210.0"); SETTING("DIM"=>"0.64");}
//DETECTOR_CSI(r27c152s1) { LABEL("CS1_152"); signal: /*R*/r29c10/in5;  hv: /*R*/r29c15/out13;  SETTING("PHI"=>"50.3 "); SETTING("THETA"=>"240.0"); SETTING("DIM"=>"0.64");}
//DETECTOR_CSI(r27c153s1) { LABEL("CS1_153"); signal: /*R*/r29c10/in6;  hv: /*R*/r29c15/out14;  SETTING("PHI"=>"50.3 "); SETTING("THETA"=>"270.0"); SETTING("DIM"=>"0.64");}
//DETECTOR_CSI(r27c154s1) { LABEL("CS1_154"); signal: /*L*/r28c10/in4;  hv: /*L*/r28c15/out12;  SETTING("PHI"=>"50.3 "); SETTING("THETA"=>"300.0"); SETTING("DIM"=>"0.64");}
//DETECTOR_CSI(r27c155s1) { LABEL("CS1_155"); signal: /*L*/r28c10/in5;  hv: /*L*/r28c15/out13;  SETTING("PHI"=>"50.3 "); SETTING("THETA"=>"330.0"); SETTING("DIM"=>"0.64");}
//DETECTOR_CSI(r27c156s1) { LABEL("CS1_156"); signal: /*L*/r28c10/in6;  hv: /*L*/r28c15/out14;  SETTING("PHI"=>"50.3 "); SETTING("THETA"=>"360.0"); SETTING("DIM"=>"0.64");}


// Left panel

JOINER_8(r28c1) // L1
{
 in1: r27c001s1/signal;
 in2: r27c002s1/signal;
 in3: r27c003s1/signal;
 in4: r27c010s1/signal;
 in5: r27c011s1/signal;
 in6: r27c012s1/signal;
 in7: r27c013s1/signal;
 in8: r27c014s1/signal;

 out1_8: r13c3s21/in1_8;
}

JOINER_8(r28c2) // L2
{
 in1: r27c015s1/signal;
 in2: r27c022s1/signal;
 in3: r27c023s1/signal;
 in4: r27c024s1/signal;
 in5: r27c025s1/signal;
 in6: r27c026s1/signal;
 in7: r27c027s1/signal;
 in8: r27c034s1/signal;

 out1_8: r13c3s21/in9_16;
}

JOINER_8(r28c3) // L3
{
 in1: r27c035s1/signal;
 in2: r27c036s1/signal;
 in3: r27c037s1/signal;
 in4: r27c038s1/signal;
 in5: r27c039s1/signal;
 in6: r27c046s1/signal;
 in7: r27c047s1/signal;
 in8: r27c048s1/signal;

 out1_8: r13c3s20/in1_8;
}

JOINER_8(r28c4) // L4
{
 in1: r27c049s1/signal;
 in2: r27c050s1/signal;
 in3: r27c051s1/signal;
 in4: r27c058s1/signal;
 in5: r27c059s1/signal;
 in6: r27c060s1/signal;
 in7: r27c061s1/signal;
 in8: r27c062s1/signal;

 out1_8: r13c3s20/in9_16;
}

JOINER_8(r28c5) // L5
{
 in1: r27c063s1/signal;
 in2: r27c070s1/signal;
 in3: r27c071s1/signal;
 in4: r27c072s1/signal;
 in5: r27c073s1/signal;
 in6: r27c074s1/signal;
 in7: r27c075s1/signal;
 in8: r27c082s1/signal;

 out1_8: r13c3s19/in1_8;
}

JOINER_8(r28c6) // L6
{
 in1: r27c083s1/signal;
 in2: r27c084s1/signal;
 in3: r27c085s1/signal;
 in4: r27c086s1/signal;
 in5: r27c087s1/signal;
 in6: r27c094s1/signal;
 in7: r27c095s1/signal;
 in8: r27c096s1/signal;

 out1_8: r13c3s19/in9_16;
}

JOINER_8(r28c7) // L7
{
 in1: r27c097s1/signal;
 in2: r27c098s1/signal;
 in3: r27c099s1/signal;
 in4: r27c106s1/signal;
 in5: r27c107s1/signal;
 in6: r27c108s1/signal;
 in7: r27c109s1/signal;
 in8: r27c110s1/signal;

 out1_8: r13c3s17/in1_8;
}

JOINER_8(r28c8) // L8
{
 in1: r27c111s1/signal;
 in2: r27c118s1/signal;
 in3: r27c119s1/signal;
 in4: r27c120s1/signal;
 in5: r27c121s1/signal;
 in6: r27c122s1/signal;
 in7: r27c123s1/signal;
 in8: r27c130s1/signal;

 out1_8: r13c3s17/in9_16;
}

JOINER_8(r28c9) // L9
{
 in1: r27c131s1/signal;
 in2: r27c132s1/signal;
 in3: r27c133s1/signal;
 in4: r27c134s1/signal;
 in5: r27c135s1/signal;
 in6: r27c142s1/signal;
 in7: r27c143s1/signal;
 in8: r27c144s1/signal;

 out1_8: r13c3s16/in1_8;
}


// Right panel

JOINER_8(r29c1) // R1
{
 in1: r27c004s1/signal;
 in2: r27c005s1/signal;
 in3: r27c006s1/signal;
 in4: r27c007s1/signal;
 in5: r27c008s1/signal;
 in6: r27c009s1/signal;
 in7: r27c016s1/signal;
 in8: r27c017s1/signal;

 out1_8: r13c3s16/in9_16;
}

JOINER_8(r29c2) // R2
{
 in1: r27c018s1/signal;
 in2: r27c019s1/signal;
 in3: r27c020s1/signal;
 in4: r27c021s1/signal;
 in5: r27c028s1/signal;
 in6: r27c029s1/signal;
 in7: r27c030s1/signal;
 in8: r27c031s1/signal;

 out1_8: r13c3s15/in1_8;
}

JOINER_8(r29c3) // R3
{
 in1: r27c032s1/signal;
 in2: r27c033s1/signal;
 in3: r27c040s1/signal;
 in4: r27c041s1/signal;
 in5: r27c042s1/signal;
 in6: r27c043s1/signal;
 in7: r27c044s1/signal;
 in8: r27c045s1/signal;

 out1_8: r13c3s15/in9_16;
}

JOINER_8(r29c4) // R4
{
 in1: r27c052s1/signal;
 in2: r27c053s1/signal;
 in3: r27c054s1/signal;
 in4: r27c055s1/signal;
 in5: r27c056s1/signal;
 in6: r27c057s1/signal;
 in7: r27c064s1/signal;
 in8: r27c065s1/signal;

 out1_8: r13c3s14/in1_8;
}

JOINER_8(r29c5) // R5
{
 in1: r27c066s1/signal;
 in2: r27c067s1/signal;
 in3: r27c068s1/signal;
 in4: r27c069s1/signal;
 in5: r27c076s1/signal;
 in6: r27c077s1/signal;
 in7: r27c078s1/signal;
 in8: r27c079s1/signal;

 out1_8: r13c3s14/in9_16;
}

JOINER_8(r29c6) // R6
{
 in1: r27c080s1/signal;
 in2: r27c081s1/signal;
 in3: r27c088s1/signal;
 in4: r27c089s1/signal;
 in5: r27c090s1/signal;
 in6: r27c091s1/signal;
 in7: r27c092s1/signal;
 in8: r27c093s1/signal;

 out1_8: r13c3s13/in1_8;
}

JOINER_8(r29c7) // R7
{
 in1: r27c100s1/signal;
 in2: r27c101s1/signal;
 in3: r27c102s1/signal;
 in4: r27c103s1/signal;
 in5: r27c104s1/signal;
 in6: r27c105s1/signal;
 in7: r27c112s1/signal;
 in8: r27c113s1/signal;

 out1_8: r13c3s13/in9_16;
}

JOINER_8(r29c8) // R8
{
 in1: r27c114s1/signal;
 in2: r27c115s1/signal;
 in3: r27c116s1/signal;
 in4: r27c117s1/signal;
 in5: r27c124s1/signal;
 in6: r27c125s1/signal;
 in7: r27c126s1/signal;
 in8: r27c127s1/signal;

 out1_8: r13c3s12/in1_8;
}

JOINER_8(r29c9) // R9
{
 in1: r27c128s1/signal;
 in2: r27c129s1/signal;
 in3: r27c136s1/signal;
 in4: r27c137s1/signal;
 in5: r27c138s1/signal;
 in6: r27c139s1/signal;
 in7: r27c140s1/signal;
 in8: r27c141s1/signal;

 out1_8: r13c3s12/in9_16;
}

// Left panel

HV_CONN_16(r28c11) // L1
{
 in1_16: r10c3s0/out0_15;

 out1:  r27c001s1/hv;
 out2:  r27c002s1/hv;
 out3:  r27c003s1/hv;
 out4:  r27c010s1/hv;
 out5:  r27c011s1/hv;
 out6:  r27c012s1/hv;
 out7:  r27c013s1/hv;
 out8:  r27c014s1/hv;
 out9:  r27c015s1/hv;
 out10: r27c022s1/hv;
 out11: r27c023s1/hv;
 out12: r27c024s1/hv;
 out13: r27c025s1/hv;
 out14: r27c026s1/hv;
 out15: r27c027s1/hv;
 out16: r27c034s1/hv;
}

HV_CONN_16(r28c12) // L2
{
 in1_16: r10c3s1/out0_15;

 out1:  r27c035s1/hv;
 out2:  r27c036s1/hv;
 out3:  r27c037s1/hv;
 out4:  r27c038s1/hv;
 out5:  r27c039s1/hv;
 out6:  r27c046s1/hv;
 out7:  r27c047s1/hv;
 out8:  r27c048s1/hv;
 out9:  r27c049s1/hv;
 out10: r27c050s1/hv;
 out11: r27c051s1/hv;
 out12: r27c058s1/hv;
 out13: r27c059s1/hv;
 out14: r27c060s1/hv;
 out15: r27c061s1/hv;
 out16: r27c062s1/hv;
}

HV_CONN_16(r28c13) // L3
{
 in1_16: r10c3s2/out0_15;

 out1:  r27c063s1/hv;
 out2:  r27c070s1/hv;
 out3:  r27c071s1/hv;
 out4:  r27c072s1/hv;
 out5:  r27c073s1/hv;
 out6:  r27c074s1/hv;
 out7:  r27c075s1/hv;
 out8:  r27c082s1/hv;
 out9:  r27c083s1/hv;
 out10: r27c084s1/hv;
 out11: r27c085s1/hv;
 out12: r27c086s1/hv;
 out13: r27c087s1/hv;
 out14: r27c094s1/hv;
 out15: r27c095s1/hv;
 out16: r27c096s1/hv;
}


HV_CONN_16(r28c14) // L4
{
 in1_16: r10c3s3/out0_15;

 out1:  r27c097s1/hv;
 out2:  r27c098s1/hv;
 out3:  r27c099s1/hv;
 out4:  r27c106s1/hv;
 out5:  r27c107s1/hv;
 out6:  r27c108s1/hv;
 out7:  r27c109s1/hv;
 out8:  r27c110s1/hv;
 out9:  r27c111s1/hv;
 out10: r27c118s1/hv;
 out11: r27c119s1/hv;
 out12: r27c120s1/hv;
 out13: r27c121s1/hv;
 out14: r27c122s1/hv;
 out15: r27c123s1/hv;
 out16: r27c130s1/hv;
}


HV_CONN_16(r28c15) // L5
{
 in1_16: r10c3s4/out0_15;

 out1:  r27c131s1/hv;
 out2:  r27c132s1/hv;
 out3:  r27c133s1/hv;
 out4:  r27c134s1/hv;
 out5:  r27c135s1/hv;
 out6:  r27c142s1/hv;
 out7:  r27c143s1/hv;
 out8:  r27c144s1/hv;
 // out9:  r27cs1/hv;
 // out10: r27cs1/hv;
 // out11: r27cs1/hv;
 // out12: r27cs1/hv;
 // out13: r27cs1/hv;
 // out14: r27cs1/hv;
 // out15: r27cs1/hv;
 // out16: r27cs1/hv;
}

#if USING_CV
HV_CONN_16(r28c16) // L6
{
 in1_16: r10c3s10/out0_15;

 out1:  rCVc001s1/hv;
 out2:  rCVc002s1/hv;
 out3:  rCVc003s1/hv;
 out4:  rCVc004s1/hv;
 out5:  rCVc005s1/hv;
 out6:  rCVc006s1/hv;
 out7:  rCVc007s1/hv;
 out8:  rCVc008s1/hv;
 out9:  rCVc009s1/hv;
 out10: rCVc010s1/hv;
 out11: rCVc011s1/hv;
 out12: rCVc012s1/hv;
}
#endif

// Right panel

HV_CONN_16(r29c11) // R1
{
 in1_16: r10c3s5/out0_15;

 out1:  r27c004s1/hv;
 out2:  r27c005s1/hv;
 out3:  r27c006s1/hv;
 out4:  r27c007s1/hv;
 out5:  r27c008s1/hv;
 out6:  r27c009s1/hv;
 out7:  r27c016s1/hv;
 out8:  r27c017s1/hv;
 out9:  r27c018s1/hv;
 out10: r27c019s1/hv;
 out11: r27c020s1/hv;
 out12: r27c021s1/hv;
 out13: r27c028s1/hv;
 out14: r27c029s1/hv;
 out15: r27c030s1/hv;
 out16: r27c031s1/hv;
}

HV_CONN_16(r29c12) // R2
{
 in1_16: r10c3s6/out0_15;

 out1:  r27c032s1/hv;
 out2:  r27c033s1/hv;
 out3:  r27c040s1/hv;
 out4:  r27c041s1/hv;
 out5:  r27c042s1/hv;
 out6:  r27c043s1/hv;
 out7:  r27c044s1/hv;
 out8:  r27c045s1/hv;
 out9:  r27c052s1/hv;
 out10: r27c053s1/hv;
 out11: r27c054s1/hv;
 out12: r27c055s1/hv;
 out13: r27c056s1/hv;
 out14: r27c057s1/hv;
 out15: r27c064s1/hv;
 out16: r27c065s1/hv;
}

HV_CONN_16(r29c13) // R3
{
 in1_16: r10c3s7/out0_15;

 out1:  r27c066s1/hv;
 out2:  r27c067s1/hv;
 out3:  r27c068s1/hv;
 out4:  r27c069s1/hv;
 out5:  r27c076s1/hv;
 out6:  r27c077s1/hv;
 out7:  r27c078s1/hv;
 out8:  r27c079s1/hv;
 out9:  r27c080s1/hv;
 out10: r27c081s1/hv;
 out11: r27c088s1/hv;
 out12: r27c089s1/hv;
 out13: r27c090s1/hv;
 out14: r27c091s1/hv;
 out15: r27c092s1/hv;
 out16: r27c093s1/hv;
}


HV_CONN_16(r29c14) // R4
{
 in1_16: r10c3s8/out0_15;

 out1:  r27c100s1/hv;
 out2:  r27c101s1/hv;
 out3:  r27c102s1/hv;
 out4:  r27c103s1/hv;
 out5:  r27c104s1/hv;
 out6:  r27c105s1/hv;
 out7:  r27c112s1/hv;
 out8:  r27c113s1/hv;
 out9:  r27c114s1/hv;
 out10: r27c115s1/hv;
 out11: r27c116s1/hv;
 out12: r27c117s1/hv;
 out13: r27c124s1/hv;
 out14: r27c125s1/hv;
 out15: r27c126s1/hv;
 out16: r27c127s1/hv;
}


HV_CONN_16(r29c15) // R5
{
 in1_16: r10c3s9/out0_15;

 out1:  r27c128s1/hv;
 out2:  r27c129s1/hv;
 out3:  r27c136s1/hv;
 out4:  r27c137s1/hv;
 out5:  r27c138s1/hv;
 out6:  r27c139s1/hv;
 out7:  r27c140s1/hv;
 out8:  r27c141s1/hv;
 // out9:  r27cs1/hv;
 // out10: r27cs1/hv;
 // out11: r27cs1/hv;
 // out12: r27cs1/hv;
 // out13: r27cs1/hv;
 // out14: r27cs1/hv;
 // out15: r27cs1/hv;
 // out16: r27cs1/hv;
}




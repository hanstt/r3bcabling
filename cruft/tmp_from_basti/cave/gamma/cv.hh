MODULE(DETECTOR_CV)
{
  HAS_SETTING("PHI" => "%f");

  CONNECTOR(signal,SIGNAL); // Lemo out?
  CONNECTOR(hv,TENSION); // Lemo in?
}

// Inner veto barrel:

DETECTOR_CV(rCVc001s1) { LABEL("CV1_001"); signal: rBEAMR1c3/in1;  hv: /*L*/r28c16/out1;  SETTING("PHI"=>" 15.0"); }
DETECTOR_CV(rCVc002s1) { LABEL("CV1_002"); signal: rBEAMR1c3/in2;  hv: /*L*/r28c16/out2;  SETTING("PHI"=>" 45.0"); }
DETECTOR_CV(rCVc003s1) { LABEL("CV1_003"); signal: rBEAMR1c3/in3;  hv: /*L*/r28c16/out3;  SETTING("PHI"=>" 75.0"); }
DETECTOR_CV(rCVc004s1) { LABEL("CV1_004"); signal: rBEAMR1c3/in4;  hv: /*R*/r28c16/out4;  SETTING("PHI"=>"105.0"); }
DETECTOR_CV(rCVc005s1) { LABEL("CV1_005"); signal: rBEAMR1c3/in5;  hv: /*R*/r28c16/out5;  SETTING("PHI"=>"135.0"); }
DETECTOR_CV(rCVc006s1) { LABEL("CV1_006"); signal: rBEAMR1c3/in6;  hv: /*R*/r28c16/out6;  SETTING("PHI"=>"165.0"); }
DETECTOR_CV(rCVc007s1) { LABEL("CV1_007"); signal: rBEAMR1c3/in7;  hv: /*R*/r28c16/out7;  SETTING("PHI"=>"195.0"); }
DETECTOR_CV(rCVc008s1) { LABEL("CV1_008"); signal: rBEAMR1c3/in8;  hv: /*R*/r28c16/out8;  SETTING("PHI"=>"225.0"); }
DETECTOR_CV(rCVc009s1) { LABEL("CV1_009"); signal: rBEAMR1c3/in9;  hv: /*R*/r28c16/out9;  SETTING("PHI"=>"255.0"); }
DETECTOR_CV(rCVc010s1) { LABEL("CV1_010"); signal: rBEAMR1c3/in10; hv: /*L*/r28c16/out10; SETTING("PHI"=>"285.0"); }
DETECTOR_CV(rCVc011s1) { LABEL("CV1_011"); signal: rBEAMR1c3/in11; hv: /*L*/r28c16/out11; SETTING("PHI"=>"315.0"); }
DETECTOR_CV(rCVc012s1) { LABEL("CV1_012"); signal: rBEAMR1c3/in12; hv: /*L*/r28c16/out12; SETTING("PHI"=>"345.0"); }

CABLE_DELAY_16(rBEAMR1c3)
{
  SETTING("DELAY" => "50ns");

 in1:  rCVc001s1/signal;
 in2:  rCVc002s1/signal;
 in3:  rCVc003s1/signal;
 in4:  rCVc004s1/signal;
 in5:  rCVc005s1/signal;
 in6:  rCVc006s1/signal;
 in7:  rCVc007s1/signal;
 in8:  rCVc008s1/signal;

 out1_8: r13c3s18/in1_8;

 in9:  rCVc009s1/signal;
 in10: rCVc010s1/signal;
 in11: rCVc011s1/signal;
 in12: rCVc012s1/signal;

 out9_12: r13c3s18/in10_13; // cables 2,3,4,5
}


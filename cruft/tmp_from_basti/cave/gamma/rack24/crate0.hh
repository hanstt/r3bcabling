//--------------- VME CRATE -------------//
VME_CRATE(r24c0)
{
  SETTING("CRATE_NO" => "3");
}

RIO4(r24c0s1) 
{
    SETTING("HOSTNAME"=>"r4-11");
}
      
TRIVA5(r24c0s3) 
{
  ;
}

MADC32(r24c0s05)
{
  SERIAL("CBR01");

  SETTING("ADDRESS" => "0x00730000");
  SETTING("VIRTUAL_SLOT" => "3");

  in1_16:  r22c0s15/e1_16; // module 7
  in17_32: r22c0s17/e1_16; // module 8
  nim_gate0: .s13/nim_out5;
  nim_busy: ;
}

MADC32(r24c0s07)
{
  SERIAL("CBR02");

  SETTING("ADDRESS" => "0x00720000");
  SETTING("VIRTUAL_SLOT" => "2");

  in1_16:  r22c0s11/e1_16; // module 5
  in17_32: r22c0s13/e1_16; // module 6
  nim_gate0: .s13/nim_out4;
  nim_busy: ;
}

MADC32(r24c0s09)
{
  SERIAL("CBR03");

  SETTING("ADDRESS" => "0x00710000");
  SETTING("VIRTUAL_SLOT" => "1");

  in1_16:  r22c0s07/e1_16; // module 3
  in17_32: r22c0s09/e1_16; // module 4
  nim_gate0: .s13/nim_out3;
  nim_busy: ;
}
MADC32(r24c0s11)
{
  SERIAL("CBR04");

  SETTING("ADDRESS" => "0x00700000");
  SETTING("VIRTUAL_SLOT" => "0");

  in1_16:  r22c0s01/e1_16; // module 1
  in17_32: r22c0s03/e1_16; // module 2
  nim_gate0: .s13/nim_out2;
  nim_busy: r22c0s1/rc;
}

MADC32(r24c0s12)
{
  SERIAL("CBR05");

  SETTING("ADDRESS" => "0x00770000");

  nim_busy: r22c0s07/rc;
}

ENV1(r24c0s13)
{
  nim_in1:    .s21/out8;

  in_ecl2_9:  .s13/out_ecl1_8; // Not exactly true as in9 is not connected
  out_ecl1_8: .s13/in_ecl2_9; 
  
  nim_out1:   .s19/in_nim;
  nim_out2:   .s11/nim_gate0;
  nim_out3:   .s9/nim_gate0;
  nim_out4:   .s7/nim_gate0;
  nim_out5:   .s5/nim_gate0;
}

HDCAN1(r24c0s15)
{
  in1_16:  r22c0s15/t1_16; // module 7
  in17_32: r22c0s17/t1_16; // module 8

  out1_32:	   .s19/in1_32;
}

HDCAN1(r24c0s16)
{
  in1_16:  r22c0s11/t1_16; // module 5
  in17_32: r22c0s13/t1_16; // module 6

  out1_32:	   .s19/in33_64;
}

HDCAN1(r24c0s17)
{
  in1_16:  r22c0s07/t1_16; // module 3
  in17_32: r22c0s09/t1_16; // module 4

  out1_32:	   .s19/in65_96;
}

HDCAN1(r24c0s18)
{
  in1_16:  r22c0s01/t1_16; // module 1
  in17_32: r22c0s03/t1_16; // module 2

  out1_32:	   .s19/in97_128;
}

VUPROM2(r24c0s19)
{
  SERIAL("CBR06");

  SETTING("ADDRESS" => "0x05000000");
  SETTING("VIRTUAL_SLOT" => "4");

  in1_32:	  .s15/out1_32;
  in33_64:	  .s16/out1_32;
  in65_96:	  .s17/out1_32;
  in97_128:	  .s18/out1_32;

  in_nim: .s13/nim_out1;

  out1_32:	  .s20/in1_32;
}

HDTTL1(r24c0s20)
{
  in1_32: .s19/out1_32;
}

TRIDI(r24c0s21)
{
  in1: r22c0s19u1/out1;

  out8: .s13/nim_in1;
}



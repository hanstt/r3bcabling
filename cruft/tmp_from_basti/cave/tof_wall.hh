MODULE(DETECTOR_TFW)
{
  CONNECTOR(signal);
  CONNECTOR(hv);
}

MODULE(DETECTOR_NTF)
{
  CONNECTOR(signal);
  CONNECTOR(hv);
}

MODULE(DETECTOR_DTF)
{
  CONNECTOR(signal);
}


/* The connectors for the TFW wall are on two panels one leg of the frame.
 * 
 * Left panel   Right panel
 *
 * TFW1 (hv)    TFW4 (hv)
 * TFW2 (hv)
 * TFW3 (hv)
 *
 * 1 (signal)   7 (signal)
 * 2 (signal)   8 (signal)
 * 3 (signal)   
 * 4 (signal)   
 * 5 (signal)   
 * 6 (signal)   
 *
 * The single channels in the signal connectors are numberd from right
 * to left, 1 - 8.
 *
 * Signal connector 1-4, first half of 5th serve vertical paddles
 * (extended in Y direction), connectors second half of 5th and 5-8 serve
 * horisontal paddles.
 *
 * Paddles 1-18 are vertical, 19-32 horizontal.
 */

/* Naming scheme for time-of-flight wall:
 *
 * DETECTOR_TFW ('the pm tube', r20cXXXsY), XXX is the paddle number,
 * Y is the pm number.  1 or 2.
 *
 * JOINER_8(r26cXX) are the (signal) cable joiners sitting on the frame.
 *
 * HV_CONN_16(r26cXX) are the HV connectors sitting on the frame.
 *
 * r25 is thus the detector itself.
 * r26 is the cable joiners on the frame.
 
 *
 * DTF:
 * C Langer on 05/08/2010...new detector for s393
 * it uses the old CB electronics
 *

*/


DETECTOR_TFW(r25c01s1) { LABEL("TFW01_1"); signal: r26c1/in1; hv: r26c11/out1;  }
DETECTOR_TFW(r25c01s2) { LABEL("TFW01_2"); signal: r26c1/in2; hv: r26c11/out2;  }
DETECTOR_TFW(r25c02s1) { LABEL("TFW02_1"); signal: r26c1/in3; hv: r26c11/out3;  }
DETECTOR_TFW(r25c02s2) { LABEL("TFW02_2"); signal: r26c1/in4; hv: r26c11/out4;  }
DETECTOR_TFW(r25c03s1) { LABEL("TFW03_1"); signal: r26c1/in5; hv: r26c11/out5;  }
DETECTOR_TFW(r25c03s2) { LABEL("TFW03_2"); signal: r26c1/in6; hv: r26c11/out6;  }
DETECTOR_TFW(r25c04s1) { LABEL("TFW04_1"); signal: r26c1/in7; hv: r26c11/out7;  }
DETECTOR_TFW(r25c04s2) { LABEL("TFW04_2"); signal: r26c1/in8; hv: r26c11/out8;  }
DETECTOR_TFW(r25c05s1) { LABEL("TFW05_1"); signal: r26c2/in1; hv: r26c11/out9;  }
DETECTOR_TFW(r25c05s2) { LABEL("TFW05_2"); signal: r26c2/in2; hv: r26c11/out10; }
DETECTOR_TFW(r25c06s1) { LABEL("TFW06_1"); signal: r26c2/in3; hv: r26c11/out11; }
DETECTOR_TFW(r25c06s2) { LABEL("TFW06_2"); signal: r26c2/in4; hv: r26c11/out12; }
DETECTOR_TFW(r25c07s1) { LABEL("TFW07_1"); signal: r26c2/in5; hv: r26c11/out13; }
DETECTOR_TFW(r25c07s2) { LABEL("TFW07_2"); signal: r26c2/in6; hv: r26c11/out14; }
DETECTOR_TFW(r25c08s1) { LABEL("TFW08_1"); signal: r26c2/in7; hv: r26c11/out15; }
DETECTOR_TFW(r25c08s2) { LABEL("TFW08_2"); signal: r26c2/in8; hv: r26c11/out16; }

DETECTOR_TFW(r25c09s1) { LABEL("TFW09_1"); signal: r26c3/in1; hv: r26c12/out1;  }
DETECTOR_TFW(r25c09s2) { LABEL("TFW09_2"); signal: r26c3/in2; hv: r26c12/out2;  }
DETECTOR_TFW(r25c10s1) { LABEL("TFW10_1"); signal: r26c3/in3; hv: r26c12/out3;  }
DETECTOR_TFW(r25c10s2) { LABEL("TFW10_2"); signal: r26c3/in4; hv: r26c12/out4;  }
DETECTOR_TFW(r25c11s1) { LABEL("TFW11_1"); signal: r26c3/in5; hv: r26c12/out5;  }
DETECTOR_TFW(r25c11s2) { LABEL("TFW11_2"); signal: r26c3/in6; hv: r26c12/out6;  }
DETECTOR_TFW(r25c12s1) { LABEL("TFW12_1"); signal: r26c3/in7; hv: r26c12/out7;  }
DETECTOR_TFW(r25c12s2) { LABEL("TFW12_2"); signal: r26c3/in8; hv: r26c12/out8;  }
DETECTOR_TFW(r25c13s1) { LABEL("TFW13_1"); signal: r26c4/in1; hv: r26c12/out9;  }
DETECTOR_TFW(r25c13s2) { LABEL("TFW13_2"); signal: r26c4/in2; hv: r26c12/out10; }
DETECTOR_TFW(r25c14s1) { LABEL("TFW14_1"); signal: r26c4/in3; hv: r26c12/out11; }
DETECTOR_TFW(r25c14s2) { LABEL("TFW14_2"); signal: r26c4/in4; hv: r26c12/out12; }
DETECTOR_TFW(r25c15s1) { LABEL("TFW15_1"); signal: r26c4/in5; hv: r26c12/out13; }
DETECTOR_TFW(r25c15s2) { LABEL("TFW15_2"); signal: r26c4/in6; hv: r26c12/out14; }
DETECTOR_TFW(r25c16s1) { LABEL("TFW16_1"); signal: r26c4/in7; hv: r26c12/out15; }
DETECTOR_TFW(r25c16s2) { LABEL("TFW16_2"); signal: r26c4/in8; hv: r26c12/out16; }

DETECTOR_TFW(r25c17s1) { LABEL("TFW17_1"); signal: r26c5/in1; hv: r26c13/out1;  }
DETECTOR_TFW(r25c17s2) { LABEL("TFW17_2"); signal: r26c5/in2; hv: r26c13/out2;  }
DETECTOR_TFW(r25c18s1) { LABEL("TFW18_1"); signal: r26c5/in3; hv: r26c13/out3;  }
DETECTOR_TFW(r25c18s2) { LABEL("TFW18_2"); signal: r26c5/in4; hv: r26c13/out4;  }
DETECTOR_TFW(r25c19s1) { LABEL("TFW19_1"); signal: r26c5/in5; hv: r26c13/out5;  }
DETECTOR_TFW(r25c19s2) { LABEL("TFW19_2"); signal: r26c5/in6; hv: r26c13/out6;  }
DETECTOR_TFW(r25c20s1) { LABEL("TFW20_1"); signal: r26c5/in7; hv: r26c13/out7;  }
DETECTOR_TFW(r25c20s2) { LABEL("TFW20_2"); signal: r26c5/in8; hv: r26c13/out8;  }
DETECTOR_TFW(r25c21s1) { LABEL("TFW21_1"); signal: r26c6/in1; hv: r26c13/out9;  }
DETECTOR_TFW(r25c21s2) { LABEL("TFW21_2"); signal: r26c6/in2; hv: r26c13/out10; }
DETECTOR_TFW(r25c22s1) { LABEL("TFW22_1"); signal: r26c6/in3; hv: r26c13/out11; }
DETECTOR_TFW(r25c22s2) { LABEL("TFW22_2"); signal: r26c6/in4; hv: r26c13/out12; }
DETECTOR_TFW(r25c23s1) { LABEL("TFW23_1"); signal: r26c6/in5; hv: r26c13/out13; }
DETECTOR_TFW(r25c23s2) { LABEL("TFW23_2"); signal: r26c6/in6; hv: r26c13/out14; }
DETECTOR_TFW(r25c24s1) { LABEL("TFW24_1"); signal: r26c6/in7; hv: r26c13/out15; }
DETECTOR_TFW(r25c24s2) { LABEL("TFW24_2"); signal: r26c6/in8; hv: r26c13/out16; }

DETECTOR_TFW(r25c25s1) { LABEL("TFW25_1"); signal: r26c7/in1; hv: r26c14/out1;  }
DETECTOR_TFW(r25c25s2) { LABEL("TFW25_2"); signal: r26c7/in2; hv: r26c14/out2;  }
DETECTOR_TFW(r25c26s1) { LABEL("TFW26_1"); signal: r26c7/in3; hv: r26c14/out3;  }
DETECTOR_TFW(r25c26s2) { LABEL("TFW26_2"); signal: r26c7/in4; hv: r26c14/out4;  }
DETECTOR_TFW(r25c27s1) { LABEL("TFW27_1"); signal: r26c7/in5; hv: r26c14/out5;  }
DETECTOR_TFW(r25c27s2) { LABEL("TFW27_2"); signal: r26c7/in6; hv: r26c14/out6;  }
DETECTOR_TFW(r25c28s1) { LABEL("TFW28_1"); signal: r26c7/in7; hv: r26c14/out7;  }
DETECTOR_TFW(r25c28s2) { LABEL("TFW28_2"); signal: r26c7/in8; hv: r26c14/out8;  }
DETECTOR_TFW(r25c29s1) { LABEL("TFW29_1"); signal: r26c8/in1; hv: r26c14/out9;  }
DETECTOR_TFW(r25c29s2) { LABEL("TFW29_2"); signal: r26c8/in2; hv: r26c14/out10; }
DETECTOR_TFW(r25c30s1) { LABEL("TFW30_1"); signal: r26c8/in3; hv: r26c14/out11; }
DETECTOR_TFW(r25c30s2) { LABEL("TFW30_2"); signal: r26c8/in4; hv: r26c14/out12; }
DETECTOR_TFW(r25c31s1) { LABEL("TFW31_1"); signal: r26c8/in5; hv: r26c14/out13; }
DETECTOR_TFW(r25c31s2) { LABEL("TFW31_2"); signal: r26c8/in6; hv: r26c14/out14; }
DETECTOR_TFW(r25c32s1) { LABEL("TFW32_1"); signal: r26c8/in7; hv: r26c14/out15; }
DETECTOR_TFW(r25c32s2) { LABEL("TFW32_2"); signal: r26c8/in8; hv: r26c14/out16; }

JOINER_8(r26c1)
{
 in1: r25c01s1/signal;
 in2: r25c01s2/signal;
 in3: r25c02s1/signal;
 in4: r25c02s2/signal;
 in5: r25c03s1/signal;
 in6: r25c03s2/signal;
 in7: r25c04s1/signal;
 in8: r25c04s2/signal;

 out1_8: r13c1s21/in1_8;
}

JOINER_8(r26c2)
{
 in1: r25c05s1/signal;
 in2: r25c05s2/signal;
 in3: r25c06s1/signal;
 in4: r25c06s2/signal;
 in5: r25c07s1/signal;
 in6: r25c07s2/signal;
 in7: r25c08s1/signal;
 in8: r25c08s2/signal;

 out1_8: r13c1s21/in9_16;
}

JOINER_8(r26c3)
{
 in1: r25c09s1/signal;
 in2: r25c09s2/signal;
 in3: r25c10s1/signal;
 in4: r25c10s2/signal;
 in5: r25c11s1/signal;
 in6: r25c11s2/signal;
 in7: r25c12s1/signal;
 in8: r25c12s2/signal;

 out1_8: r13c1s20/in1_8;
}

JOINER_8(r26c4)
{
 in1: r25c13s1/signal;
 in2: r25c13s2/signal;
 in3: r25c14s1/signal;
 in4: r25c14s2/signal;
 in5: r25c15s1/signal;
 in6: r25c15s2/signal;
 in7: r25c16s1/signal;
 in8: r25c16s2/signal;

 out1_8: r13c1s20/in9_16;
}

JOINER_8(r26c5)
{
 in1: r25c17s1/signal;
 in2: r25c17s2/signal;
 in3: r25c18s1/signal;
 in4: r25c18s2/signal;
 in5: r25c19s1/signal;
 in6: r25c19s2/signal;
 in7: r25c20s1/signal;
 in8: r25c20s2/signal;

 out1_8: r13c1s16/in1_8;
}

JOINER_8(r26c6)
{
 in1: r25c21s1/signal;
 in2: r25c21s2/signal;
 in3: r25c22s1/signal;
 in4: r25c22s2/signal;
 in5: r25c23s1/signal;
 in6: r25c23s2/signal;
 in7: r25c24s1/signal;
 in8: r25c24s2/signal;

 out1_8: r13c1s16/in9_16;
}

JOINER_8(r26c7)
{
 in1: r25c25s1/signal;
 in2: r25c25s2/signal;
 in3: r25c26s1/signal;
 in4: r25c26s2/signal;
 in5: r25c27s1/signal;
 in6: r25c27s2/signal;
 in7: r25c28s1/signal;
 in8: r25c28s2/signal;

 out1_8: r13c1s15/in1_8;
}

JOINER_8(r26c8)
{
 in1: r25c29s1/signal;
 in2: r25c29s2/signal;
 in3: r25c30s1/signal;
 in4: r25c30s2/signal;
 in5: r25c31s1/signal;
 in6: r25c31s2/signal;
 in7: r25c32s1/signal;
 in8: r25c32s2/signal;

 out1_8: r13c1s15/in9_16;
}

HV_CONN_16(r26c11)
{
 in1_16: r16c2s15/out0_15;

 out1:  r25c01s1/hv;
 out2:  r25c01s2/hv;
 out3:  r25c02s1/hv;
 out4:  r25c02s2/hv;
 out5:  r25c03s1/hv;
 out6:  r25c03s2/hv;
 out7:  r25c04s1/hv;
 out8:  r25c04s2/hv;
 out9:  r25c05s1/hv;
 out10: r25c05s2/hv;
 out11: r25c06s1/hv;
 out12: r25c06s2/hv;
 out13: r25c07s1/hv;
 out14: r25c07s2/hv;
 out15: r25c08s1/hv;
 out16: r25c08s2/hv;
}

HV_CONN_16(r26c12)
{
 in1_16: r10c3s15/out0_15;

 out1:  r25c09s1/hv;
 out2:  r25c09s2/hv;
 out3:  r25c10s1/hv;
 out4:  r25c10s2/hv;
 out5:  r25c11s1/hv;
 out6:  r25c11s2/hv;
 out7:  r25c12s1/hv;
 out8:  r25c12s2/hv;
 out9:  r25c13s1/hv;
 out10: r25c13s2/hv;
 out11: r25c14s1/hv;
 out12: r25c14s2/hv;
 out13: r25c15s1/hv;
 out14: r25c15s2/hv;
 out15: r25c16s1/hv;
 out16: r25c16s2/hv;
}

HV_CONN_16(r26c13)
{
 in1_16: r16c2s11/out0_15;

 out1:  r25c17s1/hv;
 out2:  r25c17s2/hv;
 out3:  r25c18s1/hv;
 out4:  r25c18s2/hv;
 out5:  r25c19s1/hv;
 out6:  r25c19s2/hv;
 out7:  r25c20s1/hv;
 out8:  r25c20s2/hv;
 out9:  r25c21s1/hv;
 out10: r25c21s2/hv;
 out11: r25c22s1/hv;
 out12: r25c22s2/hv;
 out13: r25c23s1/hv;
 out14: r25c23s2/hv;
 out15: r25c24s1/hv;
 out16: r25c24s2/hv;
}

HV_CONN_16(r26c14)
{
 in1_16: r10c3s13/out0_15;

 out1:  r25c25s1/hv;
 out2:  r25c25s2/hv;
 out3:  r25c26s1/hv;
 out4:  r25c26s2/hv;
 out5:  r25c27s1/hv;
 out6:  r25c27s2/hv;
 out7:  r25c28s1/hv;
 out8:  r25c28s2/hv;
 out9:  r25c29s1/hv;
 out10: r25c29s2/hv;
 out11: r25c30s1/hv;
 out12: r25c30s2/hv;
 out13: r25c31s1/hv;
 out14: r25c31s2/hv;
 out15: r25c32s1/hv;
 out16: r25c32s2/hv;
}



/* New TOF wall. */

/* Vertical paddles */

DETECTOR_NTF(rNTFc01s1) { LABEL("NTF01_1"); signal: rNTFRc1/in1; hv: rNTFRc11/out1;  }
DETECTOR_NTF(rNTFc01s2) { LABEL("NTF01_2"); signal: rNTFRc1/in5; hv: rNTFRc11/out9;  }
DETECTOR_NTF(rNTFc02s1) { LABEL("NTF02_1"); signal: rNTFRc2/in3; hv: rNTFRc11/out2;  }
DETECTOR_NTF(rNTFc02s2) { LABEL("NTF02_2"); signal: rNTFRc2/in7; hv: rNTFRc11/out10;  }
DETECTOR_NTF(rNTFc03s1) { LABEL("NTF03_1"); signal: rNTFRc1/in2; hv: rNTFRc11/out3;  }
DETECTOR_NTF(rNTFc03s2) { LABEL("NTF03_2"); signal: rNTFRc1/in6; hv: rNTFRc11/out11;  }
DETECTOR_NTF(rNTFc04s1) { LABEL("NTF04_1"); signal: rNTFRc2/in4; hv: rNTFRc11/out4;  }
DETECTOR_NTF(rNTFc04s2) { LABEL("NTF04_2"); signal: rNTFRc2/in8; hv: rNTFRc11/out12;  }
DETECTOR_NTF(rNTFc05s1) { LABEL("NTF05_1"); signal: rNTFRc1/in3; hv: rNTFRc11/out5;  }
DETECTOR_NTF(rNTFc05s2) { LABEL("NTF05_2"); signal: rNTFRc1/in7; hv: rNTFRc11/out13; }
DETECTOR_NTF(rNTFc06s1) { LABEL("NTF06_1"); signal: rNTFRc2/in1; hv: rNTFRc11/out6; }
DETECTOR_NTF(rNTFc06s2) { LABEL("NTF06_2"); signal: rNTFRc2/in5; hv: rNTFRc11/out14; }
DETECTOR_NTF(rNTFc07s1) { LABEL("NTF07_1"); signal: rNTFRc1/in4; hv: rNTFRc11/out7; }
DETECTOR_NTF(rNTFc07s2) { LABEL("NTF07_2"); signal: rNTFRc1/in8; hv: rNTFRc11/out15; }
DETECTOR_NTF(rNTFc08s1) { LABEL("NTF08_1"); signal: rNTFRc2/in2; hv: rNTFRc11/out8; }
DETECTOR_NTF(rNTFc08s2) { LABEL("NTF08_2"); signal: rNTFRc2/in6; hv: rNTFRc11/out16; }


JOINER_8(rNTFRc1)
{
 in1: rNTFc01s1/signal;
 in2: rNTFc03s1/signal;
 in3: rNTFc05s1/signal;
 in4: rNTFc07s1/signal;
 in5: rNTFc01s2/signal;
 in6: rNTFc03s2/signal;
 in7: rNTFc05s2/signal;
 in8: rNTFc07s2/signal;

// out1_8: r0c1s16/in1_8;
  out1_8: r13c3s20/in1_8;
}

JOINER_8(rNTFRc2)
{
 in1: rNTFc06s1/signal;
 in2: rNTFc08s1/signal;
 in3: rNTFc02s1/signal;
 in4: rNTFc04s1/signal;
 in5: rNTFc06s2/signal;
 in6: rNTFc08s2/signal;
 in7: rNTFc02s2/signal;
 in8: rNTFc04s2/signal;

// out1_8: r0c1s16/in9_16;
 out1_8: r13c3s20/in9_16;
}


HV_CONN_16(rNTFRc11)
{
 in1_16: r10c3s3/out0_15;

 out1:  rNTFc01s1/hv;
 out2:  rNTFc02s1/hv;
 out3:  rNTFc03s1/hv;
 out4:  rNTFc04s1/hv;
 out5:  rNTFc05s1/hv;
 out6:  rNTFc06s1/hv;
 out7:  rNTFc07s1/hv;
 out8:  rNTFc08s1/hv;
 out9:  rNTFc01s2/hv;
 out10: rNTFc02s2/hv;
 out11: rNTFc03s2/hv;
 out12: rNTFc04s2/hv;
 out13: rNTFc05s2/hv;
 out14: rNTFc06s2/hv;
 out15: rNTFc07s2/hv;
 out16: rNTFc08s2/hv;
}

/* Horozontal paddles */

DETECTOR_NTF(rNTFc09s1) { LABEL("NTF09_1"); signal: rNTFRc3/in1; hv: rNTFRc12/out1;  }
DETECTOR_NTF(rNTFc09s2) { LABEL("NTF09_2"); signal: rNTFRc3/in5; hv: rNTFRc12/out9;  }
DETECTOR_NTF(rNTFc10s1) { LABEL("NTF10_1"); signal: rNTFRc4/in3; hv: rNTFRc12/out2;  }
DETECTOR_NTF(rNTFc10s2) { LABEL("NTF10_2"); signal: rNTFRc4/in7; hv: rNTFRc12/out10;  }
DETECTOR_NTF(rNTFc11s1) { LABEL("NTF11_1"); signal: rNTFRc3/in2; hv: rNTFRc12/out3;  }
DETECTOR_NTF(rNTFc11s2) { LABEL("NTF11_2"); signal: rNTFRc3/in6; hv: rNTFRc12/out11;  }
DETECTOR_NTF(rNTFc12s1) { LABEL("NTF12_1"); signal: rNTFRc4/in4; hv: rNTFRc12/out4;  }
DETECTOR_NTF(rNTFc12s2) { LABEL("NTF12_2"); signal: rNTFRc4/in8; hv: rNTFRc12/out12;  }
DETECTOR_NTF(rNTFc13s1) { LABEL("NTF13_1"); signal: rNTFRc3/in3; hv: rNTFRc12/out5;  }
DETECTOR_NTF(rNTFc13s2) { LABEL("NTF13_2"); signal: rNTFRc3/in7; hv: rNTFRc12/out13; }
DETECTOR_NTF(rNTFc14s1) { LABEL("NTF14_1"); signal: rNTFRc4/in1; hv: rNTFRc12/out6; }
DETECTOR_NTF(rNTFc14s2) { LABEL("NTF14_2"); signal: rNTFRc4/in5; hv: rNTFRc12/out14; }
DETECTOR_NTF(rNTFc15s1) { LABEL("NTF15_1"); signal: rNTFRc3/in4; hv: rNTFRc12/out7; }
DETECTOR_NTF(rNTFc15s2) { LABEL("NTF15_2"); signal: rNTFRc3/in8; hv: rNTFRc12/out15; }
DETECTOR_NTF(rNTFc16s1) { LABEL("NTF16_1"); signal: rNTFRc4/in2; hv: rNTFRc12/out8; }
DETECTOR_NTF(rNTFc16s2) { LABEL("NTF16_2"); signal: rNTFRc4/in6; hv: rNTFRc12/out16; }

JOINER_8(rNTFRc3)
{
 in1: rNTFc09s1/signal;
 in2: rNTFc11s1/signal;
 in3: rNTFc13s1/signal;
 in4: rNTFc15s1/signal;
 in5: rNTFc09s2/signal;
 in6: rNTFc11s2/signal;
 in7: rNTFc13s2/signal;
 in8: rNTFc15s2/signal;

// out1_8: r0c1s22/in1_8;
 out1_8: r13c3s19/in1_8;
}

JOINER_8(rNTFRc4)
{
 in1: rNTFc14s1/signal;
 in2: rNTFc16s1/signal;
 in3: rNTFc10s1/signal;
 in4: rNTFc12s1/signal;
 in5: rNTFc14s2/signal;
 in6: rNTFc16s2/signal;
 in7: rNTFc10s2/signal;
 in8: rNTFc12s2/signal;

// out1_8: r0c1s22/in9_16;
 out1_8: r13c3s19/in9_16;
}


HV_CONN_16(rNTFRc12)
{
 in1_16: r10c3s4/out0_15;

 out1:  rNTFc09s1/hv;
 out2:  rNTFc10s1/hv;
 out3:  rNTFc11s1/hv;
 out4:  rNTFc12s1/hv;
 out5:  rNTFc13s1/hv;
 out6:  rNTFc14s1/hv;
 out7:  rNTFc15s1/hv;
 out8:  rNTFc16s1/hv;
 out9:  rNTFc09s2/hv;
 out10: rNTFc10s2/hv;
 out11: rNTFc11s2/hv;
 out12: rNTFc12s2/hv;
 out13: rNTFc13s2/hv;
 out14: rNTFc14s2/hv;
 out15: rNTFc15s2/hv;
 out16: rNTFc16s2/hv;
}

/*
 *
 * DTF
 *
 */ 

DETECTOR_DTF(rDTFc01s1) { LABEL("DTF01_1"); signal: rDTFRc1/in1;  }
DETECTOR_DTF(rDTFc01s2) { LABEL("DTF01_2"); signal: rDTFRc1/in2;  }
DETECTOR_DTF(rDTFc02s1) { LABEL("DTF02_1"); signal: rDTFRc1/in3;  }
DETECTOR_DTF(rDTFc02s2) { LABEL("DTF02_2"); signal: rDTFRc1/in4;   }
DETECTOR_DTF(rDTFc03s1) { LABEL("DTF03_1"); signal: rDTFRc1/in5;  }
DETECTOR_DTF(rDTFc03s2) { LABEL("DTF03_2"); signal: rDTFRc1/in6;   }
DETECTOR_DTF(rDTFc04s1) { LABEL("DTF04_1"); signal: rDTFRc1/in7;  }
DETECTOR_DTF(rDTFc04s2) { LABEL("DTF04_2"); signal: rDTFRc1/in8;   }
DETECTOR_DTF(rDTFc05s1) { LABEL("DTF05_1"); signal: rDTFRc2/in1;  }
DETECTOR_DTF(rDTFc05s2) { LABEL("DTF05_2"); signal: rDTFRc2/in2;  }
DETECTOR_DTF(rDTFc06s1) { LABEL("DTF06_1"); signal: rDTFRc2/in3; }
DETECTOR_DTF(rDTFc06s2) { LABEL("DTF06_2"); signal: rDTFRc2/in4;  }
DETECTOR_DTF(rDTFc07s1) { LABEL("DTF07_1"); signal: rDTFRc2/in5; }
DETECTOR_DTF(rDTFc07s2) { LABEL("DTF07_2"); signal: rDTFRc2/in6;  }
DETECTOR_DTF(rDTFc08s1) { LABEL("DTF08_1"); signal: rDTFRc2/in7; }
DETECTOR_DTF(rDTFc08s2) { LABEL("DTF08_2"); signal: rDTFRc2/in8;  }

JOINER_8(rDTFRc1)
{
 in1: rDTFc01s1/signal;
 in2: rDTFc01s2/signal;
 in3: rDTFc02s1/signal;
 in4: rDTFc02s2/signal;
 in5: rDTFc03s1/signal;
 in6: rDTFc03s2/signal;
 in7: rDTFc04s1/signal;
 in8: rDTFc04s2/signal;

 out1_8: r13c3s21/in1_8;
}

JOINER_8(rDTFRc2)
{
 in1: rDTFc05s1/signal;
 in2: rDTFc05s2/signal;
 in3: rDTFc06s1/signal;
 in4: rDTFc06s2/signal;
 in5: rDTFc07s1/signal;
 in6: rDTFc07s2/signal;
 in7: rDTFc08s1/signal;
 in8: rDTFc08s2/signal;

 out1_8: r13c3s21/in9_16;
}



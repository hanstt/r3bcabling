/*
 * File used for RPC Test 
 */
#if 0
#include "modules/modules.hh"
#include "rpc2006.hh"

//-----DETECTORS----------------//

TRIGGER_SCI(rSCI1c1s1) { ; }

TRIGGER_SCI_U(rSCI1c1s1u1) 
{ 
LABEL("SCIA"); 
signal: r0c2s21u1/in; 
hv: r0c2s23u1/hv; 
}

TRIGGER_SCI_U(rSCI1c1s1u2) 
{ 
LABEL("SCIB"); 
signal: r0c2s21u5/in; 
hv: r0c2s23u2/hv; 
}

TRIGGER_SCI(rSCI1c1s2) { ; }

TRIGGER_SCI_U(rSCI1c1s2u1) 
{ 
LABEL("SCI4");
signal: r0c2s21u3/in; 
//hv: r0c5s0/hv; 
}

/*
DETECTOR_RPC_U(rDMY1c1s1u1) { LABEL("DMY01_01"); signal: r4c7s21/in13; }
DETECTOR_RPC_U(rDMY2c1s1u1) { LABEL("DMY01_02"); signal: r4c7s21/in14; }
DETECTOR_RPC_U(rDMY3c1s1u1) { LABEL("DMY01_03"); signal: r4c7s21/in15; }
DETECTOR_RPC_U(rDMY4c1s1u1) { LABEL("DMY01_04"); signal: r4c7s21/in16; }
*/

//-------VME CRATE----------------------//



VME_CRATE(r0c1)
{
  ;
}


RIO3(r0c1s1)
{ //R3-31
  SETTING("HOSTNAME" => "R3-31");

}



TRIVA3(r0c1s3)
{
 in1: r0c2s17u1/out_ecl0 ;//watch +/- of output -> will inverst signal
 out1: r0c2s17u1/in_ecl7 ;//watch +/- of output -> will inverst signal
}

CAEN_V775(r0c1s6)
{
  //CAEN TDC;
  // Adress: (top) 0 4 0 0;
  SETTING("ADDRESS" => "0x00400000");
  in0_7: r0c2s17u2/out_ecl0_7 ;
 com_in: r0c2s15u1/out1;
}

CAEN_V792(r0c1s8)
{
  //CAEN QDC;
  // Adress: (top) 0 5 0 0;
  SETTING("ADDRESS" => "0x00500000");
 in0: "1" <- "1", r0c2s7/out2;
 in1:  r0c2s7/out5;
 in2: "" <- "2", r0c2s7/out8;
 gate_in: "7" <- "",  r0c2s13u5/out2;
}

CAEN_V830(r0c1s10)
{
  LABEL("SCALER");
  //CAEN Scaler;
  // Adress: (top) 0 6 0 0;
  SETTING("ADDRESS" => "0x00600000");
 in0_7: r0c2s3/out_ecl1_8 ;
 
 trig_in: r0c2s15u1/out2 ;
 
}

CAEN_V785(r0c1s13)
{
  //CAEN ADC;
  // Adress: (top) 1 5 0 0;
  SETTING("ADDRESS" => "0x00510000");
 gate_in: r0c2s15u1/out3;
}





//---------NIM CRATE-------------------------------//

NIM_CRATE(r0c2)
{
  ;
}



CLOCK_MSE(r0c2s1)
{
  ;
}



EC1601(r0c2s3)
{
//ECL-NIM-ECL converter #7077

in1_5: r0c2s17u2/outB0_4; //translator
out_ecl1_8: r0c1s10/in0_7;
}

FL8000(r0c2s5)
{
//octal fast linear amp  
  ;
}

FL8000_U(r0c2s5u1)
{
in: r0c2s21u1/out_e;
out: r0c2s7/in1;
}

FL8000_U(r0c2s5u2)
{
in: r0c2s21u5/out_e;
out: r0c2s7/in4;
}

FL8000_U(r0c2s5u3)
{
in: r0c2s21u3/out_e;
out: r0c2s7/in7;
}




DP1600(r0c2s7)
{
  //passive delay;
  SETTING("DELAY" => "340 ns");
 in1: r0c2s5u1/out;
 in2: r0c2s7/out1;
 in4: r0c2s5u2/out;
 in5: r0c2s7/out4;
 in7: r0c2s5u3/out;
 in8: r0c2s7/out7;

 out1: r0c2s7/in2; //"1" -> "1", ;
 out2: r0c1s8/in0; //"2" -> "", ;
 out4: r0c2s7/in5;
 out5: r0c1s8/in1;
 out7: r0c2s7/in8;
 out8: r0c1s8/in2;


}


DV8000(r0c2s9)
{
//variable delay
  ;
}


DV8000_U(r0c2s9u1)
{
in: r0c2s21u1/outB2;
out1: r0c2s9u2/in;
}


DV8000_U(r0c2s9u2)
{
in: r0c2s9u1/out1;
out1: r0c2s17u2/in0;
}

DV8000_U(r0c2s9u3)
{
in: r0c2s21u5/outB2;
out1: r0c2s9u4/in;
}

DV8000_U(r0c2s9u4)
{
in: r0c2s9u3/out1;
out1: r0c2s17u2/in1;
}

DV8000_U(r0c2s9u6)
{
in: r0c2s21u3/outB1;
out1: r0c2s9u7/in;
}

DV8000_U(r0c2s9u7)
{
in: r0c2s9u6/out1;
out1: r0c2s9u8/in;
}

DV8000_U(r0c2s9u8)
{
in: r0c2s9u7/out1;
out1: r0c2s17u2/in2;
}

VARIABLE_DELAY(r0c2s11)
{
  //Passive delayFE290 #1396;
  in: r0c2s17u1/outB0;
  out: r0c2s17u2/in3;
}


GG8000(r0c2s13)
{
//gate generator # 3593
  ;
}

GG8000_U(r0c2s13u5)
{
//gate generator # 3593
in: r0c2s17u1/outA7;
 out2: r0c1s8/gate_in;
}

LF4000(r0c2s15)
{
//Logic fan in-fan out#3665;
  ;
}



LF4000_8_U(r0c2s15u1)
{
 in1: r0c2s17u1/outB7;

 out1: r0c1s6/com_in;
 out2: r0c1s10/trig_in;
 out3: r0c1s13/gate_in;
 out4: r0c2s17u2/in4;
}


N638 (r0c2s17)  

{
  LABEL("test");
//translator
;
}

N638_U (r0c2s17u1)  
{
in0: r0c2s19u1/out3; //from coincidence
in_ecl7:  r0c1s3/out1; //trigger;  watch +/- of output -> will inverst signal
outB0: r0c2s11/in;  //delay
outA7: r0c2s13u5/in;    //octal gate generator
outB7: r0c2s15u1/in1;   // fifo
out_ecl0: r0c1s3/in1; //trigger;  watch +/- of output -> will inverst signal
}

N638_U (r0c2s17u2)  
{
in0: r0c2s9u2/out1; //octal variable  delay
in1: r0c2s9u4/out1; //octal variable delay
in2: r0c2s9u8/out1; //octal variable delay
in3: r0c2s11/out;   //delay
in4: r0c2s15u1/out4;   //fifo
out_ecl0_7: r0c1s6/in0_7;   // tdc
 outB0_4: r0c2s3/in1_5; //nim_ecl_nim
}


CO4001(r0c2s19)
{
//Logic box# 3391 ;
  ;
}


CO4001_U(r0c2s19u1)
{
 inA: r0c2s21u3/outB2;
 inB: r0c2s21u1/outB1;

 out3: r0c2s17u1/in0;
}





CF8000(r0c2s21)
{
//CFD
  ;
}
 
CF8000_U(r0c2s21u1)
{
in:rSCI1c1s1u1/signal; //signal from detector A
outB1: r0c2s19u1/inB; //coincidence
outB2: r0c2s9u1/in; //delay
out_e: r0c2s5u1/in; //amplifier
}

CF8000_U(r0c2s21u3)
{

in:rSCI1c1s2u1/signal; //signal from detector
outB1: r0c2s9u6/in; //delay
 outB2: r0c2s19u1/inA;
out_e: r0c2s5u3/in; //amplifier
}

CF8000_U(r0c2s21u5)
{
in:rSCI1c1s1u2/signal; //signal from detector
outB2: r0c2s9u3/in; //delay
out_e: r0c2s5u2/in; //amplifier
}



HV_NHQ203Ms(r0c2s23)
{
  ;
}

HV_NHQ203Ms_U(r0c2s23u1)
{
 hv: rSCI1c1s1u1/hv;

}

HV_NHQ203Ms_U(r0c2s23u2)
{

 hv:rSCI1c1s1u2/hv;
}

#endif





// // Messhuette Rack 1, Crate 1 (just an empty NIM frame), implented feb07
// // 
// NIM_CRATE(r0c1)
// {
// 	;
// }

// /*SPLIT_BOX_R(r1c1s2) // name: PSD 1600
// {
// in1_8:  rNTFRc1/out1_8;
// in9_16: rNTFRc2/out1_8;

// e1_16:  "NTF" -> , .c4s13/in0_15; //to VME QDC

// t1_8:   "NTF, cable 1" -> , r1c2s7/in1_8; //to CFD8104
// t9_16:  "NTF, cable 2" -> , r1c2s8/in1_8; //to CFD8104
// }*/
// /*

// SPLIT_BOX_R(r1c1s5) // name: PSD 1600
// {
// in1_8:  rNTFRc3/out1_8;
// in9_16: rNTFRc4/out1_8;
				
// e1_16:  "NTF" -> , .c4s13/in16_31; //to VME QDC
				
// t1_8:   "NTF, cable 3" -> , r1c2s9/in1_8; //to CFD8104
// t9_16:  "NTF, cable 4" -> , r1c2s10/in1_8; //to CFD8104
// }
// */

// // CHANGED BY CL @ 12/08/2010 
// // REASON: NTF IS NOW SPLIT WITH TACQUILA AND THE CONNECTORS ARE INSIDE WITH THE OLD
// // FASTBUS ELECTRONICS..


// SPLIT_BOX_R(r0c1s16) // name: PSD 1600
// {
//   ;
// //in1_8:  rNTFRc1/out1_8;
// //in9_16: rNTFRc2/out1_8;

// //e1_16:  "NTF" -> , r1c4s13/in0_15; //to VME QDC

// //t1_8:   "NTF, cable 1" -> , r1c0s9/in1_8; //to CFD8104
// //t9_16:  "NTF, cable 2" -> , r1c0s11/in1_8; //to CFD8104
// }

// /*
// CAPACITOR_BOX(r1c1s)
// {
// in1_8: .s9/e1_8;
// }
// */
  
// SPLIT_BOX_R(r0c1s22) // name: PSD 1600
// {
//   ;
// //in1_8:  rNTFRc3/out1_8;
// //in9_16: rNTFRc4/out1_8;
				
// //e1_16:  "NTF" -> , r1c4s13/in16_31; //to VME QDC
				
// //t1_8:   "NTF, cable 3" -> , r1c0s13/in1_8; //to CFD8104
// //t9_16:  "NTF, cable 4" -> , r1c0s15/in1_8; //to CFD8104
// }








//Checked 30/05/2007 by shift D


// Messhuette Rack 5, Terminal Crates 1-8, implented mar07
// POS, ROLU, GFI, PSP,...

//copied from pos.hh
/* Signals from Cave C.
 */
TERMINAL(r5c1)
{
  // LABEL("POS 1 and ROLU 1");
  
 in1_8: <- "KABEL 4 (25-32)" , rBEAMR1c2/out1_8;

  /* Energies to digitalisation. */
  
 e1_8:  r4c3s6(QDC)/in0_7;

  /* Times. */

  /* POS01 */
 t1:  r5c1s2/in1;//r4c8s11u1/in;
 t2:  r5c1s2/in2;
 t3:  r5c1s2/in3;//r4c8s11u1/in;
 t4:  r5c1s2/in4;

  /* ROLU 1 */
 t5_8:  r5c1s2(CAPACITOR_BOX)/in5_8;
}

CAPACITOR_BOX(r5c1s2)
{
  /* POS01 */
 in1: r5c1/t1;
 in2: r5c1/t2;
 in3: r5c1/t3;
 in4: r5c1/t4;
 
 out1: r4c8s7u1/in;
 out2: r4c8s7u2/in; //broken unit 2 
 out3: r4c8s7u3/in; 
 out4: r4c8s7u4/in;

  /* ROLU 1 */

 in5_8: r5c1/t5_8;

 out5:  "5" ->       , r4c8s23u5(CFD8000)/in;
 out6:      -> "2"   , r4c8s23u6(CFD8000)/in;
 out7:                 r4c8s23u7(CFD8000)/in;
 out8:                 r4c8s23u8(CFD8000)/in;
}

TERMINAL(r5c2)
{
 // LABEL("SCI 1 and 2 and ROLU 2");
  
 //in1:   "1"  <- "1"  , rSCI1c1s1u1/signal;
 //in2:   "2"  <- "1"  , rSCI1c1s1u2/signal;
 //in3:   "13" <- "13" , rSCI2c1s1u1/signal;
 //in4:   "14" <- "14" , rSCI2c1s1u2/signal; 

 //in5_8: <- "KABEL 1 (1-4)" , rBEAMR1c2/out17_20;

  /* Energies to digitalisation. */
 //e1_4:  r4c3s14/in0_3; 
 //e5_8:  r4c3s14/in4_7;

  /* Times. */

  /* SCI 1 & 2 */
 //t1_4:  r4c9s21u2_5/in;

  /* ROLU 2 */
 //t5_8:  r5c2s2(CAPACITOR_BOX)/in5_8;
;}

CAPACITOR_BOX(r5c2s2)
{
  /* POS01 */



  /* ROLU 2 */
  
  //in5_8: r5c2/t5_8;

  //out5:                 r4c9s23u5(CFD8000)/in;
  //out6:                 r4c9s23u6(CFD8000)/in;
  //out7:                 r4c9s23u7(CFD8000)/in;
  //out8:                 r4c9s23u8(CFD8000)/in;
;}
//copied from pos.hh

//copied from other.hh
TERMINAL(r5c3)
{
/*
 in1: "A4"   <- "KABEL 7 (56)", rBEAMR1c1/out8;
 in2: "KABEL 9 (8)"    <-     , rBEAMR1c1/out16;
 in3: "B2"   <- "KABEL 5 (40)", rBEAMR1c1/out24;
*/
 in1: "KABEL1 (1)"   <- "KABEL1 (1)", rBEAMR1c1/out8;
 in2: "KABEL1 (2)"   <- "KABEL1 (2)", rBEAMR1c1/out16;
 in3: "KABEL1 (3)"   <- "KABEL1 (3)", rBEAMR1c1/out24;

  /* STR (pixel) */

 e1:   r4c3s23/in1;
 e2:   r4c3s23/in0;
 e3:   r4c3s23/in2;
 e4:   r4c3s23/in3;

 t1_2:   r5c3s2/in1_2;
}

CAPACITOR_BOX(r5c3s2)
{
 in1_2:    r5c3/t1_2;
 // in3_6:    r4c11/out9_12;

 // out1:     r4c8s11u6(CFD8000)/in;
 out1:     r4c8s9u7/in;
 out2:     r4c8s11u5/in;
 //out3:     r4c8s9u7/in;
 // out3_6:   r4c3s13/in1_4;

 in8:      r5c4/t2;
 out8:     r4c8s23u2/in;
}
//copied from other.hh

//copied from gfi_time.hh
TERMINAL(r5c4)
{
  /* GFI time */

// TODO: unhack this!
  //in1: r2c11/front41;//rGFI1c1s1u35/signal; // connected to GFI time 1 kabel B5!! 
  //in2: r2c11/front42;//rGFI2c1s1u35/signal; // connected to GFI time 2
 //in3: r2c11/out43;//; // connected to GFI time 3

 e1_3:   r4c3s23/in4_6; //cabels marked 5_7

 t1:     r4c8s23u1/in;
 t2:     r5c3s2/in8;
 t3:     r4c8s23u3/in;
}
//copied from gfi_time.hh

//copied from other.hh
CAPACITOR_BOX(r5c4s2)
{
  /*in1_3:    r5c4/t1_3;*/
 /*
 out1:     r4c8s23u1(CFD8000)/in;
 out2:     r4c8s23u2(CFD8000)/in;
 out3:     r4c8s23u3(CFD8000)/in;
 */
  ;
}
//copied from other.hh

//copied from pos.hh
/* Delay before DAQ. */
CABLE_DELAY_8(r5c5)
{
  //LABEL("POS 1 and 2");
  SETTING("DELAY" => "400ns");
	// front:
 in1:  "1" <- "1" , r4c8s7u1/outA;
 in2:  "2" <- "2" , r4c8s7u2/outA; 
 in3:  "3" <- "3" , r4c8s7u3/outA;
 in4:  "4" <- "4" , r4c8s7u4/outA; 
 out5:	r4c7s23/out14;
 out6:	r4c7s23/out16;
 out7:	r4c7s23/out15;
 out8:	;
	// back:
   out1_4:            r4c7s21/in1_4; //former r4c7s23/
  // in5:               r4c7s23/out14;
  //in6:               r4c7s23/out16;
  // in7:               r4c7s23/out15;
 in8:	;
}
//copied from pos.hh

//copied from other.hh
CABLE_DELAY_8(r5c6)
{
//LABEL("PIX");
SETTING("DELAY" => "400ns");

//in1:  "1" <- "1" , r4c8s11u6(CFD8000)/outA;
in1:  "1" <- "1" , r4c8s9u7(CFD8000)/outA;
in2:  "2" <- "2" , r4c8s11u5(CFD8000)/outA;
//in3:  "3" <- "3" , r4c8s9u7(CFD8000)/outA;

			// back:
out1:    r4c7s17u1/in;
out2:    r4c7s17u3/in;
out3:    r4c7s17u5/in;


in5:  "5" <- "5" , r4c8s23u1/outA;;
in6:  "6" <- "6" , r4c8s23u2/outA;;
in7:  "7" <- "7" , r4c8s23u3/outA;;

			// back:
out5: r3c2/in3;
out6: r4c7s23/in2;
//out7: r4c3s17/stop6;
}
//copied from other.hh

//copied from pos.hh
/*
TERMINAL(r5c7)
{
 // Time for RPC 

 in1:   "3"  <- "3"  , rRPCRc1s1/t1;
 in2:   "4"  <- "4"  , rRPCRc1s1/t2;
 in3:   "5"  <- "5"  , rRPCRc1s1/t3;
 in4:   "6"  <- "6"  , rRPCRc1s1/t4;
 in5:   "15" <- "15" , rRPCLc1s1/t1;
 in6:   "16" <- "16" , rRPCLc1s1/t2;
 in7:   "17" <- "17" , rRPCLc1s1/t3;
 in8:   "18" <- "18" , rRPCLc1s1/t4;

 t1_2: r4c7s21/in1_2;
 t3_8: r4c7s21/in4_9; 
}
*/

TERMINAL(r5c8)
{
  // LABEL("RPC");

 // Energies for RPC to ADC

/* in1:   "7"  <- "7"  , rRPCRc1s1/e1;
 in2:   "8"  <- "8"  , rRPCRc1s1/e2;
 in3:   "9"  <- "9"  , rRPCRc1s1/e3;
 in4:   "10" <- "10" , rRPCRc1s1/e4;
 in5:   "19" <- "19" , rRPCLc1s1/e1;
 in6:   "20" <- "20" , rRPCLc1s1/e2;
 in7:   "21" <- "21" , rRPCLc1s1/e3;
 in8:   "22" <- "22" , rRPCLc1s1/e4;

 e1_8:  r4c3s15/in0_7;
*/
	;
}
//copied from pos.hh

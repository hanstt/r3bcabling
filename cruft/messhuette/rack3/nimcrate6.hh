// Messhuette Rack 3, NimCrate 6, implented mar07, modified 20070531 (shift A)
// Containing BoS, EoS logics 
NIM_CRATE(r3c6)
{
	;
}

//copied from trigger.hh
/* Spill on / Spill off and
 * BoS / EoS mimic
 */
LF4000(r3c6s1)
{
  SERIAL("3614");
}

LF4000_4_U(r3c6s1u1)
{
 in1:     .s5u2/out1;
 //out1:    .s3u1/start;
 //out2:    .s3u2/reset;
 out3: r2c11/front47;
 //out4: r2c2s11u2/start;
}

LF4000_4_U(r3c6s1u2)
{
 in1:  .s5u1/end_marker;
 out1: .s3u1/start;
 out2: .s3u2/reset;
}

LF4000_4_U(r3c6s1u4)
{
 in1: .s5u2/end_marker;
 out1: .s3u2/start;
 out2: .s3u1/reset;
}

CAEN2255B(r3c6s3)
{
  SERIAL("686");
}

CAEN2255B_U(r3c6s3u1)
{
  LABEL("SPILL ON");

 start:   .s1u2/out1;
 reset:   .s1u4/out2;

  // out1:    r2c2s1/in16;
}

CAEN2255B_U(r3c6s3u2)
{
  LABEL("SPILL OFF");


 start:   .s1u4/out1;
 reset:   .s1u2/out2;

  //out1:    r2c1s19u2/inB;
}

CAEN2255B(r3c6s5)
{
  ;
}

CAEN2255B_U(r3c6s5u1)
{
  LABEL("BOS");
 start:        .s7u1/out_nim;

 end_marker:    .s1u2/in1;
 out1:          r2c11/front46;
  //out2:          r2c2s11u1/start;
}

CAEN2255B_U(r3c6s5u2)
{
  LABEL("EOS");

 start:        .s7u2/out_nim;

 end_marker:   .s1u4/in1;
 out1:         .s1u1/in1;
}

LA8000(r3c6s7)
{
  ;
}

LA8000_U(r3c6s7u1)
{
 in_nim:  r1c2s2/o4; // spill simulation //clock .s19u1/nim_out;
 // in_ttl:  .s9u1/out;

 out_nim: .s5u1/start;
}

LA8000_U(r3c6s7u2)
{
 in_nim:  r1c2s2/o2; // spill simulation //clock .s19u2/nim_out;
 //in_ttl:  .s9u2/out;

 out_nim: .s5u2/start;
}

LA8000_U(r3c6s7u3)
{
 in_ttl:  ;//r2c11/out28;
 out_nim: ;//r2c3s1/in5;
}

LA8000_U(r3c6s7u4)
{
 in_ttl:  r3c5s7/out_ttl_1MHz;
 //out_nim: r6c1s1u1/in1;
}

LA8000_U(r3c6s7u5)
{
  //in_ttl:  r2c11/front25;
 out_nim: r2c3s1/in6;
}

LA8000_U(r3c6s7u6)
{
 in_nim: r2c2s23u2/out2 ;//r4c9s3/out5;
 out_ttl: ;//goes to the speaker;
}

LA8000_U(r3c6s7u7)
{
  // in_ttl:    .s9u3/out;
 out_nim:   r2c3s1/in7;
}

LA8000_U(r3c6s7u8)
{
 in_nim:  r4c8s21u1/out1;
 // out_ttl: r2c11/front1; // TO HKR
 out_nim: r2c11/front1;
}

//////////////////////continue here 17:35 2010/08/01

//TO4000(r3c6s9)
//{
//  SERIAL("2215");
//}

//TO4000_U(r3c6s9u1)
//{
// in:   r2c11/front3; // patch
// out:  .s7u1/in_ttl;
//}

//TO4000_U(r3c6s9u2)
//{
// in:  r2c11/front4; // patch
// out: .s7u2/in_ttl;
//}

//TO4000_U(r3c6s9u3)
//{
// in:  .c7s1u2/out_ttl; // patch
// out: .s7u7/in_ttl;
 //}
//copied from trigger.hh

//copied from gate.hh
EC8000(r3c6s15)
{
 in_ecl7:  r4c3s23/gate_bus;
 in1:  r3c1s2/out;//.s15/out1;
 //in3:  r3c5s11u4/out4;
 in4:  r3c1s3/out;
 // in5:  .s17u3/out2;
 // in6:  r3c2/out28;
 // in7:  r2c1s17/out2;
 // in8:  .s5u3/out4;
 // in9:  ;
 // in10: r3c2/out30;
 // in11: .s15u1/out1;
 // in12: r3c2/out26;
 // in13: .s13u7/out2;
 // in14: r3c2/out31;
 // in15: ;
 // in16: ;

 out_ecl1:  r4c3s19/start;
 out_ecl4:  r4c3s11/start;
 
 out4:  r4c3s17/common_start;
 //out14: r4c3s17/common_start;
}

CO4000(r3c6s17)
{
	;
}

CO4000_U(r3c6s17u1)
{
inA: r3c5s15u1/out11;

out2: r3c5s23/in1;
out3: r3c5s23/in4;
}
CO4000_U(r3c6s17u2)
{
 inB:r3c2/out26;
 inA: r2c1s23u5/out1;
 out1: r3c5s5u2/in1;
}
CO4000_U(r3c6s17u3)
{
  // A and B
  inB: r3c7s19u5/out2;
  inA: r2c4s3u3/out4;
  
  out2: .s21u4/in2;
}

//copied from gate.hh

LF4000(r3c6s21)
{
  SERIAL("7310");
}

LF4000_4_U(r3c6s21u1)
{
 in1:  .c5s19u4/out2;
 in2: r3c5s15u1/out15;
 out1: .c5s23/in3;
}
LF4000_4_U(r3c6s21u3)
{
  in1:  .c5s5u2/out4;

  out3: .s21u4/in1;
  out4: .c7s19u5/in;
}

LF4000_4_U(r3c6s21u4)
{
  in1:  .s21u3/out3;
  in2:  .s17u3/out2;

  out4: r2c4s9/in1;
}

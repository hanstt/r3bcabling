/*
 * Camac crate 0 in messcontainer.  H�tte West.
 *
 */

/***
 * The following modules were moved from camac2 in 
 * the same rack, and concern the NTF detector.
 * 2-aug-2007 TLB
 */

CAMAC_CRATE(r1c0)
{
  ;
}
TO4000(r1c0s5)
{
  ;
}

TO4000_U(r1c0s5u1){
 in:  r2c11/front3;
 out: r1c0s7/in_ttl1;
}

TO4000_U(r1c0s5u2){
 in:  r2c11/front4;
 out: r1c0s7/in_ttl3;
}

PEGEL(r1c0s7)
{  

 in_ttl1: r1c0s5u1/out;
 out_nim1: r1c0s13u1/in1;

 in_ttl3: r1c0s5u2/out;
 out_nim3: r1c0s13u2/in1;
}


//we use a CF8103, while CF8104 is plugged in (4 following modules)
//CF8103(r1c0s9)
//{
  // SERIAL("LCF4006");

//in1_8:   r0c1s16/t1_8;
//tb1_8:   "Eins" -> , .s19/in1_8;
//th1_8:	.c4s15/in0_7; //the VME scalers in the new VME crate

//m:       r2c3s17/in1;
 //sum:     ;
 //or:      ;
  //test:   .s17/out13 ;
//mux_tb:  ;
//}

LRS222(r1c0s9)
{
;
}

LRS222_U(r1c0s9u1){

  start: r1c0s13u1/out1;
  out: r1c3s6/in1; 

}

LRS222_U(r1c0s9u2){

  start: r1c0s13u2/out1;
  out:r1c3s6/in2;
}


CAENN93B(r1c0s11)
{
;
}

CAENN93B_U(r1c0s11u1)
{
 start:  r1c0s13u1/out2;
 reset:  r1c0s13u2/out3;
}

CAENN93B_U(r1c0s11u2)
{
 start:  r1c0s13u2/out2;
 reset:  r1c0s13u1/out3;
}

LF4000(r1c0s13)
{
;
}

LF4000_4_U(r1c0s13u1)
{
 in1: r1c0s7/out_nim1;
 out1: r1c0s9u1/start;
 out2: r1c0s11u1/start;
 out3: r1c0s11u2/reset;
}

LF4000_4_U(r1c0s13u2)
{
 in1: r1c0s7/out_nim3;
 out1: r1c0s9u2/start;
 out2: r1c0s11u2/start;
 out3: r1c0s11u1/reset;
}

//CF8103(r1c0s11)
//{


  // SERIAL("LCF4006");

//in1_8:   r0c1s16/t9_16;
//tb1_8:   "Zwei" -> , .s19/in9_16;
//th1_8:		.c4s15/in8_15;// the VME scalers in the new VME crate

//m:       r2c3s17/in2;
 //sum:     ;
 //or:      ;
//test:   .s17/out14 ;
//mux_tb:  ;
//}

//CF8103(r1c0s13)
//{
  // SERIAL("LCF4006");

//in1_8:   r0c1s22/t1_8;
//tb1_8:   "Drei" -> , .s21/in1_8;
//th1_8:		.c4s15/in16_23; //the VME scalers in the new VME crate

//m:       r2c3s17/in3;
 //sum:     ;
 //or:      ;
//test:    .s17/out15;
 //mux_tb:  ;
//}

//CF8103(r1c0s15)
//{
  // SERIAL("LCF4006");

//in1_8:   r0c1s22/t9_16;
//tb1_8:   "Vier" -> , .s21/in9_16;
//th1_8:		.c4s15/in24_31; //the VME scalers in the new VME crate

//m:       r2c3s17/in4;
 //sum:     ;
 //or:      ;
//test:   .s17/out16 ;
//mux_tb:  ;
//}

// LIFO(r1c0s17)
// {
// in:    r3c5s5u2/out5;

// out13:    r1c0s9/test;
// out14:    r1c0s11/test;
// out15:    r1c0s13/test;
// out16:    r1c0s15/test;

// }

// // NTF Delays

// DL1610(r1c0s19)
// {
// in1_8:  <- "Eins"  , .c0s9/tb1_8;
// in9_16: <- "Zwei"  , .c0s11/tb1_8;

// //outa1_16:  .c4s10/in0_15; // to VME TDC
// }


// DL1610(r1c0s21)
// {
// in1_8:  <- "Drei"  , .c0s13/tb1_8;
// in9_16: <- "Vier"  , .c0s15/tb1_8;

// //outa1_16:	.c4s10/in16_31; // to VME TDC
// }

// CVC4(r1c0s24)
// {
//   SETTING("HOSTNAME"=>"CVC12");
//   SETTING("CRATE_NO"=>"0");
// }

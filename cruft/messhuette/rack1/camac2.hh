/*
 * Camac crate 2 in messcontainer.  H�tte West.
 *
 */

CAMAC_CRATE(r1c2)
{
  ;
}


IOL_BOX(r1c2s2)
{
  CONTROL("VARNAME","iol_box_2_2");
	//TODO: fix the documentation
	o4:	r3c6s7u1/in_nim; // simulate beginning of spill
	o2:	r3c6s7u2/in_nim; // simulate end of spill
	p2:	r2c4s3u2/in1;	// for random tcal
	p4:	r4c3s13/clear; // PSP clear
}

PHILLIPS7120(r1c2s4)
{
  CONTROL("VARNAME","time_calibrator_2_4");
  
 start: r2c2s19u4/in1;
 stop:  r3c2/in26;
  // gate:	r2c6s3/in4;
}

LECROY2277(r1c2s6)
{
  CONTROL("VARNAME","pileup_tdc");
  
 common:   r2c4s9/out_ecl5;
 in0:      r2c4s9/out_ecl7;
 in1:      r2c4s9/out_ecl1;
}



SC4800(r1c2s12)
{
  CONTROL("DAQ_FCN","KILL:ARGS:qnd_read");

 in0_7:    r2c3s7u1_8/out_ecl;
 in8_15:   r2c3s13u1_8/out_ecl;
 in16_23:  r3c7s23/out_ecl1_8;
 in24_31:  r2c4s15/outb1_8;
 in32_39:  ;//r2c4s17/outb1_8;
 in40_47:  r2c4s19/outb1_8;
}
    
LECROY4434(r1c2s14)
{
  CONTROL("VARNAME","scaler_2_14");

  CONTROL("DAQ_FCN","ARGS:PHASE:SCALER_QND_READ_CLEAR:qnd_read_clear");
  CONTROL("DAQ_FCN","ARGS:PHASE:SCALER_QND_READ_NO_CLEAR:qnd_read_no_clear");
  CONTROL("DAQ_FCN","ARGS:PHASE:SCALER_QND_BUFFER_READ_NO_CLEAR:qnd_buffer_read_no_clear");

 in1_16:   r4c9s3/out_ecl1_16;
 in17_32:  r2c3s1/out_ecl1_16;
}


LECROY4448(r1c2s18)
{
  CONTROL("VARNAME","coinc_2_18");

  CONTROL("DAQ_FCN","ARGS:PHASE:COINC_QND_READ_CLEAR:qnd_read_clear");
  CONTROL("DAQ_FCN","PHASE:COINC_QND_CLEAR:qnd_clear");

  // in1_8:   r2c1s1/after_reduction1a_8a;
  //in9_16:  r2c1s7/after_reduction1a_8a;

  //gate:  r2c1s13u4/out3;
}

LECROY4434(r1c2s19)
{
  CONTROL("VARNAME","scaler_2_19");

  CONTROL("DAQ_FCN","ARGS:PHASE:SCALER_QND_READ_CLEAR:qnd_read_clear");
  CONTROL("DAQ_FCN","ARGS:PHASE:SCALER_QND_READ_NO_CLEAR:qnd_read_no_clear");
  CONTROL("DAQ_FCN","ARGS:PHASE:SCALER_QND_BUFFER_READ_NO_CLEAR:qnd_buffer_read_no_clear");

  //in1_8:    r2c1s1/after_deadtime1_8;
  //in9_16:   r2c1s7/after_deadtime1_8;

  //in17_24:  r2c1s1/after_reduction1b_8b;
  //in25_32:  r2c1s7/after_reduction1b_8b;
}

LECROY4434(r1c2s20)
{
  CONTROL("VARNAME","scaler_2_20");

  CONTROL("DAQ_FCN","ARGS:PHASE:SCALER_QND_READ_CLEAR:qnd_read_clear");
  CONTROL("DAQ_FCN","ARGS:PHASE:SCALER_QND_READ_NO_CLEAR:qnd_read_no_clear");
  CONTROL("DAQ_FCN","ARGS:PHASE:SCALER_QND_BUFFER_READ_NO_CLEAR:qnd_buffer_read_no_clear");

  //in1_8:    r2c1s1/before_deadtime1_8;
  //in9_16:   r2c1s7/before_deadtime1_8;

  //in17_32:  .s21/in_cont0_15;
}

LECROY2365(r1c2s21)
{
  CONTROL("VARNAME","logic_matrix_2_21");
  CONTROL("DAQ_CODE","DAQ_EVENTWISE");

  //in0_15:   .s22/in_cont0_15;
  //in_cont0_15: .s20/in17_32;  // cable continues (not terminated)
  
  //out0a: /*LABEL("lmtrig1") */  r2c1s1/ecl_in1;
  //out1a: /*LABEL("lmtrig2") */  r2c1s1/ecl_in2;
  //out2a: /*LABEL("lmtrig3") */  r2c1s1/ecl_in3;
  //out3a: /*LABEL("lmtrig4") */  r2c1s1/ecl_in4;
  //out4a: /*LABEL("lmtrig5") */  r2c1s1/ecl_in5;
  //out5a: /*LABEL("lmtrig6") */  r2c1s1/ecl_in6;
  //out6a: /*LABEL("lmtrig7") */  r2c1s1/ecl_in7;
  //out7a: /*LABEL("lmtrig8") */  r2c1s1/ecl_in8;
}

LECROY2365(r1c2s22)
{
  CONTROL("VARNAME","logic_matrix_2_22");
  CONTROL("DAQ_CODE","DAQ_EVENTWISE");

  //in0_15:   r2c2s1/out_ecl1_16;
  //in_cont0_15: r1c2s21/in0_15;   // cable continues (not terminated)
  // ;

  //out0a: /*LABEL("lmtrig9") */  r2c1s7/ecl_in1;
  //out1a: /*LABEL("lmtrig10")*/  r2c1s7/ecl_in2;
  //out2a: /*LABEL("lmtrig11")*/  r2c1s7/ecl_in3;
  //out3a: /*LABEL("lmtrig12")*/  r2c1s7/ecl_in4;
  //out4a: /*LABEL("lmtrig13")*/  r2c1s7/ecl_in5;
  //out5a: /*LABEL("lmtrig14")*/  r2c1s7/ecl_in6;
  //out6a: /*LABEL("lmtrig15")*/  r2c1s7/ecl_in7;
  //out7a: /*LABEL("lmtrig16")*/  r2c1s7/ecl_in8;
}

// TODO: check this!
CBV1000(r1c2s24)
{
  SETTING("CRATE_NO" => "2");

 vsb_in:  r1c3s1/vsb_out;//checked
 vsb_out: r4c3s24/vsb_in;//???????
}


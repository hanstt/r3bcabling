// checked 31/05/2007 by shift D

MODULE(DUMMY_DETECTOR)
{
  LABEL("Dummy");

  CONNECTOR(signal);
}

/*
 * Camac crate 3 in messcontainer.  H�tte West.
 *
 */

CAMAC_CRATE(r4c3)
{
  ;
}

/* DAQ energies.
 */

SILENA4418Q(r4c3s6)
{
  SERIAL("SIA0429");
  //LABEL("POS 1 and ROLU 1");

  CONTROL("DAQ_FCN","ARGS:PHASE:QND_READ_CLEAR:N:qnd_read_clear");

 gate:		r3c5s23/out_ecl4; // r4c3s7/gate_bus;
 in0_7:         r5c1/e1_8;
}

/* S2 & S8 - 2007/05/03 */
SILENA4418Q(r4c3s9)
{
  SERIAL("SIA0480");

  CONTROL("DAQ_FCN","ARGS:PHASE:QND_READ_CLEAR:N:qnd_read_clear");

 gate:    r3c5s23/out_ecl3;
					
  //in4:     r3c2/out4;
  //in5:     r3c2/out5;
  //in6:     r3c5s1/out15;
  //in7:     r3c5s1/out16;
 in4:     r3c5s1/out15;
 in5:     r3c5s1/out16;
 in6:     r3c2/out4;
 in7:     r3c2/out5; 
}

/* This dummy detector is used only to create a signal in the TDC and
 * QDC below, such that the generated read-out code does not change.
 * Bad, since the data-format is fixed for the CAMAC stuff.
 */
DUMMY_DETECTOR(rDUMMYc1s1)
{
  LABEL("GFI01_35");
 signal: r4c3s11/stop7;
}
DUMMY_DETECTOR(rDUMMYc1s2)
{
  LABEL("GFI02_35");
 signal: r4c3s23/in7;
}

/* S2 & S8 - 2007/05/03 */
SILENA4418T(r4c3s11)
{
  SERIAL("SIA0400");
	
  SETTING("GAIN" => "25ps/ch");
  CONTROL("DAQ_FCN","ARGS:PHASE:QND_READ_CLEAR:N:qnd_read_clear");

 start:   r3c6s15/out_ecl4;

 stop7: rDUMMYc1s1/signal;
}

//SILENA4418Q(r4c3s7)
//{
//  SERIAL("SIA0418");
//  //LABEL("POS 2 and ROLU 2");
//
//  CONTROL("DAQ_FCN","ARGS:PHASE:QND_READ_CLEAR:qnd_read_clear");
//
// gate:   "yellow" <- "D"   , r3c5s23/out_ecl4;
// gate_bus: "D" -> "yellow" , r4c3s6/gate; // continuation
// in1_8:                      r5c2/e1_8;
//}

/* Build coincidences.  First each POS separately, then
 * both together (good beam).
 */

/*
SILENA4418T(r4c3s9)
{
  SERIAL("SIA8035");
  //LABEL("S2");

  SETTING("GAIN" => "50ps/ch");

  CONTROL("DAQ_FCN","ARGS:PHASE:QND_READ_CLEAR:N:qnd_read_clear");

 start:   "orange" <- , r3c5s23(EC1610)/out_ecl3;

 stop0_7: r4c9s17/out_ecl1_8;
}
*/
/*
// Module plugged out
SILENA4418T(r4c3s10)
{
  SERIAL("SIA0469");
  //LABEL("S2");

  SETTING("GAIN" => "50ps/ch");

  CONTROL("DAQ_FCN","ARGS:PHASE:QND_READ_CLEAR:N:qnd_read_clear");

 start:   "red" <- , r3c5s23(EC1610)/out_ecl2;

 stop0_7: r2c4s19(DL8010)/outa1_8;
}
*/
/*
SILENA4418Q(r4c3s11)
{
  SERIAL("SIA0480");
  CONTROL("DAQ_FCN","ARGS:PHASE:QND_READ_CLEAR:N:qnd_read_clear");

 gate: "brown" <- ,  r3c5s23(EC1610)/out_ecl1;
 in0:        r2c4s21u2/out1;
 in1:        r2c4s21u1/out1;
 in2:        r3c8/out3;
 in3:        r3c8/out1;
  // in1_8:  r4c9s17/out_e1_8;
}
*/
/* PSP */
PHILLIPS7164H(r4c3s13)
{
  SERIAL("PHI14829");
  SETTING("VIRTUAL_SLOT" => "1");

 clear:    r1c2s2/p4;	//r3c5s17u3/out1;
 gate:     r3c5s17u1/out2;

 in1_4: r4c10s7u1_4/out;

 in5_8: r4c10s3u1_4/out;

 in13:     r4c10s5u3/out;
 in14:     r4c10s5u1/out;
 in15:     r4c10s15u3/out;
}

/*
SILENA4418Q(r4c3s14)
{
SERIAL("SIA0458");

  CONTROL("DAQ_FCN","ARGS:PHASE:QND_READ_CLEAR:N:qnd_read_clear");

 //in0_3:    r5c2/e1_4;
 //in4_7:    r5c2/e5_8;
 //gate:     r3c5s23/out_ecl9;
}
*/

/*
SILENA4418Q(r4c3s15)
{
 SERIAL("SIA0416");

  CONTROL("DAQ_FCN","ARGS:PHASE:QND_READ_CLEAR:N:qnd_read_clear");

 in0_7:    r5c8/e1_8;
 gate:     r3c6s15/out_ecl1;

}
*/

/*PILE UP */
CAENC414(r4c3s16)
{
  SERIAL("C_FRS121");
  SETTING("GAIN" => "50ps/ch");

  CONTROL("DAQ_FCN","ARGS:PHASE:QND_READ_CLEAR:N:qnd_read_clear");

  CONTROL("VARNAME","qnd_pileup_tdc");

 common_start:    r3c5s15u1/out3;
 common_stop:     r4c7s23/out9;
 stop1:    r4c7s13u5/out2;
 stop2:    r4c8s21u1/out4;
 stop3:    r3c7s19u8/out2;
}

/*PIX GFI */
CAENC414(r4c3s17)
{
  SERIAL("C_FRS120");
  SETTING("GAIN" => "50ps/ch");

  CONTROL("DAQ_FCN","ARGS:PHASE:QND_READ_CLEAR:N:qnd_read_clear");

 common_start:    r3c6s15/out4;
 common_stop:     r4c7s23/out10;
 stop1:    r4c7s17u2/out1;
 stop2:    r4c7s17u4/out1;
 stop3_6:    r4c7s23/out5_8;
}


SILENA4418T(r4c3s19)
{
  SERIAL("SIA0426");
  //LABEL("POS 1 and ROLU 1");
  SETTING("GAIN" => "25ps/ch");
  CONTROL("DAQ_FCN","ARGS:PHASE:QND_READ_CLEAR:N:qnd_read_clear");

 start:   r3c6s15(EC1610)/out_ecl1;

 stop0_7: r4c7s21(EC1610)/out_ecl1_8;
}

/* DAQ times.
 */
/*
SILENA4418T(r4c3s20)
{
  SERIAL("SIA0400");
  SETTING("GAIN" => "25ps/ch");
  CONTROL("DAQ_FCN","ARGS:PHASE:QND_READ_CLEAR:N:qnd_read_clear");

 start:   	r3c6s15/out_ecl2;
 stop0_7:   .s22/outa1_8;//r4c7s21/out_ecl1_8;
// stop0_7: r4c7s23(EC1610)/out_ecl1_8;
}
*/
/*
SILENA4418T(r4c3s21)
{
  SERIAL("SIA0401");
  SETTING("GAIN" => "25ps/ch");
  CONTROL("DAQ_FCN","ARGS:PHASE:QND_READ_CLEAR:N:qnd_read_clear");

 start:   	r3c6s15/out_ecl3;
 stop0_7:   .s22/outa9_16;//r4c7s21/out_ecl9_16;
// stop0_7: r4c7s23(EC1610)/out_ecl1_8;
}
*/
/*
DL1610(r4c3s22)
{
	SERIAL("6663");

	in1_8: 		.c7s21/out_ecl1_8;
	in9_16: 	.c7s21/out_ecl9_16;
	outa1_8:		.s20/stop0_7;
	outa9_16:	.s21/stop0_7;
}
*/

/* PIX GFI */
SILENA4418Q(r4c3s23)
{
  SERIAL("SIA0458");
  CONTROL("DAQ_FCN","ARGS:PHASE:QND_READ_CLEAR:N:qnd_read_clear");

 gate: "green" <- , r3c5s23(EC1610)/out_ecl1;
 gate_bus: "blue" <- , r3c6s15/in_ecl7;

 in0:   r5c3/e2;
 in1:   r5c3/e1;
 in2:   r5c3/e3;
 in3:   r5c3/e4;
 in5:   r5c4/e2;
 in4:   r5c4/e1;
 in6:   r5c4/e3;

 in7:   rDUMMYc1s2/signal;
}



// TODO: check this!
CBV1000(r4c3s24)
{
  SETTING("CRATE_NO" => "3");

 vsb_in: r1c2s24/vsb_out;
}



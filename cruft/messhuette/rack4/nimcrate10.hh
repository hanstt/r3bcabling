//checked 31/05/2007 by shift D

// Messhuette Rack 4, NimCrate 10, implented mar07
// Mainly for PSP and POS 
NIM_CRATE(r4c10)
{
	;
}
//  EM7175(r4c10s1)                   //AMP1003(r4c10s3)
//  {
//    SERIAL("215"); ///5282
//  }

//  EM7175_U(r4c10s1u1)
//  {
//    SETTING("POLARITY" => "POS");
//    SETTING("GAIN" => "20");
  
//    //in:  r4c11/out9;
//   out: r4c3s13/in5;
//  }

//  EM7175_U(r4c10s1u2)
//  {
//    SETTING("POLARITY" => "POS");
//    SETTING("GAIN" => "50");

//    //in:  r4c11/out10;
//   out: r4c3s13/in6;
//  }

//  EM7175_U(r4c10s1u3)
//  {
//    SETTING("POLARITY" => "POS");
//    SETTING("GAIN" => "50");

//    //in:  r4c11/out11;
//   out: r4c3s13/in7;
//  }

//  EM7175_U(r4c10s1u4)
//  {
//    SETTING("POLARITY" => "POS");
//    SETTING("GAIN" => "20");

//    //in:  r4c11/out12;
//   out: r4c3s13/in8;
//  }
//copied from psp.hh
 EM7175(r4c10s3)                   //AMP1003(r4c10s3)
 {
   SERIAL("215"); ///5282
 }

 EM7175_U(r4c10s3u1)
 {
   SETTING("POLARITY" => "NEG");
   SETTING("GAIN" => "50");
  
   //in:  r4c11/out13;
  out: r4c3s13/in5;
 }

EM7175_U(r4c10s3u2)
{
 SETTING("POLARITY" => "POS");
 SETTING("GAIN" => "200");

 //in:  r4c11/out10;
out: r4c3s13/in6;
}

 EM7175_U(r4c10s3u3)
 {
   SETTING("POLARITY" => "NEG");
   SETTING("GAIN" => "100");

   //in:  r4c11/out29;
  out: r4c3s13/in7;
 }

EM7175_U(r4c10s3u4)
{
 SETTING("POLARITY" => "POS");
 SETTING("GAIN" => "200");

 //in:  r4c11/out12;
out: r4c3s13/in8;
}

 EM7175(r4c10s5)
 {
   SERIAL("213");
 }

 EM7175_U(r4c10s5u1)
 {
   SETTING("POLARITY" => "NEG");
   SETTING("GAIN" => "100");

  in:  r4c11/out13;
  out: r4c3s13/in14;
 }

//  EM7175_U(r4c10s5u2)
//  {
//    SETTING("POLARITY" => "POS");
//    SETTING("GAIN" => "100");

//   in:  r4c11/out10;
//   out: r4c3s13/in2;
//  }

 EM7175_U(r4c10s5u3)
 {
   SETTING("POLARITY" => "POS");
   SETTING("GAIN" => "100");

  in:  r4c11/out29;
  out: r4c3s13/in13;
 }

//  EM7175_U(r4c10s5u4)
//  {
//    SETTING("POLARITY" => "POS");
//    SETTING("GAIN" => "100");

//   in:  r4c11/out12;
//   out: r4c3s13/in4;
//  }

EM7175(r4c10s7)
{
SERIAL("211");
}

EM7175_U(r4c10s7u1)
{
SETTING("POLARITY" => "POS");
SETTING("GAIN" => "50");

in:  r4c11/out9;
out: r4c3s13/in1;
}

EM7175_U(r4c10s7u2)
{
SETTING("POLARITY" => "POS");
SETTING("GAIN" => "50");

in:  r4c11/out10;
out: r4c3s13/in2;
}

EM7175_U(r4c10s7u3)
{
SETTING("POLARITY" => "POS");
SETTING("GAIN" => "50");

in:  r4c11/out11;
out: r4c3s13/in3;
}

EM7175_U(r4c10s7u4)
{
SETTING("POLARITY" => "POS");
SETTING("GAIN" => "50");

in:  r4c11/out12;
out: r4c3s13/in4;
}

//TC248(r4c10s9)
//{
// SERIAL("2294");
  /*in:       r4c11/out13;
    out_uni:  r4c3s3/in13;*/
//out_time: .s21/in1;
//}

// TC248(r4c10s11)
// {
//   SERIAL("2296");
//  in:       r4c11/out29;
//  out_uni:  r4c3s13/in14;
//  out_time: .s21/in2;
// }

// TC248(r4c10s13)
// {
//   SERIAL("2291");
//  in:       r4c11/out13;
//  out_uni:  r4c3s13/in13;
//  out_time: .s21/in3;
// }

AMP1003(r4c10s15)
{
  ;
}


//AMP1003_U(r4c10s15u1)
//{
// SETTING("POLARITY" => "NEG");
//  SETTING("GAIN" => "50");

// in:      r4c11/out13;
// out:     r4c3s13/in13;
 //}

//AMP1003_U(r4c10s15u2)
//{
// SETTING("POLARITY" => "NEG");
// SETTING("GAIN" => "50");

//in:      r4c11/out29;
//out:     r4c3s13/in14;
//}

AMP1003_U(r4c10s15u3)
{
  SETTING("POLARITY" => "NEG");
  SETTING("GAIN" => "20");

 in:      r4c11/out45;
 out:     r4c3s13/in15;
}
/*
AMP1003_U(r4c10s15u4)
{
  SETTING("POLARITY" => "NEG");
  SETTING("GAIN" => "100");

 in:      ;
 out:     ;
}
*/
/*
TC248(r4c10s17)
{
 in:       ;
 out_uni:  ;
 out_time: ;
}
*/
//copied from psp.hh

//copied from pos.hh
DP1600(r4c10s17)
{
	//out1_4:    .c9s17/in1_4;
	//in1_4:		 .c9s21u2_5/outA;
;}
//copied from pos.hh

//copied from psp.hh
/*
TC248(r4c10s19)
{
 in:       ;
 out_uni:  ; 
 out_time: ;
}
*/

CF8201(r4c10s21)
{
  // in1:  .s9/out_time;
 // in2:  .s11/out_time;
 // in3:  .s13/out_time;

 //out1: ;
 //out2: ;
 //out3: ;

 ta_ecl1_8: .s23/in1_8;
}

DL8010(r4c10s23)
{
 in1_8:  .s21/ta_ecl1_8;
}
//copied from psp.hh

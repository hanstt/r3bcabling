// checked 31/05/2007 by shift D

// Messhuette Rack 4, NimCrate 8, implented mar07
// Mainly logic electronics, e.g. for POS
NIM_CRATE(r4c8)
{
	;
}
//copied from pos.hh
/* Constant fractions for times.
 */
/* POS 1 and 2 */
CF8000(r4c8s7){
  ;
}

CF8000_U(r4c8s7u1)
{
 in:    r5c1s2/out1;
 outB2: .s13u4/inA;
 outA:  r5c5/in1;
}

CF8000_U(r4c8s7u2)
{
 in:    r5c1s2/out2;
 outB2: .s13u4/inB; 
 outA:  r5c5/in2;
}
CF8000_U(r4c8s7u3)
{
 in:    r5c1s2/out3;
 outB2: .s13u4/inC;
 outA:  r5c5/in3;
}
CF8000_U(r4c8s7u4)
{
 in: r5c1s2/out4;
 outB2: .s13u4/inD;
 outA: r5c5/in4;
 }
CF8000(r4c8s9)
{
  ;
}


CF8000_U(r4c8s9u1)
{
  SETTING("DELAY"    => "4ns");
  SETTING("FRACTION" => "0.4");

  in:      .c8s13u4/out3;
  outB2:   LABEL("POS01_ANY")  r4c9s3/in5;
}

//CF8000_U(r4c8s9u3)
//{
  //LABEL("POS 1 3");
//  SETTING("DELAY"    => "4ns");
// S ETTING("FRACTION" => "0.4");

// in:    "13" <- "13" , r5c1s2/out3;
// outA:  "3"  -> "3"  , r5c5(DELAY)/in3;
// outB2:                r4c8s13u1(LOGIC_UNIT)/inC;
//}
  
CF8000_U(r4c8s9u7)
{
  //LABEL("POS 2 4");
  SETTING("DELAY"    => "4ns");
  SETTING("FRACTION" => "0.4");

 in:    "1"  <- "1"  , r5c3s2/out1;
 outA:  "1"  -> "1"  , r5c6/in1;
 outB2:                r4c8s19u3(LOGIC_UNIT)/inA;
}

CF8000(r4c8s11)
{
  SERIAL("90");
}

//CF8000_U(r4c8s11u1)
//{
  //LABEL("POS 1 1");
  //  SETTING("DELAY"    => "4ns");
  //  SETTING("FRACTION" => "0.4");
  // in:    "11" <- "11" , r5c1s2/out1;
 // outA:                 r5c6/in1;
// outB2:                r4c8s13u1/inA;
//}

//CF8000_U(r4c8s11u2)     // broken
//{
//  //LABEL("POS 1 2");
  //SETTING("DELAY"    => "4ns");
  //SETTING("FRACTION" => "0.4");

//in:    "12" <- "12" , r5c1s2/out2;
//outA:  "2"  -> "2"  , r5c5(DELAY)/in2;
//outB2:                r4c8s13u1(LOGIC_UNIT)/inB;
//}

// Moved to .s9
//CF8000_U(r4c8s11u3)
//{
  //LABEL("POS 1 3");
  // SETTING("DELAY"    => "4ns");
  //SETTING("FRACTION" => "0.4");

  // in:    "13" <- "13" , r5c1s2/out3;
 //outA:  "3"  -> "3"  , r5c5(DELAY)/in3;
 //outB2:                r4c8s13u1(LOGIC_UNIT)/inC;
 //}


//CF8000_U(r4c8s11u4)
//{
  //LABEL("POS 1 4");
  //SETTING("DELAY"    => "4ns");
  //SETTING("FRACTION" => "0.4");

//in:    "14" <- "14" , r5c1s2/out4;
// outA:  "4"  -> "4"  , r5c5(DELAY)/in4;
// outB2:                r4c8s13u1(LOGIC_UNIT)/inD;
//}


CF8000_U(r4c8s11u5)
{
  //LABEL("POS 2 1");
   SETTING("DELAY"    => "4ns");
   SETTING("FRACTION" => "0.4");

 in:    	       r5c3s2/out2;
 outA:  "2"  -> "2"  , r5c6(DELAY)/in2;
 outB1: r2c11/front2;
 outB2: r4c8s19u3/inB;
   // r4c8s19u3(LOGIC_UNIT)/inA;
}
//CF8000_U(r4c8s11u6)
//{
  //LABEL("POS 2 2");
//  SETTING("DELAY"    => "4ns");
// SETTING("FRACTION" => "0.4");

// in:    "1" <- "1"   , r5c1s2/out2;    //r5c3s2/out1;
//outA:  "1"  -> "1"  , r5c5/in2;       //r5c6(DELAY)/in1;
//outB2:                r4c8s13u1/inB;  //r4c8s19u3(LOGIC_UNIT)/inB;
//}

//CF8000_U(r4c8s11u7)
//{
  //LABEL("POS 2 3");
//  SETTING("DELAY"    => "4ns");
//  SETTING("FRACTION" => "0.4");

//in:    "23" <- "23" , r4c7s11(DELAY)/out;
// outA:  "7"  -> "7"  , r5c6/in2;
// outB2:                r4c8s19u3(LOGIC_UNIT)/inA;
  //}

/* MOVED to module slot 9 26/09/07
CF8000_U(r4c8s11u8)
{
  //LABEL("POS 2 4");
  SETTING("DELAY"    => "4ns");
  SETTING("FRACTION" => "0.4");

 in:    "3"  <- "3"  , r5c3s2/out3;
 outA:  "3"  -> "3"  , r5c6/in3;
 outB2:                r4c8s19u3(LOGIC_UNIT)/inC;
}
*/
//copied from pos.hh

//copied from other.hh
PS756(r4c8s13)
{
  SERIAL("15934");
}

//PS756_U(r4c8s13u1)
//{
 //LABEL("POS01");
 //inA:    .s11u1/outB2;
 //inB:    .s11u6/outB2;
 //inC:    .s9u3/outB2;
 //inD:    .s11u4/outB2;
 //out1:   .s21u1/in1;

 //out3:   .s9u1/in;//LABEL("POS01_ANY")  r4c9s3/in5;
 //out2:   r4c8s13u3/inA;
 //out4:   .s15u2/in;  
//}  
PS756_U(r4c8s13u2)
{
 inA:  .s23u5/outB1;
 inB:  .s23u6/outB1;
 inC:  .s23u7/outB1;
 inD:  .s23u8/outB1;

 out1: .s13u3/veto;			 
 //out2: r2c11/front2; 
 //out3: .s19u1/inA;
}

PS756_U(r4c8s13u3)
{
 inA:   .s13u4/out2;
 veto:  .s13u2/out1;  //.s19u1/out1;
 out2:  r4c9s3/in6;
 out3:  r3c2/in14;
} 

PS756_U(r4c8s13u4)
{
 inA:  r4c8s7u1/outB2;
 inB:  r4c8s7u2/outB2;
 inC:  r4c8s7u3/outB2;
 inD:  r4c8s7u4/outB2;

 out1: r4c8s21u1/in1;
 out2: r4c8s13u3/inA;
 out3: .c8s9u1/in;
}
//copied from other.hh

//copied from pos.hh
/*DV8000(r4c8s15)
{
	SERIAL("2655");
}


DV8000_U(r4c8s15u2)
{
 in:   .s13u1/out4;
 out1: r4c7s15/in1;	       
 out2: .s13u3/inA;
}
*/
//copied from pos.hh

//copied from other.hh
CO4001(r4c8s19)
{
  SERIAL("7316");
}

//CO4001_U(r4c8s19u1)
//{
// SETTING("A" => "OR");
// SETTING("B" => "OR");
// SETTING("C" => "OR");
// SETTING("OUT" => "OR");

 //inA:      .s13u2/out3;
 //inB:      .s13u4/out3;
 //out1:     .s13u3/veto;
//}

CO4001_U(r4c8s19u2)
{
 SETTING("A" => "OR");
 SETTING("B" => "OR");
 SETTING("C" => "OR");
 SETTING("OUT" => "OR");
 inA:      .s23u3/outB2;
 inB:      .s23u2/outB2;
 out2:     r2c2s5u4/in;
}

CO4001_U(r4c8s19u3)
{
  SETTING("A" => "OR");
  SETTING("B" => "OR");
  SETTING("C" => "OR");
  SETTING("OUT" => "OR");


  // SETTING("A" => "OR");
  //SETTING("B" => "OR");
  //SETTING("C" => "OR");
  //SETTING("OUT" => "OR");

 inA:      .s9u7/outB2;
 inB:      .s11u5/outB2;
 out2:     r4c9s3/in8;
 out3:      r3c1s8/in;



  // inA:     .s11u5/outB2;
  //inB:     .s11u6/outB2;
 
  //inC:     .s9u7/outB2;
  //out1:    r4c9s3/in8;
  //out3:    r3c1s8(KABEL)/in;
}

//CO4001_U(r4c8s19u4)
  //
  //{ SETTING("A" => "OR");
  //SETTING("B" => "OR");
  //SETTING("C" => "AND");
  //SETTING("OUT" => "AND");
  
  //inA:    r4c9s21u4/outB1;
  //inB:    r4c9s21u5/outB1;
  //inB:    s11u7/outB2;
  //inC     .s11u8/outB2;
//}

LF4000(r4c8s21)
{
  SERIAL("3667");
}

LF4000_16_U(r4c8s21u1)
{
 in1:       .s13u4/out1;
 out1:     r3c6s7u8/in_nim;    //r4c7s1u2/start;
 out2:      r2c2s1/in2;
 out3:      r2c2s21u4/inA;
 out4:      r4c3s16/stop2;  
 out5:      r4c7s15/in1;
 out8:      r3c7s19u8/in;
// not_out1:  r2c2s23u1/start; //Yellow cable that was sabotage
}

// LF4000_16_U(r4c8s21u2)
// {     
//  out1:     r4c7s15/in1;
//  out4:     r3c7s19u8/in;
// }



//LF4000_16_U(r4c8s21u4)
//{
// out3:     r2c6s5u7/in;
//}


/* ROLU 1 */

//CF8000(r4c8s21)
//{
//  SERIAL("2521");
// out_or: r4c8s13u3/veto;
//}

CF8000(r4c8s23)
{
  SERIAL("7021");
}

CF8000_U(r4c8s23u1)
{
 in:     r5c4/t1;
 outB1:  r4c9s3/in14;
 outA:   r5c6/in5;
}


CF8000_U(r4c8s23u2)
{
 in:     r5c3s2/out8;
 outB1:  r4c9s3/in15;
 outB2:  .s19u2/inB;
 outA:   r5c6/in6;
}

CF8000_U(r4c8s23u3)
{
 in:     r5c4/t3;
 outB1:  r4c9s3/in16;
 outB2:  .s19u2/inA;
 outA:   r5c6/in7;
}

CF8000_U(r4c8s23u5)
{
 in:     r5c1s2/out5;
 outA:   r4c9s3/in1;
 outB1:  .s13u2/inA;
}

CF8000_U(r4c8s23u6)
{
 in:     r5c1s2/out6;
 outA:   r4c9s3/in2;
 outB1:  .s13u2/inB;
}

CF8000_U(r4c8s23u7)
{
 in:     r5c1s2/out7;
 outA:   r4c9s3/in3;
 outB1:  .s13u2/inC;
}

CF8000_U(r4c8s23u8)
{
 in:     r5c1s2/out8;
 outA:   r4c9s3/in4;
 outB1:  .s13u2/inD;
}
//copied from other.hh

// Messhuette Rack 2, NimCrate 3, implented mar07, modified 20070530 (DR)
// 
NIM_CRATE(r2c3)
{
	; // mainly FRS related modules...
}
//copied from other.hh
EC1601(r2c3s1)
{
 in1:  .c4s11/out1;
 in2:  .c4s11/out2;
 in3:  .c4s11/out9;
 in4:  .c4s11/out10;
			 
 in5:  r3c5s5u2/out8;//r2c4s9/out1;//r3c6s7u5/out_nim;//rSAFDEPc1s1u1/signal;//.s23u3/out1;
 in6:  r3c6s7u5/out_nim;//rSAFDEPc1s1u2/signal;//.s23u4/out1;
 
 in7:  r3c6s7u7/out_nim;
 in8:  r2c4s3u3/out1;//r2c2s15u8/out1;//r3c5s7/out_1MHz;

 in9:  r2c3s7/out_or;
 in10: r2c3s13/out_or;

 in11: .c4s11/out3;
 in12: .c4s11/out5;
 in13: .c4s11/out11;
 in14: .c4s11/out12;
				 
 //in15:   r2c1s1/sum3;
 //in16:   r2c1s7/sum2;

 out_ecl1_16: r1c2s14/in17_32;
 //out5 : r6c1s1u3/in1;
 //out6 : r6c1s1u4/in1;
 //out8: r1c4s6/nim_in13;
 
 // in_ecl9_16:   r2c2s1/out_ecl9_16;
}

//copied from frs.hh
OCTAL_SPLIT(r2c3s5)
{
	;
}

OCTAL_SPLIT_U(r2c3s5u1)
{
in:   r0c10/out9;
			// out1: r1c2s9/in0;
out2: r2c3s7u1/in;
}
OCTAL_SPLIT_U(r2c3s5u2)
{
in:   r0c10/out10;
			// out1: r1c2s9/in1;
out2: r2c3s7u2/in;
}
OCTAL_SPLIT_U(r2c3s5u3)
{
in:   r0c10/out11;
			// out1: r1c2s9/in2;
out2: r2c3s7u3/in;
}
OCTAL_SPLIT_U(r2c3s5u4)
{
in:   r0c10/out12;
			// out1: r1c2s9/in3;
out2: r2c3s7u4/in;
}
OCTAL_SPLIT_U(r2c3s5u5)
{
in:   r0c10/out13;
			// out1: r1c2s9/in4;
out2: r2c3s7u5/in;
}
OCTAL_SPLIT_U(r2c3s5u6)
{
in:   r0c10/out14;
			// out1: r1c2s9/in5;
out2: r2c3s7u6/in;
}
OCTAL_SPLIT_U(r2c3s5u7)
{
  //in:   ; //r0c10/out15;
			// out1: r1c2s9/in6;
out2: r2c3s7u7/in;
}
OCTAL_SPLIT_U(r2c3s5u8)
{
  //in:  ;// r0c10/out16;
			// out1: r1c2s9/in7;
out2: r2c3s7u8/in;
}




CF8000(r2c3s7)
{
out_or:   r2c3s1/in9;
}

CF8000_U(r2c3s7u1) { in: .s5u1/out2; outA: .s11/in9;  out_ecl: r1c2s12/in0; }//the outs ecl are coming from the back part, somebody should check it
CF8000_U(r2c3s7u2) { in: .s5u2/out2; outA: .s11/in10; out_ecl: r1c2s12/in1; }
CF8000_U(r2c3s7u3) { in: .s5u3/out2; outA: .s11/in11; out_ecl: r1c2s12/in2; }
CF8000_U(r2c3s7u4) { in: .s5u4/out2; outA: .s11/in12; out_ecl: r1c2s12/in3; }
CF8000_U(r2c3s7u5) { in: .s5u5/out2; outA: .s11/in16; out_ecl: r1c2s12/in4; }
CF8000_U(r2c3s7u6) { in: .s5u6/out2; outA: .s11/in14; out_ecl: r1c2s12/in5; }
CF8000_U(r2c3s7u7) { in: .s5u7/out2; outA: .s11/in15; out_ecl: r1c2s12/in6; }
CF8000_U(r2c3s7u8) { in: .s5u8/out2; outA: ;          out_ecl: r1c2s12/in7; }

EC1601(r2c3s11)
{
in1_8:   .s13u1_8/outA;
in9_12:  .s7u1_4/outA;
in13:  ; // not used since input 13 (1-based) on TDC broken
in14:  .s7u6/outA;
in15:  .s7u7/outA;
in16:  .s7u5/outA;

			 //out_ecl1_8:  r4c3s1/stop1_8;
			 //out_ecl9_12: r4c3s1/stop9_12;
			 //out_ecl14_16: r4c3s1/stop14_16;
}

CF8000(r2c3s13)
{
out_or:   r2c3s1/in10 ;
}

CF8000_U(r2c3s13u1) { in: .s15u1/out1; outA: .s11/in1; out_ecl: r1c2s12/in8; }
CF8000_U(r2c3s13u2) { in: .s15u2/out1; outA: .s11/in2; out_ecl: r1c2s12/in9; }
CF8000_U(r2c3s13u3) { in: .s15u3/out1; outA: .s11/in3; out_ecl: r1c2s12/in10; }
CF8000_U(r2c3s13u4) { in: .s15u4/out1; outA: .s11/in4; out_ecl: r1c2s12/in11; }
CF8000_U(r2c3s13u5) { in: .s15u5/out1; outA: .s11/in5; out_ecl: r1c2s12/in12; }
CF8000_U(r2c3s13u6) { in: .s15u6/out1; outA: .s11/in6; out_ecl: r1c2s12/in13; }
CF8000_U(r2c3s13u7) { in: .s15u7/out1; outA: .s11/in7; out_ecl: r1c2s12/in14; }
CF8000_U(r2c3s13u8) { in: .s15u8/out1; outA: .s11/in8; out_ecl: r1c2s12/in15; }

OCTAL_SPLIT(r2c3s15)
{
	;
}

OCTAL_SPLIT_U(r2c3s15u1)
{
in:    r0c10/out1;
out1:  .s13u1/in;
			 // out2:  r1c2s10/in0;
}

OCTAL_SPLIT_U(r2c3s15u2)
{
in:    r0c10/out2;
out1:  .s13u2/in;
			 // out2:  r1c2s10/in1;
}

OCTAL_SPLIT_U(r2c3s15u3)
{
in:    r0c10/out3;
out1:  .s13u3/in;
			 // out2:  r1c2s10/in2;
}

OCTAL_SPLIT_U(r2c3s15u4)
{
in:    r0c10/out4;
out1:  .s13u4/in;
			 // out2:  r1c2s10/in3;
}

OCTAL_SPLIT_U(r2c3s15u5)
{
in:    r0c10/out5;
out1:  .s13u5/in;
			 // out2:  r1c2s10/in4;
}

OCTAL_SPLIT_U(r2c3s15u6)
{
in:    r0c10/out6;
out1:  .s13u6/in;
			 // out2:  r1c2s10/in5;
}

OCTAL_SPLIT_U(r2c3s15u7)
{
in:    r0c10/out7;
out1:  .s13u7/in;
			 // out2:  r1c2s10/in6;
}

OCTAL_SPLIT_U(r2c3s15u8)
{
in:    r0c10/out8;
out1:  .s13u8/in;
			 // out2:  r1c2s10/in7;
}

/*SU1200(r2c3s17)
{
	SETTING("SELECT"   => "1-6");
	SETTING("POLARITY" => "NEG");

in1:  r1c0s9/m;
in2:  r1c0s11/m;
in3:  r1c0s13/m;
in4:  r1c0s15/m;

analog_out: .c2s15u3/in; //r2c2s1/in2;
}*/


/*
	 EC1601(r2c3s17)
	 {
in_ecl1_16:   r4c9s3/out_ecl1_16;

out_ecl1_16:  r1c2s14/in1_16;

in7:          r2c3s21u6/out2;
in9:          r2c2s13u1/out3;
in10:         r2c3s13/out_or;
in11:         r2c3s7/out_or;
// in13:         r2c2s5u6/out1;
}
*/

/*
	 GG8000(r2c3s21)
	 {
	 ;
	 }

	 GG8000_U(r2c3s21u6)
	 {
// in:    r2c2s13u4/out3;
// out2:  .s17/in7;
}

*/

CAPACITOR_BOX(r2c3s19)
{
 in1:  .c11/front27;//.c2s15u7/out2;
 // in2:  .c2s15u7/in;
 // in3:  .c2s15u3/out2; inside the cave SUM DTF
 in4:  .c11/front28;
 in5:  r0c9/out14;
 in6:  r0c9/out16;
 in7:  r0c9/out12;
 in8:  r0c9/out11;

 out1:  .c2s15u7/in;
 out2:  .c2s13u1/in;
 out3:  .c2s15u6/in;
 out4:  .c2s15u3/in;
 out5:  .c4s23u1/in;
 out6:  .c4s23u2/in;
 out7:  .c3s21u7/in;//.c4s23u3/in;
 out8:  .c3s21u8/in;//.c4s23u4/in;
}

CF8000(r2c3s21)
{;
}
//CF8000_U(r2c3s21u2)
//{
// in:  .s19/out3;

// outA:  r3c2/in25;
  //}

CF8000_U(r2c3s21u7)
{
 in:    .c3s19/out7;

 outB1:  r3c2/in18;
 outB2:  r2c3s23u4/inA;
//outA:   .c4s11/in11;
 out_e:  r3c5s1/in13;
}
CF8000_U(r2c3s21u8)
{
 in:    .c3s19/out8;

 //outB1:  r2c3s23u4/inB;
 outB2: r2c3s23u4/inB;
 outA:  r3c2/in19;
 out_e:  r3c5s1/in14;
}
CO4001(r2c3s23)
{
	SERIAL("3445");
}

CO4001_U(r2c3s23u1)
{
	// LABEL("S2");

	SETTING("A"   => "AND");
	SETTING("B"   => "OR");
	SETTING("C"   => "OR");
	SETTING("OUT" => "AND");

inA:  .c4s3u2/out1;


out2: r3c2/in9;
out3: r2c3s23u2/inA;
			// Scaler needed for the safety department...
			// out1: .s1/in5;
}

/*CO4001_U(r2c3s23u1)
{
  // Reshape Random TCAL start pulse to have another width (100 ns)

  SETTING("A"   => "AND");
  SETTING("B"   => "OR");
  SETTING("C"   => "OR");
  SETTING("OUT" => "AND");
  
 inA:  r2c4s3u2/out1;
  
  // In reality, out1 goes to cable delay, but that is marked as used.
  // I (HTJ) cannot fix that from far away
  //  out1: r3c2/in9;
  // instead documenting to point after cable delay
 out1: r2c5s1u1/trig;
 out3: .s23u2/inA;
 //} */

CO4001_U(r2c3s23u2)
{
  // Make sure random TCAL 1MHz stop pulses are vetoed to not appear
  // when the starts come

  SETTING("A"   => "OR");
  SETTING("B"   => "AND");
  SETTING("C"   => "OR");
  SETTING("OUT" => "AND");
  
 inA:  .s23u1/out3;
 inB:  r2c4s3u3/out3;


 overlap:  .c5s3u2/in1;
}

CO4001_U(r2c3s23u3)
{
	LABEL("S2");

	SETTING("A"   => "OR");
	SETTING("B"   => "OR");
	SETTING("C"   => "OR");
	SETTING("OUT" => "OR");

inA:  r2c4s23u2/outB1;
inB:  r2c4s23u1/outB1;

			//out1: r2c2s1/in11;
			// Scaler needed for the safety department...
			// out1: .s1/in5;
}

CO4001_U(r2c3s23u4)
{
	LABEL("S8");

	SETTING("A"   => "AND");
	SETTING("B"   => "AND");
	SETTING("C"   => "OR");
	SETTING("OUT" => "AND");

inA:  r2c3s21u7/outB2;
inB:  r2c3s21u8/outB2;

out3: r2c2s1/in13;
			// Scaler needed for the safety department...
			// out1: r2c3s1/in6;
}
//copied from frs.hh

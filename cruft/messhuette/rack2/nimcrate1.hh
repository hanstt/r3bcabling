// Messhuette Rack 2, NimCrate 1, implented mar07, modified 20070530(DR)
// 
NIM_CRATE(r2c1)
{
	;//mainly trigger-related modules coming from trigger.hh
}

//copied from trigger.hh
// GSI_TRIGGER_BOX(r2c1s1)
// {
//  ecl_in1_8:r1c2s21/out0a_7a;  

//  deadtime: .s15u1/out3;
//  sum1:     .s13u3/in1;
//  sum2:     .s21u4/in1;
//  sum3: LABEL("TrigOR1") r2c3s1/in15;
//  out1a:    r2c2s23u2/start;

//  before_deadtime1_8:   /*LABEL("BEFORE_DT_+")*/ r1c2s20/in1_8;
//  after_deadtime1_8:    /*LABEL("AFTER_DT_+") */ r1c2s19/in1_8;
//  after_reduction1a_8a: /*LABEL("AFTER_RED_+")*/ r1c2s18/in1_8;
//  after_reduction1b_8b: /*LABEL("AFTER_RED_+")*/ r1c2s19/in17_24;
// }

// GSI_TRIGGER_BOX(r2c1s7)
// {
//  ecl_in1_8:            r1c2s22/out0a_7a;
//  in5: r2c1s19u1/out1;
//  in6: .s19u3/out1;
//  in7: .c2s9u1/out3;
//  in8: .c2s9u2/out3;
 
//  out1a_4a:           .s13u1/in1_4;
//  out5a: -> "Clock" , .s17/in3;
//  out6a: -> "TCal"  , .s17/in4;
//  // out7a: -> "BoS"   , .s15/in12;
//  //out8a: -> "EoS"   , .s15/in13;

//  out5b: .s21u3/in1;
//  out6b: .s21u2/in1;
//  out7b: .s17/in12;
//  out8b: .s17/in13;

//  deadtime: .s15u1/out4;
//  sum1:     .s13u4/in2;
//  sum2: LABEL("TrigOR2") r2c3s1/in16;

//  before_deadtime1_8:   /*LABEL("BEFORE_DT_+")*/ r1c2s20/in9_16;
//  after_deadtime1_8:    /*LABEL("AFTER_DT_+") */ r1c2s19/in9_16;
//  after_reduction1a_8a: /*LABEL("AFTER_RED_+")*/ r1c2s18/in9_16;
//  after_reduction1b_8b: /*LABEL("AFTER_RED_+")*/ r1c2s19/in25_32;
// }

// LF4000(r2c1s13)
// {
//   ;
// }

// LF4000_4_U(r2c1s13u1)
// {
//  in1_4:  .s7/out1a_4a;
 
//  // Prioritz encoder
//  out1:   .s17/in2;

//  // Master Start
//  out2:   .s21u4/in3;
// }

// LF4000_4_U(r2c1s13u2)
// {/*
//   LABEL("DeadTime");

//  in1:  .s21u1/out1;
//  in3:  .s23u8/out1;

//  out1: .s7/deadtime;
//  out2: .s1/deadtime;

//  out3: .c2s9u1/inB;
//  out4: .c2s9u2/inB;*/

// ;}

// LF4000_4_U(r2c1s13u3)
// {
//  in1:  .s1/sum1;

//  out1: .s17/in1;
//  out4: .s13u4/in1;
// }

// LF4000_4_U(r2c1s13u4)
// {
//   LABEL("Accepted trigger");
 
//  in1:  .s13u3/out4;
//  in2:  .s7/sum1;
//   //in3:  .s7/sum1; 
//   //in4:  .s1/sum1; 

//  out1: .s23u6/in;
//   //out2: .s15/in1;
//  out3: r1c2s18/gate;
//  out4: .s23u8/in;
// }

// /*PRIORITY_ENCODER(r2c1s15)
// {
//  clear: .s23u2/out1;
//  gate:  .s13u4/out1;
 
//  in1:   .s13u4/out2;
//  in2:   .s13u1/out1;
//  in3:   "Clock" <- , .s7/out5a;
//  in4:   "TCal"  <- , .s7/out6a;

//  in12:  "BoS"   <- , .s7/out7a;
//  in13:  "EoS"   <- , .s7/out8a;

//  out1_4: r2c1s17/in_ecl9_12; // Trigger word
// }*/

// LF4000(r2c1s15){
//   ;
// }

// LF4000_4_U(r2c1s15u1)
// {
//  in1:  .s15u2/out1;
//  in2:  .s23u4/out2;// This is really out3 but that doesn�t compile

//  out1: .s15u3/in1;
//  out3: .s1/deadtime;
//  out4: .s7/deadtime;
// }

// LF4000_4_U(r2c1s15u2)
// {
//  in1:  r1c3s11/nim_out4;
//  in4:  .s23u8/out2;

//  not_out1:  .s23u4/in;

//  out1:  .s15u1/in1;
// }

// LF4000_4_U(r2c1s15u3)
// {
//  in1:  .s15u1/out1;

//  //out1:  ;
//  out3:  .c2s9u1/inB;
//  out4:  .c2s9u2/inB;
// }

// LF4000_4_U(r2c1s15u4)
// {
//   ;
// }

// EC1601(r2c1s17)
// {
//   // LABEL("Trigger module communication");

//  out_ecl1_8:  r1c3s4/in_ecl1_8;

//  out_ecl9_16:   r1c3s7/in_ecl9_16;
//   // out_ecl9_16: r1c3s5/in1_8; // ECL to trigger module
//  in1:  .s13u3/out1;
//  in2:  .s13u1/out1;
//  in3:  .s7/out5a;
//  in4:  .s7/out6a;
//  in12: .s7/out7b;
//  in13: .s7/out8b;
 
//  //out2:   ;//     r3c5s23/in7; // DAQ fast clear
//  //out4:        r2c1s21u1/in1; // DAQ deadtime
// }

// CO4001(r2c1s19)
// {
//   ;
// }
// CO4001_U(r2c1s19u1)
// {
//   // TODO: check settings (A, B, C, OUT) (AND / OR)
// 	// 13 Oct 2005:	OUT = A OR B OR C
//   SETTING("A"   => "AND");
//   SETTING("B"   => "OR");
//   SETTING("C"   => "OR");
//   SETTING("OUT" => "AND");

//  inA:     r3c5s7/out_100Hz;
//  out1:    .s7/in5;
//  notout1: .s19u4/inA;
// }

// CO4001_U(r2c1s19u2)
// {
//   // TODO: check settings (A, B, C, OUT) (AND / OR)
// 	// 13 Oct 2005: OUT = A AND B OR C
//   SETTING("A"   => "AND");
//   SETTING("B"   => "AND");
//   SETTING("C"   => "OR");
//   SETTING("OUT" => "AND");

//  inA:      r3c5s5u1/out8;
//  inB:      r3c6s3u2/out1; // Spill off
//  out1:     .s19u3/inA;
// }

// CO4001_U(r2c1s19u3)
// {
//   // TODO: check settings (A, B, C, OUT) (AND / OR)
// 	// 13 oct 2005: !OUT = !A OR B OR C
//   SETTING("A"   => "OR");
//   SETTING("B"   => "OR");
//   SETTING("C"   => "OR");
//   SETTING("OUT" => "OR");

//  inA:   .s19u2/out1;
//  inB:   .s19u4/out1;
//  out1:  .s7/in6;
 
// }

// CO4001_U(r2c1s19u4)
// {
//   SETTING("A"   => "AND");
//   SETTING("B"   => "OR");
//   SETTING("C"   => "OR");
//   SETTING("OUT" => "AND");

//  inA:  .s19u1/notout1;
//  out1: .s19u3/inB;
// }

// LF4000(r2c1s21)
// {
//   ;
// }

// LF4000_4_U(r2c1s21u1)
// {
//   //in1:    .s17/out4;
//   //out1:   .s13u2/in1;
// ;
// }

// LF4000_4_U(r2c1s21u2)
// {
//  in1:    .s7/out6b;
//  out4:   r3c5s17u8/in;
// }

// LF4000_4_U(r2c1s21u3)
// {
//  in1:    .s7/out5b;
//  out1:   r2c1s21u4/in2;
// } 

// LF4000_4_U(r2c1s21u4)
// {
//   LABEL("Master start");

//  in1:  .s1/sum2;
//  in2:  .s21u3/out1;
//  in3:  r2c1s13u1/out2;
//  in4:  r3c5s19u8/out2;

//  out1: r3c5s15u1/in1;
//  out2: r3c2/in23; //r3c2/in1;
//  out3: rGATEc1s1/in;
//  out4: rARMc1s1/in;
// }

// ELECTRONIC_FILTER(rGATEc1s1)
// {
// 	LABEL("GATE");

// 	in:		r2c1s21u4/out3;
// 	out:	;
// }

// ELECTRONIC_FILTER(rARMc1s1)
// {
// 	LABEL("ARM");

// 	in:		r2c1s21u4/out4;
// 	out:	;
// }

GG8000(r2c1s23)
{
  ;
}

// GG8000_U(r2c1s23u1)
// {
//   //in:   .s13u3/out4;
//   //out1: .s23u2/in;
// ;
// }

// GG8000_U(r2c1s23u2)
// {
//   //in:   .s23u1/out1;
//   //out1: .s15/clear;
//   ; 
// }

// GG8000_U(r2c1s23u4){
//  in:  .s15u2/not_out1;
//  out2: .s15u1/in2; //This is really out3 but that doesn�t compile
//}

GG8000_U(r2c1s23u5){
 in:  r2c2s19u4/out3;
 out1: r3c6s17u2/inA; //This is really out3 but that doesn�t compile
}

// GG8000_U(r2c1s23u6){
//  in:  .s13u4/out1;
//  out2:  r1c4s6/nim_in1;
// }

GG8000_U(r2c1s23u7)
{
 in:   r2c2s21u4/out1;
 out2: r2c2s19u3/in1; // r2c2s23u1/start; // pile-up rejection
}

// GG8000_U(r2c1s23u8)
// {
//  in:   r2c1s13u4/out4;
//  out2: r2c1s15u2/in4;
// }
//copied from trigger.hh

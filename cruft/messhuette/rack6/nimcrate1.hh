// // Checked 30/05/2007 by shift D

//  // Messhuette Rack 6, NimCrate 1, implented mar07
// // Used to be Haakan's playground for the spill timing...

// //copied from spill_timing.hh

// /* A small local DAQ to measure timing between POS signals.
//  * Useful for seeing the fraction of pileup one may expect.
//  */

// /* The timing is measured mainly with two clocks (1 MHz and 10 MHz and a
//  * scaler.)  Small interval (100-300ns) are measured with a TDC.
//  */

// NIM_CRATE(r6c1)
// {
//   ;
// }

// LF4000(r6c1s1)
// {
//   ;
// }

// LF4000_4_U(r6c1s1u1)
// {
//  in1:   r3c6s7u4/out_nim;

//  out1:  .s9/in1;
//  out2:  .s7u1/inA;

// }

// LF4000_4_U(r6c1s1u2)
// {
//  in1:   r3c5s7/out_10MHz;

//  out1:  .s7u3/inA;
// }

// LF4000_4_U(r6c1s1u3)
// {
//  in1:   r2c3s1/out5;

//  out1:  .s9/in2;
// }

// LF4000_4_U(r6c1s1u4)
// {
//  in1:   r2c3s1/out6;
 
//  out1:  .s9/in3;
// }

// LF4000(r6c1s3)
// {
//   ;
// }

// LF4000_4_U(r6c1s3u1)
// {
//  in1:  r4c8s21u2/out2;

//  out1:  .s9/in4;
//  out2:  ;
// }

// LF4000_4_U(r6c1s3u2)
// {
//   // Time calibration source (start & stop)

//  in1:   .s7u2/out2;
//  out1:	.s3u4/in1;
// }

// LF4000_4_U(r6c1s3u3)
// {
//  in1:   .s13u1/out;

//  out1:  .s7u1/inB;
//  out2:  .s7u3/inB;
// }

// LF4000_4_U(r6c1s3u4)
// {
//  in1:   .s3u2/out1;

//  out1:  .s17u3/in1;
//  out2:  .s5u1/in;
//  out3:  .s13u1/stop;
//  out4:  ;//.c2s8/stop0;
// }

// RD2000(r6c1s5)
// {
//   SERIAL("6163");
// }

// RD2000_U(r6c1s5u1)
// {
//  in:     .s3u4/out2;
//  divn_1: .s7u4/inA;
// }

// RD2000_U(r6c1s5u2)
// {
//   ;
// }

// CO4001(r6c1s7)
// {
//   SERIAL("3456");
// }

// CO4001_U(r6c1s7u1)
// {
//  inA:   .s1u1/out2;
//  inB:   .s3u3/out1;

//  out2:  .s9/in7;
// }

// CO4001_U(r6c1s7u2)
// {
//   // BROKEN;
//  inA:   r6c1s17u1/out1;
//  out2:  .s3u2/in1;
// }

// CO4001_U(r6c1s7u3)
// {
//  inA:   .s1u2/out1;
//  inB:   .s3u3/out2;

//  out2:  .s9/in8;
// }

// CO4001_U(r6c1s7u4)
// {
//  inA:   .s5u1/divn_1;
//  inB:   .s9/out6;
//  inC:   .s11u8/out2;

//  out2:  .s17u4/in1;
//  out1:  .s9/in10;   // trigger 2
//  out3:  .s9/in5;
// }

// EC1601(r6c1s9)
// {
//  in1:   .s1u1/out1;
//  in2:   .s1u3/out1;
//  in3:   .s1u4/out1;
//  in4:   .s3u1/out1;
//  in5:   .s7u4/out3;
//  in6:   ;
//  in7:   .s7u1/out2;
//  in8:   .s7u3/out2;
//  in9:   .s15u2/divn_1;
//  in10:  .s7u4/out1;

//   in_ecl6:  ;//   .c2s1/out6; // deadtime // howcome it was out5?

//  out6:  .s7u4/inB;

//  // out_ecl1_8:  .c2s14/in8_15;       // missing
//  out_ecl9_16: ;//.c2s1/in1_8;
// }

// GG8000(r6c1s11)
// {
//   SERIAL("7665");
// }
// GG8000_U(r6c1s11u6)
// {
//  in:    .s13u2/delayed;
//  // out2:  .s23/in1;
// }
 
// GG8000_U(r6c1s11u7)
// {
//  in:   .s17u4/out1;
//  out2: .s13u1/start;
// }

// GG8000_U(r6c1s11u8)
// {
//  in:   .s17u4/out2;
//  out2: .s7u4/inC;
// }

// LRS222(r6c1s13)
// {
//   SERIAL("3566");
// }

// LRS222_U(r6c1s13u1)
// {
//  start:   .s11u7/out2;
//  stop:    .s3u4/out3;

//  out:     .s3u3/in1;
// }

// LRS222_U(r6c1s13u2)
// {
//  start:   .s17u4/out4;
//  delayed: .s11u6/in;
// } 
// RD2000(r6c1s15)
// {
//   SERIAL("6163");
// }

// RD2000_U(r6c1s15u1)
// {
//   in:     .s17u3/out1;
//   divn_1: .s15u2/in;
// }

// RD2000_U(r6c1s15u2)
// {
//  in:      .s15u1/divn_1;
//  divn_1:  .s9/in9;       // trigger 1
// }

// LF4000(r6c1s17)
// {
//   ;
// }

// LF4000_4_U(r6c1s17u1)
// {
//  in1:    .s19/stop;
//  in2:    .s19/start;

//  out1:   .s7u2/inA;
// }

// LF4000_4_U(r6c1s17u2)
// {
//   ;
// }

// LF4000_4_U(r6c1s17u3)
// {
//  in1:    .s3u4/out1;
//  out1:   .s15u1/in;
//  // out4:   .s23/in2;

// }

// LF4000_4_U(r6c1s17u4)
// {
//  in1:    .s7u4/out2;

//  out1:   .s11u7/in;
//  out2:   .s11u8/in;
//  out3:   ;//.c2s8/common_start;
//  out4:   .s13u2/start;
// }

// ORTEC462(r6c1s19)
// {
//   SERIAL("2046/47");

//  start:  .s17u1/in2;
//  stop:   .s17u1/in1;
// }

// //EC8000(r6c1s23)
// //{
// // in1:  .s11u6/out2;
// // in2:  .s17u3/out4;
// //}

// /***
//  * CAMAC Crate moved to r1c0...
//  *

// CAMAC_CRATE(r6c2)
// {
//   ;
// }

// CAMAC_TRIGGER(r6c2s1)
// {
//   SERIAL("8115");

//  in1_8:   .c1s9/out_ecl9_16;
//  out6:    .c1s9/in_ecl6;  // former out5
// }

// LECROY2228A(r6c2s8)
// {
//  common_start: .c1s17u4/out3;
 
//  stop0:        .c1s3u4/out4;
// }

// IOL_BOX(r6c2s11)
// {
//   SERIAL("0750");
// }

// //SC4800(r6c2s14)
// //{
// //  in8_15:      .c1s9/out_ecl1_8;
// //}

// CVC4(r6c2s24)
// {
//   SETTING("HOSTNAME"=>"CVC12");
//   SETTING("CRATE_NO"=>"0");
// }
// //copied from spill_timing.hh
// */

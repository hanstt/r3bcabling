######################################################################################
#		FILE TO GENERATE THE LAND PLANES WITH TACQUILA
#				CL MARCH 2012
######################################################################################

#/usr/bin/perl

use warnings;

my $PLANE;
my $PADDLE;
my $PMT;

my $INCRATE;
my $INPUT = 8;

my $TEXTBUFFER;

my $FILENAME;

my $JOINER;

my $TACCRATE;
my $EVEN;
my @TACSLOT;
@TACSLOT = (1,3,6,8,1,3,6,8,1,3);

for($PLANE = 1; $PLANE <= 10; $PLANE++) {
  $INPUT = 8;
  $FILENAME = sprintf(">land/plane_%d.hh",$PLANE);
  open(FILEOUT, $FILENAME) || die "$FILENAME does not work!!!\n";
  print "*** Generating plane $PLANE in $FILENAME... *** \n";
  print FILEOUT "///////////////////// FILE AUTO-GENERATED WITH gen_planes.pl\n";
  $INCRATE = 21;
  for($PADDLE = 1; $PADDLE <= 20; $PADDLE++) {
    for($PMT = 1; $PMT <= 2; $PMT++) {
       $TEXTBUFFER = sprintf('DETECTOR_LAND(rNP%dc%02ds%d) { LABEL("N%02d_%02d_%d"); signal: rNP%dc%d/in%d;}',$PLANE,$PADDLE,$PMT,$PLANE,$PADDLE,$PMT,$PLANE,$INCRATE,$INPUT);
       $INPUT--;
       if($INPUT == 0) {
	 $INPUT = 8;
	 $INCRATE++;
       }
       print FILEOUT "$TEXTBUFFER\n";
  }
 }

#### WRITING THE JOINER PART

 print FILEOUT "\n\n////// JOINER8 PART --- auto-generated \n\n";

 $INCRATE = 21;
 $INPUT = 8;
 
 if($PLANE > 0 && $PLANE < 5) 
 {
   $TACCRATE = 1;
 }
 elsif ($PLANE > 4 && $PLANE < 9)
 {
   $TACCRATE = 2;
 }
 elsif ($PLANE > 8)
 {
   $TACCRATE = 3;
 }

 $EVEN = (($PLANE % 2) == 0) ? 1 : 0;
 $REALEVEN = (($PLANE % 2) == 1) ? 1 : 0;
 $MYTACSLOT = ($REALEVEN == 1) ? $TACSLOT[$PLANE - 1] - 1 : $TACSLOT[$PLANE - 1];
 
 for($JOINER = 1; $JOINER <= 5; $JOINER++)
 {
    $TEXTBUFFER = sprintf("JOINER_8(rNP%dc%d)\n{\n", $PLANE,$INCRATE);
    print FILEOUT "$TEXTBUFFER";
 
    $PMT = 2;
    $CRATE = $JOINER * 4; 
    
    for(my $COUNT = 1; $COUNT <= 8; $COUNT++)
    {
      $TEXTBUFFER = sprintf("in%d: rNP%dc%02ds%d/signal;\n", $COUNT, $PLANE, $CRATE, $PMT); 
      $PMT = ($PMT == 1) ? 2 : 1;
      $CRATE = ($PMT == 2) ? $CRATE-1 : $CRATE;    
      print FILEOUT "$TEXTBUFFER"; 
    }

    $INPUT = ($EVEN == 0) ? 8 : 16;
    $MYTACSLOT = ($REALEVEN == 1) ? $MYTACSLOT=$MYTACSLOT+1 : $MYTACSLOT;
    
    print FILEOUT "\n";
    
    for(my $COUNT = 1; $COUNT <= 8; $COUNT++)
    {
      $TEXTBUFFER = sprintf("out%d: rRLANDTACQc%ds%du1/in%d;\n", $COUNT, $TACCRATE, $MYTACSLOT, $INPUT); 
      $INPUT--;
      print FILEOUT "$TEXTBUFFER"; 
    }
 
    $EVEN = ($EVEN == 1) ? 0 : 1;
    $REALEVEN = ($EVEN == 1) ? 0 : 1;

    print FILEOUT "}\n\n";
    $INCRATE++;
 }
 close(FILEOUT);
}


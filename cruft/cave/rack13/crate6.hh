//This is the splitter for POS1 and ROLU1

//FW: copied from the CVS version, messhuette, rack5, terminal_crates1_8.hh, modified to r13c6:

//r4 needs to be renamed...

//TERMINAL(r5c1)
TERMINAL(r13c6)
{
  // LABEL("POS 1 and ROLU 1");
  
 //in1_8: <- "KABEL 4 (25-32)" , rBEAMR1c2/out1_8;
 in1_8: <- "KABEL 0 110ns" , rBEAMR1c2/out1_8;

 // Energies to digitisation.
 //check this: 
 //e1_8:  r4c3s6(QDC)/in0_7;
 e1_4:  r14c5/in1_4;
 e5_8:  r14c5/in18_21;

 // Times.
 //check this: 
 /* POS01 */
 //t1:  r13c6s2/in1;//r4c8s11u1/in;
 //t2:  r13c6s2/in2;
 //t3:  r13c6s2/in3;//r4c8s11u1/in;
 //t4:  r13c6s2/in4;
 /* ROLU 1 */
 //t5_8:  r13c6s2(CAPACITOR_BOX)/in5_8;
  t1_8: r14c6s1/in1_8;	//POS and ROLU	
}

CAPACITOR_BOX(r13c6s2)
{
 //FW: this also has changed. Taken out temporarily until checked.
 // POS01
 /*
 in1: r13c6/t1;
 in2: r13c6/t2;
 in3: r13c6/t3;
 in4: r13c6/t4;
 
 out1: r4c8s7u1/in;
 out2: r4c8s7u2/in; //broken unit 2 
 out3: r4c8s7u3/in; 
 out4: r4c8s7u4/in;

 // ROLU 1 
 in5_8: r13c6/t5_8;

 out5:  "5" ->       , r4c8s23u5(CFD8000)/in;
 out6:      -> "2"   , r4c8s23u6(CFD8000)/in;
 out7:                 r4c8s23u7(CFD8000)/in;
 out8:                 r4c8s23u8(CFD8000)/in;
 */
 ;
}


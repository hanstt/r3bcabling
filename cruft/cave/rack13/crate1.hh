CABLE_DELAY_16(r13c1)
{


  SETTING("DELAY" => "150 ns");
// TFW signal 28_1 to 28_2
  in1:  r11c5s1/e7;
  out1: r13c1/in2;
  in2:  r13c1/out1;
  out2: r13c1/in3;
  in3:  r13c1/out2;
  out3: r15c4s5/in22;
  
  in4:  r11c5s1/e8;
  out4: r13c1/in5;
  in5:  r13c1/out4;
  out5: r13c1/in6;
  in6:  r13c1/out5;
  out6: r15c4s5/in23;

  //Pixel
  in7_9: r13c8/e1_3;
  out7_9: r14c2s22/in13_15;
}

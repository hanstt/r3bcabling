//This is the red 48 patch panel at the bottom of rack13

//FW: copied from the CVS version, cave, rack13, crate11 (cave_patch.hh), changed to crate10
//The connections are probably completely wrong and need to be checked carefully:

//PATCH_PANEL_48_new(r13c11)
PATCH_PANEL_48_new(r13c10)
{
  /*
	           // LABELS
 front1:      .c4s19u1/out3; // MLAND
 front2:      .c4s19u7/out2; // MVETO
 front3:      .c4s19u3/out4; // MTOF
 front4:      ; // TODO: HW trig to DCH
 front5:      r14c6s18/analog_out; // STDET
 front6:      .c4s17u1/out16; // ORTDET
 front7:      ; // TODO: HW trig to DCH -TR1
 front8:      r14c9s7u1/outl6; // GATE LAND
 front9:      r14c9s7u1/outl4; // GATE TDET
 front10:     r51c25/out7; // GFI MON 1
 front11:     r53c29/out7; // GFI MON 2
 front12:     ; // GFI MON 3

 back1_12: r2c11/back25_36;					
					
 front13:     ; // GFI Gate
 front14:     r14c7s19/out4; // MUX T
 front15:     r14c7s19/out5; // MUX E
 front16:     r14c7s19/out6; // MUX WLK
 front17:     rGFI1c1s1u35/signal; // GFI 1 T
 front18:     rGFI2c1s1u35/signal; // GFI 2 T
 front19:     r12c11s15/out15; // TODO: HW trig to DCH -TR2 20080714:DT from R2
 front20:     r12c11s15/out14; // TODO: HW trig to DCH -TR3
 front21:     .c4s1u1/in1; 
 front22:     r12c11s15/in1; // SPILL ON
 front23:     r12c11s15/in2; // SPILL OFF
 front24:     r14c9s13u3/in1; // TDC STOP

 back13_24: r2c11/back37_48;					
					
 front25:     ;
 front26:     ;
 front27:     r14c7s8/or;
 front28:     ;
 front29:     ;
 front30:     ;
 front31:     ;
 front32:     ;
 front33:     ;
 front34:     ;
 front35:     ;
 front36:     ;

 back25_36: ;					
					
 front37:     ;
 front38:     ;
 front39:     ;
 front40:     ;
 front41:     ;
 front42:     ;
 front43:     ;
 front44:     ;
 front45:     ;
 front46:     .c4s19u1/out2; // MLAND, too 
 front47:     r14c9s13u1/in1; // GATE MH
 front48:     ; 

 back37_48: ;					
*/					
;}

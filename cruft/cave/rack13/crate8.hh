//This is the splitter for PIX 1,2,3 and Klaus 1,2

//FW: copied from the CVS version, messhuette, rack5, terminal_crates1_8.hh, modified to r13c6:

//TERMINAL(r5c3)
TERMINAL(r13c8)
{
/*
 in1: "A4"   <- "KABEL 7 (56)", rBEAMR1c1/out8;
 in2: "KABEL 9 (8)"    <-     , rBEAMR1c1/out16;
 in3: "B2"   <- "KABEL 5 (40)", rBEAMR1c1/out24;
*/
 in1: "KABEL1 (1)"   <- "KABEL1 (1)", rBEAMR1c1/out8;
 in2: "KABEL1 (2)"   <- "KABEL1 (2)", rBEAMR1c1/out16;
 in3: "KABEL1 (3)"   <- "KABEL1 (3)", rBEAMR1c1/out24;

  /* STR (pixel) */
 //FW: needs to be fixed.
 //e1:   r4c3s23/in1;
 //e2:   r4c3s23/in0;
 //e3:   r4c3s23/in2;
 //e4:   r4c3s23/in3;

 //t1_2:   r13c8s2/in1_2;
 
 e1:   r13c1/in7;
 e2:   r13c1/in8;
 e3:   r13c1/in9;
}

CAPACITOR_BOX(r13c8s2)
{
  //FW: needs to be checked - until then taken out.
 //in1_2:    r13c8/t1_2;
 //// in3_6:    r4c11/out9_12;

 //// out1:     r4c8s11u6(CFD8000)/in;
 //out1:     r4c8s9u7/in;
 //out2:     r4c8s11u5/in;
 ////out3:     r4c8s9u7/in;
 //// out3_6:   r4c3s13/in1_4;

 //in8:      r13c9/t2;
 //out8:     r4c8s23u2/in;
  ;
}


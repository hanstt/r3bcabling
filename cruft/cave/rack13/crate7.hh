//This is the splitter for SCI 1&2 (S2 & S8) and ROLU2

//FW: copied from the CVS version, messhuette, rack5, terminal_crates1_8.hh, modified to r13c7:

//TERMINAL(r5c2)
TERMINAL(r13c7)
{
 // LABEL("SCI 1 and 2 and ROLU 2");
  
 //in1:   "1"  <- "1"  , rSCI1c1s1u1/signal;
 //in2:   "2"  <- "1"  , rSCI1c1s1u2/signal;
 //in3:   "13" <- "13" , rSCI2c1s1u1/signal;
 //in4:   "14" <- "14" , rSCI2c1s1u2/signal; 

 //in5_8: <- "KABEL 1 (1-4)" , rBEAMR1c2/out17_20;

  /* Energies to digitalisation. */
 //e1_4:  r4c3s14/in0_3; 
 //e5_8:  r4c3s14/in4_7;

  /* Times. */

  /* SCI 1 & 2 */
 //t1_4:  r4c9s21u2_5/in;

  /* ROLU 2 */
 //t5_8:  r13c7s2(CAPACITOR_BOX)/in5_8;
;}

CAPACITOR_BOX(r13c7s2)
{
  /* POS01 */



  /* ROLU 2 */
  
  //in5_8: r13c7/t5_8;

  //out5:                 r4c9s23u5(CFD8000)/in;
  //out6:                 r4c9s23u6(CFD8000)/in;
  //out7:                 r4c9s23u7(CFD8000)/in;
  //out8:                 r4c9s23u8(CFD8000)/in;
;}


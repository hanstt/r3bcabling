#include "gfi.hh"

/* GFI in ALADIN
 * Naming scheme for GFI 1 detector:
 *
 * DETECTOR_GFI ('the detector', r50c1), XXX is the crystal number.
 * DETECTOR_GFI_PM ('the timing PM', r50c1s2)
 * DETECTOR_GFI_PSPM ('the position sensitive PM',r50c1s1u1)
 * DETECTOR_GFI_PSPM_U ('one channel from PSPM',r50c1s1u1)
 *
 * Each cable that goes to fastbus has on GFI end a shorter cable
 * that goes from lemo to 8-fold connector.  This connection is a joiner 8.
 * At the other end is another joiner 8 before it goes to paddle card into QDC.
 *
 * rGFI1c1 is the detector.
 * rGFI1c2 is the vacuum-flange.
 *
 * r51c1  is the crate (below the flange)
 * r51c1x are the cable joiners close to the rack
 * r51c2x are the cable joiners close to the fastbus
 */

// GFI 1 /******************************************************/

DETECTOR_GFI(rGFI1c1)
{
  ;
}

DETECTOR_GFI_PSPM(rGFI1c1s1)
{
  ;
}

DETECTOR_GFI_PSPM_U(rGFI1c1s1u1)  { LABEL("GFI01_01"); signal: rGFI1c2/x_in1; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u2)  { LABEL("GFI01_02"); signal: rGFI1c2/x_in2; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u3)  { LABEL("GFI01_03"); signal: rGFI1c2/x_in3; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u4)  { LABEL("GFI01_04"); signal: rGFI1c2/x_in4; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u5)  { LABEL("GFI01_05"); signal: rGFI1c2/x_in5; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u6)  { LABEL("GFI01_06"); signal: rGFI1c2/x_in6; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u7)  { LABEL("GFI01_07"); signal: rGFI1c2/x_in7; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u8)  { LABEL("GFI01_08"); signal: rGFI1c2/x_in8; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u9)  { LABEL("GFI01_09"); signal: rGFI1c2/x_in9; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u10) { LABEL("GFI01_10"); signal: rGFI1c2/x_in10; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u11) { LABEL("GFI01_11"); signal: rGFI1c2/x_in11; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u12) { LABEL("GFI01_12"); signal: rGFI1c2/x_in12; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u13) { LABEL("GFI01_13"); signal: rGFI1c2/x_in13; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u14) { LABEL("GFI01_14"); signal: rGFI1c2/x_in14; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u15) { LABEL("GFI01_15"); signal: rGFI1c2/x_in15; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u16) { LABEL("GFI01_16"); signal: rGFI1c2/x_in16; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u17) { LABEL("GFI01_17"); signal: rGFI1c2/x_in17; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u18) { LABEL("GFI01_18"); signal: rGFI1c2/x_in18; }

DETECTOR_GFI_PSPM_U(rGFI1c1s1u19) { LABEL("GFI01_19"); signal: rGFI1c2/y_in1; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u20) { LABEL("GFI01_20"); signal: rGFI1c2/y_in2; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u21) { LABEL("GFI01_21"); signal: rGFI1c2/y_in3; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u22) { LABEL("GFI01_22"); signal: rGFI1c2/y_in4; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u23) { LABEL("GFI01_23"); signal: rGFI1c2/y_in5; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u24) { LABEL("GFI01_24"); signal: rGFI1c2/y_in6; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u25) { LABEL("GFI01_25"); signal: rGFI1c2/y_in7; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u26) { LABEL("GFI01_26"); signal: rGFI1c2/y_in8; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u27) { LABEL("GFI01_27"); signal: rGFI1c2/y_in9; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u28) { LABEL("GFI01_28"); signal: rGFI1c2/y_in10; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u29) { LABEL("GFI01_29"); signal: rGFI1c2/y_in11; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u30) { LABEL("GFI01_30"); signal: rGFI1c2/y_in12; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u31) { LABEL("GFI01_31"); signal: rGFI1c2/y_in13; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u32) { LABEL("GFI01_32"); signal: rGFI1c2/y_in14; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u33) { LABEL("GFI01_33"); signal: rGFI1c2/y_in15; }
DETECTOR_GFI_PSPM_U(rGFI1c1s1u34) { LABEL("GFI01_34"); signal: rGFI1c2/y_in16; }

VACUUM_FLANGE36(rGFI1c2)
{
 x_in1_18: rGFI1c1s1u1_18/signal;
 y_in1_16: rGFI1c1s1u19_34/signal;
 
 x_out1:   r51c1s9/in1;
 x_out2:   r51c1s9/in3;
 x_out3:   r51c1s9/in5;
 x_out4:   r51c1s9/in7;
 x_out5:   r51c1s9/in9;
 x_out6:   r51c1s9/in11;
 x_out7:   r51c1s9/in13;
 x_out8:   r51c1s9/in15;
 x_out9:   r51c1s9/in17;
 x_out10:  r51c1s9/in19;
 x_out11:  r51c1s9/in21;
 x_out12:  r51c1s9/in23;
 x_out13:  r51c1s9/in25;
 x_out14:  r51c1s9/in27;
 x_out15:  r51c1s9/in29;
 
 x_out16:  r51c1s4/in10;
 x_out17:  r51c1s4/in1;
 x_out18:  r51c1s4/in2;

 y_out1:   r51c1s9/in6;
 y_out2:   r51c1s9/in8;
 y_out3:   r51c1s9/in31;
 y_out4:   r51c1s9/in12;
 y_out5:   r51c1s9/in14;
 y_out6:   r51c1s9/in16;
 y_out7:   r51c1s9/in18;
 y_out8:   r51c1s9/in20;
 y_out9:   r51c1s9/in22;
 y_out10:  r51c1s9/in24;
 y_out11:  r51c1s9/in26;
 y_out12:  r51c1s9/in28;
 y_out13:  r51c1s9/in30;
 y_out14:  r51c1s9/in32;	  
 y_out15:  r51c1s9/in2;  
 y_out16:  r51c1s9/in4;

}

 // done here. cables from flange to modules.






// GFI 2      /*************************************************/

DETECTOR_GFI(rGFI2c1)
{
  ;
}

DETECTOR_GFI_PSPM(rGFI2c1s1)
{
  ;
}
// x
DETECTOR_GFI_PSPM_U(rGFI2c1s1u1)  { LABEL("GFI02_01"); signal: r53c1s1/in1; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u2)  { LABEL("GFI02_02"); signal: r53c1s1/in2; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u3)  { LABEL("GFI02_03"); signal: r53c1s1/in3; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u4)  { LABEL("GFI02_04"); signal: r53c1s1/in4; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u5)  { LABEL("GFI02_05"); signal: r53c1s1/in5; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u6)  { LABEL("GFI02_06"); signal: r53c1s1/in6; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u7)  { LABEL("GFI02_07"); signal: r53c1s1/in7; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u8)  { LABEL("GFI02_08"); signal: r53c1s1/in8; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u9)  { LABEL("GFI02_09"); signal: r53c1s6/in9; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u10) { LABEL("GFI02_10"); signal: r53c1s1/in10; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u11) { LABEL("GFI02_11"); signal: r53c1s1/in11; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u12) { LABEL("GFI02_12"); signal: r53c1s1/in12; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u13) { LABEL("GFI02_13"); signal: r53c1s1/in13; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u14) { LABEL("GFI02_14"); signal: r53c1s1/in14; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u15) { LABEL("GFI02_15"); signal: r53c1s1/in15; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u16) { LABEL("GFI02_16"); signal: r53c1s1/in16; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u17) { LABEL("GFI02_17"); signal: r53c1s6/in5; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u18) { LABEL("GFI02_18"); signal: r53c1s6/in6; }
// y 
DETECTOR_GFI_PSPM_U(rGFI2c1s1u19) { LABEL("GFI02_19"); signal: r53c1s1/in19; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u20) { LABEL("GFI02_20"); signal: r53c1s1/in20; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u21) { LABEL("GFI02_21"); signal: r53c1s1/in21; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u22) { LABEL("GFI02_22"); signal: r53c1s1/in22; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u23) { LABEL("GFI02_23"); signal: r53c1s1/in23; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u24) { LABEL("GFI02_24"); signal: r53c1s1/in24; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u25) { LABEL("GFI02_25"); signal: r53c1s1/in25; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u26) { LABEL("GFI02_26"); signal: r53c1s1/in26; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u27) { LABEL("GFI02_27"); signal: r53c1s1/in27; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u28) { LABEL("GFI02_28"); signal: r53c1s1/in28; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u29) { LABEL("GFI02_29"); signal: r53c1s1/in29; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u30) { LABEL("GFI02_30"); signal: r53c1s1/in30; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u31) { LABEL("GFI02_31"); signal: r53c1s1/in31; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u32) { LABEL("GFI02_32"); signal: r53c1s1/in32; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u33) { LABEL("GFI02_33"); signal: r53c1s1/in17; }
DETECTOR_GFI_PSPM_U(rGFI2c1s1u34) { LABEL("GFI02_34"); signal: r53c1s1/in18; }



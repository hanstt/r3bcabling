/*
 * r54 CRATE 2 
 *
 * NIM Crate mit logic units und GG
 *
 *
 *
 *
 */
 
 NIM_CRATE(r54c2)
 {
 ;
 }
 
 CLOCK_MSE(r54c2s1)
 {
 out_10Hz: r54c2s4u8/in_ttl;
 }
 
 LF4000(r54c2s2)
 {
 ;
 }
 
 LF4000_4_U(r54c2s2u1)
 {
 in1: r54c2s5u1/out1; 
 in3: r54c2s2u2/out1;
 in4: r54c2s2u3/out1;
 }
 
 LF4000_4_U(r54c2s2u2)
 {
 in1: r54c2s8u1/outA;
 out1: r54c2s2u1/in3; 
 out2:  r54c1s7/nim_in1;
// out3:rMASTERc3s6u1/out3;

 }
 
  LF4000_4_U(r54c2s2u3)
 {
 in1: r54c2s8u2/outA;
 
 out1: r54c2s2u1/in4; 
 out2: r54c1s7/nim_in2;
// out3: rMASTERc3s2/in7;
// out4:  rMASTERc3s2/in6;
}
 
  LF4000_4_U(r54c2s2u4)
 {
 in1: r54c1s7/nim_out9;
 in2: r54c1s7/nim_out10;
 in3: r54c1s7/nim_out11;
 in4: r54c1s7/nim_out12;
 
 //out1: leer
 }
 
 
 GG8000(r54c2s3)
 {
 ;
 }
 GG8000_U(r54c2s3u1)
 {
 in:   r54c2s5u2/out1;
 out1: r54c1s9/in1;
 //out2: irgendwas auf dem dack von rack 55, definie as crate0
// levcon in 8
 
 }
 
 GG8000_U(r54c2s3u8)
 {
 in:   r54c2s4u8/out_nim;
 out1:  r54c2s4u7/in_nim; 
 }
 
 LA8010(r54c2s4)
 {
 ;
 }
 
 LA8010_U(r54c2s4u6)
 {
 in_nim:r54c2s4u7/out_nim ;
 //out_ttl: 
 out_nim: r54c2s5u1/in1;
 
 }


 LA8010_U(r54c2s4u7)
 {
 in_nim: r54c2s3u8/out1;
 //out_ttl: pulser: triplex interface in r13. modul liegt auf nem anderen , pulser in
 out_nim: r54c2s4u6/in_nim;
 
 }
 
  LA8010_U(r54c2s4u8)
 {
 in_ttl:r54c2s1/out_10Hz ;
 out_nim: r54c2s3u8/in;
 
 }



LF4000(r54c2s5)
{
;
}

LF4000_4_U(r54c2s5u1)
{
in1: r54c2s4u6/out_nim;
out1: r54c2s2u1/in1;
out2: r54c1s7/nim_in4;
}

LF4000_4_U(r54c2s5u2)
{
//in1: nach RMASTER-> leer
out1: r54c2s3u1/in;
}


LF4000_4_U(r54c2s5u4)
{
;
in1: r54c2s10u5/out1;
out1: r54c2s10u1/gate_in1;// oben oberster abschnitt des modules;
out2: r54c2s10u3/gate_in1; // oben dritter abschnitt des modules;
}

CF8000(r54c2s8)
{
;
}

CF8000_U(r54c2s8u1)
{
in: r54c2s9u1/out2; 
outB1: r54c2s10u2/nim_in;//zweiter abschnitt des modules
outA: r54c2s2u2/in1;
}

CF8000_U(r54c2s8u2)
{
in: r54c2s9u1/out4; 
outB1: r54c2s10u3/nim_in; //dritter abschnitt des modules
outA: r54c2s2u3/in1;
}

LECROY428F(r54c2s9)
{
;
}

LECROY428F_U(r54c2s9u1)
{
//in1: von r13c0
out2: r54c2s8u1/in;
out4: r54c2s8u2/in;
}


CAEN_N145(r54c2s10)
{
;
}

CAEN_N145_U(r54c2s10u1)
{
//ttl_in: clock von dach r13 levcon out1
gate_in1: r54c2s5u4/out1;
gate_in2: r54c2s10u2/gate_in1; //gate oben in abschnitt 2 des modules
}

CAEN_N145_U(r54c2s10u2)
{

nim_in: r54c2s8u1/outB1;
gate_in1: r54c2s10u1/gate_in2;
}

CAEN_N145_U(r54c2s10u3)
{
nim_in: r54c2s8u2/outB1;

gate_in1: r54c2s5u4/out2;
gate_in2: r54c2s10u4/gate_in1;
}

CAEN_N145_U(r54c2s10u4)
{

//ttl_in: dach triplex interface or
gate_in1: r54c2s10u3/gate_in2;
}

CAEN_N145_U(r54c2s10u5)
{
out1: r54c2s5u4/in1;
}


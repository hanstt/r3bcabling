/*
 * rack 54 CRATE 1 LAND
 * 
 * VME Crate with Triva, SAM etc...
 *
 *
 *
 */
FOUR_POST_RACK(r54)
{
  LABEL("LAND VME and NIM Crate for Tacquila readout");
}
 
VME_CRATE(r54c1)
 {
 ;
 }

RIO3(r54c1s1)
{SETTING("HOSTNAME"=>"r3-52");
} 


TRIVA7(r54c1s5)
{

//tribu1: rMASTERc2s2/tribu1; //raus zu masterrack
//tribu2: platine
in1_8: r54c1s11/io_ecl1_8;
out1_8: r54c1s11/io_ecl9_16;
}

ENV1(r54c1s7)
{
in_ecl1_8: r54c1s11/out_ecl1_8;
out_ecl1_8: r54c1s11/in_ecl1_8; // ecl nim converter
 

nim_in1: r54c2s2u2/out2;
nim_in2: r54c2s2u3/out2;
nim_in4: r54c2s5u1/out2;

nim_out9:  r54c2s2u4/in1;
nim_out10: r54c2s2u4/in2;
nim_out11: r54c2s2u4/in3;
nim_out12: r54c2s2u4/in4;
}


TRIDI(r54c1s9)
{
out_ecl1_8: r54c1s13/in_ecl1_8;// geht auf beide Module
outB_ecl1_8: r54c1s15/in_ecl1_8;
in1: r54c2s3u1/out1;
}



VULOM1(r54c1s11)
{ 
in_ecl1_8: r54c1s7/out_ecl1_8;
io_ecl1_8: r54c1s5/in1_8;
io_ecl9_16: r54c1s5/out1_8;
out_ecl1_8: r54c1s7/in_ecl1_8;
}


SAM4(r54c1s13)
{
  SETTING("SAM_NO" => "5");
in_ecl1_8: r54c1s9/out_ecl1_8;
//GTB1: land signalboxen?? untereste der drei
gtb1: rRLANDTACQc3s5/gtb_master;
}

SAM5(r54c1s15)
{
  SETTING("SAM_NO" => "6");
in_ecl1_8: r54c1s9/outB_ecl1_8;
//GTB1 TAC up
//GTB2 TAC middle
gtb1: rRLANDTACQc1s10/gtb_master;
gtb2: rRLANDTACQc2s10/gtb_master;
}

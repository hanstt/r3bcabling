MODULE(DETECTOR_POS)
{
  LABEL("POS detector");
}

UNIT(DETECTOR_POS_U)
{
  CONNECTOR(signal);
  CONNECTOR(hv);
}

MODULE(DETECTOR_ROLU)
{
  LABEL("ROLU detector");
}

UNIT(DETECTOR_ROLU_U)
{
  CONNECTOR(signal);
  CONNECTOR(hv);
}


/* POS1 rPOS1c1s1  */

DETECTOR_POS(rPOS1c1s1)
{
  ;
}

DETECTOR_POS_U(rPOS1c1s1u1) { LABEL("POS01_01"); signal: r47c1s5/in1;  }
DETECTOR_POS_U(rPOS1c1s1u2) { LABEL("POS01_02"); signal: r47c1s5/in9;  }
DETECTOR_POS_U(rPOS1c1s1u3) { LABEL("POS01_03"); signal: r47c1s5/in2;  }
DETECTOR_POS_U(rPOS1c1s1u4) { LABEL("POS01_04"); signal: r47c1s5/in10;  }

/* POS2 rPOS2c1s1  */

DETECTOR_POS(rPOS2c1s1)
{
  ;
}

DETECTOR_POS_U(rPOS2c1s1u1) { LABEL("POS02_01"); signal: r47c1s5/in3;  }
DETECTOR_POS_U(rPOS2c1s1u2) { LABEL("POS02_02"); signal: r47c1s5/in11;  }
DETECTOR_POS_U(rPOS2c1s1u3) { LABEL("POS02_03"); signal: r47c1s5/in4;  }
DETECTOR_POS_U(rPOS2c1s1u4) { LABEL("POS02_04"); signal: r47c1s5/in12;  }





/* ROLU1 rROLU1c1s1 */

DETECTOR_ROLU(rROLU1c1s1)
{
  ;
}

DETECTOR_ROLU_U(rROLU1c1s1u1) { LABEL("ROL01_01"); signal: r47c1s5/in5; } //hv: r10c3s11/out4;  }
DETECTOR_ROLU_U(rROLU1c1s1u2) { LABEL("ROL01_02"); signal: r47c1s5/in13; } //hv: r10c3s11/out7;  }
DETECTOR_ROLU_U(rROLU1c1s1u3) { LABEL("ROL01_03"); signal: r47c1s5/in6; } //hv: r10c3s11/out9;  }
DETECTOR_ROLU_U(rROLU1c1s1u4) { LABEL("ROL01_04"); signal: r47c1s5/in14; } //hv: r10c3s11/out10;  }

/* ROLU2 rROLU2c1s1 */

DETECTOR_ROLU(rROLU2c1s1)
{
  ;
}

DETECTOR_ROLU_U(rROLU2c1s1u1) { LABEL("ROL02_01"); signal: r47c1s5/in7; } //hv: r10c3s11/out11;  }
DETECTOR_ROLU_U(rROLU2c1s1u2) { LABEL("ROL02_02"); signal: r47c1s5/in15; } //hv: r10c3s11/out12;  }
DETECTOR_ROLU_U(rROLU2c1s1u3) { LABEL("ROL02_03"); signal: r47c1s5/in8; } //hv: r10c3s11/out13;  }
DETECTOR_ROLU_U(rROLU2c1s1u4) { LABEL("ROL02_04"); signal: r47c1s5/in16; } //hv: r10c3s11/out14;  }

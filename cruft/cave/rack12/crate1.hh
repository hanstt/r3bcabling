// TFW signal 25_2 to 27_2
CABLE_DELAY_16(r12c1)
{


  SETTING("DELAY" => "150 ns");

  in1:  r11c5s1/e2;
  out1: r12c1/in2;
  in2:  r12c1/out1;
  out2: r12c1/in3;
  in3:  r12c1/out2;
  out3: r15c4s5/in17;
  
  in4:  r11c5s1/e3;
  out4: r12c1/in5;
  in5:  r12c1/out4;
  out5: r12c1/in6;
  in6:  r12c1/out5;
  out6: r15c4s5/in18;


  in7:  r11c5s1/e4;
  out7: r12c1/in8;
  in8:  r12c1/out7;
  out8: r12c1/in9;
  in9:  r12c1/out8;
  out9: r15c4s5/in19;

  in10:  r11c5s1/e5;
  out10: r12c1/in11;
  in11:  r12c1/out10;
  out11: r12c1/in12;
  in12:  r12c1/out11;
  out12: r15c4s5/in20;
  
  in13:  r11c5s1/e6;
  out13: r12c1/in14;
  in14:  r12c1/out13;
  out14: r12c1/in15;
  in15:  r12c1/out14;
  out15: r15c4s5/in21;
}

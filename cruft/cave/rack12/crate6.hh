/* 
 * r12 CRATE 6
 * 
 * CAMAC Crate Adress Nr. 3 with CFD, LIFO, Delay...
 *
 */
 
 CAMAC_CRATE(r12c6)
 {
 ;
 }
 //TFW1
 CF8103(r12c6s2)
 {
 in1_8: r11c4s1/t1_8;
// m: r52c2s10/in5;
 mux_tb: r12c6s18/in2c;
 mux_e: r12c6s18/in6c;
 mux_mon: r12c6s18/in10c;
 test: r12c6s16/out5;
 tb1_8: r12c6s10/in1_8;
 }
 
 //TFW2
 CF8103(r12c6s4)
 {
 in1_8: r11c4s2/t1_8;
// m: r52c2s10/in6;
 mux_tb: r12c6s18/in2d;
 mux_e: r12c6s18/in6d;
 mux_mon: r12c6s18/in10d;
 test: r12c6s16/out6;
 tb1_8: r12c6s10/in9_16;
 }
 //TFW3 
 CF8103(r12c6s6)
 {
 in1_8: r11c4s3/t1_8;
// m: r52c2s10/in7;
 mux_tb: r12c6s18/in3a;
 mux_e: r12c6s18/in7a;
 mux_mon: r12c6s18/in11a;
 test: r12c6s16/out7;
 tb1_8: r12c6s12/in1_8;
 }
 //TFW4 
 CF8103(r12c6s8)
 {
 in1_8: r11c4s4/t1_8;
// m: r52c2s10/in8;
 mux_tb: r12c6s18/in3b;
 mux_e: r12c6s18/in7b;
 mux_mon: r12c6s18/in11b;
 test: r12c6s16/out8;
 tb1_8: r12c6s12/in9_16;
 }

DL1600(r12c6s10)
{
in1_8:     r12c6s2/tb1_8;
in9_16:    r12c6s4/tb1_8;
outa1_16:  r15c4s6/in0_15;
outb1_8:   r12c6s14/in16_23;
outb9_16:  r12c6s14/in24_31;
}

DL1600(r12c6s12)
{
in1_8:     r12c6s6/tb1_8;
in9_16:    r12c6s8/tb1_8;
outa1_16:  r15c4s6/in16_31;
outb1_8:   r12c6s14/in32_39;
outb9_16:  r12c6s14/in40_47;
}

SC4800(r12c6s14)
{

in16_23:  r12c6s10/outb1_8;
in24_31:  r12c6s10/outb9_16;
in32_39:  r12c6s12/outb1_8;
in40_47:  r12c6s12/outb9_16;
}

LIFO(r12c6s16)
{
//in:   r52c2s1u4/out2; 
out1: r12c5s1/test; 
out2: r12c5s3/test; 
out3: r12c5s5/test;
out4: r12c5s7/test;
out5: r12c6s2/test;
out6: r12c6s4/test;
out7: r12c6s6/test;
out8: r12c6s8/test;
}


RMX3_2(r12c6s18)
{
in2c:  r12c6s2/mux_tb;
in2d:  r12c6s4/mux_tb;
in3a:  r12c6s6/mux_tb;
in3b:  r12c6s8/mux_tb;
in6c:  r12c6s2/mux_e;
in6d:  r12c6s4/mux_e;
in7a:  r12c6s6/mux_e;
in7b:  r12c6s8/mux_e;
in10c: r12c6s2/mux_mon;
in10d: r12c6s4/mux_mon;
in11a: r12c6s6/mux_mon;
in11b: r12c6s8/mux_mon;

out1_12: r12c6s20/in1a_3d;
}

RMX3_2(r12c6s20)
{
in1a_3d: r12c6s18/out1_12;
out1: r12c6s21/in4c;
out2: r12c6s21/in5c;
out3: r12c6s21/in6c;
}




RMX2(r12c6s21) 
{
in4a: r12c6s21/out1;
in4b: r12c5s17/out1;
in4c: r12c6s20/out1;
//in4d: r52c5s5/out1;
in5a: r12c6s21/out2;
in5b: r12c5s17/out2;
in5c: r12c6s20/out2;
//in5d: r52c5s5/out2;
in6a: r12c6s21/out3;
in6b: r12c5s17/out3;
in6c: r12c6s20/out3;
//in6d: r52c5s5/out3;
out1: r12c6s21/in4a;
out2: r12c6s21/in5a;
out3: r12c6s21/in6a;
/* gehen raus zum muxxer:
out4: 
out5:
out6:*/
}

DMS1000(r12c6s23) // Different kind of camac interface/communication module
{
;
}

CBV1000(r12c6s24)
{
SETTING("CRATE_NO"=>"3");
vsb_in: r15c1s3/vsb_out; // in
vsb_out: r12c5s24/vsb_in; // out slot 23
}
DMS1000(r12c6s25)
{;
}

// TFW signal 7_1 to 12_2
CABLE_DELAY_12(r12c3)
{


  SETTING("DELAY" => "200 ns");

  in1_12:  r12c4/out1_12;
  out1_12: r15c4s4/in12_23;
}

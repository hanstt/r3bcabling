// TFW signal 23_1 to 25_1
CABLE_DELAY_16(r12c2)
{


  SETTING("DELAY" => "150 ns");

  in1:  r11c4s6/e5;
  out1: r12c2/in2;
  in2:  r12c2/out1;
  out2: r12c2/in3;
  in3:  r12c2/out2;
  out3: r15c4s5/in12;
  
  in4:  r11c4s6/e6;
  out4: r12c2/in5;
  in5:  r12c2/out4;
  out5: r12c2/in6;
  in6:  r12c2/out5;
  out6: r15c4s5/in13;


  in7:  r11c4s6/e7;
  out7: r12c2/in8;
  in8:  r12c2/out7;
  out8: r12c2/in9;
  in9:  r12c2/out8;
  out9: r15c4s5/in14;

  in10:  r11c4s6/e8;
  out10: r12c2/in11;
  in11:  r12c2/out10;
  out11: r12c2/in12;
  in12:  r12c2/out11;
  out12: r15c4s5/in15;
  
  in13:  r11c5s1/e1;
  out13: r12c2/in14;
  in14:  r12c2/out13;
  out14: r12c2/in15;
  in15:  r12c2/out14;
  out15: r15c4s5/in16;
}

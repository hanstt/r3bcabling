/* 
 * r12 CRATE 5
 * 
 * CAMAC Crate Adress 4 with CFD, Scaler, Controller
 *
 */
 
 CAMAC_CRATE(r12c5)
 {
 ;
 }
 
 //TFW5
 CF8103(r12c5s1)
 {
 in1_8: r11c4s5/t1_8;
 
// m: r52c2s10/in1; multiplicity output
 mux_tb: r12c5s15/in1a; //anderes RMX3 Modul: mit 6x mehrfachstecker
 mux_e: r12c5s15/in5a;
 mux_mon: r12c5s15/in9a;
 test: r12c6s16/out1;
 tb1_8: r12c5s9/in1_8;
 }
 //TFW6
 CF8103(r12c5s3)
 {
 in1_8: r11c4s6/t1_8;
 
// m: r52c2s10/in2;
 
 mux_tb: r12c5s15/in1b; //anderes RMX3 Modul: mit 6x mehrfachstecker
 mux_e: r12c5s15/in5b;
 mux_mon: r12c5s15/in9b;
 test: r12c6s16/out2;
 tb1_8: r12c5s9/in9_16;
 }
 //TFW7
 CF8103(r12c5s5)
 {
 in1_8: r11c5s1/t1_8;
 
// m: r52c2s10/in3;
 mux_tb: r12c5s15/in1c; //anderes RMX3 Modul: mit 6x mehrfachstecker
 mux_e: r12c5s15/in5c;
 mux_mon: r12c5s15/in9c;
 test: r12c6s16/out3;
 tb1_8: r12c5s11/in1_8;
 }
 
 //TFW8
 CF8103(r12c5s7)
 {
 in1_8: r11c5s2/t1_8;
 
// m: r52c2s10/in4;
 
 mux_tb: r12c5s15/in1d; //anderes RMX3 Modul: mit 6x mehrfachstecker
 mux_e: r12c5s15/in5d;
 mux_mon: r12c5s15/in9d;
 test: r12c6s16/out4;
 tb1_8: r12c5s11/in9_16;
 }
 
 
 
 
 DL1600(r12c5s9)
 {
 in1_8: r12c5s1/tb1_8;
 in9_16: r12c5s3/tb1_8;
 outa1_16: r15c4s7/in0_15;
 outb1_8: r12c5s13/in16_23;
 outb9_16: r12c5s13/in24_31;
 }
 
DL1600(r12c5s11)
 {
 in1_8: r12c5s5/tb1_8;
 in9_16: r12c5s7/tb1_8;
 outa1_16: r15c4s7/in16_31;
 outb1_8: r12c5s13/in32_39;
 outb9_16: r12c5s13/in40_47;
 }


SC4800(r12c5s13)
{
in16_23: r12c5s9/outb1_8;
in24_31: r12c5s9/outb9_16;
in32_39: r12c5s11/outb1_8;
in40_47: r12c5s11/outb9_16;

}


RMX3_2(r12c5s15) // RMX3 with combined connectors
{
in1a: r12c5s1/mux_tb;
in1b: r12c5s3/mux_tb;
in1c: r12c5s5/mux_tb;
in1d: r12c5s7/mux_tb;

in5a: r12c5s1/mux_e;
in5b: r12c5s3/mux_e;
in5c: r12c5s5/mux_e;
in5d: r12c5s7/mux_e;

in9a: r12c5s1/mux_mon;
in9b: r12c5s3/mux_mon;
in9c: r12c5s5/mux_mon;
in9d: r12c5s7/mux_mon;

out1_12: r12c5s17/in1a_3d;//? ist anderer RMX3. also mit buchstaben?

}

RMX3(r12c5s17)
{
in1a: r12c5s15/out1;
in1b: r12c5s15/out2;
in1c: r12c5s15/out3;
in1d: r12c5s15/out4;
in2a: r12c5s15/out5;
in2b: r12c5s15/out6;
in2c: r12c5s15/out7;
in2d: r12c5s15/out8;
in3a: r12c5s15/out9;
in3b: r12c5s15/out10;
in3c: r12c5s15/out11;
in3d: r12c5s15/out12;
out1: r12c6s21/in4b; //(geht in rmx2)
out2: r12c6s21/in5b;
out3: r12c6s21/in6b;
}

/*
Durchführung QDC FRS (r12c5s18)
{
out_ecl: abgezogen
}
Durchführung QDC FRS (r12c5s20)
{
out_ecl: abgezogen
in9_16: abgezogen
}
 */

CBV1000(r12c5s24)
{

//Karte mit 2 vsb. unterer: out, oberer in
SETTING("CRATE_NO"=> "4");
vsb_in:r12c6s24/vsb_out;//?? maybe processor
vsb_out: r14c6s24/vsb_in;//?? wieder CBV10000
}

DMS1000(r12c5s25)
{
;
}


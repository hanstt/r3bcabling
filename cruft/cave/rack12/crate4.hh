// TFW signal 7_1 to 12_2
CABLE_DELAY_12(r12c4)
{


  SETTING("DELAY" => "250 ns");

  in1_4:  r11c4s2/e5_8;
  in5_12: r11c4s3/e1_8;
  out1_12: r12c3/in1_12;
}

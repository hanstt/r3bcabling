MODULE(DETECTOR_TFW)
{
  CONNECTOR(signal);
  CONNECTOR(hv);
}

MODULE(DETECTOR_NTF)
{
  CONNECTOR(signal);
  CONNECTOR(hv);
}

/* The connectors for the TFW wall are on two panels one leg of the frame.
 * 
 * Left panel   Right panel
 *
 * TFW1 (hv)    TFW4 (hv)
 * TFW2 (hv)
 * TFW3 (hv)
 *
 * 1 (signal)   7 (signal)
 * 2 (signal)   8 (signal)
 * 3 (signal)   
 * 4 (signal)   
 * 5 (signal)   
 * 6 (signal)   
 *
 * The single channels in the signal connectors are numberd from right
 * to left, 1 - 8.
 *
 * Signal connector 1-4, first half of 5th serve vertical paddles
 * (extended in Y direction), connectors second half of 5th and 5-8 serve
 * horisontal paddles.
 *
 * Paddles 1-18 are vertical, 19-32 horizontal.
 */

/* Naming scheme for time-of-flight wall:
 *
 * DETECTOR_TFW ('the pm tube', r20cXXXsY), XXX is the paddle number,
 * Y is the pm number.  1 or 2.
 *
 * JOINER_8(rTFWRcXX) are the (signal) cable joiners sitting on the frame.
 *
 * HV_CONN_16(rTFWRcXX) are the HV connectors sitting on the frame.
 *
 * rTFW is thus the detector itself.
 * rTFWR is the cable joiners on the frame.
 
 *
 * 
 * 
 

*/


DETECTOR_TFW(rTFWc01s1) { LABEL("TFW01_1"); signal: rTFWRc1/in1; }//hv: rTFWRc11/out1;  }
DETECTOR_TFW(rTFWc01s2) { LABEL("TFW01_2"); signal: rTFWRc1/in2; }//hv: rTFWRc11/out2;  }
DETECTOR_TFW(rTFWc02s1) { LABEL("TFW02_1"); signal: rTFWRc1/in3; }//hv: rTFWRc11/out3;  }
DETECTOR_TFW(rTFWc02s2) { LABEL("TFW02_2"); signal: rTFWRc1/in4; }//hv: rTFWRc11/out4;  }
DETECTOR_TFW(rTFWc03s1) { LABEL("TFW03_1"); signal: rTFWRc1/in5; }//hv: rTFWRc11/out5;  }
DETECTOR_TFW(rTFWc03s2) { LABEL("TFW03_2"); signal: rTFWRc1/in6; }//hv: rTFWRc11/out6;  }
DETECTOR_TFW(rTFWc04s1) { LABEL("TFW04_1"); signal: rTFWRc1/in7;}// hv: rTFWRc11/out7;  }
DETECTOR_TFW(rTFWc04s2) { LABEL("TFW04_2"); signal: rTFWRc1/in8;}// hv: rTFWRc11/out8;  }
DETECTOR_TFW(rTFWc05s1) { LABEL("TFW05_1"); signal: rTFWRc2/in1; }//hv: rTFWRc11/out9;  }
DETECTOR_TFW(rTFWc05s2) { LABEL("TFW05_2"); signal: rTFWRc2/in2;}// hv: rTFWRc11/out10; }
DETECTOR_TFW(rTFWc06s1) { LABEL("TFW06_1"); signal: rTFWRc2/in3;}// hv: rTFWRc11/out11; }
DETECTOR_TFW(rTFWc06s2) { LABEL("TFW06_2"); signal: rTFWRc2/in4;}// hv: rTFWRc11/out12; }
DETECTOR_TFW(rTFWc07s1) { LABEL("TFW07_1"); signal: rTFWRc2/in5; }//hv: rTFWRc11/out13; }
DETECTOR_TFW(rTFWc07s2) { LABEL("TFW07_2"); signal: rTFWRc2/in6; }//hv: rTFWRc11/out14; }
DETECTOR_TFW(rTFWc08s1) { LABEL("TFW08_1"); signal: rTFWRc2/in7;}// hv: rTFWRc11/out15; }
DETECTOR_TFW(rTFWc08s2) { LABEL("TFW08_2"); signal: rTFWRc2/in8;}// hv: rTFWRc11/out16; }

DETECTOR_TFW(rTFWc09s1) { LABEL("TFW09_1"); signal: rTFWRc3/in1;}// hv: rTFWRc12/out1;  }
DETECTOR_TFW(rTFWc09s2) { LABEL("TFW09_2"); signal: rTFWRc3/in2; }//hv: rTFWRc12/out2;  }
DETECTOR_TFW(rTFWc10s1) { LABEL("TFW10_1"); signal: rTFWRc3/in3; }//hv: rTFWRc12/out3;  }
DETECTOR_TFW(rTFWc10s2) { LABEL("TFW10_2"); signal: rTFWRc3/in4; }//hv: rTFWRc12/out4;  }
DETECTOR_TFW(rTFWc11s1) { LABEL("TFW11_1"); signal: rTFWRc3/in5; }//hv: rTFWRc12/out5;  }
DETECTOR_TFW(rTFWc11s2) { LABEL("TFW11_2"); signal: rTFWRc3/in6; }//hv: rTFWRc12/out6;  }
DETECTOR_TFW(rTFWc12s1) { LABEL("TFW12_1"); signal: rTFWRc3/in7; }//hv: rTFWRc12/out7;  }
DETECTOR_TFW(rTFWc12s2) { LABEL("TFW12_2"); signal: rTFWRc3/in8;}// hv: rTFWRc12/out8;  }
DETECTOR_TFW(rTFWc13s1) { LABEL("TFW13_1"); signal: rTFWRc4/in1;}// hv: rTFWRc12/out9;  }
DETECTOR_TFW(rTFWc13s2) { LABEL("TFW13_2"); signal: rTFWRc4/in2; }//hv: rTFWRc12/out10; }
DETECTOR_TFW(rTFWc14s1) { LABEL("TFW14_1"); signal: rTFWRc4/in3; }//hv: rTFWRc12/out11; }
DETECTOR_TFW(rTFWc14s2) { LABEL("TFW14_2"); signal: rTFWRc4/in4;}// hv: rTFWRc12/out12; }
DETECTOR_TFW(rTFWc15s1) { LABEL("TFW15_1"); signal: rTFWRc4/in5;}// hv: rTFWRc12/out13; }
DETECTOR_TFW(rTFWc15s2) { LABEL("TFW15_2"); signal: rTFWRc4/in6; }//hv: rTFWRc12/out14; }
DETECTOR_TFW(rTFWc16s1) { LABEL("TFW16_1"); signal: rTFWRc4/in7;}// hv: rTFWRc12/out15; }
DETECTOR_TFW(rTFWc16s2) { LABEL("TFW16_2"); signal: rTFWRc4/in8;}// hv: rTFWRc12/out16; }

DETECTOR_TFW(rTFWc17s1) { LABEL("TFW17_1"); signal: rTFWRc5/in1; }//hv: rTFWRc13/out1;  }
DETECTOR_TFW(rTFWc17s2) { LABEL("TFW17_2"); signal: rTFWRc5/in2; }//hv: rTFWRc13/out2;  }
DETECTOR_TFW(rTFWc18s1) { LABEL("TFW18_1"); signal: rTFWRc5/in3; }//hv: rTFWRc13/out3;  }
DETECTOR_TFW(rTFWc18s2) { LABEL("TFW18_2"); signal: rTFWRc5/in4; }//hv: rTFWRc13/out4;  }
DETECTOR_TFW(rTFWc19s1) { LABEL("TFW19_1"); signal: rTFWRc5/in5; }//hv: rTFWRc13/out5;  }
DETECTOR_TFW(rTFWc19s2) { LABEL("TFW19_2"); signal: rTFWRc5/in6; }//hv: rTFWRc13/out6;  }
DETECTOR_TFW(rTFWc20s1) { LABEL("TFW20_1"); signal: rTFWRc5/in7; }//hv: rTFWRc13/out7;  }
DETECTOR_TFW(rTFWc20s2) { LABEL("TFW20_2"); signal: rTFWRc5/in8; }//hv: rTFWRc13/out8;  }
DETECTOR_TFW(rTFWc21s1) { LABEL("TFW21_1"); signal: rTFWRc6/in1; }//hv: rTFWRc13/out9;  }
DETECTOR_TFW(rTFWc21s2) { LABEL("TFW21_2"); signal: rTFWRc6/in2; }//hv: rTFWRc13/out10; }
DETECTOR_TFW(rTFWc22s1) { LABEL("TFW22_1"); signal: rTFWRc6/in3; }//hv: rTFWRc13/out11; }
DETECTOR_TFW(rTFWc22s2) { LABEL("TFW22_2"); signal: rTFWRc6/in4; }//hv: rTFWRc13/out12; }
DETECTOR_TFW(rTFWc23s1) { LABEL("TFW23_1"); signal: rTFWRc6/in5; }//hv: rTFWRc13/out13; }
DETECTOR_TFW(rTFWc23s2) { LABEL("TFW23_2"); signal: rTFWRc6/in6; }//hv: rTFWRc13/out14; }
DETECTOR_TFW(rTFWc24s1) { LABEL("TFW24_1"); signal: rTFWRc6/in7; }//hv: rTFWRc13/out15; }
DETECTOR_TFW(rTFWc24s2) { LABEL("TFW24_2"); signal: rTFWRc6/in8; }//hv: rTFWRc13/out16; }

DETECTOR_TFW(rTFWc25s1) { LABEL("TFW25_1"); signal: rTFWRc7/in1; }//hv: rTFWRc14/out1;  }
DETECTOR_TFW(rTFWc25s2) { LABEL("TFW25_2"); signal: rTFWRc7/in2; }//hv: rTFWRc14/out2;  }
DETECTOR_TFW(rTFWc26s1) { LABEL("TFW26_1"); signal: rTFWRc7/in3; }//hv: rTFWRc14/out3;  }
DETECTOR_TFW(rTFWc26s2) { LABEL("TFW26_2"); signal: rTFWRc7/in4; }//hv: rTFWRc14/out4;  }
DETECTOR_TFW(rTFWc27s1) { LABEL("TFW27_1"); signal: rTFWRc7/in5; }//hv: rTFWRc14/out5;  }
DETECTOR_TFW(rTFWc27s2) { LABEL("TFW27_2"); signal: rTFWRc7/in6; }//}//hv: rTFWRc14/out6;  }
DETECTOR_TFW(rTFWc28s1) { LABEL("TFW28_1"); signal: rTFWRc7/in7; }//hv: rTFWRc14/out6;  }
DETECTOR_TFW(rTFWc28s2) { LABEL("TFW28_2"); signal: rTFWRc7/in8; }//hv: rTFWRc14/out8;  }
DETECTOR_TFW(rTFWc29s1) { LABEL("TFW29_1"); signal: rTFWRc8/in1; }//hv: rTFWRc14/out9;  }
DETECTOR_TFW(rTFWc29s2) { LABEL("TFW29_2"); signal: rTFWRc8/in2; }//hv: rTFWRc14/out10; }
DETECTOR_TFW(rTFWc30s1) { LABEL("TFW30_1"); signal: rTFWRc8/in3; }//hv: rTFWRc14/out11; }
DETECTOR_TFW(rTFWc30s2) { LABEL("TFW30_2"); signal: rTFWRc8/in4; }//hv: rTFWRc14/out12; }
DETECTOR_TFW(rTFWc31s1) { LABEL("TFW31_1"); signal: rTFWRc8/in5; }//hv: rTFWRc14/out13; }
DETECTOR_TFW(rTFWc31s2) { LABEL("TFW31_2"); signal: rTFWRc8/in6; }//hv: rTFWRc14/out14; }
DETECTOR_TFW(rTFWc32s1) { LABEL("TFW32_1"); signal: rTFWRc8/in7; }//hv: rTFWRc14/out15; }
DETECTOR_TFW(rTFWc32s2) { LABEL("TFW32_2"); signal: rTFWRc8/in8; }//hv: rTFWRc14/out16; }

JOINER_8(rTFWRc1)
{
 in1: rTFWc01s1/signal;
 in2: rTFWc01s2/signal;
 in3: rTFWc02s1/signal;
 in4: rTFWc02s2/signal;
 in5: rTFWc03s1/signal;
 in6: rTFWc03s2/signal;
 in7: rTFWc04s1/signal;
 in8: rTFWc04s2/signal;
 
 out1_8: r11c4s1/in1_8;
}

JOINER_8(rTFWRc2)
{
 in1: rTFWc05s1/signal;
 in2: rTFWc05s2/signal;
 in3: rTFWc06s1/signal;
 in4: rTFWc06s2/signal;
 in5: rTFWc07s1/signal;
 in6: rTFWc07s2/signal;
 in7: rTFWc08s1/signal;
 in8: rTFWc08s2/signal;
 
 out1_8: r11c4s2/in1_8;
}

JOINER_8(rTFWRc3)
{
 in1: rTFWc09s1/signal;
 in2: rTFWc09s2/signal;
 in3: rTFWc10s1/signal;
 in4: rTFWc10s2/signal;
 in5: rTFWc11s1/signal;
 in6: rTFWc11s2/signal;
 in7: rTFWc12s1/signal;
 in8: rTFWc12s2/signal;
 
 out1_8: r11c4s3/in1_8;
}

JOINER_8(rTFWRc4)
{
 in1: rTFWc13s1/signal;
 in2: rTFWc13s2/signal;
 in3: rTFWc14s1/signal;
 in4: rTFWc14s2/signal;
 in5: rTFWc15s1/signal;
 in6: rTFWc15s2/signal;
 in7: rTFWc16s1/signal;
 in8: rTFWc16s2/signal;
 
 out1_8: r11c4s4/in1_8;
 }

JOINER_8(rTFWRc5)
{
 in1: rTFWc17s1/signal;
 in2: rTFWc17s2/signal;
 in3: rTFWc18s1/signal;
 in4: rTFWc18s2/signal;
 in5: rTFWc19s1/signal;
 in6: rTFWc19s2/signal;
 in7: rTFWc20s1/signal;
 in8: rTFWc20s2/signal;
 
 out1_8: r11c4s5/in1_8;
}

JOINER_8(rTFWRc6)
{
 in1: rTFWc21s1/signal;
 in2: rTFWc21s2/signal;
 in3: rTFWc22s1/signal;
 in4: rTFWc22s2/signal;
 in5: rTFWc23s1/signal;
 in6: rTFWc23s2/signal;
 in7: rTFWc24s1/signal;
 in8: rTFWc24s2/signal;
 
 out1_8: r11c4s6/in1_8;
}

JOINER_8(rTFWRc7)
{
 in1: rTFWc25s1/signal;
 in2: rTFWc25s2/signal;
 in3: rTFWc26s1/signal;
 in4: rTFWc26s2/signal;
 in5: rTFWc27s1/signal;
 in6: rTFWc27s2/signal;
 in7: rTFWc28s1/signal;
 in8: rTFWc28s2/signal;
 
 out1_8: r11c5s1/in1_8;
}

JOINER_8(rTFWRc8)
{
 in1: rTFWc29s1/signal;
 in2: rTFWc29s2/signal;
 in3: rTFWc30s1/signal;
 in4: rTFWc30s2/signal;
 in5: rTFWc31s1/signal;
 in6: rTFWc31s2/signal;
 in7: rTFWc32s1/signal;
 in8: rTFWc32s2/signal;
 
 out1_8: r11c5s2/in1_8;
}
/*
HV_CONN_16(rTFWRc11)
{
 in1_16: rHV1c1s1/out17_32;

 out1:  rTFWc01s1/hv;
 out2:  rTFWc01s2/hv;
 out3:  rTFWc02s1/hv;
 out4:  rTFWc02s2/hv; 
 out5:  rTFWc03s1/hv;
 out6:  rTFWc03s2/hv;
 out7:  rTFWc04s1/hv;
 out8:  rTFWc04s2/hv;
 out9:  rTFWc05s1/hv;
 out10: rTFWc05s2/hv;
 out11: rTFWc06s1/hv;
 out12: rTFWc06s2/hv;
 out13: rTFWc07s1/hv;
 out14: rTFWc07s2/hv;
 out15: rTFWc08s1/hv;
 out16: rTFWc08s2/hv;
}

HV_CONN_16(rTFWRc12)
{
 in1_16: rHV1c1s1/out33_48;

 out1:  rTFWc09s1/hv;
 out2:  rTFWc09s2/hv;
 out3:  rTFWc10s1/hv;
 out4:  rTFWc10s2/hv;
 out5:  rTFWc11s1/hv;
 out6:  rTFWc11s2/hv;
 out7:  rTFWc12s1/hv;
 out8:  rTFWc12s2/hv;
 out9:  rTFWc13s1/hv;
 out10: rTFWc13s2/hv;
 out11: rTFWc14s1/hv;
 out12: rTFWc14s2/hv;
 out13: rTFWc15s1/hv;
 out14: rTFWc15s2/hv;
 out15: rTFWc16s1/hv;
 out16: rTFWc16s2/hv;
}

HV_CONN_16(rTFWRc13)
{
 in1_16: rHV1c1s1/out49_64;

 out1:  rTFWc17s1/hv;
 out2:  rTFWc17s2/hv;
 out3:  rTFWc18s1/hv;
 out4:  rTFWc18s2/hv;
 out5:  rTFWc19s1/hv;
 out6:  rTFWc19s2/hv;
 out7:  rTFWc20s1/hv;
 out8:  rTFWc20s2/hv;
 out9:  rTFWc21s1/hv;
 out10: rTFWc21s2/hv;
 out11: rTFWc22s1/hv;
 out12: rTFWc22s2/hv;
 out13: rTFWc23s1/hv;
 out14: rTFWc23s2/hv;
 out15: rTFWc24s1/hv;
 out16: rTFWc24s2/hv;
}

HV_CONN_16(rTFWRc14)
{
 in1_16: rHV1c1s1/out65_80;

 out1:  rTFWc25s1/hv;
 out2:  rTFWc25s2/hv;
 out3:  rTFWc26s1/hv;
 out4:  rTFWc26s2/hv;
 out5:  rTFWc27s1/hv;
 out6:  rTFWc27s2/hv;
 out7:  rTFWc28s1/hv;
 out8:  rTFWc28s2/hv;
 out9:  rTFWc29s1/hv;
 out10: rTFWc29s2/hv;
 out11: rTFWc30s1/hv;
 out12: rTFWc30s2/hv;
 out13: rTFWc31s1/hv;
 out14: rTFWc31s2/hv;
 out15: rTFWc32s1/hv;
 out16: rTFWc32s2/hv;
}

*/

/* New TOF wall. */

/* Vertical paddles */
// Changed by KB to new cabeling March 2012
// TO DO_ HV!!!

DETECTOR_NTF(rNTFc01s1) { LABEL("NTF01_1"); signal: rNTFRc1/in1; }//hv: rNTFRc11/out1;  }
DETECTOR_NTF(rNTFc01s2) { LABEL("NTF01_2"); signal: rNTFRc1/in2; }//hv: rNTFRc11/out9;  }
DETECTOR_NTF(rNTFc02s1) { LABEL("NTF02_1"); signal: rNTFRc1/in3; }//hv: rNTFRc11/out2;  }
DETECTOR_NTF(rNTFc02s2) { LABEL("NTF02_2"); signal: rNTFRc1/in4; }//hv: rNTFRc11/out10;  }
DETECTOR_NTF(rNTFc03s1) { LABEL("NTF03_1"); signal: rNTFRc1/in5; }//hv: rNTFRc11/out3;  }
DETECTOR_NTF(rNTFc03s2) { LABEL("NTF03_2"); signal: rNTFRc1/in6; }//hv: rNTFRc11/out11;  }
DETECTOR_NTF(rNTFc04s1) { LABEL("NTF04_1"); signal: rNTFRc1/in7; }//hv: rNTFRc11/out4;  }
DETECTOR_NTF(rNTFc04s2) { LABEL("NTF04_2"); signal: rNTFRc1/in8; }//hv: rNTFRc11/out12;  }
DETECTOR_NTF(rNTFc05s1) { LABEL("NTF05_1"); signal: rNTFRc2/in1; }//hv: rNTFRc11/out5;  }
DETECTOR_NTF(rNTFc05s2) { LABEL("NTF05_2"); signal: rNTFRc2/in2; }//hv: rNTFRc11/out13; }
DETECTOR_NTF(rNTFc06s1) { LABEL("NTF06_1"); signal: rNTFRc2/in3; }//hv: rNTFRc11/out6; }
DETECTOR_NTF(rNTFc06s2) { LABEL("NTF06_2"); signal: rNTFRc2/in4; }//hv: rNTFRc11/out14; }
DETECTOR_NTF(rNTFc07s1) { LABEL("NTF07_1"); signal: rNTFRc2/in5; }//hv: rNTFRc11/out7; }
DETECTOR_NTF(rNTFc07s2) { LABEL("NTF07_2"); signal: rNTFRc2/in6; }//hv: rNTFRc11/out15; }
DETECTOR_NTF(rNTFc08s1) { LABEL("NTF08_1"); signal: rNTFRc2/in7; }//hv: rNTFRc11/out8; }
DETECTOR_NTF(rNTFc08s2) { LABEL("NTF08_2"); signal: rNTFRc2/in8; }//hv: rNTFRc11/out16; }


JOINER_8(rNTFRc1)
{
 in1: rNTFc01s1/signal;
 in2: rNTFc01s2/signal;
 in3: rNTFc02s1/signal;
 in4: rNTFc02s2/signal;
 in5: rNTFc03s1/signal;
 in6: rNTFc03s2/signal;
 in7: rNTFc04s1/signal;
 in8: rNTFc04s2/signal;
 
 out1_8: r11c5s3/in1_8;
}

JOINER_8(rNTFRc2)
{
 in1: rNTFc05s1/signal;
 in2: rNTFc05s2/signal;
 in3: rNTFc06s1/signal;
 in4: rNTFc06s2/signal;
 in5: rNTFc07s1/signal;
 in6: rNTFc07s2/signal;
 in7: rNTFc08s1/signal;
 in8: rNTFc08s2/signal;
 
out1_8: r11c5s4/in1_8;
}

/*
HV_CONN_16(rNTFRc11)
{
 in1_16: rHV1c1s1/out81_96;

 out1:  rNTFc01s1/hv;
 out2:  rNTFc02s1/hv;
 out3:  rNTFc03s1/hv;
 out4:  rNTFc04s1/hv;
 out5:  rNTFc05s1/hv;
 out6:  rNTFc06s1/hv;
 out7:  rNTFc07s1/hv;
 out8:  rNTFc08s1/hv;
 out9:  rNTFc01s2/hv;
 out10: rNTFc02s2/hv;
 out11: rNTFc03s2/hv;
 out12: rNTFc04s2/hv;
 out13: rNTFc05s2/hv;
 out14: rNTFc06s2/hv;
 out15: rNTFc07s2/hv;
 out16: rNTFc08s2/hv;
}
*/
/* Horizontal paddles */

DETECTOR_NTF(rNTFc09s1) { LABEL("NTF09_1"); signal: rNTFRc3/in1; }//hv: rNTFRc12/out1;  }
DETECTOR_NTF(rNTFc09s2) { LABEL("NTF09_2"); signal: rNTFRc3/in2; }//hv: rNTFRc12/out9;  }
DETECTOR_NTF(rNTFc10s1) { LABEL("NTF10_1"); signal: rNTFRc3/in3; }//hv: rNTFRc12/out2;  }
DETECTOR_NTF(rNTFc10s2) { LABEL("NTF10_2"); signal: rNTFRc3/in4; }//hv: rNTFRc12/out10;  }
DETECTOR_NTF(rNTFc11s1) { LABEL("NTF11_1"); signal: rNTFRc3/in5; }//hv: rNTFRc12/out3;  }
DETECTOR_NTF(rNTFc11s2) { LABEL("NTF11_2"); signal: rNTFRc3/in6; }//hv: rNTFRc12/out11;  }
DETECTOR_NTF(rNTFc12s1) { LABEL("NTF12_1"); signal: rNTFRc3/in7; }//hv: rNTFRc12/out4;  }
DETECTOR_NTF(rNTFc12s2) { LABEL("NTF12_2"); signal: rNTFRc3/in8;}// hv: rNTFRc12/out12;  }
DETECTOR_NTF(rNTFc13s1) { LABEL("NTF13_1"); signal: rNTFRc4/in1; }//hv: rNTFRc12/out5;  }
DETECTOR_NTF(rNTFc13s2) { LABEL("NTF13_2"); signal: rNTFRc4/in2; }//hv: rNTFRc12/out13; }
DETECTOR_NTF(rNTFc14s1) { LABEL("NTF14_1"); signal: rNTFRc4/in3; }//hv: rNTFRc12/out6; }
DETECTOR_NTF(rNTFc14s2) { LABEL("NTF14_2"); signal: rNTFRc4/in4; }//hv: rNTFRc12/out14; }
DETECTOR_NTF(rNTFc15s1) { LABEL("NTF15_1"); signal: rNTFRc4/in5; }//hv: rNTFRc12/out7; }
DETECTOR_NTF(rNTFc15s2) { LABEL("NTF15_2"); signal: rNTFRc4/in6; }//hv: rNTFRc12/out15; }
DETECTOR_NTF(rNTFc16s1) { LABEL("NTF16_1"); signal: rNTFRc4/in7; }//hv: rNTFRc12/out8; }
DETECTOR_NTF(rNTFc16s2) { LABEL("NTF16_2"); signal: rNTFRc4/in8; }//hv: rNTFRc12/out16; }

JOINER_8(rNTFRc3)
{
 in1: rNTFc09s1/signal;
 in2: rNTFc09s2/signal;
 in3: rNTFc10s1/signal;
 in4: rNTFc10s2/signal;
 in5: rNTFc11s1/signal;
 in6: rNTFc11s2/signal;
 in7: rNTFc12s1/signal;
 in8: rNTFc12s2/signal;

 out1_8: r11c5s5/in1_8;
}

JOINER_8(rNTFRc4)
{
 in1: rNTFc13s1/signal;
 in2: rNTFc13s2/signal;
 in3: rNTFc14s1/signal;
 in4: rNTFc14s2/signal;
 in5: rNTFc15s1/signal;
 in6: rNTFc15s2/signal;
 in7: rNTFc16s1/signal;
 in8: rNTFc16s2/signal;
 
 out1_8: r11c5s6/in1_8;
}

/*
HV_CONN_16(rNTFRc12)
{
 in1_16: rHV1c1s1/out97_112;

 out1:  rNTFc09s1/hv;
 out2:  rNTFc10s1/hv;
 out3:  rNTFc11s1/hv;
 out4:  rNTFc12s1/hv;
 out5:  rNTFc13s1/hv;
 out6:  rNTFc14s1/hv;
 out7:  rNTFc15s1/hv;
 out8:  rNTFc16s1/hv;
 out9:  rNTFc09s2/hv;
 out10: rNTFc10s2/hv;
 out11: rNTFc11s2/hv;
 out12: rNTFc12s2/hv;
 out13: rNTFc13s2/hv;
 out14: rNTFc14s2/hv;
 out15: rNTFc15s2/hv;
 out16: rNTFc16s2/hv;
}
*/

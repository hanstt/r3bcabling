#ifndef __GFI_HH__
#define __GFI_HH__

/* General declarations */
/* GFI prototype */

CRATE(DETECTOR_GFI)
{
  LABEL("GFI detector");
}

/* Timing tube */

MODULE(DETECTOR_GFI_PM)
{
  CONNECTOR(hv);
  CONNECTOR(signal);
}

/* Position sensitive PMT */
/* There's one HV signal / detector */

MODULE(DETECTOR_GFI_PSPM)
{
  CONNECTOR(hv);
}

/* ... but several individual signal outputs */

UNIT(DETECTOR_GFI_PSPM_U)
{
  CONNECTOR(signal);
}

/* Vacuum flange in Aladin */

CRATE(VACUUM_FLANGE36)
{
  HANDLER("MULTI_IN_OUT","x_in=x_out:y_in=y_out:te_in=te_out:p0_in=p0_out");

  LEMO_INPUT(x_in1_18);
  LEMO_OUTPUT(x_out1_18);
  LEMO_INPUT(y_in1_16);
  LEMO_OUTPUT(y_out1_16);
  LEMO_INPUT(te_in);
  LEMO_OUTPUT(te_out);
  LEMO_INPUT(p0_in);
  LEMO_OUTPUT(p0_out);
}


#endif /* __GFI_HH__ */

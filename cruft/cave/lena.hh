#ifndef __LENA_HH__
#define __LENA_HH__

/* General declarations */
/* LENA detector from ATOMKI/GSI */

CRATE(DETECTOR_LENA)
{
  LABEL("LENA detector");
}

/* PMT */

MODULE(DETECTOR_LENA_PM)
{
  CONNECTOR(hv);
  CONNECTOR(signal);
}

#endif 

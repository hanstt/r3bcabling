//  Rack 11, crate 4 for split boxes TFW        
NIM_CRATE(r11c4)
{
;
}

SPLIT_BOX_PASSIVE8(r11c4s1)
{
in1_8: rTFWRc1/out1_8;
t1_8: r12c6s2/in1_8;
//e1_8: not connected
}
SPLIT_BOX_PASSIVE8(r11c4s2)
{
in1_8: rTFWRc2/out1_8;
t1_8: r12c6s4/in1_8;
//e1_4: not connected
e5_8: r12c4/in1_4;
}
SPLIT_BOX_PASSIVE8(r11c4s3)
{
in1_8: rTFWRc3/out1_8;
t1_8: r12c6s6/in1_8;
e1_8: r12c4/in5_12;
}
SPLIT_BOX_PASSIVE8(r11c4s4)
{
in1_8: rTFWRc4/out1_8;
t1_8: r12c6s8/in1_8;
//e1_8: r13c3/in1_8;
}
SPLIT_BOX_PASSIVE8(r11c4s5)
{
in1_8: rTFWRc5/out1_8;
t1_8: r12c5s1/in1_8;
//e1_8: r13c4/in1_8;
}
SPLIT_BOX_PASSIVE8(r11c4s6)
{
in1_8: rTFWRc6/out1_8;
t1_8: r12c5s3/in1_8;
e5: r12c2/in1;
e6: r12c2/in4;
e7: r12c2/in7;
e8: r12c2/in10;
}

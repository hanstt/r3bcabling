//  Rack 11, crate 5 for split boxes TFW and NTF
NIM_CRATE(r11c5)
{
;
}
//25_1 to 28_2
SPLIT_BOX_PASSIVE8(r11c5s1)
{
in1_8: rTFWRc7/out1_8;
t1_8: r12c5s5/in1_8;
e1: r12c2/in13;
e2: r12c1/in1;
e3: r12c1/in4;
e4: r12c1/in7;
e5: r12c1/in10;
e6: r12c1/in13;
e7: r13c1/in1;
e8: r13c1/in4;
}
SPLIT_BOX_PASSIVE8(r11c5s2)
{
in1_8: rTFWRc8/out1_8;
t1_8: r12c5s7/in1_8;
//e1_8: r13c3/in1_8;
}
SPLIT_BOX_PASSIVE8(r11c5s3)
{
in1_8: rNTFRc1/out1_8;
t1_8: r14c6s4/in1_8;
e1_8: r13c2/in1_8;
}
SPLIT_BOX_PASSIVE8(r11c5s4)
{
in1_8: rNTFRc2/out1_8;
t1_8: r14c6s6/in1_8;
e1_8: r13c3/in1_8;
}
SPLIT_BOX_PASSIVE8(r11c5s5)
{
in1_8: rNTFRc3/out1_8;
t1_8: r14c6s8/in1_8;
e1_8: r13c4/in1_8;
}
SPLIT_BOX_PASSIVE8(r11c5s6)
{
in1_8: rNTFRc4/out1_8;
t1_8: r14c6s10/in1_8;
e1_8: r13c5/in1_8;
}

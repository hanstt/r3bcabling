VME_CRATE(r22c2)
{
  ;
}

// HAS TO BE CHECKED !!!

E7(r22c2s1)
{
  SETTING("HOSTNAME" => "e7_3");
}

HV_CTRL_XB_VME(r22c2s5)
{
 ctrl_out0_7:     r22c1s3/ctrl_in1_8;
 ctrl_out8_15:    r22c1s7/ctrl_in1_8;
 ctrl_out16_23:   r22c1s9/ctrl_in1_8;
 ctrl_out24_31:   r22c1s13/ctrl_in1_8;
 ctrl_out32_39:   r22c1s15/ctrl_in1_8;
}

HV_CTRL_XB_VME(r22c2s7)
{
 ctrl_out0_7:     r22c1s17/ctrl_in1_8;
 ctrl_out8_15:    r22c1s19/ctrl_in1_8;
 ctrl_out16_23:   r22c1s21/ctrl_in1_8;
 ctrl_out24_31:   r22c1s23/ctrl_in1_8;
 ctrl_out32_39:   r23c2s23/ctrl_in1_8;	// ctrl_out36 would be for #077
}

HV_CTRL_XB_VME(r22c2s9)
{
 ctrl_out0_7:     r23c2s19/ctrl_in1_8;
 ctrl_out8_15:    r23c2s15/ctrl_in1_8;
 ctrl_out16_23:   r23c2s11/ctrl_in1_8;
 ctrl_out24_31:   r23c2s7/ctrl_in1_8;
 ctrl_out32_39:   r23c1s23/ctrl_in1_8;
}

HV_CTRL_XB_VME(r22c2s11)
{
 ctrl_out0_7:    r23c1s19/ctrl_in1_8;
 ctrl_out8_15:   r23c1s15/ctrl_in1_8;
 ctrl_out16_23:  r23c1s11/ctrl_in1_8;
 ctrl_out24_31:  r23c1s7/ctrl_in1_8;
 ctrl_out32_39:  r23c1s3/ctrl_in1_8;
}



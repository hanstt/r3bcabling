//This is the bottom NIM crate in r22 which is nearly empty
NIM_CRATE(r22c3)
{
    ;
}

FL8000_U(r22c3s1u1) // HAS TO BE CHECKED !!!
{
// 	in: r22c3s07/or;
	in:	r22c0s23/analog_out;

	out: r22c3s3/in1;
}

FL8000_U(r22c3s1u2) // HAS TO BE CHECKED !!!
{
// 	in: r22c0s07/or;
	in:	r22c0s19/analog_out;

	out:	r22c3s3/in2;
}

SPLIT_BOX_PASSIVE(r22c3s3)
{
	in1:	r22c3s1u1/out;	
	in2:	r22c3s1u2/out;

	t1:		r22c3s7/in1;
	t2:		r22c3s7/in2;

	e1:		r23c4s5/in3;
	e2:		r23c4s5/in4;
}

SU1200(r22c3s7)
{
	in1:	r22c3s3/t1;
	in2:	r22c3s3/t2;

	analog_out:	r22c3s19u6/in; // ok
}

GG8000(r22c3s17)
{
	;
}

CF8000_U(r22c3s19u6)
{
	in:	r22c3s7/analog_out; // ok

	outB1:	; // not connected
}

CF8000_U(r22c3s19u7)
{
	in:	; // not connected

	outB1:	; // not connected ?
	outB2:	;	// not connected
}

LF4000_4_U(r22c3s21u2)
{
	out1:	r24c0s7/nim_gate0; //ok
	out2:	r24c0s9/nim_gate0; //ok
	out3:	r24c0s11/nim_gate0; //ok
	out4:	r24c0s13/nim_gate0; //ok
}

LF4000_4_U(r22c3s21u4)
{
	in1:	r22c0s21u3/out1;
	out1:	r24c0s2/in_lemo1; //ok
}

LF4000_4_U(r22c3s23u1)
{
	in1:	r24c0s2/out_lemo1; //ok
	
	out1:	r24c0s21/in_nim; // ok
}


NIM_CRATE(r22c1)
{
  ;
}

HV_CTRL_XB_NIM(r22c1s3)
{
 ctrl_in1_8: r22c2s5/ctrl_out0_7;
 ctrl_out1: r20c001s2/hv_ctrl; power_out1: r20c001s2/power;
 ctrl_out2: r20c002s2/hv_ctrl; power_out2: r20c002s2/power;
 ctrl_out3: r20c003s2/hv_ctrl; power_out3: r20c003s2/power;
 ctrl_out4: r20c004s2/hv_ctrl; power_out4: r20c004s2/power;
 ctrl_out5: r20c005s2/hv_ctrl; power_out5: r20c005s2/power;
 ctrl_out6: r20c006s2/hv_ctrl; power_out6: r20c006s2/power;
 ctrl_out7: r20c007s2/hv_ctrl; power_out7: r20c007s2/power;
 ctrl_out8: r20c008s2/hv_ctrl; power_out8: r20c008s2/power;
}

HV_CTRL_XB_NIM(r22c1s7)
{
 ctrl_in1_8: r22c2s5/ctrl_out8_15;
 ctrl_out1: r20c009s2/hv_ctrl; power_out1: r20c009s2/power;
 ctrl_out2: r20c010s2/hv_ctrl; power_out2: r20c010s2/power;
 ctrl_out3: r20c011s2/hv_ctrl; power_out3: r20c011s2/power;
 ctrl_out4: r20c012s2/hv_ctrl; power_out4: r20c012s2/power;
 ctrl_out5: r20c013s2/hv_ctrl; power_out5: r20c013s2/power;
 ctrl_out6: r20c014s2/hv_ctrl; power_out6: r20c014s2/power;
 ctrl_out7: r20c015s2/hv_ctrl; power_out7: r20c015s2/power;
 ctrl_out8: r20c016s2/hv_ctrl; power_out8: r20c016s2/power;
}

HV_CTRL_XB_NIM(r22c1s9)
{
 ctrl_in1_8: r22c2s5/ctrl_out16_23;
 ctrl_out1: r20c017s2/hv_ctrl; power_out1: r20c017s2/power;
 ctrl_out2: r20c018s2/hv_ctrl; power_out2: r20c018s2/power;
 ctrl_out3: r20c019s2/hv_ctrl; power_out3: r20c019s2/power;
 ctrl_out4: r20c020s2/hv_ctrl; power_out4: r20c020s2/power;
 ctrl_out5: r20c021s2/hv_ctrl; power_out5: r20c021s2/power;
 ctrl_out6: r20c022s2/hv_ctrl; power_out6: r20c022s2/power;
 ctrl_out7: r20c023s2/hv_ctrl; power_out7: r20c023s2/power;
 ctrl_out8: r20c024s2/hv_ctrl; power_out8: r20c024s2/power;
}

HV_CTRL_XB_NIM(r22c1s13)
{
 ctrl_in1_8: r22c2s5/ctrl_out24_31;
 ctrl_out1: r20c025s2/hv_ctrl; power_out1: r20c025s2/power;
 ctrl_out2: r20c026s2/hv_ctrl; power_out2: r20c026s2/power;
 ctrl_out3: r20c027s2/hv_ctrl; power_out3: r20c027s2/power;
 ctrl_out4: r20c028s2/hv_ctrl; power_out4: r20c028s2/power;
 ctrl_out5: r20c029s2/hv_ctrl; power_out5: r20c029s2/power;
 ctrl_out6: r20c030s2/hv_ctrl; power_out6: r20c030s2/power;
 ctrl_out7: r20c031s2/hv_ctrl; power_out7: r20c031s2/power;
 ctrl_out8: r20c032s2/hv_ctrl; power_out8: r20c032s2/power;
}

HV_CTRL_XB_NIM(r22c1s15)
{
 ctrl_in1_8: r22c2s5/ctrl_out32_39;
 ctrl_out1: r20c033s2/hv_ctrl; power_out1: r20c033s2/power;
 ctrl_out2: r20c034s2/hv_ctrl; power_out2: r20c034s2/power;
 ctrl_out3: r20c035s2/hv_ctrl; power_out3: r20c035s2/power;
 ctrl_out4: r20c036s2/hv_ctrl; power_out4: r20c036s2/power;
 ctrl_out5: r20c037s2/hv_ctrl; power_out5: r20c037s2/power;
 ctrl_out6: r20c038s2/hv_ctrl; power_out6: r20c038s2/power;
 ctrl_out7: r20c039s2/hv_ctrl; power_out7: r20c039s2/power;
 ctrl_out8: r20c040s2/hv_ctrl; power_out8: r20c040s2/power;
}

HV_CTRL_XB_NIM(r22c1s17)
{
 ctrl_in1_8: r22c2s7/ctrl_out0_7;
 ctrl_out1: r20c041s2/hv_ctrl; power_out1: r20c041s2/power;
 ctrl_out2: r20c042s2/hv_ctrl; power_out2: r20c042s2/power;
 ctrl_out3: r20c043s2/hv_ctrl; power_out3: r20c043s2/power;
 ctrl_out4: r20c044s2/hv_ctrl; power_out4: r20c044s2/power;
 ctrl_out5: r20c045s2/hv_ctrl; power_out5: r20c045s2/power;
 ctrl_out6: r20c046s2/hv_ctrl; power_out6: r20c046s2/power;
 ctrl_out7: r20c047s2/hv_ctrl; power_out7: r20c047s2/power;
 ctrl_out8: r20c048s2/hv_ctrl; power_out8: r20c048s2/power;
}

HV_CTRL_XB_NIM(r22c1s19)
{
 ctrl_in1_8: r22c2s7/ctrl_out8_15;
 ctrl_out1: r20c049s2/hv_ctrl; power_out1: r20c049s2/power;
 ctrl_out2: r20c050s2/hv_ctrl; power_out2: r20c050s2/power;
 ctrl_out3: r20c051s2/hv_ctrl; power_out3: r20c051s2/power;
 ctrl_out4: r20c052s2/hv_ctrl; power_out4: r20c052s2/power;
 ctrl_out5: r20c053s2/hv_ctrl; power_out5: r20c053s2/power;
 ctrl_out6: r20c054s2/hv_ctrl; power_out6: r20c054s2/power;
 ctrl_out7: r20c055s2/hv_ctrl; power_out7: r20c055s2/power;
 ctrl_out8: r20c056s2/hv_ctrl; power_out8: r20c056s2/power;
}

HV_CTRL_XB_NIM(r22c1s21)
{
 ctrl_in1_8: r22c2s7/ctrl_out16_23;
 ctrl_out1: r20c057s2/hv_ctrl; power_out1: r20c057s2/power;
 ctrl_out2: r20c058s2/hv_ctrl; power_out2: r20c058s2/power;
 ctrl_out3: r20c059s2/hv_ctrl; power_out3: r20c059s2/power;
 ctrl_out4: r20c060s2/hv_ctrl; power_out4: r20c060s2/power;
 ctrl_out5: r20c061s2/hv_ctrl; power_out5: r20c061s2/power;
 ctrl_out6: r20c062s2/hv_ctrl; power_out6: r20c062s2/power;
 ctrl_out7: r20c063s2/hv_ctrl; power_out7: r20c063s2/power;
 ctrl_out8: r20c064s2/hv_ctrl; power_out8: r20c064s2/power;
}

HV_CTRL_XB_NIM(r22c1s23)
{
 ctrl_in1_8: r22c2s7/ctrl_out24_31;
 ctrl_out1: r20c065s2/hv_ctrl; power_out1: r20c065s2/power;
 ctrl_out2: r20c066s2/hv_ctrl; power_out2: r20c066s2/power;
 ctrl_out3: r20c067s2/hv_ctrl; power_out3: r20c067s2/power;
 ctrl_out4: r20c068s2/hv_ctrl; power_out4: r20c068s2/power;
 ctrl_out5: r20c069s2/hv_ctrl; power_out5: r20c069s2/power;
 ctrl_out6: r20c070s2/hv_ctrl; power_out6: r20c070s2/power;
 ctrl_out7: r20c071s2/hv_ctrl; power_out7: r20c071s2/power;
 ctrl_out8: r20c072s2/hv_ctrl; power_out8: r20c072s2/power;
}



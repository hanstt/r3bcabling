R1307(r20c001s2) { signal: r20c001s1/signal; out1: LABEL("+_1") r22c0s11/in3;  out2: LABEL("+_2");  hv_ctrl:  r22c1s3/ctrl_out1; power:  r22c1s3/power_out1; }
R1307(r20c002s2) { signal: r20c002s1/signal; out1: LABEL("+_1") r22c0s11/in1;  out2: LABEL("+_2");  hv_ctrl:  r22c1s3/ctrl_out2; power:  r22c1s3/power_out2; }
R1307(r20c003s2) { signal: r20c003s1/signal; out1: LABEL("+_1") r22c0s09/in5;  out2: LABEL("+_2") r22c0s3/in13;    hv_ctrl:  r22c1s3/ctrl_out3; power:  r22c1s3/power_out3; }
R1307(r20c004s2) { signal: r20c004s1/signal; out1: LABEL("+_1") r22c0s09/in6;  out2: LABEL("+_2") r22c0s3/in14;   hv_ctrl:  r22c1s3/ctrl_out4; power:  r22c1s3/power_out4; }
R1307(r20c005s2) { signal: r20c005s1/signal; out1: LABEL("+_1") r22c0s11/in2;  out2: LABEL("+_2");  hv_ctrl:  r22c1s3/ctrl_out5; power:  r22c1s3/power_out5; }
R1307(r20c006s2) { signal: r20c006s1/signal; out1: LABEL("+_1") r22c0s11/in11;  out2: LABEL("+_2");  hv_ctrl:  r22c1s3/ctrl_out6; power:  r22c1s3/power_out6; }
R1307(r20c007s2) { signal: r20c007s1/signal; out1: LABEL("+_1") r22c0s11/in12;  out2: LABEL("+_2");  hv_ctrl:  r22c1s3/ctrl_out7; power:  r22c1s3/power_out7; }
R1307(r20c008s2) { signal: r20c008s1/signal; out1: LABEL("+_1") r22c0s11/in9;  out2: LABEL("+_2");  hv_ctrl:  r22c1s3/ctrl_out8; power:  r22c1s3/power_out8; }
R1307(r20c009s2) { signal: r20c009s1/signal; out1: LABEL("+_1") r22c0s09/in15; out2: LABEL("+_2");  hv_ctrl:  r22c1s7/ctrl_out1; power:  r22c1s7/power_out1; } //r21c10/in5
R1307(r20c010s2) { signal: r20c010s1/signal; out1: LABEL("+_1") r22c0s09/in7;  out2: LABEL("+_2") r22c0s3/in15;    hv_ctrl:  r22c1s7/ctrl_out2; power:  r22c1s7/power_out2; }
R1307(r20c011s2) { signal: r20c011s1/signal; out1: LABEL("+_1") r22c0s07/in11;  out2: LABEL("+_2") r22c0s3/in4;   hv_ctrl:  r22c1s7/ctrl_out3; power:  r22c1s7/power_out3; }
R1307(r20c012s2) { signal: r20c012s1/signal; out1: LABEL("+_1") r22c0s07/in10;  out2: LABEL("+_2") r22c0s3/in3;    hv_ctrl:  r22c1s7/ctrl_out4; power:  r22c1s7/power_out4; }
R1307(r20c013s2) { signal: r20c013s1/signal; out1: LABEL("+_1") r22c0s07/in12;  out2: LABEL("+_2") r22c0s3/in5;   hv_ctrl:  r22c1s7/ctrl_out5; power:  r22c1s7/power_out5; }
R1307(r20c014s2) { signal: r20c014s1/signal; out1: LABEL("+_1") r22c0s09/in8;  out2: LABEL("+_2") r22c0s3/in16;    hv_ctrl:  r22c1s7/ctrl_out6; power:  r22c1s7/power_out6; }
R1307(r20c015s2) { signal: r20c015s1/signal; out1: LABEL("+_1") r22c0s09/in16;  out2: LABEL("+_2");  hv_ctrl:  r22c1s7/ctrl_out7; power:  r22c1s7/power_out7; }
R1307(r20c016s2) { signal: r20c016s1/signal; out1: LABEL("+_1") r22c0s11/in10;  out2: LABEL("+_2");  hv_ctrl:  r22c1s7/ctrl_out8; power:  r22c1s7/power_out8; }
R1307(r20c017s2) { signal: r20c017s1/signal; out1: LABEL("+_1") r22c0s13/in5;  out2: LABEL("+_2");  hv_ctrl:  r22c1s9/ctrl_out1; power:  r22c1s9/power_out1; }
R1307(r20c018s2) { signal: r20c018s1/signal; out1: LABEL("+_1") r22c0s13/in7;  out2: LABEL("+_2");  hv_ctrl:  r22c1s9/ctrl_out2; power:  r22c1s9/power_out2; }
R1307(r20c019s2) { signal: r20c019s1/signal; out1: LABEL("+_1") r22c0s13/in6;  out2: LABEL("+_2");  hv_ctrl:  r22c1s9/ctrl_out3; power:  r22c1s9/power_out3; }
R1307(r20c020s2) { signal: r20c020s1/signal; out1: LABEL("+_1") r22c0s13/in3;  out2: LABEL("+_2");  hv_ctrl:  r22c1s9/ctrl_out4; power:  r22c1s9/power_out4; }
R1307(r20c021s2) { signal: r20c021s1/signal; out1: LABEL("+_1") r22c0s11/in7;  out2: LABEL("+_2");  hv_ctrl:  r22c1s9/ctrl_out5; power:  r22c1s9/power_out5; }
R1307(r20c022s2) { signal: r20c022s1/signal; out1: LABEL("+_1") r22c0s09/in9;  out2: LABEL("+_2");  hv_ctrl:  r22c1s9/ctrl_out6; power:  r22c1s9/power_out6; }
R1307(r20c023s2) { signal: r20c023s1/signal; out1: LABEL("+_1") r22c0s07/in13;  out2: LABEL("+_2") r22c0s3/in6;   hv_ctrl:  r22c1s9/ctrl_out7; power:  r22c1s9/power_out7; }
R1307(r20c024s2) { signal: r20c024s1/signal; out1: LABEL("+_1") r22c0s07/in1;  out2: LABEL("+_2") r22c0s1/in14;   hv_ctrl:  r22c1s9/ctrl_out8; power:  r22c1s9/power_out8; }
R1307(r20c025s2) { signal: r20c025s1/signal; out1: LABEL("+_1") r22c0s05/in13;  out2: LABEL("+_2") r22c0s1/in10;   hv_ctrl: r22c1s13/ctrl_out1; power: r22c1s13/power_out1; }
R1307(r20c026s2) { signal: r20c026s1/signal; out1: LABEL("+_1") r22c0s05/in14;  out2: LABEL("+_2") r22c0s1/in11;   hv_ctrl: r22c1s13/ctrl_out2; power: r22c1s13/power_out2; }
R1307(r20c027s2) { signal: r20c027s1/signal; out1: LABEL("+_1") r22c0s07/in3;  out2: LABEL("+_2") r22c0s1/in15;  hv_ctrl: r22c1s13/ctrl_out3; power: r22c1s13/power_out3; }
R1307(r20c028s2) { signal: r20c028s1/signal; out1: LABEL("+_1") r22c0s07/in14;  out2: LABEL("+_2") r22c0s3/in7;   hv_ctrl: r22c1s13/ctrl_out4; power: r22c1s13/power_out4; }
R1307(r20c029s2) { signal: r20c029s1/signal; out1: LABEL("+_1") r22c0s09/in10;  out2: LABEL("+_2");  hv_ctrl: r22c1s13/ctrl_out5; power: r22c1s13/power_out5; }
R1307(r20c030s2) { signal: r20c030s1/signal; out1: LABEL("+_1") r22c0s11/in8;  out2: LABEL("+_2");  hv_ctrl: r22c1s13/ctrl_out6; power: r22c1s13/power_out6; }
R1307(r20c031s2) { signal: r20c031s1/signal; out1: LABEL("+_1") r22c0s13/in4;  out2: LABEL("+_2");  hv_ctrl: r22c1s13/ctrl_out7; power: r22c1s13/power_out7; }
R1307(r20c032s2) { signal: r20c032s1/signal; out1: LABEL("+_1") r22c0s13/in13;  out2: LABEL("+_2");  hv_ctrl: r22c1s13/ctrl_out8; power: r22c1s13/power_out8; }
R1307(r20c033s2) { signal: r20c033s1/signal; out1: LABEL("+_1") r22c0s15/in1;  out2: LABEL("+_2");  hv_ctrl: r22c1s15/ctrl_out1; power: r22c1s15/power_out1; }
R1307(r20c034s2) { signal: r20c034s1/signal; out1: LABEL("+_1") r22c0s15/in2;  out2: LABEL("+_2");  hv_ctrl: r22c1s15/ctrl_out2; power: r22c1s15/power_out2; }
R1307(r20c035s2) { signal: r20c035s1/signal; out1: LABEL("+_1") r22c0s13/in14;  out2: LABEL("+_2");  hv_ctrl: r22c1s15/ctrl_out3; power: r22c1s15/power_out3; }
R1307(r20c036s2) { signal: r20c036s1/signal; out1: LABEL("+_1") r22c0s13/in9;  out2: LABEL("+_2");  hv_ctrl: r22c1s15/ctrl_out4; power: r22c1s15/power_out4; }
R1307(r20c037s2) { signal: r20c037s1/signal; out1: LABEL("+_1") r22c0s11/in14;  out2: LABEL("+_2");  hv_ctrl: r22c1s15/ctrl_out5; power: r22c1s15/power_out5; }
R1307(r20c038s2) { signal: r20c038s1/signal; out1: LABEL("+_1") r22c0s09/in13;  out2: LABEL("+_2");  hv_ctrl: r22c1s15/ctrl_out6; power: r22c1s15/power_out6; }
R1307(r20c039s2) { signal: r20c039s1/signal; out1: LABEL("+_1") r22c0s09/in1;  out2: LABEL("+_2") r22c0s3/in10;   hv_ctrl: r22c1s15/ctrl_out7; power: r22c1s15/power_out7; }
R1307(r20c040s2) { signal: r20c040s1/signal; out1: LABEL("+_1") r22c0s07/in5;  out2: LABEL("+_2") r22c0s1/in16;    hv_ctrl: r22c1s15/ctrl_out8; power: r22c1s15/power_out8; }
R1307(r20c041s2) { signal: r20c041s1/signal; out1: LABEL("+_1") r22c0s05/in6;  out2: LABEL("+_2") r22c0s1/in5;    hv_ctrl: r22c1s17/ctrl_out1; power: r22c1s17/power_out1; }
R1307(r20c042s2) { signal: r20c042s1/signal; out1: LABEL("+_1") r22c0s05/in8;  out2: LABEL("+_2") r22c0s1/in7;  hv_ctrl: r22c1s17/ctrl_out2; power: r22c1s17/power_out2; }
R1307(r20c043s2) { signal: r20c043s1/signal; out1: LABEL("+_1") r22c0s05/in7;  out2: LABEL("+_2") r22c0s1/in6;   hv_ctrl: r22c1s17/ctrl_out3; power: r22c1s17/power_out3; }
R1307(r20c044s2) { signal: r20c044s1/signal; out1: LABEL("+_1") r22c0s07/in6;  out2: LABEL("+_2") r22c0s3/in1;   hv_ctrl: r22c1s17/ctrl_out4; power: r22c1s17/power_out4; }
R1307(r20c045s2) { signal: r20c045s1/signal; out1: LABEL("+_1") r22c0s09/in2;  out2: LABEL("+_2") r22c0s3/in11;    hv_ctrl: r22c1s17/ctrl_out5; power: r22c1s17/power_out5; }
R1307(r20c046s2) { signal: r20c046s1/signal; out1: LABEL("+_1") r22c0s09/in14;  out2: LABEL("+_2");  hv_ctrl: r22c1s17/ctrl_out6; power: r22c1s17/power_out6; }
R1307(r20c047s2) { signal: r20c047s1/signal; out1: LABEL("+_1") r22c0s11/in15;  out2: LABEL("+_2");  hv_ctrl: r22c1s17/ctrl_out7; power: r22c1s17/power_out7; }
R1307(r20c048s2) { signal: r20c048s1/signal; out1: LABEL("+_1") r22c0s13/in10;  out2: LABEL("+_2");  hv_ctrl: r22c1s17/ctrl_out8; power: r22c1s17/power_out8; }
R1307(r20c049s2) { signal: r20c049s1/signal; out1: LABEL("+_1") r22c0s15/in10;  out2: LABEL("+_2");  hv_ctrl: r22c1s19/ctrl_out1; power: r22c1s19/power_out1; }
R1307(r20c050s2) { signal: r20c050s1/signal; out1: LABEL("+_1") r22c0s15/in9;  out2: LABEL("+_2");  hv_ctrl: r22c1s19/ctrl_out2; power: r22c1s19/power_out2; }
R1307(r20c051s2) { signal: r20c051s1/signal; out1: LABEL("+_1") r22c0s15/in11;  out2: LABEL("+_2");  hv_ctrl: r22c1s19/ctrl_out3; power: r22c1s19/power_out3; }
R1307(r20c052s2) { signal: r20c052s1/signal; out1: LABEL("+_1") r22c0s15/in5;  out2: LABEL("+_2");  hv_ctrl: r22c1s19/ctrl_out4; power: r22c1s19/power_out4; }
R1307(r20c053s2) { signal: r20c053s1/signal; out1: LABEL("+_1") r22c0s13/in15;  out2: LABEL("+_2");  hv_ctrl: r22c1s19/ctrl_out5; power: r22c1s19/power_out5; }
R1307(r20c054s2) { signal: r20c054s1/signal; out1: LABEL("+_1") r22c0s13/in1;  out2: LABEL("+_2");  hv_ctrl: r22c1s19/ctrl_out6; power: r22c1s19/power_out6; }
R1307(r20c055s2) { signal: r20c055s1/signal; out1: LABEL("+_1") r22c0s11/in5;  out2: LABEL("+_2");  hv_ctrl: r22c1s19/ctrl_out7; power: r22c1s19/power_out7; }
R1307(r20c056s2) { signal: r20c056s1/signal; out1: LABEL("+_1") r22c0s09/in11;  out2: LABEL("+_2");  hv_ctrl: r22c1s19/ctrl_out8; power: r22c1s19/power_out8; }
R1307(r20c057s2) { signal: r20c057s1/signal; out1: LABEL("+_1") r22c0s07/in15;  out2: LABEL("+_2") r22c0s3/in8;   hv_ctrl: r22c1s21/ctrl_out1; power: r22c1s21/power_out1; }
R1307(r20c058s2) { signal: r20c058s1/signal; out1: LABEL("+_1") r22c0s05/in15;  out2: LABEL("+_2") r22c0s1/in12;  hv_ctrl: r22c1s21/ctrl_out2; power: r22c1s21/power_out2; }
R1307(r20c059s2) { signal: r20c059s1/signal; out1: LABEL("+_1") r22c0s05/in9;  out2: LABEL("+_2") r22c0s1/in8;   hv_ctrl: r22c1s21/ctrl_out3; power: r22c1s21/power_out3; }
R1307(r20c060s2) { signal: r20c060s1/signal; out1: LABEL("+_1") r22c0s05/in2;  out2: LABEL("+_2") r22c0s1/in2;  hv_ctrl: r22c1s21/ctrl_out4; power: r22c1s21/power_out4; }
R1307(r20c061s2) { signal: r20c061s1/signal; out1: LABEL("+_1") r22c0s05/in1;  out2: LABEL("+_2") r22c0s1/in1;   hv_ctrl: r22c1s21/ctrl_out5; power: r22c1s21/power_out5; }
R1307(r20c062s2) { signal: r20c062s1/signal; out1: LABEL("+_1") r22c0s05/in3;  out2: LABEL("+_2") r22c0s1/in3;  hv_ctrl: r22c1s21/ctrl_out6; power: r22c1s21/power_out6; }
R1307(r20c063s2) { signal: r20c063s1/signal; out1: LABEL("+_1") r22c0s05/in10;  out2: LABEL("+_2") r22c0s1/in9;   hv_ctrl: r22c1s21/ctrl_out7; power: r22c1s21/power_out7; }
R1307(r20c064s2) { signal: r20c064s1/signal; out1: LABEL("+_1") r22c0s05/in16;  out2: LABEL("+_2") r22c0s1/in13;  hv_ctrl: r22c1s21/ctrl_out8; power: r22c1s21/power_out8; }
R1307(r20c065s2) { signal: r20c065s1/signal; out1: LABEL("+_1") r22c0s07/in16;  out2: LABEL("+_2") r22c0s3/in9;   hv_ctrl: r22c1s23/ctrl_out1; power: r22c1s23/power_out1; }
R1307(r20c066s2) { signal: r20c066s1/signal; out1: LABEL("+_1") r22c0s09/in12;  out2: LABEL("+_2");  hv_ctrl: r22c1s23/ctrl_out2; power: r22c1s23/power_out2; }
R1307(r20c067s2) { signal: r20c067s1/signal; out1: LABEL("+_1") r22c0s11/in6;  out2: LABEL("+_2");  hv_ctrl: r22c1s23/ctrl_out3; power: r22c1s23/power_out3; }
R1307(r20c068s2) { signal: r20c068s1/signal; out1: LABEL("+_1") r22c0s13/in2;  out2: LABEL("+_2");  hv_ctrl: r22c1s23/ctrl_out4; power: r22c1s23/power_out4; }
R1307(r20c069s2) { signal: r20c069s1/signal; out1: LABEL("+_1") r22c0s13/in16;  out2: LABEL("+_2");  hv_ctrl: r22c1s23/ctrl_out5; power: r22c1s23/power_out5; }
R1307(r20c070s2) { signal: r20c070s1/signal; out1: LABEL("+_1") r22c0s15/in6;  out2: LABEL("+_2");  hv_ctrl: r22c1s23/ctrl_out6; power: r22c1s23/power_out6; }
R1307(r20c071s2) { signal: r20c071s1/signal; out1: LABEL("+_1") r22c0s15/in13;  out2: LABEL("+_2");  hv_ctrl: r22c1s23/ctrl_out7; power: r22c1s23/power_out7; }
R1307(r20c072s2) { signal: r20c072s1/signal; out1: LABEL("+_1") r22c0s15/in15;  out2: LABEL("+_2");  hv_ctrl: r22c1s23/ctrl_out8; power: r22c1s23/power_out8; }
R1307(r20c073s2) { signal: r20c073s1/signal; out1: LABEL("+_1") r22c0s15/in14;  out2: LABEL("+_2");  hv_ctrl: r23c2s23/ctrl_out1; power: r23c2s23/power_out1; } // r21c2/in1: swapped # 009 and # 073 cables at Patch Panel
R1307(r20c074s2) { signal: r20c074s1/signal; out1: LABEL("+_1") r22c0s15/in12; out2: LABEL("+_2");  hv_ctrl: r23c2s23/ctrl_out2; power: r23c2s23/power_out2; }
R1307(r20c075s2) { signal: r20c075s1/signal; out1: LABEL("+_1") r22c0s13/in8; out2: LABEL("+_2");  hv_ctrl: r23c2s23/ctrl_out3; power: r23c2s23/power_out3; }
R1307(r20c076s2) { signal: r20c076s1/signal; out1: LABEL("+_1") r22c0s11/in13; out2: LABEL("+_2");  hv_ctrl: r23c2s23/ctrl_out4; power: r23c2s23/power_out4; }
//R1307(r20c077s2) { signal: r20c077s1/signal; out1: LABEL("+_1") r21c10/in5; out2: LABEL("+_2");  hv_ctrl: r23c2s23/ctrl_out5; power: r23c2s23/power_out5; } // Modified AcB (formerly # 101) used for the single crystal used in the s223 proton test beam time
R1307(r20c078s2) { signal: r20c078s1/signal; out1: LABEL("+_1") r22c0s09/in3; out2: LABEL("+_2") r22c0s3/in12;  hv_ctrl: r23c2s23/ctrl_out6; power: r23c2s23/power_out6; }
R1307(r20c079s2) { signal: r20c079s1/signal; out1: LABEL("+_1") r22c0s07/in9; out2: LABEL("+_2") r22c0s3/in2;    hv_ctrl: r23c2s23/ctrl_out7; power: r23c2s23/power_out7; }
R1307(r20c080s2) { signal: r20c080s1/signal; out1: LABEL("+_1") r22c0s05/in5; out2: LABEL("+_2") r22c0s1/in4;    hv_ctrl: r23c2s23/ctrl_out8; power: r23c2s23/power_out8; }
// 81 <-> 161
// 82 <-> 162
R1307(r20c083s2) { signal: r20c083s1/signal; out1: LABEL("+_1") r23c0s03/in11; out2: LABEL("+_2");  hv_ctrl: r23c2s19/ctrl_out3; power: r23c2s19/power_out3; }
R1307(r20c084s2) { signal: r20c084s1/signal; out1: LABEL("+_1") r23c0s05/in8; out2: LABEL("+_2");  hv_ctrl: r23c2s19/ctrl_out4; power: r23c2s19/power_out4; }
R1307(r20c085s2) { signal: r20c085s1/signal; out1: LABEL("+_1") r23c0s07/in13; out2: LABEL("+_2");  hv_ctrl: r23c2s19/ctrl_out5; power: r23c2s19/power_out5; }
R1307(r20c086s2) { signal: r20c086s1/signal; out1: LABEL("+_1") r23c0s07/in3; out2: LABEL("+_2");  hv_ctrl: r23c2s19/ctrl_out6; power: r23c2s19/power_out6; }
R1307(r20c087s2) { signal: r20c087s1/signal; out1: LABEL("+_1") r23c0s09/in3; out2: LABEL("+_2") r23c0s15/in12;   hv_ctrl: r23c2s19/ctrl_out7; power: r23c2s19/power_out7; }
R1307(r20c088s2) { signal: r20c088s1/signal; out1: LABEL("+_1") r23c0s11/in9; out2: LABEL("+_2") r23c0s15/in2;   hv_ctrl: r23c2s19/ctrl_out8; power: r23c2s19/power_out8; }
R1307(r20c089s2) { signal: r20c089s1/signal; out1: LABEL("+_1") r23c0s13/in5; out2: LABEL("+_2") r23c0s17/in4;   hv_ctrl: r23c2s15/ctrl_out1; power: r23c2s15/power_out1; }
R1307(r20c090s2) { signal: r20c090s1/signal; out1: LABEL("+_1") r23c0s13/in2; out2: LABEL("+_2") r23c0s17/in2;  hv_ctrl: r23c2s15/ctrl_out2; power: r23c2s15/power_out2; }
R1307(r20c091s2) { signal: r20c091s1/signal; out1: LABEL("+_1") r23c0s13/in1; out2: LABEL("+_2") r23c0s17/in1;   hv_ctrl: r23c2s15/ctrl_out3; power: r23c2s15/power_out3; }
R1307(r20c092s2) { signal: r20c092s1/signal; out1: LABEL("+_1") r23c0s13/in3; out2: LABEL("+_2") r23c0s17/in3;  hv_ctrl: r23c2s15/ctrl_out4; power: r23c2s15/power_out4; }
R1307(r20c093s2) { signal: r20c093s1/signal; out1: LABEL("+_1") r23c0s13/in9; out2: LABEL("+_2") r23c0s17/in8;   hv_ctrl: r23c2s15/ctrl_out5; power: r23c2s15/power_out5; }
R1307(r20c094s2) { signal: r20c094s1/signal; out1: LABEL("+_1") r23c0s13/in15; out2: LABEL("+_2") r23c0s17/in12;  hv_ctrl: r23c2s15/ctrl_out6; power: r23c2s15/power_out6; }
R1307(r20c095s2) { signal: r20c095s1/signal; out1: LABEL("+_1") r23c0s11/in15; out2: LABEL("+_2") r23c0s15/in8;   hv_ctrl: r23c2s15/ctrl_out7; power: r23c2s15/power_out7; }
R1307(r20c096s2) { signal: r20c096s1/signal; out1: LABEL("+_1") r23c0s09/in11; out2: LABEL("+_2");  hv_ctrl: r23c2s15/ctrl_out8; power: r23c2s15/power_out8; }
R1307(r20c097s2) { signal: r20c097s1/signal; out1: LABEL("+_1") r23c0s07/in5; out2: LABEL("+_2");  hv_ctrl: r23c2s11/ctrl_out1; power: r23c2s11/power_out1; }
R1307(r20c098s2) { signal: r20c098s1/signal; out1: LABEL("+_1") r23c0s05/in1; out2: LABEL("+_2");  hv_ctrl: r23c2s11/ctrl_out2; power: r23c2s11/power_out2; }
R1307(r20c099s2) { signal: r20c099s1/signal; out1: LABEL("+_1") r23c0s05/in15; out2: LABEL("+_2");  hv_ctrl: r23c2s11/ctrl_out3; power: r23c2s11/power_out3; }
R1307(r20c100s2) { signal: r20c100s1/signal; out1: LABEL("+_1") r23c0s03/in5; out2: LABEL("+_2");  hv_ctrl: r23c2s11/ctrl_out4; power: r23c2s11/power_out4; }
R1307(r20c101s2) { signal: r20c101s1/signal; out1: LABEL("+_1") r23c0s03/in13; out2: LABEL("+_2");  hv_ctrl: r23c2s11/ctrl_out5; power: r23c2s11/power_out5; }
R1307(r20c102s2) { signal: r20c102s1/signal; out1: LABEL("+_1") r23c0s03/in15; out2: LABEL("+_2");  hv_ctrl: r23c2s11/ctrl_out6; power: r23c2s11/power_out6; }
R1307(r20c103s2) { signal: r20c103s1/signal; out1: LABEL("+_1") r23c0s03/in14; out2: LABEL("+_2");  hv_ctrl: r23c2s11/ctrl_out7; power: r23c2s11/power_out7; }
R1307(r20c104s2) { signal: r20c104s1/signal; out1: LABEL("+_1") r23c0s03/in6; out2: LABEL("+_2");  hv_ctrl: r23c2s11/ctrl_out8; power: r23c2s11/power_out8; }
R1307(r20c105s2) { signal: r20c105s1/signal; out1: LABEL("+_1") r23c0s05/in16; out2: LABEL("+_2");  hv_ctrl:  r23c2s7/ctrl_out1; power:  r23c2s7/power_out1; }
R1307(r20c106s2) { signal: r20c106s1/signal; out1: LABEL("+_1") r23c0s05/in2; out2: LABEL("+_2");  hv_ctrl:  r23c2s7/ctrl_out2; power:  r23c2s7/power_out2; }
R1307(r20c107s2) { signal: r20c107s1/signal; out1: LABEL("+_1") r23c0s07/in6; out2: LABEL("+_2");  hv_ctrl:  r23c2s7/ctrl_out3; power:  r23c2s7/power_out3; }
R1307(r20c108s2) { signal: r20c108s1/signal; out1: LABEL("+_1") r23c0s09/in12; out2: LABEL("+_2");  hv_ctrl:  r23c2s7/ctrl_out4; power:  r23c2s7/power_out4; }
R1307(r20c109s2) { signal: r20c109s1/signal; out1: LABEL("+_1") r23c0s11/in16; out2: LABEL("+_2") r23c0s15/in9;   hv_ctrl:  r23c2s7/ctrl_out5; power:  r23c2s7/power_out5; }
R1307(r20c110s2) { signal: r20c110s1/signal; out1: LABEL("+_1") r23c0s13/in16; out2: LABEL("+_2") r23c0s17/in13;  hv_ctrl:  r23c2s7/ctrl_out6; power:  r23c2s7/power_out6; }
R1307(r20c111s2) { signal: r20c111s1/signal; out1: LABEL("+_1") r23c0s13/in10; out2: LABEL("+_2") r23c0s17/in9;  hv_ctrl:  r23c2s7/ctrl_out7; power:  r23c2s7/power_out7; }
R1307(r20c112s2) { signal: r20c112s1/signal; out1: LABEL("+_1") r23c0s13/in6; out2: LABEL("+_2") r23c0s17/in5;   hv_ctrl:  r23c2s7/ctrl_out8; power:  r23c2s7/power_out8; }
R1307(r20c113s2) { signal: r20c113s1/signal; out1: LABEL("+_1") r23c0s13/in8; out2: LABEL("+_2") r23c0s17/in7;  hv_ctrl: r23c1s23/ctrl_out1; power: r23c1s23/power_out1; }
R1307(r20c114s2) { signal: r20c114s1/signal; out1: LABEL("+_1") r23c0s13/in7; out2: LABEL("+_2") r23c0s17/in6;  hv_ctrl: r23c1s23/ctrl_out2; power: r23c1s23/power_out2; }
R1307(r20c115s2) { signal: r20c115s1/signal; out1: LABEL("+_1") r23c0s11/in5; out2: LABEL("+_2") r23c0s17/in16;  hv_ctrl: r23c1s23/ctrl_out3; power: r23c1s23/power_out3; }
R1307(r20c116s2) { signal: r20c116s1/signal; out1: LABEL("+_1") r23c0s09/in1; out2: LABEL("+_2") r23c0s15/in10;   hv_ctrl: r23c1s23/ctrl_out4; power: r23c1s23/power_out4; }
R1307(r20c117s2) { signal: r20c117s1/signal; out1: LABEL("+_1") r23c0s09/in13; out2: LABEL("+_2");  hv_ctrl: r23c1s23/ctrl_out5; power: r23c1s23/power_out5; }
R1307(r20c118s2) { signal: r20c118s1/signal; out1: LABEL("+_1") r23c0s07/in14; out2: LABEL("+_2");  hv_ctrl: r23c1s23/ctrl_out6; power: r23c1s23/power_out6; }
R1307(r20c119s2) { signal: r20c119s1/signal; out1: LABEL("+_1") r23c0s05/in9; out2: LABEL("+_2");  hv_ctrl: r23c1s23/ctrl_out7; power: r23c1s23/power_out7; }
R1307(r20c120s2) { signal: r20c120s1/signal; out1: LABEL("+_1") r23c0s03/in10; out2: LABEL("+_2");  hv_ctrl: r23c1s23/ctrl_out8; power: r23c1s23/power_out8; }
R1307(r20c121s2) { signal: r20c121s1/signal; out1: LABEL("+_1") r23c0s03/in9; out2: LABEL("+_2");  hv_ctrl: r23c1s19/ctrl_out1; power: r23c1s19/power_out1; }
R1307(r20c122s2) { signal: r20c122s1/signal; out1: LABEL("+_1") r23c0s03/in12; out2: LABEL("+_2");  hv_ctrl: r23c1s19/ctrl_out2; power: r23c1s19/power_out2; }
R1307(r20c123s2) { signal: r20c123s1/signal; out1: LABEL("+_1") r23c0s05/in10; out2: LABEL("+_2");  hv_ctrl: r23c1s19/ctrl_out3; power: r23c1s19/power_out3; }
R1307(r20c124s2) { signal: r20c124s1/signal; out1: LABEL("+_1") r23c0s07/in15; out2: LABEL("+_2");  hv_ctrl: r23c1s19/ctrl_out4; power: r23c1s19/power_out4; }
//R1307(r20c125s2) { signal: r20c125s1/signal; out1: LABEL("+_1") r23c0s09/in14; out2: LABEL("+_2");  hv_ctrl: r23c1s19/ctrl_out5; power: r23c1s19/power_out5; }
R1307(r20c125s2) { signal: r20c125s1/signal; out1: LABEL("+_1") r23c0s03/in16; out2: LABEL("+_2");  hv_ctrl: r23c1s19/ctrl_out5; power: r23c1s19/power_out5; }
R1307(r20c126s2) { signal: r20c126s1/signal; out1: LABEL("+_1") r23c0s09/in2; out2: LABEL("+_2") r23c0s15/in11;   hv_ctrl: r23c1s19/ctrl_out6; power: r23c1s19/power_out6; }
R1307(r20c127s2) { signal: r20c127s1/signal; out1: LABEL("+_1") r23c0s11/in6; out2: LABEL("+_2") r23c0s15/in1;   hv_ctrl: r23c1s19/ctrl_out7; power: r23c1s19/power_out7; }
R1307(r20c128s2) { signal: r20c128s1/signal; out1: LABEL("+_1") r23c0s11/in1; out2: LABEL("+_2") r23c0s17/in14;  hv_ctrl: r23c1s19/ctrl_out8; power: r23c1s19/power_out8; }
R1307(r20c129s2) { signal: r20c129s1/signal; out1: LABEL("+_1") r23c0s13/in13; out2: LABEL("+_2") r23c0s17/in10;  hv_ctrl: r23c1s15/ctrl_out1; power: r23c1s15/power_out1; }
R1307(r20c130s2) { signal: r20c130s1/signal; out1: LABEL("+_1") r23c0s13/in14; out2: LABEL("+_2") r23c0s17/in11;   hv_ctrl: r23c1s15/ctrl_out2; power: r23c1s15/power_out2; }
R1307(r20c131s2) { signal: r20c131s1/signal; out1: LABEL("+_1") r23c0s11/in2; out2: LABEL("+_2") r23c0s17/in15;  hv_ctrl: r23c1s15/ctrl_out3; power: r23c1s15/power_out3; }
R1307(r20c132s2) { signal: r20c132s1/signal; out1: LABEL("+_1") r23c0s11/in13; out2: LABEL("+_2") r23c0s15/in6;   hv_ctrl: r23c1s15/ctrl_out4; power: r23c1s15/power_out4; }
R1307(r20c133s2) { signal: r20c133s1/signal; out1: LABEL("+_1") r23c0s09/in9; out2: LABEL("+_2");  hv_ctrl: r23c1s15/ctrl_out5; power: r23c1s15/power_out5; }
R1307(r20c134s2) { signal: r20c134s1/signal; out1: LABEL("+_1") r23c0s07/in7; out2: LABEL("+_2");  hv_ctrl: r23c1s15/ctrl_out6; power: r23c1s15/power_out6; }
R1307(r20c135s2) { signal: r20c135s1/signal; out1: LABEL("+_1") r23c0s05/in3; out2: LABEL("+_2");  hv_ctrl: r23c1s15/ctrl_out7; power: r23c1s15/power_out7; }
R1307(r20c136s2) { signal: r20c136s1/signal; out1: LABEL("+_1") r23c0s05/in13; out2: LABEL("+_2");  hv_ctrl: r23c1s15/ctrl_out8; power: r23c1s15/power_out8; }
R1307(r20c137s2) { signal: r20c137s1/signal; out1: LABEL("+_1") r23c0s03/in1; out2: LABEL("+_2");  hv_ctrl: r23c1s11/ctrl_out1; power: r23c1s11/power_out1; }
R1307(r20c138s2) { signal: r20c138s1/signal; out1: LABEL("+_1") r23c0s03/in2; out2: LABEL("+_2");  hv_ctrl: r23c1s11/ctrl_out2; power: r23c1s11/power_out2; }
R1307(r20c139s2) { signal: r20c139s1/signal; out1: LABEL("+_1") r23c0s05/in14; out2: LABEL("+_2");  hv_ctrl: r23c1s11/ctrl_out3; power: r23c1s11/power_out3; }
R1307(r20c140s2) { signal: r20c140s1/signal; out1: LABEL("+_1") r23c0s05/in4; out2: LABEL("+_2");  hv_ctrl: r23c1s11/ctrl_out4; power: r23c1s11/power_out4; }
R1307(r20c141s2) { signal: r20c141s1/signal; out1: LABEL("+_1") r23c0s07/in8; out2: LABEL("+_2");  hv_ctrl: r23c1s11/ctrl_out5; power: r23c1s11/power_out5; }
R1307(r20c142s2) { signal: r20c142s1/signal; out1: LABEL("+_1") r23c0s09/in10; out2: LABEL("+_2");  hv_ctrl: r23c1s11/ctrl_out6; power: r23c1s11/power_out6; }
R1307(r20c143s2) { signal: r20c143s1/signal; out1: LABEL("+_1") r23c0s11/in14; out2: LABEL("+_2") r23c0s15/in7;  hv_ctrl: r23c1s11/ctrl_out7; power: r23c1s11/power_out7; }
R1307(r20c144s2) { signal: r20c144s1/signal; out1: LABEL("+_1") r23c0s11/in11; out2: LABEL("+_2") r23c0s15/in4;   hv_ctrl: r23c1s11/ctrl_out8; power: r23c1s11/power_out8; }
R1307(r20c145s2) { signal: r20c145s1/signal; out1: LABEL("+_1") r23c0s11/in10; out2: LABEL("+_2") r23c0s15/in3;   hv_ctrl:  r23c1s7/ctrl_out1; power:  r23c1s7/power_out1; }
R1307(r20c146s2) { signal: r20c146s1/signal; out1: LABEL("+_1") r23c0s11/in12; out2: LABEL("+_2") r23c0s15/in5;  hv_ctrl:  r23c1s7/ctrl_out2; power:  r23c1s7/power_out2; }
R1307(r20c147s2) { signal: r20c147s1/signal; out1: LABEL("+_1") r23c0s09/in7; out2: LABEL("+_2") r23c0s15/in15;   hv_ctrl:  r23c1s7/ctrl_out3; power:  r23c1s7/power_out3; }
R1307(r20c148s2) { signal: r20c148s1/signal; out1: LABEL("+_1") r23c0s09/in15; out2: LABEL("+_2");  hv_ctrl:  r23c1s7/ctrl_out4; power:  r23c1s7/power_out4; }
R1307(r20c149s2) { signal: r20c149s1/signal; out1: LABEL("+_1") r23c0s07/in9; out2: LABEL("+_2");  hv_ctrl:  r23c1s7/ctrl_out5; power:  r23c1s7/power_out5; }
R1307(r20c150s2) { signal: r20c150s1/signal; out1: LABEL("+_1") r23c0s05/in5; out2: LABEL("+_2");  hv_ctrl:  r23c1s7/ctrl_out6; power:  r23c1s7/power_out6; }
R1307(r20c151s2) { signal: r20c151s1/signal; out1: LABEL("+_1") r23c0s05/in7; out2: LABEL("+_2");  hv_ctrl:  r23c1s7/ctrl_out7; power:  r23c1s7/power_out7; }
R1307(r20c152s2) { signal: r20c152s1/signal; out1: LABEL("+_1") r23c0s05/in6; out2: LABEL("+_2");  hv_ctrl:  r23c1s7/ctrl_out8; power:  r23c1s7/power_out8; }
R1307(r20c153s2) { signal: r20c153s1/signal; out1: LABEL("+_1") r23c0s07/in10; out2: LABEL("+_2");  hv_ctrl:  r23c1s3/ctrl_out1; power:  r23c1s3/power_out1; }
R1307(r20c154s2) { signal: r20c154s1/signal; out1: LABEL("+_1") r23c0s09/in16; out2: LABEL("+_2");  hv_ctrl:  r23c1s3/ctrl_out2; power:  r23c1s3/power_out2; }
R1307(r20c155s2) { signal: r20c155s1/signal; out1: LABEL("+_1") r23c0s09/in8; out2: LABEL("+_2") r23c0s15/in16;   hv_ctrl:  r23c1s3/ctrl_out3; power:  r23c1s3/power_out3; }
R1307(r20c156s2) { signal: r20c156s1/signal; out1: LABEL("+_1") r23c0s09/in5; out2: LABEL("+_2") r23c0s15/in13;  hv_ctrl:  r23c1s3/ctrl_out4; power:  r23c1s3/power_out4; }
R1307(r20c157s2) { signal: r20c157s1/signal; out1: LABEL("+_1") r23c0s09/in6; out2: LABEL("+_2") r23c0s15/in14;   hv_ctrl:  r23c1s3/ctrl_out5; power:  r23c1s3/power_out5; }
R1307(r20c158s2) { signal: r20c158s1/signal; out1: LABEL("+_1") r23c0s07/in1; out2: LABEL("+_2");  hv_ctrl:  r23c1s3/ctrl_out6; power:  r23c1s3/power_out6; }
R1307(r20c159s2) { signal: r20c159s1/signal; out1: LABEL("+_1") r23c0s07/in11; out2: LABEL("+_2");  hv_ctrl:  r23c1s3/ctrl_out7; power:  r23c1s3/power_out7; }
R1307(r20c160s2) { signal: r20c160s1/signal; out1: LABEL("+_1") r23c0s07/in12; out2: LABEL("+_2");  hv_ctrl:  r23c1s3/ctrl_out8; power:  r23c1s3/power_out8; }
R1307(r20c161s2) { signal: r20c161s1/signal; out1: LABEL("+_1") r23c0s07/in2; out2: LABEL("+_2");  hv_ctrl: r23c2s19/ctrl_out1; power: r23c2s19/power_out1; } // 81 <-> 161
R1307(r20c162s2) { signal: r20c162s1/signal; out1: LABEL("+_1") r23c0s07/in4; out2: LABEL("+_2");  hv_ctrl: r23c2s19/ctrl_out2; power: r23c2s19/power_out2; } // 81 <-> 161

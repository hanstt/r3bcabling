//--------------- VME CRATE -------------//
VME_CRATE(r23c3)
{
  SETTING("CRATE_NO" => "4");
}

RIO4(r23c3s1) 
{
    SETTING("HOSTNAME"=>"r4-12");
}
      
TRIDI(r23c3s2) // IN8 HAS TO BE CHECKED !!!
{
  in_lemo1:	r23c4s19u1/out1; //ok
	in_lemo6:	r23c3s7/nim_out1; //ok
	in_lemo7:	rMASTERc2s1u4/out1; //ok
	in_lemo8:	rMASTERc2s21u3/out3; // makes no sense !!!

	out_lemo1:	r23c4s19u2/in1; //ok
	out_lemo7:	rMASTERc1s2/in_lemo6; //ok
}

ENV1(r23c3s7) // IN1 HAS TO BE CHECKED !!!
{
	in1:	;	

  nim_out1: r23c3s2/in_lemo6; //ok
}

MESYTEC_MADC32(r23c3s12) // OKAY
{
  SERIAL("CBL02");

  SETTING("ADDRESS" => "0x00720000");
  SETTING("VIRTUAL_SLOT" => "2");

  in1_16:  r23c0s17/e1_16; // module 1 //to check
  in17_32: r23c0s15/e1_16; // module 2 //to check
  nim_gate0: r23c4s19u3/out4; //ok
  nim_busy: ;
}

MESYTEC_MADC32(r23c3s13) // OKAY
{
  SERIAL("CBL03");

  SETTING("ADDRESS" => "0x00710000");
  SETTING("VIRTUAL_SLOT" => "1");

  in1_16:  r23c0s13/e1_16; // module 3 //ok
  in17_32: r23c0s11/e1_16; // module 4 //ok
  nim_gate0: r23c4s19u3/out3; //ok
  nim_busy: ;
}
/* Proton MADC */
MESYTEC_MADC32(r23c3s14) // OKAY
{
  SERIAL("CBL04");

  SETTING("ADDRESS" => "0x00700000");
  SETTING("VIRTUAL_SLOT" => "0");

  in1_16:  r23c0s09/e1_16; // module 5 //ok
  in17_32: r23c0s07/e1_16; // module 6 //ok
  nim_gate0: r23c4s19u3/out1; //ok
//   nim_busy: r23c0s1/rc;	// not connected
}

MESYTEC_MADC32(r23c3s15) // OKAY
{
  SERIAL("CBL01");

  SETTING("ADDRESS" => "0x00730000");
  SETTING("VIRTUAL_SLOT" => "3");

  in1_16:  r23c0s05/e1_16; // module 7 //ok
  in17_32: r23c0s03/e1_16; // module 8 //ok
  nim_gate0: r23c4s19u3/out2; //ok
  nim_busy: ;
} 


HDCAN1(r23c3s08) // IN1 HAS TO BE CHECKED !!!
{
  out1_32:	.s21/in129_160;
}

HDCAN1(r23c3s16) // IN1 HAS TO BE CHECKED !!!
{
	in1_16:		r23c0s15/t1_16;       // module 2
	in17_32:	r23c0s17/t1_16;			  // module 1

  out1_32:	.s21/in1_32;
}

HDCAN1(r23c3s17) // OKAY
{
  in1_16:  	r23c0s11/t1_16; // module 4
  in17_32: 	r23c0s13/t1_16; // module 3

  out1_32:  .s21/in33_64;
}

HDCAN1(r23c3s18) // OKAY
{
  in1_16:  	r23c0s07/t1_16; // module 6
  in17_32: 	r23c0s09/t1_16; // module 5

  out1_32:	.s21/in65_96;
}

HDCAN1(r23c3s19) // OKAY
{
  in1_16:  	r23c0s03/t1_16; // module 8
  in17_32: 	r23c0s05/t1_16; // module 7

  out1_32:	.s21/in97_128;
}

VUPROM2(r23c3s21) // OKAY
{
  SERIAL("CBL06");

  SETTING("ADDRESS" => "0x05000000");
  SETTING("VIRTUAL_SLOT" => "4");

  in1_32:			.s16/out1_32; // ok
  in33_64:		.s17/out1_32; // ok
  in65_96:		.s18/out1_32; // ok
  in97_128:		.s19/out1_32; // ok

  in129_160:	.s8/out1_32; // ok

  in_nim: 		r23c4s19u2/out2; // ok

  out1_32: ; // not connected !!!
}

// HDTTL1(r23c3s20)
// {
//   in1_32: .s19/out1_32;
// }
// 

// MESYTEC_MADC32(r23c3s12)
// {
//   SERIAL("CBL05");
// 
//   SETTING("ADDRESS" => "0x00770000");
// 
//   nim_busy: r22c0s07/rc;
// }

// ENV1(r23c3s13)
// {
//   nim_in1:    .s21/out8;
// 
//   in_ecl2_9:  .s13/out_ecl1_8; // Not exactly true as in9 is not connected
//   out_ecl1_8: .s13/in_ecl2_9; 
//   
//   nim_out1:   .s19/in_nim;
//   nim_out2:   .s11/nim_gate0;
//   nim_out3:   .s9/nim_gate0;
//   nim_out4:   .s7/nim_gate0;
//   nim_out5:   .s5/nim_gate0;
// }


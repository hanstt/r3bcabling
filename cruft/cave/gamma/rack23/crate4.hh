//This is the bottom NIM crate in r22 which is nearly empty
NIM_CRATE(r23c4)
{
    ;
}

FL8000_U(r23c4s1u3) // HAS TO BE CHECKED !!!
{
	in: r23c0s23/analog_out;

	out: ;
}

FL8000_U(r23c4s1u4) // HAS TO BE CHECKED !!!
{
	in: r23c0s21/analog_out;

	out: ;
}

SU1200(r23c4s5)
{
	in3:	r22c3s3/e1;
	in4:	r22c3s3/e2;

	analog_out:	.s09u3/in; // ok
}

GG8000(r23c4s17)
{
	;
}

CF8000_U(r23c4s09u3)
{
	in:	.s5/analog_out; // ok

	outB1:	; // not connected
}

LF4000_4_U(r23c4s11u2)	// blue one
{
	in1:	r23c0s19u2/out4;
	in2:	r22c0s21u2/out1;
}

LF4000_4_U(r23c4s11u4)	// blue one
{
	in1:	r23c0s19u1/out4;
	in2:	r22c0s21u1/out1;

}

CF8000_U(r23c4s19u7)
{
	in:	; // HAS TO BE CHECKED !!!

	outB1:	; // HAS TO BE CHECKED !!!
}

LF4000_4_U(r23c4s19u1)
{
	out1:	r23c3s2/in_lemo1; //ok
	
}

LF4000_4_U(r23c4s19u2)
{
	in1:	r23c3s2/out_lemo1; //ok
	
	out2:	r23c3s21/in_nim;
}

LF4000_4_U(r23c4s19u3)
{
	out1:	r23c3s14/nim_gate0; //ok
	out2:	r23c3s15/nim_gate0; //ok
	out3:	r23c3s13/nim_gate0; //ok
	out4:	r23c3s12/nim_gate0; //ok
}


LF4000_4_U(r23c4s23u1)
{
	in1:	; // not connected
	
	out1:	; // not connected
}


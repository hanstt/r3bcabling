//--------------- NIM CRATE -------------//
NIM_CRATE(r23c0)		// Delays for the right half CB proton signals
{
  ;
}


MSCF16(r23c0s03)             //module 16, left
{
  in1:	  r20c137s2/out1;
  in2:	  r20c138s2/out1;


  in5:	  r20c100s2/out1;
  in6:	  r20c104s2/out1;


  in9:	  r20c121s2/out1;	
  in10:	  r20c120s2/out1;
  in11:	  r20c083s2/out1;
  in12:	  r20c122s2/out1;
  in13:	  r20c101s2/out1;
  in14:	  r20c103s2/out1;
  in15:	  r20c102s2/out1;
  in16:	  r20c125s2/out1;

  t1_16:  r23c3s19/in1_16;
  e1_16:  r23c3s15/in17_32;

  or:	r23c0s19u1/in2;
//   rc:	  .c3s11/nim_busy;	// not connected
}

MSCF16(r23c0s05)             //module 15, left
{
  in1:	  r20c098s2/out1;
  in2:	  r20c106s2/out1;
  in3:	  r20c135s2/out1;
  in4:	  r20c140s2/out1;
  in5:	  r20c150s2/out1;
  in6:	  r20c152s2/out1;
  in7:	  r20c151s2/out1;
  in8:	  r20c084s2/out1; 
  in9:	  r20c119s2/out1;	
  in10:	  r20c123s2/out1;
  in13:	  r20c136s2/out1;
  in14:	  r20c139s2/out1;
  in15:	  r20c099s2/out1;
  in16:	  r20c105s2/out1;

  t1_16:  r23c3s19/in17_32;
  e1_16:  r23c3s15/in1_16;

  or: r23c0s19u1/in3;
  // rc: chained to r23c0s01
}

MSCF16(r23c0s07)             //module 14, left
{
  in1:	  r20c158s2/out1;
  in2:	  r20c161s2/out1;
  in3:	  r20c086s2/out1;
  in4:	  r20c162s2/out1;
  in5:	  r20c097s2/out1;
  in6:	  r20c107s2/out1;
  in7:	  r20c134s2/out1;
  in8:	  r20c141s2/out1; 
  in9:	  r20c149s2/out1;	
  in10:	  r20c153s2/out1;
  in11:	  r20c159s2/out1;
  in12:	  r20c160s2/out1;
  in13:	  r20c085s2/out1;
  in14:	  r20c118s2/out1;
  in15:	  r20c124s2/out1;

  t1_16:  r23c3s18/in1_16;
  e1_16:  r23c3s14/in17_32;

  or: r23c0s19u1/in4;
  // rc: chained to r23c0s01
}

MSCF16(r23c0s09)             //module 13, left
{
  in1:	  r20c116s2/out1;
  in2:	  r20c126s2/out1;
  in3:	  r20c087s2/out1;

  in5:	  r20c156s2/out1;
  in6:	  r20c157s2/out1;
  in7:	  r20c147s2/out1;
  in8:	  r20c155s2/out1; 
  in9:	  r20c133s2/out1;	
  in10:	  r20c142s2/out1;
  in11:	  r20c096s2/out1;
  in12:	  r20c108s2/out1;
  in13:	  r20c117s2/out1;
  //in14:	  r20c125s2/out1;
  in15:	  r20c148s2/out1;
  in16:	  r20c154s2/out1;

  t1_16:  r23c3s18/in17_32;
  e1_16:  r23c3s14/in1_16;

  or: r23c0s19u2/in2;
  // rc: chained to r23c0s01
}

MSCF16(r23c0s11)             //module 12, left
{
  in1:	  r20c128s2/out1;
  in2:	  r20c131s2/out1;
 
  
  in5:	  r20c115s2/out1;
  in6:	  r20c127s2/out1;

 
  in9:	  r20c088s2/out1;	
  in10:	  r20c145s2/out1;
  in11:	  r20c144s2/out1;
  in12:	  r20c146s2/out1;
  in13:	  r20c132s2/out1;
  in14:	  r20c143s2/out1;
  in15:	  r20c095s2/out1;
  in16:	  r20c109s2/out1;

  t1_16:  r23c3s17/in1_16;
  e1_16:  r23c3s13/in17_32;

  or: r23c0s19u2/in3;
  // rc: chained to r23c0s01
}

MSCF16(r23c0s13)             //module 11, left
{
  in1:	  r20c091s2/out1;
  in2:	  r20c090s2/out1;
  in3:	  r20c092s2/out1;
  
  in5:	  r20c089s2/out1;
  in6:	  r20c112s2/out1;
  in7:	  r20c114s2/out1;
  in8:	  r20c113s2/out1;
  in9:	  r20c093s2/out1;	
  in10:	  r20c111s2/out1;
  in13:	  r20c129s2/out1;
  in14:	  r20c130s2/out1;
  in15:	  r20c094s2/out1;
  in16:	  r20c110s2/out1;

  t1_16:  r23c3s17/in17_32;
  e1_16:  r23c3s13/in1_16;

  or: r23c0s19u2/in4;
  // rc: chained to r23c0s01
}

MSCF16(r23c0s15)             //module 10 , left
{
  in1:	  r20c127s2/out2;
  in2:	  r20c088s2/out2;
  in3:	  r20c145s2/out2;
  in4:	  r20c144s2/out2;
  in5:	  r20c146s2/out2;
  in6:	  r20c132s2/out2;
  in7:	  r20c143s2/out2;
  in8:	  r20c095s2/out2;
	         
  in9:	  r20c109s2/out2;	
  in10:	  r20c116s2/out2;
  in11:	  r20c126s2/out2;
  in12:	  r20c087s2/out2;
  in13:	  r20c156s2/out2;
  in14:	  r20c157s2/out2;
  in15:	  r20c147s2/out2;
  in16:	  r20c155s2/out2;

  t1_16:  r23c3s16/in1_16;
  e1_16:  r23c3s12/in17_32;

//   or: r23c0s19u2/in1;	// not connected
  // rc: chained to r23c0s17
}

MSCF16(r23c0s17)             //module 9, left
{
  in1:	  r20c091s2/out2;
  in2:	  r20c090s2/out2;
  in3:	  r20c092s2/out2;
  in4:	  r20c089s2/out2;
  in5:	  r20c112s2/out2;
  in6:	  r20c114s2/out2;
  in7:	  r20c113s2/out2;
  in8:	  r20c093s2/out2;
	         
  in9:	  r20c111s2/out2;	
  in10:	  r20c129s2/out2;
  in11:	  r20c130s2/out2;
  in12:	  r20c094s2/out2;
  in13:	  r20c110s2/out2;
  in14:	  r20c128s2/out2;
  in15:	  r20c131s2/out2;
  in16:	  r20c115s2/out2;

  t1_16:  r23c3s16/in17_32;
  e1_16:  r23c3s12/in1_16;
  
//   rc: r23c3s05/nim_busy;	// not connected
  mc: ;
//   or: r23c0s19u2/in2;	// not connected
  mult: ;
}


LF4000_4_U(r23c0s19u1)
{
  in2: r23c0s03/or;
  in3: r23c0s05/or;
  in4: r23c0s07/or;

  out3: r23c0s19u3/in1;
	out4:	r23c4s11u4/in1;
}

LF4000_4_U(r23c0s19u2)
{
  in2: r23c0s09/or;
  in3: r23c0s11/or;
  in4: r23c0s13/or;

  out3: r23c0s19u3/in2;
	out4:	r23c4s11u2/in1;
}

LF4000_4_U(r23c0s19u3)
{
	in1:	r23c0s19u1/out3;
	in2:	r23c0s19u2/out3;

}

SU1200(r23c0s21)
{

	analog_out:	r23c4s1u4/in;
}

SU1200(r23c0s23)
{

	analog_out:	r23c4s1u3/in;
}

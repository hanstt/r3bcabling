NIM_CRATE(r23c1)
{
  ;
}

HV_CTRL_XB_NIM(r23c1s23)
{
 ctrl_in1_8: r22c2s9/ctrl_out32_39;
 ctrl_out1: r20c113s2/hv_ctrl; power_out1: r20c113s2/power;
 ctrl_out2: r20c114s2/hv_ctrl; power_out2: r20c114s2/power;
 ctrl_out3: r20c115s2/hv_ctrl; power_out3: r20c115s2/power;
 ctrl_out4: r20c116s2/hv_ctrl; power_out4: r20c116s2/power;
 ctrl_out5: r20c117s2/hv_ctrl; power_out5: r20c117s2/power;
 ctrl_out6: r20c118s2/hv_ctrl; power_out6: r20c118s2/power;
 ctrl_out7: r20c119s2/hv_ctrl; power_out7: r20c119s2/power;
 ctrl_out8: r20c120s2/hv_ctrl; power_out8: r20c120s2/power;
}

HV_CTRL_XB_NIM(r23c1s19)
{
 ctrl_in1_8: r22c2s11/ctrl_out0_7;
 ctrl_out1: r20c121s2/hv_ctrl; power_out1: r20c121s2/power;
 ctrl_out2: r20c122s2/hv_ctrl; power_out2: r20c122s2/power;
 ctrl_out3: r20c123s2/hv_ctrl; power_out3: r20c123s2/power;
 ctrl_out4: r20c124s2/hv_ctrl; power_out4: r20c124s2/power;
 ctrl_out5: r20c125s2/hv_ctrl; power_out5: r20c125s2/power;
 ctrl_out6: r20c126s2/hv_ctrl; power_out6: r20c126s2/power;
 ctrl_out7: r20c127s2/hv_ctrl; power_out7: r20c127s2/power;
 ctrl_out8: r20c128s2/hv_ctrl; power_out8: r20c128s2/power;
}

HV_CTRL_XB_NIM(r23c1s15)
{
 ctrl_in1_8: r22c2s11/ctrl_out8_15;
 ctrl_out1: r20c129s2/hv_ctrl; power_out1: r20c129s2/power;
 ctrl_out2: r20c130s2/hv_ctrl; power_out2: r20c130s2/power;
 ctrl_out3: r20c131s2/hv_ctrl; power_out3: r20c131s2/power;
 ctrl_out4: r20c132s2/hv_ctrl; power_out4: r20c132s2/power;
 ctrl_out5: r20c133s2/hv_ctrl; power_out5: r20c133s2/power;
 ctrl_out6: r20c134s2/hv_ctrl; power_out6: r20c134s2/power;
 ctrl_out7: r20c135s2/hv_ctrl; power_out7: r20c135s2/power;
 ctrl_out8: r20c136s2/hv_ctrl; power_out8: r20c136s2/power;
}

HV_CTRL_XB_NIM(r23c1s11)
{
 ctrl_in1_8: r22c2s11/ctrl_out16_23;
 ctrl_out1: r20c137s2/hv_ctrl; power_out1: r20c137s2/power;
 ctrl_out2: r20c138s2/hv_ctrl; power_out2: r20c138s2/power;
 ctrl_out3: r20c139s2/hv_ctrl; power_out3: r20c139s2/power;
 ctrl_out4: r20c140s2/hv_ctrl; power_out4: r20c140s2/power;
 ctrl_out5: r20c141s2/hv_ctrl; power_out5: r20c141s2/power;
 ctrl_out6: r20c142s2/hv_ctrl; power_out6: r20c142s2/power;
 ctrl_out7: r20c143s2/hv_ctrl; power_out7: r20c143s2/power;
 ctrl_out8: r20c144s2/hv_ctrl; power_out8: r20c144s2/power;
}

HV_CTRL_XB_NIM(r23c1s7)
{
 ctrl_in1_8: r22c2s11/ctrl_out24_31;
 ctrl_out1: r20c145s2/hv_ctrl; power_out1: r20c145s2/power;
 ctrl_out2: r20c146s2/hv_ctrl; power_out2: r20c146s2/power;
 ctrl_out3: r20c147s2/hv_ctrl; power_out3: r20c147s2/power;
 ctrl_out4: r20c148s2/hv_ctrl; power_out4: r20c148s2/power;
 ctrl_out5: r20c149s2/hv_ctrl; power_out5: r20c149s2/power;
 ctrl_out6: r20c150s2/hv_ctrl; power_out6: r20c150s2/power;
 ctrl_out7: r20c151s2/hv_ctrl; power_out7: r20c151s2/power;
 ctrl_out8: r20c152s2/hv_ctrl; power_out8: r20c152s2/power;
}

HV_CTRL_XB_NIM(r23c1s3)
{
 ctrl_in1_8: r22c2s11/ctrl_out32_39;
 ctrl_out1: r20c153s2/hv_ctrl; power_out1: r20c153s2/power;
 ctrl_out2: r20c154s2/hv_ctrl; power_out2: r20c154s2/power;
 ctrl_out3: r20c155s2/hv_ctrl; power_out3: r20c155s2/power;
 ctrl_out4: r20c156s2/hv_ctrl; power_out4: r20c156s2/power;
 ctrl_out5: r20c157s2/hv_ctrl; power_out5: r20c157s2/power;
 ctrl_out6: r20c158s2/hv_ctrl; power_out6: r20c158s2/power;
 ctrl_out7: r20c159s2/hv_ctrl; power_out7: r20c159s2/power;
 ctrl_out8: r20c160s2/hv_ctrl; power_out8: r20c160s2/power;
}



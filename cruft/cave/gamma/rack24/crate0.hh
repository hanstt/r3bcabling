//--------------- VME CRATE -------------//
VME_CRATE(r24c0)
{
  SETTING("CRATE_NO" => "3");
}

RIO4(r24c0s1) 
{
    SETTING("HOSTNAME"=>"r4-11");
}
      
TRIDI(r24c0s2) // IN8 HAS TO BE CHECKED !!!
{
  in_lemo1:	r22c3s21u4/out1; //ok
	in_lemo6:	r24c0s4/nim_out1; //ok
	in_lemo7:	rMASTERc2s1u4/out2; //ok
	in_lemo8:	rMASTERc2s21u3/out4; // makes no sense !!!

	out_lemo1:	r22c3s23u1/in1; //ok
	out_lemo7:	rMASTERc1s2/in_lemo5; //ok
}


ENV3(r24c0s4) // IN1 HAS TO BE CHECKED !!!
{
	in1:	;	

  nim_out1: r24c0s2/in_lemo6; //ok
}

MESYTEC_MADC32(r24c0s07) // OKAY
{
  SERIAL("CBR02");

  SETTING("ADDRESS" => "0x00720000");
  SETTING("VIRTUAL_SLOT" => "2");

  in1_16:  r22c0s13/e1_16; // module 7 //ok
  in17_32: r22c0s15/e1_16; // module 8 //ok
  nim_gate0: r22c3s21u2/out1; //ok
  nim_busy: ;
}

MESYTEC_MADC32(r24c0s09) // OKAY
{
  SERIAL("CBR03");

  SETTING("ADDRESS" => "0x00710000");
  SETTING("VIRTUAL_SLOT" => "1");

  in1_16:  r22c0s09/e1_16; // module 5 //ok
  in17_32: r22c0s11/e1_16; // module 6 //ok
  nim_gate0: r22c3s21u2/out2; //ok
  nim_busy: ;
}
/* Proton MADC */
MESYTEC_MADC32(r24c0s11) // OKAY
{
  SERIAL("CBR04");

  SETTING("ADDRESS" => "0x00700000");
  SETTING("VIRTUAL_SLOT" => "0");

  in1_16:  r22c0s05/e1_16; // module 3 //ok
  in17_32: r22c0s07/e1_16; // module 4 //ok
  nim_gate0: r22c3s21u2/out3; //ok
  nim_busy: r22c0s05/rc;
}

MESYTEC_MADC32(r24c0s13) // OKAY
{
  SERIAL("CBR01");

  SETTING("ADDRESS" => "0x00730000");
  SETTING("VIRTUAL_SLOT" => "3");

  in1_16:  r22c0s01/e1_16; // module 1 //ok
  in17_32: r22c0s03/e1_16; // module 2 //ok
  nim_gate0: r22c3s21u2/out4; //ok
  nim_busy: ;
} 

HDCAN1(r24c0s16) // IN1 HAS TO BE CHECKED !!!
{
  out1_32:	.s21/in129_160;
}

HDCAN1(r24c0s17) // OKAY
{
  in1_16:  	r22c0s13/t1_16; // module 7
  in17_32: 	r22c0s15/t1_16; // module 8

  out1_32:  .s21/in1_32;
}

HDCAN1(r24c0s18) // OKAY
{
  in1_16:  	r22c0s09/t1_16; // module 5
  in17_32: 	r22c0s11/t1_16; // module 6

  out1_32:	.s21/in33_64;
}

HDCAN1(r24c0s19) // OKAY
{
  in1_16:  	r22c0s05/t1_16; // module 3
  in17_32: 	r22c0s07/t1_16; // module 4

  out1_32:	.s21/in65_96;
}

HDCAN1(r24c0s20) // OKAY
{
  in1_16:  	r22c0s01/t1_16; // module 1
  in17_32: 	r22c0s03/t1_16; // module 2

  out1_32:	.s21/in97_128;
}

VUPROM3(r24c0s21) // OKAY
{
  SERIAL("CBR06");

  SETTING("ADDRESS" => "0x05000000");
  SETTING("VIRTUAL_SLOT" => "4");

  in1_32:			.s17/out1_32; // ok
  in33_64:		.s18/out1_32; // ok
  in65_96:		.s19/out1_32; // ok
  in97_128:		.s20/out1_32; // ok

  in129_160:	.s16/out1_32; // ok

  in_nim: 		r22c3s23u1/out1; // ok

  out1_32: ; // not connected !!!
}



// HDTTL1(r24c0s20)
// {
//   in1_32: .s19/out1_32;
// }
// 

// MESYTEC_MADC32(r24c0s12)
// {
//   SERIAL("CBR05");
// 
//   SETTING("ADDRESS" => "0x00770000");
// 
//   nim_busy: r22c0s07/rc;
// }

// ENV1(r24c0s13)
// {
//   nim_in1:    .s21/out8;
// 
//   in_ecl2_9:  .s13/out_ecl1_8; // Not exactly true as in9 is not connected
//   out_ecl1_8: .s13/in_ecl2_9; 
//   
//   nim_out1:   .s19/in_nim;
//   nim_out2:   .s11/nim_gate0;
//   nim_out3:   .s9/nim_gate0;
//   nim_out4:   .s7/nim_gate0;
//   nim_out5:   .s5/nim_gate0;
// }


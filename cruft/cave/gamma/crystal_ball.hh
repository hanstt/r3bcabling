MODULE(DETECTOR_XB)
{
  HAS_SETTING("PHI" => "%f");
  HAS_SETTING("THETA" => "%f");

  HAS_SETTING("CENTER" => "");

  CONNECTOR(signal,SIGNAL | TENSION);
}

MODULE(R1307)
{
  CONNECTOR(signal,SIGNAL | TENSION);

  /* Base for crystal ball. */

  LEMO_OUTPUT(out1,SIGNAL); //standard, pre-amplified output, used for gammas
  LEMO_OUTPUT(out2,SIGNAL); //modified output, coupled out omitting the last PMT dynode stage, resulting in ~30 times lower charges, used for highly energetic recoil protons.

  HAS_SETTING("GAIN ADJ" => "%f");
  
  SERIAL_CON(hv_ctrl,TENSION);

  CONNECTOR(power); // +6V -6V

  LEMO_INPUT(test);
}


MODULE(HV_CTRL_XB_VME)
{
  CONNECTOR(ctrl_out0_39);  
}

MODULE(HV_CTRL_XB_NIM)
{
  CONNECTOR(ctrl_in1_8);   // backside

  SERIAL_CON(ctrl_out1_8); // frontside
  CONNECTOR(power_out1_8); // frontside
}

/* Naming scheme for crystal ball:
 *
 * DETECTOR_XB ('the pm tube', r20cXXXs1), XXX is the crystal number.
 *
 * R1307 (the active base sitting on the detector, r20cXXXs2), XXX is
 * the crystal number.
 *
 * JOINER_8(r21cXX) is the cable joiners hanging on the ball.
 *
 * r20 is thus the ball itself.
 * r21 is the cable joiners hanging on the ball.
 *
 * r22 is the rack to the right (with VME).
 * r22c0 is NIM (top, delays for proton signals)
 * r22c1 is NIM (upper)
 * r24c0 is VME (upper)
 *
 * r23 is the rack to the left (without VME).
 * r23c0 is NIM (top, delays for proton signals)
 * r23c1 is NIM (upper)
 * r23c2 is NIM (lower)
 * r23c3 is VME 
 */

//#include "rack22/crate1.hh"
//#include "rack22/crate2.hh"

//#include "rack23/crate1.hh"
//#include "rack23/crate2.hh"
//#include "rack23/crate3.hh" //new NIM crate with the Summing of the XB signals, e.g.


DETECTOR_XB(r20c001s1) { LABEL("XB001"); signal: r20c001s2/signal; SETTING("THETA"=>" 90.0 "); SETTING("PHI"=>"180.03");}
DETECTOR_XB(r20c002s1) { LABEL("XB002"); signal: r20c002s2/signal; SETTING("THETA"=>" 89.99"); SETTING("PHI"=>"196.26");}
DETECTOR_XB(r20c003s1) { LABEL("XB003"); signal: r20c003s2/signal; SETTING("THETA"=>" 73.96"); SETTING("PHI"=>"188.3 ");}
DETECTOR_XB(r20c004s1) { LABEL("XB004"); signal: r20c004s2/signal; SETTING("THETA"=>" 73.96"); SETTING("PHI"=>"171.7 ");}
DETECTOR_XB(r20c005s1) { LABEL("XB005"); signal: r20c005s2/signal; SETTING("THETA"=>" 89.99"); SETTING("PHI"=>"163.74");}
DETECTOR_XB(r20c006s1) { LABEL("XB006"); signal: r20c006s2/signal; SETTING("THETA"=>"106.04"); SETTING("PHI"=>"171.7 ");}
DETECTOR_XB(r20c007s1) { LABEL("XB007"); signal: r20c007s2/signal; SETTING("THETA"=>"106.04"); SETTING("PHI"=>"188.3 ");}
DETECTOR_XB(r20c008s1) { LABEL("XB008"); signal: r20c008s2/signal; SETTING("THETA"=>"104.7 "); SETTING("PHI"=>"206.82");}
DETECTOR_XB(r20c009s1) { LABEL("XB009"); signal: r20c009s2/signal; SETTING("THETA"=>" 89.98"); SETTING("PHI"=>"211.72");}
DETECTOR_XB(r20c010s1) { LABEL("XB010"); signal: r20c010s2/signal; SETTING("THETA"=>" 75.3 "); SETTING("PHI"=>"206.82");}
DETECTOR_XB(r20c011s1) { LABEL("XB011"); signal: r20c011s2/signal; SETTING("THETA"=>" 60.02"); SETTING("PHI"=>"200.9 ");}
DETECTOR_XB(r20c012s1) { LABEL("XB012"); signal: r20c012s2/signal; SETTING("THETA"=>" 59.83"); SETTING("PHI"=>"179.95");}
DETECTOR_XB(r20c013s1) { LABEL("XB013"); signal: r20c013s2/signal; SETTING("THETA"=>" 60.02"); SETTING("PHI"=>"159.1 ");}
DETECTOR_XB(r20c014s1) { LABEL("XB014"); signal: r20c014s2/signal; SETTING("THETA"=>" 75.3 "); SETTING("PHI"=>"153.18");}
DETECTOR_XB(r20c015s1) { LABEL("XB015"); signal: r20c015s2/signal; SETTING("THETA"=>" 89.98"); SETTING("PHI"=>"148.28");}
DETECTOR_XB(r20c016s1) { LABEL("XB016"); signal: r20c016s2/signal; SETTING("THETA"=>"104.7 "); SETTING("PHI"=>"153.18");}
DETECTOR_XB(r20c017s1) { LABEL("XB017"); signal: r20c017s2/signal; SETTING("THETA"=>"119.98"); SETTING("PHI"=>"159.1 ");}
DETECTOR_XB(r20c018s1) { LABEL("XB018"); signal: r20c018s2/signal; SETTING("THETA"=>"120.17"); SETTING("PHI"=>"179.95");}
DETECTOR_XB(r20c019s1) { LABEL("XB019"); signal: r20c019s2/signal; SETTING("THETA"=>"119.98"); SETTING("PHI"=>"200.9 ");}
DETECTOR_XB(r20c020s1) { LABEL("XB020"); signal: r20c020s2/signal; SETTING("THETA"=>"116.67"); SETTING("PHI"=>"221.02");}
DETECTOR_XB(r20c021s1) { LABEL("XB021"); signal: r20c021s2/signal; SETTING("THETA"=>" 99.03"); SETTING("PHI"=>"224.33");}
DETECTOR_XB(r20c022s1) { LABEL("XB022"); signal: r20c022s2/signal; SETTING("THETA"=>" 80.97"); SETTING("PHI"=>"224.33");}
DETECTOR_XB(r20c023s1) { LABEL("XB023"); signal: r20c023s2/signal; SETTING("THETA"=>" 63.33"); SETTING("PHI"=>"221.02");}
DETECTOR_XB(r20c024s1) { LABEL("XB024"); signal: r20c024s2/signal; SETTING("THETA"=>" 47.58"); SETTING("PHI"=>"217.37");}
DETECTOR_XB(r20c025s1) { LABEL("XB025"); signal: r20c025s2/signal; SETTING("THETA"=>" 45.05"); SETTING("PHI"=>"192.76");}
DETECTOR_XB(r20c026s1) { LABEL("XB026"); signal: r20c026s2/signal; SETTING("THETA"=>" 45.05"); SETTING("PHI"=>"167.24");}
DETECTOR_XB(r20c027s1) { LABEL("XB027"); signal: r20c027s2/signal; SETTING("THETA"=>" 47.58"); SETTING("PHI"=>"142.63");}
DETECTOR_XB(r20c028s1) { LABEL("XB028"); signal: r20c028s2/signal; SETTING("THETA"=>" 63.33"); SETTING("PHI"=>"138.98");}
DETECTOR_XB(r20c029s1) { LABEL("XB029"); signal: r20c029s2/signal; SETTING("THETA"=>" 80.97"); SETTING("PHI"=>"135.67");}
DETECTOR_XB(r20c030s1) { LABEL("XB030"); signal: r20c030s2/signal; SETTING("THETA"=>" 99.03"); SETTING("PHI"=>"135.67");}
DETECTOR_XB(r20c031s1) { LABEL("XB031"); signal: r20c031s2/signal; SETTING("THETA"=>"116.67"); SETTING("PHI"=>"138.98");}
DETECTOR_XB(r20c032s1) { LABEL("XB032"); signal: r20c032s2/signal; SETTING("THETA"=>"132.42"); SETTING("PHI"=>"142.63");}
DETECTOR_XB(r20c033s1) { LABEL("XB033"); signal: r20c033s2/signal; SETTING("THETA"=>"134.95"); SETTING("PHI"=>"167.24");}
DETECTOR_XB(r20c034s1) { LABEL("XB034"); signal: r20c034s2/signal; SETTING("THETA"=>"134.95"); SETTING("PHI"=>"192.76");}
DETECTOR_XB(r20c035s1) { LABEL("XB035"); signal: r20c035s2/signal; SETTING("THETA"=>"132.42"); SETTING("PHI"=>"217.37");}
DETECTOR_XB(r20c036s1) { LABEL("XB036"); signal: r20c036s2/signal; SETTING("THETA"=>"125.95"); SETTING("PHI"=>"236.35");}
DETECTOR_XB(r20c037s1) { LABEL("XB037"); signal: r20c037s2/signal; SETTING("THETA"=>"108.01"); SETTING("PHI"=>"238.25");}
DETECTOR_XB(r20c038s1) { LABEL("XB038"); signal: r20c038s2/signal; SETTING("THETA"=>" 89.91"); SETTING("PHI"=>"239.83");}
DETECTOR_XB(r20c039s1) { LABEL("XB039"); signal: r20c039s2/signal; SETTING("THETA"=>" 71.99"); SETTING("PHI"=>"238.25");}
DETECTOR_XB(r20c040s1) { LABEL("XB040"); signal: r20c040s2/signal; SETTING("THETA"=>" 54.05"); SETTING("PHI"=>"236.35");}
DETECTOR_XB(r20c041s1) { LABEL("XB041"); signal: r20c041s2/signal; SETTING("THETA"=>" 30.31"); SETTING("PHI"=>"210.1 ");}
DETECTOR_XB(r20c042s1) { LABEL("XB042"); signal: r20c042s2/signal; SETTING("THETA"=>" 31.72"); SETTING("PHI"=>"180.06");}
DETECTOR_XB(r20c043s1) { LABEL("XB043"); signal: r20c043s2/signal; SETTING("THETA"=>" 30.31"); SETTING("PHI"=>"149.9 ");}
DETECTOR_XB(r20c044s1) { LABEL("XB044"); signal: r20c044s2/signal; SETTING("THETA"=>" 54.05"); SETTING("PHI"=>"123.65");}
DETECTOR_XB(r20c045s1) { LABEL("XB045"); signal: r20c045s2/signal; SETTING("THETA"=>" 71.99"); SETTING("PHI"=>"121.75");}
DETECTOR_XB(r20c046s1) { LABEL("XB046"); signal: r20c046s2/signal; SETTING("THETA"=>" 89.91"); SETTING("PHI"=>"120.17");}
DETECTOR_XB(r20c047s1) { LABEL("XB047"); signal: r20c047s2/signal; SETTING("THETA"=>"108.01"); SETTING("PHI"=>"121.75");}
DETECTOR_XB(r20c048s1) { LABEL("XB048"); signal: r20c048s2/signal; SETTING("THETA"=>"125.95"); SETTING("PHI"=>"123.65");}
DETECTOR_XB(r20c049s1) { LABEL("XB049"); signal: r20c049s2/signal; SETTING("THETA"=>"149.69"); SETTING("PHI"=>"149.9 ");}
DETECTOR_XB(r20c050s1) { LABEL("XB050"); signal: r20c050s2/signal; SETTING("THETA"=>"148.28"); SETTING("PHI"=>"180.06");}
DETECTOR_XB(r20c051s1) { LABEL("XB051"); signal: r20c051s2/signal; SETTING("THETA"=>"149.69"); SETTING("PHI"=>"210.1 ");}
DETECTOR_XB(r20c052s1) { LABEL("XB052"); signal: r20c052s2/signal; SETTING("THETA"=>"144.05"); SETTING("PHI"=>"238.21");}
DETECTOR_XB(r20c053s1) { LABEL("XB053"); signal: r20c053s2/signal; SETTING("THETA"=>"133.67"); SETTING("PHI"=>"257.49");}
DETECTOR_XB(r20c054s1) { LABEL("XB054"); signal: r20c054s2/signal; SETTING("THETA"=>"115.92"); SETTING("PHI"=>"253.63");}
DETECTOR_XB(r20c055s1) { LABEL("XB055"); signal: r20c055s2/signal; SETTING("THETA"=>" 98.01"); SETTING("PHI"=>"253.81");}
DETECTOR_XB(r20c056s1) { LABEL("XB056"); signal: r20c056s2/signal; SETTING("THETA"=>" 81.99"); SETTING("PHI"=>"253.81");}
DETECTOR_XB(r20c057s1) { LABEL("XB057"); signal: r20c057s2/signal; SETTING("THETA"=>" 64.08"); SETTING("PHI"=>"253.63");}
DETECTOR_XB(r20c058s1) { LABEL("XB058"); signal: r20c058s2/signal; SETTING("THETA"=>" 46.33"); SETTING("PHI"=>"257.49");}
DETECTOR_XB(r20c059s1) { LABEL("XB059"); signal: r20c059s2/signal; SETTING("THETA"=>" 35.95"); SETTING("PHI"=>"238.21");}
DETECTOR_XB(r20c060s1) { LABEL("XB060"); signal: r20c060s2/signal; SETTING("THETA"=>" 17.98"); SETTING("PHI"=>"243.25");}
DETECTOR_XB(r20c061s1) { LABEL("XB061"); signal: r20c061s2/signal; SETTING("THETA"=>" 16.26"); SETTING("PHI"=>"180.11");}
DETECTOR_XB(r20c062s1) { LABEL("XB062"); signal: r20c062s2/signal; SETTING("THETA"=>" 17.98"); SETTING("PHI"=>"116.75");}
DETECTOR_XB(r20c063s1) { LABEL("XB063"); signal: r20c063s2/signal; SETTING("THETA"=>" 35.95"); SETTING("PHI"=>"121.79");}
DETECTOR_XB(r20c064s1) { LABEL("XB064"); signal: r20c064s2/signal; SETTING("THETA"=>" 46.33"); SETTING("PHI"=>"102.51");}
DETECTOR_XB(r20c065s1) { LABEL("XB065"); signal: r20c065s2/signal; SETTING("THETA"=>" 64.08"); SETTING("PHI"=>"106.37");}
DETECTOR_XB(r20c066s1) { LABEL("XB066"); signal: r20c066s2/signal; SETTING("THETA"=>" 81.99"); SETTING("PHI"=>"106.19");}
DETECTOR_XB(r20c067s1) { LABEL("XB067"); signal: r20c067s2/signal; SETTING("THETA"=>" 98.01"); SETTING("PHI"=>"106.19");}
DETECTOR_XB(r20c068s1) { LABEL("XB068"); signal: r20c068s2/signal; SETTING("THETA"=>"115.92"); SETTING("PHI"=>"106.37");}
DETECTOR_XB(r20c069s1) { LABEL("XB069"); signal: r20c069s2/signal; SETTING("THETA"=>"133.67"); SETTING("PHI"=>"102.51");}
DETECTOR_XB(r20c070s1) { LABEL("XB070"); signal: r20c070s2/signal; SETTING("THETA"=>"144.05"); SETTING("PHI"=>"121.79");}
DETECTOR_XB(r20c071s1) { LABEL("XB071"); signal: r20c071s2/signal; SETTING("THETA"=>"162.02"); SETTING("PHI"=>"116.75");}
DETECTOR_XB(r20c072s1) { LABEL("XB072"); signal: r20c072s2/signal; SETTING("THETA"=>"163.74"); SETTING("PHI"=>"180.11");}
DETECTOR_XB(r20c073s1) { LABEL("XB073"); signal: r20c073s2/signal; SETTING("THETA"=>"162.02"); SETTING("PHI"=>"243.25");}
DETECTOR_XB(r20c074s1) { LABEL("XB074"); signal: r20c074s2/signal; SETTING("THETA"=>"149.86"); SETTING("PHI"=>"270.12");}
DETECTOR_XB(r20c075s1) { LABEL("XB075"); signal: r20c075s2/signal; SETTING("THETA"=>"121.75"); SETTING("PHI"=>"270.0 ");}
DETECTOR_XB(r20c076s1) { LABEL("XB076"); signal: r20c076s2/signal; SETTING("THETA"=>"106.29"); SETTING("PHI"=>"270.0 ");}
//DETECTOR_XB(r20c077s1) { LABEL("XB_pTest_1"); signal: r20c077s2/signal; /*SETTING("THETA"=>" 90.0 "); SETTING("PHI"=>"270.0 ");*/} //p-test crystal behind TFW in 30.Apr/01.May 2007 s223 proton test run.
DETECTOR_XB(r20c078s1) { LABEL("XB078"); signal: r20c078s2/signal; SETTING("THETA"=>" 73.71"); SETTING("PHI"=>"270.0 ");}
DETECTOR_XB(r20c079s1) { LABEL("XB079"); signal: r20c079s2/signal; SETTING("THETA"=>" 58.25"); SETTING("PHI"=>"270.0 ");}
DETECTOR_XB(r20c080s1) { LABEL("XB080"); signal: r20c080s2/signal; SETTING("THETA"=>" 30.14"); SETTING("PHI"=>"270.12");}
//DETECTOR_XB(r20c081s1) { LABEL("XB081"); signal: r20c081s2/signal; SETTING("THETA"=>"  0.03"); SETTING("PHI"=>"270.0 ");}
//DETECTOR_XB(r20c082s1) { LABEL("XB082"); signal: r20c082s2/signal; SETTING("THETA"=>"179.96"); SETTING("PHI"=>"225.0 ");}
DETECTOR_XB(r20c083s1) { LABEL("XB083"); signal: r20c083s2/signal; SETTING("THETA"=>"149.86"); SETTING("PHI"=>" 89.88");}
DETECTOR_XB(r20c084s1) { LABEL("XB084"); signal: r20c084s2/signal; SETTING("THETA"=>"121.75"); SETTING("PHI"=>" 90.0 ");}
DETECTOR_XB(r20c085s1) { LABEL("XB085"); signal: r20c085s2/signal; SETTING("THETA"=>"106.29"); SETTING("PHI"=>" 90.0 ");}
DETECTOR_XB(r20c086s1) { LABEL("XB086"); signal: r20c086s2/signal; SETTING("THETA"=>" 90.0 "); SETTING("PHI"=>" 90.0 ");}
DETECTOR_XB(r20c087s1) { LABEL("XB087"); signal: r20c087s2/signal; SETTING("THETA"=>" 73.71"); SETTING("PHI"=>" 90.0 ");}
DETECTOR_XB(r20c088s1) { LABEL("XB088"); signal: r20c088s2/signal; SETTING("THETA"=>" 58.25"); SETTING("PHI"=>" 90.0 ");}
DETECTOR_XB(r20c089s1) { LABEL("XB089"); signal: r20c089s2/signal; SETTING("THETA"=>" 30.14"); SETTING("PHI"=>" 89.88");}
DETECTOR_XB(r20c090s1) { LABEL("XB090"); signal: r20c090s2/signal; SETTING("THETA"=>" 17.98"); SETTING("PHI"=>" 63.25");}
DETECTOR_XB(r20c091s1) { LABEL("XB091"); signal: r20c091s2/signal; SETTING("THETA"=>" 16.26"); SETTING("PHI"=>"359.89");}
DETECTOR_XB(r20c092s1) { LABEL("XB092"); signal: r20c092s2/signal; SETTING("THETA"=>" 17.98"); SETTING("PHI"=>"296.75");}
DETECTOR_XB(r20c093s1) { LABEL("XB093"); signal: r20c093s2/signal; SETTING("THETA"=>" 35.95"); SETTING("PHI"=>"301.79");}
DETECTOR_XB(r20c094s1) { LABEL("XB094"); signal: r20c094s2/signal; SETTING("THETA"=>" 46.33"); SETTING("PHI"=>"282.51");}
DETECTOR_XB(r20c095s1) { LABEL("XB095"); signal: r20c095s2/signal; SETTING("THETA"=>" 64.08"); SETTING("PHI"=>"286.37");}
DETECTOR_XB(r20c096s1) { LABEL("XB096"); signal: r20c096s2/signal; SETTING("THETA"=>" 81.99"); SETTING("PHI"=>"286.19");}
DETECTOR_XB(r20c097s1) { LABEL("XB097"); signal: r20c097s2/signal; SETTING("THETA"=>" 98.01"); SETTING("PHI"=>"286.19");}
DETECTOR_XB(r20c098s1) { LABEL("XB098"); signal: r20c098s2/signal; SETTING("THETA"=>"115.92"); SETTING("PHI"=>"286.37");}
DETECTOR_XB(r20c099s1) { LABEL("XB099"); signal: r20c099s2/signal; SETTING("THETA"=>"133.67"); SETTING("PHI"=>"282.51");}
DETECTOR_XB(r20c100s1) { LABEL("XB100"); signal: r20c100s2/signal; SETTING("THETA"=>"144.05"); SETTING("PHI"=>"301.79");}
DETECTOR_XB(r20c101s1) { LABEL("XB101"); signal: r20c101s2/signal; SETTING("THETA"=>"162.02"); SETTING("PHI"=>"296.75");}
DETECTOR_XB(r20c102s1) { LABEL("XB102"); signal: r20c102s2/signal; SETTING("THETA"=>"163.74"); SETTING("PHI"=>"359.89");}
DETECTOR_XB(r20c103s1) { LABEL("XB103"); signal: r20c103s2/signal; SETTING("THETA"=>"162.02"); SETTING("PHI"=>" 63.25");}
DETECTOR_XB(r20c104s1) { LABEL("XB104"); signal: r20c104s2/signal; SETTING("THETA"=>"144.05"); SETTING("PHI"=>" 58.21");}
DETECTOR_XB(r20c105s1) { LABEL("XB105"); signal: r20c105s2/signal; SETTING("THETA"=>"133.67"); SETTING("PHI"=>" 77.49");}
DETECTOR_XB(r20c106s1) { LABEL("XB106"); signal: r20c106s2/signal; SETTING("THETA"=>"115.92"); SETTING("PHI"=>" 73.63");}
DETECTOR_XB(r20c107s1) { LABEL("XB107"); signal: r20c107s2/signal; SETTING("THETA"=>" 98.01"); SETTING("PHI"=>" 73.81");}
DETECTOR_XB(r20c108s1) { LABEL("XB108"); signal: r20c108s2/signal; SETTING("THETA"=>" 81.99"); SETTING("PHI"=>" 73.81");}
DETECTOR_XB(r20c109s1) { LABEL("XB109"); signal: r20c109s2/signal; SETTING("THETA"=>" 64.08"); SETTING("PHI"=>" 73.63");}
DETECTOR_XB(r20c110s1) { LABEL("XB110"); signal: r20c110s2/signal; SETTING("THETA"=>" 46.33"); SETTING("PHI"=>" 77.49");}
DETECTOR_XB(r20c111s1) { LABEL("XB111"); signal: r20c111s2/signal; SETTING("THETA"=>" 35.95"); SETTING("PHI"=>" 58.21");}
DETECTOR_XB(r20c112s1) { LABEL("XB112"); signal: r20c112s2/signal; SETTING("THETA"=>" 30.31"); SETTING("PHI"=>" 30.1 ");}
DETECTOR_XB(r20c113s1) { LABEL("XB113"); signal: r20c113s2/signal; SETTING("THETA"=>" 31.72"); SETTING("PHI"=>"359.94");}
DETECTOR_XB(r20c114s1) { LABEL("XB114"); signal: r20c114s2/signal; SETTING("THETA"=>" 30.31"); SETTING("PHI"=>"329.9 ");}
DETECTOR_XB(r20c115s1) { LABEL("XB115"); signal: r20c115s2/signal; SETTING("THETA"=>" 54.05"); SETTING("PHI"=>"303.65");}
DETECTOR_XB(r20c116s1) { LABEL("XB116"); signal: r20c116s2/signal; SETTING("THETA"=>" 71.99"); SETTING("PHI"=>"301.75");}
DETECTOR_XB(r20c117s1) { LABEL("XB117"); signal: r20c117s2/signal; SETTING("THETA"=>" 89.91"); SETTING("PHI"=>"300.17");}
DETECTOR_XB(r20c118s1) { LABEL("XB118"); signal: r20c118s2/signal; SETTING("THETA"=>"108.01"); SETTING("PHI"=>"301.75");}
DETECTOR_XB(r20c119s1) { LABEL("XB119"); signal: r20c119s2/signal; SETTING("THETA"=>"125.95"); SETTING("PHI"=>"303.65");}
DETECTOR_XB(r20c120s1) { LABEL("XB120"); signal: r20c120s2/signal; SETTING("THETA"=>"149.69"); SETTING("PHI"=>"329.9 ");}
DETECTOR_XB(r20c121s1) { LABEL("XB121"); signal: r20c121s2/signal; SETTING("THETA"=>"148.28"); SETTING("PHI"=>"359.94");}
DETECTOR_XB(r20c122s1) { LABEL("XB122"); signal: r20c122s2/signal; SETTING("THETA"=>"149.96"); SETTING("PHI"=>" 30.1 ");}
DETECTOR_XB(r20c123s1) { LABEL("XB123"); signal: r20c123s2/signal; SETTING("THETA"=>"125.95"); SETTING("PHI"=>" 56.25");}
DETECTOR_XB(r20c124s1) { LABEL("XB124"); signal: r20c124s2/signal; SETTING("THETA"=>"108.01"); SETTING("PHI"=>" 58.25");}
DETECTOR_XB(r20c125s1) { LABEL("XB125"); signal: r20c125s2/signal; SETTING("THETA"=>" 89.91"); SETTING("PHI"=>" 59.83");}
DETECTOR_XB(r20c126s1) { LABEL("XB126"); signal: r20c126s2/signal; SETTING("THETA"=>" 71.99"); SETTING("PHI"=>" 58.25");}
DETECTOR_XB(r20c127s1) { LABEL("XB127"); signal: r20c127s2/signal; SETTING("THETA"=>" 54.05"); SETTING("PHI"=>" 56.35");}
DETECTOR_XB(r20c128s1) { LABEL("XB128"); signal: r20c128s2/signal; SETTING("THETA"=>" 47.58"); SETTING("PHI"=>" 37.37");}
DETECTOR_XB(r20c129s1) { LABEL("XB129"); signal: r20c129s2/signal; SETTING("THETA"=>" 45.05"); SETTING("PHI"=>" 12.76");}
DETECTOR_XB(r20c130s1) { LABEL("XB130"); signal: r20c130s2/signal; SETTING("THETA"=>" 45.05"); SETTING("PHI"=>"347.24");}
DETECTOR_XB(r20c131s1) { LABEL("XB131"); signal: r20c131s2/signal; SETTING("THETA"=>" 47.58"); SETTING("PHI"=>"322.63");}
DETECTOR_XB(r20c132s1) { LABEL("XB132"); signal: r20c132s2/signal; SETTING("THETA"=>" 63.33"); SETTING("PHI"=>"318.98");}
DETECTOR_XB(r20c133s1) { LABEL("XB133"); signal: r20c133s2/signal; SETTING("THETA"=>" 80.97"); SETTING("PHI"=>"315.67");}
DETECTOR_XB(r20c134s1) { LABEL("XB134"); signal: r20c134s2/signal; SETTING("THETA"=>" 99.03"); SETTING("PHI"=>"315.67");}
DETECTOR_XB(r20c135s1) { LABEL("XB135"); signal: r20c135s2/signal; SETTING("THETA"=>"116.67"); SETTING("PHI"=>"318.98");}
DETECTOR_XB(r20c136s1) { LABEL("XB136"); signal: r20c136s2/signal; SETTING("THETA"=>"132.32"); SETTING("PHI"=>"322.63");}
DETECTOR_XB(r20c137s1) { LABEL("XB137"); signal: r20c137s2/signal; SETTING("THETA"=>"134.95"); SETTING("PHI"=>"347.24");}
DETECTOR_XB(r20c138s1) { LABEL("XB138"); signal: r20c138s2/signal; SETTING("THETA"=>"134.95"); SETTING("PHI"=>" 12.76");}
DETECTOR_XB(r20c139s1) { LABEL("XB139"); signal: r20c139s2/signal; SETTING("THETA"=>"132.42"); SETTING("PHI"=>" 37.37");}
DETECTOR_XB(r20c140s1) { LABEL("XB140"); signal: r20c140s2/signal; SETTING("THETA"=>"116.67"); SETTING("PHI"=>" 41.02");}
DETECTOR_XB(r20c141s1) { LABEL("XB141"); signal: r20c141s2/signal; SETTING("THETA"=>" 99.03"); SETTING("PHI"=>" 44.33");}
DETECTOR_XB(r20c142s1) { LABEL("XB142"); signal: r20c142s2/signal; SETTING("THETA"=>" 80.97"); SETTING("PHI"=>" 44.33");}
DETECTOR_XB(r20c143s1) { LABEL("XB143"); signal: r20c143s2/signal; SETTING("THETA"=>" 63.33"); SETTING("PHI"=>" 41.02");}
DETECTOR_XB(r20c144s1) { LABEL("XB144"); signal: r20c144s2/signal; SETTING("THETA"=>" 60.02"); SETTING("PHI"=>" 20.9 ");}
DETECTOR_XB(r20c145s1) { LABEL("XB145"); signal: r20c145s2/signal; SETTING("THETA"=>" 59.83"); SETTING("PHI"=>"  0.05");}
DETECTOR_XB(r20c146s1) { LABEL("XB146"); signal: r20c146s2/signal; SETTING("THETA"=>" 60.02"); SETTING("PHI"=>"339.1 ");}
DETECTOR_XB(r20c147s1) { LABEL("XB147"); signal: r20c147s2/signal; SETTING("THETA"=>" 75.3 "); SETTING("PHI"=>"333.18");}
DETECTOR_XB(r20c148s1) { LABEL("XB148"); signal: r20c148s2/signal; SETTING("THETA"=>" 89.98"); SETTING("PHI"=>"328.28");}
DETECTOR_XB(r20c149s1) { LABEL("XB149"); signal: r20c149s2/signal; SETTING("THETA"=>"104.7 "); SETTING("PHI"=>"333.18");}
DETECTOR_XB(r20c150s1) { LABEL("XB150"); signal: r20c150s2/signal; SETTING("THETA"=>"119.98"); SETTING("PHI"=>"339.1 ");}
DETECTOR_XB(r20c151s1) { LABEL("XB151"); signal: r20c151s2/signal; SETTING("THETA"=>"120.17"); SETTING("PHI"=>"  0.05");}
DETECTOR_XB(r20c152s1) { LABEL("XB152"); signal: r20c152s2/signal; SETTING("THETA"=>"119.98"); SETTING("PHI"=>" 20.9 ");}
DETECTOR_XB(r20c153s1) { LABEL("XB153"); signal: r20c153s2/signal; SETTING("THETA"=>"104.7 "); SETTING("PHI"=>" 26.82");}
DETECTOR_XB(r20c154s1) { LABEL("XB154"); signal: r20c154s2/signal; SETTING("THETA"=>" 89.98"); SETTING("PHI"=>" 31.72");}
DETECTOR_XB(r20c155s1) { LABEL("XB155"); signal: r20c155s2/signal; SETTING("THETA"=>" 75.3 "); SETTING("PHI"=>" 26.82");}
DETECTOR_XB(r20c156s1) { LABEL("XB156"); signal: r20c156s2/signal; SETTING("THETA"=>" 73.96"); SETTING("PHI"=>"  8.3 ");}
DETECTOR_XB(r20c157s1) { LABEL("XB157"); signal: r20c157s2/signal; SETTING("THETA"=>" 73.96"); SETTING("PHI"=>"351.7 ");}
DETECTOR_XB(r20c158s1) { LABEL("XB158"); signal: r20c158s2/signal; SETTING("THETA"=>" 89.99"); SETTING("PHI"=>"343.74");}
DETECTOR_XB(r20c159s1) { LABEL("XB159"); signal: r20c159s2/signal; SETTING("THETA"=>"106.04"); SETTING("PHI"=>"351.7 ");}
DETECTOR_XB(r20c160s1) { LABEL("XB160"); signal: r20c160s2/signal; SETTING("THETA"=>"106.04"); SETTING("PHI"=>"  8.3 ");}
DETECTOR_XB(r20c161s1) { LABEL("XB161"); signal: r20c161s2/signal; SETTING("THETA"=>" 89.99"); SETTING("PHI"=>" 16.26");}
DETECTOR_XB(r20c162s1) { LABEL("XB162"); signal: r20c162s2/signal; SETTING("THETA"=>" 90.0 "); SETTING("PHI"=>"359.97");}


//#include "rack20/XB.hh"

#ifdef USE_NEW_XB_CABLING

// Right
//#include "rack22/crate0.hh"
//#include "rack24/crate0.hh" //with r4_11 processor

// Left
//#include "rack23/crate0.hh"
//#include "rack23/crate3.hh"

#else //USE_NEW_XB_CABLING

//#include "rack21/joiner.hh"

#endif //USE_NEW_XB_CABLING

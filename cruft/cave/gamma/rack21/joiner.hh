//joiners updated by Sean 16/08/06. note "cable x" denotes the label on the
//cable connecting joiner to split card

JOINER_8(r21c1)                //cable A
{
/* in1: r20c001s2/out1;              
 in2: r20c002s2/out1;
 in3: r20c003s2/out1;
 in4: r20c004s2/out1;
 in5: r20c005s2/out1;
 in6: r20c006s2/out1;
 in7: r20c007s2/out1;
 in8: r20c008s2/out1;*/

 out1_8: r13c3s12/in1_8;
}

JOINER_8(r21c2)               //cable B
{
 /*in1: r20c073s2/out1;					// signal cables of 009 and 073 swapped at PP
 in2: r20c010s2/out1;
 in3: r20c011s2/out1;
 in4: r20c012s2/out1;
 in5: r20c013s2/out1;
 in6: r20c014s2/out1;
 in7: r20c015s2/out1;
 in8: r20c016s2/out1;*/

 out1_8: r13c3s13/in1_8;
}

JOINER_8(r21c3)               //cable C
{
/* in1: r20c017s2/out1;
 in2: r20c018s2/out1;
 in3: r20c019s2/out1;
 in4: r20c020s2/out1;
 in5: r20c021s2/out1;
 in6: r20c022s2/out1;
 in7: r20c023s2/out1;
 in8: r20c024s2/out1;*/

 out1_8: r13c3s14/in1_8;
}

JOINER_8(r21c4)              //cable D
{
 /*in1: r20c025s2/out1;
 in2: r20c026s2/out1;
 in3: r20c027s2/out1;
 in4: r20c028s2/out1;
 in5: r20c029s2/out1;
 in6: r20c030s2/out1;
 in7: r20c031s2/out1;
 in8: r20c032s2/out1;*/

 out1_8: r13c3s15/in1_8;
}

JOINER_8(r21c5)              //cable E
{
 /*in1: r20c033s2/out1;
 in2: r20c034s2/out1;
 in3: r20c035s2/out1;
 in4: r20c036s2/out1;
 in5: r20c037s2/out1;
 in6: r20c038s2/out1;
 in7: r20c039s2/out1;
 in8: r20c040s2/out1;*/

 out1_8: r13c3s14/in9_16;
}

JOINER_8(r21c6)              //cable F
{
/* in1: r20c041s2/out1;
 in2: r20c042s2/out1;
 in3: r20c043s2/out1;
 in4: r20c044s2/out1;
 in5: r20c045s2/out1;
 in6: r20c046s2/out1;
 in7: r20c047s2/out1;
 in8: r20c048s2/out1;*/

 out1_8: r13c3s13/in9_16;
}

JOINER_8(r21c7)             //cable G
{
/* in1: r20c049s2/out1;
 in2: r20c050s2/out1;
 in3: r20c051s2/out1;
 in4: r20c052s2/out1;
 in5: r20c053s2/out1;
 in6: r20c054s2/out1;
 in7: r20c055s2/out1;
 in8: r20c056s2/out1;*/

 out1_8: r13c3s12/in9_16;
}

JOINER_8(r21c8)             //cable H
{
/* in1: r20c057s2/out1;
 in2: r20c058s2/out1;
 in3: r20c059s2/out1;
 in4: r20c060s2/out1;
 in5: r20c061s2/out1;
 in6: r20c062s2/out1;
 in7: r20c063s2/out1;
 in8: r20c064s2/out1;*/

 out1_8: r13c3s17/in1_8;
}

JOINER_8(r21c9)             //cable I         
{
/* in1: r20c065s2/out1;
 in2: r20c066s2/out1;
 in3: r20c067s2/out1;
 in4: r20c068s2/out1;
 in5: r20c069s2/out1;
 in6: r20c070s2/out1;
 in7: r20c071s2/out1;
 in8: r20c072s2/out1;*/

 out1_8: r13c3s16/in1_8;
}

JOINER_8(r21c10)             //cable J       
{
 //in1: r20c073s2/out1;     //channel broken at joiner
 /*in2: r20c074s2/out1;
 in3: r20c075s2/out1;
 in4: r20c076s2/out1;
 in5: r20c009s2/out1;     	// signal cables of 009 and 073 swapped at PP.//signal from crystal 73 is moved here (77 was already free)
 in6: r20c078s2/out1;
 in7: r20c079s2/out1;
 in8: r20c080s2/out1;*/

 out1_8: r13c3s18/in9_16;
}
JOINER_8(r21c11)             //cable K   
{
/* in1: r20c161s2/out1;
 in2: r20c162s2/out1;
 in3: r20c083s2/out1;
 in4: r20c084s2/out1;
 in5: r20c085s2/out1;
 in6: r20c086s2/out1;
 in7: r20c087s2/out1;
 in8: r20c088s2/out1;*/

 out1_8: r13c3s15/in9_16;
}

JOINER_8(r21c12)             //cable L
{
/* in1: r20c089s2/out1;
 in2: r20c090s2/out1;
 in3: r20c091s2/out1;
 in4: r20c092s2/out1;
 in5: r20c093s2/out1;
 in6: r20c094s2/out1;
 in7: r20c095s2/out1;
 in8: r20c096s2/out1;*/

 out1_8: r13c3s19/in1_8;
}

JOINER_8(r21c13)             //cable M
{
/* in1: r20c097s2/out1;
 in2: r20c098s2/out1;
 in3: r20c099s2/out1;
 in4: r20c100s2/out1;		//crystals 100-103 re-installed
 in5: r20c101s2/out1;
 in6: r20c102s2/out1;
 in7: r20c103s2/out1;
 in8: r20c104s2/out1;*/

 out1_8: r13c3s21/in1_8;
}

JOINER_8(r21c14)              //cable N
{
 /*in1: r20c105s2/out1;
 in2: r20c106s2/out1;
 in3: r20c107s2/out1;
 in4: r20c108s2/out1;
 in5: r20c109s2/out1;
 in6: r20c110s2/out1;
 in7: r20c111s2/out1;
 in8: r20c112s2/out1;*/

 out1_8: r13c3s20/in9_16;
}

JOINER_8(r21c15)             //cable O
{ 
/* in1: r20c113s2/out1;
 in2: r20c114s2/out1;
 in3: r20c115s2/out1;
 in4: r20c116s2/out1;
 in5: r20c117s2/out1;
 in6: r20c118s2/out1;
 in7: r20c119s2/out1;
 in8: r20c120s2/out1;*/

 out1_8: r13c3s17/in9_16;
}

JOINER_8(r21c16)             //cable P
{
/* in1: r20c121s2/out1;
 in2: r20c122s2/out1;
 in3: r20c123s2/out1;
 in4: r20c124s2/out1;
 in5: r20c125s2/out1;
 in6: r20c126s2/out1;
 in7: r20c127s2/out1;
 in8: r20c128s2/out1;*/

 out1_8: r13c3s19/in9_16;
}

JOINER_8(r21c17)            //cable Q
{
/* in1: r20c129s2/out1;
 in2: r20c130s2/out1;
 in3: r20c131s2/out1;
 in4: r20c132s2/out1;
 in5: r20c133s2/out1;
 in6: r20c134s2/out1;
 in7: r20c135s2/out1;
 in8: r20c136s2/out1;*/

 out1_8: r13c3s18/in1_8;       
}

JOINER_8(r21c18)            //cable R
{
 /*in1: r20c137s2/out1;
 in2: r20c138s2/out1;
 in3: r20c139s2/out1;
 in4: r20c140s2/out1;
 in5: r20c141s2/out1;
 in6: r20c142s2/out1;
 in7: r20c143s2/out1;
 in8: r20c144s2/out1;*/

 out1_8: r13c3s16/in9_16;
}

JOINER_8(r21c19)             //cable S
{
 /*in1: r20c145s2/out1;
 in2: r20c146s2/out1;
 in3: r20c147s2/out1;
 in4: r20c148s2/out1;
 in5: r20c149s2/out1;
 in6: r20c150s2/out1;
 in7: r20c151s2/out1;
 in8: r20c152s2/out1;*/

 out1_8: r13c3s20/in1_8;
}

JOINER_8(r21c20)             //cable T
{
 /*in1: r20c153s2/out1;
 in2: r20c154s2/out1;
 in3: r20c155s2/out1;
 in4: r20c156s2/out1;
 in5: r20c157s2/out1;
 in6: r20c158s2/out1;
 in7: r20c159s2/out1;
 in8: r20c160s2/out1;*/

 out1_8: r13c3s21/in9_16;
}



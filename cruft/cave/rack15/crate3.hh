/*
 * Crate 3,r15
 * VME Crate with qdc and triva 
 * 
 */



VME_CRATE(r15c3)
{
  SETTING("CRATE_NO" => "1");
}

RIO3(r15c3s1)
{
SETTING("HOSTNAME"=>"R4-10");
}

TRIVA5(r15c3s2)
{
//tribu1: rMASTERc2s2/tribu2;
//tribu2: r52c1s2/tribu1;
;}



CAEN_V792(r15c3s4)
{
  SERIAL("QDCBEAM1"); //FW: QDC for beam detectors
  SETTING("ADDRESS" => "0x00020000");
  SETTING("VIRTUAL_SLOT" => "1");

  //in0_7: r47c1s5/e1_8; // all with 50 ns delay
  //in8_15: r47c1s5/e9_16; // no delay on this
  //gate_in: rMASTERc3s8u5/out2;

  //FW: those ones host POS and ROLU:
  in0_7: r14c2s22/out_ecl1_8;
  //FW: those ones host S8:
  in8_9: r14c2s22/out_ecl9_10;
      
  in13_15: r14c2s22/out_ecl13_15;
}


CAEN_V775(r15c3s5)
{
  SERIAL("TDCBEAM1"); //FW: TDC for beam detectors
  SETTING("ADDRESS" => "0x00030000");
   SETTING("VIRTUAL_SLOT" => "130"); //number in readout +128

  //in0_15: r47c2s18/out1_16; 
  //com_in: rMASTERc3s6u1/out8; 
  
  //FW: those ones host POS and ROLU:
  in0_7: r14c6s17/outa1_8;
  //FW: those ones host S8:
  in8: r14c6s17/outa9;  
  in12: r14c6s17/outa13;
}

//FW: this one is actually a V795, and it hosts the pixel detectors!
CAEN_V792(r15c3s7)
{
  SERIAL("QDCBEAM2");	//FW: 2nd QDC for beam detectors
  SETTING("ADDRESS" => "0x00070000");
  SETTING("VIRTUAL_SLOT" => "3");


  //in0:	r13c8/e1;	//Pix1
  //in1:	r13c8/e2;	//Pix2
  //in2:	r13c8/e3;	//Pix3

}

CAEN_V830(r15c3s10)
{

SETTING("ADDRESS" => "0x00610000");
   SERIAL("CAEN310"); //VME_SCALER_1
   SETTING("VIRTUAL_SLOT" => "0");

//in0_7: r47c4s19u2/out_ecl0_7;
//in8_15: r47c4s19u1/out_ecl0_7;
//trig_in: rMASTERc3s6u1/out4; 

  in0_7: r14c2s5u1/out_ecl0_7;
  in8_15: r14c2s5u2/out_ecl0_7;
}

/*
CAEN_V830(r15c3s12)
{

SETTING("ADDRESS" => "0x00600000");
   SERIAL("CAEN312"); //VME_SCALER_1
   SETTING("VIRTUAL_SLOT" => "30");

//in0_7: r47c4s19u2/out_ecl0_7;
//in8_15: r47c4s19u1/out_ecl0_7;
//trig_in: rMASTERc3s6u1/out4; 
;}

CAEN_V830(r15c3s13)
{

SETTING("ADDRESS" => "0x00800000");
   SERIAL("CAEN313"); //VME_SCALER_1
   SETTING("VIRTUAL_SLOT" => "30");

//in0_7: r47c4s19u2/out_ecl0_7;
//in8_15: r47c4s19u1/out_ecl0_7;
//trig_in: rMASTERc3s6u1/out4; 
;}
*/


/* 
 * CRATE 4
 * VME crate label 2
 * 
 *  VME CRATE with controller, qdc, triva etc 
 *
 */
 
 VME_CRATE(r15c4)
 {
 SETTING("CRATE_NO" => "2");
 }
 
 RIO4(r15c4s1)
 {
 SETTING("HOSTNAME" => "R4-35");
 }
 
 TRIVA7(r15c4s2)
 {
// tribu1: r47c3s2/tribu2;
 ;}
 
 CAEN_V792(r15c4s4) //TFW QDC
 {
// TFW signals 7_1 to 12_2
   SETTING("ADDRESS" => "0x00020000");
   SERIAL("QDCTFW1");
   SETTING("VIRTUAL_SLOT" => "1");

in12_23: r12c3/out1_12;

// in0_7: r52c0s8/e1_8;
// in8_15:r52c0s8/e9_16;
// in16_23: r52c0s5/e1_8;
// in24_31: r52c0s5/e9_16;
// gate_in: r15c4s15/trig_in;// (linker eingang in s15) //is this really ni and out? upper in, lower out
// gate_out: r15c4s5/gate_out;
 }
  
 CAEN_V792(r15c4s5)  //TFW QDC
 {
 //TFW signals 23_1 to 28_2
 
 SETTING("ADDRESS"=>"0x00030000");
   SERIAL("QDCTFW2");
   SETTING("VIRTUAL_SLOT" => "2");
in12: r12c2/out3; 
in13: r12c2/out6;
in14: r12c2/out9; 
in15: r12c2/out12;
in16: r12c2/out15;
in17: r12c1/out3; 
in18: r12c1/out6;
in19: r12c1/out9; 
in20: r12c1/out12;
in21: r12c1/out15;
in22: r13c1/out3;
in23: r13c1/out6;
// gate_in: r52c2s7u1/out2;  
// gate_out: r15c4s4/gate_out;
 }
 
 
 CAEN_V775(r15c4s6) //TFW TDC
 {
   SETTING("ADDRESS" => "0x00040000");
  SERIAL("TDCTFW1"); 
     SETTING("VIRTUAL_SLOT" => "131");  //number in readout + 128
  in0_15:  r12c6s10/outa1_16;
  in16_31: r12c6s12/outa1_16;  
// com_in: r52c2s4u1/out16;
// com_out: r15c4s7/com_in;
 }
 
 CAEN_V775(r15c4s7) //TFW TDC
 {
   SETTING("ADDRESS" => "0x00050000");
  SERIAL("TDCTFW2"); 
     SETTING("VIRTUAL_SLOT" => "132");
 in0_15:  r12c5s9/outa1_16;
 in16_31: r12c5s11/outa1_16;  
// com_in: r15c4s6/com_out;
 }
 
 CAEN_V775(r15c4s10) //NTF TDC
 {
   SETTING("ADDRESS" => "0x00060000");
  SERIAL("TDCNTF"); 
     SETTING("VIRTUAL_SLOT" => "133");
 in0_15: r14c6s18/outa1_16;
 in16_31:r14c6s19/outa1_16;
// com_out: r52c2s9/out;//oberer
 }
CAEN_V830(r15c4s11) //NTF Scaler
{
SETTING("ADDRESS" => "0x00600000");
  SERIAL("CAEN0098"); //VME_SCALER_1
   SETTING("VIRTUAL_SLOT" => "0");
in0_15: r14c6s18/outb1_16;
in16_31: r14c6s19/outb1_16;
//trig_in: r15c4s4/gate_in; //left
} 
 
 CAEN_V792(r15c4s12) //NTF QDC
 {
 SETTING("ADDRESS" => "0x00070000");
   SERIAL("QDCNTF");
      SETTING("VIRTUAL_SLOT" => "6");
 in0_7:   r13c2/out1_8;
 in8_15:  r13c3/out1_8;
 in16_23: r13c4/out1_8;
 in24_31: r13c5/out1_8;

// gate_in: r52c2s7u2/out2; //upper
 }
 CAEN_V792(r15c4s15) //MFI alternative readout
 {
 SETTING("ADDRESS" => "0x000e0000");
   SERIAL("QDCMFI");
      SETTING("VIRTUAL_SLOT" => "10");
// in0_7:   r13c2/out1_8;
// in8_15:  r13c3/out1_8;
// in16_23: r13c4/out1_8;
// in24_31: r13c5/out1_8;

// gate_in: r52c2s7u2/out2; //upper
 }


//  GFI 
CAEN_V792(r15c4s17)
{
SETTING("ADDRESS" => "0x000b0000");
   SERIAL("QDC792GFI1");
      SETTING("VIRTUAL_SLOT" => "7");

in0: r53c1s17/out1;
in1: r53c1s19/out1;
in2: r53c1s17/out2;
in3: r53c1s19/out2;
in4: r53c1s17/out3;
//in5: r53c1s19/out3;
in6: r53c1s17/out4;
in7: r53c1s19/out4;

in8:  r53c1s17/out5;
in9:  r53c1s19/out5;
in10: r53c1s17/out6;
in11: r53c1s19/out6;
in12: r53c1s17/out7;
in13: r53c1s19/out7;
in14: r53c1s17/out8;
in15: r53c1s19/out8;

//in16: r53c1s17/out9;
in17: r53c1s19/out9;
in18: r53c1s17/out10;
in19: r53c1s19/out10;
in20: r53c1s17/out11;
in21: r53c1s19/out11;
in22: r53c1s17/out12;
in23: r53c1s19/out12;

in24: r53c1s17/out13;
in25: r53c1s19/out13;
in26: r53c1s17/out14;
in27: r53c1s19/out14;
in28: r53c1s17/out15;
in29: r53c1s19/out15;
in30: r53c1s17/out16;
in31: r53c1s19/out16;}


CAEN_V792(r15c4s18)
{
SETTING("ADDRESS"=>"0x000c0000");
   SERIAL("QDC792GFI2");
      SETTING("VIRTUAL_SLOT" => "8");
in0: r53c1s7/out1;
in1: ;
in2: r53c1s7/out2;
in3: r53c1s9/out2;
in4: r53c1s7/out3;
//in5: r53c1s9/out3;
in6: r53c1s7/out4;
in7: r53c1s9/out4;

in8:  r53c1s7/out5;
in9:  r53c1s9/out5;
in10: r53c1s7/out6;
in11: r53c1s9/out6;
in12: r53c1s7/out7;
in13: r53c1s9/out7;
in14: r53c1s7/out8;
in15: r53c1s9/out8;

in16: r53c1s7/out9;
in17: r53c1s9/out9;
in18: r53c1s7/out10;
in19: r53c1s9/out10;
in20: r53c1s7/out11;
in21: r53c1s9/out11;
in22: r53c1s7/out12;
in23: r53c1s9/out12;

in24: r53c1s7/out13;
in25: r53c1s9/out13;
in26: r53c1s7/out14;
in27: r53c1s9/out14;
in28: r53c1s7/out15;
in29: r53c1s9/out15;
in30: r53c1s7/out16;
in31: r53c1s9/out16;
}

CAEN_V792(r15c4s19)
{
SETTING("ADDRESS"=>"0x000d0000");
   SERIAL("QDC792GFI3");
      SETTING("VIRTUAL_SLOT" => "9");

in0: r53c1s13/out1;  
in1: r53c1s13/out2;
in2: r53c1s13/out15;  
in3: r53c1s13/out16;
in4: r53c1s17/out9;
in5: r53c1s9/out1;
in6: r53c1s9/out3;

in15: r53c1s19/out3;


}

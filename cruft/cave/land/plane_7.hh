///////////////////// FILE AUTO-GENERATED WITH gen_planes.pl
DETECTOR_LAND(rNP7c01s1) { LABEL("N07_01_1"); signal: rNP7c21/in8;}
DETECTOR_LAND(rNP7c01s2) { LABEL("N07_01_2"); signal: rNP7c21/in7;}
DETECTOR_LAND(rNP7c02s1) { LABEL("N07_02_1"); signal: rNP7c21/in6;}
DETECTOR_LAND(rNP7c02s2) { LABEL("N07_02_2"); signal: rNP7c21/in5;}
DETECTOR_LAND(rNP7c03s1) { LABEL("N07_03_1"); signal: rNP7c21/in4;}
DETECTOR_LAND(rNP7c03s2) { LABEL("N07_03_2"); signal: rNP7c21/in3;}
DETECTOR_LAND(rNP7c04s1) { LABEL("N07_04_1"); signal: rNP7c21/in2;}
DETECTOR_LAND(rNP7c04s2) { LABEL("N07_04_2"); signal: rNP7c21/in1;}
DETECTOR_LAND(rNP7c05s1) { LABEL("N07_05_1"); signal: rNP7c22/in8;}
DETECTOR_LAND(rNP7c05s2) { LABEL("N07_05_2"); signal: rNP7c22/in7;}
DETECTOR_LAND(rNP7c06s1) { LABEL("N07_06_1"); signal: rNP7c22/in6;}
DETECTOR_LAND(rNP7c06s2) { LABEL("N07_06_2"); signal: rNP7c22/in5;}
DETECTOR_LAND(rNP7c07s1) { LABEL("N07_07_1"); signal: rNP7c22/in4;}
DETECTOR_LAND(rNP7c07s2) { LABEL("N07_07_2"); signal: rNP7c22/in3;}
DETECTOR_LAND(rNP7c08s1) { LABEL("N07_08_1"); signal: rNP7c22/in2;}
DETECTOR_LAND(rNP7c08s2) { LABEL("N07_08_2"); signal: rNP7c22/in1;}
DETECTOR_LAND(rNP7c09s1) { LABEL("N07_09_1"); signal: rNP7c23/in8;}
DETECTOR_LAND(rNP7c09s2) { LABEL("N07_09_2"); signal: rNP7c23/in7;}
DETECTOR_LAND(rNP7c10s1) { LABEL("N07_10_1"); signal: rNP7c23/in6;}
DETECTOR_LAND(rNP7c10s2) { LABEL("N07_10_2"); signal: rNP7c23/in5;}
DETECTOR_LAND(rNP7c11s1) { LABEL("N07_11_1"); signal: rNP7c23/in4;}
DETECTOR_LAND(rNP7c11s2) { LABEL("N07_11_2"); signal: rNP7c23/in3;}
DETECTOR_LAND(rNP7c12s1) { LABEL("N07_12_1"); signal: rNP7c23/in2;}
DETECTOR_LAND(rNP7c12s2) { LABEL("N07_12_2"); signal: rNP7c23/in1;}
DETECTOR_LAND(rNP7c13s1) { LABEL("N07_13_1"); signal: rNP7c24/in8;}
DETECTOR_LAND(rNP7c13s2) { LABEL("N07_13_2"); signal: rNP7c24/in7;}
DETECTOR_LAND(rNP7c14s1) { LABEL("N07_14_1"); signal: rNP7c24/in6;}
DETECTOR_LAND(rNP7c14s2) { LABEL("N07_14_2"); signal: rNP7c24/in5;}
DETECTOR_LAND(rNP7c15s1) { LABEL("N07_15_1"); signal: rNP7c24/in4;}
DETECTOR_LAND(rNP7c15s2) { LABEL("N07_15_2"); signal: rNP7c24/in3;}
DETECTOR_LAND(rNP7c16s1) { LABEL("N07_16_1"); signal: rNP7c24/in2;}
DETECTOR_LAND(rNP7c16s2) { LABEL("N07_16_2"); signal: rNP7c24/in1;}
DETECTOR_LAND(rNP7c17s1) { LABEL("N07_17_1"); signal: rNP7c25/in8;}
DETECTOR_LAND(rNP7c17s2) { LABEL("N07_17_2"); signal: rNP7c25/in7;}
DETECTOR_LAND(rNP7c18s1) { LABEL("N07_18_1"); signal: rNP7c25/in6;}
DETECTOR_LAND(rNP7c18s2) { LABEL("N07_18_2"); signal: rNP7c25/in5;}
DETECTOR_LAND(rNP7c19s1) { LABEL("N07_19_1"); signal: rNP7c25/in4;}
DETECTOR_LAND(rNP7c19s2) { LABEL("N07_19_2"); signal: rNP7c25/in3;}
DETECTOR_LAND(rNP7c20s1) { LABEL("N07_20_1"); signal: rNP7c25/in2;}
DETECTOR_LAND(rNP7c20s2) { LABEL("N07_20_2"); signal: rNP7c25/in1;}


////// JOINER8 PART --- auto-generated 

JOINER_8(rNP7c21)
{
in1: rNP7c04s2/signal;
in2: rNP7c04s1/signal;
in3: rNP7c03s2/signal;
in4: rNP7c03s1/signal;
in5: rNP7c02s2/signal;
in6: rNP7c02s1/signal;
in7: rNP7c01s2/signal;
in8: rNP7c01s1/signal;

out1: rRLANDTACQc2s6u1/in8;
out2: rRLANDTACQc2s6u1/in7;
out3: rRLANDTACQc2s6u1/in6;
out4: rRLANDTACQc2s6u1/in5;
out5: rRLANDTACQc2s6u1/in4;
out6: rRLANDTACQc2s6u1/in3;
out7: rRLANDTACQc2s6u1/in2;
out8: rRLANDTACQc2s6u1/in1;
}

JOINER_8(rNP7c22)
{
in1: rNP7c08s2/signal;
in2: rNP7c08s1/signal;
in3: rNP7c07s2/signal;
in4: rNP7c07s1/signal;
in5: rNP7c06s2/signal;
in6: rNP7c06s1/signal;
in7: rNP7c05s2/signal;
in8: rNP7c05s1/signal;

out1: rRLANDTACQc2s6u1/in16;
out2: rRLANDTACQc2s6u1/in15;
out3: rRLANDTACQc2s6u1/in14;
out4: rRLANDTACQc2s6u1/in13;
out5: rRLANDTACQc2s6u1/in12;
out6: rRLANDTACQc2s6u1/in11;
out7: rRLANDTACQc2s6u1/in10;
out8: rRLANDTACQc2s6u1/in9;
}

JOINER_8(rNP7c23)
{
in1: rNP7c12s2/signal;
in2: rNP7c12s1/signal;
in3: rNP7c11s2/signal;
in4: rNP7c11s1/signal;
in5: rNP7c10s2/signal;
in6: rNP7c10s1/signal;
in7: rNP7c09s2/signal;
in8: rNP7c09s1/signal;

out1: rRLANDTACQc2s7u1/in8;
out2: rRLANDTACQc2s7u1/in7;
out3: rRLANDTACQc2s7u1/in6;
out4: rRLANDTACQc2s7u1/in5;
out5: rRLANDTACQc2s7u1/in4;
out6: rRLANDTACQc2s7u1/in3;
out7: rRLANDTACQc2s7u1/in2;
out8: rRLANDTACQc2s7u1/in1;
}

JOINER_8(rNP7c24)
{
in1: rNP7c16s2/signal;
in2: rNP7c16s1/signal;
in3: rNP7c15s2/signal;
in4: rNP7c15s1/signal;
in5: rNP7c14s2/signal;
in6: rNP7c14s1/signal;
in7: rNP7c13s2/signal;
in8: rNP7c13s1/signal;

out1: rRLANDTACQc2s7u1/in16;
out2: rRLANDTACQc2s7u1/in15;
out3: rRLANDTACQc2s7u1/in14;
out4: rRLANDTACQc2s7u1/in13;
out5: rRLANDTACQc2s7u1/in12;
out6: rRLANDTACQc2s7u1/in11;
out7: rRLANDTACQc2s7u1/in10;
out8: rRLANDTACQc2s7u1/in9;
}

JOINER_8(rNP7c25)
{
in1: rNP7c20s2/signal;
in2: rNP7c20s1/signal;
in3: rNP7c19s2/signal;
in4: rNP7c19s1/signal;
in5: rNP7c18s2/signal;
in6: rNP7c18s1/signal;
in7: rNP7c17s2/signal;
in8: rNP7c17s1/signal;

out1: rRLANDTACQc2s8u1/in8;
out2: rRLANDTACQc2s8u1/in7;
out3: rRLANDTACQc2s8u1/in6;
out4: rRLANDTACQc2s8u1/in5;
out5: rRLANDTACQc2s8u1/in4;
out6: rRLANDTACQc2s8u1/in3;
out7: rRLANDTACQc2s8u1/in2;
out8: rRLANDTACQc2s8u1/in1;
}


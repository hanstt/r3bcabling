///////////////////// FILE AUTO-GENERATED WITH gen_planes.pl
DETECTOR_LAND(rNP4c01s1) { LABEL("N04_01_1"); signal: rNP4c21/in8;}
DETECTOR_LAND(rNP4c01s2) { LABEL("N04_01_2"); signal: rNP4c21/in7;}
DETECTOR_LAND(rNP4c02s1) { LABEL("N04_02_1"); signal: rNP4c21/in6;}
DETECTOR_LAND(rNP4c02s2) { LABEL("N04_02_2"); signal: rNP4c21/in5;}
DETECTOR_LAND(rNP4c03s1) { LABEL("N04_03_1"); signal: rNP4c21/in4;}
DETECTOR_LAND(rNP4c03s2) { LABEL("N04_03_2"); signal: rNP4c21/in3;}
DETECTOR_LAND(rNP4c04s1) { LABEL("N04_04_1"); signal: rNP4c21/in2;}
DETECTOR_LAND(rNP4c04s2) { LABEL("N04_04_2"); signal: rNP4c21/in1;}
DETECTOR_LAND(rNP4c05s1) { LABEL("N04_05_1"); signal: rNP4c22/in8;}
DETECTOR_LAND(rNP4c05s2) { LABEL("N04_05_2"); signal: rNP4c22/in7;}
DETECTOR_LAND(rNP4c06s1) { LABEL("N04_06_1"); signal: rNP4c22/in6;}
DETECTOR_LAND(rNP4c06s2) { LABEL("N04_06_2"); signal: rNP4c22/in5;}
DETECTOR_LAND(rNP4c07s1) { LABEL("N04_07_1"); signal: rNP4c22/in4;}
DETECTOR_LAND(rNP4c07s2) { LABEL("N04_07_2"); signal: rNP4c22/in3;}
DETECTOR_LAND(rNP4c08s1) { LABEL("N04_08_1"); signal: rNP4c22/in2;}
DETECTOR_LAND(rNP4c08s2) { LABEL("N04_08_2"); signal: rNP4c22/in1;}
DETECTOR_LAND(rNP4c09s1) { LABEL("N04_09_1"); signal: rNP4c23/in8;}
DETECTOR_LAND(rNP4c09s2) { LABEL("N04_09_2"); signal: rNP4c23/in7;}
DETECTOR_LAND(rNP4c10s1) { LABEL("N04_10_1"); signal: rNP4c23/in6;}
DETECTOR_LAND(rNP4c10s2) { LABEL("N04_10_2"); signal: rNP4c23/in5;}
DETECTOR_LAND(rNP4c11s1) { LABEL("N04_11_1"); signal: rNP4c23/in4;}
DETECTOR_LAND(rNP4c11s2) { LABEL("N04_11_2"); signal: rNP4c23/in3;}
DETECTOR_LAND(rNP4c12s1) { LABEL("N04_12_1"); signal: rNP4c23/in2;}
DETECTOR_LAND(rNP4c12s2) { LABEL("N04_12_2"); signal: rNP4c23/in1;}
DETECTOR_LAND(rNP4c13s1) { LABEL("N04_13_1"); signal: rNP4c24/in8;}
DETECTOR_LAND(rNP4c13s2) { LABEL("N04_13_2"); signal: rNP4c24/in7;}
DETECTOR_LAND(rNP4c14s1) { LABEL("N04_14_1"); signal: rNP4c24/in6;}
DETECTOR_LAND(rNP4c14s2) { LABEL("N04_14_2"); signal: rNP4c24/in5;}
DETECTOR_LAND(rNP4c15s1) { LABEL("N04_15_1"); signal: rNP4c24/in4;}
DETECTOR_LAND(rNP4c15s2) { LABEL("N04_15_2"); signal: rNP4c24/in3;}
DETECTOR_LAND(rNP4c16s1) { LABEL("N04_16_1"); signal: rNP4c24/in2;}
DETECTOR_LAND(rNP4c16s2) { LABEL("N04_16_2"); signal: rNP4c24/in1;}
DETECTOR_LAND(rNP4c17s1) { LABEL("N04_17_1"); signal: rNP4c25/in8;}
DETECTOR_LAND(rNP4c17s2) { LABEL("N04_17_2"); signal: rNP4c25/in7;}
DETECTOR_LAND(rNP4c18s1) { LABEL("N04_18_1"); signal: rNP4c25/in6;}
DETECTOR_LAND(rNP4c18s2) { LABEL("N04_18_2"); signal: rNP4c25/in5;}
DETECTOR_LAND(rNP4c19s1) { LABEL("N04_19_1"); signal: rNP4c25/in4;}
DETECTOR_LAND(rNP4c19s2) { LABEL("N04_19_2"); signal: rNP4c25/in3;}
DETECTOR_LAND(rNP4c20s1) { LABEL("N04_20_1"); signal: rNP4c25/in2;}
DETECTOR_LAND(rNP4c20s2) { LABEL("N04_20_2"); signal: rNP4c25/in1;}


////// JOINER8 PART --- auto-generated 

JOINER_8(rNP4c21)
{
in1: rNP4c04s2/signal;
in2: rNP4c04s1/signal;
in3: rNP4c03s2/signal;
in4: rNP4c03s1/signal;
in5: rNP4c02s2/signal;
in6: rNP4c02s1/signal;
in7: rNP4c01s2/signal;
in8: rNP4c01s1/signal;

out1: rRLANDTACQc1s8u1/in16;
out2: rRLANDTACQc1s8u1/in15;
out3: rRLANDTACQc1s8u1/in14;
out4: rRLANDTACQc1s8u1/in13;
out5: rRLANDTACQc1s8u1/in12;
out6: rRLANDTACQc1s8u1/in11;
out7: rRLANDTACQc1s8u1/in10;
out8: rRLANDTACQc1s8u1/in9;
}

JOINER_8(rNP4c22)
{
in1: rNP4c08s2/signal;
in2: rNP4c08s1/signal;
in3: rNP4c07s2/signal;
in4: rNP4c07s1/signal;
in5: rNP4c06s2/signal;
in6: rNP4c06s1/signal;
in7: rNP4c05s2/signal;
in8: rNP4c05s1/signal;

out1: rRLANDTACQc1s9u1/in8;
out2: rRLANDTACQc1s9u1/in7;
out3: rRLANDTACQc1s9u1/in6;
out4: rRLANDTACQc1s9u1/in5;
out5: rRLANDTACQc1s9u1/in4;
out6: rRLANDTACQc1s9u1/in3;
out7: rRLANDTACQc1s9u1/in2;
out8: rRLANDTACQc1s9u1/in1;
}

JOINER_8(rNP4c23)
{
in1: rNP4c12s2/signal;
in2: rNP4c12s1/signal;
in3: rNP4c11s2/signal;
in4: rNP4c11s1/signal;
in5: rNP4c10s2/signal;
in6: rNP4c10s1/signal;
in7: rNP4c09s2/signal;
in8: rNP4c09s1/signal;

out1: rRLANDTACQc1s9u1/in16;
out2: rRLANDTACQc1s9u1/in15;
out3: rRLANDTACQc1s9u1/in14;
out4: rRLANDTACQc1s9u1/in13;
out5: rRLANDTACQc1s9u1/in12;
out6: rRLANDTACQc1s9u1/in11;
out7: rRLANDTACQc1s9u1/in10;
out8: rRLANDTACQc1s9u1/in9;
}

JOINER_8(rNP4c24)
{
in1: rNP4c16s2/signal;
in2: rNP4c16s1/signal;
in3: rNP4c15s2/signal;
in4: rNP4c15s1/signal;
in5: rNP4c14s2/signal;
in6: rNP4c14s1/signal;
in7: rNP4c13s2/signal;
in8: rNP4c13s1/signal;

out1: rRLANDTACQc1s10u1/in8;
out2: rRLANDTACQc1s10u1/in7;
out3: rRLANDTACQc1s10u1/in6;
out4: rRLANDTACQc1s10u1/in5;
out5: rRLANDTACQc1s10u1/in4;
out6: rRLANDTACQc1s10u1/in3;
out7: rRLANDTACQc1s10u1/in2;
out8: rRLANDTACQc1s10u1/in1;
}

JOINER_8(rNP4c25)
{
in1: rNP4c20s2/signal;
in2: rNP4c20s1/signal;
in3: rNP4c19s2/signal;
in4: rNP4c19s1/signal;
in5: rNP4c18s2/signal;
in6: rNP4c18s1/signal;
in7: rNP4c17s2/signal;
in8: rNP4c17s1/signal;

out1: rRLANDTACQc1s10u1/in16;
out2: rRLANDTACQc1s10u1/in15;
out3: rRLANDTACQc1s10u1/in14;
out4: rRLANDTACQc1s10u1/in13;
out5: rRLANDTACQc1s10u1/in12;
out6: rRLANDTACQc1s10u1/in11;
out7: rRLANDTACQc1s10u1/in10;
out8: rRLANDTACQc1s10u1/in9;
}


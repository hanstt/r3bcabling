///////////////////// FILE AUTO-GENERATED WITH gen_planes.pl
DETECTOR_LAND(rNP9c01s1) { LABEL("N09_01_1"); signal: rNP9c21/in8;}
DETECTOR_LAND(rNP9c01s2) { LABEL("N09_01_2"); signal: rNP9c21/in7;}
DETECTOR_LAND(rNP9c02s1) { LABEL("N09_02_1"); signal: rNP9c21/in6;}
DETECTOR_LAND(rNP9c02s2) { LABEL("N09_02_2"); signal: rNP9c21/in5;}
DETECTOR_LAND(rNP9c03s1) { LABEL("N09_03_1"); signal: rNP9c21/in4;}
DETECTOR_LAND(rNP9c03s2) { LABEL("N09_03_2"); signal: rNP9c21/in3;}
DETECTOR_LAND(rNP9c04s1) { LABEL("N09_04_1"); signal: rNP9c21/in2;}
DETECTOR_LAND(rNP9c04s2) { LABEL("N09_04_2"); signal: rNP9c21/in1;}
DETECTOR_LAND(rNP9c05s1) { LABEL("N09_05_1"); signal: rNP9c22/in8;}
DETECTOR_LAND(rNP9c05s2) { LABEL("N09_05_2"); signal: rNP9c22/in7;}
DETECTOR_LAND(rNP9c06s1) { LABEL("N09_06_1"); signal: rNP9c22/in6;}
DETECTOR_LAND(rNP9c06s2) { LABEL("N09_06_2"); signal: rNP9c22/in5;}
DETECTOR_LAND(rNP9c07s1) { LABEL("N09_07_1"); signal: rNP9c22/in4;}
DETECTOR_LAND(rNP9c07s2) { LABEL("N09_07_2"); signal: rNP9c22/in3;}
DETECTOR_LAND(rNP9c08s1) { LABEL("N09_08_1"); signal: rNP9c22/in2;}
DETECTOR_LAND(rNP9c08s2) { LABEL("N09_08_2"); signal: rNP9c22/in1;}
DETECTOR_LAND(rNP9c09s1) { LABEL("N09_09_1"); signal: rNP9c23/in8;}
DETECTOR_LAND(rNP9c09s2) { LABEL("N09_09_2"); signal: rNP9c23/in7;}
DETECTOR_LAND(rNP9c10s1) { LABEL("N09_10_1"); signal: rNP9c23/in6;}
DETECTOR_LAND(rNP9c10s2) { LABEL("N09_10_2"); signal: rNP9c23/in5;}
DETECTOR_LAND(rNP9c11s1) { LABEL("N09_11_1"); signal: rNP9c23/in4;}
DETECTOR_LAND(rNP9c11s2) { LABEL("N09_11_2"); signal: rNP9c23/in3;}
DETECTOR_LAND(rNP9c12s1) { LABEL("N09_12_1"); signal: rNP9c23/in2;}
DETECTOR_LAND(rNP9c12s2) { LABEL("N09_12_2"); signal: rNP9c23/in1;}
DETECTOR_LAND(rNP9c13s1) { LABEL("N09_13_1"); signal: rNP9c24/in8;}
DETECTOR_LAND(rNP9c13s2) { LABEL("N09_13_2"); signal: rNP9c24/in7;}
DETECTOR_LAND(rNP9c14s1) { LABEL("N09_14_1"); signal: rNP9c24/in6;}
DETECTOR_LAND(rNP9c14s2) { LABEL("N09_14_2"); signal: rNP9c24/in5;}
DETECTOR_LAND(rNP9c15s1) { LABEL("N09_15_1"); signal: rNP9c24/in4;}
DETECTOR_LAND(rNP9c15s2) { LABEL("N09_15_2"); signal: rNP9c24/in3;}
DETECTOR_LAND(rNP9c16s1) { LABEL("N09_16_1"); signal: rNP9c24/in2;}
DETECTOR_LAND(rNP9c16s2) { LABEL("N09_16_2"); signal: rNP9c24/in1;}
DETECTOR_LAND(rNP9c17s1) { LABEL("N09_17_1"); signal: rNP9c25/in8;}
DETECTOR_LAND(rNP9c17s2) { LABEL("N09_17_2"); signal: rNP9c25/in7;}
DETECTOR_LAND(rNP9c18s1) { LABEL("N09_18_1"); signal: rNP9c25/in6;}
DETECTOR_LAND(rNP9c18s2) { LABEL("N09_18_2"); signal: rNP9c25/in5;}
DETECTOR_LAND(rNP9c19s1) { LABEL("N09_19_1"); signal: rNP9c25/in4;}
DETECTOR_LAND(rNP9c19s2) { LABEL("N09_19_2"); signal: rNP9c25/in3;}
DETECTOR_LAND(rNP9c20s1) { LABEL("N09_20_1"); signal: rNP9c25/in2;}
DETECTOR_LAND(rNP9c20s2) { LABEL("N09_20_2"); signal: rNP9c25/in1;}


////// JOINER8 PART --- auto-generated 

JOINER_8(rNP9c21)
{
in1: rNP9c04s2/signal;
in2: rNP9c04s1/signal;
in3: rNP9c03s2/signal;
in4: rNP9c03s1/signal;
in5: rNP9c02s2/signal;
in6: rNP9c02s1/signal;
in7: rNP9c01s2/signal;
in8: rNP9c01s1/signal;

out1: rRLANDTACQc3s1u1/in8;
out2: rRLANDTACQc3s1u1/in7;
out3: rRLANDTACQc3s1u1/in6;
out4: rRLANDTACQc3s1u1/in5;
out5: rRLANDTACQc3s1u1/in4;
out6: rRLANDTACQc3s1u1/in3;
out7: rRLANDTACQc3s1u1/in2;
out8: rRLANDTACQc3s1u1/in1;
}

JOINER_8(rNP9c22)
{
in1: rNP9c08s2/signal;
in2: rNP9c08s1/signal;
in3: rNP9c07s2/signal;
in4: rNP9c07s1/signal;
in5: rNP9c06s2/signal;
in6: rNP9c06s1/signal;
in7: rNP9c05s2/signal;
in8: rNP9c05s1/signal;

out1: rRLANDTACQc3s1u1/in16;
out2: rRLANDTACQc3s1u1/in15;
out3: rRLANDTACQc3s1u1/in14;
out4: rRLANDTACQc3s1u1/in13;
out5: rRLANDTACQc3s1u1/in12;
out6: rRLANDTACQc3s1u1/in11;
out7: rRLANDTACQc3s1u1/in10;
out8: rRLANDTACQc3s1u1/in9;
}

JOINER_8(rNP9c23)
{
in1: rNP9c12s2/signal;
in2: rNP9c12s1/signal;
in3: rNP9c11s2/signal;
in4: rNP9c11s1/signal;
in5: rNP9c10s2/signal;
in6: rNP9c10s1/signal;
in7: rNP9c09s2/signal;
in8: rNP9c09s1/signal;

out1: rRLANDTACQc3s2u1/in8;
out2: rRLANDTACQc3s2u1/in7;
out3: rRLANDTACQc3s2u1/in6;
out4: rRLANDTACQc3s2u1/in5;
out5: rRLANDTACQc3s2u1/in4;
out6: rRLANDTACQc3s2u1/in3;
out7: rRLANDTACQc3s2u1/in2;
out8: rRLANDTACQc3s2u1/in1;
}

JOINER_8(rNP9c24)
{
in1: rNP9c16s2/signal;
in2: rNP9c16s1/signal;
in3: rNP9c15s2/signal;
in4: rNP9c15s1/signal;
in5: rNP9c14s2/signal;
in6: rNP9c14s1/signal;
in7: rNP9c13s2/signal;
in8: rNP9c13s1/signal;

out1: rRLANDTACQc3s2u1/in16;
out2: rRLANDTACQc3s2u1/in15;
out3: rRLANDTACQc3s2u1/in14;
out4: rRLANDTACQc3s2u1/in13;
out5: rRLANDTACQc3s2u1/in12;
out6: rRLANDTACQc3s2u1/in11;
out7: rRLANDTACQc3s2u1/in10;
out8: rRLANDTACQc3s2u1/in9;
}

JOINER_8(rNP9c25)
{
in1: rNP9c20s2/signal;
in2: rNP9c20s1/signal;
in3: rNP9c19s2/signal;
in4: rNP9c19s1/signal;
in5: rNP9c18s2/signal;
in6: rNP9c18s1/signal;
in7: rNP9c17s2/signal;
in8: rNP9c17s1/signal;

out1: rRLANDTACQc3s3u1/in8;
out2: rRLANDTACQc3s3u1/in7;
out3: rRLANDTACQc3s3u1/in6;
out4: rRLANDTACQc3s3u1/in5;
out5: rRLANDTACQc3s3u1/in4;
out6: rRLANDTACQc3s3u1/in3;
out7: rRLANDTACQc3s3u1/in2;
out8: rRLANDTACQc3s3u1/in1;
}


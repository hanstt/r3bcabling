///////////////////// FILE AUTO-GENERATED WITH gen_planes.pl
DETECTOR_LAND(rNP3c01s1) { LABEL("N03_01_1"); signal: rNP3c21/in8;}
DETECTOR_LAND(rNP3c01s2) { LABEL("N03_01_2"); signal: rNP3c21/in7;}
DETECTOR_LAND(rNP3c02s1) { LABEL("N03_02_1"); signal: rNP3c21/in6;}
DETECTOR_LAND(rNP3c02s2) { LABEL("N03_02_2"); signal: rNP3c21/in5;}
DETECTOR_LAND(rNP3c03s1) { LABEL("N03_03_1"); signal: rNP3c21/in4;}
DETECTOR_LAND(rNP3c03s2) { LABEL("N03_03_2"); signal: rNP3c21/in3;}
DETECTOR_LAND(rNP3c04s1) { LABEL("N03_04_1"); signal: rNP3c21/in2;}
DETECTOR_LAND(rNP3c04s2) { LABEL("N03_04_2"); signal: rNP3c21/in1;}
DETECTOR_LAND(rNP3c05s1) { LABEL("N03_05_1"); signal: rNP3c22/in8;}
DETECTOR_LAND(rNP3c05s2) { LABEL("N03_05_2"); signal: rNP3c22/in7;}
DETECTOR_LAND(rNP3c06s1) { LABEL("N03_06_1"); signal: rNP3c22/in6;}
DETECTOR_LAND(rNP3c06s2) { LABEL("N03_06_2"); signal: rNP3c22/in5;}
DETECTOR_LAND(rNP3c07s1) { LABEL("N03_07_1"); signal: rNP3c22/in4;}
DETECTOR_LAND(rNP3c07s2) { LABEL("N03_07_2"); signal: rNP3c22/in3;}
DETECTOR_LAND(rNP3c08s1) { LABEL("N03_08_1"); signal: rNP3c22/in2;}
DETECTOR_LAND(rNP3c08s2) { LABEL("N03_08_2"); signal: rNP3c22/in1;}
DETECTOR_LAND(rNP3c09s1) { LABEL("N03_09_1"); signal: rNP3c23/in8;}
DETECTOR_LAND(rNP3c09s2) { LABEL("N03_09_2"); signal: rNP3c23/in7;}
DETECTOR_LAND(rNP3c10s1) { LABEL("N03_10_1"); signal: rNP3c23/in6;}
DETECTOR_LAND(rNP3c10s2) { LABEL("N03_10_2"); signal: rNP3c23/in5;}
DETECTOR_LAND(rNP3c11s1) { LABEL("N03_11_1"); signal: rNP3c23/in4;}
DETECTOR_LAND(rNP3c11s2) { LABEL("N03_11_2"); signal: rNP3c23/in3;}
DETECTOR_LAND(rNP3c12s1) { LABEL("N03_12_1"); signal: rNP3c23/in2;}
DETECTOR_LAND(rNP3c12s2) { LABEL("N03_12_2"); signal: rNP3c23/in1;}
DETECTOR_LAND(rNP3c13s1) { LABEL("N03_13_1"); signal: rNP3c24/in8;}
DETECTOR_LAND(rNP3c13s2) { LABEL("N03_13_2"); signal: rNP3c24/in7;}
DETECTOR_LAND(rNP3c14s1) { LABEL("N03_14_1"); signal: rNP3c24/in6;}
DETECTOR_LAND(rNP3c14s2) { LABEL("N03_14_2"); signal: rNP3c24/in5;}
DETECTOR_LAND(rNP3c15s1) { LABEL("N03_15_1"); signal: rNP3c24/in4;}
DETECTOR_LAND(rNP3c15s2) { LABEL("N03_15_2"); signal: rNP3c24/in3;}
DETECTOR_LAND(rNP3c16s1) { LABEL("N03_16_1"); signal: rNP3c24/in2;}
DETECTOR_LAND(rNP3c16s2) { LABEL("N03_16_2"); signal: rNP3c24/in1;}
DETECTOR_LAND(rNP3c17s1) { LABEL("N03_17_1"); signal: rNP3c25/in8;}
DETECTOR_LAND(rNP3c17s2) { LABEL("N03_17_2"); signal: rNP3c25/in7;}
DETECTOR_LAND(rNP3c18s1) { LABEL("N03_18_1"); signal: rNP3c25/in6;}
DETECTOR_LAND(rNP3c18s2) { LABEL("N03_18_2"); signal: rNP3c25/in5;}
DETECTOR_LAND(rNP3c19s1) { LABEL("N03_19_1"); signal: rNP3c25/in4;}
DETECTOR_LAND(rNP3c19s2) { LABEL("N03_19_2"); signal: rNP3c25/in3;}
DETECTOR_LAND(rNP3c20s1) { LABEL("N03_20_1"); signal: rNP3c25/in2;}
DETECTOR_LAND(rNP3c20s2) { LABEL("N03_20_2"); signal: rNP3c25/in1;}


////// JOINER8 PART --- auto-generated 

JOINER_8(rNP3c21)
{
in1: rNP3c04s2/signal;
in2: rNP3c04s1/signal;
in3: rNP3c03s2/signal;
in4: rNP3c03s1/signal;
in5: rNP3c02s2/signal;
in6: rNP3c02s1/signal;
in7: rNP3c01s2/signal;
in8: rNP3c01s1/signal;

out1: rRLANDTACQc1s6u1/in8;
out2: rRLANDTACQc1s6u1/in7;
out3: rRLANDTACQc1s6u1/in6;
out4: rRLANDTACQc1s6u1/in5;
out5: rRLANDTACQc1s6u1/in4;
out6: rRLANDTACQc1s6u1/in3;
out7: rRLANDTACQc1s6u1/in2;
out8: rRLANDTACQc1s6u1/in1;
}

JOINER_8(rNP3c22)
{
in1: rNP3c08s2/signal;
in2: rNP3c08s1/signal;
in3: rNP3c07s2/signal;
in4: rNP3c07s1/signal;
in5: rNP3c06s2/signal;
in6: rNP3c06s1/signal;
in7: rNP3c05s2/signal;
in8: rNP3c05s1/signal;

out1: rRLANDTACQc1s6u1/in16;
out2: rRLANDTACQc1s6u1/in15;
out3: rRLANDTACQc1s6u1/in14;
out4: rRLANDTACQc1s6u1/in13;
out5: rRLANDTACQc1s6u1/in12;
out6: rRLANDTACQc1s6u1/in11;
out7: rRLANDTACQc1s6u1/in10;
out8: rRLANDTACQc1s6u1/in9;
}

JOINER_8(rNP3c23)
{
in1: rNP3c12s2/signal;
in2: rNP3c12s1/signal;
in3: rNP3c11s2/signal;
in4: rNP3c11s1/signal;
in5: rNP3c10s2/signal;
in6: rNP3c10s1/signal;
in7: rNP3c09s2/signal;
in8: rNP3c09s1/signal;

out1: rRLANDTACQc1s7u1/in8;
out2: rRLANDTACQc1s7u1/in7;
out3: rRLANDTACQc1s7u1/in6;
out4: rRLANDTACQc1s7u1/in5;
out5: rRLANDTACQc1s7u1/in4;
out6: rRLANDTACQc1s7u1/in3;
out7: rRLANDTACQc1s7u1/in2;
out8: rRLANDTACQc1s7u1/in1;
}

JOINER_8(rNP3c24)
{
in1: rNP3c16s2/signal;
in2: rNP3c16s1/signal;
in3: rNP3c15s2/signal;
in4: rNP3c15s1/signal;
in5: rNP3c14s2/signal;
in6: rNP3c14s1/signal;
in7: rNP3c13s2/signal;
in8: rNP3c13s1/signal;

out1: rRLANDTACQc1s7u1/in16;
out2: rRLANDTACQc1s7u1/in15;
out3: rRLANDTACQc1s7u1/in14;
out4: rRLANDTACQc1s7u1/in13;
out5: rRLANDTACQc1s7u1/in12;
out6: rRLANDTACQc1s7u1/in11;
out7: rRLANDTACQc1s7u1/in10;
out8: rRLANDTACQc1s7u1/in9;
}

JOINER_8(rNP3c25)
{
in1: rNP3c20s2/signal;
in2: rNP3c20s1/signal;
in3: rNP3c19s2/signal;
in4: rNP3c19s1/signal;
in5: rNP3c18s2/signal;
in6: rNP3c18s1/signal;
in7: rNP3c17s2/signal;
in8: rNP3c17s1/signal;

out1: rRLANDTACQc1s8u1/in8;
out2: rRLANDTACQc1s8u1/in7;
out3: rRLANDTACQc1s8u1/in6;
out4: rRLANDTACQc1s8u1/in5;
out5: rRLANDTACQc1s8u1/in4;
out6: rRLANDTACQc1s8u1/in3;
out7: rRLANDTACQc1s8u1/in2;
out8: rRLANDTACQc1s8u1/in1;
}


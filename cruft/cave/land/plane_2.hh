///////////////////// FILE AUTO-GENERATED WITH gen_planes.pl
DETECTOR_LAND(rNP2c01s1) { LABEL("N02_01_1"); signal: rNP2c21/in8;}
DETECTOR_LAND(rNP2c01s2) { LABEL("N02_01_2"); signal: rNP2c21/in7;}
DETECTOR_LAND(rNP2c02s1) { LABEL("N02_02_1"); signal: rNP2c21/in6;}
DETECTOR_LAND(rNP2c02s2) { LABEL("N02_02_2"); signal: rNP2c21/in5;}
DETECTOR_LAND(rNP2c03s1) { LABEL("N02_03_1"); signal: rNP2c21/in4;}
DETECTOR_LAND(rNP2c03s2) { LABEL("N02_03_2"); signal: rNP2c21/in3;}
DETECTOR_LAND(rNP2c04s1) { LABEL("N02_04_1"); signal: rNP2c21/in2;}
DETECTOR_LAND(rNP2c04s2) { LABEL("N02_04_2"); signal: rNP2c21/in1;}
DETECTOR_LAND(rNP2c05s1) { LABEL("N02_05_1"); signal: rNP2c22/in8;}
DETECTOR_LAND(rNP2c05s2) { LABEL("N02_05_2"); signal: rNP2c22/in7;}
DETECTOR_LAND(rNP2c06s1) { LABEL("N02_06_1"); signal: rNP2c22/in6;}
DETECTOR_LAND(rNP2c06s2) { LABEL("N02_06_2"); signal: rNP2c22/in5;}
DETECTOR_LAND(rNP2c07s1) { LABEL("N02_07_1"); signal: rNP2c22/in4;}
DETECTOR_LAND(rNP2c07s2) { LABEL("N02_07_2"); signal: rNP2c22/in3;}
DETECTOR_LAND(rNP2c08s1) { LABEL("N02_08_1"); signal: rNP2c22/in2;}
DETECTOR_LAND(rNP2c08s2) { LABEL("N02_08_2"); signal: rNP2c22/in1;}
DETECTOR_LAND(rNP2c09s1) { LABEL("N02_09_1"); signal: rNP2c23/in8;}
DETECTOR_LAND(rNP2c09s2) { LABEL("N02_09_2"); signal: rNP2c23/in7;}
DETECTOR_LAND(rNP2c10s1) { LABEL("N02_10_1"); signal: rNP2c23/in6;}
DETECTOR_LAND(rNP2c10s2) { LABEL("N02_10_2"); signal: rNP2c23/in5;}
DETECTOR_LAND(rNP2c11s1) { LABEL("N02_11_1"); signal: rNP2c23/in4;}
DETECTOR_LAND(rNP2c11s2) { LABEL("N02_11_2"); signal: rNP2c23/in3;}
DETECTOR_LAND(rNP2c12s1) { LABEL("N02_12_1"); signal: rNP2c23/in2;}
DETECTOR_LAND(rNP2c12s2) { LABEL("N02_12_2"); signal: rNP2c23/in1;}
DETECTOR_LAND(rNP2c13s1) { LABEL("N02_13_1"); signal: rNP2c24/in8;}
DETECTOR_LAND(rNP2c13s2) { LABEL("N02_13_2"); signal: rNP2c24/in7;}
DETECTOR_LAND(rNP2c14s1) { LABEL("N02_14_1"); signal: rNP2c24/in6;}
DETECTOR_LAND(rNP2c14s2) { LABEL("N02_14_2"); signal: rNP2c24/in5;}
DETECTOR_LAND(rNP2c15s1) { LABEL("N02_15_1"); signal: rNP2c24/in4;}
DETECTOR_LAND(rNP2c15s2) { LABEL("N02_15_2"); signal: rNP2c24/in3;}
DETECTOR_LAND(rNP2c16s1) { LABEL("N02_16_1"); signal: rNP2c24/in2;}
DETECTOR_LAND(rNP2c16s2) { LABEL("N02_16_2"); signal: rNP2c24/in1;}
DETECTOR_LAND(rNP2c17s1) { LABEL("N02_17_1"); signal: rNP2c25/in8;}
DETECTOR_LAND(rNP2c17s2) { LABEL("N02_17_2"); signal: rNP2c25/in7;}
DETECTOR_LAND(rNP2c18s1) { LABEL("N02_18_1"); signal: rNP2c25/in6;}
DETECTOR_LAND(rNP2c18s2) { LABEL("N02_18_2"); signal: rNP2c25/in5;}
DETECTOR_LAND(rNP2c19s1) { LABEL("N02_19_1"); signal: rNP2c25/in4;}
DETECTOR_LAND(rNP2c19s2) { LABEL("N02_19_2"); signal: rNP2c25/in3;}
DETECTOR_LAND(rNP2c20s1) { LABEL("N02_20_1"); signal: rNP2c25/in2;}
DETECTOR_LAND(rNP2c20s2) { LABEL("N02_20_2"); signal: rNP2c25/in1;}


////// JOINER8 PART --- auto-generated 

JOINER_8(rNP2c21)
{
in1: rNP2c04s2/signal;
in2: rNP2c04s1/signal;
in3: rNP2c03s2/signal;
in4: rNP2c03s1/signal;
in5: rNP2c02s2/signal;
in6: rNP2c02s1/signal;
in7: rNP2c01s2/signal;
in8: rNP2c01s1/signal;

out1: rRLANDTACQc1s3u1/in16;
out2: rRLANDTACQc1s3u1/in15;
out3: rRLANDTACQc1s3u1/in14;
out4: rRLANDTACQc1s3u1/in13;
out5: rRLANDTACQc1s3u1/in12;
out6: rRLANDTACQc1s3u1/in11;
out7: rRLANDTACQc1s3u1/in10;
out8: rRLANDTACQc1s3u1/in9;
}

JOINER_8(rNP2c22)
{
in1: rNP2c08s2/signal;
in2: rNP2c08s1/signal;
in3: rNP2c07s2/signal;
in4: rNP2c07s1/signal;
in5: rNP2c06s2/signal;
in6: rNP2c06s1/signal;
in7: rNP2c05s2/signal;
in8: rNP2c05s1/signal;

out1: rRLANDTACQc1s4u1/in8;
out2: rRLANDTACQc1s4u1/in7;
out3: rRLANDTACQc1s4u1/in6;
out4: rRLANDTACQc1s4u1/in5;
out5: rRLANDTACQc1s4u1/in4;
out6: rRLANDTACQc1s4u1/in3;
out7: rRLANDTACQc1s4u1/in2;
out8: rRLANDTACQc1s4u1/in1;
}

JOINER_8(rNP2c23)
{
in1: rNP2c12s2/signal;
in2: rNP2c12s1/signal;
in3: rNP2c11s2/signal;
in4: rNP2c11s1/signal;
in5: rNP2c10s2/signal;
in6: rNP2c10s1/signal;
in7: rNP2c09s2/signal;
in8: rNP2c09s1/signal;

out1: rRLANDTACQc1s4u1/in16;
out2: rRLANDTACQc1s4u1/in15;
out3: rRLANDTACQc1s4u1/in14;
out4: rRLANDTACQc1s4u1/in13;
out5: rRLANDTACQc1s4u1/in12;
out6: rRLANDTACQc1s4u1/in11;
out7: rRLANDTACQc1s4u1/in10;
out8: rRLANDTACQc1s4u1/in9;
}

JOINER_8(rNP2c24)
{
in1: rNP2c16s2/signal;
in2: rNP2c16s1/signal;
in3: rNP2c15s2/signal;
in4: rNP2c15s1/signal;
in5: rNP2c14s2/signal;
in6: rNP2c14s1/signal;
in7: rNP2c13s2/signal;
in8: rNP2c13s1/signal;

out1: rRLANDTACQc1s5u1/in8;
out2: rRLANDTACQc1s5u1/in7;
out3: rRLANDTACQc1s5u1/in6;
out4: rRLANDTACQc1s5u1/in5;
out5: rRLANDTACQc1s5u1/in4;
out6: rRLANDTACQc1s5u1/in3;
out7: rRLANDTACQc1s5u1/in2;
out8: rRLANDTACQc1s5u1/in1;
}

JOINER_8(rNP2c25)
{
in1: rNP2c20s2/signal;
in2: rNP2c20s1/signal;
in3: rNP2c19s2/signal;
in4: rNP2c19s1/signal;
in5: rNP2c18s2/signal;
in6: rNP2c18s1/signal;
in7: rNP2c17s2/signal;
in8: rNP2c17s1/signal;

out1: rRLANDTACQc1s5u1/in16;
out2: rRLANDTACQc1s5u1/in15;
out3: rRLANDTACQc1s5u1/in14;
out4: rRLANDTACQc1s5u1/in13;
out5: rRLANDTACQc1s5u1/in12;
out6: rRLANDTACQc1s5u1/in11;
out7: rRLANDTACQc1s5u1/in10;
out8: rRLANDTACQc1s5u1/in9;
}


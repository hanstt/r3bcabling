///////////////////// FILE AUTO-GENERATED WITH gen_planes.pl
DETECTOR_LAND(rNP8c01s1) { LABEL("N08_01_1"); signal: rNP8c21/in8;}
DETECTOR_LAND(rNP8c01s2) { LABEL("N08_01_2"); signal: rNP8c21/in7;}
DETECTOR_LAND(rNP8c02s1) { LABEL("N08_02_1"); signal: rNP8c21/in6;}
DETECTOR_LAND(rNP8c02s2) { LABEL("N08_02_2"); signal: rNP8c21/in5;}
DETECTOR_LAND(rNP8c03s1) { LABEL("N08_03_1"); signal: rNP8c21/in4;}
DETECTOR_LAND(rNP8c03s2) { LABEL("N08_03_2"); signal: rNP8c21/in3;}
DETECTOR_LAND(rNP8c04s1) { LABEL("N08_04_1"); signal: rNP8c21/in2;}
DETECTOR_LAND(rNP8c04s2) { LABEL("N08_04_2"); signal: rNP8c21/in1;}
DETECTOR_LAND(rNP8c05s1) { LABEL("N08_05_1"); signal: rNP8c22/in8;}
DETECTOR_LAND(rNP8c05s2) { LABEL("N08_05_2"); signal: rNP8c22/in7;}
DETECTOR_LAND(rNP8c06s1) { LABEL("N08_06_1"); signal: rNP8c22/in6;}
DETECTOR_LAND(rNP8c06s2) { LABEL("N08_06_2"); signal: rNP8c22/in5;}
DETECTOR_LAND(rNP8c07s1) { LABEL("N08_07_1"); signal: rNP8c22/in4;}
DETECTOR_LAND(rNP8c07s2) { LABEL("N08_07_2"); signal: rNP8c22/in3;}
DETECTOR_LAND(rNP8c08s1) { LABEL("N08_08_1"); signal: rNP8c22/in2;}
DETECTOR_LAND(rNP8c08s2) { LABEL("N08_08_2"); signal: rNP8c22/in1;}
DETECTOR_LAND(rNP8c09s1) { LABEL("N08_09_1"); signal: rNP8c23/in8;}
DETECTOR_LAND(rNP8c09s2) { LABEL("N08_09_2"); signal: rNP8c23/in7;}
DETECTOR_LAND(rNP8c10s1) { LABEL("N08_10_1"); signal: rNP8c23/in6;}
DETECTOR_LAND(rNP8c10s2) { LABEL("N08_10_2"); signal: rNP8c23/in5;}
DETECTOR_LAND(rNP8c11s1) { LABEL("N08_11_1"); signal: rNP8c23/in4;}
DETECTOR_LAND(rNP8c11s2) { LABEL("N08_11_2"); signal: rNP8c23/in3;}
DETECTOR_LAND(rNP8c12s1) { LABEL("N08_12_1"); signal: rNP8c23/in2;}
DETECTOR_LAND(rNP8c12s2) { LABEL("N08_12_2"); signal: rNP8c23/in1;}
DETECTOR_LAND(rNP8c13s1) { LABEL("N08_13_1"); signal: rNP8c24/in8;}
DETECTOR_LAND(rNP8c13s2) { LABEL("N08_13_2"); signal: rNP8c24/in7;}
DETECTOR_LAND(rNP8c14s1) { LABEL("N08_14_1"); signal: rNP8c24/in6;}
DETECTOR_LAND(rNP8c14s2) { LABEL("N08_14_2"); signal: rNP8c24/in5;}
DETECTOR_LAND(rNP8c15s1) { LABEL("N08_15_1"); signal: rNP8c24/in4;}
DETECTOR_LAND(rNP8c15s2) { LABEL("N08_15_2"); signal: rNP8c24/in3;}
DETECTOR_LAND(rNP8c16s1) { LABEL("N08_16_1"); signal: rNP8c24/in2;}
DETECTOR_LAND(rNP8c16s2) { LABEL("N08_16_2"); signal: rNP8c24/in1;}
DETECTOR_LAND(rNP8c17s1) { LABEL("N08_17_1"); signal: rNP8c25/in8;}
DETECTOR_LAND(rNP8c17s2) { LABEL("N08_17_2"); signal: rNP8c25/in7;}
DETECTOR_LAND(rNP8c18s1) { LABEL("N08_18_1"); signal: rNP8c25/in6;}
DETECTOR_LAND(rNP8c18s2) { LABEL("N08_18_2"); signal: rNP8c25/in5;}
DETECTOR_LAND(rNP8c19s1) { LABEL("N08_19_1"); signal: rNP8c25/in4;}
DETECTOR_LAND(rNP8c19s2) { LABEL("N08_19_2"); signal: rNP8c25/in3;}
DETECTOR_LAND(rNP8c20s1) { LABEL("N08_20_1"); signal: rNP8c25/in2;}
DETECTOR_LAND(rNP8c20s2) { LABEL("N08_20_2"); signal: rNP8c25/in1;}


////// JOINER8 PART --- auto-generated 

JOINER_8(rNP8c21)
{
in1: rNP8c04s2/signal;
in2: rNP8c04s1/signal;
in3: rNP8c03s2/signal;
in4: rNP8c03s1/signal;
in5: rNP8c02s2/signal;
in6: rNP8c02s1/signal;
in7: rNP8c01s2/signal;
in8: rNP8c01s1/signal;

out1: rRLANDTACQc2s8u1/in16;
out2: rRLANDTACQc2s8u1/in15;
out3: rRLANDTACQc2s8u1/in14;
out4: rRLANDTACQc2s8u1/in13;
out5: rRLANDTACQc2s8u1/in12;
out6: rRLANDTACQc2s8u1/in11;
out7: rRLANDTACQc2s8u1/in10;
out8: rRLANDTACQc2s8u1/in9;
}

JOINER_8(rNP8c22)
{
in1: rNP8c08s2/signal;
in2: rNP8c08s1/signal;
in3: rNP8c07s2/signal;
in4: rNP8c07s1/signal;
in5: rNP8c06s2/signal;
in6: rNP8c06s1/signal;
in7: rNP8c05s2/signal;
in8: rNP8c05s1/signal;

out1: rRLANDTACQc2s9u1/in8;
out2: rRLANDTACQc2s9u1/in7;
out3: rRLANDTACQc2s9u1/in6;
out4: rRLANDTACQc2s9u1/in5;
out5: rRLANDTACQc2s9u1/in4;
out6: rRLANDTACQc2s9u1/in3;
out7: rRLANDTACQc2s9u1/in2;
out8: rRLANDTACQc2s9u1/in1;
}

JOINER_8(rNP8c23)
{
in1: rNP8c12s2/signal;
in2: rNP8c12s1/signal;
in3: rNP8c11s2/signal;
in4: rNP8c11s1/signal;
in5: rNP8c10s2/signal;
in6: rNP8c10s1/signal;
in7: rNP8c09s2/signal;
in8: rNP8c09s1/signal;

out1: rRLANDTACQc2s9u1/in16;
out2: rRLANDTACQc2s9u1/in15;
out3: rRLANDTACQc2s9u1/in14;
out4: rRLANDTACQc2s9u1/in13;
out5: rRLANDTACQc2s9u1/in12;
out6: rRLANDTACQc2s9u1/in11;
out7: rRLANDTACQc2s9u1/in10;
out8: rRLANDTACQc2s9u1/in9;
}

JOINER_8(rNP8c24)
{
in1: rNP8c16s2/signal;
in2: rNP8c16s1/signal;
in3: rNP8c15s2/signal;
in4: rNP8c15s1/signal;
in5: rNP8c14s2/signal;
in6: rNP8c14s1/signal;
in7: rNP8c13s2/signal;
in8: rNP8c13s1/signal;

out1: rRLANDTACQc2s10u1/in8;
out2: rRLANDTACQc2s10u1/in7;
out3: rRLANDTACQc2s10u1/in6;
out4: rRLANDTACQc2s10u1/in5;
out5: rRLANDTACQc2s10u1/in4;
out6: rRLANDTACQc2s10u1/in3;
out7: rRLANDTACQc2s10u1/in2;
out8: rRLANDTACQc2s10u1/in1;
}

JOINER_8(rNP8c25)
{
in1: rNP8c20s2/signal;
in2: rNP8c20s1/signal;
in3: rNP8c19s2/signal;
in4: rNP8c19s1/signal;
in5: rNP8c18s2/signal;
in6: rNP8c18s1/signal;
in7: rNP8c17s2/signal;
in8: rNP8c17s1/signal;

out1: rRLANDTACQc2s10u1/in16;
out2: rRLANDTACQc2s10u1/in15;
out3: rRLANDTACQc2s10u1/in14;
out4: rRLANDTACQc2s10u1/in13;
out5: rRLANDTACQc2s10u1/in12;
out6: rRLANDTACQc2s10u1/in11;
out7: rRLANDTACQc2s10u1/in10;
out8: rRLANDTACQc2s10u1/in9;
}


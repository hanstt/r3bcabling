///////////////////// FILE AUTO-GENERATED WITH gen_planes.pl
DETECTOR_LAND(rNP6c01s1) { LABEL("N06_01_1"); signal: rNP6c21/in8;}
DETECTOR_LAND(rNP6c01s2) { LABEL("N06_01_2"); signal: rNP6c21/in7;}
DETECTOR_LAND(rNP6c02s1) { LABEL("N06_02_1"); signal: rNP6c21/in6;}
DETECTOR_LAND(rNP6c02s2) { LABEL("N06_02_2"); signal: rNP6c21/in5;}
DETECTOR_LAND(rNP6c03s1) { LABEL("N06_03_1"); signal: rNP6c21/in4;}
DETECTOR_LAND(rNP6c03s2) { LABEL("N06_03_2"); signal: rNP6c21/in3;}
DETECTOR_LAND(rNP6c04s1) { LABEL("N06_04_1"); signal: rNP6c21/in2;}
DETECTOR_LAND(rNP6c04s2) { LABEL("N06_04_2"); signal: rNP6c21/in1;}
DETECTOR_LAND(rNP6c05s1) { LABEL("N06_05_1"); signal: rNP6c22/in8;}
DETECTOR_LAND(rNP6c05s2) { LABEL("N06_05_2"); signal: rNP6c22/in7;}
DETECTOR_LAND(rNP6c06s1) { LABEL("N06_06_1"); signal: rNP6c22/in6;}
DETECTOR_LAND(rNP6c06s2) { LABEL("N06_06_2"); signal: rNP6c22/in5;}
DETECTOR_LAND(rNP6c07s1) { LABEL("N06_07_1"); signal: rNP6c22/in4;}
DETECTOR_LAND(rNP6c07s2) { LABEL("N06_07_2"); signal: rNP6c22/in3;}
DETECTOR_LAND(rNP6c08s1) { LABEL("N06_08_1"); signal: rNP6c22/in2;}
DETECTOR_LAND(rNP6c08s2) { LABEL("N06_08_2"); signal: rNP6c22/in1;}
DETECTOR_LAND(rNP6c09s1) { LABEL("N06_09_1"); signal: rNP6c23/in8;}
DETECTOR_LAND(rNP6c09s2) { LABEL("N06_09_2"); signal: rNP6c23/in7;}
DETECTOR_LAND(rNP6c10s1) { LABEL("N06_10_1"); signal: rNP6c23/in6;}
DETECTOR_LAND(rNP6c10s2) { LABEL("N06_10_2"); signal: rNP6c23/in5;}
DETECTOR_LAND(rNP6c11s1) { LABEL("N06_11_1"); signal: rNP6c23/in4;}
DETECTOR_LAND(rNP6c11s2) { LABEL("N06_11_2"); signal: rNP6c23/in3;}
DETECTOR_LAND(rNP6c12s1) { LABEL("N06_12_1"); signal: rNP6c23/in2;}
DETECTOR_LAND(rNP6c12s2) { LABEL("N06_12_2"); signal: rNP6c23/in1;}
DETECTOR_LAND(rNP6c13s1) { LABEL("N06_13_1"); signal: rNP6c24/in8;}
DETECTOR_LAND(rNP6c13s2) { LABEL("N06_13_2"); signal: rNP6c24/in7;}
DETECTOR_LAND(rNP6c14s1) { LABEL("N06_14_1"); signal: rNP6c24/in6;}
DETECTOR_LAND(rNP6c14s2) { LABEL("N06_14_2"); signal: rNP6c24/in5;}
DETECTOR_LAND(rNP6c15s1) { LABEL("N06_15_1"); signal: rNP6c24/in4;}
DETECTOR_LAND(rNP6c15s2) { LABEL("N06_15_2"); signal: rNP6c24/in3;}
DETECTOR_LAND(rNP6c16s1) { LABEL("N06_16_1"); signal: rNP6c24/in2;}
DETECTOR_LAND(rNP6c16s2) { LABEL("N06_16_2"); signal: rNP6c24/in1;}
DETECTOR_LAND(rNP6c17s1) { LABEL("N06_17_1"); signal: rNP6c25/in8;}
DETECTOR_LAND(rNP6c17s2) { LABEL("N06_17_2"); signal: rNP6c25/in7;}
DETECTOR_LAND(rNP6c18s1) { LABEL("N06_18_1"); signal: rNP6c25/in6;}
DETECTOR_LAND(rNP6c18s2) { LABEL("N06_18_2"); signal: rNP6c25/in5;}
DETECTOR_LAND(rNP6c19s1) { LABEL("N06_19_1"); signal: rNP6c25/in4;}
DETECTOR_LAND(rNP6c19s2) { LABEL("N06_19_2"); signal: rNP6c25/in3;}
DETECTOR_LAND(rNP6c20s1) { LABEL("N06_20_1"); signal: rNP6c25/in2;}
DETECTOR_LAND(rNP6c20s2) { LABEL("N06_20_2"); signal: rNP6c25/in1;}


////// JOINER8 PART --- auto-generated 

JOINER_8(rNP6c21)
{
in1: rNP6c04s2/signal;
in2: rNP6c04s1/signal;
in3: rNP6c03s2/signal;
in4: rNP6c03s1/signal;
in5: rNP6c02s2/signal;
in6: rNP6c02s1/signal;
in7: rNP6c01s2/signal;
in8: rNP6c01s1/signal;

out1: rRLANDTACQc2s3u1/in16;
out2: rRLANDTACQc2s3u1/in15;
out3: rRLANDTACQc2s3u1/in14;
out4: rRLANDTACQc2s3u1/in13;
out5: rRLANDTACQc2s3u1/in12;
out6: rRLANDTACQc2s3u1/in11;
out7: rRLANDTACQc2s3u1/in10;
out8: rRLANDTACQc2s3u1/in9;
}

JOINER_8(rNP6c22)
{
in1: rNP6c08s2/signal;
in2: rNP6c08s1/signal;
in3: rNP6c07s2/signal;
in4: rNP6c07s1/signal;
in5: rNP6c06s2/signal;
in6: rNP6c06s1/signal;
in7: rNP6c05s2/signal;
in8: rNP6c05s1/signal;

out1: rRLANDTACQc2s4u1/in8;
out2: rRLANDTACQc2s4u1/in7;
out3: rRLANDTACQc2s4u1/in6;
out4: rRLANDTACQc2s4u1/in5;
out5: rRLANDTACQc2s4u1/in4;
out6: rRLANDTACQc2s4u1/in3;
out7: rRLANDTACQc2s4u1/in2;
out8: rRLANDTACQc2s4u1/in1;
}

JOINER_8(rNP6c23)
{
in1: rNP6c12s2/signal;
in2: rNP6c12s1/signal;
in3: rNP6c11s2/signal;
in4: rNP6c11s1/signal;
in5: rNP6c10s2/signal;
in6: rNP6c10s1/signal;
in7: rNP6c09s2/signal;
in8: rNP6c09s1/signal;

out1: rRLANDTACQc2s4u1/in16;
out2: rRLANDTACQc2s4u1/in15;
out3: rRLANDTACQc2s4u1/in14;
out4: rRLANDTACQc2s4u1/in13;
out5: rRLANDTACQc2s4u1/in12;
out6: rRLANDTACQc2s4u1/in11;
out7: rRLANDTACQc2s4u1/in10;
out8: rRLANDTACQc2s4u1/in9;
}

JOINER_8(rNP6c24)
{
in1: rNP6c16s2/signal;
in2: rNP6c16s1/signal;
in3: rNP6c15s2/signal;
in4: rNP6c15s1/signal;
in5: rNP6c14s2/signal;
in6: rNP6c14s1/signal;
in7: rNP6c13s2/signal;
in8: rNP6c13s1/signal;

out1: rRLANDTACQc2s5u1/in8;
out2: rRLANDTACQc2s5u1/in7;
out3: rRLANDTACQc2s5u1/in6;
out4: rRLANDTACQc2s5u1/in5;
out5: rRLANDTACQc2s5u1/in4;
out6: rRLANDTACQc2s5u1/in3;
out7: rRLANDTACQc2s5u1/in2;
out8: rRLANDTACQc2s5u1/in1;
}

JOINER_8(rNP6c25)
{
in1: rNP6c20s2/signal;
in2: rNP6c20s1/signal;
in3: rNP6c19s2/signal;
in4: rNP6c19s1/signal;
in5: rNP6c18s2/signal;
in6: rNP6c18s1/signal;
in7: rNP6c17s2/signal;
in8: rNP6c17s1/signal;

out1: rRLANDTACQc2s5u1/in16;
out2: rRLANDTACQc2s5u1/in15;
out3: rRLANDTACQc2s5u1/in14;
out4: rRLANDTACQc2s5u1/in13;
out5: rRLANDTACQc2s5u1/in12;
out6: rRLANDTACQc2s5u1/in11;
out7: rRLANDTACQc2s5u1/in10;
out8: rRLANDTACQc2s5u1/in9;
}


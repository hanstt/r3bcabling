// example for new cabling of LAND - 2012 March, 14th KB
DETECTOR_LAND(rNPYc01s1) { LABEL("NXY_01_1"); signal: rNPYc21/in8;}
DETECTOR_LAND(rNPYc01s2) { LABEL("NXY_01_2"); signal: rNPYc21/in7;}
DETECTOR_LAND(rNPYc02s1) { LABEL("NXY_02_1"); signal: rNPYc21/in6;}
DETECTOR_LAND(rNPYc02s2) { LABEL("NXY_02_2"); signal: rNPYc21/in5;}
DETECTOR_LAND(rNPYc03s1) { LABEL("NXY_03_1"); signal: rNPYc21/in4;}
DETECTOR_LAND(rNPYc03s2) { LABEL("NXY_03_2"); signal: rNPYc21/in3;}
DETECTOR_LAND(rNPYc04s1) { LABEL("NXY_04_1"); signal: rNPYc21/in2;}
DETECTOR_LAND(rNPYc04s2) { LABEL("NXY_04_2"); signal: rNPYc21/in1;}
//
DETECTOR_LAND(rNPYc05s1) { LABEL("NXY_05_1"); signal: rNPYc22/in8;}
DETECTOR_LAND(rNPYc05s2) { LABEL("NXY_05_2"); signal: rNPYc22/in7;}
DETECTOR_LAND(rNPYc06s1) { LABEL("NXY_06_1"); signal: rNPYc22/in6;}
DETECTOR_LAND(rNPYc06s2) { LABEL("NXY_06_2"); signal: rNPYc22/in5;}
DETECTOR_LAND(rNPYc07s1) { LABEL("NXY_07_1"); signal: rNPYc22/in4;}
DETECTOR_LAND(rNPYc07s2) { LABEL("NXY_07_2"); signal: rNPYc22/in3;}
DETECTOR_LAND(rNPYc08s1) { LABEL("NXY_08_1"); signal: rNPYc22/in2;}
DETECTOR_LAND(rNPYc08s2) { LABEL("NXY_08_2"); signal: rNPYc22/in1;}
//
DETECTOR_LAND(rNPYc09s1) { LABEL("NXY_09_1"); signal: rNPYc23/in8;}
DETECTOR_LAND(rNPYc09s2) { LABEL("NXY_09_2"); signal: rNPYc23/in7;}
DETECTOR_LAND(rNPYc10s1) { LABEL("NXY_10_1"); signal: rNPYc23/in6;}
DETECTOR_LAND(rNPYc10s2) { LABEL("NXY_10_2"); signal: rNPYc23/in5;}
DETECTOR_LAND(rNPYc11s1) { LABEL("NXY_11_1"); signal: rNPYc23/in4;}
DETECTOR_LAND(rNPYc11s2) { LABEL("NXY_11_2"); signal: rNPYc23/in3;}
DETECTOR_LAND(rNPYc12s1) { LABEL("NXY_12_1"); signal: rNPYc23/in2;}
DETECTOR_LAND(rNPYc12s2) { LABEL("NXY_12_2"); signal: rNPYc23/in1;}
//
DETECTOR_LAND(rNPYc13s1) { LABEL("NXY_13_1"); signal: rNPYc24/in8;}
DETECTOR_LAND(rNPYc13s2) { LABEL("NXY_13_2"); signal: rNPYc24/in7;}
DETECTOR_LAND(rNPYc14s1) { LABEL("NXY_14_1"); signal: rNPYc24/in6;}
DETECTOR_LAND(rNPYc14s2) { LABEL("NXY_14_2"); signal: rNPYc24/in5;}
DETECTOR_LAND(rNPYc15s1) { LABEL("NXY_15_1"); signal: rNPYc24/in4;}
DETECTOR_LAND(rNPYc15s2) { LABEL("NXY_15_2"); signal: rNPYc24/in3;}
DETECTOR_LAND(rNPYc16s1) { LABEL("NXY_16_1"); signal: rNPYc24/in2;}
DETECTOR_LAND(rNPYc16s2) { LABEL("NXY_16_2"); signal: rNPYc24/in1;}
//
DETECTOR_LAND(rNPYc17s1) { LABEL("NXY_17_1"); signal: rNPYc25/in8;}
DETECTOR_LAND(rNPYc17s2) { LABEL("NXY_17_2"); signal: rNPYc25/in7;}
DETECTOR_LAND(rNPYc18s1) { LABEL("NXY_18_1"); signal: rNPYc25/in6;}
DETECTOR_LAND(rNPYc18s2) { LABEL("NXY_18_2"); signal: rNPYc25/in5;}
DETECTOR_LAND(rNPYc19s1) { LABEL("NXY_19_1"); signal: rNPYc25/in4;}
DETECTOR_LAND(rNPYc19s2) { LABEL("NXY_19_2"); signal: rNPYc25/in3;}
DETECTOR_LAND(rNPYc20s1) { LABEL("NXY_20_1"); signal: rNPYc25/in2;}
DETECTOR_LAND(rNPYc20s2) { LABEL("NXY_20_2"); signal: rNPYc25/in1;}

JOINER_8(rNPYc21)
{
 in1: rNPYc04s2/signal;
 in2: rNPYc04s1/signal;
 in3: rNPYc03s2/signal;
 in4: rNPYc03s1/signal;
 in5: rNPYc02s2/signal;
 in6: rNPYc02s1/signal;
 in7: rNPYc01s2/signal;
 in8: rNPYc01s1/signal;

 out1: r13c1s1u1/in8;
 out2: r13c1s1u1/in7;
 out3: r13c1s1u1/in6;
 out4: r13c1s1u1/in5;
 out5: r13c1s1u1/in4;
 out6: r13c1s1u1/in3;
 out7: r13c1s1u1/in2;
 out8: r13c1s1u1/in1;
}
JOINER_8(rNPYc22)
{
 in1: rNPYc08s2/signal;
 in2: rNPYc08s1/signal;
 in3: rNPYc07s2/signal;
 in4: rNPYc07s1/signal;
 in5: rNPYc06s2/signal;
 in6: rNPYc06s1/signal;
 in7: rNPYc05s2/signal;
 in8: rNPYc05s1/signal;

 out1: r13c1s1u1/in16;
 out2: r13c1s1u1/in15;
 out3: r13c1s1u1/in14;
 out4: r13c1s1u1/in13;
 out5: r13c1s1u1/in12;
 out6: r13c1s1u1/in11;
 out7: r13c1s1u1/in10;
 out8: r13c1s1u1/in9;
}
JOINER_8(rNPYc23)
{
 in1: rNPYc12s2/signal;
 in2: rNPYc12s1/signal;
 in3: rNPYc11s2/signal;
 in4: rNPYc11s1/signal;
 in5: rNPYc10s2/signal;
 in6: rNPYc10s1/signal;
 in7: rNPYc09s2/signal;
 in8: rNPYc09s1/signal;
 
 out1: r13c1s2u1/in8;
 out2: r13c1s2u1/in7;
 out3: r13c1s2u1/in6;
 out4: r13c1s2u1/in5;
 out5: r13c1s2u1/in4;
 out6: r13c1s2u1/in3;
 out7: r13c1s2u1/in2;
 out8: r13c1s2u1/in1;
}
JOINER_8(rNPYc24)
{
 in1: rNPYc06s2/signal;
 in2: rNPYc16s1/signal;
 in3: rNPYc15s2/signal;
 in4: rNPYc15s1/signal;
 in5: rNPYc14s2/signal;
 in6: rNPYc14s1/signal;
 in7: rNPYc13s2/signal;
 in8: rNPYc13s1/signal;
 
 out1: r13c1s2u1/in16;
 out2: r13c1s2u1/in15;
 out3: r13c1s2u1/in14;
 out4: r13c1s2u1/in13;
 out5: r13c1s2u1/in12;
 out6: r13c1s2u1/in11;
 out7: r13c1s2u1/in10;
 out8: r13c1s2u1/in9;
}
JOINER_8(rNPYc25)
{
 in1: rNPYc20s2/signal;
 in2: rNPYc20s1/signal;
 in3: rNPYc19s2/signal;
 in4: rNPYc19s1/signal;
 in5: rNPYc18s2/signal;
 in6: rNPYc18s1/signal;
 in7: rNPYc17s2/signal;
 in8: rNPYc17s1/signal;

 out1: r13c1s3u1/in8;
 out2: r13c1s3u1/in7;
 out3: r13c1s3u1/in6;
 out4: r13c1s3u1/in5;
 out5: r13c1s3u1/in4;
 out6: r13c1s3u1/in3;
 out7: r13c1s3u1/in2;
 out8: r13c1s3u1/in1;
 
 }
 // I did not touch this HV part - KB
 /*
HV_CONN_16(rNPYc31)
{
// in1_16: r16c1s0/out0_15;
 out1: rNPYc01s1/hv;
 out2: rNPYc02s1/hv;
 out3: rNPYc03s1/hv;
 out4: rNPYc04s1/hv;
 out5: rNPYc05s1/hv;
 out6: rNPYc06s1/hv;
 out7: rNPYc07s1/hv;
 out8: rNPYc08s1/hv;
 out9: rNPYc09s1/hv;
 out10: rNPYc10s1/hv;
 out11: rNPYc11s1/hv;
 out12: rNPYc12s1/hv;
 out13: rNPYc13s1/hv;
 out14: rNPYc14s1/hv;
 out15: rNPYc15s1/hv;
 out16: rNPYc16s1/hv;
}
HV_CONN_16(rNPYc32)
{
// in1_8: r16c1s2/out0_7;
 out1: rNPYc17s1/hv;
 out2: rNPYc18s1/hv;
 out3: rNPYc19s1/hv;
 out4: rNPYc20s1/hv;
 out5: rNPYc17s2/hv;
 out6: rNPYc18s2/hv;
 out7: rNPYc19s2/hv;
 out8: rNPYc20s2/hv;
}
HV_CONN_16(rNPYc33)
{
// in1_16: r16c1s1/out0_15;
 out1: rNPYc01s2/hv;
 out2: rNPYc02s2/hv;
 out3: rNPYc03s2/hv;
 out4: rNPYc04s2/hv;
 out5: rNPYc05s2/hv;
 out6: rNPYc06s2/hv;
 out7: rNPYc07s2/hv;
 out8: rNPYc08s2/hv;
 out9: rNPYc09s2/hv;
 out10: rNPYc10s2/hv;
 out11: rNPYc11s2/hv;
 out12: rNPYc12s2/hv;
 out13: rNPYc13s2/hv;
 out14: rNPYc14s2/hv;
 out15: rNPYc15s2/hv;
 out16: rNPYc16s2/hv;
}
*/

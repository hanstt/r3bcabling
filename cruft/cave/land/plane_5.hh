///////////////////// FILE AUTO-GENERATED WITH gen_planes.pl
DETECTOR_LAND(rNP5c01s1) { LABEL("N05_01_1"); signal: rNP5c21/in8;}
DETECTOR_LAND(rNP5c01s2) { LABEL("N05_01_2"); signal: rNP5c21/in7;}
DETECTOR_LAND(rNP5c02s1) { LABEL("N05_02_1"); signal: rNP5c21/in6;}
DETECTOR_LAND(rNP5c02s2) { LABEL("N05_02_2"); signal: rNP5c21/in5;}
DETECTOR_LAND(rNP5c03s1) { LABEL("N05_03_1"); signal: rNP5c21/in4;}
DETECTOR_LAND(rNP5c03s2) { LABEL("N05_03_2"); signal: rNP5c21/in3;}
DETECTOR_LAND(rNP5c04s1) { LABEL("N05_04_1"); signal: rNP5c21/in2;}
DETECTOR_LAND(rNP5c04s2) { LABEL("N05_04_2"); signal: rNP5c21/in1;}
DETECTOR_LAND(rNP5c05s1) { LABEL("N05_05_1"); signal: rNP5c22/in8;}
DETECTOR_LAND(rNP5c05s2) { LABEL("N05_05_2"); signal: rNP5c22/in7;}
DETECTOR_LAND(rNP5c06s1) { LABEL("N05_06_1"); signal: rNP5c22/in6;}
DETECTOR_LAND(rNP5c06s2) { LABEL("N05_06_2"); signal: rNP5c22/in5;}
DETECTOR_LAND(rNP5c07s1) { LABEL("N05_07_1"); signal: rNP5c22/in4;}
DETECTOR_LAND(rNP5c07s2) { LABEL("N05_07_2"); signal: rNP5c22/in3;}
DETECTOR_LAND(rNP5c08s1) { LABEL("N05_08_1"); signal: rNP5c22/in2;}
DETECTOR_LAND(rNP5c08s2) { LABEL("N05_08_2"); signal: rNP5c22/in1;}
DETECTOR_LAND(rNP5c09s1) { LABEL("N05_09_1"); signal: rNP5c23/in8;}
DETECTOR_LAND(rNP5c09s2) { LABEL("N05_09_2"); signal: rNP5c23/in7;}
DETECTOR_LAND(rNP5c10s1) { LABEL("N05_10_1"); signal: rNP5c23/in6;}
DETECTOR_LAND(rNP5c10s2) { LABEL("N05_10_2"); signal: rNP5c23/in5;}
DETECTOR_LAND(rNP5c11s1) { LABEL("N05_11_1"); signal: rNP5c23/in4;}
DETECTOR_LAND(rNP5c11s2) { LABEL("N05_11_2"); signal: rNP5c23/in3;}
DETECTOR_LAND(rNP5c12s1) { LABEL("N05_12_1"); signal: rNP5c23/in2;}
DETECTOR_LAND(rNP5c12s2) { LABEL("N05_12_2"); signal: rNP5c23/in1;}
DETECTOR_LAND(rNP5c13s1) { LABEL("N05_13_1"); signal: rNP5c24/in8;}
DETECTOR_LAND(rNP5c13s2) { LABEL("N05_13_2"); signal: rNP5c24/in7;}
DETECTOR_LAND(rNP5c14s1) { LABEL("N05_14_1"); signal: rNP5c24/in6;}
DETECTOR_LAND(rNP5c14s2) { LABEL("N05_14_2"); signal: rNP5c24/in5;}
DETECTOR_LAND(rNP5c15s1) { LABEL("N05_15_1"); signal: rNP5c24/in4;}
DETECTOR_LAND(rNP5c15s2) { LABEL("N05_15_2"); signal: rNP5c24/in3;}
DETECTOR_LAND(rNP5c16s1) { LABEL("N05_16_1"); signal: rNP5c24/in2;}
DETECTOR_LAND(rNP5c16s2) { LABEL("N05_16_2"); signal: rNP5c24/in1;}
DETECTOR_LAND(rNP5c17s1) { LABEL("N05_17_1"); signal: rNP5c25/in8;}
DETECTOR_LAND(rNP5c17s2) { LABEL("N05_17_2"); signal: rNP5c25/in7;}
DETECTOR_LAND(rNP5c18s1) { LABEL("N05_18_1"); signal: rNP5c25/in6;}
DETECTOR_LAND(rNP5c18s2) { LABEL("N05_18_2"); signal: rNP5c25/in5;}
DETECTOR_LAND(rNP5c19s1) { LABEL("N05_19_1"); signal: rNP5c25/in4;}
DETECTOR_LAND(rNP5c19s2) { LABEL("N05_19_2"); signal: rNP5c25/in3;}
DETECTOR_LAND(rNP5c20s1) { LABEL("N05_20_1"); signal: rNP5c25/in2;}
DETECTOR_LAND(rNP5c20s2) { LABEL("N05_20_2"); signal: rNP5c25/in1;}


////// JOINER8 PART --- auto-generated 

JOINER_8(rNP5c21)
{
in1: rNP5c04s2/signal;
in2: rNP5c04s1/signal;
in3: rNP5c03s2/signal;
in4: rNP5c03s1/signal;
in5: rNP5c02s2/signal;
in6: rNP5c02s1/signal;
in7: rNP5c01s2/signal;
in8: rNP5c01s1/signal;

out1: rRLANDTACQc2s1u1/in8;
out2: rRLANDTACQc2s1u1/in7;
out3: rRLANDTACQc2s1u1/in6;
out4: rRLANDTACQc2s1u1/in5;
out5: rRLANDTACQc2s1u1/in4;
out6: rRLANDTACQc2s1u1/in3;
out7: rRLANDTACQc2s1u1/in2;
out8: rRLANDTACQc2s1u1/in1;
}

JOINER_8(rNP5c22)
{
in1: rNP5c08s2/signal;
in2: rNP5c08s1/signal;
in3: rNP5c07s2/signal;
in4: rNP5c07s1/signal;
in5: rNP5c06s2/signal;
in6: rNP5c06s1/signal;
in7: rNP5c05s2/signal;
in8: rNP5c05s1/signal;

out1: rRLANDTACQc2s1u1/in16;
out2: rRLANDTACQc2s1u1/in15;
out3: rRLANDTACQc2s1u1/in14;
out4: rRLANDTACQc2s1u1/in13;
out5: rRLANDTACQc2s1u1/in12;
out6: rRLANDTACQc2s1u1/in11;
out7: rRLANDTACQc2s1u1/in10;
out8: rRLANDTACQc2s1u1/in9;
}

JOINER_8(rNP5c23)
{
in1: rNP5c12s2/signal;
in2: rNP5c12s1/signal;
in3: rNP5c11s2/signal;
in4: rNP5c11s1/signal;
in5: rNP5c10s2/signal;
in6: rNP5c10s1/signal;
in7: rNP5c09s2/signal;
in8: rNP5c09s1/signal;

out1: rRLANDTACQc2s2u1/in8;
out2: rRLANDTACQc2s2u1/in7;
out3: rRLANDTACQc2s2u1/in6;
out4: rRLANDTACQc2s2u1/in5;
out5: rRLANDTACQc2s2u1/in4;
out6: rRLANDTACQc2s2u1/in3;
out7: rRLANDTACQc2s2u1/in2;
out8: rRLANDTACQc2s2u1/in1;
}

JOINER_8(rNP5c24)
{
in1: rNP5c16s2/signal;
in2: rNP5c16s1/signal;
in3: rNP5c15s2/signal;
in4: rNP5c15s1/signal;
in5: rNP5c14s2/signal;
in6: rNP5c14s1/signal;
in7: rNP5c13s2/signal;
in8: rNP5c13s1/signal;

out1: rRLANDTACQc2s2u1/in16;
out2: rRLANDTACQc2s2u1/in15;
out3: rRLANDTACQc2s2u1/in14;
out4: rRLANDTACQc2s2u1/in13;
out5: rRLANDTACQc2s2u1/in12;
out6: rRLANDTACQc2s2u1/in11;
out7: rRLANDTACQc2s2u1/in10;
out8: rRLANDTACQc2s2u1/in9;
}

JOINER_8(rNP5c25)
{
in1: rNP5c20s2/signal;
in2: rNP5c20s1/signal;
in3: rNP5c19s2/signal;
in4: rNP5c19s1/signal;
in5: rNP5c18s2/signal;
in6: rNP5c18s1/signal;
in7: rNP5c17s2/signal;
in8: rNP5c17s1/signal;

out1: rRLANDTACQc2s3u1/in8;
out2: rRLANDTACQc2s3u1/in7;
out3: rRLANDTACQc2s3u1/in6;
out4: rRLANDTACQc2s3u1/in5;
out5: rRLANDTACQc2s3u1/in4;
out6: rRLANDTACQc2s3u1/in3;
out7: rRLANDTACQc2s3u1/in2;
out8: rRLANDTACQc2s3u1/in1;
}


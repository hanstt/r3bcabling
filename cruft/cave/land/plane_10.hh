///////////////////// FILE AUTO-GENERATED WITH gen_planes.pl
DETECTOR_LAND(rNP10c01s1) { LABEL("N10_01_1"); signal: rNP10c21/in8;}
DETECTOR_LAND(rNP10c01s2) { LABEL("N10_01_2"); signal: rNP10c21/in7;}
DETECTOR_LAND(rNP10c02s1) { LABEL("N10_02_1"); signal: rNP10c21/in6;}
DETECTOR_LAND(rNP10c02s2) { LABEL("N10_02_2"); signal: rNP10c21/in5;}
DETECTOR_LAND(rNP10c03s1) { LABEL("N10_03_1"); signal: rNP10c21/in4;}
DETECTOR_LAND(rNP10c03s2) { LABEL("N10_03_2"); signal: rNP10c21/in3;}
DETECTOR_LAND(rNP10c04s1) { LABEL("N10_04_1"); signal: rNP10c21/in2;}
DETECTOR_LAND(rNP10c04s2) { LABEL("N10_04_2"); signal: rNP10c21/in1;}
DETECTOR_LAND(rNP10c05s1) { LABEL("N10_05_1"); signal: rNP10c22/in8;}
DETECTOR_LAND(rNP10c05s2) { LABEL("N10_05_2"); signal: rNP10c22/in7;}
DETECTOR_LAND(rNP10c06s1) { LABEL("N10_06_1"); signal: rNP10c22/in6;}
DETECTOR_LAND(rNP10c06s2) { LABEL("N10_06_2"); signal: rNP10c22/in5;}
DETECTOR_LAND(rNP10c07s1) { LABEL("N10_07_1"); signal: rNP10c22/in4;}
DETECTOR_LAND(rNP10c07s2) { LABEL("N10_07_2"); signal: rNP10c22/in3;}
DETECTOR_LAND(rNP10c08s1) { LABEL("N10_08_1"); signal: rNP10c22/in2;}
DETECTOR_LAND(rNP10c08s2) { LABEL("N10_08_2"); signal: rNP10c22/in1;}
DETECTOR_LAND(rNP10c09s1) { LABEL("N10_09_1"); signal: rNP10c23/in8;}
DETECTOR_LAND(rNP10c09s2) { LABEL("N10_09_2"); signal: rNP10c23/in7;}
DETECTOR_LAND(rNP10c10s1) { LABEL("N10_10_1"); signal: rNP10c23/in6;}
DETECTOR_LAND(rNP10c10s2) { LABEL("N10_10_2"); signal: rNP10c23/in5;}
DETECTOR_LAND(rNP10c11s1) { LABEL("N10_11_1"); signal: rNP10c23/in4;}
DETECTOR_LAND(rNP10c11s2) { LABEL("N10_11_2"); signal: rNP10c23/in3;}
DETECTOR_LAND(rNP10c12s1) { LABEL("N10_12_1"); signal: rNP10c23/in2;}
DETECTOR_LAND(rNP10c12s2) { LABEL("N10_12_2"); signal: rNP10c23/in1;}
DETECTOR_LAND(rNP10c13s1) { LABEL("N10_13_1"); signal: rNP10c24/in8;}
DETECTOR_LAND(rNP10c13s2) { LABEL("N10_13_2"); signal: rNP10c24/in7;}
DETECTOR_LAND(rNP10c14s1) { LABEL("N10_14_1"); signal: rNP10c24/in6;}
DETECTOR_LAND(rNP10c14s2) { LABEL("N10_14_2"); signal: rNP10c24/in5;}
DETECTOR_LAND(rNP10c15s1) { LABEL("N10_15_1"); signal: rNP10c24/in4;}
DETECTOR_LAND(rNP10c15s2) { LABEL("N10_15_2"); signal: rNP10c24/in3;}
DETECTOR_LAND(rNP10c16s1) { LABEL("N10_16_1"); signal: rNP10c24/in2;}
DETECTOR_LAND(rNP10c16s2) { LABEL("N10_16_2"); signal: rNP10c24/in1;}
DETECTOR_LAND(rNP10c17s1) { LABEL("N10_17_1"); signal: rNP10c25/in8;}
DETECTOR_LAND(rNP10c17s2) { LABEL("N10_17_2"); signal: rNP10c25/in7;}
DETECTOR_LAND(rNP10c18s1) { LABEL("N10_18_1"); signal: rNP10c25/in6;}
DETECTOR_LAND(rNP10c18s2) { LABEL("N10_18_2"); signal: rNP10c25/in5;}
DETECTOR_LAND(rNP10c19s1) { LABEL("N10_19_1"); signal: rNP10c25/in4;}
DETECTOR_LAND(rNP10c19s2) { LABEL("N10_19_2"); signal: rNP10c25/in3;}
DETECTOR_LAND(rNP10c20s1) { LABEL("N10_20_1"); signal: rNP10c25/in2;}
DETECTOR_LAND(rNP10c20s2) { LABEL("N10_20_2"); signal: rNP10c25/in1;}


////// JOINER8 PART --- auto-generated 

JOINER_8(rNP10c21)
{
in1: rNP10c04s2/signal;
in2: rNP10c04s1/signal;
in3: rNP10c03s2/signal;
in4: rNP10c03s1/signal;
in5: rNP10c02s2/signal;
in6: rNP10c02s1/signal;
in7: rNP10c01s2/signal;
in8: rNP10c01s1/signal;

out1: rRLANDTACQc3s3u1/in16;
out2: rRLANDTACQc3s3u1/in15;
out3: rRLANDTACQc3s3u1/in14;
out4: rRLANDTACQc3s3u1/in13;
out5: rRLANDTACQc3s3u1/in12;
out6: rRLANDTACQc3s3u1/in11;
out7: rRLANDTACQc3s3u1/in10;
out8: rRLANDTACQc3s3u1/in9;
}

JOINER_8(rNP10c22)
{
in1: rNP10c08s2/signal;
in2: rNP10c08s1/signal;
in3: rNP10c07s2/signal;
in4: rNP10c07s1/signal;
in5: rNP10c06s2/signal;
in6: rNP10c06s1/signal;
in7: rNP10c05s2/signal;
in8: rNP10c05s1/signal;

out1: rRLANDTACQc3s4u1/in8;
out2: rRLANDTACQc3s4u1/in7;
out3: rRLANDTACQc3s4u1/in6;
out4: rRLANDTACQc3s4u1/in5;
out5: rRLANDTACQc3s4u1/in4;
out6: rRLANDTACQc3s4u1/in3;
out7: rRLANDTACQc3s4u1/in2;
out8: rRLANDTACQc3s4u1/in1;
}

JOINER_8(rNP10c23)
{
in1: rNP10c12s2/signal;
in2: rNP10c12s1/signal;
in3: rNP10c11s2/signal;
in4: rNP10c11s1/signal;
in5: rNP10c10s2/signal;
in6: rNP10c10s1/signal;
in7: rNP10c09s2/signal;
in8: rNP10c09s1/signal;

out1: rRLANDTACQc3s4u1/in16;
out2: rRLANDTACQc3s4u1/in15;
out3: rRLANDTACQc3s4u1/in14;
out4: rRLANDTACQc3s4u1/in13;
out5: rRLANDTACQc3s4u1/in12;
out6: rRLANDTACQc3s4u1/in11;
out7: rRLANDTACQc3s4u1/in10;
out8: rRLANDTACQc3s4u1/in9;
}

JOINER_8(rNP10c24)
{
in1: rNP10c16s2/signal;
in2: rNP10c16s1/signal;
in3: rNP10c15s2/signal;
in4: rNP10c15s1/signal;
in5: rNP10c14s2/signal;
in6: rNP10c14s1/signal;
in7: rNP10c13s2/signal;
in8: rNP10c13s1/signal;

out1: rRLANDTACQc3s5u1/in8;
out2: rRLANDTACQc3s5u1/in7;
out3: rRLANDTACQc3s5u1/in6;
out4: rRLANDTACQc3s5u1/in5;
out5: rRLANDTACQc3s5u1/in4;
out6: rRLANDTACQc3s5u1/in3;
out7: rRLANDTACQc3s5u1/in2;
out8: rRLANDTACQc3s5u1/in1;
}

JOINER_8(rNP10c25)
{
in1: rNP10c20s2/signal;
in2: rNP10c20s1/signal;
in3: rNP10c19s2/signal;
in4: rNP10c19s1/signal;
in5: rNP10c18s2/signal;
in6: rNP10c18s1/signal;
in7: rNP10c17s2/signal;
in8: rNP10c17s1/signal;

out1: rRLANDTACQc3s5u1/in16;
out2: rRLANDTACQc3s5u1/in15;
out3: rRLANDTACQc3s5u1/in14;
out4: rRLANDTACQc3s5u1/in13;
out5: rRLANDTACQc3s5u1/in12;
out6: rRLANDTACQc3s5u1/in11;
out7: rRLANDTACQc3s5u1/in10;
out8: rRLANDTACQc3s5u1/in9;
}


///////////////////// FILE AUTO-GENERATED WITH gen_planes.pl
DETECTOR_LAND(rNP1c01s1) { LABEL("N01_01_1"); signal: rNP1c21/in8;}
DETECTOR_LAND(rNP1c01s2) { LABEL("N01_01_2"); signal: rNP1c21/in7;}
DETECTOR_LAND(rNP1c02s1) { LABEL("N01_02_1"); signal: rNP1c21/in6;}
DETECTOR_LAND(rNP1c02s2) { LABEL("N01_02_2"); signal: rNP1c21/in5;}
DETECTOR_LAND(rNP1c03s1) { LABEL("N01_03_1"); signal: rNP1c21/in4;}
DETECTOR_LAND(rNP1c03s2) { LABEL("N01_03_2"); signal: rNP1c21/in3;}
DETECTOR_LAND(rNP1c04s1) { LABEL("N01_04_1"); signal: rNP1c21/in2;}
DETECTOR_LAND(rNP1c04s2) { LABEL("N01_04_2"); signal: rNP1c21/in1;}
DETECTOR_LAND(rNP1c05s1) { LABEL("N01_05_1"); signal: rNP1c22/in8;}
DETECTOR_LAND(rNP1c05s2) { LABEL("N01_05_2"); signal: rNP1c22/in7;}
DETECTOR_LAND(rNP1c06s1) { LABEL("N01_06_1"); signal: rNP1c22/in6;}
DETECTOR_LAND(rNP1c06s2) { LABEL("N01_06_2"); signal: rNP1c22/in5;}
DETECTOR_LAND(rNP1c07s1) { LABEL("N01_07_1"); signal: rNP1c22/in4;}
DETECTOR_LAND(rNP1c07s2) { LABEL("N01_07_2"); signal: rNP1c22/in3;}
DETECTOR_LAND(rNP1c08s1) { LABEL("N01_08_1"); signal: rNP1c22/in2;}
DETECTOR_LAND(rNP1c08s2) { LABEL("N01_08_2"); signal: rNP1c22/in1;}
DETECTOR_LAND(rNP1c09s1) { LABEL("N01_09_1"); signal: rNP1c23/in8;}
DETECTOR_LAND(rNP1c09s2) { LABEL("N01_09_2"); signal: rNP1c23/in7;}
DETECTOR_LAND(rNP1c10s1) { LABEL("N01_10_1"); signal: rNP1c23/in6;}
DETECTOR_LAND(rNP1c10s2) { LABEL("N01_10_2"); signal: rNP1c23/in5;}
DETECTOR_LAND(rNP1c11s1) { LABEL("N01_11_1"); signal: rNP1c23/in4;}
DETECTOR_LAND(rNP1c11s2) { LABEL("N01_11_2"); signal: rNP1c23/in3;}
DETECTOR_LAND(rNP1c12s1) { LABEL("N01_12_1"); signal: rNP1c23/in2;}
DETECTOR_LAND(rNP1c12s2) { LABEL("N01_12_2"); signal: rNP1c23/in1;}
DETECTOR_LAND(rNP1c13s1) { LABEL("N01_13_1"); signal: rNP1c24/in8;}
DETECTOR_LAND(rNP1c13s2) { LABEL("N01_13_2"); signal: rNP1c24/in7;}
DETECTOR_LAND(rNP1c14s1) { LABEL("N01_14_1"); signal: rNP1c24/in6;}
DETECTOR_LAND(rNP1c14s2) { LABEL("N01_14_2"); signal: rNP1c24/in5;}
DETECTOR_LAND(rNP1c15s1) { LABEL("N01_15_1"); signal: rNP1c24/in4;}
DETECTOR_LAND(rNP1c15s2) { LABEL("N01_15_2"); signal: rNP1c24/in3;}
DETECTOR_LAND(rNP1c16s1) { LABEL("N01_16_1"); signal: rNP1c24/in2;}
DETECTOR_LAND(rNP1c16s2) { LABEL("N01_16_2"); signal: rNP1c24/in1;}
DETECTOR_LAND(rNP1c17s1) { LABEL("N01_17_1"); signal: rNP1c25/in8;}
DETECTOR_LAND(rNP1c17s2) { LABEL("N01_17_2"); signal: rNP1c25/in7;}
DETECTOR_LAND(rNP1c18s1) { LABEL("N01_18_1"); signal: rNP1c25/in6;}
DETECTOR_LAND(rNP1c18s2) { LABEL("N01_18_2"); signal: rNP1c25/in5;}
DETECTOR_LAND(rNP1c19s1) { LABEL("N01_19_1"); signal: rNP1c25/in4;}
DETECTOR_LAND(rNP1c19s2) { LABEL("N01_19_2"); signal: rNP1c25/in3;}
DETECTOR_LAND(rNP1c20s1) { LABEL("N01_20_1"); signal: rNP1c25/in2;}
DETECTOR_LAND(rNP1c20s2) { LABEL("N01_20_2"); signal: rNP1c25/in1;}


////// JOINER8 PART --- auto-generated 

JOINER_8(rNP1c21)
{
in1: rNP1c04s2/signal;
in2: rNP1c04s1/signal;
in3: rNP1c03s2/signal;
in4: rNP1c03s1/signal;
in5: rNP1c02s2/signal;
in6: rNP1c02s1/signal;
in7: rNP1c01s2/signal;
in8: rNP1c01s1/signal;

out1: rRLANDTACQc1s1u1/in8;
out2: rRLANDTACQc1s1u1/in7;
out3: rRLANDTACQc1s1u1/in6;
out4: rRLANDTACQc1s1u1/in5;
out5: rRLANDTACQc1s1u1/in4;
out6: rRLANDTACQc1s1u1/in3;
out7: rRLANDTACQc1s1u1/in2;
out8: rRLANDTACQc1s1u1/in1;
}

JOINER_8(rNP1c22)
{
in1: rNP1c08s2/signal;
in2: rNP1c08s1/signal;
in3: rNP1c07s2/signal;
in4: rNP1c07s1/signal;
in5: rNP1c06s2/signal;
in6: rNP1c06s1/signal;
in7: rNP1c05s2/signal;
in8: rNP1c05s1/signal;

out1: rRLANDTACQc1s1u1/in16;
out2: rRLANDTACQc1s1u1/in15;
out3: rRLANDTACQc1s1u1/in14;
out4: rRLANDTACQc1s1u1/in13;
out5: rRLANDTACQc1s1u1/in12;
out6: rRLANDTACQc1s1u1/in11;
out7: rRLANDTACQc1s1u1/in10;
out8: rRLANDTACQc1s1u1/in9;
}

JOINER_8(rNP1c23)
{
in1: rNP1c12s2/signal;
in2: rNP1c12s1/signal;
in3: rNP1c11s2/signal;
in4: rNP1c11s1/signal;
in5: rNP1c10s2/signal;
in6: rNP1c10s1/signal;
in7: rNP1c09s2/signal;
in8: rNP1c09s1/signal;

out1: rRLANDTACQc1s2u1/in8;
out2: rRLANDTACQc1s2u1/in7;
out3: rRLANDTACQc1s2u1/in6;
out4: rRLANDTACQc1s2u1/in5;
out5: rRLANDTACQc1s2u1/in4;
out6: rRLANDTACQc1s2u1/in3;
out7: rRLANDTACQc1s2u1/in2;
out8: rRLANDTACQc1s2u1/in1;
}

JOINER_8(rNP1c24)
{
in1: rNP1c16s2/signal;
in2: rNP1c16s1/signal;
in3: rNP1c15s2/signal;
in4: rNP1c15s1/signal;
in5: rNP1c14s2/signal;
in6: rNP1c14s1/signal;
in7: rNP1c13s2/signal;
in8: rNP1c13s1/signal;

out1: rRLANDTACQc1s2u1/in16;
out2: rRLANDTACQc1s2u1/in15;
out3: rRLANDTACQc1s2u1/in14;
out4: rRLANDTACQc1s2u1/in13;
out5: rRLANDTACQc1s2u1/in12;
out6: rRLANDTACQc1s2u1/in11;
out7: rRLANDTACQc1s2u1/in10;
out8: rRLANDTACQc1s2u1/in9;
}

JOINER_8(rNP1c25)
{
in1: rNP1c20s2/signal;
in2: rNP1c20s1/signal;
in3: rNP1c19s2/signal;
in4: rNP1c19s1/signal;
in5: rNP1c18s2/signal;
in6: rNP1c18s1/signal;
in7: rNP1c17s2/signal;
in8: rNP1c17s1/signal;

out1: rRLANDTACQc1s3u1/in8;
out2: rRLANDTACQc1s3u1/in7;
out3: rRLANDTACQc1s3u1/in6;
out4: rRLANDTACQc1s3u1/in5;
out5: rRLANDTACQc1s3u1/in4;
out6: rRLANDTACQc1s3u1/in3;
out7: rRLANDTACQc1s3u1/in2;
out8: rRLANDTACQc1s3u1/in1;
}



# This file is no longer used!  See land/plane_X.hh
#
# LAND planes are now treated like normal sources.

/* Connectors
 *
 *   Odd
 *
 *     1    2    3    4    5    6    7    8  (reversed, 1 is to the right)
 *
 * 6 19_1  5_2 20_1  6_2
 * 5 15_1  1_2 16_1  2_2
 * 4  9_2 17_2 10_2 18_2 11_2 19_2 12_2 20_2
 * 3  3_2 13_2  4_2 14_2  7_2 15_2  8_2 16_2
 * 2  5_1 13_1  6_1 14_1  7_1 17_1  8_1 18_1
 * 1  1_1  9_1  2_1 10_1  3_1 11_1  4_1 12_1
 * 
 * HV 3 (full)
 * HV 2 (half)
 * HV 1 (full)
 *
 *   Even
 *
 *     1    2    3    4    5    6    7    8  (reversed, 1 is to the right)
 *
 * 6                     19_1  5_2 20_1  6_2
 * 5                     15_1  1_2 16_1  2_2
 * 4  As odd...
 * 3  As odd...
 * 2  As odd...
 * 1  As odd...
 * 
 */

/* Please note:  at least in S245 database, N10_12_1 and N10_18_1 were interchanged at
 * constant fraction.  However, not at TDC or energy.  Which basically is impossible...  
 * Check if old DB ok (by correlations in cosmic data).  (Data most likely is ok...)
 */

/* Note:  This file is used as a template.
 *
 * It is not used directly, only the files gen/gen_plane_x.hh are used.
 */

// Plane 1

DETECTOR_LAND(rNP1c01s1) { LABEL("N01_01_1"); signal: rNP1c21/in1; hv: rNP1c31/out1;   }
DETECTOR_LAND(rNP1c02s1) { LABEL("N01_02_1"); signal: rNP1c21/in3; hv: rNP1c31/out2;   }
DETECTOR_LAND(rNP1c03s1) { LABEL("N01_03_1"); signal: rNP1c21/in5; hv: rNP1c31/out3;   }
DETECTOR_LAND(rNP1c04s1) { LABEL("N01_04_1"); signal: rNP1c21/in7; hv: rNP1c31/out4;   }
DETECTOR_LAND(rNP1c05s1) { LABEL("N01_05_1"); signal: rNP1c22/in1; hv: rNP1c31/out5;   }
DETECTOR_LAND(rNP1c06s1) { LABEL("N01_06_1"); signal: rNP1c22/in3; hv: rNP1c31/out6;   }
DETECTOR_LAND(rNP1c07s1) { LABEL("N01_07_1"); signal: rNP1c22/in5; hv: rNP1c31/out7;   }
DETECTOR_LAND(rNP1c08s1) { LABEL("N01_08_1"); signal: rNP1c22/in7; hv: rNP1c31/out8;   }
DETECTOR_LAND(rNP1c09s1) { LABEL("N01_09_1"); signal: rNP1c21/in2; hv: rNP1c31/out9;   }
DETECTOR_LAND(rNP1c10s1) { LABEL("N01_10_1"); signal: rNP1c21/in4; hv: rNP1c31/out10;  }
DETECTOR_LAND(rNP1c11s1) { LABEL("N01_11_1"); signal: rNP1c21/in6; hv: rNP1c31/out11;  }
DETECTOR_LAND(rNP1c12s1) { LABEL("N01_12_1"); signal: rNP1c21/in8; hv: rNP1c31/out12;  }
DETECTOR_LAND(rNP1c13s1) { LABEL("N01_13_1"); signal: rNP1c22/in2; hv: rNP1c31/out13;  }
DETECTOR_LAND(rNP1c14s1) { LABEL("N01_14_1"); signal: rNP1c22/in4; hv: rNP1c31/out14;  }
DETECTOR_LAND(rNP1c15s1) { LABEL("N01_15_1"); signal: rNP1c25/in8; hv: rNP1c31/out15;  } /*SHARE 4*/
DETECTOR_LAND(rNP1c16s1) { LABEL("N01_16_1"); signal: rNP1c25/in6; hv: rNP1c31/out16;  } /*SHARE 2*/
DETECTOR_LAND(rNP1c17s1) { LABEL("N01_17_1"); signal: rNP1c22/in6; hv: rNP1c32/out1;   }
DETECTOR_LAND(rNP1c18s1) { LABEL("N01_18_1"); signal: rNP1c22/in8; hv: rNP1c32/out2;   }
DETECTOR_LAND(rNP1c19s1) { LABEL("N01_19_1"); signal: rNP1c26/in8; hv: rNP1c32/out3;   } /*SHARE 4*/
DETECTOR_LAND(rNP1c20s1) { LABEL("N01_20_1"); signal: rNP1c26/in6; hv: rNP1c32/out4;   } /*SHARE 2*/

DETECTOR_LAND(rNP1c01s2) { LABEL("N01_01_2"); signal: rNP1c25/in7; hv: rNP1c33/out1;   } /*SHARE 3*/
DETECTOR_LAND(rNP1c02s2) { LABEL("N01_02_2"); signal: rNP1c25/in5; hv: rNP1c33/out2;   } /*SHARE 1*/
DETECTOR_LAND(rNP1c03s2) { LABEL("N01_03_2"); signal: rNP1c23/in1; hv: rNP1c33/out3;   }
DETECTOR_LAND(rNP1c04s2) { LABEL("N01_04_2"); signal: rNP1c23/in3; hv: rNP1c33/out4;   }
DETECTOR_LAND(rNP1c05s2) { LABEL("N01_05_2"); signal: rNP1c26/in7; hv: rNP1c33/out5;   } /*SHARE 3*/
DETECTOR_LAND(rNP1c06s2) { LABEL("N01_06_2"); signal: rNP1c26/in5; hv: rNP1c33/out6;   } /*SHARE 1*/
DETECTOR_LAND(rNP1c07s2) { LABEL("N01_07_2"); signal: rNP1c23/in5; hv: rNP1c33/out7;   }
DETECTOR_LAND(rNP1c08s2) { LABEL("N01_08_2"); signal: rNP1c23/in7; hv: rNP1c33/out8;   }
DETECTOR_LAND(rNP1c09s2) { LABEL("N01_09_2"); signal: rNP1c24/in1; hv: rNP1c33/out9;   }
DETECTOR_LAND(rNP1c10s2) { LABEL("N01_10_2"); signal: rNP1c24/in3; hv: rNP1c33/out10;  }
DETECTOR_LAND(rNP1c11s2) { LABEL("N01_11_2"); signal: rNP1c24/in5; hv: rNP1c33/out11;  }
DETECTOR_LAND(rNP1c12s2) { LABEL("N01_12_2"); signal: rNP1c24/in7; hv: rNP1c33/out12;  }
DETECTOR_LAND(rNP1c13s2) { LABEL("N01_13_2"); signal: rNP1c23/in2; hv: rNP1c33/out13;  }
DETECTOR_LAND(rNP1c14s2) { LABEL("N01_14_2"); signal: rNP1c23/in4; hv: rNP1c33/out14;  }
DETECTOR_LAND(rNP1c15s2) { LABEL("N01_15_2"); signal: rNP1c23/in6; hv: rNP1c33/out15;  }
DETECTOR_LAND(rNP1c16s2) { LABEL("N01_16_2"); signal: rNP1c23/in8; hv: rNP1c33/out16;  }
DETECTOR_LAND(rNP1c17s2) { LABEL("N01_17_2"); signal: rNP1c24/in2; hv: rNP1c32/out5;   }
DETECTOR_LAND(rNP1c18s2) { LABEL("N01_18_2"); signal: rNP1c24/in4; hv: rNP1c32/out6;   }
DETECTOR_LAND(rNP1c19s2) { LABEL("N01_19_2"); signal: rNP1c24/in6; hv: rNP1c32/out7;   }
DETECTOR_LAND(rNP1c20s2) { LABEL("N01_20_2"); signal: rNP1c24/in8; hv: rNP1c32/out8;   }

//DETECTOR_LAND(rNP1c01s1) { LABEL("N01_01_1"); signal: rNP1c21/in1; hv: rNP1c31/out1;   }
//DETECTOR_LAND(rNP1c02s1) { LABEL("N01_02_1"); signal: rNP1c21/in3; hv: rNP1c31/out2;   }
//DETECTOR_LAND(rNP1c03s1) { LABEL("N01_03_1"); signal: rNP1c21/in5; hv: rNP1c31/out3;   }
//DETECTOR_LAND(rNP1c04s1) { LABEL("N01_04_1"); signal: rNP1c21/in7; hv: rNP1c31/out4;   }
//DETECTOR_LAND(rNP1c05s1) { LABEL("N01_05_1"); signal: rNP1c22/in1; hv: rNP1c31/out5;   }
//DETECTOR_LAND(rNP1c06s1) { LABEL("N01_06_1"); signal: rNP1c22/in3; hv: rNP1c31/out6;   }
//DETECTOR_LAND(rNP1c07s1) { LABEL("N01_07_1"); signal: rNP1c22/in5; hv: rNP1c31/out7;   }
//DETECTOR_LAND(rNP1c08s1) { LABEL("N01_08_1"); signal: rNP1c22/in7; hv: rNP1c31/out8;   }
//DETECTOR_LAND(rNP1c09s1) { LABEL("N01_09_1"); signal: rNP1c21/in2; hv: rNP1c31/out9;   }
//DETECTOR_LAND(rNP1c10s1) { LABEL("N01_10_1"); signal: rNP1c21/in4; hv: rNP1c31/out10;  }
//DETECTOR_LAND(rNP1c11s1) { LABEL("N01_11_1"); signal: rNP1c21/in6; hv: rNP1c31/out11;  }
//DETECTOR_LAND(rNP1c12s1) { LABEL("N01_12_1"); signal: rNP1c21/in8; hv: rNP1c31/out12;  }
//DETECTOR_LAND(rNP1c13s1) { LABEL("N01_13_1"); signal: rNP1c22/in2; hv: rNP1c31/out13;  }
//DETECTOR_LAND(rNP1c14s1) { LABEL("N01_14_1"); signal: rNP1c22/in4; hv: rNP1c31/out14;  }
//DETECTOR_LAND(rNP1c15s1) { LABEL("N01_15_1"); signal: rNP1c25/in5; hv: rNP1c31/out15;  } /*SHARE 1*/
//DETECTOR_LAND(rNP1c16s1) { LABEL("N01_16_1"); signal: rNP1c25/in7; hv: rNP1c31/out16;  } /*SHARE 3*/
//DETECTOR_LAND(rNP1c17s1) { LABEL("N01_17_1"); signal: rNP1c22/in6; hv: rNP1c32/out1;   }
//DETECTOR_LAND(rNP1c18s1) { LABEL("N01_18_1"); signal: rNP1c22/in8; hv: rNP1c32/out2;   }
//DETECTOR_LAND(rNP1c19s1) { LABEL("N01_19_1"); signal: rNP1c26/in5; hv: rNP1c32/out3;   } /*SHARE 1*/
//DETECTOR_LAND(rNP1c20s1) { LABEL("N01_20_1"); signal: rNP1c26/in7; hv: rNP1c32/out4;   } /*SHARE 3*/

//DETECTOR_LAND(rNP1c01s2) { LABEL("N01_01_2"); signal: rNP1c25/in6; hv: rNP1c33/out1;   } /*SHARE 2*/
//DETECTOR_LAND(rNP1c02s2) { LABEL("N01_02_2"); signal: rNP1c25/in8; hv: rNP1c33/out2;   } /*SHARE 4*/
//DETECTOR_LAND(rNP1c03s2) { LABEL("N01_03_2"); signal: rNP1c23/in1; hv: rNP1c33/out3;   }
//DETECTOR_LAND(rNP1c04s2) { LABEL("N01_04_2"); signal: rNP1c23/in3; hv: rNP1c33/out4;   }
//DETECTOR_LAND(rNP1c05s2) { LABEL("N01_05_2"); signal: rNP1c26/in6; hv: rNP1c33/out5;   } /*SHARE 2*/
//DETECTOR_LAND(rNP1c06s2) { LABEL("N01_06_2"); signal: rNP1c26/in8; hv: rNP1c33/out6;   } /*SHARE 4*/
//DETECTOR_LAND(rNP1c07s2) { LABEL("N01_07_2"); signal: rNP1c23/in5; hv: rNP1c33/out7;   }
//DETECTOR_LAND(rNP1c08s2) { LABEL("N01_08_2"); signal: rNP1c23/in7; hv: rNP1c33/out8;   }
//DETECTOR_LAND(rNP1c09s2) { LABEL("N01_09_2"); signal: rNP1c24/in1; hv: rNP1c33/out9;   }
//DETECTOR_LAND(rNP1c10s2) { LABEL("N01_10_2"); signal: rNP1c24/in3; hv: rNP1c33/out10;  }
//DETECTOR_LAND(rNP1c11s2) { LABEL("N01_11_2"); signal: rNP1c24/in5; hv: rNP1c33/out11;  }
//DETECTOR_LAND(rNP1c12s2) { LABEL("N01_12_2"); signal: rNP1c24/in7; hv: rNP1c33/out12;  }
//DETECTOR_LAND(rNP1c13s2) { LABEL("N01_13_2"); signal: rNP1c23/in2; hv: rNP1c33/out13;  }
//DETECTOR_LAND(rNP1c14s2) { LABEL("N01_14_2"); signal: rNP1c23/in4; hv: rNP1c33/out14;  }
//DETECTOR_LAND(rNP1c15s2) { LABEL("N01_15_2"); signal: rNP1c23/in6; hv: rNP1c33/out15;  }
//DETECTOR_LAND(rNP1c16s2) { LABEL("N01_16_2"); signal: rNP1c23/in8; hv: rNP1c33/out16;  }
//DETECTOR_LAND(rNP1c17s2) { LABEL("N01_17_2"); signal: rNP1c24/in2; hv: rNP1c32/out5;   }
//DETECTOR_LAND(rNP1c18s2) { LABEL("N01_18_2"); signal: rNP1c24/in4; hv: rNP1c32/out6;   }
//DETECTOR_LAND(rNP1c19s2) { LABEL("N01_19_2"); signal: rNP1c24/in6; hv: rNP1c32/out7;   }
//DETECTOR_LAND(rNP1c20s2) { LABEL("N01_20_2"); signal: rNP1c24/in8; hv: rNP1c32/out8;   }


JOINER_8(rNP1c21)
{
 in1: rNP1c01s1/signal;
 in2: rNP1c09s1/signal;
 in3: rNP1c02s1/signal;
 in4: rNP1c10s1/signal;
 in5: rNP1c03s1/signal;
 in6: rNP1c11s1/signal;
 in7: rNP1c04s1/signal;
 in8: rNP1c12s1/signal;

 out1_8: r13c1s1/in1_8;
}

JOINER_8(rNP1c22)
{
 in1: rNP1c05s1/signal;
 in2: rNP1c13s1/signal;
 in3: rNP1c06s1/signal;
 in4: rNP1c14s1/signal;
 in5: rNP1c07s1/signal;
 in6: rNP1c17s1/signal;
 in7: rNP1c08s1/signal;
 in8: rNP1c18s1/signal;

 out1_8: r13c1s1/in9_16;
}

JOINER_8(rNP1c23)
{
 in1: rNP1c03s2/signal;
 in2: rNP1c13s2/signal;
 in3: rNP1c04s2/signal;
 in4: rNP1c14s2/signal;
 in5: rNP1c07s2/signal;
 in6: rNP1c15s2/signal;
 in7: rNP1c08s2/signal;
 in8: rNP1c16s2/signal;

 out1_8: r13c1s2/in1_8;
}

JOINER_8(rNP1c24)
{
 in1: rNP1c09s2/signal;
 in2: rNP1c17s2/signal;
 in3: rNP1c10s2/signal;
 in4: rNP1c18s2/signal;
 in5: rNP1c11s2/signal;
 in6: rNP1c19s2/signal;
 in7: rNP1c12s2/signal;
 in8: rNP1c20s2/signal;

 out1_8: r13c1s2/in9_16;
}

JOINER_8(rNP1c25)
{
 in8: rNP1c15s1/signal; /*SHARE 4*/
 in7: rNP1c01s2/signal; /*SHARE 3*/
 in6: rNP1c16s1/signal; /*SHARE 2*/
 in5: rNP1c02s2/signal; /*SHARE 1*/

 out5_8: r13c1s3/in5_8;
}

JOINER_8(rNP1c26)
{
 in8: rNP1c19s1/signal; /*SHARE 4*/
 in7: rNP1c05s2/signal; /*SHARE 3*/
 in6: rNP1c20s1/signal; /*SHARE 2*/
 in5: rNP1c06s2/signal; /*SHARE 1*/

 out5_8: r13c1s3/in13_16;
}

//JOINER_8(rNP1c25)
//{
// in5: rNP1c15s1/signal; /*SHARE 1*/
// in6: rNP1c01s2/signal; /*SHARE 2*/
// in7: rNP1c16s1/signal; /*SHARE 3*/
// in8: rNP1c02s2/signal; /*SHARE 4*/
//
// out5_8: r11c1s3/in5_8;
//}
//
//JOINER_8(rNP1c26)
//{
// in5: rNP1c19s1/signal; /*SHARE 1*/
// in6: rNP1c05s2/signal; /*SHARE 2*/
// in7: rNP1c20s1/signal; /*SHARE 3*/
// in8: rNP1c06s2/signal; /*SHARE 4*/
//
// out5_8: r11c1s3/in13_16;
//}

HV_CONN_16(rNP1c31)
{
 in1_16: r16c1s0/out0_15;

 out1:  rNP1c01s1/hv;
 out2:  rNP1c02s1/hv;
 out3:  rNP1c03s1/hv;
 out4:  rNP1c04s1/hv;
 out5:  rNP1c05s1/hv;
 out6:  rNP1c06s1/hv;
 out7:  rNP1c07s1/hv;
 out8:  rNP1c08s1/hv;
 out9:  rNP1c09s1/hv;
 out10: rNP1c10s1/hv;
 out11: rNP1c11s1/hv;
 out12: rNP1c12s1/hv;
 out13: rNP1c13s1/hv;
 out14: rNP1c14s1/hv;
 out15: rNP1c15s1/hv;
 out16: rNP1c16s1/hv;
}

HV_CONN_16(rNP1c32)
{
 in1_8: r16c1s2/out0_7;

 out1:  rNP1c17s1/hv;
 out2:  rNP1c18s1/hv;
 out3:  rNP1c19s1/hv;
 out4:  rNP1c20s1/hv;
 out5:  rNP1c17s2/hv;
 out6:  rNP1c18s2/hv;
 out7:  rNP1c19s2/hv;
 out8:  rNP1c20s2/hv;
}

HV_CONN_16(rNP1c33)
{
 in1_16: r16c1s1/out0_15;

 out1:  rNP1c01s2/hv;
 out2:  rNP1c02s2/hv;
 out3:  rNP1c03s2/hv;
 out4:  rNP1c04s2/hv;
 out5:  rNP1c05s2/hv;
 out6:  rNP1c06s2/hv;
 out7:  rNP1c07s2/hv;
 out8:  rNP1c08s2/hv;
 out9:  rNP1c09s2/hv;
 out10: rNP1c10s2/hv;
 out11: rNP1c11s2/hv;
 out12: rNP1c12s2/hv;
 out13: rNP1c13s2/hv;
 out14: rNP1c14s2/hv;
 out15: rNP1c15s2/hv;
 out16: rNP1c16s2/hv;
}














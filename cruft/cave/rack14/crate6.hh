/* CAMAC crate hosting NTF CFDs, Delays, MUX
 * still missing POS, ROLU, FRS, 
 * r14 CRATE 6
 *
 * CAMAC CRATE with CFD, Delay
 *
 */
 
 CAMAC_CRATE(r14c6)
 {
 ;
 }
 

//FW: this is the POS/ROLU one
//CAREFUL: only CF8101 have tb and ta !!!
CF8101(r14c6s1)
{
in1_8:   r13c6/t1_8;
//sum:     r52c2s3/in1;
//mux_tb:  r52c5s5/in1a; 
//mux_e:   r52c5s5/in2a;
//mux_mon: r52c5s5/in3a;
//test:    r52c5s8/out16;
ta1_8:   r14c2s5u1/in0_7;  //to scalers
tb1_8:   r14c6s17/in1_8;  //to delay, to tdc
}

//FW: this is the S8 one
CF8101(r14c6s2)
{
//in1_2:   r14c2s17/t1_2;
in1: r14c2s17/t1;
in5: r14c2s17/t2;
//sum:     r52c2s3/in1;
//mux_tb:  r52c5s5/in1a; 
//mux_e:   r52c5s5/in2a;
//mux_mon: r52c5s5/in3a;
//test:    r52c5s8/out16;
ta1_8:   r14c2s5u2/in0_7;  //to scalers
//tb1_2:   r14c6s17/in9_10;  //to delay, to tdc
tb1:   r14c6s17/in9;
tb5:   r14c6s17/in13;}

CF8103(r14c6s4)
{
in1_8:   r11c5s3/t1_8;
//sum:     r52c2s3/in1;
//mux_tb:  r52c5s5/in1a; 
//mux_e:   r52c5s5/in2a;
//mux_mon: r52c5s5/in3a;
//test:    r52c5s8/out16;
tb1_8:   r14c6s18/in1_8;
}

CF8103(r14c6s6)
{
in1_8:   r11c5s4/t1_8;
//sum:     r52c2s3/in1;
//mux_tb:  r52c5s5/in1a; 
//mux_e:   r52c5s5/in2a;
//mux_mon: r52c5s5/in3a;
//test:    r52c5s8/out16;
tb1_8:   r14c6s18/in9_16;
}
CF8103(r14c6s8)
{
in1_8:   r11c5s5/t1_8;
//sum:     r52c2s3/in1;
//mux_tb:  r52c5s5/in1a; 
//mux_e:   r52c5s5/in2a;
//mux_mon: r52c5s5/in3a;
//test:    r52c5s8/out16;
tb1_8:   r14c6s19/in1_8;
}

CF8103(r14c6s10)
{
in1_8:   r11c5s6/t1_8;
//sum:     r52c2s3/in1;
//mux_tb:  r52c5s5/in1a; 
//mux_e:   r52c5s5/in2a;
//mux_mon: r52c5s5/in3a;
//test:    r52c5s8/out16;
tb1_8:   r14c6s19/in9_16;
}

DL1600(r14c6s17)
{
  //HAS_SETTING("DELAY" => "500ns");
in1_8:   .s1/tb1_8; //POS and ROLU
in9:   .s2/tb1; //S8
in13:  .s2/tb5;
//in9_16:  .s6/tb1_8;
//outa1_16:r15c4s10/in0_15; 
//outb1_16:r15c4s11/in0_15;
//this one only has one out...
outa1_8:  r15c3s5/in0_7; 
outa9:  r15c3s5/in8;
outa13: r15c3s5/in12;
}

DL1600(r14c6s18)
{
in1_8:   .s4/tb1_8;
in9_16:  .s6/tb1_8;
outa1_16:r15c4s10/in0_15; 
outb1_16:r15c4s11/in0_15;
}

DL1600(r14c6s19)
{
in1_8:   .s8/tb1_8;
in9_16:  .s10/tb1_8;
outa1_16:r15c4s10/in16_31; 
outb1_16:r15c4s11/in16_31;
}



/*
RMX3(r52c5s5)
{
in1a: r52c5s9/mux_tb; 
in1b: r52c5s13/mux_tb;
in1c: r52c5s15/mux_tb;
in1d: r52c5s17/mux_tb;
in2a: r52c5s9/mux_e; 
in2b: r52c5s13/mux_e;
in2c: r52c5s15/mux_e;
in2d: r52c5s17/mux_e;
in3a: r52c5s9/mux_mon;
in3b: r52c5s13/mux_mon;
in3c: r52c5s15/mux_mon;
in3d: r52c5s17/mux_mon;

out1: r52c3s21/in4d;
out2: r52c3s21/in5d;
out3: r52c3s21/in6d;
}


LIFO(r52c5s8)
{
in: r52c2s1u4/out1;
out13: r52c5s15/test;
out14: r52c5s13/test;
out15: r52c5s17/test;
out16: r52c5s9/test;
}

CF8103(r52c5s9)
{
in1_8:   r52c0s12/t1_8;
sum:     r52c2s3/in1;
mux_tb:  r52c5s5/in1a; 
mux_e:   r52c5s5/in2a;
mux_mon: r52c5s5/in3a;
test:    r52c5s8/out16;
tb1_8:   r52c5s19/in1_8;
}


CF8103(r52c5s13)
{
in1_8:   r52c0s23/t1_8;
sum:     r52c2s3/in3;
mux_tb:  r52c5s5/in1b; 
mux_e:   r52c5s5/in2b;
mux_mon: r52c5s5/in3b;
test:    r52c5s8/out14;
tb1_8:   r52c5s21/in1_8;
}

CF8103(r52c5s15)
{
in1_8:   r52c0s23/t9_16;
sum:     r52c2s3/in4;
mux_tb:  r52c5s5/in1c; 
mux_e:   r52c5s5/in2c;
mux_mon: r52c5s5/in3c;
test:    r52c5s8/out13;
tb1_8:   r52c5s21/in9_16;
}


CF8103(r52c5s17)
{
in1_8:   r52c0s12/t9_16;
sum:     r52c2s3/in6;
mux_tb:  r52c5s5/in1d; 
mux_e:   r52c5s5/in2d;
mux_mon: r52c5s5/in3d;
test:    r52c5s8/out15;
tb1_8:   r52c5s19/in9_16;
}

DL1600(r52c5s19)
{
in1_8:   r52c5s9/tb1_8;
in9_16:  r52c5s17/tb1_8;
outa1_16:r52c1s10/in0_15; 
outb1_16:r52c1s15/in0_15;
}

DL1600(r52c5s21)
{
in1_8:    r52c5s13/tb1_8;
in9_16:   r52c5s15/tb1_8;
outa1_16: r52c1s10/in16_31;
outb1_16: r52c1s15/in16_31;
}

*/

CBV1000(r14c6s24)
{
SETTING("CRATE_NO" => "2");
vsb_in: r12c5s24/vsb_out; // oder umgekehrt?
}

DMS1000(r14c6s25)
{
;
}


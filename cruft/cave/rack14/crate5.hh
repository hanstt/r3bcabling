//FW: this is the 32x50ns cable delay 'crate'

//FW: copied from the CVS version, messhuette, rack3, crate2, cable_delay_crates1_2_9.hh. Modified to r14c5.
//Probably all connections completely new, so need to be careful:

//CABLE_DELAY_32(r3c2)
CABLE_DELAY_32(r14c5)
{
  /* OLD code...
  SETTING("DELAY"=>"50ns");

 in1:   .c2/out2;        out1:  .c7s7/in; 
 in2:   .c2/out10;       out2:   .c2/in1;
 in3:   r5c6/out5;       out3:   r4c7s23/in1;
 in4:   r3c5s1/out7;     out4:   r4c3s9/in6;
 in5:   r3c5s1/out8;     out5:   r4c3s9/in7;
 in6:   r3c1s5/out;      out6:   .c2/in31;
 in7:   r3c5s15u1/out9;	 out7:   r3c1s3/in;

 // in8:   r2c2s21u1/overlap;     out8:   r2c2s19u1/in1;
 in9:   r2c3s23u1/out2;  out9:   r2c5s1u1/trig; 
 in10:  r3c2/out12;      out10:  .c2/in2;

 in12:  r3c5s15u1/out1;  out12:  .c2/in10;
 in14:	r4c8s13u3/out3;  out14:	 .c2/in15;
 in15:	.c2/out14;	 out15:	 .c2/in16;
 in16:	.c2/out15;	 out16:	 r3c1s1/in;

 in17:  ;                out17:  ;	// BROCKEN

 in18:  r2c3s21u7/outB1; out18:  r2c4s11/in11;
 in19:  r2c3s21u8/outA;  out19:  r2c4s11/in12;
 in20:  .c2/out22;       out20:  .c2/in21;
 in21:  .c2/out20;       out21:  .c2/in22;
 in22:  .c2/out21;       out22:  .c2/in20;

//  in23:	r2c1s21u4/out2;	 out23:	 .c2/in24;
//  in24:	.c2/out23;	 out24:	 r2c2s21u1/inB;
//  in25:	r2c3s21u2/outA;	 out25:	 r2c2s1/in9;
 in26:  r1c2s4/stop;     out26:  r3c6s17u2/inB;
 in27:  r3c5s15u1/out12; out27:  r3c1s2/in;
 in28:  r3c5s15u1/out6;  out28:  r3c5s23/in6;

 in29:  r3c5s5u2/out7;	 out29:  .c2/in30;
 in30:  .c2/out29;	 out30:  .c5s11u1/common0;

 in31:  .c2/out6;        out31:  r3c5s23/in16;
 ...OLD code */
  
  //NEW code:
  in1_4: r13c6/e1_4;	//from splitter terminal
  in18_21: r13c6/e5_8;  //from splitter terminal

  out1_4: r14c2s22/in1_4; //to custom nim-ecl converter
  out18_21: r14c2s22/in5_8; //to custom nim-ecl converter
}

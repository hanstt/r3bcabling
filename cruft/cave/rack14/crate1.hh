/*
 * Crate 1 in Rack 14
 *
 * CAMAC Crate with processor/controller
 *  for TCAL 
 */

FOUR_POST_RACK(r14)
{
  LABEL("Here & there");
}
 
CAMAC_CRATE(r14c1)
{
;
}



IOL_BOX(r14c1s20)
{
;
}

LECROY2277(r14c1s21)
{
/*  needs to be rewritten for S412
in0:   rMASTERc3s3/out_ecl8;
in1:   rMASTERc3s3/out_ecl7;
in2:   rMASTERc3s3/out_ecl6;
in3:   rMASTERc3s3/out_ecl5;
in4:   rMASTERc3s3/out_ecl4;
in5:   rMASTERc3s3/out_ecl3;
in6:   rMASTERc3s3/out_ecl2;
in7:   rMASTERc3s3/out_ecl1; // vertauscht! 1->8, 2->7 etc. ggf einzeln
*/
;}

PHILLIPS7120(r14c1s22)
{

//start: rMASTERc3s5u3/in1;
//stop: rMASTERc3s6u2/in1;
;} 
 
 CBV1000(r14c1s24)
 {
 SETTING("CRATE_NO" => "2"); //or 3? 
 vsb_in: r15c2s1/vsb_out; // comes from back plane of master rio
 }
 
 DMS1000(r14c1s25)
 {
 ;
 }
 

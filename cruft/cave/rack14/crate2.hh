//A NIM crate, hosting for example a custom-made lemo-to-ecl converter for the pos and rolu energy signals
NIM_CRATE(r14c2)
{
  ;
}

//(r14c2s5) // some sort of nim ecl converter n638 CAEN
//Used for POS ROLU
N638(r14c2s5)
{
  ;
}
N638_U(r14c2s5u1)
{
  in0_7: r14c6s1/ta1_8;
 
  out_ecl0_7: r15c3s10/in0_7; //scaler
}
N638_U(r14c2s5u2)
{
  in0_7: r14c6s2/ta1_8;
 
  out_ecl0_7: r15c3s10/in8_15; //scaler
}
SPLIT_BOX_PASSIVE(r14c2s17) //FW: An e/t splitter for S8 and others
{
  in1: rS8SCIc1s1u1/signal; //S8 left
  in2: rS8SCIc1s1u2/signal; //S8 right

  t1:	r14c6s2/in1;	//CFD
  t2:	r14c6s2/in5;
  e1_2:	r14c2s22/in9_10;//nim-ecl
}

EC1610(r14c2s22) // 16ch. NIM-ECL Converter
//FW: Actually, this is a custom-made device. Kind of like a patch panel from lemo cables to ECL flat-ribbon cables
{
  //POS&ROLU
  in1_4: r14c5/out1_4;
  in5_8: r14c5/out18_21;
  out_ecl1_8: r15c3s4/in0_7;  //v792 qdc
  
  //S8
  in9_10: r14c2s17/e1_2;
  out_ecl9_10: r15c3s4/in8_9;  //v792 qdc
  
  //Pixel	       
  in13_15: r13c1/out7_9;
  out_ecl13_15: r15c3s4/in13_15;
}


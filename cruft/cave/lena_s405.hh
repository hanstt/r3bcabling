#include "lena.hh"

/* Naming scheme for LENA detector:
 *
 * DETECTOR_LENA ('the detector', rLENAc1), c1-c15 for the 15 bars.
 * DETECTOR_LENA_PM ('the PM', rLENAc1s1)s1 for upper PMT, s2 for down
 * 
 * The LENA bars are connected to an amplifier or a splitbox. 
 * The amplified signals go to a splitbox after the amp, the unamplified ones
 * directly to said splitboxes.
 *
 * rLENA is the detector.
 * TODO: SOME LENA Channels are AMPLIFIED. AMPS no longer in setup.
 *
 * r47  is the rack close the entry of cave c including the LENA spiltboxes
 * and also the mastercrates
 * r47c1  is the hungarian NIM crate which includes the 2 PMT amps (is not at cave c anymore)
 * r47c2 is the splitboxcrate on top of r47
 * r47c3 is the CAMAC crate on top of r47
 * r47c4->r47c8 are the crates in r47 from top to bottom,  
 */

DETECTOR_LENA(rLENAc1)
{
  ;
}

DETECTOR_LENA_PM(rLENAc1s1){ LABEL("LENA01_01"); signal: r47c1s8/in1;}
DETECTOR_LENA_PM(rLENAc1s2){ LABEL("LENA01_02"); signal: r47c1s8/in6;}
DETECTOR_LENA_PM(rLENAc2s1){ LABEL("LENA02_01"); signal: r47c1s8/in2;}
DETECTOR_LENA_PM(rLENAc2s2){ LABEL("LENA02_02"); signal: r47c1s8/in7;}
DETECTOR_LENA_PM(rLENAc3s1){ LABEL("LENA03_01"); signal: r47c1s8/in3;}
DETECTOR_LENA_PM(rLENAc3s2){ LABEL("LENA03_02"); signal: r47c1s8/in8;}
DETECTOR_LENA_PM(rLENAc4s1){ LABEL("LENA04_01"); signal: r47c1s8/in4;}
DETECTOR_LENA_PM(rLENAc4s2){ LABEL("LENA04_02"); signal: r47c1s8/in9;}
DETECTOR_LENA_PM(rLENAc5s1){ LABEL("LENA05_01"); signal: r47c1s8/in5;}
DETECTOR_LENA_PM(rLENAc5s2){ LABEL("LENA05_02"); signal: r47c1s8/in10;}

DETECTOR_LENA_PM(rLENAc6s1){ LABEL("LENA06_01"); signal: r47c1s8/in11;}
DETECTOR_LENA_PM(rLENAc6s2){ LABEL("LENA06_02"); signal: r47c1s8/in16;}
DETECTOR_LENA_PM(rLENAc7s1){ LABEL("LENA07_01"); signal: r47c1s8/in12;}
DETECTOR_LENA_PM(rLENAc7s2){ LABEL("LENA07_02"); signal: r47c1s12/in1;}
DETECTOR_LENA_PM(rLENAc8s1){ LABEL("LENA08_01"); signal: r47c1s8/in13;}
DETECTOR_LENA_PM(rLENAc8s2){ LABEL("LENA08_02"); signal: r47c1s12/in2;}
DETECTOR_LENA_PM(rLENAc9s1){ LABEL("LENA09_02"); signal: r47c1s8/in14;}
DETECTOR_LENA_PM(rLENAc9s2){ LABEL("LENA09_02"); signal: r47c1s12/in3;}
DETECTOR_LENA_PM(rLENAc10s1){ LABEL("LENA10_01"); signal: r47c1s8/in15;}
DETECTOR_LENA_PM(rLENAc10s2){ LABEL("LENA10_02"); signal: r47c1s12/in4;}

DETECTOR_LENA_PM(rLENAc11s1){ LABEL("LENA11_01"); signal: r47c1s12/in5;}
DETECTOR_LENA_PM(rLENAc11s2){ LABEL("LENA11_02"); signal: r47c1s14/in2;}
DETECTOR_LENA_PM(rLENAc12s1){ LABEL("LENA12_01"); signal: r47c1s12/in6;}
DETECTOR_LENA_PM(rLENAc12s2){ LABEL("LENA12_02"); signal: r47c1s14/in3;}
DETECTOR_LENA_PM(rLENAc13s1){ LABEL("LENA13_01"); signal: r47c1s12/in7;}
DETECTOR_LENA_PM(rLENAc13s2){ LABEL("LENA13_02"); signal: r47c1s14/in4;}
DETECTOR_LENA_PM(rLENAc14s1){ LABEL("LENA14_01"); signal: r47c1s12/in8;}
DETECTOR_LENA_PM(rLENAc14s2){ LABEL("LENA14_02"); signal: r47c1s14/in5;}
DETECTOR_LENA_PM(rLENAc15s1){ LABEL("LENA15_01"); signal: r47c1s14/in1;}
DETECTOR_LENA_PM(rLENAc15s2){ LABEL("LENA15_02"); signal: r47c1s14/in6;}

//Those are empty. Just in for better compilation. (Mixing with splitboxes)
DETECTOR_LENA_PM(rLENAc16s1){ LABEL("LENA16_01"); signal: r47c1s14/in7;}
DETECTOR_LENA_PM(rLENAc16s2){ LABEL("LENA16_02"); signal: r47c1s14/in8;}





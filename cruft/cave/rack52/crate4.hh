/* 
 * r52 CRATE 4
 * CAMAC Crate with CFD, Scaler, Controller
 *
 */
 
 CAMAC_CRATE(r52c4)
 {
 ;
 }
 
 
 CF8103(r52c4s1)
 {
 in1_8: r52c0s5/t1_8;
 
 m: r52c2s10/in1;
 mux_tb: r52c4s15/in1a; //anderes RMX3 Modul: mit 6x mehrfachstecker
 mux_e: r52c4s15/in5a;
 mux_mon: r52c4s15/in9a;
 test: r52c3s16/out1;
 tb1_8: r52c4s9/in1_8;
 }
 
 CF8103(r52c4s3)
 {
 in1_8: r52c0s5/t9_16;
 
 m: r52c2s10/in2;
 
 mux_tb: r52c4s15/in1b; //anderes RMX3 Modul: mit 6x mehrfachstecker
 mux_e: r52c4s15/in5b;
 mux_mon: r52c4s15/in9b;
 test: r52c3s16/out2;
 tb1_8: r52c4s9/in9_16;
 }
 
 CF8103(r52c4s5)
 {
 in1_8: r52c0s8/t9_16;
 
 m: r52c2s10/in3;
 mux_tb: r52c4s15/in1c; //anderes RMX3 Modul: mit 6x mehrfachstecker
 mux_e: r52c4s15/in5c;
 mux_mon: r52c4s15/in9c;
 test: r52c3s16/out3;
 tb1_8: r52c4s11/in9_16;
 }
 
 
 CF8103(r52c4s7)
 {
 in1_8: r52c0s8/t1_8;
 
 m: r52c2s10/in4;
 
 mux_tb: r52c4s15/in1d; //anderes RMX3 Modul: mit 6x mehrfachstecker
 mux_e: r52c4s15/in5d;
 mux_mon: r52c4s15/in9d;
 test: r52c3s16/out4;
 tb1_8: r52c4s11/in1_8;
 }
 
 
 
 
 DL1600(r52c4s9)
 {
 in1_8: r52c4s1/tb1_8;
 in9_16: r52c4s3/tb1_8;
 outa1_16: r15c4s6/in16_31;
 outb1_8: r52c4s13/in16_23;
 outb9_16: r52c4s13/in24_31;
 }
 
DL1600(r52c4s11)
 {
 in1_8: r52c4s7/tb1_8;
 in9_16: r52c4s5/tb1_8;
 outa1_16: r15c4s6/in0_15;
 outb1_8: r52c4s13/in32_39;
 outb9_16: r52c4s13/in40_47;
 }


SC4800(r52c4s13)
{
in16_23: r52c4s9/outb1_8;
in24_31: r52c4s9/outb9_16;
in32_39: r52c4s11/outb1_8;
in40_47: r52c4s11/outb9_16;

}


RMX3_2(r52c4s15) // RMX3 with combined connectors
{
in1a: r52c4s1/mux_tb;
in1b: r52c4s3/mux_tb;
in1c: r52c4s5/mux_tb;
in1d: r52c4s7/mux_tb;

in5a: r52c4s1/mux_e;
in5b: r52c4s3/mux_e;
in5c: r52c4s5/mux_e;
in5d: r52c4s7/mux_e;

in9a: r52c4s1/mux_mon;
in9b: r52c4s3/mux_mon;
in9c: r52c4s5/mux_mon;
in9d: r52c4s7/mux_mon;

out1_12: r52c4s17/in1a_3d;//? ist anderer RMX3. also mit buchstaben?

}

RMX3(r52c4s17)
{
in1a: r52c4s15/out1;
in1b: r52c4s15/out2;
in1c: r52c4s15/out3;
in1d: r52c4s15/out4;
in2a: r52c4s15/out5;
in2b: r52c4s15/out6;
in2c: r52c4s15/out7;
in2d: r52c4s15/out8;
in3a: r52c4s15/out9;
in3b: r52c4s15/out10;
in3c: r52c4s15/out11;
in3d: r52c4s15/out12;
out1: r52c3s21/in4b; //(geht in rmx2)
out2: r52c3s21/in5b;
out3: r52c3s21/in6b;
}

/*
Durchführung QDC FRS (r52c4s18)
{
out_ecl: abgezogen
}
Durchführung QDC FRS (r52c4s20)
{
out_ecl: abgezogen
in9_16: abgezogen
}
 */

CBV1000(r52c4s24)
{

//Karte mit 2 vsb. unterer: out, oberer in
SETTING("CRATE_NO"=> "4");
vsb_in:r52c3s24/vsb_out;//?? maybe processor
vsb_out: r14c6s24/vsb_in;//?? wieder CBV10000
}

DMS1000(r52c4s25)
{
;
}


/*
 * r52 CRATE 5
 *
 * CAMAC CRATE with CFD, Delay
 *
 */
 
 CAMAC_CRATE(r52c5)
 {
 ;
 }
 
 
CF8103(r52c5s2)
{
//in1_8: abgezogen 
or: rMASTERc3s2/in10;
//rest auch abgezogen
}

RMX3(r52c5s5)
{
in1a: r52c5s9/mux_tb; 
in1b: r52c5s13/mux_tb;
in1c: r52c5s15/mux_tb;
in1d: r52c5s17/mux_tb;
in2a: r52c5s9/mux_e; 
in2b: r52c5s13/mux_e;
in2c: r52c5s15/mux_e;
in2d: r52c5s17/mux_e;
in3a: r52c5s9/mux_mon;
in3b: r52c5s13/mux_mon;
in3c: r52c5s15/mux_mon;
in3d: r52c5s17/mux_mon;

out1: r52c3s21/in4d;
out2: r52c3s21/in5d;
out3: r52c3s21/in6d;
}


LIFO(r52c5s8)
{
in: r52c2s1u4/out1;
out13: r52c5s15/test;
out14: r52c5s13/test;
out15: r52c5s17/test;
out16: r52c5s9/test;
}

CF8103(r52c5s9)
{
in1_8:   r52c0s12/t1_8;
sum:     r52c2s3/in1;
mux_tb:  r52c5s5/in1a; 
mux_e:   r52c5s5/in2a;
mux_mon: r52c5s5/in3a;
test:    r52c5s8/out16;
tb1_8:   r52c5s19/in1_8;
}


CF8103(r52c5s13)
{
in1_8:   r52c0s23/t1_8;
sum:     r52c2s3/in3;
mux_tb:  r52c5s5/in1b; 
mux_e:   r52c5s5/in2b;
mux_mon: r52c5s5/in3b;
test:    r52c5s8/out14;
tb1_8:   r52c5s21/in1_8;
}

CF8103(r52c5s15)
{
in1_8:   r52c0s23/t9_16;
sum:     r52c2s3/in4;
mux_tb:  r52c5s5/in1c; 
mux_e:   r52c5s5/in2c;
mux_mon: r52c5s5/in3c;
test:    r52c5s8/out13;
tb1_8:   r52c5s21/in9_16;
}


CF8103(r52c5s17)
{
in1_8:   r52c0s12/t9_16;
sum:     r52c2s3/in6;
mux_tb:  r52c5s5/in1d; 
mux_e:   r52c5s5/in2d;
mux_mon: r52c5s5/in3d;
test:    r52c5s8/out15;
tb1_8:   r52c5s19/in9_16;
}

DL1600(r52c5s19)
{
in1_8:   r52c5s9/tb1_8;
in9_16:  r52c5s17/tb1_8;
outa1_16:r52c1s10/in0_15; 
outb1_16:r52c1s15/in0_15;
}

DL1600(r52c5s21)
{
in1_8:    r52c5s13/tb1_8;
in9_16:   r52c5s15/tb1_8;
outa1_16: r52c1s10/in16_31;
outb1_16: r52c1s15/in16_31;
}



CBV1000(r52c5s24)
{
SETTING("CRATE_NO" => "5");
vsb_in: r52c4s24/vsb_out; // oder umgekehrt?
}

DMS1000(r52c5s25)
{
;
}


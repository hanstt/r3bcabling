/* 
 * r52 CRATE 3
 * 
 * CAMAC Crate with CFD, LIFO, Delay...
 *
 */
 
 CAMAC_CRATE(r52c3)
 {
 ;
 }
 
 CF8103(r52c3s2)
 {
 in1_8: r52c0s15/t1_8;
 m: r52c2s10/in5;
 mux_tb: r52c3s18/in2c;
 mux_e: r52c3s18/in6c;
 mux_mon: r52c3s18/in10c;
 test: r52c3s16/out5;
 tb1_8: r52c3s10/in1_8;
 }
 
 
 CF8103(r52c3s4)
 {
 in1_8: r52c0s15/t9_16;
 m: r52c2s10/in6;
 mux_tb: r52c3s18/in2d;
 mux_e: r52c3s18/in6d;
 mux_mon: r52c3s18/in10d;
 test: r52c3s16/out6;
 tb1_8: r52c3s10/in9_16;
 }
  
 CF8103(r52c3s6)
 {
 in1_8: r52c0s18/t1_8;
 m: r52c2s10/in7;
 mux_tb: r52c3s18/in3a;
 mux_e: r52c3s18/in7a;
 mux_mon: r52c3s18/in11a;
 test: r52c3s16/out7;
 tb1_8: r52c3s12/in1_8;
 }
  
 CF8103(r52c3s8)
 {
 in1_8: r52c0s18/t9_16;
 m: r52c2s10/in8;
 mux_tb: r52c3s18/in3b;
 mux_e: r52c3s18/in7b;
 mux_mon: r52c3s18/in11b;
 test: r52c3s16/out8;
 tb1_8: r52c3s12/in9_16;
 }

DL1600(r52c3s10)
{
in1_8:     r52c3s2/tb1_8;
in9_16:    r52c3s4/tb1_8;
outa1_16:  r15c4s7/in16_31;
outb1_8:   r52c3s14/in16_23;
outb9_16:  r52c3s14/in24_31;
}

DL1600(r52c3s12)
{
in1_8:     r52c3s6/tb1_8;
in9_16:    r52c3s8/tb1_8;
outa1_16:  r15c4s7/in0_15;
outb1_8:   r52c3s14/in32_39;
outb9_16:  r52c3s14/in40_47;
}

SC4800(r52c3s14)
{

in16_23:  r52c3s10/outb1_8;
in24_31:  r52c3s10/outb9_16;
in32_39:  r52c3s12/outb1_8;
in40_47:  r52c3s12/outb9_16;
}

LIFO(r52c3s16)
{
in:   r52c2s1u4/out2; 
out1: r52c4s1/test; 
out2: r52c4s3/test; 
out3: r52c4s5/test;
out4: r52c4s7/test;
out5: r52c3s2/test;
out6: r52c3s4/test;
out7: r52c3s6/test;
out8: r52c3s8/test;
}


RMX3_2(r52c3s18)
{
in2c:  r52c3s2/mux_tb;
in2d:  r52c3s4/mux_tb;
in3a:  r52c3s6/mux_tb;
in3b:  r52c3s8/mux_tb;
in6c:  r52c3s2/mux_e;
in6d:  r52c3s4/mux_e;
in7a:  r52c3s6/mux_e;
in7b:  r52c3s8/mux_e;
in10c: r52c3s2/mux_mon;
in10d: r52c3s4/mux_mon;
in11a: r52c3s6/mux_mon;
in11b: r52c3s8/mux_mon;

out1_12: r52c3s20/in1a_3d;
}

RMX3_2(r52c3s20)
{
in1a_3d: r52c3s18/out1_12;
out1: r52c3s21/in4c;
out2: r52c3s21/in5c;
out3: r52c3s21/in6c;
}



RMX2(r52c3s21) 
{
in4a: r52c3s21/out1;
in4b: r52c4s17/out1;
in4c: r52c3s20/out1;
in4d: r52c5s5/out1;
in5a: r52c3s21/out2;
in5b: r52c4s17/out2;
in5c: r52c3s20/out2;
in5d: r52c5s5/out2;
in6a: r52c3s21/out3;
in6b: r52c4s17/out3;
in6c: r52c3s20/out3;
in6d: r52c5s5/out3;
out1: r52c3s21/in4a;
out2: r52c3s21/in5a;
out3: r52c3s21/in6a;
/* gehen raus zum muxxer:
out4: 
out5:
out6:*/
}



DMS1000(r52c3s23) // Different kind of camac interface/communication module
{
;
}

CBV1000(r52c3s24)
{
SETTING("CRATE_NO"=>"3");
vsb_in: r47c2s24/vsb_out; // in
vsb_out: r52c4s24/vsb_in; // out slot 23
}
DMS1000(r52c3s25)
{;
}

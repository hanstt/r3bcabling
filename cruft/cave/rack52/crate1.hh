/* 
 * CRATE 1
 * 
 *  VME CRATE with controller, qdc, triva etc 
 *
 */
 
 VME_CRATE(r52c1)
 {
 SETTING("CRATE_NO" => "78");
 }
 
 RIO3(r52c1s1)
 {
 SETTING("HOSTNAME" => "R3-4");
 }
 
 TRIVA7(r52c1s2)
 {
 tribu1: r47c3s2/tribu2;
 }
 
 CAEN_V792(r52c1s4)
 {
// TFW 5,6,7,8
 SETTING("ADDRESS" => "0x00020000");
 in0_7: r52c0s8/e1_8;
 in8_15:r52c0s8/e9_16;
 in16_23: r52c0s5/e1_8;
 in24_31: r52c0s5/e9_16;
 gate_in: r52c1s15/trig_in;// (linker eingang in s15) //is this really ni and out? upper in, lower out
 gate_out: r52c1s5/gate_out;
 }
  
 CAEN_V792(r52c1s5)
 {
 //TFW 1,2,3,4
 
 SETTING("ADDRESS"=>"0x00030000");
 in0_7: r52c0s18/e1_8;
 in8_15:r52c0s18/e9_16;
 in16_23: r52c0s15/e1_8;
 in24_31: r52c0s15/e9_16;
 gate_in: r52c2s7u1/out2;  
 gate_out: r52c1s4/gate_out;
 }
 
 
 CAEN_V775(r52c1s6)
 {
   SETTING("ADDRESS" => "0x00000000");

 in0_15: r52c4s11/outa1_16;
 in16_31: r52c4s9/outa1_16;  
 com_in: r52c2s4u1/out16;
 com_out: r52c1s7/com_in;
 }
 
 CAEN_V775(r52c1s7)
 {
   SETTING("ADDRESS" => "0x00000000");

 in0_15: r52c3s12/outa1_16;
 in16_31: r52c3s10/outa1_16;  
 com_in: r52c1s6/com_out;
 }
 
 CAEN_V775(r52c1s10)
 {
   SETTING("ADDRESS" => "0x00000000");

 in0_15: r52c5s19/outa1_16;
 in16_31:r52c5s21/outa1_16;
 com_out: r52c2s9/out;//oberer
 }
 
 
 CAEN_V792(r52c1s13)
 {
 SETTING("ADDRESS" => "0x00070000");
 in0_7: r52c0s12/e1_8;
 in8_15: r52c0s12/e9_16;
 in16_23: r52c0s23/e1_8; 
 in24_31: r52c0s23/e9_16;
 gate_in: r52c2s7u2/out2; //upper
 }


CAEN_V830(r52c1s15)
{
SETTING("ADDRESS" => "0x00600000");
   SERIAL("CAEN0098"); //VME_SCALER_1
   SETTING("VIRTUAL_SLOT" => "30");

in0_15: r52c5s19/outb1_16;
in16_31: r52c5s21/outb1_16;
trig_in: r52c1s4/gate_in; //left
}

CAEN_V792(r52c1s17)
{
SETTING("ADDRESS" => "0x000a0000");

in0: r53c1s2/out1;
in1: r53c1s3/out1;
in2: r53c1s2/out2;
in3: r53c1s3/out2;
in4: r53c1s2/out3;
in5: r53c1s3/out3;
//in6:
in7: r53c1s3/out4;

in8:  r53c1s2/out5;
in9:  r53c1s3/out5;
in10: r53c1s2/out6;
in11: r53c1s3/out6;
in12: r53c1s2/out7;
in13: r53c1s3/out7;
in14: r53c1s2/out8;
in15: r53c1s3/out8;

in16: r53c1s2/out9;
in17: r53c1s3/out9;
in18: r53c1s2/out10;
in19: r53c1s3/out10;
in20: r53c1s2/out11;
in21: r53c1s3/out11;
in22: r53c1s2/out12;
in23: r53c1s3/out12;

in24: r53c1s2/out13;
in25: r53c1s3/out13;
in26: r53c1s2/out14;
in27: r53c1s3/out14;
in28: r53c1s2/out15;
in29: r53c1s3/out15;
in30: r53c1s2/out16;
in31: r53c1s3/out16;
gate_in:r52c2s5u1/out3; //obere
}


CAEN_V792(r52c1s19)
{
SETTING("ADDRESS"=>"0x0008000");
in0_7: r51c1s7/out1_8;
in8_15: r51c1s7/out9_16;

in16_19: r51c1s11/out1_4;
in21_22: r51c1s11/out6_7;
in23:    r51c1s4/out27; 

in24:    r51c1s4/out17;
in25_31: r51c1s11/out10_16;
gate_in: r52c2s5u2/out4;//obere
}

CAEN_V965(r52c1s20)
{
SETTING("ADDRESS"=>"0x00090000");
//in0,2,4 leer
in5: r51c3s2/out; // delaybox 
in9: r51c3s4/out; // delaybox
in11: r51c3s6/out;
gate_in: r52c2s5u3/out4;//oberer
}

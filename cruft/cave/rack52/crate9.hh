/*
 * r52 CRATE 9
 *
 * NIM Crate with amplifiers
 *
 */
 
 
 NIM_CRATE(r52c9)
 {
 ;
 }
 
 FA8000(r52c9s5)
 {
; 
 }

FA8000_U(r52c9s5u1)
{
in: rNTFRc3/out3;//       NTF13.1
out1: r52c0s23/in3; //NTF3_3splitbox
out2: r52c8s2/in5;
}

FA8000_U(r52c9s5u2)
{
in: rNTFRc3/out7; //      NTF13.2
out1: r52c0s23/in7; //NTF3_7splitbox
out2: r52c8s2/in6;
}

FA8000_U(r52c9s5u3)
{
in: rNTFRc4/out1; //      NTF14.1
out1: r52c0s23/in9; //NTF4_1splitbox
}

FA8000_U(r52c9s5u4)
{
in: rNTFRc4/out5; //      NTF14.2
out1: r52c0s23/in13; //NTF4_5splitbox
}

FA8000_U(r52c9s5u5)
{
in: rNTFRc3/out4; //      NTF15.1
out1: r52c0s23/in4; //NTF3_4splitbox
}

FA8000_U(r52c9s5u6)
{
in: rNTFRc3/out8; //      NTF15.2
out1: r52c0s23/in8; //NTF3_8splitbox
;}

FA8000_U(r52c9s5u7)
{
in: rNTFRc4/out2; //      NTF16.1
out1: r52c0s23/in10; //NTF4_2splitbox
}

FA8000_U(r52c9s5u8)
{
in: rNTFRc4/out6; //      NTF16.2
out1: r52c0s23/in14; //NTF4_6splitbox
}


FA8000(r52c9s7)
{
;
}

FA8000_U(r52c9s7u1)
{
in: rNTFRc3/out1; //NTF9.1
out1:r52c0s23/in1; //NTF3_1 splitpbox
}

FA8000_U(r52c9s7u2)
{
in: rNTFRc3/out5; //NTF9.2
out1:r52c0s23/in5; //NTF3_5 splitpbox
}

FA8000_U(r52c9s7u3)
{
in: rNTFRc4/out3; //NTF10.1
out1: r52c0s23/in11; //NTF4_3 splitpbox
}

FA8000_U(r52c9s7u4)
{
in: rNTFRc4/out7; //NTF10.2
out1: r52c0s23/in15; //NTF4_7 splitbox
}

FA8000_U(r52c9s7u5)
{
in: rNTFRc3/out2; // NTF11.1
out1:r52c0s23/in2; //NTF3_2 splitbox
}

FA8000_U(r52c9s7u6)
{
in: rNTFRc3/out6; //NTF11.2
out1: r52c0s23/in6; //NTF3_6 splitbox
}

FA8000_U(r52c9s7u7)
{
in: rNTFRc4/out4; //NTF12.1
out1: r52c0s23/in12; //NTF4_4 splitpbox
out2: r52c8s3/in5;
}

FA8000_U(r52c9s7u8)
{
in: rNTFRc4/out8; //NTF12.2
out1: r52c0s23/in16; //NTF4_8 splitpbox
out2: r52c8s3/in6;
}


FA8000(r52c9s9)
{
;
}

FA8000_U(r52c9s9u1)
{
in: rNTFRc1/out3; //NTF5.1
out1: r52c0s12/in3; //NTF1_3 splitbox:
out2: r52c8s8/in5;
}

FA8000_U(r52c9s9u2)
{
in: rNTFRc1/out7; //NTF5.2
out1: r52c0s12/in7; //NTF1_7 splitbox:
out2: r52c8s8/in6;
}

FA8000_U(r52c9s9u3)
{//kein input
out2: r52c8s11/in5;
}

FA8000_U(r52c9s9u4)
{//kein input
out2: r52c8s11/in6;
}

FA8000_U(r52c9s9u5)
{
in: rNTFRc1/out4; //NTF7.1
out1: r52c0s12/in4; //NTF1_4 splitbox:
out2: r52c8s12/in5;
}

FA8000_U(r52c9s9u6)
{
in: rNTFRc1/out8; //NTF7.2
out1: r52c0s12/in8;//NTF1_8 splitbox:
out2: r52c8s12/in6;
}

FA8000_U(r52c9s9u7)
{
in: rNTFRc2/out2; //NTF8.1
out1: r52c0s12/in10; //NTF2_2 splitbox:
}

FA8000_U(r52c9s9u8)
{
in: rNTFRc2/out6;   //NTF8.2
out1: r52c0s12/in14;//NTF2_6 splitbox:
}


FA8000(r52c9s11)
{
;
}

FA8000_U(r52c9s11u1)
{
in: rNTFRc1/out1;
out1: r52c0s12/in1; //ntf 1_1
}

FA8000_U(r52c9s11u2)
{
in: rNTFRc1/out5; //NTF 1.2
out1: r52c0s12/in5;//ntf 1_5
}

FA8000_U(r52c9s11u3)
{
in: rNTFRc2/out3; //NTF 2.1
out1: r52c0s12/in11;//ntf 2_3
out2: r52c8s6/in5;
}

FA8000_U(r52c9s11u4)
{
in: rNTFRc2/out7; //NTF 2.2
out1: r52c0s12/in15;//ntf2_7
out2: r52c8s6/in6;
}

FA8000_U(r52c9s11u5)
{
in: rNTFRc1/out2; //TNF3.1
out1: r52c0s12/in2;//ntf1_2
out2: r52c8s5/in5;
}

FA8000_U(r52c9s11u6)
{
in: rNTFRc1/out6; //NTF 3.2
out1: r52c0s12/in6;//ntf1_6
out2: r52c8s5/in6;
}

FA8000_U(r52c9s11u7)
{
in: rNTFRc2/out4; //NTF 4.1
out1: r52c0s12/in12;//ntf2_4
out2: r52c8s9/in5;
}

FA8000_U(r52c9s11u8)
{
in: rNTFRc2/out8; //NTF 4.2
out1: r52c0s12/in16; //ntf2_8
out2: r52c8s9/in6;
}

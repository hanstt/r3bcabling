/*
 * r52 CRATE 2
 *
 * NIM Crate with GG, Delay, LF
 *
 */
 
 NIM_CRATE(r52c2)
 {
 ;
 }
 
LF4000(r52c2s1)
{
;
}
LF4000_4_U(r52c2s1u4)
{
in1:  r52c2s8/out;
//out1: r52c5s8/in;
out2: r52c3s16/in;
}

EC1601(r52c2s2)
{
in1: r52c2s7u7/out2;
}

SU1200(r52c2s3)
{

in1: r52c5s9/sum;
in3: r52c5s13/sum;
in4: r52c5s15/sum;
in6: r52c5s17/sum;
analog_out: r52c2s11u4/in;
 }

LF4000(r52c2s4)
{
;}
LF4000_16_U(r52c2s4u1)
{
in1: rMASTERc3s6u1/out1;
out1: r52c2s7u1/in;
out2: r52c2s7u2/in;
out3: r52c2s7u3/in;
out4: r52c2s5u1/in4;
//out5: dangling
//out6: FASTBUS 
out7: r52c2s7u4/in;
out8: r52c2s5u2/in4;
out9: r52c2s7u7/in;
out11: r52c2s7u5/in;
out12: r52c2s5u3/in4;
//out13: dangling
out14: r52c2s9/in; //out? rechter lemo ausgang
//out15: dangling 
out16: r15c4s6/com_in;
}

LF4000(r52c2s5)
{
;
}

LF4000_4_U(r52c2s5u1)
 {
 in3:  r52c2s7u3/out2;
 in4:  r52c2s4u1/out4;
 out3: r15c4s17/gate_in;
 //out4: abgezogen
 }
 
 LF4000_4_U(r52c2s5u2)
 {
 in3:  r52c2s7u4/out2;
 in4:  r52c2s4u1/out8;
 //out3:  dangling
 out4: r15c4s19/gate_in;
  }
LF4000_4_U(r52c2s5u3)
 {
 in3:  r52c2s7u5/out2;
 in4:  r52c2s4u1/out12;
 out4: r15c4s20/gate_in;
 //out3: abgezogen
 }
 
 GG8000(r52c2s7)
 {
 ;
 }
 GG8000_U(r52c2s7u1)
 {
 in:   r52c2s4u1/out1;
 //out1: geht auch ins leere
 out2: r15c4s5/gate_in;
 }
GG8000_U(r52c2s7u2)
 {
 in:   r52c2s4u1/out2;
 //out1: geht auch ins leere -> FASTBUS
 out2: r15c4s13/gate_in;
 }
GG8000_U(r52c2s7u3)
 {
 in:   r52c2s4u1/out3;
 out2: r52c2s5u1/in3;
 } 


 GG8000_U(r52c2s7u4)
 {
 in:   r52c2s4u1/out7;
 out2: r52c2s5u2/in3;
 } 

GG8000_U(r52c2s7u5)
 {
 in:   r52c2s4u1/out11;
 out2: r52c2s5u3/in3;
 } 
 GG8000_U(r52c2s7u7)
 {
 in:   r52c2s4u1/out9;
 out2: r52c2s2/in1;
 } 


VARIABLE_DELAY(r52c2s8)//delaymodul
{
//fe290
SETTING("DELAY" => "60 ns");
in:  rMASTERc3s6u2/out8; //links
out: r52c2s1u4/in1;
}

VARIABLE_DELAY(r52c2s9)
{
//fe290
SETTING("DELAY" => "63.5 ns");
in:  r52c2s4u1/out14;
out: r15c4s10/com_out;
}


SU1200(r52c2s10)
{
in1: r52c4s1/m;
in2: r52c4s3/m;
in3: r52c4s5/m;
in4: r52c4s7/m;
in5: r52c3s2/m;
in6: r52c3s4/m;
in7: r52c3s6/m;
in8: r52c3s8/m;

analog_out: r52c2s11u3/in;
}

CF8000(r52c2s11)
{
;
}

CF8000_U(r52c2s11u3)
{
in: r52c2s10/analog_out;
outA:rMASTERc3s2/in4;
}
CF8000_U(r52c2s11u4)
{
in: r52c2s3/analog_out;
outA: rMASTERc3s4u3/in1;
}

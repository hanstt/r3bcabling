/*
 * Crate 2 in Masterrack
 *
 * VME Crate with processor/controller/TRIVA
 *
 */
VME_CRATE(rMASTERc2)
{
;
}

// TO DO: include this! ist auch prozessor
RIO3(rMASTERc2s1) 
{
SETTING("HOSTNAME" => "R3-14");
vsb_out:rMASTERc1s24/vsb_in; //hinten
//cons: vscom port 2//von da usb pc und output vscom port1:  dann rMASTERc4s3/ser1
}

TRIVA7(rMASTERc2s2)
{
tribu1: r54c1s5/tribu1; // triva7 modul
tribu2: r47c3s2/tribu1;

in1_4: rMASTERc2s3/io_ecl1_4;
out1_8: rMASTERc2s3/io_ecl9_16;

}

VULOM4(rMASTERc2s3)

{

in_ecl1_8: rMASTERc3s2/out_ecl1_8;
in_ecl9_16: rMASTERc3s2/out_ecl9_16;

io_ecl1_4: rMASTERc2s2/in1_4;
io_ecl9_16: rMASTERc2s2/out1_8;

out_ecl1: rMASTERc3s3/in_ecl16;
out_ecl2: rMASTERc3s3/in_ecl9;
in1: rMASTERc3s9u2/out4;
in2: rMASTERc3s9u4/out4;
out1: rMASTERc3s6u1/in1;
}


VULOM4(rMASTERc2s14)
{
out_ecl1_8: rMASTERc2s15/in_ecl1_8;
out_ecl9_16: rMASTERc2s15/in_ecl9_16;
}

ENV3(rMASTERc2s15)
 {
 in_ecl1_8: rMASTERc2s14/out_ecl1_8;
 in_ecl9_16: rMASTERc2s14/out_ecl9_16; 
// nim_out5: // TTESTPULSE GFI-> testpuls unn�tig
 nim_out6: rMASTERc3s9u2/in2;
 nim_out7: rMASTERc3s9u4/in2;
 
 }

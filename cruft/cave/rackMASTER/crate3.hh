/* rMASTER Crate 3
 *
 * NIM Crate
 * Some Modules should be moved to r47c3
 *
 */
 NIM_CRATE(rMASTERc3)
 {
 ;
 }
 
 EC1610(rMASTERc3s2)
 {
 in1: r47c4s17u3/out3;
 in2: rMASTERc3s4u3/out2;
 in3: rMASTERc3s4u3/out3;
// in4: r52c2s11u3/outA; taken out by KB
 in5: rMASTERc3s2/out4; 
 in6: r54c2s2u3/out4;
 in7: r54c2s2u3/out3; // delayed 50ns
 in8: rMASTERc3s12u8/outB1;
 in9: rMASTERc3s2/out8;
// in10: r52c5s2/or; 
 //in11:   LaBr Trigger;
 in12: r47c4s17u4/out4;
 out_ecl1_8: rMASTERc2s3/in_ecl1_8;
 out_ecl9_16: rMASTERc2s3/in_ecl9_16;
 out4: rMASTERc3s2/in5; 
 //out6:muxxer
 out8: rMASTERc3s2/in9;
 }
 
 EC1610(rMASTERc3s3)
 {
 in_ecl9: rMASTERc2s3/out_ecl2;
 in_ecl16: rMASTERc2s3/out_ecl1;
 in1: r47c4s17u4/out2;
 in3: rMASTERc3s6u2/out1;
 in4: rMASTERc3s5u3/out1;
 in9: rMASTERc3s6u2/out3;
 out_ecl1: rMASTERc1s21/in7;// vertauscht, 1-8,2-7 etc.
 out_ecl2: rMASTERc1s21/in6;
 out_ecl3: rMASTERc1s21/in5;
 out_ecl4: rMASTERc1s21/in4;
 out_ecl5: rMASTERc1s21/in3;
 out_ecl6: rMASTERc1s21/in2;
 out_ecl7: rMASTERc1s21/in1;
 out_ecl8: rMASTERc1s21/in0;

 out10: rMASTERc3s6u1/in4;
 }
 
 LF4000(rMASTERc3s4)
 {
 ;
 }
 LF4000_4_U(rMASTERc3s4u3)
 {
// in1: r52c2s11u4/outA;
 out2: rMASTERc3s2/in2;
 out3:rMASTERc3s2/in3;
 }
 
 
 LF4000(rMASTERc3s5)
 {
 ;
 }
 LF4000_4_U(rMASTERc3s5u1)
 {
 in1:rMASTERc3s6u1/out7; 
 //out2: abgezogen;
 out3:rMASTERc3s8u6/in;
 }
 
 LF4000_4_U(rMASTERc3s5u3)
 {
 in1: rMASTERc1s22/start;
 out1: rMASTERc3s3/in4;
 out4: rMASTERc3s6u1/in2; //delay 550ns
  }


LF4000(rMASTERc3s6)
 {
 ;
 }
 LF4000_8_U(rMASTERc3s6u1)
 {
 in1: rMASTERc2s3/out1;
 in2: rMASTERc3s5u3/out4;
 in4: rMASTERc3s3/out10;
// out1: r52c2s4u1/in1;
 out2: r47c3s8/com_in; // delay 100ns kann das gate out sein? 
 out3: r54c2s2u2/out3;
 out4: r47c3s12/trig_in;
 out5: rMASTERc3s8u4/in;
 out6: rMASTERc3s8u5/in;
 out7: rMASTERc3s5u1/in1;
 out8: r47c3s5/com_in;
  
 }
 LF4000_8_U(rMASTERc3s6u2)
 {
in1: rMASTERc1s22/stop;
out1: rMASTERc3s3/in3;
out2: r47c2s14/in;
out3: rMASTERc3s3/in9;
out4: rMASTERc3s12u1/in;
// out8: r52c2s8/in;
}





SU1200(rMASTERc3s7)
{
in1: r47c2s10/sum;
in2: r47c2s8/sum;
in3: r47c2s4/sum;
in4: r47c2s6/sum;
//beide schalter unten

analog_out: rMASTERc3s12u8/in;
}

GG8000(rMASTERc3s8)
{
;
}

GG8000_U(rMASTERc3s8u4)
{
in: rMASTERc3s6u1/out5;
out2: r47c3s7/gate_in; //oder wie immer es heisst
}
GG8000_U(rMASTERc3s8u5)
{
in: rMASTERc3s6u1/out6;
out2: r47c3s4/gate_in; //oder wie immer es heisst
} 
GG8000_U(rMASTERc3s8u6)
{
in: rMASTERc3s5u1/out3;
//out2: r47c3s10/gate_in; //oder wie immer es heisst. LaBr->nicht mehr da
}

LF4000(rMASTERc3s9)
{
;
}
LF4000_4_U(rMASTERc3s9u2)
{
in1: rMASTERc3s11u1/end_marker; // modul?
in2: rMASTERc2s15/nim_out6;
out1: rMASTERc3s10u1/start;
out2: rMASTERc3s10u2/reset;
out4: rMASTERc2s3/in1;
}

LF4000_4_U(rMASTERc3s9u4)
{
in1: rMASTERc3s11u2/end_marker; // modul?
in2: rMASTERc2s15/nim_out7;
out1: rMASTERc3s10u2/start;
out2: rMASTERc3s10u1/reset;
out4: rMASTERc2s3/in2;
}

CAEN_V93B(rMASTERc3s10)
{
;
}
CAEN_V93B_U(rMASTERc3s10u1)
{
start: rMASTERc3s9u2/out1; 
reset: rMASTERc3s9u4/out2; 
}
CAEN_V93B_U(rMASTERc3s10u2)
{
start: rMASTERc3s9u4/out1; 
reset: rMASTERc3s9u2/out2; 
}


CAEN_V93B(rMASTERc3s11)
{
;
}
CAEN_V93B_U(rMASTERc3s11u1)
{
//start: nach draussen? spill on.. 
end_marker: rMASTERc3s9u2/in1;
ecl_out1: rMASTERc4s5/in1;
}


CAEN_V93B_U(rMASTERc3s11u2)
{
//start: nach draussen? spill off.. 
end_marker: rMASTERc3s9u4/in1;
ecl_out1: rMASTERc4s5/in2;
}

CF8000(rMASTERc3s12)
{
//delay 4ns
;}

CF8000_U(rMASTERc3s12u1)
{
in: rMASTERc3s6u2/out4; //ewig viel verzögert

}
CF8000_U(rMASTERc3s12u8)
{
in: rMASTERc3s7/analog_out; //ewig viel verzögert
outA: r47c4s9u5/in;
outB1: rMASTERc3s2/in8;
}




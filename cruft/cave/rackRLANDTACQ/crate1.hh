/*
 * TACQUILA BOARDS LAND. SIGNAL from detector
 *
 * adapted after new cabling of LAND in March 2012
 *
 */

LANDFEE2(rRLANDTACQc1s1u1)   // slot 1
{
 in1: rNP1c21/out8;
 in2: rNP1c21/out7;
 in3: rNP1c21/out6;
 in4: rNP1c21/out5;
 in5: rNP1c21/out4;
 in6: rNP1c21/out3;
 in7: rNP1c21/out2;
 in8: rNP1c21/out1;
   
 in9:  rNP1c22/out8;
 in10: rNP1c22/out7;
 in11: rNP1c22/out6;
 in12: rNP1c22/out5;
 in13: rNP1c22/out4;
 in14: rNP1c22/out3;
 in15: rNP1c22/out2;
 in16: rNP1c22/out1;
  //gtb_out1_16: r54c1s15/gtb;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}

LANDFEE2(rRLANDTACQc1s2u1)  // slot 2
{
 in1: rNP1c23/out8;
 in2: rNP1c23/out7;
 in3: rNP1c23/out6;
 in4: rNP1c23/out5;
 in5: rNP1c23/out4;
 in6: rNP1c23/out3;
 in7: rNP1c23/out2;
 in8: rNP1c23/out1;
   
 in9:  rNP1c24/out8;
 in10: rNP1c24/out7;
 in11: rNP1c24/out6;
 in12: rNP1c24/out5;
 in13: rNP1c24/out4;
 in14: rNP1c24/out3;
 in15: rNP1c24/out2;
 in16: rNP1c24/out1;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}
LANDFEE2(rRLANDTACQc1s3u1)  //slot 3
{
 in1: rNP1c25/out8;
 in2: rNP1c25/out7;
 in3: rNP1c25/out6;
 in4: rNP1c25/out5;
 in5: rNP1c25/out4;
 in6: rNP1c25/out3;
 in7: rNP1c25/out2;
 in8: rNP1c25/out1;
   
 in9:  rNP2c21/out8;
 in10: rNP2c21/out7;
 in11: rNP2c21/out6;
 in12: rNP2c21/out5;
 in13: rNP2c21/out4;
 in14: rNP2c21/out3;
 in15: rNP2c21/out2;
 in16: rNP2c21/out1;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}

LANDFEE2(rRLANDTACQc1s4u1) //slot 4
{
 in1: rNP2c22/out8;
 in2: rNP2c22/out7;
 in3: rNP2c22/out6;
 in4: rNP2c22/out5;
 in5: rNP2c22/out4;
 in6: rNP2c22/out3;
 in7: rNP2c22/out2;
 in8: rNP2c22/out1;
   
 in9:  rNP2c23/out8;
 in10: rNP2c23/out7;
 in11: rNP2c23/out6;
 in12: rNP2c23/out5;
 in13: rNP2c23/out4;
 in14: rNP2c23/out3;
 in15: rNP2c23/out2;
 in16: rNP2c23/out1;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}
LANDFEE2(rRLANDTACQc1s5u1) //slot 5
{
 in1: rNP2c24/out8;
 in2: rNP2c24/out7;
 in3: rNP2c24/out6;
 in4: rNP2c24/out5; 
 in5: rNP2c24/out4;
 in6: rNP2c24/out3;
 in7: rNP2c24/out2;
 in8: rNP2c24/out1;
  
 in9:  rNP2c25/out8;
 in10: rNP2c25/out7;
 in11: rNP2c25/out6;
 in12: rNP2c25/out5;
 in13: rNP2c25/out4;
 in14: rNP2c25/out3;
 in15: rNP2c25/out2;
 in16: rNP2c25/out1;  

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}
 LANDFEE2(rRLANDTACQc1s6u1)   // slot 6
{
 in1: rNP3c21/out8;
 in2: rNP3c21/out7;
 in3: rNP3c21/out6;
 in4: rNP3c21/out5;
 in5: rNP3c21/out4;
 in6: rNP3c21/out3;
 in7: rNP3c21/out2;
 in8: rNP3c21/out1;
   
 in9:  rNP3c22/out8;
 in10: rNP3c22/out7;
 in11: rNP3c22/out6;
 in12: rNP3c22/out5;
 in13: rNP3c22/out4;
 in14: rNP3c22/out3;
 in15: rNP3c22/out2;
 in16: rNP3c22/out1;
  //gtb_out1_16: r54c1s15/gtb;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}

LANDFEE2(rRLANDTACQc1s7u1)  // slot 7
{
 in1: rNP3c23/out8;
 in2: rNP3c23/out7;
 in3: rNP3c23/out6;
 in4: rNP3c23/out5;
 in5: rNP3c23/out4;
 in6: rNP3c23/out3;
 in7: rNP3c23/out2;
 in8: rNP3c23/out1;
   
 in9:  rNP3c24/out8;
 in10: rNP3c24/out7;
 in11: rNP3c24/out6;
 in12: rNP3c24/out5;
 in13: rNP3c24/out4;
 in14: rNP3c24/out3;
 in15: rNP3c24/out2;
 in16: rNP3c24/out1;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}
LANDFEE2(rRLANDTACQc1s8u1)  //slot 8
{
 in1: rNP3c25/out8;
 in2: rNP3c25/out7;
 in3: rNP3c25/out6;
 in4: rNP3c25/out5;
 in5: rNP3c25/out4;
 in6: rNP3c25/out3;
 in7: rNP3c25/out2;
 in8: rNP3c25/out1;
   
 in9:  rNP4c21/out8;
 in10: rNP4c21/out7;
 in11: rNP4c21/out6;
 in12: rNP4c21/out5;
 in13: rNP4c21/out4;
 in14: rNP4c21/out3;
 in15: rNP4c21/out2;
 in16: rNP4c21/out1;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}

LANDFEE2(rRLANDTACQc1s9u1) //slot 9
{
 in1: rNP4c22/out8;
 in2: rNP4c22/out7;
 in3: rNP4c22/out6;
 in4: rNP4c22/out5;
 in5: rNP4c22/out4;
 in6: rNP4c22/out3;
 in7: rNP4c22/out2;
 in8: rNP4c22/out1;
   
 in9:  rNP4c23/out8;
 in10: rNP4c23/out7;
 in11: rNP4c23/out6;
 in12: rNP4c23/out5;
 in13: rNP4c23/out4;
 in14: rNP4c23/out3;
 in15: rNP4c23/out2;
 in16: rNP4c23/out1;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}
LANDFEE2(rRLANDTACQc1s10u1) //slot 10
{
 in1: rNP4c24/out8;
 in2: rNP4c24/out7;
 in3: rNP4c24/out6;
 in4: rNP4c24/out5; 
 in5: rNP4c24/out4;
 in6: rNP4c24/out3;
 in7: rNP4c24/out2;
 in8: rNP4c24/out1;
  
 in9:  rNP4c25/out8;
 in10: rNP4c25/out7;
 in11: rNP4c25/out6;
 in12: rNP4c25/out5;
 in13: rNP4c25/out4;
 in14: rNP4c25/out3;
 in15: rNP4c25/out2;
 in16: rNP4c25/out1;  

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}


/* ... */

#define TRIPLEX2_TACQ_QDC2_TACQUILA3(rcs,gtbmaster,gtbslave,upj,downj5,downj7,qserial,tserial) \
						\
  TRIPLEX2(rcs##u2)				\
  {						\
    in1_16:  .u1/e1_16;				\
    out1_16: .u3/in1_16;			\
  						\
    j6: upj;					\
    j5: downj5;					\
    j7: downj7;					\
  }						\
  						\
  TACQ_QDC2(rcs##u3)				\
  {  						\
    /* nonhack : SERIAL(qserial);*/		\
    /* hack */                                  \
    SERIAL(tserial);                            \
    /* endhack */                               \
    in1_16:  .u2/out1_16;			\
  }						\
  						\
  TACQUILA3(rcs)				\
  {						\
    SERIAL(tserial);                      	\
    in1_16: .u1/t1_16;				\
    /* hack */                                  \
    out1: .s/hackin17;				\
    hackin17: .s/out1;                          \
    /* endhack */                               \
    gtb_master: gtbmaster;			\
    gtb_slave:  gtbslave;			\
  }


// TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc1s10,r54c1s15/gtb1,.s9/gtb_master,.s8u2/j7,,,"TQDCc1t10","TTDCc1t10");
// TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc1s9,.s10/gtb_slave,.s8/gtb_master,.s8u2/j5,,,"TQDCc1t9","TTDCc1t9");
// TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc1s8,.s9/gtb_slave,.s7/gtb_master,.s4u2/j7,.s9u2/j6,.s10u2/j6,"TQDCc1t8","TTDCc1t8");
// TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc1s7,.s8/gtb_slave,.s6/gtb_master,.s5u2/j7,,,"TQDCc1t7","TTDCc1t7");
// TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc1s6,.s7/gtb_slave,.s5/gtb_master,.s5u2/j5,,,"TQDCc1t6","TTDCc1t6");
// TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc1s5,.s6/gtb_slave,.s4/gtb_master,.s4u2/j5,.s6u2/j6,.s7u2/j6,"TQDCc1t5","TTDCc1t5");
// TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc1s4,.s5/gtb_slave,.s3/gtb_master,.s3u2/j5,.s5u2/j6,.s8u2/j6,"TQDCc1t4","TTDCc1t4");
// TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc1s3,.s4/gtb_slave,.s2/gtb_master,.s1u2/j7,.s4u2/j6,,"TQDCc1t3","TTDCc1t3");
// TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc1s2,.s3/gtb_slave,.s1/gtb_master,.s1u2/j5,rRLANDTACQc2s1u2/j6,rRLANDTACQc3s1u2/j6,"TQDCc1t2","TTDCc1t2");
// TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc1s1,.s2/gtb_slave,,,.s2u2/j6,.s3u2/j6,"TQDCc1t1","TTDCc1t1");

TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc1s10,r54c1s15/gtb1,.s9/gtb_master,.s8u2/j7,,,"SAM6_GTB0_TAC10","SAM6_GTB0_TAC10");
TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc1s9,.s10/gtb_slave,.s8/gtb_master,.s8u2/j5,,,"SAM6_GTB0_TAC09","SAM6_GTB0_TAC09");
TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc1s8,.s9/gtb_slave,.s7/gtb_master,.s4u2/j7,.s9u2/j6,.s10u2/j6,"SAM6_GTB0_TAC08","SAM6_GTB0_TAC08");
TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc1s7,.s8/gtb_slave,.s6/gtb_master,.s5u2/j7,,,"SAM6_GTB0_TAC07","SAM6_GTB0_TAC07");
TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc1s6,.s7/gtb_slave,.s5/gtb_master,.s5u2/j5,,,"SAM6_GTB0_TAC06","SAM6_GTB0_TAC06");
TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc1s5,.s6/gtb_slave,.s4/gtb_master,.s4u2/j5,.s6u2/j6,.s7u2/j6,"SAM6_GTB0_TAC05","SAM6_GTB0_TAC05");
TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc1s4,.s5/gtb_slave,.s3/gtb_master,.s3u2/j5,.s5u2/j6,.s8u2/j6,"SAM6_GTB0_TAC04","SAM6_GTB0_TAC04");
TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc1s3,.s4/gtb_slave,.s2/gtb_master,.s1u2/j7,.s4u2/j6,,"SAM6_GTB0_TAC03","SAM6_GTB0_TAC03");
TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc1s2,.s3/gtb_slave,.s1/gtb_master,.s1u2/j5,rRLANDTACQc2s1u2/j6,rRLANDTACQc3s1u2/j6,"SAM6_GTB0_TAC02","SAM6_GTB0_TAC02");
TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc1s1,.s2/gtb_slave,,,.s2u2/j6,.s3u2/j6,"SAM6_GTB0_TAC01","SAM6_GTB0_TAC01");

/*
 * TACQUILA BOARDS LAND. SIGNAL from detector
 *
 * adapted after new cabling of LAND in March 2012
 *
 */

LANDFEE2(rRLANDTACQc2s1u1)   // slot 1
{
 in1: rNP5c21/out8;
 in2: rNP5c21/out7;
 in3: rNP5c21/out6;
 in4: rNP5c21/out5;
 in5: rNP5c21/out4;
 in6: rNP5c21/out3;
 in7: rNP5c21/out2;
 in8: rNP5c21/out1;
   
 in9:  rNP5c22/out8;
 in10: rNP5c22/out7;
 in11: rNP5c22/out6;
 in12: rNP5c22/out5;
 in13: rNP5c22/out4;
 in14: rNP5c22/out3;
 in15: rNP5c22/out2;
 in16: rNP5c22/out1;
  //gtb_out1_16: r54c1s15/gtb;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}

LANDFEE2(rRLANDTACQc2s2u1)  // slot 2
{
 in1: rNP5c23/out8;
 in2: rNP5c23/out7;
 in3: rNP5c23/out6;
 in4: rNP5c23/out5;
 in5: rNP5c23/out4;
 in6: rNP5c23/out3;
 in7: rNP5c23/out2;
 in8: rNP5c23/out1;
   
 in9:  rNP5c24/out8;
 in10: rNP5c24/out7;
 in11: rNP5c24/out6;
 in12: rNP5c24/out5;
 in13: rNP5c24/out4;
 in14: rNP5c24/out3;
 in15: rNP5c24/out2;
 in16: rNP5c24/out1;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}
LANDFEE2(rRLANDTACQc2s3u1)  //slot 3
{
 in1: rNP5c25/out8;
 in2: rNP5c25/out7;
 in3: rNP5c25/out6;
 in4: rNP5c25/out5;
 in5: rNP5c25/out4;
 in6: rNP5c25/out3;
 in7: rNP5c25/out2;
 in8: rNP5c25/out1;
   
 in9:  rNP6c21/out8;
 in10: rNP6c21/out7;
 in11: rNP6c21/out6;
 in12: rNP6c21/out5;
 in13: rNP6c21/out4;
 in14: rNP6c21/out3;
 in15: rNP6c21/out2;
 in16: rNP6c21/out1;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}

LANDFEE2(rRLANDTACQc2s4u1) //slot 4
{
 in1: rNP6c22/out8;
 in2: rNP6c22/out7;
 in3: rNP6c22/out6;
 in4: rNP6c22/out5;
 in5: rNP6c22/out4;
 in6: rNP6c22/out3;
 in7: rNP6c22/out2;
 in8: rNP6c22/out1;
   
 in9:  rNP6c23/out8;
 in10: rNP6c23/out7;
 in11: rNP6c23/out6;
 in12: rNP6c23/out5;
 in13: rNP6c23/out4;
 in14: rNP6c23/out3;
 in15: rNP6c23/out2;
 in16: rNP6c23/out1;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}
LANDFEE2(rRLANDTACQc2s5u1) //slot 5
{
 in1: rNP6c24/out8;
 in2: rNP6c24/out7;
 in3: rNP6c24/out6;
 in4: rNP6c24/out5; 
 in5: rNP6c24/out4;
 in6: rNP6c24/out3;
 in7: rNP6c24/out2;
 in8: rNP6c24/out1;
  
 in9:  rNP6c25/out8;
 in10: rNP6c25/out7;
 in11: rNP6c25/out6;
 in12: rNP6c25/out5;
 in13: rNP6c25/out4;
 in14: rNP6c25/out3;
 in15: rNP6c25/out2;
 in16: rNP6c25/out1;  

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}
 LANDFEE2(rRLANDTACQc2s6u1)   // slot 6
{
 in1: rNP7c21/out8;
 in2: rNP7c21/out7;
 in3: rNP7c21/out6;
 in4: rNP7c21/out5;
 in5: rNP7c21/out4;
 in6: rNP7c21/out3;
 in7: rNP7c21/out2;
 in8: rNP7c21/out1;
   
 in9:  rNP7c22/out8;
 in10: rNP7c22/out7;
 in11: rNP7c22/out6;
 in12: rNP7c22/out5;
 in13: rNP7c22/out4;
 in14: rNP7c22/out3;
 in15: rNP7c22/out2;
 in16: rNP7c22/out1;
  //gtb_out1_16: r54c1s15/gtb;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}

LANDFEE2(rRLANDTACQc2s7u1)  // slot 7
{
 in1: rNP7c23/out8;
 in2: rNP7c23/out7;
 in3: rNP7c23/out6;
 in4: rNP7c23/out5;
 in5: rNP7c23/out4;
 in6: rNP7c23/out3;
 in7: rNP7c23/out2;
 in8: rNP7c23/out1;
   
 in9:  rNP7c24/out8;
 in10: rNP7c24/out7;
 in11: rNP7c24/out6;
 in12: rNP7c24/out5;
 in13: rNP7c24/out4;
 in14: rNP7c24/out3;
 in15: rNP7c24/out2;
 in16: rNP7c24/out1;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}
LANDFEE2(rRLANDTACQc2s8u1)  //slot 8
{
 in1: rNP7c25/out8;
 in2: rNP7c25/out7;
 in3: rNP7c25/out6;
 in4: rNP7c25/out5;
 in5: rNP7c25/out4;
 in6: rNP7c25/out3;
 in7: rNP7c25/out2;
 in8: rNP7c25/out1;
   
 in9:  rNP8c21/out8;
 in10: rNP8c21/out7;
 in11: rNP8c21/out6;
 in12: rNP8c21/out5;
 in13: rNP8c21/out4;
 in14: rNP8c21/out3;
 in15: rNP8c21/out2;
 in16: rNP8c21/out1;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}

LANDFEE2(rRLANDTACQc2s9u1) //slot 9
{
 in1: rNP8c22/out8;
 in2: rNP8c22/out7;
 in3: rNP8c22/out6;
 in4: rNP8c22/out5;
 in5: rNP8c22/out4;
 in6: rNP8c22/out3;
 in7: rNP8c22/out2;
 in8: rNP8c22/out1;
   
 in9:  rNP8c23/out8;
 in10: rNP8c23/out7;
 in11: rNP8c23/out6;
 in12: rNP8c23/out5;
 in13: rNP8c23/out4;
 in14: rNP8c23/out3;
 in15: rNP8c23/out2;
 in16: rNP8c23/out1;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}
LANDFEE2(rRLANDTACQc2s10u1) //slot 10
{
 in1: rNP8c24/out8;
 in2: rNP8c24/out7;
 in3: rNP8c24/out6;
 in4: rNP8c24/out5; 
 in5: rNP8c24/out4;
 in6: rNP8c24/out3;
 in7: rNP8c24/out2;
 in8: rNP8c24/out1;
  
 in9:  rNP8c25/out8;
 in10: rNP8c25/out7;
 in11: rNP8c25/out6;
 in12: rNP8c25/out5;
 in13: rNP8c25/out4;
 in14: rNP8c25/out3;
 in15: rNP8c25/out2;
 in16: rNP8c25/out1;  

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}


// Triplex  part still needs to be fixed
//define TRIPLEX2_TACQ_QDC2_TACQUILA3(rcs,gtbmaster,gtbslave,upj,downj5,downj7,qserial,tserial) 

// TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc2s10,r54c1s15/gtb2,.s9/gtb_master,.s8u2/j7,,,"TQDCc2t10","TTDCc2t10");
// TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc2s9,.s10/gtb_slave,.s8/gtb_master,.s8u2/j5,,,"TQDCc2t9","TTDCc2t9");
// TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc2s8,.s9/gtb_slave,.s7/gtb_master,.s4u2/j5,.s9u2/j6,.s10u2/j6,"TQDCc2t8","TTDCc2t8");
// TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc2s7,.s8/gtb_slave,.s6/gtb_master,.s5u2/j7,,,"TQDCc2t7","TTDCc2t7");
// TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc2s6,.s7/gtb_slave,.s5/gtb_master,.s5u2/j5,,,"TQDCc2t6","TTDCc2t6");
// TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc2s5,.s6/gtb_slave,.s4/gtb_master,.s4u2/j7,.s6u2/j6,.s7u2/j6,"TQDCc2t5","TTDCc2t5");
// TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc2s4,.s5/gtb_slave,.s3/gtb_master,.s1u2/j5,.s8u2/j6,.s5u2/j6,"TQDCc2t4","TTDCc2t4");
// TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc2s3,.s4/gtb_slave,.s2/gtb_master,.s2u2/j5,,,"TQDCc2t3","TTDCc2t3");
// TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc2s2,.s3/gtb_slave,.s1/gtb_master,.s1u2/j7,.s3u2/j6,,"TQDCc2t2","TTDCc2t2");
// TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc2s1,.s2/gtb_slave,,rRLANDTACQc1s2u2/j5,.s4u2/j6,.s2u2/j6,"TQDCc2t1","TTDCc2t1");

TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc2s10,r54c1s15/gtb2,.s9/gtb_master,.s8u2/j7,,,"SAM6_GTB1_TAC10","SAM6_GTB1_TAC10");
TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc2s9,.s10/gtb_slave,.s8/gtb_master,.s8u2/j5,,,"SAM6_GTB1_TAC09","SAM6_GTB1_TAC09");
TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc2s8,.s9/gtb_slave,.s7/gtb_master,.s4u2/j5,.s9u2/j6,.s10u2/j6,"SAM6_GTB1_TAC08","SAM6_GTB1_TAC08");
TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc2s7,.s8/gtb_slave,.s6/gtb_master,.s5u2/j7,,,"SAM6_GTB1_TAC07","SAM6_GTB1_TAC07");
TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc2s6,.s7/gtb_slave,.s5/gtb_master,.s5u2/j5,,,"SAM6_GTB1_TAC06","SAM6_GTB1_TAC06");
TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc2s5,.s6/gtb_slave,.s4/gtb_master,.s4u2/j7,.s6u2/j6,.s7u2/j6,"SAM6_GTB1_TAC05","SAM6_GTB1_TAC05");
TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc2s4,.s5/gtb_slave,.s3/gtb_master,.s1u2/j5,.s8u2/j6,.s5u2/j6,"SAM6_GTB1_TAC04","SAM6_GTB1_TAC04");
TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc2s3,.s4/gtb_slave,.s2/gtb_master,.s2u2/j5,,,"SAM6_GTB1_TAC03","SAM6_GTB1_TAC03");
TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc2s2,.s3/gtb_slave,.s1/gtb_master,.s1u2/j7,.s3u2/j6,,"SAM6_GTB1_TAC02","SAM6_GTB1_TAC02");
TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc2s1,.s2/gtb_slave,,rRLANDTACQc1s2u2/j5,.s4u2/j6,.s2u2/j6,"SAM6_GTB1_TAC01","SAM6_GTB1_TAC01");



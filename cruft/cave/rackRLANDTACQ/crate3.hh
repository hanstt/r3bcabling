/*
 * TACQUILA BOARDS LAND. SIGNAL from detector
 *
 * adapted after new cabling of LAND in March 2012
 *
 */

LANDFEE2(rRLANDTACQc3s1u1)   // slot 1
{
 in1: rNP9c21/out8;
 in2: rNP9c21/out7;
 in3: rNP9c21/out6;
 in4: rNP9c21/out5;
 in5: rNP9c21/out4;
 in6: rNP9c21/out3;
 in7: rNP9c21/out2;
 in8: rNP9c21/out1;
   
 in9:  rNP9c22/out8;
 in10: rNP9c22/out7;
 in11: rNP9c22/out6;
 in12: rNP9c22/out5;
 in13: rNP9c22/out4;
 in14: rNP9c22/out3;
 in15: rNP9c22/out2;
 in16: rNP9c22/out1;
  //gtb_out1_16: r54c1s15/gtb;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}

LANDFEE2(rRLANDTACQc3s2u1)  // slot 2
{
 in1: rNP9c23/out8;
 in2: rNP9c23/out7;
 in3: rNP9c23/out6;
 in4: rNP9c23/out5;
 in5: rNP9c23/out4;
 in6: rNP9c23/out3;
 in7: rNP9c23/out2;
 in8: rNP9c23/out1;
   
 in9:  rNP9c24/out8;
 in10: rNP9c24/out7;
 in11: rNP9c24/out6;
 in12: rNP9c24/out5;
 in13: rNP9c24/out4;
 in14: rNP9c24/out3;
 in15: rNP9c24/out2;
 in16: rNP9c24/out1;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}
LANDFEE2(rRLANDTACQc3s3u1)  //slot 3
{
 in1: rNP9c25/out8;
 in2: rNP9c25/out7;
 in3: rNP9c25/out6;
 in4: rNP9c25/out5;
 in5: rNP9c25/out4;
 in6: rNP9c25/out3;
 in7: rNP9c25/out2;
 in8: rNP9c25/out1;
   
 in9:  rNP10c21/out8;
 in10: rNP10c21/out7;
 in11: rNP10c21/out6;
 in12: rNP10c21/out5;
 in13: rNP10c21/out4;
 in14: rNP10c21/out3;
 in15: rNP10c21/out2;
 in16: rNP10c21/out1;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}

LANDFEE2(rRLANDTACQc3s4u1) //slot 4
{
 in1: rNP10c22/out8;
 in2: rNP10c22/out7;
 in3: rNP10c22/out6;
 in4: rNP10c22/out5;
 in5: rNP10c22/out4;
 in6: rNP10c22/out3;
 in7: rNP10c22/out2;
 in8: rNP10c22/out1;
   
 in9:  rNP10c23/out8;
 in10: rNP10c23/out7;
 in11: rNP10c23/out6;
 in12: rNP10c23/out5;
 in13: rNP10c23/out4;
 in14: rNP10c23/out3;
 in15: rNP10c23/out2;
 in16: rNP10c23/out1;

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}
LANDFEE2(rRLANDTACQc3s5u1) //slot 5
{
 in1: rNP10c24/out8;
 in2: rNP10c24/out7;
 in3: rNP10c24/out6;
 in4: rNP10c24/out5; 
 in5: rNP10c24/out4;
 in6: rNP10c24/out3;
 in7: rNP10c24/out2;
 in8: rNP10c24/out1;
  
 in9:  rNP10c25/out8;
 in10: rNP10c25/out7;
 in11: rNP10c25/out6;
 in12: rNP10c25/out5;
 in13: rNP10c25/out4;
 in14: rNP10c25/out3;
 in15: rNP10c25/out2;
 in16: rNP10c25/out1;  

 t1_16: .s/in1_16;
 e1_16: .u2/in1_16;
}
LANDFEE2(rRLANDTACQc3s6u1) //slot 6  VETO not yet documented
{
;
}
LANDFEE2(rRLANDTACQc3s7u1) //slot 7  VETO not yet documented
{
;
}
LANDFEE2(rRLANDTACQc3s8u1) //slot 8  VETO (in1-8) not yet documented, in9-16 spare
{
;
}
LANDFEE2(rRLANDTACQc3s9u1) //slot 9  spare tacquila module
{
;
}
LANDFEE2(rRLANDTACQc3s10u1) //slot 10  spare tacquila module
{
;
}

// Triplex part still needs to be fixed

// TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc3s5,r54c1s13/gtb1,.s4/gtb_master,.s1u2/j5,,,"TQDCc3t5","TTDCc3t5");
// TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc3s4,.s5/gtb_slave,.s3/gtb_master,.s2u2/j7,,,"TQDCc3t4","TTDCc3t4");
// TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc3s3,.s4/gtb_slave,.s2/gtb_master,.s2u2/j5,,,"TQDCc3t3","TTDCc3t3");
// TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc3s2,.s3/gtb_slave,.s1/gtb_master,.s1u2/j7,.s3u2/j6,.s4u2/j6,"TQDCc3t2","TTDCc3t2");
// TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc3s1,.s2/gtb_slave,,rRLANDTACQc1s2u2/j7,.s5u2/j6,.s2u2/j6,"TQDCc3t1","TTDCc3t1");

TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc3s5,r54c1s13/gtb1,.s4/gtb_master,.s1u2/j5,,,"SAM5_GTB0_TAC05","SAM5_GTB0_TAC05");
TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc3s4,.s5/gtb_slave,.s3/gtb_master,.s2u2/j7,,,"SAM5_GTB0_TAC04","SAM5_GTB0_TAC04");
TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc3s3,.s4/gtb_slave,.s2/gtb_master,.s2u2/j5,,,"SAM5_GTB0_TAC03","SAM5_GTB0_TAC03");
TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc3s2,.s3/gtb_slave,.s1/gtb_master,.s1u2/j7,.s3u2/j6,.s4u2/j6,"SAM5_GTB0_TAC02","SAM5_GTB0_TAC02");
TRIPLEX2_TACQ_QDC2_TACQUILA3(rRLANDTACQc3s1,.s2/gtb_slave,,rRLANDTACQc1s2u2/j7,.s5u2/j6,.s2u2/j6,"SAM5_GTB0_TAC01","SAM5_GTB0_TAC01");

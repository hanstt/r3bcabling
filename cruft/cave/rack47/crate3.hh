/*
 * Crate 3,r47
 * VME Crate with qdc and triva 
 * 
 */



VME_CRATE(r47c3)
{
  SETTING("CRATE_NO" => "77");
}

RIO3(r47c3s1)
{
SETTING("HOSTNAME"=>"R3-15");
}

TRIVA5(r47c3s2)
{
tribu1: rMASTERc2s2/tribu2;
tribu2: r52c1s2/tribu1;
}



CAEN_V965(r47c3s4)
{
SETTING("ADDRESS" => "0x00020000");



in0_7: r47c1s5/e1_8; // all with 50 ns delay
in8_15: r47c1s5/e9_16; // no delay on this
gate_in: rMASTERc3s8u5/out2; 
}


CAEN_V775(r47c3s5)
{
SETTING("ADDRESS" => "0x00000000");
in0_15: r47c2s18/out1_16; 
com_in: rMASTERc3s6u1/out8; 
}




CAEN_V862(r47c3s7)
{
SETTING("ADDRESS" => "0x00040000");

ina0_7: r47c4s1/outa1_8;    //doppelkabel: 2x 16 channel ECL auf 1 16 channel ECL. Unteres a,Oberes b
ina8_15: r47c4s3/outa1_8;
inb0_5: r47c1s18/e1_6;  // mit paddlecard
inb6: r47c1s23/e1;
inb7: r47c1s18/e8;
inb8_15: r47c1s18/e9_16;
ina16_23: r47c4s5/outa1_8;
ina24_31: r47c4s7/outa1_8;
inb16_23: r47c1s21/e1_8;
inb24_31: r47c1s21/e9_16;
gate_in: rMASTERc3s8u4/out2; 

}

CAEN_V775(r47c3s8)
{
SETTING("ADDRESS" => "0x00000000");

in0_15: r47c2s17/out1_16;
in16_31: r47c2s19/out1_16;
com_in: rMASTERc3s6u1/out2;// 100 ns delayed 
//com_out: r47c3s13/com_in;
}



//in r47c3s10_ ADC von LaBr3



CAEN_V830(r47c3s12)
{

SETTING("ADDRESS" => "0x00600000");
   SERIAL("CAEN0098"); //VME_SCALER_1
   SETTING("VIRTUAL_SLOT" => "30");

in0_7: r47c4s19u2/out_ecl0_7;
in8_15: r47c4s19u1/out_ecl0_7;
trig_in: rMASTERc3s6u1/out4; 
}


//CAEN_V775(r47c3s13)
//{
//com_in: r47c3s8/com_out;
// LaBR?
//}



/* 
 * Crate 2
 * First Crate in the rack, CAMAC with CDF for LENA and POS/ROLU
 */

CAMAC_CRATE(r47c2)
{
;
}
CF8101(r47c2s1)
{
  SERIAL("Number?"); // TODO: check
  in1_8: r47c1s5/t1_8;
  mux_tb: r47c2s21/in1a;
  mux_e: r47c2s21/in3a;
  mux_mon: r47c2s21/in5a;
  test: r47c2s14/out6;
  ta1_8: r47c4s19u1/in_ecl0_7;
  tb1_8:r47c2s18/in1_8;
  
   
}


CF8101(r47c2s2)
{
  SERIAL("Number?"); // TODO: check
  in1_8: r47c1s5/t9_16;
  mux_tb: r47c2s21/in1b;
  mux_e: r47c2s21/in3b;
  mux_mon: r47c2s21/in5b;
  test: r47c2s14/out5;
  ta1_8: r47c4s19u2/in_ecl0_7;
  tb1_8:r47c2s18/in9_16;
    
}


CF8101(r47c2s4)
{
  SERIAL("Number?"); // TODO: check
  

  in1: r47c1s8/t1;
  in2: r47c1s8/t9;
  in3: r47c1s8/t2;
  in4: r47c1s8/t10;
  in5: r47c1s8/t3;
  in6: r47c1s8/t11;
  in7: r47c1s8/t4;
  in8: r47c1s8/t12;
  
  sum: rMASTERc3s7/in3; // SU kommt aus dem masterrack-crate raus, wohl in c4
  mux_tb: r47c2s21/in1c;
  mux_e: r47c2s21/in3c;
  mux_mon: r47c2s21/in5c;
  test: r47c2s14/out3;
  ta1_8: r47c4s1/in1_8;  //last crate in the rack
  tb1_8:r47c2s17/in1_8; 
    
}



CF8101(r47c2s6)
{

in1: r47c1s8/t5;
in2: r47c1s8/t13;
in3: r47c1s8/t6;
in4: r47c1s8/t14;
in5: r47c1s8/t7;
in6: r47c1s8/t15;
in7: r47c1s8/t8;
in8: r47c1s8/t16;

sum: rMASTERc3s7/in4; // SU kommt aus dem masterrack-crate raus, wohl in c4
mux_tb: r47c2s21/in1d;
mux_e:  r47c2s21/in3d;
mux_mon: r47c2s21/in5d;
test:  r47c2s14/out4;
ta1_8: r47c4s3/in1_8;
tb1_8: r47c2s17/in9_16;


}

CF8101(r47c2s8)
{
in1: r47c1s12/t1;
in2: r47c1s14/t1;
in3: r47c1s12/t2;
in4: r47c1s14/t2;
in5: r47c1s12/t3;
in6: r47c1s14/t3;
in7: r47c1s12/t4;
in8: r47c1s14/t4;

sum: rMASTERc3s7/in2; // SU kommt aus dem masterrack-crate raus, wohl in c4
mux_tb: r47c2s21/in2a;
mux_e:  r47c2s21/in4a;
mux_mon: r47c2s21/in6a;
test:  r47c2s14/out2;
ta1_8: r47c4s5/in1_8;
tb1_8: r47c2s19/in1_8;


}

CF8101(r47c2s10)
{
in1: r47c1s12/t5;
in2: r47c1s14/t5;
in3: r47c1s12/t6;
in4: r47c1s14/t6;
in5: r47c1s12/t7;
in6: r47c1s14/t7;
in7: r47c1s12/t8;
in8: r47c1s14/t8;

sum: rMASTERc3s7/in1; // SU kommt aus dem masterrack-crate raus, wohl in c4
mux_tb: r47c2s21/in2b;
mux_e:  r47c2s21/in4b;
mux_mon: r47c2s21/in6b;
test:  r47c2s14/out1;
ta1_8: r47c4s7/in1_8;
tb1_8: r47c2s19/in9_16;


}


LIFO(r47c2s14)
{
in: rMASTERc3s6u2/out2; 
out1: r47c2s10/test; 
out2: r47c2s8/test;
out3: r47c2s4/test;
out4: r47c2s6/test;

out5: r47c2s2/test;
out6: r47c2s1/test;
}

RMX3(r47c2s15)
{
// empty
;
}


DL1610(r47c2s17) // just 2 slots for output. no a or b like in the module list...
{

in1_8: r47c2s4/tb1_8;
in9_16: r47c2s6/tb1_8;
out1_16: r47c3s8/in0_15; 

}

DL1610(r47c2s18)
{

in1_8: r47c2s1/tb1_8;
in9_16: r47c2s2/tb1_8;
out1_16: r47c3s5/in0_15; 

}

DL1610(r47c2s19)
{

in1_8: r47c2s8/tb1_8;
in9_16: r47c2s10/tb1_8;
out1_16: r47c3s8/in16_31; 

}

RMX3(r47c2s21)
{
// die ersten 4 Eing�nge scheinen zu Ausgang 1 zu geh�ren (T), die n�chsten zu
// 2(E) etc...->Modulbeschreibung RMX. F�r alle CFDs �bernehmen. 



in1a: r47c2s1/mux_tb;
in1b: r47c2s2/mux_tb;

in1c: r47c2s4/mux_tb;
in1d: r47c2s6/mux_tb;
in2a: r47c2s8/mux_tb;
in2b: r47c2s10/mux_tb;

in3a: r47c2s1/mux_e;
in3b: r47c2s2/mux_e;
in3c: r47c2s4/mux_e;
in3d: r47c2s6/mux_e;
in4a: r47c2s8/mux_e;
in4b: r47c2s10/mux_e;

in5a: r47c2s1/mux_mon;
in5b: r47c2s2/mux_mon;
in5c: r47c2s4/mux_mon;
in5d: r47c2s6/mux_mon;
in6a: r47c2s8/mux_mon;
in6b: r47c2s10/mux_mon;

out1: r47c2s22/in1a;
out2: r47c2s22/in1b;

out3: r47c2s22/in2a;
out4: r47c2s22/in2b;

out5: r47c2s22/in2d;
out6: r47c2s22/in3a;


}

RMX3(r47c2s22)
{
in1a: r47c2s21/out1;
in1b: r47c2s21/out2;

in2a: r47c2s21/out3;
in2b: r47c2s21/out4;

in2d: r47c2s21/out5;
in3a: r47c2s21/out6;


//out1_3: somewhere out ?!
  }
  
  
CBV1000(r47c2s24)
{
SETTING("CRATE_NO" => "11");

//CRATE_NO: 2;
vsb_in: rMASTERc4s3/vsb_out; // controller
vsb_out: r52c3s24/vsb_in; // slot25  for controller, goes to different rack
}  
DMS1000(r47c2s25)
{
  ;
}
 

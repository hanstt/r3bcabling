/* 
 * Crate 1,r47
 * on top of the rack, splitboxes + split-delayboxes
 * 
 * in1 is always on the left
 */

//Taken out by FW for s412 - maybe renaming will do the job...
//
//SPLIT_BOX(r47c1s5) // POS und ROLU: Signale werden vorher sortiert. PSO und ROLU
//		   //in beam.hh
//{
//in1:  rPOS1c1s1u1/signal;
//in2:  rPOS1c1s1u3/signal;
//in3:  rPOS2c1s1u1/signal;
//in4:  rPOS2c1s1u3/signal;
//in5:  rROLU1c1s1u1/signal;
//in6:  rROLU1c1s1u3/signal;
//in7:  rROLU2c1s1u1/signal; 
//in8:  rROLU2c1s1u3/signal;
//in9:  rPOS1c1s1u2/signal;
//in10: rPOS1c1s1u4/signal;
//in11: rPOS2c1s1u2/signal;
//in12: rPOS2c1s1u4/signal;
//in13: rROLU1c1s1u2/signal;
//in14: rROLU1c1s1u4/signal;
//in15: rROLU2c1s1u2/signal;
//in16: rROLU2c1s1u4/signal;
//
//e1_8: r47c3s4/in0_7;
////Before QDC there is a 50 ns delay for every channel e1_8 -> to be included
//
//e9_16: r47c3s4/in8_15; 
//t1_8: r47c2s1/in1_8;
//t9_16: r47c2s2/in1_8;
//
//}


/* The following LENA Channels are amplified: amp-rack/crate does not exist anymore:
  
1u   2100  no channel
1d   1730  ATOMKI-2
2u   1960  GUF-12
2d   1730  GUF-11
3u   1720  ATOMKI-3
3d   1850  ATOMKI-4
4u   1775  ATMOKI-5
4d   1775  ATOMKI-6
5u   1740  ATOMKI-7
5d   1750  ATOMKI-8
6u   1860  ATOMKI-9
6d   1800  ATOMKI-10
7u   1775  ATOMKI-11
7d   1800  ATOMKI-12
11u  1800  GUF-1
11d  1910  GUF-2
12u  1960  GUF-3
12d  1865  GUF-4
13u  2010  GUF-5
13d  1825  GUF-6
14u  1710  GUF-7
14d  1750  GUF-8
15u  2025  GUF-9
15d  2050  GUF-10
After ampflification they go to the splitboxes as indicated below
*/

//Taken out by FW for s412 - LENA is not in place anymore
//SPLIT_BOX_PASSIVE(r47c1s8)
//{
// in1: rLENAc1s1/signal;
// in2: rLENAc2s1/signal; 
// in3: rLENAc3s1/signal;
// in4: rLENAc4s1/signal;
// in5: rLENAc5s1/signal;
// in6: rLENAc1s2/signal;
// in7: rLENAc2s2/signal;
// in8: rLENAc3s2/signal;
// in9: rLENAc4s2/signal;
// in10:rLENAc5s2/signal;
// in11:rLENAc6s1/signal;
// in12:rLENAc7s1/signal;
// in13:rLENAc8s1/signal;
// in14:rLENAc9s1/signal;
// in15:rLENAc10s1/signal;
// in16:rLENAc6s2/signal;     
// 
// e1: r47c1s18/in1;
// t1: r47c2s4/in1;
// e2: r47c1s18/in2;
// t2: r47c2s4/in3;
// e3: r47c1s18/in3;
// t3: r47c2s4/in5;
// e4: r47c1s23/in1;
// t4: r47c2s4/in7;
// e5: r47c1s18/in5;
// t5: r47c2s6/in1;
// e6: r47c1s18/in6;
// t6: r47c2s6/in3;
// e7: r47c1s18/in7;
// t7: r47c2s6/in5;
// e8: r47c1s18/in8;
// t8: r47c2s6/in7;
// e9: r47c1s18/in9;
// t9: r47c2s4/in2;
// e10: r47c1s18/in10;
// t10: r47c2s4/in4;
// e11: r47c1s18/in11;
// t11: r47c2s4/in6;
// e12: r47c1s18/in12;
// t12: r47c2s4/in8;
// e13: r47c1s18/in13;
// t13: r47c2s6/in2;
// e14: r47c1s18/in14;
// t14: r47c2s6/in4;
// e15: r47c1s18/in15;
// t15: r47c2s6/in6;
// e16: r47c1s18/in16;
// t16: r47c2s6/in8;
//}
//
//SPLIT_BOX_PASSIVE(r47c1s12)
//{
// in1:rLENAc7s2/signal;
// in2:rLENAc8s2/signal;
// in3:rLENAc9s2/signal;
// in4:rLENAc10s2/signal;
// in5:rLENAc11s1/signal;
// in6:rLENAc12s1/signal;
// in7:rLENAc13s1/signal;     
// in8:rLENAc14s1/signal;     
//  
// e1: r47c1s21/in1;
// t1: r47c2s8/in1;
// e2: r47c1s21/in2;
// t2: r47c2s8/in3;
// e3: r47c1s21/in3;
// t3: r47c2s8/in5;
// e4: r47c1s21/in4;
// t4: r47c2s8/in7;
// e5: r47c1s21/in5;
// t5: r47c2s10/in1;
// e6: r47c1s21/in6;
// t6: r47c2s10/in3;
// e7: r47c1s21/in7;
// t7: r47c2s10/in5;
// e8: r47c1s21/in8;
// t8: r47c2s10/in7;
// }
//
//SPLIT_BOX_PASSIVE(r47c1s14)
//{
// in1:rLENAc15s1/signal;
// in2:rLENAc11s2/signal;
// in3:rLENAc12s2/signal;
// in4:rLENAc13s2/signal;
// in5:rLENAc14s2/signal;
// in6:rLENAc15s2/signal;
// in7:rLENAc16s1/signal;
// in8:rLENAc16s2/signal;
//
// e1: r47c1s21/in9;
// t1: r47c2s8/in2;
// e2: r47c1s21/in10;
// t2: r47c2s8/in4;
// e3: r47c1s21/in11;
// t3: r47c2s8/in6;
// e4: r47c1s21/in12;
// t4: r47c2s8/in8;
// e5: r47c1s21/in13;
// t5: r47c2s10/in2;  
// e6: r47c1s21/in14;
// t6: r47c2s10/in4;
// e7: r47c1s21/in15;
// t7: r47c2s10/in6;
// e8: r47c1s21/in16; //empty
// t8: r47c2s10/in8;  //empty
//}

SPLIT_BOX(r47c1s18)
{
in1_3: r47c1s8/e1_3;
in5_16: r47c1s8/e5_16;

e1_6: r47c3s7/inb0_5; // TODO: CHECK QDC INPUT (the module) for TIME UND ENERGIE
e8: r47c3s7/inb7;     // e7->in6 from other splitbox
e9_16: r47c3s7/inb8_15;
}

SPLIT_BOX(r47c1s21)
{
in1_8: r47c1s12/e1_8;
in9_16: r47c1s14/e1_8;

e1_8:   r47c3s7/inb16_23; // TODO: CHECK QDC INPUT (the module) for TIME UND ENERGIE
e9_16: r47c3s7/inb24_31;

}

SPLIT_BOX(r47c1s23)
{
in1: r47c1s8/e4;
e1:r47c3s7/inb6; // TODO: CHECK QDC INPUT (the module) for TIME UND ENERGIE
}		// FIND OUT: What are the INPUT names

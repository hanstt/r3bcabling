/*
 * Crate 4
 * 
 * NIM Crate with delays and SU
 * 
 */
 
NIM_CRATE(r47c4)
{
;
}
 
DL8010(r47c4s1)
{
SETTING("DELAY" => "400ns");
in1_8: r47c2s4/ta1_8;
outa1_8: r47c3s7/ina0_7;
}

DL8010(r47c4s3)
{
SETTING("DELAY" => "400ns");
in1_8: r47c2s6/ta1_8;
outa1_8: r47c3s7/ina8_15;
}


DL8010(r47c4s5)
{
SETTING("DELAY" => "400ns");
in1_8: r47c2s8/ta1_8;
outa1_8: r47c3s7/ina16_23;
}


DL8010(r47c4s7)
{
SETTING("DELAY" => "400ns");
in1_8: r47c2s10/ta1_8;
outa1_8: r47c3s7/ina24_31;
}



GG8000(r47c4s9)
{
;
}

GG8000_U(r47c4s9u3)
{
in: r47c4s9u6/out1; // zweiotes out zu u6-> neg?
out2: r47c4s11/in3;                  //
}

GG8000_U(r47c4s9u5)
{
in: rMASTERc3s12u8/outA; // check: cf 8000nim
out1: r47c4s9u6/in;  

}

GG8000_U(r47c4s9u6)
{
in: r47c4s9u5/out1;
out1: r47c4s9u3/in;  
}


EC1610(r47c4s11)
{
in3: r47c4s9u3/out2; // welches out genau? 1,2, neg?

}

LECROY429A(r47c4s15)
{
;
}
LECROY429A_8_U(r47c4s15u1)
{
//inputs 1:ol 2or 3ul 4ur

in1: r47c4s19u2/outr7; // caen n638 ecl nim converter
in2: r47c4s19u2/outr6;
in3: r47c4s19u2/outr5;
in4: r47c4s19u2/outr4;

//abschnitt 2, In 5-8, selbe reihenfolge
in5: r47c4s19u2/outr3;
in6: r47c4s19u2/outr2;
in7: r47c4s19u2/outr1;
in8: r47c4s19u2/outr0;

out8: r47c4s17u3/veto;


}

PS756(r47c4s17)
{
;
}

PS756_U(r47c4s17u3)
{
//Abschnitt 3    // regulierung �ber UNIT=4 units pro modul. 
SETTING ("LEVEL" => "1");

inA: r47c4s17u4/out3; //andere unit, ein abschnitt tiefer
veto: r47c4s15u1/out8;
out3: rMASTERc3s2/in1;
}
PS756_U(r47c4s17u4)
{
SETTING ("LEVEL" => "2");
inA: r47c4s19u1/outr0; //  modul in slot 19 hat links und rechts je 2 x 4inputs. ist rechtes der beiden
//linken
inB:  r47c4s19u1/outr1;
inC: r47c4s19u1/outr2;
inD: r47c4s19u1/outr3;

out2: rMASTERc3s3/in1;
out3: r47c4s17u3/inA; 
out4: rMASTERc3s2/in12;

}


N638(r47c4s19)
{
;
}


N638_U(r47c4s19u1)
{



//ECLS, LEMO siehe oben
in_ecl0_7: r47c2s1/ta1_8;
out_ecl0_7: r47c3s12/in8_15;

outr0: r47c4s17u4/inA;
outr1: r47c4s17u4/inB;
outr2: r47c4s17u4/inC;
outr3: r47c4s17u4/inD;
}

N638_U(r47c4s19u2)
{
//inputs 1:ol 2or 3ul 4ur

outr0: r47c4s15u1/in8;
outr1: r47c4s15u1/in7; // caen n638 ecl nim converter
outr2: r47c4s15u1/in6;
outr3: r47c4s15u1/in5;
outr4: r47c4s15u1/in4;
outr5: r47c4s15u1/in3;
outr6: r47c4s15u1/in2;
outr7: r47c4s15u1/in1;

in_ecl0_7: r47c2s2/ta1_8;
out_ecl0_7: r47c3s12/in0_7;

}


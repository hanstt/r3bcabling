#!/bin/bash

# This file is no longer used!  See land/plane_X.hh
#
# LAND planes are now treated like normal sources.

echo "NO LONGER IN USE!"

exit 1

function add_replacement
{
    # $1 is the tube name (unchanged)
    # $2 is the connector it went to (should have if scheme followed)
    # $3        channel
    # $4 is the connector it goes to (actual)
    # $5        channel
    # $6 is the tube that used to connector originally

    REPLACEMENTS="$REPLACEMENTS -e s/\(DETECTOR_LAND.*$1.*\)$2\/$3/\\1$4\/$5/"

    # This is so ugly that there are no words for it.  We require one space
    # after the colon, and put two in.  Just to avoid replacing back...
    # I did not write this.  I will deny to have had anything to do with this!

    REPLACEMENTS="$REPLACEMENTS -e s/\($5\:\(.\)\)$6/\\1\\2$1/"

    echo "Replacing: $1 -> $2/$3  now instead -> $4/$5 (was used by $6)"

# in5: rNP1c02s2/signal; /*SHARE 1*/

}

for plane in 1 2 3 4 5 6 7 8 9 10
do
  rack=`expr $plane`
  if [[ $plane == 10 ]] ; 
    then PLANE=`expr $plane` ;
    else PLANE=0`expr $plane` ;
  fi

  HVREPL0='r16c1s0\/out0_15'
  HVREPL1='r16c1s1\/out0_15'
  HVREPL2='r16c1s2\/out0_7'

  JNREPL0='r13c1s1'
  JNREPL1='r13c1s2'
  JNREPL2='out5_8\:\ r13c1s3\/in5_8'
  JNREPL3='out5_8\:\ r13c1s3\/in13_16'

  REPLSHARE="s/in\(.\)\(.*\)\/\*SHARE \(.\)\*\//in\\3\\2/g"

  REPLACEMENTS='';

  case $plane in
    1)  HVREPLTO0='r16c1s0\/out0_15'
        HVREPLTO1='r16c1s1\/out0_15'
        HVREPLTO2='r16c1s2\/out0_7'
	JNREPLTO0='r13c1s1'
	JNREPLTO1='r13c1s2'
	JNREPLTO2='out5_8\:\ r13c1s3\/in5_8'
	JNREPLTO3='out5_8\:\ r13c1s3\/in13_16'
	REPLSHARE='s/a/a/';
	add_replacement 'rNP1c05s2' 'rNP1c26' 'in7' 'rNP1c26' 'in5' 'rNP1c06s2'
	add_replacement 'rNP1c06s2' 'rNP1c26' 'in5' 'rNP1c26' 'in7' 'rNP1c05s2'
        ;;
    2)  HVREPLTO0='r16c1s3\/out0_15'
        HVREPLTO1='r16c1s4\/out0_15'
        HVREPLTO2='r16c1s2\/out8_15'
	JNREPLTO0='r13c1s4'
	JNREPLTO1='r13c1s5'
	JNREPLTO2='out1_4\:\ r13c1s3\/in1_4'
	JNREPLTO3='out1_4\:\ r13c1s3\/in9_12'
        ;;
    3)  HVREPLTO0='r16c1s5\/out0_15'
        HVREPLTO1='r16c1s6\/out0_15'
        HVREPLTO2='r16c1s7\/out0_7'
	JNREPLTO0='r13c1s6'
	JNREPLTO1='r13c1s7'
	JNREPLTO2='out5_8\:\ r13c1s8\/in5_8'
	JNREPLTO3='out5_8\:\ r13c1s8\/in13_16'
	REPLSHARE='s/a/a/';
	add_replacement 'rNP3c05s2' 'rNP3c26' 'in7' 'rNP3c26' 'in5' 'rNP3c06s2'
	add_replacement 'rNP3c06s2' 'rNP3c26' 'in5' 'rNP3c26' 'in7' 'rNP3c05s2'
        ;;
    4)  HVREPLTO0='r16c1s8\/out0_15'
        HVREPLTO1='r16c1s9\/out0_15'
        HVREPLTO2='r16c1s7\/out8_15'
	JNREPLTO0='r13c1s9'
	JNREPLTO1='r13c1s10'
	JNREPLTO2='out1_4\:\ r13c1s8\/in1_4'
	JNREPLTO3='out1_4\:\ r13c1s8\/in9_12'
        ;;
    5)  HVREPLTO0='r16c1s10\/out0_15'
        HVREPLTO1='r16c1s11\/out0_15'
        HVREPLTO2='r16c1s12\/out0_7'
	JNREPLTO0='r13c1s11'
	JNREPLTO1='r13c1s12'
	JNREPLTO2='out5_8\:\ r13c1s13\/in5_8'
	JNREPLTO3='out5_8\:\ r13c1s13\/in13_16'
	REPLSHARE='s/a/a/';
	add_replacement 'rNP5c05s2' 'rNP5c26' 'in7' 'rNP5c26' 'in5' 'rNP5c06s2'
	add_replacement 'rNP5c06s2' 'rNP5c26' 'in5' 'rNP5c26' 'in7' 'rNP5c05s2'
        ;;
    6)  HVREPLTO0='r16c1s13\/out0_15'
        HVREPLTO1='r16c1s14\/out0_15'
        HVREPLTO2='r16c1s12\/out8_15'
	JNREPLTO0='r13c1s14'
	JNREPLTO1='r13c3s1'
	JNREPLTO2='out1_4\:\ r13c1s13\/in1_4'
	JNREPLTO3='out1_4\:\ r13c1s13\/in9_12'
        ;;
    7)  HVREPLTO0='r16c1s15\/out0_15'
        HVREPLTO1='r16c2s0\/out0_15'
        HVREPLTO2='r16c2s1\/out0_7'
	JNREPLTO0='r13c3s2'
	JNREPLTO1='r13c3s3'
	JNREPLTO2='out5_8\:\ r13c3s4\/in5_8'
	JNREPLTO3='out5_8\:\ r13c3s4\/in13_16'
	REPLSHARE='s/a/a/';
	add_replacement 'rNP7c05s2' 'rNP7c26' 'in7' 'rNP7c26' 'in5' 'rNP7c06s2'
	add_replacement 'rNP7c06s2' 'rNP7c26' 'in5' 'rNP7c26' 'in7' 'rNP7c05s2'
        ;;
    8)  HVREPLTO0='r16c2s2\/out0_15'
        HVREPLTO1='r16c2s3\/out0_15'
        HVREPLTO2='r16c2s1\/out8_15'
	JNREPLTO0='r13c3s5'
	JNREPLTO1='r13c3s6'
	JNREPLTO2='out1_4\:\ r13c3s4\/in1_4'
	JNREPLTO3='out1_4\:\ r13c3s4\/in9_12'
        ;;
    9)  HVREPLTO0='r16c2s4\/out0_15'
        HVREPLTO1='r16c2s5\/out0_15'
        HVREPLTO2='r16c2s6\/out0_7'
	JNREPLTO0='r13c3s7'
	JNREPLTO1='r13c3s8'
	JNREPLTO2='out5_8\:\ r13c3s9\/in5_8'
	JNREPLTO3='out5_8\:\ r13c3s9\/in13_16'
	REPLSHARE='s/a/a/';
	add_replacement 'rNP9c05s2' 'rNP9c26' 'in7' 'rNP9c26' 'in5' 'rNP9c06s2'
	add_replacement 'rNP9c06s2' 'rNP9c26' 'in5' 'rNP9c26' 'in7' 'rNP9c05s2'
        ;;
    10) HVREPLTO0='r16c2s7\/out0_15'
        HVREPLTO1='r16c2s8\/out0_15'
        HVREPLTO2='r16c2s6\/out8_15'
	JNREPLTO0='r13c3s10'
	JNREPLTO1='r13c3s11'
	JNREPLTO2='out1_4\:\ r13c3s9\/in1_4'
	JNREPLTO3='out1_4\:\ r13c3s9\/in9_12'
	add_replacement 'rNP10c12s1' 'rNP10c21' 'in8' 'rNP10c22' 'in8' 'rNP10c18s1'
	add_replacement 'rNP10c18s1' 'rNP10c22' 'in8' 'rNP10c21' 'in8' 'rNP10c12s1'
        ;;
  esac

  # echo ":$HVREPL0: -> :$HVREPLTO0:"
  # echo ":$HVREPL1: -> :$HVREPLTO1:"
  # echo ":$HVREPL2: -> :$HVREPLTO2:"

  # And then single tubes which have been misconnected.
  # The handling for this is in no way beautiful!  
  # (And as of yet non-existent...)
  #
  # rNP1c05s2  rNP1c26/in7 -> rNP1c26/in5 (was rNP1c06s2)  
  # rNP1c06s2  rNP1c26/in5 -> rNP1c26/in7 (was rNP1c05s2)

  # echo $REPLACEMENTS;

  echo "// Do not edit, this file is auto-generated" > gen/gen_plane_$plane.hh

  cat land_planes.hh |               \
    sed -e "$REPLSHARE" |            \
    cpp -P |                         \
    sed -e "s/rNP1/rNP$rack/g"       \
        -e "s/N01/N$PLANE/g"         \
        -e "s/$HVREPL0/$HVREPLTO0/g" \
        -e "s/$HVREPL1/$HVREPLTO1/g" \
        -e "s/$HVREPL2/$HVREPLTO2/g" \
        -e "s/$JNREPL0/$JNREPLTO0/g" \
        -e "s/$JNREPL1/$JNREPLTO1/g" \
        -e "s/$JNREPL2/$JNREPLTO2/g" \
        -e "s/$JNREPL3/$JNREPLTO3/g" \
        $REPLACEMENTS                >> \
    gen/gen_plane_$plane.hh

done




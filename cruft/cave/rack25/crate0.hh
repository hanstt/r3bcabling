// ----------- NIM CRATE for PSP ---------------------- //
// Rack 25 is a "virtual rack". It does not exist, only the crate(s) are there.

NIM_CRATE(r25c0)
{
  ;
}

MSCF16(r25c0s07)             //module 17
{

  in1:    rPSP1c4s1/e;    // PSP 1.1
  in2:    rPSP1c4s2/e;    // PSP 1.2
  in3:    rPSP1c4s3/e;    // PSP 1.3
  in4:    rPSP1c4s4/e;    // PSP 1.4
  in13:    rPSP1c4s5/e;    // PSP 1.5

  in5:    rPSP2c4s1/e;    // PSP 2.1
  in6:    rPSP2c4s2/e;    // PSP 2.2
  in7:    rPSP2c4s3/e;    // PSP 2.3
  in8:    rPSP2c4s4/e;    // PSP 2.4
  in14:   rPSP2c4s5/e;    // PSP 2.5
  
  in9:    rPSP3c4s1/e;    // PSP 3.1
  in10:    rPSP3c4s2/e;    // PSP 3.2
  in11:    rPSP3c4s3/e;    // PSP 3.3
  in12:    rPSP3c4s4/e;    // PSP 3.4
  in15:    rPSP3c4s5/e;    // PSP 3.5

  e1_16:  r23c3s20/in1_16;   // PSP MADC
}



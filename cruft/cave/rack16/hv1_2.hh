
/* 
 * The LeCroy 1440 HV mainframes.
 *
 * r16c1 r16c2 r10c3
 */


// Crate 1: LAND

LECROY1440(r16c1)
{
  ;
}

LECROY1442(r16c1s16) { ; } // Front, leftmost
LECROY1442(r16c1s17) { ; } // Front, second from left
LECROY1441(r16c1s18) { ; } // Front, third from left
LECROY1445(r16c1s19) // Front, rightmost
{
  SETTING("CRATE_NO" => "1");

 j1: r12c5s21/j0; // upstream (controller)
 j2: r16c2s19/j1; // next
}

// Planes 1 & 2

LECROY1443N(r16c1s0)  { out0_15: rNP1c31/in1_16; }
LECROY1443N(r16c1s1)  { out0_15: rNP1c33/in1_16; }
LECROY1443N(r16c1s2)  { out0_7:  rNP1c32/in1_8;  out8_15: rNP2c32/in1_8; }
LECROY1443N(r16c1s3)  { out0_15: rNP2c31/in1_16; }
LECROY1443N(r16c1s4)  { out0_15: rNP2c33/in1_16; }

// Planes 3 & 4

LECROY1443N(r16c1s5)  { out0_15: rNP3c31/in1_16; }
LECROY1443N(r16c1s6)  { out0_15: rNP3c33/in1_16; }
LECROY1443N(r16c1s7)  { out0_7:  rNP3c32/in1_8;  out8_15: rNP4c32/in1_8; }
LECROY1443N(r16c1s8)  { out0_15: rNP4c31/in1_16; }
LECROY1443N(r16c1s9)  { out0_15: rNP4c33/in1_16; }

// Planes 5 & 6

LECROY1443N(r16c1s10) { out0_15: rNP5c31/in1_16; }
LECROY1443N(r16c1s11) { out0_15: rNP5c33/in1_16; }
LECROY1443N(r16c1s12) { out0_7:  rNP5c32/in1_8;  out8_15: rNP6c32/in1_8; }
LECROY1443N(r16c1s13) { out0_15: rNP6c31/in1_16; }
LECROY1443N(r16c1s14) { out0_15: rNP6c33/in1_16; }

// Plane 7...

LECROY1443N(r16c1s15) { out0_15: rNP7c31/in1_16; }

// Crate 2: LAND (cont'd)

LECROY1440(r16c2)
{
  ;
}

LECROY1442(r16c2s16) { ; } // Front, leftmost
LECROY1442(r16c2s17) { ; } // Front, second from left
LECROY1441(r16c2s18) { ; } // Front, third from left
LECROY1445(r16c2s19) // Front, rightmost
{
  SETTING("CRATE_NO" => "2");

 j1: r16c1s19/j2; // upstream (controller)
 j2: r10c3s19/j1; // next
}

// Planes 7 (cont'd) & 8

LECROY1443N(r16c2s0)  { out0_15: rNP7c33/in1_16; }
LECROY1443N(r16c2s1)  { out0_7:  rNP7c32/in1_8;  out8_15: rNP8c32/in1_8; }
LECROY1443N(r16c2s2)  { out0_15: rNP8c31/in1_16; }
LECROY1443N(r16c2s3)  { out0_15: rNP8c33/in1_16; }

// Planes 9 & 10

LECROY1443N(r16c2s4)  { out0_15: rNP9c31/in1_16; }
LECROY1443N(r16c2s5)  { out0_15: rNP9c33/in1_16; }
LECROY1443N(r16c2s6)  { out0_7:  rNP9c32/in1_8;  out8_15: rNP10c32/in1_8; }
LECROY1443N(r16c2s7)  { out0_15: rNP10c31/in1_16; }
LECROY1443N(r16c2s8)  { out0_15: rNP10c33/in1_16; }

// Land Veto

LECROY1443N(r16c2s9)  { out0_15: rVP1c31/in1_16; }
LECROY1443N(r16c2s10) { out0_15: rVP1c32/in1_16; }
LECROY1443N(r16c2s12) { out0_7:  rVP1c33/in1_8;  }

// TFW

LECROY1443N(r16c2s11) { out0_15: r26c13/in1_16; }
LECROY1443N(r16c2s15) { out0_15: r26c11/in1_16; }
//LECROY1443N(r16c2s15) { out0_15: rNTFRc11/in1_16; }

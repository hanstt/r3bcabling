#include "modules/modules.hh"

//#include "cave/fiber/gfi1.hh"
//#include "cave/fiber/gfi2.hh"

#include "cave/neuland_detector.hh"
#include "cave/neuland/dplane01.hh"
#include "cave/neuland/dplane02.hh"
#include "cave/neuland/dplane03.hh"
#include "cave/neuland/dplane04.hh"

//#include "cave/rackFIBER/crate1.hh"

#include "cave/rackMASTER/crate1.hh"
#include "cave/rackMASTER/crate2.hh"
// #include "cave/rackMASTER/crate3.hh"

#include "cave/rackLAND1/crate1.hh"
#include "cave/rackLAND1/crate2.hh"
#include "cave/rackLAND1/crate3.hh"
#include "cave/rackLAND1/crate4.hh"
#include "cave/rackLAND2/crate1.hh"
#include "cave/rackLAND2/crate2.hh"
#include "cave/rackLAND2/crate3.hh"

#include "cave/rackNEULANDHV/crate3.hh"
#include "cave/rackNEULANDHV/crate4.hh"

#include "cave/rackNEULAND1/crate1.hh"
#include "cave/rackNEULAND1/crate2.hh"
#include "cave/rackNEULAND1/crate3.hh"
#include "cave/rackNEULAND1/crate4.hh"

#include "cave/rackPOS1/detectors.hh"
#include "cave/rackPOS1/crate1.hh"
#include "cave/rackPOS1/crate2.hh"
#include "cave/rackPOS2/crate1.hh"

#include "cave/rackTOF1/crate1.hh"
#include "cave/rackTOF1/crate2.hh"
#include "cave/rackTOF2/crate1.hh"

#include "cave/rackACTAR/detectorACTAR.hh"
#include "cave/rackACTAR/crateACTAR.hh"

#!/usr/bin/perl

entry:
while ($line = <STDIN>)
{
# print "; $line ";

	if ($line =~ /([_a-zA-Z]*)\((.+)\)/)
	{
		$table = lc($1);
		$data  = $2;

		$data  =~ s/ , /,/g;

		$data  =~ s/PM_IDENT/!/;
		$data  =~ s/CABLE\s*,\s*-1/0,0/;

		@data = split /,/,$data;

		$name = shift @data;

		if ($name =~ /^\s*N\d\d/)     { $table =~ s/sig_beam/sig_land/; }
		if ($name =~ /^\s*V\d/)       { $table =~ s/sig_beam/sig_veto/; }
		if ($name =~ /^\s*(CS|CV|XB)\d/) { $table =~ s/sig_beam/sig_tdet/; }

		$name =~ s/CS1_/CS_/;
		$name =~ s/XB1_/XB_/;
		$name =~ s/STR(\d)_0/STR0$1_/;

		$name =~ s/(POS|ROL)(\d+)_0/$1$2_/;

		$command   = "t/m/e";
		$dum_param = "! ! ! ";

		if ($table eq "elecloc")
		{
			if (!($name =~ /^\s*LCF/) &&
					!($name =~ /^\s*L_MPX/))
			{
				next entry;
			}
			$data[4] = "!";
		}
		else
		{
			if ( $name =~ /(XB)/)	    
			{
				$data[0]  = $name;
				$data[3]  = 0;
				$data[4]  = 0;
				$data[5]  = 0;
				$data[10] = "none";
				$data[11] = 1;
				$data[12] = 0;
				$data[13] = 0;
				$command   = "t/w/e";
				$dum_param = "! ";
#				$data[3]  = 0;
#				$data[4]  = 0;
#				$data[5]  = 0;
#				$data[10] = "none";
#				$data[11] = 1;
#				$data[12] = 0;
#				$data[13] = 0;
#				$command   = "t/w/e";
#				$dum_param = "! ";
			}
			else
			{
#	    for ($i = 6; $i <= 13; $i++)
				for ($i = 10; $i <= 13; $i++)
				{
					$data[$i] = "!";
				}
				if ($data[1] =~ /(NIM|NONE)/) 
				{
					$data[2] = "!"; 
					$data[1] = "!";
				}
				$data[0] = "!";
			}

			if ($name =~ /(SCI|ZST|PSP)/)
			{
				next entry;
			}


			if ($table eq "sig_beam" && $data[3] == -1)
			{
				$data[3] = "!";
				$data[4] = "!";
				$data[5] = "!";
			}
		}

		$data_line = join(' ',@data);

		print "$command $table $dum_param $name $data_line y\n";
	}
}

